# Welsh Mutation Analyser

[![pipeline status](https://gitlab.com/prvInSpace/mutation-analyser/badges/master/pipeline.svg)](https://gitlab.com/prvInSpace/mutation-analyser/-/commits/master)

An application that scans through CorCenCC and a version of the corpus that has been retagged Canolfan Bedwyr's SpaCy model
to analyse the usage of mutations in the Welsh language.

## Sources

The mutation rules have been gathered from 3 sources:
| Name                       | Author            | Year |
|----------------------------|-------------------|------|
| Ymarfer Ysgrifennu Cymraeg | Gwyn Thomas       | 2012 |
| Modern Welsh               | Gareth King       | 2003 |
| Y Treigladur               | D. Geraint Thomas | 2018 |

## Running from the commandline

### Run the analysis

Assuming that you have Java 11 installed and setup you can do the following
to launch the application:

```
./gradlew shadowJar
java -jar build/libs/Welsh\ Mutation\ Analyser-1.0-all.jar
```

After the application is finished the data will be exported to the folder called out/

The application only takes 1 parameter which is the verification mode flag.
This is a number and simply actives different verificators in case you want that.
The default is 0 which is off. This is good enough if you just want to run it and see what it exports.
(putting in any other flag will leave you verifying samples for a couple of hours)

### Running the tests

To run the tests on Linux or similar run:
```
./gradlew check
```

## Libraries used

| Name        | Usage |
|-------------|-------|
| [Prv\-Commons](https://gitlab.com/prvInSpace/prv-commons) | Library containing some common components for the project |
| [Ogma](https://gitlab.com/prvInSpace/ogma) | Library for parsing and applying Ogma patterns  |
| [json.org](https://mvnrepository.com/artifact/org.json/json) | JSON library |
| JUnit | Testing library |

