package cymru.prv.mutations;

import cymru.prv.common.threading.TaskSequentializer;
import cymru.prv.mutations.corcen.*;
import cymru.prv.mutations.lemma.Lemma;
import cymru.prv.mutations.lemma.Lemmatizer;
import cymru.prv.mutations.mutations.MutationType;
import cymru.prv.mutations.mutations.Mutations;
import cymru.prv.mutations.verification.ClassificationVerifier;
import cymru.prv.mutations.data.SingleDataPoint;
import cymru.prv.mutations.verification.POSTaggerVerificator;
import cymru.prv.mutations.verification.VerificationMode;
import cymru.prv.ogma.Match;

import java.io.*;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * The main script file of the application
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class CorCenMutationAnalyser implements AutoCloseable {

    // Int to say which verifications methods are enabled
    public static int VERIFICATION_MODE = VerificationMode.of(VerificationMode.NONE);

    private static final String RETAGGED_PATH = "res/retagged-corpus";
    private static final String PATH = "res/corcen/";

    // Other common resources
    private final CorCenCorpus corpus = new CorCenCorpus(PATH);
    private final Lemmatizer lemmatizer = new Lemmatizer();
    private final MutationMatcher matcher = new MutationMatcher(lemmatizer);
    private final TaskSequentializer inputSequentializer = new TaskSequentializer();
    private final POSTaggerVerificator posTaggerVerificator = new POSTaggerVerificator(inputSequentializer, 4000);
    private final ClassificationVerifier classificationVerifier = new ClassificationVerifier(inputSequentializer, 1385);

    // Overarching results
    private final MutationClassificationMap results = new MutationClassificationMap();

    // Contributor results
    private final MutationClassificationMapCollection<Integer> ageResults = new MutationClassificationMapCollection<>();
    private final MutationClassificationMapCollection<Integer> ageResultsFiltered = new MutationClassificationMapCollection<>();
    private final MutationClassificationMapCollection<Integer> fluencyResults = new MutationClassificationMapCollection<>();
    private final MutationClassificationMapCollection<Integer> learnerResults = new MutationClassificationMapCollection<>();
    private final MutationClassificationMapCollection<Location> placeResults = new MutationClassificationMapCollection<>();

    // File type results
    private final MutationClassificationMapCollection<CorCenFormat> formatResults = new MutationClassificationMapCollection<>();

    // Rule-based results
    private final MutationClassificationMapCollection<String> ruleResults = new MutationClassificationMapCollection<>();

    // Word results
    // Word Correct Wrong
    private final Map<String, SingleDataPoint> wordResults = new ConcurrentHashMap<>();
    private final Map<String, AtomicInteger> ambiguousResults = new ConcurrentHashMap<>();

    // Random for random stuff
    private final Random random = new Random();

    // Constructor required so that exceptions can be thrown
    public CorCenMutationAnalyser() throws IOException {}

    // Running analysis

    /**
     * Starts the analysis of the entire
     * corpus using multi-threading.
     */
    public void runAnalysis() {
        corpus.getFiles().parallelStream().forEach(this::runAnalysisForFile);
    }


    /**
     * Starts the analysis for the given CorCenCC file
     *
     * @param file the file to analyse
     */
    private void runAnalysisForFile(CorCenFile file) {
        try {
            var textReader = new CorCenFileReader(
                    posTaggerVerificator,
                    file,
                    new Scanner(Path.of(PATH, "complete_corpus", file.getName())),
                    new Scanner(Path.of(RETAGGED_PATH, file.getName()))
            );
            matcher.findMatches(textReader, (m) -> handleMatch(file, m));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // Exporting Data

    /**
     * Exports all of the data gathered from the corpus and
     * from the human verification.
     *
     * @throws IOException If an error occurs during writing to the files
     */
    public void exportData() throws IOException {

        // Wait for all input tasks to finish

        results.export("out/results.tsv", "results");
        ageResults.export("out/age.tsv");
        ageResultsFiltered.export("out/age-filtered.tsv");
        fluencyResults.export("out/fluency.tsv");
        learnerResults.export("out/learner.tsv");
        ruleResults.export("out/rules.tsv");
        formatResults.export("out/format.tsv");
        placeResults.export("out/place.tsv");

        posTaggerVerificator.export("out/pos");
        classificationVerifier.export("out/classification");

        // Type, Tag, Results
        Map<String, MutationClassificationMapCollection<String>> taxonomyResults = new HashMap<>();
        for (Taxonomy taxonomy : corpus.getTaxonomies()) {
            taxonomyResults.putIfAbsent(taxonomy.getType(), new MutationClassificationMapCollection<>());
            var map = taxonomyResults.get(taxonomy.getType());
            map.put(taxonomy.getTag(), taxonomy.getData());
        }

        // Export a file for every type of taxonomy
        for (String type : taxonomyResults.keySet()) {
            taxonomyResults.get(type).export("out/" + type + ".tsv");
        }

        // Export words:
        FileWriter wordsOut = new FileWriter("out/words.tsv");
        for(String key : wordResults.keySet()) {
            var val = wordResults.get(key);
            wordsOut.write(key + "\t" + val.getCorrect() + "\t" + val.getSample() + "\n");
        }
        wordsOut.flush();

        // Export ambiguous counters
        FileWriter ambiguousOut = new FileWriter("out/ambiguous.tsv");
        for(String key : ambiguousResults.keySet())
            ambiguousOut.write(key + "\t" + ambiguousResults.get(key) + "\n");
        ambiguousOut.flush();
    }

    // Handle matches

    /**
     * Handles an individual match. Tries to figure
     * out what mutation is should have been and which
     * mutation is actually present, then increment
     * the data in this file and for the CorCen file.
     *
     * @param file The CorCenCC file we are reading
     * @param match The match itself
     */
    private void handleMatch(CorCenFile file, Match match) {
        // Fetch possible mutations
        var possibleMutations = getPotentialMutations(match);
        if(possibleMutations.size() == 0)
            return;

        // Fetch mutation-types
        var mutationTypes = possibleMutations.values().stream().distinct().toArray(MutationType[]::new);
        var expected = Enum.valueOf(MutationType.class, new LinkedList<>(match.getGroups()).get(0));
        var actual = mutationTypes.length == 1 ? mutationTypes[0] : MutationType.AMBIGUOUS;

        // Check if word can actually mutate to the given mutation
        var lemma = possibleMutations.keySet().toArray(String[]::new)[0];
        if(possibleMutations.size() == 1 && !Mutations.canMutate(lemma, expected))
            expected = MutationType.RADICAL;

        // Update table
        results.increment(expected, actual);
        ruleResults.increment(match.getSearchPatternName(), expected, actual);
        formatResults.increment(file.getFormat(), expected, actual);

        file.updateData(expected, actual);
        addDataBasedOnContributor(file.getContributor(), expected, actual);

        if(VerificationMode.MATCHER.isSet(VERIFICATION_MODE))
            classificationVerifier.addInstance(match, expected, actual, mutationTypes);

        // Update word info
        for(String potentialLemma : possibleMutations.keySet()) {
            // If the word can not even mutate to the expected thing there is
            // reason to add data about whether it was mutated or not
            if(!Mutations.canMutate(potentialLemma, expected))
                continue;

            // Add data about whether the word was correctly mutated or not
            wordResults.putIfAbsent(potentialLemma, new SingleDataPoint());
            if (expected == actual)
                wordResults.get(potentialLemma).incrementCorrect();
            else
                wordResults.get(potentialLemma).incrementWrong();

            // If it is ambiguous add data update data about that
            if(actual == MutationType.AMBIGUOUS){
                ambiguousResults.putIfAbsent(potentialLemma, new AtomicInteger(0));
                ambiguousResults.get(potentialLemma).incrementAndGet();
            }
        }
    }


    /**
     * Creates a map with the potential mutation types
     * that could be present and what word could have
     * given rise to that mutation. (The radical form)
     *
     * @param match The match
     * @return A map with radical forms that could have
     *         given rise to the mutation.
     */
    private Map<String, MutationType> getPotentialMutations(Match match){
        var target = match.getTarget().split("_")[0].toLowerCase();
        var possibleMutations = Mutations.getPossibleMutations(target);

        // If no mutations is possible, return
        if (possibleMutations.size() == 0)
            return possibleMutations;

        // Filter out words that does not exist in our dictionary
        for (String form : possibleMutations.keySet().toArray(String[]::new)) {
            if (!lemmatizer.containsForm(form))
                possibleMutations.remove(form);
        }

        // If greater than 0 see if we can narrow it down based on the lemmas type
        if (possibleMutations.size() > 1)
            filterOutLemmasBasedOnType(match, possibleMutations);

        return possibleMutations;
    }


    /**
     * Filters the map based on the type of the word.
     * This is in an attempt to make the list of probable
     * words shorter (hopefully down to 1).
     *
     * N.B Directly edits the map that it is served
     *
     * @param match The match
     * @param possibleMutations Map of possible mutations
     */
    private void filterOutLemmasBasedOnType(Match match, Map<String, MutationType> possibleMutations){
        // Type we are searching for
        var type = match.getTarget().split("_")[1];

        // List all forms
        List<String> doesNotMatch = new LinkedList<>(possibleMutations.keySet());
        for (String form : possibleMutations.keySet()) {
            var words = lemmatizer.getLemmas(form);
            for(Lemma word : words)
                if(type.startsWith(word.getType()))
                    doesNotMatch.remove(form);
        }

        // If not all forms are taken away:
        if(doesNotMatch.size() != 0 && doesNotMatch.size() != possibleMutations.size()) {
            if(random.nextInt(1000) == 0 && false){
                System.out.println();
                System.out.println(String.join(" ", match.getWords()));
                System.out.println("Possible: " + String.join(", ", possibleMutations.keySet()));
                System.out.println("Removed:  " + String.join(", ", doesNotMatch));
                System.out.println("Did not match: " + type);
            }
            for (String form : doesNotMatch)
                possibleMutations.remove(form);
        }
    }


    /**
     * Adds data based on the data of the contributor.
     *
     * @param contributor The contributor in question
     * @param expected The expected classification
     * @param actual The actual classification
     */
    private void addDataBasedOnContributor(Contributor contributor, MutationType expected, MutationType actual) {
        if (contributor == null || contributor.getType() != 1)
            return;

        if (contributor.getYearOfBirth() != 0) {
            ageResults.increment(contributor.getYearOfBirth(), expected, actual);

            var decade = contributor.getYearOfBirth() - contributor.getYearOfBirth() % 10;
            ageResultsFiltered.increment(decade, expected, actual);
        }

        if (contributor.getFluency() != 0)
            fluencyResults.increment(contributor.getFluency(), expected, actual);

        if (contributor.getLearner() != 0)
            learnerResults.increment(contributor.getLearner(), expected, actual);

        if (contributor.getLocation() != Location.NO_PLACE)
            placeResults.increment(contributor.getLocation(), expected, actual);
    }

    // Main function

    public static void main(String[] args) {

        if(args.length > 0){
            VERIFICATION_MODE = Integer.parseInt(args[0]);
            System.out.println("Using verification mode " + VERIFICATION_MODE);
            for(VerificationMode mode : VerificationMode.values())
                if(mode.isSet(VERIFICATION_MODE) && mode != VerificationMode.ALL)
                    System.out.println(mode);
        }

        long start = System.currentTimeMillis();
        try(var analyser = new CorCenMutationAnalyser()){
            // Setup analyser
            System.out.println("Setup: " + (System.currentTimeMillis() - start) + " ms");

            // Run analysis
            long analysisStart = System.currentTimeMillis();
            analyser.runAnalysis();
            System.out.println("\nAnalysing: " + (System.currentTimeMillis() - analysisStart) + " ms");

            System.out.println("Finished collecting data, waiting for input to finish");
            analyser.inputSequentializer.finishAndClose();
            System.out.println("Input : " + ((System.currentTimeMillis() - start)/(1000*60)) + " min");

            // Run exporter
            long exportStart = System.currentTimeMillis();
            analyser.exportData();
            System.out.println("Exporting: " + (System.currentTimeMillis() - exportStart) + " ms");
            System.out.println("Total: " + (System.currentTimeMillis() - start) + " ms");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * If the application crashes we need to ensure
     * that the input sequentializer thread is properly
     * stopped.
     *
     * @throws Exception If it fails to kill the thread
     */
    @Override
    public void close() throws Exception {
        // Forces the input thread to die
        inputSequentializer.close();
    }
}
