package cymru.prv.mutations.data;

import java.util.concurrent.atomic.AtomicInteger;

public class SingleDataPoint extends DataPoint {

    private final AtomicInteger correct = new AtomicInteger();

    public void incrementCorrect(){
        incrementSample();
        correct.incrementAndGet();
    }

    public void incrementWrong(){
        incrementSample();
    }


    public int getCorrect() {
        return correct.get();
    }

    @Override
    public String toString() {
        return correct.get() + "\t" + super.toString();
    }
}
