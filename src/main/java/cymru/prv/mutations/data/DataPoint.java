package cymru.prv.mutations.data;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * An abstract class representing a single datapoint
 * A datapoint contains the total number of occurances
 * plus the number of samples so that a margin of error
 * can be calculated later
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public abstract class DataPoint {

    private final AtomicInteger total = new AtomicInteger();
    private final AtomicInteger sample = new AtomicInteger();

    void incrementSample(){
        sample.incrementAndGet();
    }


    /**
     * Increments the total number of
     * occurrences
     */
    public void incrementTotal(){
        total.incrementAndGet();
    }


    /**
     * The total amount of occurrences
     *
     * @return the total amount of occurrences
     */
    public int getTotal(){
        return total.get();
    }


    /**
     * The amount of samples taken
     *
     * @return the number of samples
     */
    public int getSample(){
        return sample.get();
    }

    /**
     * Make a TSV formatted string with
     * the values contained in the object
     *
     * @return the object in a TSV form
     */
    @Override
    public String toString() {
        return sample + "\t" + total;
    }
}
