package cymru.prv.mutations.data;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MultiDataPoint<K> extends DataPoint {

    private final Map<K, AtomicInteger> sets = new HashMap<>();

    public void increaseForSets(Collection<K> setsToIncrement){
        incrementSample();
        for(K set : setsToIncrement){
            sets.putIfAbsent(set, new AtomicInteger());
            sets.get(set).incrementAndGet();
        }
    }

    public Set<K> getSets(){
        return sets.keySet();
    }

    public int getValueForSet(K set){
        if(!sets.containsKey(set))
            return 0;
        return sets.get(set).get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MultiDataPoint<?> that = (MultiDataPoint<?>) o;
        return Objects.equals(sets.keySet(), that.sets.keySet());
    }

    @Override
    public int hashCode() {
        return Objects.hash(sets.keySet());
    }

    public String toString(Iterable<K> order){
        StringBuilder sb = new StringBuilder();
        for(K key : order){
            sb.append(getValueForSet(key))
                    .append("\t");
        }
        sb.append(super.toString());
        return sb.toString();
    }

    @Override
    public String toString() {
        return toString(sets.keySet());
    }
}
