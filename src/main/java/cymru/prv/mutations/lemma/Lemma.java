package cymru.prv.mutations.lemma;

import java.util.Objects;

public class Lemma {

    private final String word;
    private final String type;

    public Lemma(String word, String type){
        this.word = word;
        this.type = type;
    }

    public String getWord(){
        return word;
    }

    public String getType(){
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lemma lemma = (Lemma) o;
        return Objects.equals(word, lemma.word) && Objects.equals(type, lemma.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, type);
    }
}
