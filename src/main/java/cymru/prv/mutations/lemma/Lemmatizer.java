package cymru.prv.mutations.lemma;

import cymru.prv.mutations.mutations.Mutations;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Simple lemmatizer for Welsh. It uses a merged of 2 lexicons + an additional list of
 * missing terms.
 * <p>
 * Uses the Bangor Welsh Lexicon provided by the Language Technologies Unit
 * at Bangor University. The lexicon can be found at
 * <a href="https://github.com/techiaith/lecsicon-cymraeg-bangor">Github</a>
 * </p>
 * <p>
 * It also uses the lexicon available in the source code for CyTag.
 * The lexicon can be found at
 * <a href="https://github.com/CorCenCC/CyTag">Github</a>
 * </p>
 *
 * @author Preben Vangberg
 * @since 25.02.2021
 */
public class Lemmatizer {

    private final HashMap<String, List<Lemma>> words = new HashMap<>();

    // Type Word Object
    private final Map<String, Map<String, Lemma>> lemmas = new HashMap<>();

    long mergedlemmas = 0;

    public Lemmatizer() throws IOException {
        loadMergedLexicon();
    }

    private void loadMergedLexicon() throws IOException {
        loadLexicon();
        //loadLexiconJson("/lemmatizer/lecsicon.json");
        //loadLexiconJson("/lemmatizer/lecsicon_missing.json");
    }

    private void loadLexicon() throws IOException {
        InputStream loader = Lemmatizer.class.getResourceAsStream("/lemmatizer/lecsicon_corcen.json");
        JSONObject object = null;
        object = new JSONObject(new String(loader.readAllBytes()));
        for(String key : object.keySet()){
            JSONArray array = object.getJSONArray(key);
            for(Object o : array){
                JSONObject lemma = (JSONObject) o;
                var word = lemma.getString("lemma");
                var type = lemma.getString("pos_basic");

                // Fetch Lemma object
                lemmas.putIfAbsent(type, new HashMap<>());
                lemmas.get(type).putIfAbsent(word, new Lemma(word, type));
                var lemmaObj = lemmas.get(type).get(word);

                words.putIfAbsent(key, new LinkedList<>());
                if(!words.get(key).contains(lemmaObj))
                    words.get(key).add(lemmaObj);
            }
        }
    }

    /*
    private void loadLexiconJson(String path){
        InputStream loader = Lemmatizer.class.getResourceAsStream(path);
        JSONObject object = null;
        try {
            object = new JSONObject(new String(loader.readAllBytes()));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        for (Iterator<String> it = object.keys(); it.hasNext(); ) {
            String key = it.next();
            if(!words.containsKey(key))
                words.put(key, new LinkedList<>());
            List<String> lemmas = words.get(key);
            JSONArray array = object.getJSONArray(key);
            mergedlemmas++;
            for(Object o : array) {
                String lemma = (String) o;
                if(!lemmas.contains(lemma))
                    lemmas.add(lemma);
            }
        }
    }
    */
    /**
     * Finds all the lemmas for any given word.
     * Will try to demutate the word as well
     * if it can't find it.
     *
     * @param word the form you want to lemmatize
     * @return a list of possible lemmas
     */
    public List<Lemma> getLemmas(String word) {
        word = word.toLowerCase();
        if(words.containsKey(word))
            return words.get(word);

        List<Lemma> foundLemmas = new LinkedList<>();
        Set<String> potentialMutations = Mutations.getPossibleMutations(word).keySet();
        for(String form : potentialMutations) {
            if (words.containsKey(form))
                foundLemmas.addAll(words.get(form));
        }

        return foundLemmas.size() != 0 ? foundLemmas : null;
    }

    public boolean containsForm(String string){
        return words.containsKey(string);
    }

    /**
     * Returns the number of forms the dictionary contains
     * @return the number of forms.
     */
    public long getNumberOfWords(){
        return words.size();
    }

}
