package cymru.prv.mutations.mutations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A helper class containing several functions
 * related to Welsh mutations
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public final class Mutations {

    // To prevent initializations
    private Mutations(){}


    /**
     * Returns a regex that matches all mutation forms
     * of the word in question.
     *
     * @param radical The word to mutate
     * @return A regex that matches every mutated for of the word
     */
    public static String toRegexWithMutations(String radical){
        if(radical.matches("^p[^h].*"))
            return "(ph?|b|mh)" + radical.substring(1);
        if(radical.matches("^t[^h].*"))
            return "(th?|d|nh)" + radical.substring(1);
        if(radical.matches("^c[^h].*"))
            return "(ch?|g|ngh)" + radical.substring(1);
        if(radical.startsWith("b"))
            return "[bfm]" + radical.substring(1);
        if(radical.startsWith("g"))
            return "(g|ng)?" + radical.substring(1);
        if(radical.startsWith("d"))
            return "(dd?|n)" + radical.substring(1);
        if(radical.startsWith("ll"))
            return "ll?" + radical.substring(2);
        if(radical.startsWith("rh"))
            return "rh?" + radical.substring(2);
        if(radical.startsWith("m"))
            return "[mf]" + radical.substring(1);
        return radical;
    }


    /**
     * Takes the word and tries to figure out all
     * the possible radical versions of the word.
     *
     * @param word The word to de-mutate
     * @return A map containing the potential radical version and the potential mutation
     */
    public static Map<String, MutationType> getPossibleMutations(String word){
        word = word.toLowerCase();
        Map<String, MutationType> map = new HashMap<>();
        if(word.matches("([bg]|[ptcm][^h]|d[^d]|ll|rh).*"))
            map.put(word, MutationType.RADICAL);

        // Soft mutations
        if(word.startsWith("b"))
            map.put("p" + word.substring(1), MutationType.SOFT);
        if(word.startsWith("dd"))
            map.put("d" + word.substring(2), MutationType.SOFT);
        if(word.matches("f[^f].*")){
            map.put("b" + word.substring(1), MutationType.SOFT);
            map.put("m" + word.substring(1), MutationType.SOFT);
        }
        if(word.startsWith("g"))
            map.put("c" + word.substring(1), MutationType.SOFT);
        if(word.matches("l[^l].*"))
            map.put("l" + word, MutationType.SOFT);
        if(word.matches("d[^d].*"))
            map.put("t" + word.substring(1), MutationType.SOFT);
        if(word.matches("r[^h].*"))
            map.put("rh" + word.substring(1), MutationType.SOFT);
        if(word.matches("^([aeiouwyâêîôûŵŷ]|r[^h]|l[^l]).*"))
            map.put("g" + word, MutationType.SOFT);

        // Nasal mutations
        if(word.startsWith("mh"))
            map.put("p" + word.substring(2), MutationType.NASAL);
        else if(word.startsWith("m"))
            map.put("b" + word.substring(1), MutationType.NASAL);
        else if(word.startsWith("ngh"))
            map.put("c" + word.substring(3), MutationType.NASAL);
        else if(word.startsWith("ng"))
            map.put(word.substring(1), MutationType.NASAL);
        else if(word.startsWith("nh"))
            map.put("t" + word.substring(2), MutationType.NASAL);
        else if(word.startsWith("n"))
            map.put("d" + word.substring(1), MutationType.NASAL);

        // Aspirate mutations
        if(word.startsWith("ch"))
            map.put("c" + word.substring(2), MutationType.ASPIRATE);
        if(word.startsWith("ph"))
            map.put("p" + word.substring(2), MutationType.ASPIRATE);
        if(word.startsWith("th"))
            map.put("t" + word.substring(2), MutationType.ASPIRATE);

        // H-prothesis
        if(word.matches("^h[aeiouwyâêîôûŵŷ].*")) {
            map.put(word, MutationType.RADICAL);
            map.put(word.substring(1), MutationType.H_PROTHESIS);
        }

        return map;
    }


    /**
     * Checks whether the word is able to mutate to the given mutation
     * form.
     *
     * @param word The word to check
     * @param type The target mutation type
     * @return True if the word can mutate to the target mutation type, false otherwise
     */
    public static boolean canMutate(String word, MutationType type){
        if(unMutateableWords.contains(word))
            return false;
        if (type == MutationType.SOFT)
            return word.matches("^([ptcbdgm][^h]|ll|rh).*");
        if (type == MutationType.NASAL)
            return word.matches("^[ptcbdg][^h].*");
        if (type == MutationType.ASPIRATE)
            return word.matches("^[ptc].*");
        return true;
    }

    /**
     * List of words that can not mutate
     *
     * @see Mutations::canMutate
     */
    private static final List<String> unMutateableWords = List.of(
            "beth",
            "ble",
            "byth",
            "dros",
            "doedd",
            "doeddet",
            "doeddwn",
            "doedden",
            "doeddech",
            "dy",
            "dwi",
            "dw",
            "dydw",
            "dwyt",
            "dydyn",
            "dydy",
            "dych",
            "dyw",
            "dydi",
            "lle",
            "mae",
            "mai",
            "mor",
            "pan",
            "taw",
            "tua"
    );

}
