package cymru.prv.mutations.mutations;


/**
 * Represents all possible mutations + ambiguous for cases
 * where the mutation can not be determined (see 'ei' for example)
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public enum MutationType {
    RADICAL, SOFT, NASAL, ASPIRATE, H_PROTHESIS, AMBIGUOUS
}
