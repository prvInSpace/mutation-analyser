package cymru.prv.mutations;

import cymru.prv.common.classification.ClassificationMap;
import cymru.prv.mutations.mutations.MutationType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;


/**
 * Represents a classification map.
 * Used to store information about the number
 * of expected object and the actual class.
 *
 * The class is tread-safe.
 *
 * Other than that it contains functions to export
 * the data of the classification map as a tab
 * separated table.
 *
 * @see ClassificationMap
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class MutationClassificationMap extends ClassificationMap<MutationType> {


    /**
     * Exports the data of the classification map
     * to the path given as a parameter.
     *
     * @param path Where to export the data
     * @param name The name of the table
     * @throws FileNotFoundException if the program is unable to open the file
     */
    public void export(String path, String name) throws FileNotFoundException {
        File file = new File(path);
        file.getParentFile().mkdirs();
        PrintWriter writer = new PrintWriter(file);
        this.export(writer, name);
    }


    /**
     * Prints the data of the classification map to
     * the writer provided as a tab separated table.
     *
     * @param writer where to write the data
     * @param name the name of the table
     */
    public void export(PrintWriter writer, String name) {

        // Write headers
        writer.print(name);
        for (MutationType type : MutationType.values())
            writer.print("\t" + type);
        writer.println();

        // Write data
        for (MutationType expected : MutationType.values()) {
            if(expected == MutationType.AMBIGUOUS)
                continue;
            writer.write(expected.toString());
            for (MutationType actual : MutationType.values())
                writer.write("\t" + getData(expected, actual));
            writer.println();
        }

        // Flush the file
        writer.flush();
    }
}
