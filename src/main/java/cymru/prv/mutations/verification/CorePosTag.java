package cymru.prv.mutations.verification;


/**
 * An enum representing all the possible
 * core part-of-speech tags that we are recognising.
 *
 * Compared to some other sets of core POS lists
 * this was created as an intersection of the POS
 * tags of the CorCenCC corpus and hte Techiaith
 * Spacy model.
 *
 * Note that the tag AUX (auxiliary verb) has been
 * merged with VERB as CorCenCC does not make a
 * distinction between them.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public enum CorePosTag {
    ABBR    ("Abbreviation"),
    ADP     ("Adposition"),
    ADV     ("Adverb"),
    ADJ     ("Adjective"),
    CONJ    ("Conjunction"),
    DET     ("Determiner"),
    NUM     ("Number"),
    NOUN    ("Noun"),
    INTJ    ("Interjection"),
    PART    ("Particle"),
    PRON    ("Pronoun"),
    PROPN   ("Proper noun"),
    PUNCT    ("Punctuation"),
    SYM     ("Symbol"),
    VERB    ("Verb"),
    // Should not really be used as the taggers should make an attempt
    OTHER   ("Other"),
    FOREIGN ("Foreign"),
    UNK     ("Unknown"),
    LETTER  ("Letter");

    private final String fullname;

    CorePosTag(String fullname) {
        this.fullname = fullname;
    }


    /**
     * Takes a POS tag from the CorCenCC corpus and
     * returns the CorPosTag that it equals.
     *
     * @param tag The CorCenCC tag to convert
     * @return The CorePosTag is is linked to
     */
    public static CorePosTag fromCorCenTag(String tag){
        // Check determiners and verbs as they overlap somewhat
        if(tag.startsWith("Ban") || tag.equals("YFB"))
            return DET;
        if(tag.startsWith("B"))
            return VERB;

        // Proper nouns, nouns, and interjections overlap as well.
        if(tag.equals("Ebych"))
            return INTJ;
        if(tag.startsWith("Ep"))
            return PROPN;
        if(tag.startsWith("E"))
            return NOUN;

        if(tag.equals("Gwacr") || tag.equals("Gwtalf"))
            return ABBR;
        if(tag.startsWith("Ans"))
            return ADJ;
        if(tag.startsWith("Ar"))
            return ADP;
        if(tag.startsWith("Adf"))
            return ADV;
        if(tag.startsWith("Cys"))
            return CONJ;
        if(tag.startsWith("Rhi") || tag.equals("Gwdig") || tag.equals("Gwrhuf"))
            return NUM;
        if(tag.startsWith("U"))
            return PART;
        if(tag.startsWith("Atd"))
            return PUNCT;
        if(tag.startsWith("Rha"))
            return PRON;
        if(tag.equals("Gwfform") || tag.equals("Gwsym"))
            return SYM;

        if(tag.equals("Gwann"))
            return OTHER;
        if(tag.equals("Gwllyth"))
            return LETTER;
        if(tag.equals("Gwest"))
            return FOREIGN;
        if(tag.equals("Anon") || tag.equals("unk") || tag.equals("Ymadr") || tag.isBlank())
            return UNK;

        throw new RuntimeException("Unknown tag " + tag);
    }


    /**
     * Takes a POS tag from the Bangor/Techiaith
     * Spacy model and returns the CorPosTag
     * that it equals.
     *
     * Note that AUX tags are converted to VERB
     * tags.
     *
     * @param tag The CorCenCC tag to convert
     * @return The CorePosTag is is linked to
     */
    public static CorePosTag fromBangorTag(String tag){
        if(tag.isBlank())
            return UNK;
        if(tag.equals("AUX"))
            return VERB;
        if(tag.equals("X"))
            return UNK;
        if(tag.equals("SPACE"))
            return UNK;
        return Enum.valueOf(CorePosTag.class, tag);
    }

}
