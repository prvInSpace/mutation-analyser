package cymru.prv.mutations.verification;

import cymru.prv.mutations.data.MultiDataPoint;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * A class that represents a verificator
 * result for an unspecified number of
 * POSTaggers.
 *
 * Overrides equals and hashCode
 * to ensure that it works with sets.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class POSTaggerResult {

    private final Map<POSTagger, CorePosTag> keys;
    private final AtomicInteger total = new AtomicInteger();
    private final MultiDataPoint<POSTagger> data = new MultiDataPoint<>();

    public POSTaggerResult(Map<POSTagger, CorePosTag> keys) {
        this.keys = keys;
    }

    public void incrementTotal(){
        total.incrementAndGet();
    }

    public void increment(Collection<POSTagger> postaggers){
        data.increaseForSets(postaggers);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        POSTaggerResult that = (POSTaggerResult) o;
        return Objects.equals(keys, that.keys);
    }

    @Override
    public int hashCode() {
        return Objects.hash(keys);
    }
}
