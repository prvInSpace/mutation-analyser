package cymru.prv.mutations.verification;

import cymru.prv.common.cli.ConsoleInterface;
import cymru.prv.common.threading.TaskSequentializer;
import cymru.prv.mutations.data.SingleDataPoint;
import cymru.prv.mutations.mutations.MutationType;
import cymru.prv.ogma.Match;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Preben Vangberg
 */
public class ClassificationVerifier {

    private final Random random = new Random();
    private final TaskSequentializer sequentializer;
    private final int chance;

    private final SingleDataPoint results = new SingleDataPoint();

    private final Map<String, SingleDataPoint> ruleResults = new HashMap<>();

    public ClassificationVerifier(TaskSequentializer sequentializer, int chance){
        this.sequentializer = sequentializer;
        this.chance = chance;
    }

    public void addInstance(Match match, MutationType expected, MutationType actual, MutationType[] mutationTypes){

        results.incrementTotal();

        ruleResults.putIfAbsent(match.getSearchPatternName(), new SingleDataPoint());

        var datapoints = List.of(
                ruleResults.get(match.getSearchPatternName())
        );

        for(SingleDataPoint point : datapoints)
            point.incrementTotal();

        if(random.nextInt(chance) == 0)
            sequentializer.addTask(() -> handleSample(match, expected, actual, mutationTypes, datapoints));
    }

    private void handleSample(Match match, MutationType expected, MutationType actual, MutationType[] mutationTypes, List<SingleDataPoint> datapoints){
        printMatch(match, expected, actual, mutationTypes);
        System.out.println("\nShould a mutation occur here?\n1: Correct | 2: Wrong:");
        var answer = getSelection();
        if(answer) {
            results.incrementCorrect();
            datapoints.forEach(SingleDataPoint::incrementCorrect);
        }
        else {
            results.incrementWrong();
            datapoints.forEach(SingleDataPoint::incrementWrong);
        }
    }

    private void printMatch(Match match, MutationType expected, MutationType actual, MutationType[] mutationTypes){
        var sequence = match.getFullSequence();
        sequence.set(match.getTargetIndex(), "\u001B[31m" + match.getTarget() + "\u001B[0m");
        sequence = sequence.stream().map(x -> x.replaceAll("_[A-Za-z0-9]+", "")).collect(Collectors.toList());

        System.out.println();
        System.out.println(String.join(" ", match.getWords()));
        System.out.println(String.join(" ", sequence));
        System.out.println(match.getSearchPatternName());
        System.out.println("Expected: " + expected + " Actual: " + actual);

        // If ambiguous print all the types it could have been
        if(actual == MutationType.AMBIGUOUS) {
            System.out.println(String.join(", ",
                    Arrays.stream(mutationTypes).map(MutationType::toString).toArray(String[]::new)));
        }
    }

    private boolean getSelection(){
        String line = "";
        while(line.isBlank()){
            line = ConsoleInterface.get().nextLine();
            if(!line.matches("([1-2]|stop)"))
                line = "";
        }
        if(line.equals("stop"))
            Thread.currentThread().interrupt();
        return line.equals("1");
    }

    public void export(String folderPath) throws IOException {
        Path.of(folderPath).toFile().mkdirs();
        Files.writeString(Path.of(folderPath, "resuls.tsv"), results.toString());

        Files.writeString(Path.of(folderPath, "rules.tsv"),
                String.join("\n", ruleResults.keySet().stream()
                        .map(x -> x + "\t" + ruleResults.get(x))
                        .toArray(String[]::new)));

    }

}
