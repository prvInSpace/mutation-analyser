package cymru.prv.mutations.verification;

import cymru.prv.common.cli.ConsoleInterface;
import cymru.prv.common.threading.TaskSequentializer;
import cymru.prv.mutations.corcen.CorCenFormat;
import cymru.prv.mutations.data.MultiDataPoint;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class POSTaggerVerificator {

    private final MultiDataPoint<POSTagger> results = new MultiDataPoint<>();
    private final Map<Map<POSTagger, CorePosTag>, MultiDataPoint<POSTagger>> tagResults = new ConcurrentHashMap<>();
    private final Map<CorCenFormat, MultiDataPoint<POSTagger>> resultsByFormat = new ConcurrentHashMap<>();

    private final int chance;
    private final TaskSequentializer sequentializer;

    private final Random random = new Random();

    public POSTaggerVerificator(TaskSequentializer sequentializer, int chance) {
        this.chance = chance;
        this.sequentializer = sequentializer;

        for (CorCenFormat format : CorCenFormat.values())
            resultsByFormat.put(format, new MultiDataPoint<>());
    }

    public void addInstance(String text, CorCenFormat format, Map<POSTagger, CorePosTag> taggers) {

        // Merge the taggers together based on their common tags
        HashMap<CorePosTag, List<POSTagger>> possibilities = new HashMap<>();
        for (POSTagger tagger : taggers.keySet()) {
            var tag = taggers.get(tagger);
            possibilities.putIfAbsent(tag, new LinkedList<>());
            possibilities.get(tag).add(tagger);
        }

        // If it 1 that means that all taggers agree
        if (possibilities.size() == 1)
            return;

        // Create a temp object and try to retrieve it
        // from the set of results
        if(!tagResults.containsKey(taggers))
            tagResults.put(taggers, new MultiDataPoint<>());
        var tagResult = tagResults.get(taggers);

        // Increment total for all of the data-points
        results.incrementTotal();
        tagResult.incrementTotal();
        resultsByFormat.get(format).incrementTotal();

        // Check if we want to verify this sample
        if (chance > 0 && random.nextInt(chance) != 0)
                return;

        // List of increment functions
        List<Consumer<List<POSTagger>>> incrementers = List.of(
                tagResult::increaseForSets,
                resultsByFormat.get(format)::increaseForSets
        );

        // Send the sample to be verified by a qualified human
        sequentializer.addTask(() -> handleSample(text, incrementers, possibilities));
    }


    /**
     * Gets input from the user to determine which data-points to increase.
     *
     * @param text          The text to display to the user
     * @param dataPoints    The data-points to increase
     * @param possibilities The possible POS tags and the POS taggers
     */
    private void handleSample(String text, List<Consumer<List<POSTagger>>> dataPoints, Map<CorePosTag, List<POSTagger>> possibilities) {
        // Print sentences
        System.out.println();
        System.out.println(text);

        // Shuffle and print options
        var keys = new LinkedList<>(possibilities.keySet());
        Collections.shuffle(keys);

        for (int i = 0; i < keys.size(); ++i) {
            System.out.printf("%d: %s ", (i + 1), keys.get(i));
        }
        System.out.println("0: None");

        Set<Integer> indexes = getIndexes();

        var selectedKeys = keys.stream()
                .filter(x -> indexes.contains(keys.indexOf(x)))
                .collect(Collectors.toList());

        LinkedList<POSTagger> selectedTaggers = new LinkedList<>();
        for (CorePosTag tag : selectedKeys)
            selectedTaggers.addAll(possibilities.get(tag));

        results.increaseForSets(selectedTaggers);
        for (var dataPoint : dataPoints)
            dataPoint.accept(selectedTaggers);
    }

    MultiDataPoint<POSTagger> getResults() {
        return results;
    }

    Map<CorCenFormat, MultiDataPoint<POSTagger>> getFormatResults(){
        return resultsByFormat;
    }

    Set<Integer> getIndexes() {
        var line = "";

        var in = ConsoleInterface.get();

        while (line.isBlank()) {
            line = in.nextLine();
            line = line.trim();
            if (!line.matches("(0|[1-9]+|stop)"))
                line = "";
        }
        if(line.equals("stop"))
            Thread.currentThread().interrupt();

        if (line.equals("0"))
            return Set.of();
        return line.chars().map(x -> x - 1 - '0').boxed().collect(Collectors.toSet());
    }

    public void export(String folderPath) throws IOException {
        Path.of(folderPath).toFile().mkdirs();
        printResults(folderPath);
        printTagResults(folderPath);
        printFormatResults(folderPath);
    }

    private void printResults(String folderPath) throws IOException {
        var writer = new PrintWriter(new FileWriter(Path.of(folderPath, "results.tsv").toFile()));
        for(POSTagger tagger : POSTagger.values())
            writer.print(tagger + "\t");
        exportMultiSetDataPoint(writer, results);
        writer.flush();
        writer.close();
    }

    private void printTagResults(String folderPath) throws IOException {
        var writer = new PrintWriter(new FileWriter(Path.of(folderPath, "tags.tsv").toFile()));
        for(Map<POSTagger, CorePosTag> tags : tagResults.keySet()) {
            for (POSTagger tagger : POSTagger.values())
                writer.print(tags.get(tagger) + "\t");
            exportMultiSetDataPoint(writer, tagResults.get(tags));
        }
        writer.flush();
        writer.close();
    }

    private void printFormatResults(String folderPath) throws IOException {
        var writer = new PrintWriter(new FileWriter(Path.of(folderPath, "formats.tsv").toFile()));
        writer.println("format\tCorCen\tBangor\tCombined\tSample\tTotal");
        for(CorCenFormat format : resultsByFormat.keySet()){
            writer.print(format + "\t");
            writer.print(resultsByFormat.get(format).toString(Arrays.asList(POSTagger.values())));
            writer.println();
        }
        writer.flush();
        writer.close();
    }

    private void exportMultiSetDataPoint(PrintWriter writer, MultiDataPoint<POSTagger> data){
        writer.print(data.toString(Arrays.asList(POSTagger.values())));
        writer.println();
    }

}
