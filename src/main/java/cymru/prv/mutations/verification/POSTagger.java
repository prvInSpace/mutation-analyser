package cymru.prv.mutations.verification;

import java.util.function.Function;

public enum POSTagger {
    CorCen (0, CorePosTag::fromCorCenTag),
    Bangor (1, CorePosTag::fromBangorTag),
    Combined (2, CorePosTag::fromCorCenTag);

    private final int id;
    private final Function<String, CorePosTag> getter;

    POSTagger(int id, Function<String, CorePosTag> getter) {
        this.id = id;
        this.getter = getter;
    }

    public int getId(){
        return id;
    }

    public CorePosTag getTag(String s){
        return getter.apply(s);
    }
}
