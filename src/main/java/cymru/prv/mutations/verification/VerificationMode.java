package cymru.prv.mutations.verification;

public enum VerificationMode {
    NONE             (0b000),
    POS_TAGGER       (0b001),
    POS_TAG_MISMATCH (0b010),
    MATCHER          (0b100),
    ALL              (0b111);

    private final int flag;

    VerificationMode(int flag) {
        this.flag = flag;
    }

    public boolean isSet(int mode){
        return (mode & this.flag) != 0;
    }

    public boolean isSet(VerificationMode mode){
        return (mode.flag & flag) != 0;
    }

    public static int of(VerificationMode... modes){
        int flag = 0;
        for(VerificationMode mode : modes)
            flag |= mode.flag;
        return flag;
    }

}
