package cymru.prv.mutations.corcen;

import cymru.prv.common.cli.Colors;
import cymru.prv.common.levenshtein.LevenshteinDistance;
import cymru.prv.mutations.CorCenMutationAnalyser;
import cymru.prv.mutations.verification.CorePosTag;
import cymru.prv.mutations.verification.POSTagger;
import cymru.prv.mutations.verification.POSTaggerVerificator;
import cymru.prv.mutations.verification.VerificationMode;
import cymru.prv.ogma.TextReader;

import java.util.List;
import java.util.Map;
import java.util.Scanner;


/**
 * A file used by Ogma to fetch the next line to search through.
 * <p>
 * In this case we want to read the CorCen corpus file + a retagged
 * version of the same file. This is due to the retagged version
 * being more accurate in some instances.
 * <p>
 * After a line is read some corrections will be done by this class
 * before it gets returned. This is again to improve the accuracy of
 * the POS-tagging so that the searching becomes more accurate.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class CorCenFileReader implements TextReader {

    private final Scanner in;
    private final Scanner retagged;
    private final LevenshteinDistance levenshtein = new LevenshteinDistance();
    private final POSTaggerVerificator verificator;
    private final CorCenFile file;

    CorCenFileReader(Scanner in, Scanner out) {
        this(null, null, in, out);
    }

    /**
     * Creates a new instance of the object.
     * Requires two scanners. One for the CorCen
     * file and one for the re-tagged file.
     * <p>
     * These number of lines of these two files
     * are expected to be equal.
     *
     * @param in       The CorCen file
     * @param retagged The re-tagged files
     */
    public CorCenFileReader(POSTaggerVerificator verificator, CorCenFile file, Scanner in, Scanner retagged) {
        this.verificator = verificator;
        this.file = file;
        this.in = in;
        this.retagged = retagged;
    }


    /**
     * Fetches the line from the CorCen file
     * <p>
     * It then goes through and makes some corrections
     * both using simple replaces, but also
     * comparing the tags to the Bangor POS-tagger
     *
     * @return A string array of tokens
     */
    @Override
    public String[] getNext() {
        if (!in.hasNext())
            return null;

        // The full line of text
        var corcenLine = in.nextLine();
        var bangorLine = retagged.nextLine();

        // Do some replacements
        corcenLine = corcenLine.replace("i'w_Arsym", "i_Arsym 'w_Banmedd3gu");

        // The tokens including their POS tags
        var corcenTokens = corcenLine.split("\\s");
        var bangorTokens = bangorLine.split("\\s");

        // The lines without their POS tags
        var corcenWords = corcenLine.replaceAll("_[A-Za-z0-9]+", "").split("\\s");
        var bangorWords = bangorLine.replaceAll("_[A-Za-z0-9]+", "").split("\\s");

        // List of common tokens
        var commonWords = levenshtein.difference(corcenWords, bangorWords).getCommon();

        // Iterate over and find mis-matches
        int corCenIndex = 0;
        int bangorIndex = 0;
        for (int commonIndex = 0; commonIndex < commonWords.size(); ++commonIndex) {

            var common = commonWords.get(commonIndex);

            // Iterate to find the common tag
            while (!bangorWords[bangorIndex].equals(common))
                bangorIndex++;
            while (!corcenWords[corCenIndex].equals(common))
                corCenIndex++;

            // Fetch the token and the tag for both lists
            var corcenTag = corcenTokens[corCenIndex]
                    .substring(corcenTokens[corCenIndex].lastIndexOf('_') + 1);

            var bangorTag = bangorTokens[bangorIndex]
                    .substring(bangorTokens[bangorIndex].lastIndexOf('_') + 1);

            // Bangor tags auxiliary verbs, but we don't need that information
            if (bangorTag.equals("AUX"))
                bangorTag = "VERB";

            var stdCorCenTag = CorePosTag.fromCorCenTag(corcenTag);
            var stdBangorTag = CorePosTag.fromBangorTag(bangorTag);
            var stdCombinedTag = stdCorCenTag;

            // Search through the tag replacements
            // If one matches, replace the CorCen tag with the correct one
            for (PosTagReplacement repl : tokensToReplace) {
                if (repl.matches(corcenTag, bangorTag)) {
                    corcenTokens[corCenIndex] = corcenTokens[corCenIndex]
                            .replaceFirst("(?<=_)[A-Za-z0-9]+$", repl.getReplacement());
                    stdCombinedTag = CorePosTag.fromCorCenTag(repl.getReplacement());
                    break;
                }
            }

            if (verificator != null
                    && VerificationMode.POS_TAGGER.isSet(CorCenMutationAnalyser.VERIFICATION_MODE)
                    && !corcenTag.equals("Anon")
                    && stdCorCenTag != stdBangorTag)
            {
                var corcenBefore = corcenWords[corCenIndex];
                corcenWords[corCenIndex] = Colors.RED + corcenBefore + Colors.RESET;
                var text = String.join(" ", corcenWords);
                corcenWords[corCenIndex] = corcenBefore;
                verificator.addInstance(text, file.getFormat(), Map.of(
                        POSTagger.CorCen, stdCorCenTag,
                        POSTagger.Bangor, stdBangorTag,
                        POSTagger.Combined, stdCombinedTag));
            }

        }

        return corcenTokens;
    }


    /**
     * A list of all tags to be replaced.
     * If the corcen tag matches the string on the left
     * and the bangor tag matches the tag on the right
     * the corcen tag will be replaced
     */
    private final static List<PosTagReplacement> tokensToReplace = List.of(
            new PosTagReplacement("unk", "NOUN", "Egu"),
            new PosTagReplacement("Gwest", "PROPN", "Ep"),
            new PosTagReplacement("Gwest", "NOUN", "Eu"),
            new PosTagReplacement("Adf", "INTJ", "Ebych"),
            new PosTagReplacement("Rha.*", "PART", "U"),
            new PosTagReplacement("Rha.*", "ADP", "Ar")

            // Rules that would have had no real benefit
            //new PosTagReplacement("E[^p]*", "DET", ""),
            //new PosTagReplacement("B(?!an).*", "DET", "Ban"),
            //new PosTagReplacement("Ar.*", "PART", "U"),
            //new PosTagReplacement("Ar.*", "CONJ", "Cys"),
    );

}
