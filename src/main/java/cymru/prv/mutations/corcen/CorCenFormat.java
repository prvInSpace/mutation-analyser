package cymru.prv.mutations.corcen;

/**
 * @author Preben Vangberg
 */
public enum CorCenFormat {
    LLA,
    YSG,
    ELE;

    public static CorCenFormat from(String filename){
        return Enum.valueOf(CorCenFormat.class, filename.substring(0, 3).toUpperCase());
    }

}
