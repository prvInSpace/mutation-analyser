package cymru.prv.mutations.corcen;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Preben Vangberg
 */
public enum Location {
    NO_PLACE(0, "no place"),
    BLAENAU_GWENT(1, "Blaenau Gwent"),
    BRIDGEND(2, "Bridgend"),
    CAERPHILLY(3, "Caerphilly"),
    CARDIFF(4, "Cardiff"),
    CARMARTHENSHIRE(5, "Carmarthenshire"),
    CEREDIGION(6, "Ceredigion"),
    CONWY(7, "Conwy"),
    DENBIGHSHIRE(8, "Denbighshire"),
    FLINTSHIRE(9, "Flintshire"),
    GWYNEDD(10, "Gwynedd"),
    ANGLESEY(11, "Anglesey"),
    MERTHYR_TYDFIL(12, "Merthyr Tydfil"),
    MONMOUTHSHIRE(13, "Monmouthshire"),
    NEATH_PORT_TALBOT(14, "Neath Port Talbot"),
    NEWPORT(15, "Newport"),
    PEMBROKESHIRE(16, "Pembrokeshire"),
    POWYS(17, "Powys"),
    RHONDDA_CYNON_TAFF(18, "Rhondda Cynon Taff"),
    SWANSEA(19, "Swansea"),
    TORFAEN(20, "Torfaen"),
    VALE_OF_GLAMORGAN(21, "Vale of Glamorgan"),
    WREXHAM(22, "Wrexham"),
    BLANK(23, "blank"),
    ENGLAND(24, "England"),
    NORTHERN_IRELAND(25, "Northern Ireland"),
    SCOTLAND(26, "Scotland"),
    REST_OF_THE_WORD(27, "Rest of the world");

    private static final Map<Integer, Location> locationMap = new HashMap<>();

    private final String name;
    private final int id;

    static {
        for(Location loc : values())
            locationMap.put(loc.id, loc);
    }

    public static Location fromId(int id){
        return locationMap.get(id);
    }

    Location(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
