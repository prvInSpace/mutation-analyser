package cymru.prv.mutations.corcen;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


/**
 * Represents an instance of the CorCenCC corpus
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class CorCenCorpus {

    private final Map<String, Contributor> contributors = new HashMap<>();
    private final Map<String, CorCenFile> files = new HashMap<>();
    private final Map<Integer, Taxonomy> taxonomies = new HashMap<>();

    private final String path;


    /**
     * Creates an instance of the corpus.
     * Tries to read all of the data contained
     * at the path.
     *
     * The corpus itself has to be extracted.
     *
     * @param path The path in which the corpus is located.
     * @throws FileNotFoundException If it is unable to read all the data
     */
    public CorCenCorpus(String path) throws FileNotFoundException {
        this.path = path;

        // Load information from the corpus files
        loadContributors();
        loadFiles();
        loadTaxonomyInformation();
        loadContributionInformation();
        loadCategorisationInformation();
    }


    /**
     * Returns a collection of all the CorCenCiles
     * that it has found and created
     *
     * @return list of all CorCenFile objects
     */
    public Collection<CorCenFile> getFiles(){
        return files.values();
    }


    /**
     * Returns all the taxonomies that was loaded
     * during initialization
     *
     * @return a list of taxonomies
     */
    public Collection<Taxonomy> getTaxonomies() {
        return taxonomies.values();
    }


    /**
     * Loads the content of the contributor.tsv
     * file and uses this to populate the
     * contributors map
     *
     * @throws FileNotFoundException If it is unable to read the file
     */
    private void loadContributors() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(path + "contributor.tsv"));
        scanner.nextLine();
        while (scanner.hasNext()) {
            var contributor = new Contributor(scanner.nextLine());
            contributors.put(contributor.getId(), contributor);
        }
    }


    /**
     * Fetches the name of all the text files
     * in the corpus and uses this to create
     * new objects based on this information.
     *
     * This is so that we can populate these objects
     * with other information later.
     */
    private void loadFiles() {
        var filenames = new File(path + "complete_corpus").list();
        for (String file : filenames)
            files.put(file.replaceFirst("\\.txt$", ""), new CorCenFile(file));
    }


    /**
     * Loads the content of the taxonomy.tsv
     * file and uses this to populate the
     * taxonomies map
     *
     * @throws FileNotFoundException If it is unable to read the file
     */
    private void loadTaxonomyInformation() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(path + "taxonomy.tsv"));
        scanner.nextLine();
        while (scanner.hasNext()) {
            var taxonomy = new Taxonomy(scanner.nextLine());
            taxonomies.put(taxonomy.getId(), taxonomy);
        }
    }


    /**
     * Loads the content of the contrib_links.tsv
     * files and uses this to link the different
     * contributors to the different corpus files
     *
     * @throws FileNotFoundException If it is unable to read the file
     */
    private void loadContributionInformation() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(path + "contrib_links.tsv"));
        scanner.nextLine();
        while (scanner.hasNext()) {
            var fields = scanner.nextLine().split("\t");
            if (files.containsKey(fields[0]))
                files.get(fields[0]).addContributor(contributors.get(fields[1]));
            else {
                System.out.println("Missing file: " + fields[0]);
            }
        }
    }


    /**
     * Loads the content of the categorisation.tsv
     * files and uses this to populate the different
     * files with the different taxonomies that is found
     * in the file.
     *
     * @throws FileNotFoundException If it is unable to read the file
     */
    private void loadCategorisationInformation() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(path + "categorisation.tsv"));
        scanner.nextLine();
        while (scanner.hasNext()) {
            var fields = scanner.nextLine().split("\t");
            files.get(fields[0]).addTaxonomy(taxonomies.get(Integer.parseInt(fields[1])));
        }
    }

}
