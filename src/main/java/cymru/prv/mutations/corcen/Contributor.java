package cymru.prv.mutations.corcen;


/**
 * Represents a contributor to the CorCen corpus
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class Contributor {

    private final String id;

    // 1 : Individual
    // 2 : Company
    private final int type;

    private final int yearOfBirth;

    // 1-5
    // 1 fluent native speaker
    // 5 basic understanding
    private final int fluency;

    // 1 learner
    // 2 not learner
    private final int learner;

    private final Location location;

    /**
     * Creates a new instance of the object
     * using information from the entry
     * in the contributor file in the CorCen
     * corpus.
     *
     * @param entry A line from the contributor file
     */
    public Contributor(String entry){
        String[] fields = entry.split("\t");
        id = fields[0];
        type = Integer.parseInt(fields[1]);
        yearOfBirth = Integer.parseInt(fields[2]);
        location = Location.fromId(Integer.parseInt(fields[4]));
        fluency = Integer.parseInt(fields[8]);
        learner = Integer.parseInt(fields[9]);
    }

    // Getters
    public String getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public int getFluency() {
        return fluency;
    }

    public int getLearner() {
        return learner;
    }

    public Location getLocation() {
        return location;
    }
}
