package cymru.prv.mutations.corcen;

import cymru.prv.mutations.mutations.MutationType;

import java.util.LinkedList;
import java.util.List;


/**
 * Represents a single file in the CorCen corpus
 *
 * Contains information about the contributor and
 * the taxonomies for the file.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class CorCenFile {

    private final String name;
    private Contributor contributor;
    private final List<Taxonomy> taxonomies = new LinkedList<>();

    public CorCenFile(String name){
        this.name = name;
    }

    public void addContributor(Contributor contributor){
        this.contributor = contributor;
    }

    public void addTaxonomy(Taxonomy taxonomy){
        taxonomies.add(taxonomy);
    }

    public String getName() {
        return name;
    }

    public void updateData(MutationType expected, MutationType actual) {
        for(Taxonomy taxonomy : taxonomies)
            taxonomy.updateData(expected, actual);
    }

    public Contributor getContributor() {
        return contributor;
    }

    public CorCenFormat getFormat(){
        return CorCenFormat.from(name);
    }
}
