package cymru.prv.mutations.corcen;


import cymru.prv.mutations.MutationClassificationMap;
import cymru.prv.mutations.mutations.MutationType;

/**
 * Represents a taxonomy in the CorCen corpus
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class Taxonomy {

    private final int id;
    private final String type;
    private final String format;
    private final String tag;

    private final MutationClassificationMap results = new MutationClassificationMap();

    public Taxonomy(String line) {
        var fields = line.split("\t");
        id = Integer.parseInt(fields[0]);
        type = fields[1];
        format = fields[2];
        tag = fields[3];
    }

    public String getType(){
        return type;
    }

    public String getTag(){
        return tag;
    }

    public int getId() {
        return id;
    }

    public String getFormat() {
        return format;
    }

    public void updateData(MutationType expected, MutationType actual){
        results.increment(expected, actual);
    }

    public MutationClassificationMap getData() {
        return results;
    }
}
