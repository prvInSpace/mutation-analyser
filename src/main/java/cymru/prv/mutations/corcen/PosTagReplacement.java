package cymru.prv.mutations.corcen;


/**
 * A class to store information about tag combinations
 * that should be replaced.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class PosTagReplacement {

    private final String corcen;
    private final String bangor;
    private final String replacement;


    /**
     * Creates a new instance of the object
     *
     * @param corcen A regex matching CorCen tags
     * @param bangor A regex matching Bangor tangs
     * @param replacement The tag to replace the CorCen tag
     *                    with in-case of a match
     */
    public PosTagReplacement(String corcen, String bangor, String replacement) {
        this.corcen = corcen;
        this.bangor = bangor;
        this.replacement = replacement;
    }


    /**
     * Checks whether the two provided tags
     * matches the tag regexes in the class
     *
     * @param corcen The CorCen tag to match
     * @param bangor The Bangor tag to match
     * @return true if both tags match, false otherwise
     */
    public boolean matches(String corcen, String bangor){
        return corcen.matches(this.corcen) && bangor.matches(this.bangor);
    }


    /**
     * Returns the tag to replace the CorCen
     * tag with in-case a match is found
     *
     * @return The replacement tag
     */
    public String getReplacement(){
        return replacement;
    }

}
