package cymru.prv.mutations;

import cymru.prv.mutations.corcen.CorCenFile;
import cymru.prv.mutations.data.SingleDataPoint;
import cymru.prv.mutations.lemma.Lemma;
import cymru.prv.mutations.lemma.Lemmatizer;
import cymru.prv.mutations.mutations.MutationType;
import cymru.prv.mutations.mutations.Mutations;
import cymru.prv.mutations.verification.VerificationMode;
import cymru.prv.ogma.Match;
import cymru.prv.ogma.Ogma;
import cymru.prv.ogma.TextReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * A class to store all the information related
 * to Ogma and rule-set that is used to match
 * places where mutations could occur.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class MutationMatcher {


    private static final String OGMA_SRC = "src/main/ogma/";

    private final Lemmatizer lemmatizer;
    private final Ogma ogma = new Ogma();

    public MutationMatcher(Lemmatizer lemmatizer) throws FileNotFoundException {
        this.lemmatizer = lemmatizer;
        ogma.addMatcherFunction("lemma", this::matchesLemma);
        ogma.parseOgmaFile(new FileReader(OGMA_SRC + "corcencc.ogma"));
        ogma.parseOgmaFile(new FileReader(OGMA_SRC + "mutations.ogma"));

    }

    public void findMatches(TextReader reader, Consumer<Match> callback) throws IOException {
        ogma.findMatches(reader, callback);
    }


    /**
     * Lemmatizes the words in question and matches it up
     * against a parameter from Ogma.
     *
     * @param word The word to lemmatize
     * @param params The regex to check the lemma up against
     * @return whether any of the lemmas matches the regex
     */
    private boolean matchesLemma(String word, String[] params) {
        var lemmas = lemmatizer.getLemmas(word.split("_")[0]);
        if(lemmas == null)
            return false;
        for(Lemma lemma : lemmas){
            if(lemma.getWord().matches(params[0]))
                return true;
        }
        return false;
    }



}
