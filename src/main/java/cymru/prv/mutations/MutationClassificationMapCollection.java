package cymru.prv.mutations;

import cymru.prv.mutations.mutations.MutationType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Represents a collection of classification maps.
 * In addition to the normal functions of a ConcurrentHashMap
 * it contains functions to export the data as a tab
 * separated table.
 *
 * @see MutationClassificationMap
 * @see ConcurrentHashMap
 *
 * @author Preben Vangberg
 * @since 1.0.0
 *
 * @param <K> The key of the map
 */
public class MutationClassificationMapCollection<K> extends ConcurrentHashMap<K, MutationClassificationMap> {


    /**
     * Increments the value in the classification map at the given key
     *
     * @param key of the classification map
     * @param expected the expected classification
     * @param actual the actual classification
     */
    public void increment(K key, MutationType expected, MutationType actual){
        putIfAbsent(key, new MutationClassificationMap());
        get(key).increment(expected, actual);
    }


    /**
     * Exports the data in the collection to a file
     * as a list of tab separated tables.
     *
     * @param path The path to export the data to
     * @throws FileNotFoundException if the file could not be opened
     */
    public void export(String path) throws FileNotFoundException {
        File file = new File(path);
        file.getParentFile().mkdirs();

        PrintWriter writer = new PrintWriter(file);
        boolean first = true;
        for (K key : keySet()) {
            if (first)
                first = false;
            else
                writer.println();
            get(key).export(writer, key.toString());
        }
    }

}
