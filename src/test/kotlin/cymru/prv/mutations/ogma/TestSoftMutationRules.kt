package cymru.prv.mutations.ogma

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.*


/**
 * A class to the Ogma rules to ensure
 * that they do what they are supposed to
 * do.
 *
 * All classes are listed with the source
 * of the rules. These are given as numbers
 * as you can see in the list below.
 *
 * References:
 * 1: Gwyn Thomas      : Ymarfer Ysgrifennu Cymraeg
 * 2: Gareth King      : Modern Welsh
 * 3: D. Geraint Lewis : Y Treigladur
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
@TestInstance(Lifecycle.PER_CLASS)
class TestSoftMutationRules: AbstractTestMutations() {

    /**
     * Sources: 1, 2, 3
     */
    @Nested
    inner class TestNounsAfterDefiniteArticle {

        @Test
        fun `feminine singular nouns should mutate after definite article`(){
            var found = false
            matcher.findMatches(wrap("y_YFB gath_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("gath_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `masculine singular nouns should not mutate after definite article`(){
            var found = false
            matcher.findMatches(wrap("y_YFB dyn_Egu")) { match ->
                assertTrue(match.groups.contains("RADICAL"))
                assertFalse(match.groups.contains("SOFT"))
                assertEquals("dyn_Egu", match.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `plural nouns should not mutate after definite article`(){
            var found = false;
            matcher.findMatches(wrap("y_YFB cathau_Ebll")) { match ->
                assertTrue(match.groups.contains("RADICAL"))
                assertFalse(match.groups.contains("SOFT"))
                assertEquals("cathau_Ebll", match.target)
                found = true
            }
            assertTrue(found)
        }

    }


    /**
     * Sources: 1, 2, 3
     *
     * Nouns after dy, 'th, ei, 'i, 'w
     * should mutate. The issue with ei, 'i, 'w
     * is that they only apply to the masculine
     * version of those words which means that
     * they are dependent on context.
     *
     * Note: each pronoun can be both a determiner
     * and a pronoun so we need to check both.
     */
    @Nested
    inner class TestNounsAfterSomePronouns {

        @Test
        fun `nouns after 'dy' should mutate`(){
            var found = false
            matcher.findMatches(wrap("dy_Banmedd2u ben_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("ben_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'th should mutate`(){
            var found = false
            matcher.findMatches(wrap("gyda_Ar 'th_Rhadib2u gath_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("gath_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

    }


    /**
     * Sources: 1, 2, 3
     */
    @Nested
    inner class TestNounsAfterCertainPrepositions {

        // Please note that random nouns have been used and that
        // sentences might not make sense and that the gender
        // might be wrong

        @Test
        fun `nouns after 'am' should mutate`(){
            var found = false
            matcher.findMatches(wrap("am_Ar geiniog_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("geiniog_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'ar' should mutate`() {
            find("ar_Ar lan_Egu", "lan_Egu", "SOFT")
        }

        @Test
        fun `nouns after 'at' should mutate`(){
            var found = false
            matcher.findMatches(wrap("at_Ar deintydd_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("deintydd_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'dan' should mutate`(){
            var found = false
            matcher.findMatches(wrap("dan_Ar ddylanwad_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("ddylanwad_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'dros' should mutate`(){
            var found = false
            matcher.findMatches(wrap("dros_Ar ragrith_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("ragrith_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'drwy' should mutate`(){
            var found = false
            matcher.findMatches(wrap("drwy_Ar dactegau_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("dactegau_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'gan' should mutate`(){
            var found = false
            matcher.findMatches(wrap("Mae_Be ganddyn_Ar ni_Rha gactws_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("gactws_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'heb' should mutate`(){
            var found = false
            matcher.findMatches(wrap("heb_Ar gactws_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("gactws_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'hyd' should mutate`(){
            var found = false
            matcher.findMatches(wrap("hyd_Ar ddiwedd_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("ddiwedd_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'i' should mutate`(){
            var found = false
            matcher.findMatches(wrap("i_Ar gymdeithas_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("gymdeithas_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'o' should mutate`(){
            var found = false
            matcher.findMatches(wrap("o_Ar Gymru_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("Gymru_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

        @Test
        fun `nouns after 'wrth' should mutate`(){
            var found = false
            matcher.findMatches(wrap("wrth_Ar gefnogaeth_Ebu")) {
                assertTrue(it.groups.contains("SOFT"))
                assertEquals("gefnogaeth_Ebu", it.target)
                found = true
            }
            assertTrue(found)
        }

    }


    /**
     * Sources: 1, 2, 3
     *
     * Source two only mentions 1 and 2
     * and does not mention 7 and 8 under soft
     * mutations. Source 2 also exclude
     * words starting with ll or rh from mutating
     * after 1.
     */
    @Nested
    inner class TestNounsAfterCertainNumbers {

        // Un can be both a number, pronoun and a determiner

        @Test
        fun `feminine nouns after 'un' as a number should mutate`() =
            find("un_Rhifold gath_Ebu", "gath_Ebu", "SOFT")

        @Test
        fun `feminine nouns after 'un' as a determiner should mutate`() =
            find("un_Bancynnar gath_Ebu","gath_Ebu", "SOFT")

        @Test
        fun `feminine nouns after 'un' as a pronoun should mutate`() =
            find("un_Rhaamh gath_Ebu","gath_Ebu", "SOFT")

        @Test
        fun `masculine nouns after 'un' pronoun should not mutate`() =
            find("un_Rhaamh dyn_Egu","dyn_Egu", "RADICAL")

        @Test
        fun `nouns after 'dwy' pronoun should mutate`() =
            find("dwy_Rhi ddyn_Egu","ddyn_Egu", "SOFT")

        @Test
        fun `nouns after 'dau' pronoun should mutate`() =
            find("dau_Rhi faneg_Ebu","faneg_Ebu", "SOFT")

        @Test
        fun `nouns after '2' pronoun should mutate`() =
            find("2_Rhi ddyn_Egu","ddyn_Egu", "SOFT")

        @Test
        fun `some words after 'saith' pronoun should mutate`() =
            find("saith_Rhi bunt_Egu","bunt_Egu", "SOFT")

        @Test
        fun `some words after '7' pronoun should mutate`() =
            find("7_Rhi gant_Egu","gant_Egu", "SOFT")

        @Test
        fun `some words after 'wyth' pronoun should mutate`() =
            find("wyth_Rhi bwys_Egu","bwys_Egu", "SOFT")

        @Test
        fun `some words after '8' pronoun should mutate`() =
            find("8_Rhi geiniog_Egu","geiniog_Egu", "SOFT")

    }

    @Nested
    inner class TestNounsAfterMiscTriggerWords {

        /**
         * Source 1, 2, 3
         */
        @Test
        fun `nouns after 'yn' should mutate`() =
            find("yn_Arsym ddyn_Egu", "ddyn_Egu", "SOFT")

        @Test
        fun `nouns starting with ll or rh after 'yn' should not mutate`() =
            find("yn_Arsym rhaglen_Egu", "rhaglen_Egu", "RADICAL")

        @Test
        fun `nouns after conjugated verb should mutate`() =
            find("canodd_Bgorff3u fe_Rha3u gân_Egu", "gân_Egu", "SOFT")

        @Test
        fun `nouns after 'dyma' verb should mutate`() =
            find("Dyma_Be ddarlun_Egu", "ddarlun_Egu", "SOFT")

        @Test
        fun `nouns after 'dyna' should mutate`() =
            find("Dyna_Be ddyn_Egu", "ddyn_Egu", "SOFT")

        @Test
        fun `nouns after 'dacw' should mutate`() =
            find("Dacw_Be fynydd_Egu", "fynydd_Egu", "SOFT")

        @Test
        fun `nouns after 'neu' should mutate`() =
            find("Nos_Ebu neu_Cys ddydd_Ebu", "ddydd_Ebu", "SOFT")

        @Test
        fun `nouns after 'pa' should mutate`() =
            find("Pa_Adf gair_Ebu", "gair_Ebu", "SOFT")

        @Test
        fun `nouns after 'pa fath' should mutate`() {
            val words = HashSet<String>(setOf("fath_Egbu", "ddyn_Egu"))
            matcher.findMatches(wrap("pa_Adf fath_Egbu ddyn_Egu")) {
                assertTrue(it.groups.contains("SOFT"), "Did not contain group")
                assertTrue(words.contains(it.target), "Target is wrong")
                words.remove(it.target)
            }
            assertEquals(0, words.size, "Did not find all targets\n$words")
        }

        @Test
        fun `nouns after 'rhyw' should mutate`() =
            find("rhyw_Egbu gair_Ebu", "gair_Ebu", "SOFT")

        @Test
        fun `nouns after 'unrhyw' should mutate`() =
            find("unrhyw_Egbu gair_Ebu", "gair_Ebu", "SOFT")

        @Test
        fun `nouns after 'cyfryw' should mutate`() =
            find("cyfryw_Egbu gair_Ebu", "gair_Ebu", "SOFT")

        @Test
        fun `nouns after 'amryw' should mutate`() =
            find("amryw_Egbu gair_Ebu", "gair_Ebu", "SOFT")

        @Test
        fun `nouns after 'sut' should mutate`() =
            find("Sut_Adf gar_Ebu", "gar_Ebu", "SOFT")

        @Test
        fun `nouns after 'naill' should mutate`() =
            find("naill_Banmeint goes_Ebu", "goes_Ebu", "SOFT")

        @Test
        fun `nouns after 'ychydig' as pronoun should mutate`() =
            find("ychydig_Rhaamh ddyddiau_Egll", "ddyddiau_Egll", "SOFT")

        @Test
        fun `nouns after 'ychydig' as determiner should mutate`() =
            find("Ychydig_Banmeint ddyddiau_Egll", "ddyddiau_Egll", "SOFT")

        @Test
        fun `nouns after 'ambell' should mutate`() =
            find("ambell_Banmeint ddyddiau_Egll", "ddyddiau_Egll", "SOFT")

        @Test
        fun `nouns after 'aml' should mutate`() =
            find("aml_Banmeint ddyddiau_Egll", "ddyddiau_Egll", "SOFT")

    }

    @Nested
    inner class TestAdjectivesAfterSingFemNouns {

        @Test
        fun `adjectives should mutate after feminine singular nouns`() =
            find("cath_Ebu ddu_Ans", "ddu_Ans","SOFT")

        // Needs a word in front to work
        @Test
        fun `adjectives should not mutate after masculine singular nouns`() =
            find("rhai_Ban dyn_Egu da_Ans", "da_Ans","RADICAL")

        // Needs a word in front to work
        @Test
        fun `adjectives should not mutate after plural nouns`() =
            find("rhai_Ban cathau_Ebll du_Ans", "du_Ans","RADICAL")

    }

    @Nested
    inner class TestAdjectivesAfterMiscTriggerWords {

        @Test
        fun `adjectives after yn should mutate`() =
            find("yn_Arsym gyflym_Ans", "gyflym_Ans", "SOFT")

        @Test
        fun `adjectives after neu should mutate`() =
            find("neu_Arsym ddrwg_Ans", "ddrwg_Ans", "SOFT")

        @Test
        fun `adjectives after 'mor' should mutate`() =
            find("mor_Adf felys_Ans", "felys_Ans", "SOFT")

        /**
         * Source 1, 3
         */
        @Test
        fun `adjectives starting with ll or rh after 'mor' should not mutate`() =
            find("mor_Adf rhwydd_Ans", "rhwydd_Ans", "RADICAL")

        @Test
        fun `adjectives after 'cyn' should mutate`() =
            find("cyn_Adf laned_Ans", "laned_Ans", "SOFT")

        @Test
        fun `adjectives starting with ll or rh after 'cyn' should not mutate`() =
            find("cyn_Adf llonydd_Ans", "llonydd_Ans", "RADICAL")

        @Test
        fun `adjectives after 'po' should mutate`() =
            find("po_Adf lanaf_Ans", "lanaf_Ans", "SOFT")

        @Test
        fun `adjectives after 'rhy' should mutate`() =
            find("rhy_Adf dda_Ans", "dda_Ans", "SOFT")

        @Test
        fun `adjectives after 'gweddol' should mutate`() =
            find("gweddol_Adf fuan_Ans", "fuan_Ans", "SOFT")

    }

    @Nested
    inner class TestVerbsAfterMiscTriggerWords {

        @Test
        fun `verbs after the particle 'a' should mutate`() =
            find("a_Rhaperth glywais_Bpres1u", "glywais_Bpres1u", "SOFT")

        @Test
        fun `verbs after the particle 'Fe' should mutate`() =
            find("Fe_Uberf frathodd_Bpres3u", "frathodd_Bpres3u", "SOFT")

        @Test
        fun `verbs after the particle 'Mi' should mutate`() =
            find("Mi_Uberf frathodd_Bpres3u", "frathodd_Bpres3u", "SOFT")

        @Test
        fun `verbs after the particle 'pan' should mutate`() =
            find("pan_Cyscyd ddaw_Bpres3u", "ddaw_Bpres3u", "SOFT")

        @Test
        fun `verbs starting with g after the particle 'ni' should mutate`() =
            find("ni_Uberf welais_Bpres1u", "welais_Bpres1u", "SOFT")

        @Test
        fun `bod should mutate after efallai`() =
            find("Efallai_Adf fod_Be", "fod_Be", "SOFT")

    }

    @Nested
    inner class TestNegatedVerbs {

        @Test
        fun `negated verbs should mutate`() =
            find("Welais_Bams1u i_Rha1u ddim_Adf byd_Egu", "Welais_Bams1u", "SOFT")

        @Test
        fun `negated verbs should mutate if negated by mo`() =
            find("Welaist_Bams2u ti_Rha2u mo_Adf 'r_YFB", "Welaist_Bams2u", "SOFT")

    }
}
