package cymru.prv.mutations.ogma

import cymru.prv.mutations.MutationMatcher
import cymru.prv.mutations.lemma.Lemmatizer
import cymru.prv.ogma.TextLineReader
import org.junit.jupiter.api.Assertions
import java.io.BufferedReader
import java.io.StringReader

/**
 * @author Preben Vangberg
 * @since 1.0.0
 */
open class AbstractTestMutations {

    val matcher = MutationMatcher(Lemmatizer())

    /**
     * Checks the sentence to ensure that it is
     * matched, that the target is correct,
     * and that it has been matched to the right group.
     */
    fun find(sent: String, target: String, group: String){
        val found = HashSet<String>()
        matcher.findMatches(wrap(sent)) {
            Assertions.assertTrue(it.groups.contains(group), "Did not contain group '$group'\n${it.searchPatternName}\n$found\n")
            Assertions.assertEquals(target, it.target, "Target is wrong")
            if (!found.contains(it.searchPatternName))
                found.add(it.searchPatternName)
        }
        Assertions.assertEquals(1, found.size, "Match not found or found more than once\n$found")
    }


    /**
     * Ensures that all of the words are found with the correct
     * group. Useful if multiple words might be find in the same
     * sentence by different rules.
     */
    fun findAll(sent: String, target: MutableMap<String, String>) {
        matcher.findMatches(wrap(sent)) {
            Assertions.assertTrue(target.containsKey(it.target), "Missing target word")
            Assertions.assertTrue(it.groups.contains(target[it.target]), "Did not contain the expected group")
            target.remove(it.target)
        }
        Assertions.assertEquals(0, target.size, "Did not find all elements")

    }


    /**
     * Wraps a string in a TextLineReader
     * so that it can be sent to Ogma to
     * be analysed
     */
    fun wrap(s: String): TextLineReader {
        return TextLineReader(BufferedReader(StringReader(s)))
    }
}

