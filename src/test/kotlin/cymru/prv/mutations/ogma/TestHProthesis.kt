package cymru.prv.mutations.ogma

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

/**
 * A couple of tests to ensure that the few
 * h-prothesis rules that we have is working.
 *
 * Source 1, 2, and 3 all agree that the pronouns
 * / determiners ei (feminine), eu, and ein cause
 * h-prothesis in the following word.
 *
 * As with aspirate and soft mutation we do
 * not consider ei due to the difficulty of
 * determining whether the pronoun is masculine
 * or feminine.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestHProthesis: AbstractTestMutations() {

    @Test
    fun `words after 'eu' shoud mutate`() =
        find("eu_Rha hafalau_Egll", "hafalau_Egll", "H_PROTHESIS")

    @Test
    fun `words after 'ein' shoud mutate`() =
        find("ein_Rha hundod_Ebu", "hundod_Ebu", "H_PROTHESIS")

}