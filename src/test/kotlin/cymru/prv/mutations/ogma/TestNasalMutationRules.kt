package cymru.prv.mutations.ogma

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

/**
 * A class that contains tests rules for
 * nasal mutation.
 *
 * This mutation is more difficult to detect
 * and analyse because CorCenCC does not distinguish
 * between the particle 'yn' and the adposition form
 * of the same word.
 *
 * This limits the amount of rules we could realistically
 * search for to words following 'fy' and to special cases
 * related to numbers and some specific words.
 *
 * All classes are listed with the source
 * of the rules. These are given as numbers
 * as you can see in the list below.
 *
 * References:
 * 1: Gwyn Thomas      : Ymarfer Ysgrifennu Cymraeg
 * 2: Gareth King      : Modern Welsh
 * 3: D. Geraint Lewis : Y Treigladur
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestNasalMutationRules: AbstractTestMutations() {

    /**
     * Source 1, 2, 3
     */
    @Nested
    inner class TestNounsAfterSingularFirstPronoun {

        @Test
        fun `nouns after fy should mutate`() =
            find("fy_Banmedd1u nghath_Egu", "nghath_Egu", "NASAL")

        @Test
        fun `verbs after fy should mutate`() =
            find("fy_Banmedd1u mod_Be yn_Arsym", "mod_Be", "NASAL")

        @Test
        fun `verbs after fy should mutate even if it classified as a pronoun`() =
            find("fy_Rhadib1u mod_Be yn_Arsym", "mod_Be", "NASAL")

    }

    /**
     * The words in question are diwrnod, blwydd, blynedd
     *
     * These words receive a nasal mutation after certain
     * number.
     *
     * All sources have these in common:
     * pum (5), saith (7), wyth(8), naw(9), deng(10), deuddeng(12),
     * pymtheng(15), deunaw(18), ugain(20), and can(100).
     *
     * Source 2 and 3 also lists: chwe(6)
     * Source 2 also lists: hanner can(50)
     * Source 3 also lists: deugain(40), trigain(60), and un(1) in compound numbers
     */
    @Nested
    inner class TestSpecificWordsFollowingCertainNumbers {

        // Since there are so many different versions
        // we are only testing a sample of the overall amount

        @Test
        fun `diwrnod should mutate after pum`() =
            find("pum_Rhifol niwrnod_Egu", "niwrnod_Egu", "NASAL")

        @Test
        fun `diwrnod should mutate after saith`() =
            find("saith_Rhifol niwrnod_Egu", "niwrnod_Egu", "NASAL")

        @Test
        fun `blynedd should mutate after wyth`() =
            find("wyth_Rhifol mlynedd_Egu", "mlynedd_Egu", "NASAL")

        @Test
        fun `blwydd should mutate after naw`() =
            find("naw_Rhifol mlwydd_Egu", "mlwydd_Egu", "NASAL")

        @Test
        fun `blwydd should mutate after deng`() =
            find("deng_Rhifol mlwydd_Egu", "mlwydd_Egu", "NASAL")

        @Test
        fun `blwydd should mutate after deng as a number`() =
            find("10_Gwa mlwydd_Egu", "mlwydd_Egu", "NASAL")

    }

}