package cymru.prv.mutations.ogma

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

/**
 * A series of tests to ensure that the
 * aspirate rules are correct.
 *
 * References:
 * 1: Gwyn Thomas      : Ymarfer Ysgrifennu Cymraeg
 * 2: Gareth King      : Modern Welsh
 * 3: D. Geraint Lewis : Y Treigladur
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestAspirateMutations: AbstractTestMutations() {

    @Nested
    inner class TestAfterCertainNumbers {

        @Test
        fun `test nouns after tri should receive mutation`() =
            find("tri_Rhifol chi_Egu", "chi_Egu", "ASPIRATE")

        @Test
        fun `test adjectives after tri should not receive mutation`() =
            find("tri_Rhifol cyflym_Ans", "cyflym_Ans", "RADICAL")

        @Test
        fun `test adjectives after a noun after tri should receive mutation`() =
            findAll("tri_Rhifol chadarn_Egu ŵr_Ans", mutableMapOf(
                Pair("chadarn_Egu", "ASPIRATE"), Pair("ŵr_Ans", "ASPIRATE")))

        @Test
        fun `test nouns after chwe should receive mutation`() =
            find("chwe_Rhifol chi_Egu", "chi_Egu", "ASPIRATE")

        @Test
        fun `test adjectives after chwe should not receive mutation`() =
            find("chwe_Rhifol cyflym_Ans", "cyflym_Ans", "RADICAL")

        @Test
        fun `test adjectives after a noun after chwe should receive mutation`() =
            findAll("chwe_Rhifol chadarn_Egu ŵr_Ans", mutableMapOf(
                Pair("chadarn_Egu", "ASPIRATE"), Pair("ŵr_Ans", "ASPIRATE")))
    }

    @Nested
    inner class TestAfterCertainPrepositions {

        @Test
        fun `words after 'a' should mutate`() =
            find("a_Arsym phensel_Egu", "phensel_Egu", "ASPIRATE")

        @Test
        fun `words after 'gyda' should mutate`() =
            find("gyda_Arsym thri_Rhifol", "thri_Rhifol", "ASPIRATE")

        @Test
        fun `words after 'tua' should mutate`() =
            find("tua_Arsym phen_Egu", "phen_Egu", "ASPIRATE")

        @Test
        fun `words after 'efo' should mutate`() =
            find("efo_Arsym phensel_Egu", "phensel_Egu", "ASPIRATE")

        @Test
        fun `words after 'hefo' should mutate`() =
            find("efo_Arsym phensel_Egu", "phensel_Egu", "ASPIRATE")

    }

    @Nested
    inner class TestAfterCertainConjunctions {

        @Test
        fun `words after 'a' should mutate`() =
            find("a_Cyscyd chath_Egu", "chath_Egu", "ASPIRATE")

        @Test
        fun `words after 'na' should mutate`() =
            find("na_Cyscyd thŷ_Egu", "thŷ_Egu", "ASPIRATE")

        @Test
        fun `words after 'oni' should mutate`() =
            find("oni_Cyscyd chlwych_Egu", "chlwych_Egu", "ASPIRATE")


    }

    @Nested
    inner class TestAfterCertainNegationWords {

        @Test
        fun `words after 'ni' should mutate`() =
            find("Ni_Uneg chana_Bpres1u", "chana_Bpres1u", "ASPIRATE")

        @Test
        fun `words after 'na' should mutate`() =
            find("na_Uneg thalai_Bcon3u", "thalai_Bcon3u", "ASPIRATE")

    }

    @Nested
    inner class TestAfterCertainAdverbs {

        @Test
        fun `words after 'tra' should mutate`() =
            find("tra_Adf thwp_Ans", "thwp_Ans", "ASPIRATE")

    }
}