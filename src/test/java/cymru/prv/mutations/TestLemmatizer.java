package cymru.prv.mutations;

import cymru.prv.mutations.lemma.Lemmatizer;
import cymru.prv.ogma.Ogma;
import cymru.prv.ogma.TextLineReader;
import cymru.prv.ogma.TextReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestLemmatizer {

    @Test
    public void testLemmatizerAethShouldReturnMynd() throws IOException {
        Lemmatizer lemmatizer = new Lemmatizer();
        var list = lemmatizer.getLemmas("aeth");
        assertEquals(2, list.size());

        // Conjugation of mynd (to go) and aeth (pain)
        assertEquals(list.get(1).getWord(), "mynd");
        assertEquals(list.get(0).getWord(), "aeth");
    }
}
