package cymru.prv.mutations.verification;

import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Tests for the POS tag converts in CorePosTag
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class TestCorePosTag {

    /**
     * These tags are taken from
     * https://github.com/techiaith/lecsicon-cymraeg-bangor
     * and are assumed to be the tags that the Bangor POS tagger
     * uses as well.
     */
    @Test
    public void testBangorTags() {
        var map = Stream.of(new Object[][]{
                {"ADJ", CorePosTag.ADJ},
                {"ADP", CorePosTag.ADP},
                {"ADV", CorePosTag.ADV},
                {"AUX", CorePosTag.VERB},
                {"CONJ", CorePosTag.CONJ},
                {"DET", CorePosTag.DET},
                {"INTJ", CorePosTag.INTJ},
                {"NOUN", CorePosTag.NOUN},
                {"NUM", CorePosTag.NUM},
                {"PART", CorePosTag.PART},
                {"PRON", CorePosTag.PRON},
                {"PROPN", CorePosTag.PROPN},
                {"PUNCT", CorePosTag.PUNCT},
                {"SYM", CorePosTag.SYM},
                {"VERB", CorePosTag.VERB}
        }).collect(Collectors.toMap(data -> (String) data[0], data -> (CorePosTag) data[1]));

        for(String tag : map.keySet())
            assertEquals(map.get(tag), CorePosTag.fromBangorTag(tag));

    }

    @Test
    public void testConvertCCAdjectives(){
        assertEquals(CorePosTag.ADJ, CorePosTag.fromCorCenTag("Anscadbu"));
    }

    @Test
    public void testConvertCCAdverb(){
        assertEquals(CorePosTag.ADV, CorePosTag.fromCorCenTag("Adf"));
    }

    @Test
    public void testConvertCCArticles(){
        // The only article in CorCen is the definite article
        // which is a determiner
        assertEquals(CorePosTag.DET, CorePosTag.fromCorCenTag("YFB"));
    }

    @Test
    public void testConvertCCConjunctions(){
        assertEquals(CorePosTag.CONJ, CorePosTag.fromCorCenTag("Cyscyd"));
    }

    @Test
    public void testConvertCCNumber(){
        assertEquals(CorePosTag.NUM, CorePosTag.fromCorCenTag("Rhitref"));
        assertEquals(CorePosTag.NUM, CorePosTag.fromCorCenTag("Gwdig"));
        assertEquals(CorePosTag.NUM, CorePosTag.fromCorCenTag("Gwrhuf"));
    }

    @Test
    public void testConvertCCInterjections() {
        assertEquals(CorePosTag.INTJ, CorePosTag.fromCorCenTag("Ebych"));
    }

    @Test
    public void testConvertCCNouns() {
        assertEquals(CorePosTag.NOUN, CorePosTag.fromCorCenTag("Egbll"));
    }

    @Test
    public void testConvertCCParticles() {
        assertEquals(CorePosTag.PART, CorePosTag.fromCorCenTag("Uberf"));
    }

    @Test
    public void testConvertCCPrepositions() {
        // Prepositions are a type of adpositions
        assertEquals(CorePosTag.ADP, CorePosTag.fromCorCenTag("Ar2u"));
    }

    @Test
    public void testConvertCCPronouns() {
        assertEquals(CorePosTag.PRON, CorePosTag.fromCorCenTag("Rhamedd3gu"));
    }

    @Test
    public void testConvertCCProperNouns() {
        assertEquals(CorePosTag.PROPN, CorePosTag.fromCorCenTag("Epg"));
    }

    @Test
    public void testConvertCCPunctuation() {
        assertEquals(CorePosTag.PUNCT, CorePosTag.fromCorCenTag("Atdchw"));
    }

    @Test
    public void testConvertCCSymbols() {
        assertEquals(CorePosTag.SYM, CorePosTag.fromCorCenTag("Gwsym"));
        assertEquals(CorePosTag.SYM, CorePosTag.fromCorCenTag("Gwfform"));
    }

    @Test
    public void testConvertCCVerbs() {
        assertEquals(CorePosTag.VERB, CorePosTag.fromCorCenTag("Bamod1u"));
    }

    @Test
    public void testConvertCCVerbNoun() {
        assertEquals(CorePosTag.VERB, CorePosTag.fromCorCenTag("Be"));
    }

}
