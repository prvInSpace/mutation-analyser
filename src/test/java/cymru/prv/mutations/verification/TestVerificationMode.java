package cymru.prv.mutations.verification;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestVerificationMode {

    @Test
    public void testAllFlagShouldMatchEverythingElse(){
        var all = VerificationMode.ALL;
        assertTrue(all.isSet(VerificationMode.POS_TAGGER));
        assertTrue(all.isSet(VerificationMode.MATCHER));
        assertFalse(all.isSet(VerificationMode.NONE));
    }

    @Test
    public void testSingleFlag(){
        var mode = VerificationMode.POS_TAGGER;
        assertTrue(mode.isSet(VerificationMode.POS_TAGGER));
        assertFalse(mode.isSet(VerificationMode.MATCHER));
        assertFalse(mode.isSet(VerificationMode.NONE));
        assertTrue(mode.isSet(VerificationMode.ALL));
    }

    @Test
    public void testCombineFlags(){
        var mode = VerificationMode.of(VerificationMode.POS_TAGGER, VerificationMode.MATCHER);
        assertTrue(VerificationMode.POS_TAGGER.isSet(mode));
        assertTrue(VerificationMode.MATCHER.isSet(mode));
        assertTrue(VerificationMode.ALL.isSet(mode));
        assertFalse(VerificationMode.NONE.isSet(mode));
    }

}
