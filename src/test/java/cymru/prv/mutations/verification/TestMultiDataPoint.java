package cymru.prv.mutations.verification;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Preben Vangberg
 */
public class TestMultiDataPoint {

    @Test
    public void testEqualsMethod(){

        var map1 = Map.of(
            POSTagger.CorCen, CorePosTag.CONJ,
            POSTagger.Bangor, CorePosTag.ADP
        );

        var map2 = Map.of(
                POSTagger.CorCen, CorePosTag.CONJ,
                POSTagger.Bangor, CorePosTag.ADP
        );

        assertEquals(map1, map2);

        var parentMap = Map.of(map1, "");
        assertTrue(parentMap.containsKey(map2));
    }


}
