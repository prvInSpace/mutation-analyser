package cymru.prv.mutations.verification;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TestPOSTaggerResult {

    @Test
    public void testObjectsShouldBeEqualIfTagsAreEqual(){

        var test1 = new POSTaggerResult(Map.of(
                POSTagger.Bangor, CorePosTag.ADJ,
                POSTagger.CorCen, CorePosTag.ADP
        ));

        var test2 = new POSTaggerResult(Map.of(
                POSTagger.Bangor, CorePosTag.ADJ,
                POSTagger.CorCen, CorePosTag.ADP
        ));

        assertEquals(test1, test2);
    }

    @Test
    public void testTestIfSetWorks(){
        Set<POSTaggerResult> results = new HashSet<>();
        results.add(new POSTaggerResult(Map.of(
                POSTagger.Bangor, CorePosTag.VERB,
                POSTagger.CorCen, CorePosTag.VERB
        )));
        results.add(new POSTaggerResult(Map.of(
                POSTagger.Bangor, CorePosTag.PROPN,
                POSTagger.CorCen, CorePosTag.NOUN
        )));
        results.add(new POSTaggerResult(Map.of(
                POSTagger.Bangor, CorePosTag.ADJ,
                POSTagger.CorCen, CorePosTag.ADP
        )));

        assertTrue(results.contains(new POSTaggerResult(Map.of(
                POSTagger.Bangor, CorePosTag.ADJ,
                POSTagger.CorCen, CorePosTag.ADP
        ))));

        assertFalse(results.contains(new POSTaggerResult(Map.of(
                POSTagger.Bangor, CorePosTag.ADJ,
                POSTagger.CorCen, CorePosTag.VERB
        ))));
    }


    @Test
    public void testIfMapsIsEqual(){

        var map = Map.of(
                POSTagger.Bangor, CorePosTag.VERB,
                POSTagger.CorCen, CorePosTag.VERB
        );

        var map2 = Map.of(
                POSTagger.Bangor, CorePosTag.VERB,
                POSTagger.CorCen, CorePosTag.VERB
        );

        var parentMap = Map.of(
                map, "hello"
        );

        assertTrue(parentMap.containsKey(map));
        assertTrue(parentMap.containsKey(map2));

        // Make sure that if the values differ they do not differ
        assertFalse(parentMap.containsKey(Map.of(
                POSTagger.Bangor, CorePosTag.VERB,
                POSTagger.CorCen, CorePosTag.NOUN
        )));

        // Make sure that it does not only check the values
        assertFalse(parentMap.containsKey(Map.of(
                POSTagger.Combined, CorePosTag.VERB,
                POSTagger.CorCen, CorePosTag.VERB
        )));
    }

}
