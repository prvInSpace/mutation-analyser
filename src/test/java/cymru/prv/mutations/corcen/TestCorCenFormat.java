package cymru.prv.mutations.corcen;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Preben Vangberg
 */
public class TestCorCenFormat {

    @Test
    public void testWrittenFormat(){
        Assertions.assertEquals(
                CorCenFormat.YSG,
                CorCenFormat.from("ysg_mr_170907_001.txt")
        );
    }

}
