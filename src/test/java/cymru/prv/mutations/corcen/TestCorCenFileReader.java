package cymru.prv.mutations.corcen;

import org.junit.jupiter.api.Test;

import java.io.StringReader;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

public class TestCorCenFileReader {

    private Scanner getScanner(List<String> lines){
        return new Scanner(
                new StringReader(String.join("\n", lines))
        );
    }

    @Test
    public void testGetScanner(){
        var scanner = getScanner(List.of("hello", "world"));
        assertEquals("hello", scanner.nextLine());
        assertEquals("world", scanner.nextLine());
        assertFalse(scanner.hasNext());
    }

    @Test
    public void testReplacement(){
        var file = new CorCenFileReader(
                getScanner(List.of("Mae_Be Preben_Gwest yn_YFB mynd_Be")),
                getScanner(List.of("Mae_AUX Preben_PROPN yn_PART mynd_VERB"))
        );
        var tokens = file.getNext();
        assertArrayEquals(
                new String[]{"Mae_Be", "Preben_Ep", "yn_YFB", "mynd_Be"},
                tokens
        );
    }

    @Test
    public void testIWIsReplaced(){
        var file = new CorCenFileReader(
                getScanner(List.of("mynd_Be i'w_Arsym dŷ_Egu")),
                getScanner(List.of("mynd_VERB i_ADP 'w_DET dŷ_NOUN"))
        );
        var tokens = file.getNext();
        assertArrayEquals(
                new String[]{"mynd_Be", "i_Arsym", "'w_Banmedd3gu", "dŷ_Egu"},
                tokens
        );
    }

}
