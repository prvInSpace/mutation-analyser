package cymru.prv.mutations;

import cymru.prv.mutations.mutations.MutationType;
import cymru.prv.mutations.mutations.Mutations;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class TestMutations {

    private void assertRegexIsCorrect(String original, List<String> mutations){
        Pattern pattern = Pattern.compile(Mutations.toRegexWithMutations(original));
        for (String word : mutations) {
            Assertions.assertTrue(pattern.matcher(word).matches());
        }
    }

    @Test
    public void testUnmutatableStringShouldNotChange(){
        Assertions.assertEquals(
                "afon",
                Mutations.toRegexWithMutations("afon")
        );
    }

    @Test
    public void testWordStartingWithPShouldMutate(){
        assertRegexIsCorrect("pen", List.of(
                "pen", "ben", "mhen", "phen"
        ));
    }

    @Test
    public void testWordStartingWithTShouldMutate(){
        assertRegexIsCorrect("trwyn", List.of(
                "trwyn", "drwyn", "nhrwyn", "thrwyn"
        ));
    }

    @Test
    public void testWordStartingWithCShouldMutate(){
        assertRegexIsCorrect("ceg", List.of(
                "ceg", "geg", "ngheg", "cheg"
        ));
    }

    @Test
    public void testWordStartingWithBShouldMutate(){
        assertRegexIsCorrect("bol", List.of(
                "bol", "fol", "mol"
        ));
    }

    @Test
    public void testWordStartingWithDShouldMutate(){
        assertRegexIsCorrect("dant", List.of(
                "dant", "ddant", "nant"
        ));
    }

    @Test
    public void testWordStartingWithGShouldMutate(){
        assertRegexIsCorrect("gardd", List.of(
                "gardd", "ardd", "ngardd"
        ));
    }

    @Test
    public void testWordStartingWithLLShouldMutate(){
        assertRegexIsCorrect("llaw", List.of(
                "llaw", "law"
        ));
    }

    @Test
    public void testWordStartingWithRHShouldMutate(){
        assertRegexIsCorrect("rhaw", List.of(
                "rhaw", "raw"
        ));
    }

    @Test
    public void testWordStartingWithMShouldMutate(){
        assertRegexIsCorrect("maneg", List.of(
                "maneg", "faneg"
        ));
    }

    @Test
    public void testChwaraeShouldNotMutate(){
        Assertions.assertEquals(
                "chwarae",
                Mutations.toRegexWithMutations("chwarae"));
    }

    @Test
    public void testTheatrShouldNotMutate(){
        Assertions.assertEquals(
                "theatr",
                Mutations.toRegexWithMutations("theatr"));
    }

    @Test
    public void testPhariseaiddShouldNotMutate(){
        Assertions.assertEquals(
                "phariseaidd",
                Mutations.toRegexWithMutations("phariseaidd"));
    }


    /*
            TESTS FOR GET POSSIBLE MUTATIONS
     */

    @Test
    public void testPossibleMutationsForWordStartingWithVowelShouldReturnSoftMutationForG(){
        String vowels = "aeiouwyâêîôûŵŷ";
        for(char c : vowels.toCharArray()){
            Map<String, MutationType> map = Mutations.getPossibleMutations(""+ c);
            Assertions.assertEquals(1, map.size());
            Assertions.assertEquals(MutationType.SOFT, map.get("g" + c));
        }
    }

    @Test
    public void testPossibleMutationsForWordStartingWithB(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("bol");
        Assertions.assertEquals(2, map.size());
        Assertions.assertEquals(MutationType.RADICAL, map.get("bol"));
        Assertions.assertEquals(MutationType.SOFT, map.get("pol"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithC(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("ceg");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.RADICAL, map.get("ceg"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithCh(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("cheg");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.ASPIRATE, map.get("ceg"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithD(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("dydd");
        Assertions.assertEquals(2, map.size());
        Assertions.assertEquals(MutationType.RADICAL, map.get("dydd"));
        Assertions.assertEquals(MutationType.SOFT, map.get("tydd"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithDd(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("ddant");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.SOFT, map.get("dant"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithF(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("fol");
        Assertions.assertEquals(2, map.size());
        Assertions.assertEquals(MutationType.SOFT, map.get("bol"));
        Assertions.assertEquals(MutationType.SOFT, map.get("mol"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithG(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("geg");
        Assertions.assertEquals(2, map.size());
        Assertions.assertEquals(MutationType.RADICAL, map.get("geg"));
        Assertions.assertEquals(MutationType.SOFT, map.get("ceg"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithL(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("law");
        Assertions.assertEquals(2, map.size());
        Assertions.assertEquals(MutationType.SOFT, map.get("glaw"));
        Assertions.assertEquals(MutationType.SOFT, map.get("llaw"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithLl(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("llaw");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.RADICAL, map.get("llaw"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithM(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("mol");
        Assertions.assertEquals(2, map.size());
        Assertions.assertEquals(MutationType.NASAL, map.get("bol"));
        Assertions.assertEquals(MutationType.RADICAL, map.get("mol"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithMh(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("mhen");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.NASAL, map.get("pen"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithN(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("nant");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.NASAL, map.get("dant"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithNg(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("ngardd");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.NASAL, map.get("gardd"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithNgh(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("ngheg");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.NASAL, map.get("ceg"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithNh(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("nhrwyn");
        Assertions.assertEquals(1, map.size());
        Assertions.assertTrue(map.containsKey("trwyn"));
        Assertions.assertEquals(MutationType.NASAL, map.get("trwyn"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithP(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("pen");
        Assertions.assertEquals(1, map.size());
        Assertions.assertTrue(map.containsKey("pen"));
        Assertions.assertEquals(MutationType.RADICAL, map.get("pen"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithPh(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("phen");
        Assertions.assertEquals(1, map.size());
        Assertions.assertTrue(map.containsKey("pen"));
        Assertions.assertEquals(MutationType.ASPIRATE, map.get("pen"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithR(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("raw");
        Assertions.assertEquals(2, map.size());
        Assertions.assertEquals(MutationType.SOFT, map.get("graw"));
        Assertions.assertEquals(MutationType.SOFT, map.get("rhaw"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithRh(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("rhaw");
        Assertions.assertEquals(1, map.size());
        Assertions.assertEquals(MutationType.RADICAL, map.get("rhaw"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithT(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("trwyn");
        Assertions.assertEquals(1, map.size());
        Assertions.assertTrue(map.containsKey("trwyn"));
        Assertions.assertEquals(MutationType.RADICAL, map.get("trwyn"));
    }

    @Test
    public void testPossibleMutationsForWordStartingWithTh(){
        Map<String, MutationType> map = Mutations.getPossibleMutations("thrwyn");
        Assertions.assertEquals(1, map.size());
        Assertions.assertTrue(map.containsKey("trwyn"));
        Assertions.assertEquals(MutationType.ASPIRATE, map.get("trwyn"));
    }

}
