<_SYM img_VERB /_PUNCT >_SYM
Eisteddfod_NOUN Genedlaethol_ADJ Caerdydd_PROPN 2018_NUM
Hoffech_VERB chi_PRON chwarae_VERB rhan_NOUN allweddol_ADJ yn_ADP seremonïau_NOUN Eisteddfod_NOUN Genedlaethol_ADJ Caerdydd_PROPN ?_PUNCT
Mae_VERB gan_ADP Orsedd_PROPN y_DET Beirdd_NOUN amryw_DET o_ADP seremonïau_NOUN lliwgar_ADJ yn_ADP yr_DET Eisteddfod_PROPN ei_DET hun_NOUN ac_CONJ yn_ADP y_DET Cyhoeddi_PROPN ,_PUNCT a_CONJ gynhelir_VERB yng_ADP Nghaerdydd_PROPN ,_PUNCT ddydd_NOUN Sadwrn_PROPN 24_NUM Mehefin_PROPN ._PUNCT
Rydym_AUX yn_PART gwahodd_VERB ceisiadau_NOUN gan_ADP unigolion_NOUN i_PART fod_VERB yn_PART rhan_NOUN o_ADP 'r_DET seremonïau_NOUN ._PUNCT
Rydym_AUX yn_PART chwilio_VERB am_ADP y_DET canlynol_NOUN :_PUNCT
DAWNS_VERB FLODAU_PROPN
LLAWFORYNION_PROPN
-_PUNCT Angen_NOUN dwy_NUM ferch_NOUN sydd_VERB ar_ADP hyn_PRON o_ADP bryd_NOUN ym_ADP mlynyddoedd_NOUN 4_NUM ,_PUNCT 5_NUM neu_CONJ 6_NUM yn_ADP yr_DET ysgol_NOUN ._PUNCT
MACWYAID_PROPN
-_PUNCT Angen_NOUN dau_NUM fachgen_NOUN sydd_VERB ar_ADP hyn_PRON o_ADP bryd_NOUN ym_ADP mlynyddoedd_NOUN 4_NUM ,_PUNCT 5_NUM neu_CONJ 6_NUM yn_ADP yr_DET ysgol_NOUN ._PUNCT
CYFLWYNYDD_VERB Y_DET FLODEUGED_PROPN
-_PUNCT Merch_PROPN ifanc_ADJ ,_PUNCT tua_ADV 17_NUM -_PUNCT 18_NUM oed_NOUN ._PUNCT
CYFLWYNYDD_VERB Y_DET CORN_NOUN HIRLAS_SYM
-_PUNCT Cyflwynir_VERB y_DET Corn_NOUN Hirlas_PROPN gan_ADP un_NUM o_ADP blith_NOUN mamau_NOUN 'r_DET fro_NOUN ._PUNCT
Manylion_NOUN pellach_ADJ a_CONJ ffurflen_NOUN gais_VERB ar_ADP gael_VERB ar_ADP wefan_NOUN yr_DET Eisteddfod_PROPN
cyfeiriad_NOUN
