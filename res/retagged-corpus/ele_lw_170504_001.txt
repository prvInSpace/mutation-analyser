<_SYM img_VERB /_PUNCT >_SYM
Beth_PRON sy_VERB '_PUNCT Mlân_PROPN '_PUNCT ?_PUNCT
Bydd_VERB 2_NUM o_ADP Gomediwyr_NOUN gorau_ADJ 'r_DET wlad_NOUN yn_ADP enw_NOUN nos_NOUN Sadwrn_PROPN yma_ADV ._PUNCT
Am_ADP ddigon_ADJ o_ADP hwyl_NOUN a_CONJ chwerthin_VERB gyda_ADP pheint_NOUN o_ADP gwrw_NOUN braf_ADJ ,_PUNCT beth_PRON am_ADP ymuno_VERB â_ADP ni_PRON nos_NOUN Sadwrn_PROPN ?_PUNCT
I_ADP archebu_VERB eich_DET tocynau_NOUN ,_PUNCT plîs_INTJ dilynwch_VERB y_DET linc_NOUN isod_ADV ._PUNCT
cyfeiriad_NOUN
<_SYM img_VERB /_PUNCT >_SYM
Taith_NOUN Fws_PROPN i_ADP fod_VERB yn_PART rhan_NOUN o_ADP gynulleidfa_NOUN Cythrel_PROPN Canu_PROPN 10.5_NUM ._PUNCT 1_NUM 7_NUM
Bydd_VERB bws_PRON am_ADP ddim_PART yn_PART gadael_VERB enw_NOUN am_ADP 5:45_NUM pm_NOUN Dydd_NOUN Mercher_PROPN nesaf_ADJ (_PUNCT 10.5_NUM ._PUNCT 1_NUM 7_NUM )_PUNCT i_PART weld_VERB y_DET sioe_NOUN anhygoel_ADJ yma_ADV llawn_ADJ bwrlwm_NOUN a_CONJ hiwmor_NOUN !_PUNCT
Bws_NOUN rhad_ADJ ac_CONJ am_ADP ddim_PRON !_PUNCT
Beth_PRON arall_ADJ sydd_VERB ymlaen_ADV gyda_ADP chi_PRON ar_ADP nos_NOUN Fercher_NOUN ?_PUNCT
Dim_PRON ond_ADP lle_ADV i_ADP 16_NUM sydd_VERB ar_ADP y_DET bws_NOUN felly_ADV cyntaf_ADJ i_ADP 'r_DET felin_NOUN ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT Cysylltwch_VERB gyda_ADP
cyfeiriad_NOUN -_PUNCT bost_NOUN
i_PART archebu_VERB lle_ADV ._PUNCT
cyfeiriad_NOUN
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Eich_DET tro_NOUN chi_PRON wrth_ADP y_DET meic_NOUN !_PUNCT
Ydych_AUX chi_PRON 'n_PART canu_VERB ,_PUNCT barddoni_VERB ,_PUNCT chwarae_VERB offeryn_NOUN ,_PUNCT dawnsio_VERB neu_CONJ eisiau_ADV dweud_VERB cwpwl_ADP o_ADP jôcs_NOUN ?_PUNCT
Mae_AUX 'r_DET enw_NOUN yn_PART cynnal_VERB noson_NOUN meic_NOUN agored_VERB nos_NOUN Wener_NOUN Mai_PROPN 19_NUM eg_PRON ._PUNCT
Os_CONJ hoffech_VERB chi_PRON gymryd_VERB rhan_NOUN ,_PUNCT cysylltwch_VERB gyda_ADP
cyfeiriad_NOUN -_PUNCT bost_NOUN
Os_CONJ ydych_AUX chi_PRON 'n_PART dysgu_VERB Cymraeg_PROPN ,_PUNCT mae_VERB 'r_DET noson_NOUN yn_PART addas_ADJ i_ADP chi_PRON hefyd_ADV !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Radio_VERB Dysgwyr_PROPN
Dylinwch_VERB y_DET linc_NOUN isod_ADV am_ADP gyfle_NOUN arall_ADJ i_PART wrando_VERB ar_ADP raglen_NOUN Radio_PROPN gan_ADP griw_NOUN o_ADP ddygwyr_NOUN o_ADP Abertawe_PROPN ._PUNCT
cyfeiriad_NOUN
<_SYM img_VERB /_PUNCT >_SYM
cyfeiriad_NOUN
Hoffa_VERB 'i_PRON cyfeiriad_NOUN estyn_NOUN gwahoddiad_NOUN i_ADP chi_PRON ymuno_VERB â_ADP nhw_PRON ar_ADP brynhawn_NOUN Dydd_NOUN Sul_PROPN am_ADP ginio_NOUN Capel_PROPN ._PUNCT
Am_ADP fwy_PRON o_ADP wybodaeth_NOUN cysylltwch_VERB â_ADP
cyfeiriad_NOUN -_PUNCT bost_NOUN
<_SYM img_VERB /_PUNCT >_SYM
enw_NOUN ,_PUNCT Eich_DET Canolfan_NOUN Gymraeg_PROPN Chi_PRON !_PUNCT
Annwyl_NOUN Gyfeillion_PROPN ,_PUNCT
Mae_VERB enw_NOUN ,_PUNCT canolfan_NOUN Gymraeg_PROPN lleoliad_NOUN __NOUN ,_PUNCT yn_PART dymuno_ADV ychwanegu_VERB at_ADP ei_DET weithgareddau_NOUN cymdeithasol_ADJ llwyddiannus_ADJ a_CONJ deall_VERB yr_DET hyn_PRON mae_VERB ein_DET cymuned_VERB yn_PART dymuno_ADV cynnal_VERB yn_ADP y_DET ganolfan_NOUN ._PUNCT
Hoffen_NOUN ni_PART glywed_VERB eich_DET barn_NOUN chi_PRON a_CONJ bydden_VERB ni_PRON 'n_PART ddiolchgar_ADJ iawn_ADV petaech_AUX chi_PRON 'n_PART cwblhau_VERB 'r_DET holiadur_NOUN byr_ADJ hwn_DET ._PUNCT
Bydd_AUX hyn_PRON yn_PART ein_PRON helpu_VERB i_ADP gynllunio_VERB i_ADP 'r_DET dyfodol_NOUN ac_CONJ ateb_VERB gofynion_NOUN ein_DET cymuned_VERB ._PUNCT
Cliciwch_VERB ar_ADP y_DET linc_NOUN isod_ADV ._PUNCT
Diolch_INTJ
cyfeiriad_NOUN
<_SYM img_VERB /_PUNCT >_SYM
