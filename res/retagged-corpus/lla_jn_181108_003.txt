Rhywle_ADV yng_ADP Ngymru_PROPN yn_PART bell_ADJ o_ADP bob_DET man_NOUN mae_VERB gwesty_NOUN arbennig_ADJ sy_AUX 'n_PART dod_VERB â_ADP phobl_NOUN yn_PART ôl_NOUN at_ADP ei_DET gilydd_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Oh_X my_NOUN goodness_VERB [_PUNCT giglan_VERB ]_PUNCT +_SYM
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM +_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ti_PRON nôl_ADV [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Rhaid_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
I_ADP 'r_DET rhai_PRON sy_AUX 'n_PART bwcio_VERB +_SYM
Helo_INTJ ._PUNCT
+_VERB '_PUNCT stafell_NOUN yma_ADV ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Helo_INTJ ._PUNCT
Sut_ADV mae_VERB ?_PUNCT
Croeso_NOUN i_ADP 'r_DET gwesty_NOUN ._PUNCT
Mae_VERB bywyd_NOUN ar_ADP fin_NOUN newid_VERB am_ADP byth_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT chwythu_VERB ]_PUNCT After_PROPN so_PART long_NOUN +_SYM
<_SYM S_NUM ?_PUNCT >_SYM Oh_X my_NOUN God_PROPN ._PUNCT
Hello_VERB gorgeous_ADJ <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM +_SYM how_VERB are_NOUN you_NOUN ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT cusanu_VERB ]_PUNCT Oh_X my_NOUN God_PROPN ._PUNCT
Gwesty_NOUN wedi_PART gael_VERB ei_DET greu_VERB ydy_VERB hwn_PRON ._PUNCT
Ar_ADP gyfer_NOUN pobl_NOUN sydd_VERB am_ADP gyfarfod_VERB rhywun_NOUN o_ADP 'i_PRON gorffennol_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM I_ADP know_VERB her_NOUN name_VERB now_PRON ._PUNCT
Ar_ADP ôl_NOUN blynyddoedd_NOUN o_ADP chwilio_VERB bydd_AUX rhai_PRON yn_PART llwyddo_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ôô_NOUN ._PUNCT
Eraill_NOUN +_SYM
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT llefain_NOUN ]_PUNCT ._PUNCT
+_AUX yn_PART methu_VERB ._PUNCT
Mae_AUX 'n_PART mynd_VERB i_PART ypsetio_VERB ti_PRON ._PUNCT
Ond_CONJ bydd_VERB ein_DET staff_NOUN yna_DET i_PART gefnogi_VERB pob_DET un_NUM ._PUNCT
Helo_INTJ enw_NOUN ._PUNCT
54.599_ADV
Mae_AUX pawb_PRON sy_AUX 'n_PART dod_VERB yma_ADV yn_PART chwilio_VERB am_ADP rhywbeth_NOUN neu_CONJ rhywun_NOUN ._PUNCT
Ac_CONJ am_ADP ei_PRON gael_VERB yn_PART ôl_NOUN yn_ADP enw_NOUN ._PUNCT
71.281_ADJ
Wel_ADV mae_VERB popeth_PRON gydan_VERB ni_PRON yma_ADV yn_ADP enw_NOUN ._PUNCT
Mae_VERB 'r_DET spa_NOUN mae_VERB 'r_DET pwll_NOUN nofio_NOUN mae_VERB gerddi_NOUN gydan_ADP ni_PRON ._PUNCT
Mae_VERB bwyd_NOUN gwych_ADJ gydan_VERB ni_PRON ._PUNCT
Mae_VERB tîm_NOUN gwych_ADJ gydan_VERB ni_PRON yma_ADV ._PUNCT
Mae_VERB enwb_NOUN gydan_PART ni_PRON sy_AUX 'n_PART edrych_VERB ar_ADP ôl_NOUN y_DET gwesteion_NOUN ._PUNCT
Helo_INTJ ._PUNCT
Gwesty_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mae_VERB enwg_VERB enwb_NOUN ac_CONJ __NOUN gydan_ADP ni_PRON i_ADP sicrhau_ADV bod_VERB pawb_PRON yn_PART cyfforddus_ADJ ._PUNCT
Jyst_ADV rhoi_VERB 'r_DET amser_NOUN a_CONJ 'r_DET siawns_NOUN i_ADP bobl_NOUN ddod_VERB i_ADP rhannu_VERB profiadau_NOUN nhw_PRON ._PUNCT
[_PUNCT S_NUM ?_PUNCT wrs_NOUN aneglur_ADJ yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
Mae_VERB heddiw_ADV yn_PART ddiwrnod_NOUN mawr_ADJ i_ADP enwb_VERB cyfenw_NOUN ._PUNCT
Mae_AUX wedi_PART dod_VERB i_ADP 'r_DET gwesty_NOUN i_ADP gyfarfod_VERB ei_DET hanner_NOUN chwaer_NOUN am_ADP y_DET tro_NOUN cynta'_ADJUNCT erioed_ADV ._PUNCT
A_CONJ hynny_PRON ar_ADP ôl_NOUN chwarter_NOUN canrif_NOUN o_ADP chwilio_VERB ._PUNCT
O_ADP helo_VERB enwb_NOUN ._PUNCT
Helo_INTJ ._PUNCT
Shwmae_INTJ ._PUNCT
A_CONJ sut_ADV siwrnai_NOUN gest_VERB ti_PRON ?_PUNCT
Da_ADJ iawn_ADV diolch_VERB yn_PART fawr_ADJ ._PUNCT
Ôô_NOUN ._PUNCT
Da_ADJ iawn_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
enwb_VERB ydw_VERB i_PRON ._PUNCT
Sut_ADV mae_VERB ?_PUNCT
Helo_INTJ sut_ADV mae_VERB ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT giglan_VERB ]_PUNCT helo_VERB ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART gwybod_VERB ers_ADP pan_CONJ o_ADP 'n_PART i_PRON 'n_PART bedair_NOUN ar_ADP ddeg_NOUN mae_VERB 'di_PART fy_PRON mabwysiadu_VERB ._PUNCT
enwg_VERB ._PUNCT
Mae_VERB 'na_ADV focs_NOUN hefyd_ADV tu_NOUN allan_ADJ ._PUNCT
Ydy_VERB o_PRON ?_PUNCT
Mae_VERB o_PRON 'na_ADV fel_ADP rhyw_DET fath_NOUN o_ADP gyS_ADJ ?_PUNCT od_DET bach_ADJ mewn_ADP ffordd_NOUN felly_ADV [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART atgoffa_VERB fi_PRON ie_INTJ wel_PRON mae_VERB 'na_ADV rhywun_NOUN ar_ADP ôl_NOUN yn_ADP dy_PRON golli_VERB di_PRON a_CONJ ti_PRON ddim_PART yn_PART mynd_VERB i_PART gwybod_VERB pwy_PRON '_PUNCT dyn_NOUN nhw_PRON ._PUNCT
Sut_ADV ydych_AUX chi_PRON 'n_PART teimlo_VERB ?_PUNCT
Dw_AUX i_PRON 'n_PART teimlo_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mae_VERB 'n_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Hi_PRON ydy_VERB 'r_DET unig_ADJ berson_NOUN sydd_VERB â_ADP 'r_DET un_NUM cig_NOUN a_CONJ gwaed_NOUN â_ADP fi_PRON dw_VERB i_PRON 'di_PART gyfarfod_VERB erioed_ADV heblaw_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT fy_DET hun_PRON felly_ADV ._PUNCT
O_ADP reit_NOUN ._PUNCT
Ie_INTJ ._PUNCT
A_CONJ felly_ADV yndy_NOUN ._PUNCT
Mae_VERB yn_PART reit_NOUN gyffrous_ADJ de_NOUN ._PUNCT
Yndy_INTJ ._PUNCT
Ydy_VERB ._PUNCT
Ond_CONJ -_PUNCT dy_DET dych_VERB chi_PRON wedi_ADP S_NUM ?_PUNCT wrsio_VERB o_ADP 'r_DET blaen_NOUN ._PUNCT
Do_INTJ ._PUNCT
Ar_ADP y_DET ffôn_NOUN felly_CONJ de_NOUN ._PUNCT
Okay_INTJ ._PUNCT
Fe_PART dos_VERB i_ADP o_ADP hyd_NOUN i_ADP 'm_DET hanner_NOUN chwaer_NOUN hefo_ADP 'r_DET prawf_NOUN DNA_PROPN ._PUNCT
Ââ_INTJ ._PUNCT
Trwy_ADP 'r_DET dde_NOUN rŵan_ADV ._PUNCT
Ac_CONJ  _SPACE bron_ADV iawn_ADV -_PUNCT yn_PART dw_VERB i_PRON 'n_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT lle_ADV mi_PART ges_VERB i_ADP neges_NOUN yn_PART ôl_NOUN yn_PART d'eud_VERB "_PUNCT My_X long_NOUN -_PUNCT lost_NOUN baby_VERB sister_NOUN ._PUNCT
I_ADP have_X been_NOUN looking_X for_X you_X for_X forty_NOUN years_NOUN "_PUNCT <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
173.411_NOUN
Yn_ADP y_DET car_NOUN ar_ADP y_DET ffordd_NOUN i_ADP 'r_DET gwesty_NOUN mae_VERB enwb_NOUN cyfenw_NOUN ._PUNCT
Mae_VERB 'di_PART hedfan_VERB yr_DET holl_DET ffordd_NOUN o_ADP Awstralia_PROPN i_ADP gyfarfod_VERB ei_DET chwaer_NOUN fach_ADJ am_ADP y_DET tro_NOUN cynta'_ADJUNCT ._PUNCT
Dros_ADP y_DET chwe_NUM mis_NOUN diwetha'_ADJUNCT mae_AUX enwb_NOUN a_CONJ enwb_VERB wedi_PART bod_VERB yn_PART e_PRON -_PUNCT bostio_VERB a_CONJ ffonio_VERB ei_DET gilydd_NOUN ._PUNCT
Rhannu_VERB 'r_DET un_NUM tad_NOUN biolegol_ADJ mae_VERB 'r_DET ddwy_NUM felly_CONJ roedd_VERB yn_PART dipyn_PRON o_ADP sioc_NOUN i_ADP enwb_ADV ddarganfod_ADV fod_AUX enwb_NOUN wedi_PART byw_VERB gyda_ADP 'i_PRON mam_NOUN biolegol_ADJ hefyd_ADV __NOUN ._PUNCT
Y_DET ffordd_NOUN mae_AUX enwb_NOUN yn_PART siarad_VERB amdani_ADP hi_PRON felly_ADV oedd_AUX hi_PRON 'n_PART ei_DET gweld_VERB hi_PRON fel_ADP mam_NOUN bron_ADV ._PUNCT
Ac_CONJ oedd_AUX hi_PRON 'n_PART ei_DET galw_VERB ei_DET hunan_VERB auntie_VERB enwb_NOUN ._PUNCT
Pan_CONJ oedd_VERB enwb_NOUN yn_PART dair_NUM ar_ADP hugain_NOUN oed_NOUN aeth_VERB i_PART fyw_VERB gyda_ADP teulu_NOUN enwb_NOUN fel_ADP lojar_NOUN ._PUNCT
Tra_ADV 'n_PART aros_VERB yna_ADV mi_PART gafodd_VERB berthynas_NOUN gyda_ADP 'r_DET tad_NOUN ._PUNCT
A_CONJ chanlyniad_NOUN yr_DET affêr_NOUN oedd_VERB enwb_NOUN ._PUNCT
Cadwyd_VERB yr_DET holl_DET beth_NOUN yn_PART dawel_ADJ ac_CONJ mi_PART gafodd_VERB enwb_NOUN ei_DET danfon_VERB i_ADP ffwrdd_ADV i_ADP gael_VERB ei_PRON mabwysiadu_VERB ._PUNCT
Ond_CONJ roedd_VERB 'na_ADV un_NUM ferch_NOUN fach_ADJ yn_PART gwybod_VERB am_ADP y_DET gyfrinach_NOUN enwb_NOUN ._PUNCT
Ac_CONJ mi_PART gadodd_VERB mewn_ADP cysylltiad_NOUN gyda_ADP enwb_NOUN ar_ADP hyd_NOUN ei_DET hoes_NOUN ._PUNCT
Roedd_AUX hi_PRON hyd_NOUN yn_PART oed_NOUN yna_ADV ar_ADP y_DET diwrnod_NOUN y_PART bu_VERB farw_VERB enwb_NOUN ._PUNCT
Wnaeth_VERB hi_PRON roi_VERB e_PRON -_PUNCT bost_NOUN ataf_ADP i_ADP yn_PART d'eud_VERB  _SPACE "_PUNCT Your_NOUN lovely_NOUN mother_NOUN has_NOUN just_ADV died_VERB in_ADP my_NOUN arms_NOUN "_PUNCT felly_ADV ._PUNCT
Ac_CONJ o_ADP 'n_PART i_PRON 'n_PART teimlo_VERB ie_INTJ mae_AUX yn_PART gwneud_VERB ti_PRON 'n_PART fwy_ADV arbennig_ADJ fel_ADP chwaer_NOUN ._PUNCT
Ydy_VERB ._PUNCT
yyy_CONJ 'na_ADV ochr_NOUN '_PUNCT nhad_NOUN ydy_VERB hi_PRON felly_ADV ._PUNCT
Dw_AUX i_PRON 'n_PART torri_VERB '_PUNCT mol_NOUN eisiau_ADV siarad_VERB efo_ADP hi_PRON de_NOUN ._PUNCT
268.633_ADJ
I_ADP can't_NOUN wait_VERB to_NOUN meet_VERB her_NOUN ._PUNCT
I_ADP mean_NOUN she_ADP 's_X flesh_NUM and_NOUN blood_NOUN ._PUNCT
What_NOUN more_NOUN can_NOUN you_NOUN ask_ADP for_CONJ ?_PUNCT
All_NOUN these_ADJ years_NOUN to_NOUN make_NOUN up_NUM for_CONJ <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
That_ADJ 's_X a_CONJ lovely_NOUN coat_ADJ ._PUNCT
Is_CONJ it_ADP real_VERB fur_NOUN <_SYM /_PUNCT en_PRON >_SYM ?_PUNCT
Yes_INTJ ._PUNCT
Mink_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Is_CONJ it_ADP really_ADV ?_PUNCT
Yeah_INTJ ._PUNCT
There_VERB 's_X lots_NOUN more_NOUN to_NOUN tell_NOUN her_NOUN ._PUNCT
Lots_NOUN more_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
[_PUNCT Cerddoriaeth_NOUN ]_PUNCT ._PUNCT
Mae_VERB rhai_PRON o_ADP 'r_DET gwesteion_NOUN yn_PART aros_VERB y_DET noson_NOUN ._PUNCT
Tra_CONJ bod_AUX eraill_PRON yn_PART taro_VERB fewn_ADP i_ADP 'r_DET gwesty_NOUN i_ADP gwrdd_VERB â_ADP hen_ADJ ffrindiau_NOUN neu_CONJ deulu_NOUN dros_ADP ginio_NOUN neu_CONJ de_NOUN y_DET prynhawn_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mae_VERB aduniadau_NOUN yn_PART bwysig_ADJ oherwydd_CONJ '_PUNCT dyn_NOUN ni_PART eisio_ADV gwybod_VERB bebe_NOUN sy_VERB 'di_PART digwydd_VERB i_ADP bobl_NOUN o_ADP 'n_PART gorffennol_ADJ ._PUNCT
Yn_PART eisio_VERB cau_VERB 'r_DET stori_NOUN mewn_ADP ffordd_NOUN ._PUNCT
Mae_AUX o_PRON 'n_PART ddiweddglo_NOUN ._PUNCT
Mae_AUX rhywun_NOUN yn_PART medru_ADV dweud_VERB wel_NOUN o_ADP leia'_ADJUNCT dw_AUX i_PRON 'n_PART gwybod_VERB rŵan_ADV ._PUNCT
Mae_AUX rhai_PRON yn_PART dod_VERB yma_ADV i_PART gael_VERB eu_DET S_NUM ?_PUNCT wylio_VERB yn_ADP y_DET spa_NOUN ._PUNCT
Un_NUM sy_AUX 'n_PART haeddu_VERB ei_PRON phampro_VERB ac_CONJ wedi_ADP 'i_PRON gwahadd_NOUN fel_ADP syrpreis_NOUN ydy_VERB 'r_DET cyn-_NOUN nyrs_NOUN plant_NOUN enwb_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART edrych_VERB ymlaen_ADV ._PUNCT
Yndw_INTJ ._PUNCT
Ond_CONJ [_PUNCT aneglur_ADJ ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT mae_VERB 'n_PART siŵr_ADJ bod_VERB 'na_ADV bobl_NOUN eraill_ADJ mae_VERB haeddu_VERB mwy_ADV S_NUM ?_PUNCT wylio_VERB na_PART fi_PRON de_NOUN ._PUNCT
347.148_ADV
Heb_ADP yn_PART wybod_VERB enwb_NOUN mae_VERB rhai_PRON o_ADP 'r_DET plant_NOUN oedd_AUX hi_PRON 'n_PART gofalu_VERB amdanyn_ADP nhw_DET flynyddoedd_NOUN yn_PART ôl_NOUN ar_ADP eu_DET ffordd_NOUN i_ADP 'r_DET gwesty_NOUN i_ADP dd'eud_VERB diolch_INTJ ._PUNCT
A_CONJ nhw_PRON sy_VERB 'di_PART trefnu_VERB prynhawn_NOUN o_ADP bampro_NOUN iddi_ADP ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ ._PUNCT
Felly_CONJ '_PUNCT dych_VERB chi_PRON 'di_PART mwynhau_VERB eich_DET hun_PRON yn_PART  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Do_INTJ ._PUNCT
Do_INTJ ._PUNCT
Gorwedd_VERB allan_ADV y_DET pwll_NOUN ?_PUNCT
Mae_VERB 'n_PART lyfli_NOUN on'd_VERB yndy_NOUN ?_PUNCT
Yn_PART gofalu_VERB am_ADP enwb_NOUN a_CONJ 'i_PRON thraed_VERB y_DET prynhawn_NOUN 'ma_ADV mae_VERB enwb_NOUN ._PUNCT
So_PART oes_VERB gennych_ADP chi_PRON blant_NOUN eich_DET hun_PRON ?_PUNCT
Dim_DET plant_NOUN eich_DET hun_PRON ._PUNCT
Dim_DET plant_NOUN fy_DET hun_PRON ._PUNCT
Plant_VERB pobl_NOUN eraill_ADJ ._PUNCT
Oeddech_VERB chi_PRON 'n_PART awydd_NOUN plant_NOUN eich_DET hun_PRON ?_PUNCT
O_ADP 'n_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Baswn_VERB i_PRON 'n_PART licio_ADJ ._PUNCT
Ond_CONJ doedd_VERB y_DET cyn-_VERB ŵr_ADP ddim_PRON ._PUNCT
Oedd_VERB o_PRON ddim_PART quite_ADJ mor_ADV keen_ADJ felly_CONJ wedyn_ADV mae_VERB gen_ADP i_PRON dri_NUM ci_NOUN so_AUX fi_PRON 'n_PART meddwl_VERB bod_AUX nhw_PRON 'n_PART fabis_NOUN i_ADP mi_PRON de_VERB [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
O_ADP ._PUNCT So_PART mae_AUX 'r_DET ci_NOUN yn_PART mynd_VERB i_ADP
A_PART maen_VERB nhw_PRON chwarae_VERB teg_ADJ ._PUNCT
+_VERB cadw_VERB chi_PRON 'n_PART brysur_ADJ ._PUNCT
Ydy_VERB ._PUNCT
Yndy_INTJ ._PUNCT
Ond_CONJ mae_VERB delio_VERB efo_ADP teuluoedd_NOUN sydd_VERB ar_ADP fin_NOUN colli_VERB plentyn_NOUN neu_CONJ yn_PART gweld_VERB +_SYM
Neu_PART fynd_VERB drwy_ADP driniaeth_NOUN ife_ADV ._PUNCT
+_AUX eu_DET plant_NOUN yn_PART mynd_VERB drw_ADV '_PUNCT [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Achos_CONJ efo_ADP liwcemia_NOUN dechreuwch_VERB chi_PRON am_ADP dair_NUM blynedd_NOUN iddyn_ADP nhw_PRON de_NOUN ._PUNCT
Dych_AUX chi_PRON 'n_PART dod_VERB yn_PART bart_NOUN o_ADP 'u_PRON teulu_NOUN nhw_PRON a_CONJ mae_VERB hynna_PRON 'n_PART brofiad_NOUN anhygoel_ADJ really_ADV achos_ADP maen_AUX nhw_PRON 'n_PART trystio_VERB chi_PRON cyn_ADP gymaint_ADV ._PUNCT
Mae_AUX sawl_ADJ plentyn_NOUN wedi_PART bod_VERB dan_ADP ofal_NOUN enwb_NOUN ._PUNCT
Un_NUM o_ADP 'r_DET rheini_NOUN ydy_VERB enwb_NOUN ._PUNCT
Dw_AUX i_PRON yma_ADV achos_PART wnaeth_VERB enwb_NOUN edrych_VERB ar_ADP ôl_NOUN fi_PRON a_CONJ fy_DET nheulu_NOUN  _SPACE un_NUM deg_NUM wyth_NUM flynedd_NOUN yn_PART ôl_NOUN ._PUNCT
Pan_CONJ o_ADP 'n_PART i_PRON 'n_PART un_NUM ar_ADP ddeg_NOUN ges_VERB i_ADP 'r_DET newyddion_NOUN bod_VERB gennai_VERB lewcemia_NOUN ._PUNCT
O'dd_NOUN 'na_ADV rhyw_DET vibe_ADJ amazing_NOUN efo_ADP hi_PRON [_PUNCT =_SYM ]_PUNCT O'dd_NOUN hi_PRON [_PUNCT /=_PROPN ]_PUNCT o'dd_AUX hi_PRON 'n_PART medru_ADV troi_VERB rhywun_NOUN oedd_AUX yn_PART teimlo_VERB mor_ADV negative_NOUN i_ADP rywun_NOUN oedd_AUX yn_PART teimlo_VERB mwya_ADJ positif_ADJ mewn_ADP awr_NOUN ._PUNCT
O'dd_NOUN hi_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Dyna_ADV sut_ADV mae_VERB hi_PRON ._PUNCT
Gweld_VERB bod_AUX chi_PRON 'di_PART dechrau_ADV relacsio_VERB -_PUNCT yn_PART mae_VERB eisiau_NOUN gorffan_NOUN yn_PART iawn_ADV on'd_X oes_NOUN ?_PUNCT
Ie_INTJ ond_CONJ mae_VERB gen_ADP i_PRON dri_NUM ci_NOUN fydd_AUX yn_PART diS_VERB ?_PUNCT wyl_ADV amdanai_NOUN i_ADP fynd_VERB â_ADP nhw_PRON am_ADP dro_NOUN wedyn_ADV on'd_VERB oes_NOUN ?_PUNCT
Ôô_NOUN ._PUNCT
Mae_VERB mywyd_NOUN bach_ADJ i_ADP 'n_PART mynd_VERB o_ADP gwmpas_NOUN eu_DET bywyd_NOUN bach_ADJ nhw_PRON [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Cwmni_NOUN Mae_VERB hwnna_PRON 'r_DET un_NUM beth_PRON de_NOUN ._PUNCT
Yn_PART enwedig_ADJ ar_ADP ôl_NOUN bod_VERB mor_ADV brysur_ADJ a_CONJ bod_VERB o_ADP gwmpas_NOUN pobol_ADJ Wedyn_PROPN adio_VERB [_PUNCT aneglur_ADJ ]_PUNCT de_NOUN ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART meddwl_VERB wrthi_ADP fy_DET hun_DET un_NUM diwrnod_NOUN de_NOUN ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART eiste_NOUN '_PUNCT ac_CONJ oedd_AUX hi_PRON wedi_PART bod_AUX yn_PART glawio_VERB ._PUNCT
A_PART wnes_VERB i_PRON ddim_PART siarad_VERB efo_ADP neb_PRON a_PART wnes_VERB i_PRON ddim_PART gweld_VERB neb_PRON ._PUNCT
Dim_PRON ond_ADP y_DET cŵn_NOUN a_CONJ mae_VERB 'n_PART fel_ADP hyn_PRON "_PUNCT oh_VERB my_NOUN goodness_VERB dw_AUX i_ADP ddim_PART eisiau_VERB hyn_PRON fod_VERB yn_PART bebe_NOUN ydy_AUX ymddeoliad_NOUN fi_PRON 'n_PART mynd_VERB i_ADP fod_VERB "_PUNCT ._PUNCT
'_PUNCT Dych_NOUN chi_PRON 'n_PART bored_VERB wrth_ADP gwrs_NOUN It_ADP was_NOUN a_CONJ bad_NOUN day_NUM de_NOUN ._PUNCT
Ôô_NOUN ._PUNCT
A_CONJ wedyn_ADV mae_AUX pob_DET dim_PRON yn_PART edrych_VERB yn_PART waeth_ADJ on'd_NOUN ydy_VERB ?_PUNCT
493.056_ADJ
Helo_INTJ ?_PUNCT
Helo_INTJ enwb_VERB enwb_NOUN sy_VERB 'ma_ADV ._PUNCT
Fedru_VERB di_PRON ddod_VERB lawr_ADV i_ADP 'r_DET llyfrgell_NOUN ?_PUNCT
Mae_AUX dy_DET chwaer_NOUN 'di_PART cyrraedd_VERB ._PUNCT
Okay_INTJ ._PUNCT
Wela_VERB i_ADP di_PRON yna_ADV ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART mynd_VERB trwy_ADP mywyd_NOUN yn_PART diS_VERB ?_PUNCT wyl_NOUN faswn_VERB i_ADP byth_ADV yn_PART cyfarfod_VERB neb_PRON ._PUNCT
A_CONJ rŵan_ADV mae_VERB hanner_NOUN chwaer_NOUN yma_ADV a_CONJ dw_AUX i_PRON 'n_PART mynd_VERB i_ADP 'w_PRON chyfarfod_VERB hi_PRON ._PUNCT
Dw_VERB i_PRON 'n_PART fath_NOUN o_ADP "_PUNCT ah_INTJ "_PUNCT [_PUNCT giglan_VERB ]_PUNCT felly_ADV ._PUNCT
Dw_AUX i_PRON eisiau_ADV gwybod_VERB bebe_NOUN ddigwyddodd_VERB i_ADP fy_DET mam_NOUN genegol_ADJ ac_CONJ wrth_ADP gwrs_NOUN fydd_AUX enwb_NOUN yn_PART gwybod_VERB achos_CONJ oedd_VERB hi_PRON 'n_PART ffrindiau_NOUN mawr_ADJ efo_ADP hi_PRON ._PUNCT
Yn_PART ogystal_ADJ â_ADP bod_VERB [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART ferch_NOUN i_ADP 'm_DET nhad_NOUN felly_ADV ._PUNCT
Mae_AUX cael_VERB aduniad_NOUN yn_PART gallu_VERB bod_VERB yn_PART brofiad_NOUN poenus_ADJ ac_CONJ hapus_ADJ yr_DET un_NUM pryd_NOUN on'd_NOUN ydy_VERB ?_PUNCT
Dw_VERB i_PRON 'na_ADV fel_ADP conselyr_NOUN ._PUNCT
Yma_ADV yn_PART helpu_VERB nhw_PRON ._PUNCT
Wedyn_ADV glust_NOUN neu_CONJ roi_VERB cysur_NOUN iddyn_ADP nhw_PRON ._PUNCT
544.502_NOUN
Ar_ADP hyd_NOUN ei_DET hoes_NOUN mae_AUX enwb_NOUN wedi_PART bod_AUX yn_PART chwilio_VERB am_ADP atebion_NOUN i_ADP 'w_PRON holl_DET gwestiynau_NOUN ._PUNCT
Mae_VERB 'r_DET aros_VERB bron_ADV ar_ADP ben_NOUN ._PUNCT
Mae_VERB enwb_NOUN ar_ADP fin_NOUN cyfarfod_VERB ei_DET chwaer_NOUN enwb_NOUN am_ADP y_DET tro_NOUN cyntaf_ADJ erioed_ADV ._PUNCT
Mae_AUX 'r_DET ddwy_NUM chwaer_NOUN yn_PART rhannu_VERB 'r_DET un_NUM tad_NOUN [_PUNCT saib_NOUN ]_PUNCT ond_CONJ yn_PART rhyfeddol_ADJ roedd_VERB enwb_NOUN efo_ADP mam_NOUN gwaed_NOUN enwb_NOUN ar_ADP y_DET diwrnod_NOUN y_PART bu_VERB farw_VERB ._PUNCT
Dw_AUX i_PRON 'n_PART dal_ADV yn_PART pinsio_VERB 'm_DET hun_PRON a_CONJ dw_AUX i_PRON 'n_PART dal_ADV methu_ADV credu_VERB bod_VERB enwb_NOUN yn_PART dywad_ADJ yr_DET holl_DET ffordd_NOUN o_ADP ochr_NOUN arall_ADJ y_DET byd_NOUN felly_ADV ._PUNCT
Ac_CONJ wna_VERB i_PRON chyfarfod_VERB hi_PRON yn_ADP yr_DET munudau_NOUN nesa'_ADJUNCT ._PUNCT
648.018_NOUN
Ben_VERB arall_ADJ y_DET gwesty_NOUN yn_ADP y_DET spa_NOUN mae_AUX enwb_NOUN yn_PART rhoi_VERB 'r_DET polish_NOUN ola_X '_PUNCT i_PRON draed_NOUN y_DET cyn-_VERB nyrs_NOUN plant_NOUN enwb_NOUN ._PUNCT
So_INTJ '_PUNCT dych_AUX chi_PRON wedi_PART mwynhau_VERB eich_DET pedicure_VERB y_DET prynhawn_NOUN 'ma_ADV ?_PUNCT
Do_INTJ ._PUNCT
Wir_ADV ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ ._PUNCT
Dych_VERB chi_PRON 'n_PART -_PUNCT eitha_ADV da_ADJ iawn_ADV ._PUNCT
Da_ADJ iawn_ADV ._PUNCT
Wi_VERB 'di_PART edrych_VERB ar_ADP ôl_NOUN gwinedd_NOUN traed_VERB mewn_ADP <_SYM anegur_NOUN 2_NUM >_SYM ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
Wel_INTJ ._PUNCT
Allai_VERB  _SPACE cael_VERB slipers_NOUN bach_ADJ i_PART fynd_VERB â_ADP 'r_DET +_PROPN
Ie_INTJ ._PUNCT
+_VERB dillad_NOUN ar_ADP lein_NOUN o_ADP hyn_PRON ymlaen_ADV [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Mae_AUX 'n_PART amser_NOUN i_ADP [_PUNCT aneglur_ADJ ]_PUNCT ar_ADP ôl_NOUN eich_DET hun_NOUN rŵan_ADV ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Dw_VERB i_PRON yn_PART dueddol_ADJ i_ADP roi_VERB pobl_NOUN ymlaen_ADV i_ADP fy_DET hun_PRON felly_ADV ._PUNCT
Wrth_ADP gwrs_NOUN dw_AUX i_PRON 'di_PART ymddeol_VERB rŵan_ADV so_ADJ ._PUNCT
Achos_CONJ dw_AUX i_PRON 'n_PART colli_VERB 'r_DET cysylltiad_NOUN efo_ADP 'r_DET plant_NOUN ._PUNCT
Achos_CONJ o'dden_VERB nhw_PRON 'n_PART blant_NOUN arbennig_ADJ de_NOUN ._PUNCT
Fel_ADP syrpreis_NOUN i_ADP enwb_NOUN mae_VERB rhai_PRON o_ADP 'r_DET teuluoedd_NOUN mae_AUX hi_PRON 'di_PART helpu_VERB dros_ADP y_DET blynyddoedd_NOUN yma_DET yn_ADP y_DET gwesty_NOUN ._PUNCT
Y_DET cynta'_NOUNUNCT sy_VERB eisiau_ADV d'eud_VERB diolch_INTJ ydy_VERB enwb_NOUN ._PUNCT
dw_VERB i_ADP yma_ADV achos_PART wnaeth_VERB enwb_NOUN edrych_VERB ar_ADP ôl_NOUN fi_PRON a_CONJ fy_DET nheulu_NOUN  _SPACE un_NUM deg_NUM wyth_NUM flynedd_NOUN yn_PART ôl_NOUN ._PUNCT
Pan_CONJ o_ADP 'n_PART i_PRON 'n_PART un_NUM ar_ADP ddeg_NOUN ges_VERB i_ADP 'r_DET newyddion_NOUN bod_VERB gennai_VERB lewcemia_NOUN ._PUNCT
Does_VERB 'na_ADV geiriau_NOUN i_ADP dd'eud_VERB sut_ADV o'dd_VERB hi_PRON ._PUNCT
O'dd_NOUN hi_PRON jyst_ADV yn_PART amazing_NOUN ._PUNCT
O_ADP 'r_DET diwedd_NOUN mae_AUX enwb_NOUN yn_PART cael_ADV gwybod_VERB pwy_PRON sydd_AUX wedi_PART trefnu_VERB ei_DET diwrnod_NOUN yn_ADP y_DET spa_NOUN heddiw_ADV ._PUNCT
Hello_PROPN ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
Ôô_NOUN ._PUNCT
[_PUNCT llefain_ADJ ]_PUNCT ._PUNCT
Ti_PRON 'n_PART iawn_ADJ ?_PUNCT
[_PUNCT llefain_ADJ ]_PUNCT ._PUNCT
Ie_INTJ ._PUNCT
880.566_VERB
Sut_ADV mae_VERB pethau_NOUN ?_PUNCT
Popeth_NOUN +_SYM
Iawn_INTJ ._PUNCT
Sut_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
+_VERB yn_PART iawn_ADJ ?_PUNCT
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
Yndy_INTJ ._PUNCT
Sut_ADV mae_VERB pethau_NOUN felly_ADV ?_PUNCT
Iawn_INTJ ._PUNCT
Jyst_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ti_PRON 'n_PART mwynhau_VERB ?_PUNCT
Nac_PART ydw_VERB [_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
Dw_VERB i_PRON ddim_PART yn_PART siŵr_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT peth_NOUN gorau_ADJ dw_AUX i_PRON 'di_PART wneud_VERB <_SYM aneglur_ADJ 2_NUM >_SYM ._PUNCT
Ond_CONJ wi_AUX 'n_PART ddo_VERB '_PUNCT i_ADP 'r_DET arfer_NOUN ._PUNCT
Wi_AUX 'n_PART colli_VERB nhw_PRON on'd_ADJ oes_NOUN i_PRON ?_PUNCT
O_ADP dwi_VERB 'n_PART +_SYM
Ôô_NOUN paid_VERB ._PUNCT
+_SYM colli_VERB nhw_PRON i_ADP gyd_ADP ._PUNCT
Colli_VERB chi_PRON i_ADP gyd_ADP ._PUNCT
Ôô_NOUN paid_VERB ._PUNCT
[_PUNCT llefain_ADJ ]_PUNCT ._PUNCT
So_PART nid_PART  _SPACE fi_PRON ydy_VERB 'r_DET unig_ADJ plentyn_NOUN sydd_VERB yma_ADV heddiw_ADV 'ma_ADV ._PUNCT
Mae_VERB 'na_ADV lot_NOUN mwy_ADJ yma_ADV ._PUNCT
Nag_PART oes_VERB ._PUNCT
Oes_VERB ._PUNCT
933.087_NOUN
Dros_ADP de_NOUN brynhawn_NOUN mae_VERB enwb_NOUN yn_PART barod_ADJ i_ADP ddatgelu_VERB mwy_ADV i_ADP 'w_PRON chwaer_NOUN enwb_NOUN [_PUNCT saib_NOUN ]_PUNCT am_ADP ei_DET tad_NOUN ac_CONJ am_ADP ei_DET affêr_VERB ._PUNCT
[_PUNCT S_NUM ?_PUNCT wrs_NOUN aneglur_ADJ yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
Bendigedig_ADJ ._PUNCT
Oh_INTJ lovely_NOUN +_SYM
Yma_ADV ._PUNCT
+_VERB thank_NOUN -_PUNCT you_NOUN ._PUNCT
Wow_PROPN ._PUNCT
Well_ADJ we_NOUN are_NOUN being_NOUN spoilt_NOUN aren't_X we_NOUN ._PUNCT
S7_VERB ?_PUNCT Bendigedig_PROPN ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ iawn_ADV ._PUNCT
There_VERB 's_X your_NOUN dad_NOUN ._PUNCT
Yeah_VERB '_PUNCT cause_NOUN <_SYM /_PUNCT en_DET >_SYM [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
You_ADV look_VERB so_ADV much_NOUN like_VERB him_PRON yeah_ADP ._PUNCT
Do_VERB you_NOUN think_NOUN ?_PUNCT
Yes_INTJ ?_PUNCT <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Yeah_VERB you_NOUN do_PRON ._PUNCT
Think_NOUN you_X do_X too_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
[_PUNCT giglan_VERB ]_PUNCT He_PART was_NOUN always_ADJ laughing_NOUN ._PUNCT
Yeah_INTJ ?_PUNCT
He_PART obviously_NOUN was_NOUN yeah_ADV <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Oh_X he_PART never_NOUN stopped_ADJ smiling_NOUN ._PUNCT
Pan_CONJ o'dd_NOUN enwb_NOUN yn_PART ddeuddeg_NUM oed_NOUN mi_PART gafodd_VERB ei_DET thad_NOUN enwg_VERB __NOUN affêr_VERB gyda_ADP ffrind_NOUN i_ADP 'r_DET teulu_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Auntie_VERB enwb_NOUN came_VERB to_X live_X with_NOUN us_NOUN when_NOUN I_ADP was_NOUN three_NOUN ._PUNCT
When_ADV I_ADP was_NOUN twelve_ADV [_PUNCT saib_NOUN ]_PUNCT we_NOUN went_VERB to_NOUN Australia_PROPN for_CONJ eighteen_NOUN months_NOUN ._PUNCT
We_NOUN left_VERB dad_NOUN ._PUNCT
That_ADJ was_NOUN a_CONJ long_NOUN time_NOUN to_NOUN [_PUNCT =_SYM ]_PUNCT leave_ADV [_PUNCT /=_PROPN ]_PUNCT leave_NOUN a_CONJ man_NOUN ._PUNCT
Yn_PART ystod_NOUN y_DET cyfnod_NOUN oedd_VERB enwb_NOUN a_CONJ 'i_PRON mam_NOUN yn_PART Awstralia_PROPN daeth_VERB enwb_NOUN yn_PART feichiog_ADJ gyda_ADP babi_NOUN __NOUN ._PUNCT
Penderfynodd_VERB y_DET ddau_NUM yrru_VERB 'r_DET ferch_NOUN fach_ADJ i_PART gael_VERB ei_PRON mabwysiadu_VERB ._PUNCT
Cyn_CONJ i_ADP enwb_NOUN a_CONJ 'i_PRON mam_NOUN gyrraedd_VERB adra_ADV '_PUNCT ._PUNCT
1184.28_PROPN
Dw_VERB i_PRON ddim_PART yn_PART flin_ADJ efo_ADP enwg_VERB na_CONJ enwb_NOUN ._PUNCT
Ar_ADP ôl_NOUN feddwl_VERB yn_PART ôl_NOUN rŵan_ADV maen_AUX nhw_PRON wedi_PART gorfod_ADV celu_VERB hynna_PRON am_ADP gymaint_ADV o_ADP flynyddoedd_NOUN a_CONJ neb_PRON yn_PART gwybod_VERB ._PUNCT
Mae_VERB hwnna_PRON 'n_PART lyfli_NOUN [_PUNCT aneglur_ADJ ]_PUNCT +_SYM
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
+_VERB wneud_VERB yr_DET un_NUM peth_NOUN It_ADP 's_X lovely_NOUN ._PUNCT
Ac_CONJ un_NUM arall_ADJ ._PUNCT
Gwna_VERB i_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ben_VERB arall_ADJ y_DET gwesty_NOUN mae_VERB aduniad_NOUN enwb_NOUN gyda_ADP 'r_DET plant_NOUN y_PART bu_AUX 'n_PART gofalu_VERB amdanyn_ADP nhw_PRON dros_ADP y_DET blynyddoedd_NOUN yn_PART dirwyn_VERB i_ADP ben_NOUN ._PUNCT
Wel_INTJ dw_AUX i_PRON 'n_PART colli_VERB nhw_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
enwb_VERB  _SPACE dw_AUX i_PRON 'n_PART meddwl_VERB bod_VERB pawb_PRON yma_ADV really_ADV eisiau_VERB diolch_VERB i_ADP ti_PRON ._PUNCT
ymm_ADV dw_AUX i_PRON 'n_PART meddwl_VERB bod_VERB o_PRON 'n_PART andros_ADJ o_ADP  _SPACE fraint_NOUN i_ADP fod_VERB yma_ADV fel_ADP survivor_NOUN a_CONJ i_ADP fod_VERB  _SPACE efo_ADP byd_NOUN o_ADP 'r_DET teuluoedd_NOUN 'ma_ADV ._PUNCT
Ac_CONJ mae_AUX pawb_PRON jyst_ADV eisiau_ADV dweud_VERB diolch_NOUN ._PUNCT
A_PART diolch_NOUN yn_PART [_PUNCT aneglur_ADJ ]_PUNCT ti_PRON 'di_PART wneud_VERB yn_PART arbennig_ADJ ._PUNCT
'_PUNCT Sdim_PROPN eisiau_VERB i_ADP chi_PRON dd'eud_VERB diolch_NOUN i_ADP fi_PRON ._PUNCT
Nac_PART oes_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Oes_VERB ._PUNCT
<_SYM SS_X >_SYM Oes_VERB ._PUNCT
Diolch_INTJ ._PUNCT
Oes_VERB ._PUNCT
Dw_AUX i_PRON ddim_PART yn_PART meddwl_VERB bod_AUX ti_PRON 'n_PART dall_ADJ faint_ADV ._PUNCT
Ti_PRON actually_VERB 'di_PART wneud_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dw_AUX i_PRON ddim_PART yn_PART meddwl_VERB '_PUNCT swn_NOUN i_PRON 'ma_ADV ._PUNCT
-_PUNCT S_NUM sorri_VERB enwb_NOUN ._PUNCT
No_NOUN go_ADV on_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dw_AUX i_PRON ddim_PART yn_PART meddwl_VERB '_PUNCT swn_NOUN i_ADP yma_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT os_CONJ na_PART faswn_VERB i_ADP wedi_PART cyrraedd_VERB cyfarfod_NOUN chi_PRON ._PUNCT
Ie_INTJ ._PUNCT
Diolch_INTJ ._PUNCT
So_PART ni_PRON 'n_PART wneud_VERB cheers_NOUN bach_ADJ ._PUNCT
O_ADP 'n_PART i_VERB wrth_ADP fy_DET modd_NOUN efo_ADP 'r_DET job_NOUN [_PUNCT =_SYM ]_PUNCT Oedd_VERB y_DET [_PUNCT /=_PROPN ]_PUNCT oedd_VERB y_DET [_PUNCT saib_NOUN ]_PUNCT plant_NOUN bach_ADJ 'na_ADV i_ADP gyd_NOUN yn_PART mynd_VERB drw_ADV '_PUNCT amser_NOUN caled_ADJ âu_ADP teuluoedd_NOUN ._PUNCT
Y_DET cwbl_NOUN o_ADP 'n_PART i_ADP eisiau_ADV wneud_VERB o'dd_NOUN wneud_VERB pob_DET dim_PRON yn_PART iawn_ADJ iddyn_ADP nhw_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Gobeithio_ADV welwn_VERB ni_PRON chi_PRON 'n_PART fuan_ADJ ._PUNCT
Jyst_ADV eisiau_ADV helpu_VERB nhw_PRON o_ADP 'n_PART i_PRON ._PUNCT
Jyst_ADV eisiau_ADV wneud_VERB pob_DET dim_PRON yn_PART iawn_ADJ iddyn_ADP nhw_PRON ._PUNCT
Maen_AUX nhw_PRON 'di_PART mynd_VERB drwy_ADP digon_ADJ fel_ADP o'dd_NOUN hi_PRON ._PUNCT
O'ddwn_VERB i_ADP eisio_VERB jyst_ADV wneud_VERB siŵr_ADV [_PUNCT =_SYM ]_PUNCT bod_VERB [_PUNCT /=_PROPN ]_PUNCT bod_AUX nhw_PRON ddim_PART yn_PART gorfod_ADV cael_VERB dim_DET byd_NOUN arall_ADJ o'dd_NOUN [_PUNCT saib_NOUN ]_PUNCT i_ADP fi_PRON fod_AUX yn_PART wneud_VERB â_ADP nhw_PRON ._PUNCT
1311.229_NOUN
Wel_ADV mae_VERB popeth_PRON gydan_VERB ni_PRON yma_ADV yn_ADP enw_NOUN ._PUNCT
Mae_VERB bwyd_NOUN gwych_ADJ gydan_VERB ni_PRON ._PUNCT
yyy_ADV ystafelloedd_NOUN moethus_ADJ ._PUNCT
Dw_AUX i_PRON eisiau_ADV bod_VERB popeth_PRON ar_ADP gael_VERB i_ADP 'r_DET bobl_NOUN yma_ADV ._PUNCT
Yn_PART gweithio_VERB 'r_DET bar_NOUN a_CONJ 'r_DET bwyty_NOUN heno_ADV mae_VERB enwb_NOUN ._PUNCT
Dw_VERB i_PRON wrth_ADP fy_DET modd_NOUN efo_ADP 'r_DET job_NOUN 'ma_ADV ._PUNCT
Dw_VERB i_PRON 'n_PART [_PUNCT saib_NOUN ]_PUNCT licio_VERB busnesu_VERB [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Dim_PART gormod_NOUN ._PUNCT
Oedd_VERB hynna_PRON ddim_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT tu_NOUN fewn_ADP ond_CONJ t'mod_VERB mae_AUX 'n_PART people_ADV watsio_VERB ie_INTJ Relief_PROPN ydy_VERB 'r_DET peth_NOUN mwya'_ADJUNCT ._PUNCT
Weithiau_ADV maen_AUX nhw_PRON yn_PART cael_VERB pethau_NOUN off_ADP eu_DET chest_VERB nhw_PRON ._PUNCT
[_PUNCT S_NUM ?_PUNCT wrs_NOUN aneglur_ADJ yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
A_PART mae_VERB 'na_ADV sawl_ADJ person_NOUN yma_ADV heno_ADV sy_AUX 'n_PART chwilio_VERB am_ADP atebion_NOUN a_CONJ 'u_PRON aduniad_NOUN perffaith_NOUN nhw_DET ._PUNCT
Un_NUM o_ADP 'r_DET rheini_NOUN ydy_VERB enwb_NOUN ._PUNCT
BeBe_AUX ga_NOUN i_PRON i_ADP chi_PRON i_ADP yfed_VERB felly_ADV ?_PUNCT
dim_PRON ots_NOUN o_ADP gael_VERB gwin_NOUN gwyn_ADJ os_CONJ oes_VERB yna_ADV ?_PUNCT
Gwin_NOUN gwyn_ADJ ?_PUNCT
Ie_INTJ ._PUNCT
<_SYM fr_VERB >_SYM Sauvignon_PROPN Blanc_PROPN <_SYM /_PUNCT fr_VERB >_SYM ?_PUNCT
Ie_INTJ plîs_INTJ ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ iawn_ADV ._PUNCT
Sydd_VERB yma_ADV i_PART gyfarfod_VERB un_NUM dyn_NOUN arbennig_ADJ iawn_ADV ._PUNCT
Wel_INTJ dw_AUX i_PRON 'n_PART meddwl_ADV enwg_VERB cyfenw_NOUN heno_ADV 'ma_ADV ._PUNCT
O'dd_NOUN o_PRON yn_ADP y_DET teitl_NOUN efo_ADP 'm_DET mrawd_NOUN ._PUNCT
Sut_ADV '_PUNCT dych_AUX chi_PRON 'n_PART nabod_ADV enwg_VERB felly_ADV ?_PUNCT
Wel_PART wnes_VERB i_PRON golli_VERB fy_DET mrawd_NOUN yn_ADP y_DET Falklands_PROPN +_SYM
O_ADP na_DET ddo_VERB ._PUNCT
Mae_VERB 'n_PART ddrwg_ADJ gen_ADP i_PRON ._PUNCT
+_VERB  _SPACE dros_ADP [_PUNCT =_SYM ]_PUNCT tri_NUM deg_NUM [_PUNCT /=_PROPN ]_PUNCT tri_NUM deg_NUM chwech_NUM mlynedd_NOUN yn_PART ôl_NOUN rŵan_ADV ._PUNCT
Mmm_VERB ._PUNCT
A_PART dw_AUX i_ADP wedi_PART nabod_ADV enwg_VERB a_CONJ enwg_VERB efo_ADP ;_PUNCT i_PART gwch_NOUN ._PUNCT
Efo_ADP fo_PRON ?_PUNCT
Efo_ADP fo_PRON ie_INTJ ._PUNCT
Dw_AUX i_PRON eisiau_VERB gofyn_VERB ychydig_PRON o_ADP bethau_NOUN iddo_ADP fo_PRON yma_ADV heddiw_ADV ._PUNCT
Sut_ADV mae_AUX o_PRON yn_PART teimlo_VERB ar_ADP ôl_NOUN yr_DET holl_DET flynyddoedd_NOUN a_CONJ sut_ADV dw_AUX i_PRON 'n_PART teimlo_VERB ._PUNCT
A_CONJ sut_ADV fyddan_AUX ni_PRON 'n_PART helpu_VERB ein_DET gilydd_NOUN a_CONJ cadw_VERB y_DET Falklands_PROPN yn_PART mynd_VERB ._PUNCT
Bod_AUX pobl_NOUN ddim_PART yn_PART anghofio_VERB ._PUNCT
1459.593_VERB
2850.987_ADP
