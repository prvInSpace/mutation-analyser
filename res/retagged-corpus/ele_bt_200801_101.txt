"_PUNCT *_SYM *_SYM *_SYM Late_PROPN Change_PROPN S4C_PROPN -_PUNCT Newid_NOUN Hwyr_ADJ S4C_PROPN *_SYM *_SYM *_SYM -_PUNCT Wythnos_PROPN 18_NUM -_PUNCT Wednesday_X 29_NUM /_SYM 04_NUM /_SYM 2020_NUM Dydd_NOUN Mercher_PROPN "_PUNCT ,_PUNCT "_PUNCT *_SYM *_SYM Late_PROPN Change_PROPN S4C_PROPN -_PUNCT Newid_NOUN Hwyr_ADJ S4C_PROPN *_SYM *_SYM
Wythnos_PROPN 18_NUM
29_NUM /_SYM 04_NUM /_SYM 2020_NUM Dydd_NOUN Mercher_PROPN
21_NUM :_PUNCT 30_NUM Rhaglen_NOUN Newydd_ADJ Cyswllt_PROPN (_PUNCT Mewn_ADP COVID_NOUN )_PUNCT (_PUNCT rhif_NOUN cynhyrchu_VERB )_PUNCT
22_NUM :_PUNCT 00_NUM Dim_DET Newid_NOUN Iaith_NOUN ar_ADP Daith_PROPN
23_NUM :_PUNCT 00_NUM Amser_NOUN Newydd_ADJ Wil_PROPN ac_CONJ Aeron_PROPN :_PUNCT Taith_NOUN yr_DET Alban_PROPN
23_NUM :_PUNCT 30_NUM Amser_NOUN Newydd_ADJ Cymoedd_PROPN Roy_PROPN Noble_PROPN
00_NUM :_PUNCT 05_NUM Amser_NOUN Newydd_ADJ Closedown_PROPN
21_NUM :_PUNCT 30_NUM
CYSWLLT_VERB (_PUNCT MEWN_NOUN COVID_NOUN )_PUNCT
Cyfres_NOUN ddrama_NOUN newydd_ADJ i_PART adlewyrchu_VERB 'r_DET byd_NOUN dros_ADP 3_NUM wythnos_NOUN wrth_ADP iddo_ADP newid_VERB o_ADP 'n_PART cwmpas_NOUN yn_PART llythrennol_ADJ ._PUNCT
Mae_VERB ein_DET gliniaduron_NOUN a_CONJ 'n_PART ffonau_NOUN wedi_PART dod_VERB yn_PART lifelines_NOUN a_CONJ bydd_AUX y_DET ddrama_NOUN hon_DET yn_PART cael_VERB ei_PRON saethu_VERB yn_PART bennaf_ADJ yn_PART nhai_VERB 'r_DET cast_NOUN ar_ADP liniaduron_NOUN a_CONJ ffonau_NOUN ._PUNCT
Wrth_PART ddychwelyd_VERB o_ADP shift_NOUN yn_ADP yr_DET ysbyty_NOUN mae_VERB Ffion_PROPN (_PUNCT Hannah_PROPN Daniel_PROPN )_PUNCT yn_PART derbyn_VERB galwad_NOUN mae_AUX hi_PRON wedi_PART bod_AUX yn_PART ei_DET ofni_VERB ac_CONJ mae_AUX Dewi_PROPN (_PUNCT Aneirin_PROPN Hughes_X )_PUNCT yn_PART ceisio_ADV cadw_VERB rhamant_VERB ei_DET berthynas_NOUN yn_PART fyw_VERB er_ADP gwaetha_ADJ rheolau_NOUN hunan_ADP ynysu_VERB ._PUNCT
22_NUM :_PUNCT 00_NUM
IAITH_NOUN AR_NOUN DAITH_X :_PUNCT Colin_PROPN Jackson_PROPN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT SC_X )_PUNCT (_PUNCT AD_X )_PUNCT (_PUNCT HD_X )_PUNCT
Ymunwch_VERB gyda_ADP 'r_DET dysgwr_NOUN ac_CONJ athletwr_NOUN o_ADP fri_NOUN Colin_PROPN Jackson_PROPN wrth_ADP iddo_ADP fe_PRON a_CONJ 'r_DET cyflwynwraig_NOUN Eleri_PROPN Siôn_PROPN deithio_VERB ar_ADP draws_ADJ Cymru_PROPN gyda_ADP 'i_PRON gilydd_NOUN ._PUNCT
Ar_ADP hyd_NOUN y_DET daith_NOUN fe_PART fydd_VERB sialensau_NOUN ieithyddol_ADJ gwahanol_ADJ yn_PART ei_PRON wynebu_VERB ._PUNCT
Tybed_ADV a_PART fydd_AUX Colin_PROPN yn_PART cipio_VERB 'r_DET fedal_NOUN aur_NOUN am_ADP ddysgu_VERB Cymraeg_PROPN ?_PUNCT
23_NUM :_PUNCT 00_NUM
WIL_PROPN AC_X AERON_X :_PUNCT TAITH_X YR_X ALBAN_X (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT SC_X )_PUNCT (_PUNCT HD_X )_PUNCT
Mae_VERB 'r_DET ddau_NUM ffermwr_NOUN ,_PUNCT Wil_X Hendreseifion_X ac_CONJ Aeron_PROPN Pughe_PROPN ,_PUNCT ar_ADP daith_NOUN trwy_ADP gefn_NOUN gwlad_NOUN yr_DET Alban_PROPN mewn_ADP hen_ADJ camperfan_NOUN ._PUNCT
Y_DET tro_NOUN hwn_DET ,_PUNCT mae_VERB 'n_PART amser_NOUN iddyn_ADP nhw_PRON ffarwelio_VERB â_ADP 'r_DET tir_NOUN mawr_ADJ a_CONJ chroesi_VERB i_ADP Ynys_PROPN Uist_PROPN yn_ADP yr_DET Hebrides_NOUN ._PUNCT
Yma_ADV ,_PUNCT maen_AUX nhw_PRON 'n_PART ymdrochi_VERB ym_ADP mywyd_NOUN pysgotwyr_NOUN allan_ADV ar_ADP y_DET môr_NOUN mawr_ADJ ,_PUNCT un_NUM o_ADP 'r_DET swyddi_NOUN caletaf_ADP ym_ADP Mhrydain_PROPN ._PUNCT
Ond_CONJ mae_AUX Wil_PROPN yn_PART cael_VERB trafferth_NOUN ymdopi_NOUN â_ADP 'r_DET tonnau_NOUN !_PUNCT
23_NUM :_PUNCT 30_NUM
CYMOEDD_VERB ROY_PROPN NOBLE_PROPN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT SC_X )_PUNCT (_PUNCT HD_X )_PUNCT
Cwm_NOUN ola_ADP 'r_DET gyfres_NOUN yw_VERB Dyffryn_NOUN Tywi_PROPN ._PUNCT
Y_DET ffermwr_NOUN Aled_ADJ Edwards_PROPN a_CONJ 'i_PRON wraig_NOUN Eleri_PROPN sy_AUX 'n_PART tywys_VERB Roy_PROPN o_ADP amgylch_NOUN eu_DET fferm_NOUN lle_NOUN maen_AUX nhw_PRON 'n_PART magu_VERB gwartheg_NOUN Limousin_PROPN ._PUNCT
Yna_ADV ,_PUNCT Prif_NOUN Fachgen_NOUN Coleg_NOUN Llanymddyfri_PROPN ,_PUNCT Huw_PROPN Richards_PROPN Price_PROPN ,_PUNCT sy_AUX 'n_PART croesawu_VERB Roy_PROPN i_ADP 'r_DET coleg_NOUN hanesyddol_ADJ ._PUNCT
Jim_NOUN Griffiths_PROPN sydd_AUX yn_PART treulio_VERB misoedd_NOUN ar_ADP y_DET tro_NOUN yn_PART teithio_VERB 'r_DET byd_NOUN ar_ADP gefn_NOUN ei_DET feic_NOUN modur_NOUN sydd_VERB nesaf_ADJ cyn_ADP i_ADP Roy_DET gwrdd_NOUN â_ADP 'i_PRON hen_ADJ ffrind_NOUN ,_PUNCT y_DET ddarlledwraig_NOUN Sara_NOUN Edwards_PROPN ,_PUNCT sydd_VERB erbyn_ADP hyn_PRON yn_ADP Arglwydd_PROPN Raglaw_NOUN Sir_NOUN Ddyfed_PROPN ._PUNCT
00_NUM :_PUNCT 05_NUM
DIWEDD_PROPN
Rydw_VERB i_PRON 'n_PART siaradwr_NOUN Cymraeg_PROPN rhugl_ADJ ._PUNCT
