enwg_VERB !_PUNCT
Shwd_ADV mae_VERB pethau_NOUN ?_PUNCT
Gobeithio_VERB bo_AUX ti_PRON 'n_PART cael_VERB amser_NOUN i_PART ymlacio_VERB nawr_ADV ar_ADP ôl_NOUN blwyddyn_NOUN brysur_ADJ !_PUNCT
Jest_PART cysylltu_VERB er_ADP mwyn_NOUN rhoi_ADV gwybod_VERB mod_AUX i_ADP wedi_PART sicrhau_VERB swydd_NOUN addysgu_VERB (_PUNCT tiwtor_NOUN y_DET Gymraeg_PROPN )_PUNCT yn_ADP y_DET adran_NOUN Gymraeg_PROPN yn_PART sefydliad_NOUN ,_PUNCT dechrau_VERB mis_NOUN Medi_PROPN mae_VERB 'n_PART debyg_ADJ !_PUNCT
Heb_ADP roi_VERB 'r_DET newyddion_NOUN ar_ADP FB_PROPN eto_ADV achos_CONJ bod_AUX angen_NOUN i_ADP HR_ADV brosesu_VERB poeth_ADJ ._PUNCT
Rhan_NOUN o_ADP 'r_DET swydd_NOUN yn_PART mynnu_VERB creu_VERB cysylltiadau_NOUN rhwng_ADP ysgolion_NOUN a_CONJ 'r_DET brifysgol_NOUN ,_PUNCT a_CONJ ceisio_ADV cynyddu_VERB niferoedd_NOUN etc_PART ._PUNCT
Felly_CONJ fe_PART fyddi_AUX di_PRON 'n_PART clywed_VERB gen_ADP i_PRON rywbryd_ADV yn_ADP y_DET dyfodol_NOUN agos_ADJ mae_VERB 'n_PART debyg_ADJ !_PUNCT
Eniwe_PROPN ,_PUNCT gobeithio_VERB bod_VERB popeth_PRON yn_PART iawn_ADJ !_PUNCT
A_CONJ chi_PRON 'ch_PRON dwy_NUM 'n_PART cael_VERB llonydd_NOUN gyda_ADP 'r_DET babi_NOUN !_PUNCT
Siarad_VERB yn_PART fuan_ADJ !_PUNCT
