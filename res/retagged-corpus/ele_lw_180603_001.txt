Newyddion_NOUN diweddara'_VERBUNCT gan_ADP enw_NOUN
Annwyl_NOUN Aelod_PROPN
Gyda_ADP mis_NOUN newydd_ADJ daw_VERB e_PRON -_PUNCT bost_NOUN newydd_ADJ i_ADP 'ch_PRON hysbysu_VERB o_ADP ddigwyddiadau_NOUN enw_NOUN a_CONJ 'n_PART cyfeillion_VERB enw_NOUN __NOUN ._PUNCT
Estynnir_VERB groeso_VERB i_ADP enw_NOUN i_ADP ddiwrnod_NOUN cenedlaethol_ADJ Galicia_PROPN ar_ADP 24_NUM ain_NOUN a_CONJ 'r_DET 25_NUM ain_NOUN o_ADP Orffennaf_X ._PUNCT
Dyddiad_NOUN cau_ADV cofrestru_VERB yw_VERB 'r_DET 5_NUM ed_DET o_ADP Orffennaf_PROPN ac_CONJ mae_AUX costau_NOUN 'n_PART cael_VERB eu_PRON talu_VERB ar_ADP ran_NOUN enw_NOUN ._PUNCT
Ceir_VERB mwy_PRON o_ADP wybodaeth_NOUN
yma_ADV ._PUNCT
Fe_PART 'm_DET gwahoddwyd_VERB iwŷl_VERB gerddoriaeth_NOUN Catalonia_NOUN (_PUNCT enw_NOUN )_PUNCT sy_AUX 'n_PART cael_VERB ei_DET gynnal_VERB ar_ADP yr_DET 19_NUM eg_NOUN -_PUNCT 21_NUM ain_NOUN o_ADP Orffennaf_X ._PUNCT
Dyddiad_NOUN cau_ADV cofrestru_VERB yw_VERB 'r_DET 15_NUM ed_DET o_ADP Fehefin_NOUN ac_CONJ mae_VERB mynediad_NOUN am_ADP ddim_PRON i_ADP ddau_NUM aelod_NOUN ._PUNCT
Ceir_VERB mwy_ADV owybodaeth_NOUN
yma_ADV ,_PUNCT
Digwyddiad_NOUN Dafydd_PROPN Wigley_PROPN
-_PUNCT Ar_ADP ddydd_NOUN Iau_NOUN 7_NUM ed_DET o_ADP Fehefin_NOUN bydd_AUX Dafydd_PROPN Wigley_PROPN yn_PART cynnal_VERB trafodaeth_NOUN ar_ADP Brexit_NOUN yn_ADP Nhŷ_NOUN 'r_DET Arglwyddi_NOUN ._PUNCT
Cynhelir_VERB y_DET digwyddiad_NOUN yn_ADP enw_NOUN ar_ADP Westgate_X Street_X am_ADP 7_NUM o_ADP 'r_DET gloch_NOUN ._PUNCT
Dilynwch_VERB y_DET
linc_NOUN
ymaer_VERB mwyn_NOUN cofrestru_VERB 'ch_PRON lle_NOUN ._PUNCT
Diwrnodau_NOUN Arolwg_PROPN
-_PUNCT Mae_VERB enw_NOUN yn_PART brysur_ADJ o_ADP hyd_NOUN gyda_ADP 'r_DET arolwg_NOUN cenedlaethol_ADJ ._PUNCT
Dros_ADP yr_DET wythnosau_NOUN nesaf_ADJ bydd_VERB y_DET sesiynau_NOUN canlynol_ADJ :_PUNCT
2_NUM il_PRON o_ADP Fehefin_NOUN
Casnewydd_PROPN ,_PUNCT 11_NUM am_ADP ger_ADP cerflun_NOUN y_DET Siartwyr_PROPN
Pen_NOUN -_PUNCT y_DET -_PUNCT Bont_PROPN ,_PUNCT 11_NUM am_ADP yng_ADP Nghaerdydd_PROPN Canolog_PROPN cyn_CONJ dal_DET tren_NOUN i_ADP Ben_PROPN -_PUNCT y_DET -_PUNCT Bont_PROPN
9_NUM ed_DET o_ADP Fehefin_NOUN -_PUNCT Merthyr_PROPN ,_PUNCT 11_NUM am_ADP yng_ADP Nghaerdydd_PROPN Canolog_PROPN cyn_CONJ dal_DET tren_NOUN i_ADP Ferthyr_PROPN
16_NUM eg_NOUN o_ADP Fehefin_NOUN -_PUNCT Casnewydd_PROPN ,_PUNCT 11_NUM am_ADP ger_ADP cerflun_NOUN y_DET Siartwyr_PROPN
30_NUM ain_NOUN o_ADP Fehefin_NOUN -_PUNCT Casnewydd_PROPN ,_PUNCT 11_NUM am_ADP ger_ADP cerflun_NOUN y_DET Siartwyr_PROPN
Taith_NOUN Gerdded_PROPN <_SYM aon_NOUN >_SYM enwg_VERB 1_NUM cyfenw_NOUN 1_NUM -_PUNCT dewch_VERB i_ADP gefnogi_VERB ymgyrch_NOUN sy_AUX 'n_PART codi_VERB arian_NOUN tuag_ADP at_ADP Ganolfan_PROPN Ganser_NOUN enw_NOUN lleoliad_NOUN ._PUNCT
Bydd_AUX y_DET daith_NOUN gerdded_AUX yn_PART digwyddar_VERB Orffennaf_PROPN 14_NUM eg_PRON ac_CONJ yn_PART dechrau_VERB yng_ADP Nghanolfan_NOUN Ymwelwyr_NOUN Coedwig_NOUN enw_NOUN ._PUNCT
Cliciwch_VERB
yma_ADV
am_ADP fwy_PRON o_ADP wybodaeth_NOUN ac_CONJ i_PART gofrestru_VERB ._PUNCT
