Rwy_AUX 'n_PART credu_VERB bod_AUX Y_DET Ddaear_NOUN mynd_VERB i_PART bod_VERB yn_PART siâp_NOUN triongl_ADJ +_SYM
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
+_VERB neu_CONJ sgwâr_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Hecsagon_NOUN [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Bydd_VERB e_PRON 'n_PART cŵl_ADJ os_CONJ oedd_VERB Y_DET Ddaear_NOUN yn_PART siâp_NOUN calon_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
<_SYM SS_X >_SYM Ie_INTJ ._PUNCT
Os_CONJ o_ADP 'n_PART i_PRON 'di_PART glanio_VERB fel_ADP ar_ADP planed_NOUN Mawrth_PROPN bydd_VERB e_PRON fel_ADP Y_DET Ddaear_PROPN ond_CONJ bydd_VERB e_PRON ddim_PART fel_ADP brenhines_NOUN a_CONJ fel_ADP president_NOUN bydd_VERB e_PRON fel_ADP yn_PART fel_ADP alien_NOUN a_CONJ stwff_NOUN fel_ADP 'na_ADV sy_AUX 'n_PART controlo_VERB 'r_DET planed_NOUN i_ADP gyd_ADP ._PUNCT
Wel_CONJ fi_PRON dim_PRON really_ADV moyn_ADV mynd_VERB i_ADP 'r_DET gofod_NOUN achos_ADP chi_PRON just_ADV yn_PART mynd_VERB lan_ADV fan_NOUN 'na_ADV i_ADP just_ADV gweld_VERB y_DET lleuad_NOUN chi_PRON just_ADV yn_PART gallu_ADV gweld_VERB e_PRON o_ADP ystafell_NOUN wely_NOUN chi_PRON ._PUNCT
Bydd_VERB fel_ADP hotel_NOUN +_SYM
Ie_INTJ ._PUNCT
+_VERB yn_ADP y_DET gofod_NOUN ._PUNCT
Fel_CONJ yn_PART planedau_NOUN pob_DET planed_NOUN yn_PART gyda_ADP hotel_NOUN ti_PRON gallu_ADV mynd_VERB i_ADP +_SYM
<_SYM sibrwd_VERB >_SYM Ie_INTJ ._PUNCT
+_VERB achos_CONJ mae_VERB rhai_DET dwym_ADJ mae_VERB rhai_DET oer_ADJ ._PUNCT
Hyd_ADP :_PUNCT 00.04_NUM ._PUNCT 5_NUM 5_NUM -_SYM 00.05_NUM ._PUNCT 4_NUM 5_NUM =_SYM 50_NUM eiliad_NOUN ._PUNCT
Gwyliau_NOUN ar_ADP blaned_NOUN arall_ADJ [_PUNCT saib_NOUN ]_PUNCT byddai_VERB hynna_PRON 'n_PART grêt_ADJ ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Reit_VERB nesaf_ADJ mae_AUX enwg_VERB yn_PART mynd_VERB i_PART wneud_VERB pethau_NOUN sy_AUX 'n_PART edrych_VERB yn_PART amhosib_ADJ ._PUNCT
Ydw_VERB i_PRON ?_PUNCT
Dim_DET ti_PRON y_DET llall_PRON ._PUNCT
Hyd_ADP :_PUNCT 00.05_NUM ._PUNCT 5_NUM 0_NUM -_PUNCT 00.06_NUM ._PUNCT 0_NUM 0_NUM =_SYM 10_NUM eiliad_NOUN ._PUNCT
Pawb_PART dweud_VERB helo_VERB i_ADP enwg_VERB os_CONJ gwelwch_VERB yn_PART dda_ADJ ?_PUNCT
<_SYM SS_X >_SYM Helo_INTJ enwg_VERB ._PUNCT
Nawr_ADV mae_AUX 'n_PART rhaid_VERB ni_PRON gael_ADV enwg_VERB '_PUNCT nôl_NOUN yn_ADP ei_DET dŷ_NOUN ond_CONJ yn_PART anffodus_ADJ dydy_AUX e_PRON ddim_PART yn_PART gallu_ADV ffitio_VERB mewn_ADP ._PUNCT
Gallwn_VERB ni_PRON gael_VERB rhyw_DET syniadau_NOUN o_ADP sut_ADV gallwn_VERB ni_PRON gael_VERB e_PRON yn_PART ôl_NOUN i_ADP 'w_PRON dŷ_NOUN ?_PUNCT
Gallwn_VERB ni_PRON fel_ADV cracio_VERB fe_PRON a_CONJ mewn_ADV ?_PUNCT
Allwn_VERB ni_PRON gracio_VERB fe_PRON ?_PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Byddai_VERB ti_PRON eisiau_VERB cracio_VERB dy_DET ffrind_NOUN gorau_ADJ ?_PUNCT
Ââ_VERB na_INTJ ._PUNCT
Na_INTJ ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Mae_VERB eisiau_NOUN i_ADP chi_PRON gael_VERB rhyw_DET fath_NOUN o_ADP cael_VERB e_PRON 'n_PART llai_ADJ o_ADP 'r_DET gorau_ADJ ?_PUNCT
Beth_PRON os_CONJ fyddwn_AUX ni_PRON 'n_PART tynnu_VERB tamaid_NOUN o_ADP 'r_DET plisg_NOUN i_ADP ffwrdd_ADV ?_PUNCT
<_SYM SS_X >_SYM Ie_INTJ ._PUNCT
[_PUNCT Y_DET plant_NOUN yn_PART siarad_VERB ar_ADP draws_ADJ ei_DET gilydd_NOUN ]_PUNCT ._PUNCT
O_ADP 'r_DET gorau_ADJ te_NOUN yn_PART ofalus_ADJ beth_PRON dw_AUX i_PRON 'n_PART mynd_VERB i_PART wneud_VERB yw_ADV tynnu_VERB 'r_DET plisg_NOUN +_SYM
<_SYM S_NUM ?_PUNCT >_SYM Ââ_X ._PUNCT
+_VERB i_ADP ffwrdd_ADV [_PUNCT saib_NOUN ]_PUNCT Chi_PRON 'n_PART meddwl_VERB bydd_AUX e_PRON 'n_PART ffitio_VERB mewn_ADP nawr_ADV ?_PUNCT
<_SYM SS_X >_SYM Na_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ie_INTJ ._PUNCT
Gawn_VERB ni_PRON weld_VERB ._PUNCT
<_SYM SB_SYM >_SYM Sa_X i_PRON 'n_PART gwybod_VERB ._PUNCT
<_SYM SB_SYM >_SYM Edrych_VERB rhy_ADV squashed_VERB ._PUNCT
Bron_ADV â_ADP fod_VERB ._PUNCT
<_SYM SS_X >_SYM [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT Plant_PROPN yn_PART siarad_VERB ar_ADP draws_ADJ ei_DET gilydd_NOUN ]_PUNCT ._PUNCT
<_SYM SB_SYM >_SYM Beth_PRON os_CONJ +_SYM
Ond_CONJ mae_VERB +_SYM
<_SYM SB_SYM >_SYM ni_PRON 'n_PART squasho_VERB fe_PRON ?_PUNCT
+_VERB rhywbeth_NOUN tu_NOUN fewn_ADP ._PUNCT
Beth_PRON sydd_VERB fewn_ADP mae_AUX 'n_PART rhaid_VERB ni_PRON cael_VERB gwared_NOUN o_ADP ?_PUNCT
<_SYM SS_NOUN >_SYM Aer_PROPN ._PUNCT
<_SYM SG_VERB >_SYM  _SPACE aer_NOUN ?_PUNCT
Aer_NOUN ?_PUNCT
Da_ADJ iawn_ADV ._PUNCT
Nawr_ADV ni_PRON methu_VERB [_PUNCT chwythu_VERB ]_PUNCT chwythu_VERB 'r_DET aer_NOUN mas_ADJ ._PUNCT
Ni_PART methu_VERB [_PUNCT anadlu_VERB ]_PUNCT sugno_VERB 'r_DET aer_NOUN mas_ADJ ond_CONJ mae_VERB gen_ADP i_ADP ffordd_NOUN ._PUNCT
Ni_PART 'n_PART mynd_VERB i_ADP llosgi_VERB 'r_DET ocsigen_NOUN o_ADP 'r_DET aer_NOUN a_CONJ gobeithio_VERB wedyn_ADV bydd_VERB e_PRON 'n_PART digon_ADV cynnes_ADJ i_PART enwg_VERB i_PART mynd_VERB mewn_ADP i_ADP 'w_PRON tŷ_NOUN ._PUNCT
Chi_PRON moyn_ADV gweld_VERB ?_PUNCT
<_SYM SS_X >_SYM Ydyn_VERB ._PUNCT
O_ADP 'r_DET gorau_ADJ yn_PART ofalus_ADJ '_PUNCT te_NOUN dw_AUX i_PRON 'n_PART mynd_VERB i_PART gynnu_VERB 'r_DET fflam_NOUN [_PUNCT saib_NOUN ]_PUNCT a_CONJ byddwn_AUX ni_PRON 'n_PART gosod_VERB e_PRON i_ADP mewn_ADP i_ADP 'r_DET fflasg_NOUN ._PUNCT
[_PUNCT Sŵn_NOUN cynnau_NOUN 'r_DET fflam_NOUN gyda_ADP thaniwr_NOUN ]_PUNCT ._PUNCT
Hyd_ADP :_PUNCT 00.06_NUM ._PUNCT 1_NUM 3_NUM -_SYM 00.07_NUM ._PUNCT 2_NUM 5_NUM =_SYM 1_NUM funud_NOUN 12_NUM eiliad_NOUN ._PUNCT
Efallai_ADV bydd_VERB y_DET rhai_DET craff_ADJ ohonoch_ADP chi_PRON yn_PART sylwi_VERB bod_VERB ni_PART tu_NOUN fas_ADJ am_ADP yr_DET arbrawf_NOUN olaf_ADJ ._PUNCT
Achos_CONJ bod_VERB e_PRON 'n_PART rhy_ADV beryglus_ADJ i_PART wneud_VERB yn_ADP y_DET stiwdio_NOUN ._PUNCT
Ni_PART 'n_PART mynd_VERB i_ADP roi_VERB nitrogen_NOUN hylifol_ADJ yn_ADP y_DET botel_NOUN 'ma_ADV a_CONJ mae_VERB hynna_PRON 'n_PART hollol_ADV saff_ADJ ._PUNCT
Ydy_VERB ._PUNCT
Mae_AUX 'r_DET hylif_NOUN yn_PART troi_VERB 'n_PART nwy_NOUN a_CONJ heb_ADP y_DET caead_NOUN '_PUNCT sdim_NOUN problem_NOUN ._PUNCT
Ond_CONJ gyda_ADP 'r_DET caead_NOUN [_PUNCT -_PUNCT ]_PUNCT
Bydd_AUX y_DET nwy_NOUN yn_PART gwneud_VERB i_ADP 'r_DET gwasgedd_NOUN yn_ADP y_DET botel_NOUN mynd_VERB lan_ADV a_CONJ lan_ADV a_CONJ lan_ADV [_PUNCT -_PUNCT ]_PUNCT
Nes_CONJ bod_AUX e_PRON 'n_PART ffrwydro_VERB ._PUNCT
Efallai_ADV dylwn_VERB ni_PRON ddim_PART gwneud_VERB e_DET te_NOUN ._PUNCT
Ti_PRON moyn_NOUN gofyn_VERB i_ADP nhw_PRON ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT Chi_PRON moyn_ADV gweld_VERB ffrwydriad_NOUN ?_PUNCT
<_SYM SS_X >_SYM Ydyn_VERB ._PUNCT
Dr_NOUN enwb_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT Cyn_CONJ i_ADP ni_PRON ddechrau_VERB allaf_VERB i_ADP just_ADV ofyn_VERB cwestiwn_NOUN gyflym_ADJ ?_PUNCT
ymm_PART allwn_VERB ni_PRON rhoi_VERB rhain_PRON mewn_ADP plis_INTJ ?_PUNCT
Ah_PART enwg_VERB '_PUNCT ychan_NOUN ._PUNCT
Chi_PRON moyn_ADV rhoi_VERB rhain_PRON mewn_ADP ?_PUNCT
<_SYM SS_X >_SYM Ydyn_VERB ._PUNCT
Rhybudd_NOUN i_ADP chi_PRON nawr_ADV ._PUNCT
Unwaith_ADV ni_PRON 'n_PART dechrau_VERB hwn_PRON ni_PART ffaelu_VERB stopio_VERB a_CONJ efallai_ADV bydd_VERB bach_ADJ o_ADP sŵn_NOUN so_ADV rhowch_VERB eich_DET dwylo_NOUN dros_ADP eich_DET clustiau_NOUN ._PUNCT
Hyd_ADP :_PUNCT 00.09_NUM ._PUNCT 0_NUM 5_NUM -_PUNCT 00.09_NUM ._PUNCT 5_NUM 8_NUM =_SYM 53_NUM eiliad_NOUN ._PUNCT
Mae_AUX 'r_DET ffrwydriad_NOUN yn_PART chwalu_VERB 'r_DET botel_NOUN ond_CONJ nid_PART y_DET bin_NOUN felly_CONJ yr_DET unig_ADJ ffordd_NOUN gall_ADJ y_DET peli_NOUN fynd_VERB yw_VERB syth_ADJ lan_ADV i_ADP 'r_DET awyr_NOUN ._PUNCT
[_PUNCT Sŵn_NOUN ffrwydriad_NOUN ar_ADP y_DET dechrau_NOUN ]_PUNCT ._PUNCT
Hyd_ADP :_PUNCT 00.10_NUM ._PUNCT 5_NUM 7_NUM -_PUNCT 00.11_NUM ._PUNCT 0_NUM 4_NUM =_SYM 7_NUM eiliad_NOUN ._PUNCT
Tro_VERB nesaf_ADJ ar_ADP enw_NOUN ni_PRON 'n_PART llosgi_VERB losin_NOUN i_PART weld_VERB beth_PRON sy_AUX 'n_PART digwydd_VERB ._PUNCT
Byddwn_AUX ni_PRON 'n_PART dangos_VERB i_ADP chi_PRON sut_ADV mae_VERB creu_VERB braich_NOUN robotig_ADJ [_PUNCT saib_NOUN ]_PUNCT a_CONJ byddaf_VERB i_PRON 'n_PART trial_ADJ mesur_NOUN cyflymder_NOUN jet_X ._PUNCT
Hyd_ADP :_PUNCT 00.11_NUM ._PUNCT 0_NUM 8_NUM -_PUNCT 00.11_NUM ._PUNCT 2_NUM 1_NUM =_SYM 13_NUM eiliad_NOUN ._PUNCT
Hyd_ADP y_DET trawsgrifiad_NOUN =_SYM 00.03_NUM ._PUNCT 2_NUM 5_NUM ._PUNCT
684.949_VERB
