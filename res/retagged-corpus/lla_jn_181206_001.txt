0.0_NOUN N_NUM /_PUNCT A_CONJ
Mae_AUX pawb_PRON sy_AUX 'n_PART dod_VERB yma_ADV yn_PART chwilio_VERB am_ADP rywbeth_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Neu_CONJ rywun_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ac_CONJ am_ADP ei_DET ga'l_NOUN yn_PART ôl_NOUN yn_ADP enw_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mae_VERB 'n_PART fore_ADJ newydd_ADJ yn_ADP enw_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ 'r_DET staff_NOUN yn_PART cyfarfod_VERB i_ADP roi_VERB trefn_NOUN ar_ADP y_DET dydd_NOUN ._PUNCT
Ma'_VERBUNCT rhaid_VERB cofio_VERB bobo_VERB pawb_PRON sy_AUX 'n_PART dod_VERB trwy_ADP 'r_DET drws_NOUN yma_DET nawr_ADV mama_VERB nhw_PRON yma_ADV i_ADP ga'l_VERB aduniad_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Amser_NOUN dw_AUX i_ADP meddwl_VERB yw_VERB 'r_DET peth_NOUN mwya'_ADJUNCT pwysig_ADJ i_PART rhoi_VERB i_ADP 'r_DET gwesteion_NOUN yma_ADV ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Amser_NOUN i_ADP siarad_VERB yn_PART rhydd_ADJ am_ADP y_DET pethe_NOUN sy_AUX 'n_PART digwydd_VERB yn_PART bywyde_ADJ nhw_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Amser_NOUN i_ADP ffwrdd_ADV o_ADP bywyde_NOUN pob_DET dydd_NOUN y_DET bobol_NOUN yma_ADV ._PUNCT
Myn_VERB '_PUNCT pass_NOUN yma_DET enwb_NOUN ?_PUNCT
Hmm_INTJ ._PUNCT
Rili_VERB neis_ADJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Yr_DET cynta'_NOUNUNCT i_ADP gyrra'dd_NOUN y_DET gwesty_NOUN heddi_NOUN efo_ADP 'i_PRON merch_NOUN enwb_NOUN ydy_VERB enwb_NOUN __NOUN o_ADP __NOUN __NOUN __NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X '_PUNCT hi_PRON yma_ADV i_ADP ddarganfod_VERB y_DET gwir_NOUN am_ADP 'i_PRON thad_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Da_ADJ [_PUNCT saib_NOUN ]_PUNCT mewn_ADP â_ADP ni_PRON ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dw_VERB i_PRON jes_VERB '_PUNCT yma_ADV i_PART drio_ADV ffindio_VERB allan_ADV pwy_PRON '_PUNCT dy_DET '_PUNCT nhad_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM ym_ADP ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Bysa_X 'n_PART gallu_VERB bo_VERB 'n_PART un_NUM o_ADP dri_NUM '_PUNCT dan_ADP ni_PRON meddwl_VERB ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ '_PUNCT dan_ADP ni_PRON 'n_PART dod_VERB i_ADP 'ch_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
I_ADP ga'l_VERB yr_DET DNA_NOUN results_ADJ ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
I_ADP 'r_DET chwith_NOUN '_PUNCT wan_ADJ ia_PRON ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP ia_PRON ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Aru_X mam_NOUN erioed_ADV d_PART '_PUNCT eu_PRON '_PUNCT thof_NOUN fi_PRON o_ADP ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Nest_PROPN i_ADP ffindio_VERB allan_ADV trw_X '_PUNCT ffrindia'_NOUNUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ffrindia_PROPN '_PUNCT fi_PRON a_PART ffrindia'_VERBUNCT hen_ADJ ffrindia'_NOUNUNCT mam_NOUN ._PUNCT
[_PUNCT Sgwrs_PROPN yn_PART rhy_ADV dawel_ADP i_ADP 'w_PRON chlywed_VERB ]_PUNCT ._PUNCT
Ma_INTJ '_PUNCT 'na_ADV lot_PRON o_ADP bobol_NOUN yn_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X '_PUNCT ffrindia_VERB 'n_PART d'eud_VERB gwahanol_ADJ betha_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dw_VERB i_PRON jyst_ADV '_PUNCT sho_NOUN gw'bod_ADJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
For_ADP cert_NOUN '_PUNCT '_PUNCT lly_X ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yndw_X ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Rili_ADV edrych_VERB ymlaen_ADV i_ADP fe_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Lla'_NOUNUNCT i_ADP 'm_DET gwtsiad_NOUN ddeu_ADJ '_PUNCT gwir_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
'_PUNCT S_NUM been_NOUN a_CONJ long_NOUN time_NOUN coming_NOUN ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Gobeithio_VERB gei_PRON di_PRON ateb_VERB genna'_VERBUNCT 'i_PRON ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Dw_VERB i_PRON siŵr_ADV gaga_VERB i_ADP ryw_VERB ateb_NOUN enwb_NOUN ._PUNCT
O_ADP dw_VERB i_ADP me'wl_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT bod_VERB o_PRON 'n_PART iawn_ADJ i_ADP bawb_PRON gaga_VERB gw'bobo_DET lle_NOUN mama_VERB nhw_PRON 'di_PART dod_VERB o_ADP ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT C'os_NOUN ar_ADP y_DET funud_NOUN '_PUNCT dy_DET mam_NOUN 'm_DET gw'bobo_NOUN dim_DET byd_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ fydd_VERB o_ADP neis_NOUN i_ADP mam_NOUN ca'l_VERB ffindio_VERB allan_ADV y_DET gwir_NOUN o_ADP 'r_DET diwadd_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM BeBe_NOUN '_PUNCT sach_NOUN chdi_NOUN licio_VERB fo_PRON dd'eud_VERB ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Dw_VERB i_PRON [_PUNCT /=_PROPN ]_PUNCT dw_AUX i_PRON 'di_PART mynd_VERB i_ADP 'r_DET stage_NOUN '_PUNCT wan_ADJ dim_DET bwys_NOUN genna'_VERBUNCT i_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dw_VERB i_PRON jes_VERB '_PUNCT '_PUNCT sho_NOUN gw'bod_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT W_X 's_DET isho_NOUN gw'bod_VERB '_PUNCT wbath_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Wbath_X ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Positif_PROPN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ti_PRON teimlo_VERB bobo_VERB chdi_NOUN mwydro_VERB pen_NOUN pobol_ADJ o_ADP hyd_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ti_PRON 'n_PART gw'bobo_DET hyn_DET ti_PRON 'n_PART gw'bobo_ADJ llall_DET ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Pwy_PRON '_PUNCT dy_DET hwn_DET pwy_PRON '_PUNCT dy_DET hwnna_DET ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Bo_X '_PUNCT nhw_PRON sick_ADJ o_ADP cwest'ynna'_VERBUNCT o_ADP hyd_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Mae_VERB 'r_DET gwesty_NOUN yma_DET i_PART groesawi_VERB unrhyw_DET un_NUM sy_VERB am_ADP gyfarfod_NOUN hen_ADJ ffrindie_NOUN dros_ADP baned_VERB neu_CONJ broseco_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Ma_VERB 'n_PART braf_ADJ weithia'_VERBUNCT ca'l_NOUN aduniad_NOUN o_ADP ffrindia'_NOUNUNCT sy_VERB jyst_ADV isho_NOUN cyfarfod_NOUN 'u_PRON gilydd_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dod_VERB yma_DET am_ADP dê_NOUN p'nawn_SYM ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ca'l_VERB hwyl_NOUN ._PUNCT
Hel_PART atgofion_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
A_CONJ dyna_PRON 'n_PART union_ADJ mae_VERB 'r_DET cyn_ADP nyrs_NOUN enwb_NOUN cyfenw_NOUN am_ADP ei_PRON gael_VERB heddiw_ADV ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Roedd_VERB hi_PRON a_CONJ 'r_DET dair_NUM ffrind_NOUN yma_DET yn_PART nyrsio_VERB efo_ADP 'i_PRON gilydd_NOUN yn_ADP ysbyty_NOUN lleoliad_NOUN '_PUNCT nôl_NOUN yn_ADP y_DET chwedegau_NOUN a_CONJ 'r_DET saithdegau_NOUN ._PUNCT
Sut_ADV w't_ADJ ti_PRON 'r_DET '_PUNCT en_PRON [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
W_INTJ iawn_INTJ [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
[_PUNCT Pawb_PROPN yn_PART siarad_VERB ar_ADP yr_DET un_NUM tro_NOUN ]_PUNCT ._PUNCT
244.172_VERB N_NUM /_PUNCT A_CONJ
Mae_VERB hi_PRON ar_ADP fin_NOUN ca'l_VERB canlyniade_NOUN prawf_NOUN DNA_PROPN fydd_AUX yn_PART profi_VERB pa_PART un_NUM o_ADP dri_NUM dyn_NOUN posib_NOUN ydy_VERB 'i_PRON thad_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Ok_INTJ ._PUNCT
Ok_INTJ grêt_INTJ diolch_INTJ ._PUNCT
Iawn_INTJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Ar_ADP y_DET soffa_NOUN yn_PART fan_NOUN 'na_ADV chdi_NOUN ia_ADJ ?_PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
'_PUNCT Sdim_PROPN coasters_NOUN yma_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Nagoes_PROPN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Na_VERB 'th_NOUN o_ADP jyst_ADV 'di_PART bod_AUX yn_PART deimlad_NOUN horrible_VERB drost_NOUN y_DET blynyddoedd_NOUN i_ADP dd'eud_VERB y_DET gwir_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ddim_PART yn_PART gw'bot_NOUN pwy_PRON '_PUNCT lly_X o_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
enwbg_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Tad_NOUN y_DET chwaer_NOUN hynna_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ wedyn_ADV mama_NOUN 'na_ADV enwg_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Tad_X [_PUNCT saib_NOUN ]_PUNCT '_PUNCT ym_ADP mrawd_NOUN a_CONJ chwaer_NOUN lleia'_ADJUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_PART enwg_VERB '_PUNCT dyn_NOUN ni_PRON meddwl_ADV ydy_VERB w'rach_NOUN '_PUNCT yn_PART nhad_NOUN i_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ti_PRON mor_ADV nerfus_ADJ a_CONJ fi_DET t'wel_NOUN '_PUNCT ._PUNCT
Ma_VERB 'n_PART iawn_ADJ na_CONJ dw_AUX i_PRON 'n_PART teimlo_VERB 'n_PART iawn_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP 'n_PART i_ADP me'wl_NOUN '_PUNCT sa_PRON 'n_PART i_PRON ddim_PART pan_CONJ goda_VERB 's_DET i_PART bora_VERB 'ma_ADV den_NOUN ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART nervous_ADJ wrech_VERB bora'_VERBUNCT mama_NOUN nde_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP 'n_PART i_PRON 'n_PART me'wl_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT no_PRON way_NOUN '_PUNCT llai_ADV fynd_VERB fydda'_VERBUNCT i_PRON 'n_PART t'mlo_VERB sal_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Jyst_ADV dim_PRON yn_PART gw'bod_ADJ ._PUNCT
Mae_VERB canlyniad_NOUN y_DET profion_NOUN DNA_PROPN gan_ADP enwb_NOUN cwnselydd_ADJ y_DET gwesty_NOUN ._PUNCT
So_PART do_AUX 's_X 'na_ADV ddim_PART byd_NOUN yn_PART mynd_VERB i_PART disapointio_VERB fi_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Helo_INTJ ._PUNCT
Helo_INTJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Sud_NOUN '_PUNCT dach_VERB chi_PRON 'ch_PRON dwy_NUM ?_PUNCT
'_PUNCT Dan_PROPN ni_PRON 'n_PART ok_X ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Wel_INTJ dw_VERB i_PRON 'n_PART iawn_ADJ on_X '_PUNCT mae_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Dw_VERB [_PUNCT /=_PROPN ]_PUNCT dw_VERB i_ADP me'wl_NOUN bo_VERB fi_PRON mwy_ADV nerfys_NOUN na_CONJ mam_NOUN ._PUNCT
W't_VERB ti_PRON ?_PUNCT
'_PUNCT Dw_VERB [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Pam_ADV de_NOUN ?_PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gw'bod_VERB d'mo_VERB '_PUNCT fi_PRON jyst_ADV yn_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ishe_PROPN neud_VERB o_ADP i_ADP mam_NOUN os_CONJ '_PUNCT dach_VERB chi_PRON 'n_PART d'allt_ADJ ._PUNCT
Isho_NOUN neud_VERB o_ADP i_ADP mam_NOUN ._PUNCT
chicks_ADJ o_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ti_PRON 'di_PART bod_AUX y_DET '_PUNCT chwilio_VERB am_ADP dy_DET dad_NOUN yndo_NOUN am_ADP hir_ADJ ?_PUNCT
Do_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Do_VERB 'n_PART hir_ADJ '_PUNCT wan_ADJ do_PRON ._PUNCT
Ia_INTJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM SB_SYM >_SYM Do_INTJ <_SYM pesychu_VERB >_SYM ._PUNCT
A_CONJ ti_PRON 'di_PART gofyn_VERB i_ADP ni_PRON neud_VERB profion_NOUN DNA_PROPN +_SYM
Do_INTJ ._PUNCT
+_VERB a_CONJ ballu_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ia_INTJ ._PUNCT
Do_INTJ ._PUNCT
'_PUNCT De_NOUN '_PUNCT ni_PRON ga'l_VERB dipyn_NOUN bach_ADJ o_ADP dy_DET hanas_NOUN di_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ia_INTJ ._PUNCT
BeBe_VERB '_PUNCT dach_VERB chi_PRON ish_ADJ '_PUNCT '_PUNCT w'bod_VERB ?_PUNCT
Mam_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Pwy_PRON o'dd_NOUN mam_NOUN ?_PUNCT
enwb_VERB o'dd_NOUN mam_NOUN ._PUNCT
Ia_INTJ ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ sut_ADV hogan_NOUN o_ADP '_PUNCT 'na_ADV ?_PUNCT
O'dd_NOUN enwb_NOUN yn_PART ddynas_NOUN neis_ADJ o'dd_NOUN enwb_NOUN yn_PART [_PUNCT saib_NOUN ]_PUNCT happy_ADJ go_ADV lucky_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Not_VERB a_CONJ care_VERB in_X the_X worlt_X ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Aru_X fi_PRON golli_VERB mam_NOUN pan_CONJ o_ADP 'n_PART i_PRON 'n_PART [_PUNCT anadlu_VERB ]_PUNCT twenty_VERB one_NOUN twenty_NOUN two_ADP ia_PRON ?_PUNCT
O'ddach_NOUN chdi_NOUN 'n_PART byw_VERB efo_ADP enwb_NOUN a_CONJ  _SPACE [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
O_ADP 'n_PART o_PRON 'n_PART i_PRON 'n_PART byw_VERB adra_ADV hefo_ADP enwb_NOUN a_CONJ enwg_VERB ._PUNCT
A_PART enwg_VERB ._PUNCT
Ag_NOUN o_ADP 'n_PART i_PRON 'n_PART meddwl_VERB mama_NOUN enwg_VERB o'dd_NOUN '_PUNCT yn_PART nhad_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Sut_ADV [_PUNCT /=_PROPN ]_PUNCT sut_ADV foi_NOUN o'dd_NOUN enwg_VERB ta_ADJ ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
O'dd_NOUN yn_PART iawn_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O'dd_NOUN o_ADP 'm_DET yn_PART [_PUNCT saib_NOUN ]_PUNCT o'dd_NOUN o_ADP 'm_DET yn_PART berson_NOUN '_PUNCT swn_AUX i_PRON 'n_PART gallu_VERB eistad_NOUN a_CONJ siarad_VERB [_PUNCT saib_NOUN ]_PUNCT d'eud_VERB '_PUNCT yn_PART nheimlada'_NOUNUNCT a_CONJ [_PUNCT saib_NOUN ]_PUNCT petha'_NOUNUNCT felly_ADV '_PUNCT tho_NOUN fo_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Strict_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Oedd_AUX o_PRON 'n_PART ok_ADJ o'dd_NOUN o_ADP reit_NOUN strict_NOUN oedd_VERB ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ fo_PRON '_PUNCT ru_PRON fagu_VERB fi_PRON a_CONJ '_PUNCT dyn_NOUN dat_PRON o_ADP 'n_PART i_PRON 'n_PART galw_VERB fo_PRON de_NOUN ._PUNCT
Ia_INTJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Wetyn_PROPN [_PUNCT saib_NOUN ]_PUNCT '_PUNCT aru_VERB fi_PRON ffeindio_VERB  _SPACE [_PUNCT saib_NOUN ]_PUNCT llyfr_NOUN family_ADJ '_PUNCT lowence_ADJ mam_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ag_NOUN o'dd_NOUN y_DET surname_VERB i_PRON yn_PART wahanol_ADJ ar_ADP y_DET llyfr_NOUN fam'ly_NOUN '_PUNCT lowence_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O'dd_NOUN yr_DET un_NUM un_NUM fath_NOUN a_CONJ un_NUM y_DET chwaer_NOUN hynna_PRON '_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ o_ADP 'n_PART i_PRON 'n_PART [_PUNCT saib_NOUN ]_PUNCT o_ADP 'n_PART i_DET shocked_NOUN '_PUNCT c'os_NOUN o_ADP 'n_PART i_PRON '_PUNCT tha'_NOUNUNCT im_NUM dyna_DET '_PUNCT dy_DET 'n_ADP enw_NOUN i_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Wedyn_X '_PUNCT aru_VERB fi_PRON ofyn_VERB w_ADP 'th_PRON ffrind_NOUN [_PUNCT saib_NOUN ]_PUNCT -_PUNCT d_PRON a_PART d'eud_VERB bebe_NOUN o_ADP 'n_PART i_PRON 'di_PART ffeindio_VERB ag_CONJ '_PUNCT aru_VERB hi_PRON ddeud_VERB adag_ADP '_PUNCT ynny_VERB wel_NOUN [_PUNCT =_SYM ]_PUNCT '_PUNCT im_NUM [_PUNCT /=_PROPN ]_PUNCT '_PUNCT im_PRON enwg_VERB ydy_VERB dy_DET dad_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ac_CONJ o_ADP 'n_PART i_PRON '_PUNCT tha_PART sut_ADV ti_PRON 'n_PART gw'bobo_DET ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ac_CONJ o_ADP '_PUNCT hi_PRON 'di_PART clywad_NOUN 'i_PRON mham_NOUN a_CONJ 'i_PRON thad_NOUN hi_PRON 'n_PART siarad_VERB [_PUNCT saib_NOUN ]_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT enwg_VERB a_CONJ mama_NOUN  _SPACE enwg_VERB o_ADP __NOUN ._PUNCT
689.985_NOUN N_NUM /_PUNCT A_CONJ
Ddyna_VERB o'dd_NOUN yr_DET nodia'_VERBUNCT [_PUNCT saib_NOUN ]_PUNCT yn_PART naturiol_ADJ o'dd_NOUN pobol_ADJ yn_PART marw_VERB 'na_ADV ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM  _SPACE pobol_ADJ yn_PART ca'l_VERB newyddion_NOUN drwg_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT llawer_PRON o_ADP fendio_VERB a_CONJ petha'_NOUNUNCT felly_ADV nhw_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP ydw_VERB i_PRON 'n_PART cofio_VERB ambyty_NOUN 'r_DET '_PUNCT ogan_NOUN bach_ADJ 'na_ADV
[_PUNCT saib_NOUN ]_PUNCT i_PART [_PUNCT saib_NOUN ]_PUNCT [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Pan_CONJ o_ADP 'n_PART i_PRON ar_ADP ward_NOUN y_DET plant_NOUN
<_SYM S_NUM ?_PUNCT >_SYM hogan_NOUN bach_ADJ saith_ADV oed_NOUN o_ADP ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Rwy_AUX 'n_PART gwelt_NOUN 'i_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT '_PUNCT mlaen_NOUN 'na_ADV ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM ym_ADP ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Gor'o_VERB '_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Gen_ADP ti_PRON 'm_DET air_NOUN ambiwlans_ADJ na_CONJ 'm_DET byd_NOUN nag_CONJ oedd_VERB amser_NOUN +_SYM
[_PUNCT =_SYM ]_PUNCT Nagoedd_NOUN [_PUNCT /=_PROPN ]_PUNCT nagoedd_NOUN ._PUNCT
+_VERB yna_ADV ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ mi_PRON o'dd_NOUN lleoliad_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT mynd_VERB a_CONJ hi_PRON [_PUNCT =_SYM ]_PUNCT i_PRON [_PUNCT /=_PROPN ]_PUNCT i_ADP lleoliad_NOUN ._PUNCT
Oedd_VERB ._PUNCT
<_SYM SB_SYM >_SYM Hmm_X ._PUNCT
A_CONJ hithe_VERB mor_ADV wael_ADJ a_CONJ chdi_NOUN ofn_NOUN '_PUNCT wbeth_NOUN ddigwydd_VERB ar_ADP y_DET ffor_NOUN '_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Anyway_NOUN clirodd_VERB '_PUNCT wnna_NOUN yn_ADP pob_DET dim_PRON a_PART trosglwyddio_VERB 'i_PRON drosodd_VERB i_ADP lleoliad_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ [_PUNCT saib_NOUN ]_PUNCT dod_VERB yn_PART ôl_NOUN [_PUNCT saib_NOUN ]_PUNCT a_CONJ yr_DET hogia'_NOUNUNCT ambiwlans_NOUN yn_PART ca'l_VERB message_NOUN i_ADP dd'ed_NOUN de_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Bod_VERB yr_DET '_PUNCT ogan_NOUN bach_ADJ wedi_PART marw_VERB ._PUNCT
A_CONJ ._PUNCT
Yssy_VERB ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yn_PART  _SPACE +_SYM
A_CONJ ._PUNCT
+_VERB yn_PART drist_ADJ ofnadwy_ADJ de_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
'_PUNCT Dan_PROPN ni_PRON 'di_PART -_PUNCT c_PART chwerthin_VERB efo_ADP 'n_PART gilydd_NOUN a_CONJ 'di_PART crio_VERB efo_ADP 'n_PART gilydd_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Wedyn_ADV ydy_VERB mae_AUX 'di_PART bod_VERB yn_PART [_PUNCT saib_NOUN ]_PUNCT mae_VERB 'di_PART bod_VERB yn_PART fraint_NOUN ca'l_VERB bod_VERB yn_PART nyrs_NOUN yn_ADP yr_DET adag_NOUN yna_DET ._PUNCT
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
'_PUNCT Sgin_PROPN i_ADP 'm_DET '_PUNCT im_NOUN llawer_PRON o_ADP deulu_NOUN ._PUNCT
Oes_VERB mama_NOUN genna'_VERBUNCT i_PRON chwaer_NOUN a_CONJ mama_NOUN genna'_VERBUNCT i_PRON -_PUNCT ba_PART brawd_NOUN yng_ADP nghyfra_VERB 'th_NOUN a_CONJ betha_VERB felly_ADV a_CONJ mama_NOUN genna'_VERBUNCT i_PRON ŵr_NOUN da_NOUN ofnadwy_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ [_PUNCT saib_NOUN ]_PUNCT heb_ADP y_DET genod_NOUN '_PUNCT mama_NOUN sa_PRON '_PUNCT i_PRON 'n_PART o_ADV unig_ADJ de_NOUN ?_PUNCT
Fyssa_PROPN ._PUNCT
'_PUNCT Dan_PROPN ni_PRON 'di_PART ryw_ADV gydweithio_VERB felly_ADV '_PUNCT fo_PRON 'n_PART
Do_INTJ ._PUNCT
+_VERB gilydd_NOUN yndo_NOUN ?_PUNCT
A_CONJ 'di_PART ca'l_VERB hwyl_NOUN '_PUNCT o_PRON 'n_PART gilydd_NOUN +_SYM
Do_INTJ ._PUNCT
Do_INTJ ._PUNCT
+_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT weld_VERB o_PRON ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT '_PUNCT sw_PRON i_PRON 'n_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT am_ADP dri_NUM deg_NUM naw_NUM mlynadd_NOUN '_PUNCT swn_NOUN i_ADP 'm_DET yn_PART hapus_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Swn_PROPN i_PART mynd_VERB yn_PART ôl_NOUN '_PUNCT fory_ADV to_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Swn_PROPN i_PRON 'n_PART ca'l_VERB ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ dw_AUX i_PRON 'di_PART mynd_VERB rhy_ADV hen_ADJ rwan_ADV ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Chyma_VERB nhw_PRON m'onna_NOUN fi_PRON ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
<_SYM SB_SYM >_SYM BeBe_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
<_SYM SB_SYM >_SYM '_PUNCT Yt_PART ti_PRON siŵr_ADJ ?_PUNCT
<_SYM SB_SYM >_SYM O_ADP ia_PRON ._PUNCT
<_SYM SB_SYM >_SYM Oes_VERB ._PUNCT
Prizegiving_PROPN ._PUNCT
<_SYM SB_SYM >_SYM O_ADP -_PUNCT i_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Pwy_PRON t_VERB shirt_NOUN sydd_VERB 'na_ADV ?_PUNCT
<_SYM SB_SYM >_SYM Tissue_PROPN ._PUNCT
I_ADP chwythu_VERB dy_DET drwyn_NOUN ?_PUNCT
<_SYM SB_SYM >_SYM Nage_X ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Cyn_CONJ troi_VERB am_ADP adra_ADV mama_NOUN enwb_NOUN 'n_PART mynd_VERB a_CONJ souvenir_NOUN bach_ADJ efo_ADP hi_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM I_ADP gofio_VERB am_ADP ei_DET diwrnod_NOUN arbennig_ADJ hefo_ADP 'i_PRON ffrindie_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM SB_SYM >_SYM P'un_NOUN ti'sho_NOUN ?_PUNCT
[_PUNCT Sgwrs_PROPN yn_PART rhy_ADV dawel_ADP i_ADP 'w_PRON chlywed_VERB ]_PUNCT ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Hengen_NOUN o_ADP adran_NOUN '_PUNCT dan_ADP ni_PRON a_CONJ hengen_NOUN o_ADP adran_NOUN fyddan_VERB ni_PRON ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Chi_PRON siŵr_ADV '_PUNCT dach_VERB chi_PRON 'di_PART gorffan_NOUN r'an_PUNCT ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Do_INTJ diolch_NOUN boi_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM SB_X >_SYM That_X meal_NOUN was_NOUN very_VERB nice_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
O'dd_NOUN o_PRON 'n_PART neis_ADJ iawn_ADV ?_PUNCT
O_ADP da_ADJ iawn_ADV ._PUNCT
O_ADP 'n_PART i_PRON am_PART ofyn_VERB '_PUNCT thoch_VERB chi_PRON ._PUNCT
So_PART chi_PRON 'di_PART joio_NOUN ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Draw_ADV yn_ADP y_DET llyfrgell_NOUN mae_AUX enwb_NOUN yn_PART aros_VERB i_ADP gl'wed_VERB canlyniade_NOUN profion_NOUN DNA_PROPN gan_ADP enwb_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dd'eudodd_VERB mam_NOUN enwb_NOUN erioed_ADV wrthi_ADP pwy_PRON oedd_VERB 'i_PRON thad_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ai_PART enwbg_NOUN gŵr_NOUN cynta_ADJ 'i_PRON mam_NOUN ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
enwg_VERB yr_DET un_NUM fagodd_VERB enwb_NOUN ers_ADP yn_PART fabi_NOUN ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Neu_CONJ dyn_NOUN o_ADP 'r_DET enw_NOUN enwg_VERB o_ADP 'r_DET lleoliad_NOUN ddaeth_VERB i_ADP Gymru_PROPN i_ADP weithio_VERB am_ADP gyfnod_NOUN ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dim_PRON ond_ADP prawf_NOUN DNA_PROPN all_ADP brofi_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Gan_CONJ fod_VERB y_DET tri_NUM bellach_ADV wedi_PART marw_VERB ._PUNCT
Prawf_NOUN DNA_PROPN enwbg_NOUN gynta'_ADJUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Achos_CONJ ddaru_VERB ni_PRON neud_VERB y_DET DNA_NOUN 'di_PART ga'l_VERB hwn_PRON yn_PART iawn_ADJ rwan_ADV ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Efo_NOUN dy_DET chwaer_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Reit_X ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM I_NUM weld_VERB os_CONJ ydy_AUX enwbg_NOUN yn_PART gallu_VERB bod_VERB yn_PART dad_NOUN i_ADP ti_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X 'n_PART glir_ADJ bobo_VERB chi_PRON 'n_PART rhannu_VERB 'r_DET un_NUM fam_NOUN ._PUNCT
1150.116_NOUN N_NUM /_PUNCT A_CONJ
Bod_VERB 'i_PRON dim_DET yma_ADV ddim_PART mwy_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM So_NOUN oedd_VERB lot_PRON o_ADP pen_NOUN fi_PRON yn_PART mynd_VERB o_ADP dw_AUX i_PART mynd_VERB i_PART neud_VERB hyn_PRON dw_AUX i_PART mynd_VERB i_PART neud_VERB hynna_PRON efo_ADP mam_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ dw_AUX i_ADP actually_VERB ddim_PART ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Mae_AUX 'r_DET gwesty_NOUN wedi_PART dod_VERB o_ADP hyd_NOUN i_ADP fwy_PRON o_ADP wybodaeth_NOUN i_ADP enwb_VERB am_ADP 'i_PRON theulu_NOUN gwaed_NOUN yn_PART lleoliad_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ 'r_DET tro_NOUN yma_DET dydy_AUX o_ADP ddim_PART yn_PART newyddion_NOUN drwg_ADJ i_ADP gyd_ADP ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Criw_NOUN sydd_AUX wedi_PART rhoi_VERB 'u_PRON hunan_VERB ar_ADP y_DET lein_NOUN yn_PART llythrennol_ADJ ydy_VERB 'r_DET criw_NOUN nesa'_ADJUNCT i_PRON gyrra'dd_NOUN y_DET gwesty_NOUN ._PUNCT
Pan_CONJ fydd_VERB pasio_VERB willi_VERB nilly_X [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT gwaith_NOUN ._PUNCT
W't_VERB ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Fe_PART fuodd_VERB y_DET pump_NUM ffrind_NOUN yma_DET yn_PART sefyll_VERB ar_ADP lein_NOUN biced_NOUN tu_NOUN allan_ADV i_ADP ffatri_NOUN ffatri_NOUN yng_ADP lleoliad_NOUN am_ADP dair_NUM blynedd_NOUN anodd_ADJ iawn_ADV ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mae_VERB enwg_VERB enwg_VERB __NOUN __NOUN __NOUN __NOUN ac_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN wedi_PART dod_VERB i_ADP 'r_DET gwesty_NOUN heddiw_ADV i_PART nodi_VERB fo_PRON '_PUNCT 'na_ADV bron_ADV i_ADP bymtheg_ADJ mlynedd_NOUN ers_ADP diwedd_NOUN y_DET streic_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Fatri_NOUN o'dd_NOUN yn_PART mwy_ADV fath_NOUN '_PUNCT a_CONJ cymuned_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yn_ADP y_DET -_PUNCT d_ADP  _SPACE dechreua'_VERBUNCT duon_VERB -_PUNCT o_ADP o_ADP 'r_DET hanesion_NOUN clywes_VERB i_ADP gin_NOUN yr_DET hen_ADJ hogia'_NOUNUNCT de_NOUN yn_ADP bob_DET amsar_NOUN cinio_NOUN a_CONJ -_PUNCT ch_PART  _SPACE cinio_NOUN bach_ADJ sydyn_ADJ a_CONJ Eisteddfod_PROPN wedyn_ADV ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP '_PUNCT 'na_ADV canu_VERB a_CONJ barddoni_VERB a_CONJ bob_DET peth_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM I_NUM -_PUNCT b_ADP  _SPACE hogyn_NOUN ifanc_ADJ fath_NOUN a_CONJ fi_PRON  _SPACE o'dd_NOUN '_PUNCT mond_ADV 'di_PART byw_VERB yn_PART lleoliad_NOUN  _SPACE -_PUNCT yd_NOUN i_PRON dod_VERB i_ADP mewn_ADP i_ADP 'r_DET cymuned_NOUN yna_ADV [_PUNCT saib_NOUN ]_PUNCT -_PUNCT o_ADP o'dd_NOUN o_ADP 'n_PART fantastic_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM  _SPACE -_PUNCT o_ADP o'dd_NOUN o_PRON 'n_PART cymuned_VERB hollol_ADV wahanol_ADJ ._PUNCT
Roedd_VERB ffatri_NOUN ffatri_NOUN yn_PART gyflogwr_NOUN mawr_ADJ a_CONJ phwysig_ADJ yn_PART ardal_NOUN lleoliad_NOUN ers_ADP mil_NUM naw_NUM chwech_NUM pedwar_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ ym_ADP mil_NUM naw_NUM naw_NUM saith_NUM fe_PART gafodd_VERB y_DET ffatri_NOUN 'i_PRON brynnu_VERB gan_ADP ethnonym_NOUN o_ADP 'r_DET enw_NOUN enwg_VERB __NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Newidiodd_VERB yr_DET enw_NOUN o_ADP ffatri_NOUN i_ADP ffatri_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ thrio_ADV mynd_VERB ati_ADP i_PART newid_VERB yr_DET amodau_NOUN gwaith_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ cha'l_DET gwared_NOUN o_ADP 'r_DET undeb_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM SG_VERB >_SYM [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
<_SYM SG_VERB >_SYM [_PUNCT =_SYM ]_PUNCT Ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ie_INTJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
No_NOUN branch_ADV meetings_NOUN to_NOUN be_ADP held_VERB on_X site_NOUN ._PUNCT
Other_VERB unions_VERB could_VERB <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
1710.104_NUM N_NUM /_PUNCT A_CONJ
So_PART mama_NOUN 'di_PART dod_VERB fel_ADP sypreis_NOUN iddyn_ADP nhw_PRON hefyd_ADV ._PUNCT
O_ADP do_PRON [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Do_INTJ ._PUNCT
Iawn_INTJ ?_PUNCT
Hmm_INTJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Hmm_INTJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
BeBe_ADV sy_VERB 'n_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT am_ADP llawer_PRON o_ADP neb_PRON yn_PART gw'bod_VERB [_PUNCT saib_NOUN ]_PUNCT bobo_VERB ti_PRON 'di_PART ca'l_VERB taid_NOUN '_PUNCT falle_NOUN nagoedd_NOUN ?_PUNCT
Na_INTJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
A_CONJ mi_PART ddaru_VERB enwb_NOUN sôn_VERB wrth_ADP y_DET teulu_NOUN yn_PART lleoliad_NOUN amdanach_ADJ chdi_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X '_PUNCT dy_DET holl_DET deulu_NOUN di_PRON draw_ADV fan_NOUN 'na_ADV wedi_PART dotio_VERB bobo_VERB chdi_NOUN 'di_PART ca'l_VERB dy_DET ffeindio_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X '_PUNCT nhw_PRON 'n_PART thrilled_ADJ ._PUNCT
O_ADP ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Ydyn_VERB ._PUNCT
-_PUNCT T_NUM [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ma'_VERBUNCT nhw_PRON wrth_ADP 'u_PRON bodd_NOUN bobo_VERB chdi_NOUN 'ma_ADV ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ bobo_VERB chdi_NOUN 'di_PART bod_AUX 'n_PART yn_PART chwilio_VERB amdanyn_ADP nhw_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ mama_VERB nhw_PRON isho_NOUN dy_DET nabod_VERB di_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X '_PUNCT nhw_PRON isho_NOUN [_PUNCT saib_NOUN ]_PUNCT isho_NOUN dy_DET groesawi_VERB di_PRON i_ADP fewn_ADP i_ADP 'w_PRON teulu_NOUN nhw_PRON ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Ma_VERB 'n_PART lot_ADV tydy_VERB ?_PUNCT
I_ADP ca'_NOUNUNCT mew_NOUN '_PUNCT ie_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ti_PRON 'n_PART ok_X ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ti_PRON 'n_PART iawn_ADJ ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ia_INTJ ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
Wel_INTJ mama_VERB nhw_PRON 'n_PART deulu_NOUN mawr_ADJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O'dd_NOUN gan_ADP dy_DET fam_NOUN bedair_NOUN chwaer_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ mama_AUX nhw_PRON wedi_PART [_PUNCT saib_NOUN ]_PUNCT dafon_VERB llun_NOUN i_ADP chdi_NOUN o_ADP dy_DET fam_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Ia_INTJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Dyma_DET dy_DET fam_NOUN ._PUNCT
Diolch_INTJ [_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Ma_VERB 'r_DET gêg_NOUN yna_DET ._PUNCT
2178.015_NOUN N_NUM /_PUNCT A_CONJ
Mae_AUX wedi_PART bod_VERB yn_PART b'nawn_NOUN emosiynol_ADJ i_ADP enwb_VERB yn_ADP y_DET gwesty_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Am_ADP y_DET tro_NOUN cynta'_ADJUNCT mae_AUX wedi_PART gweld_VERB llun_NOUN o_ADP 'i_PRON mam_NOUN waed_NOUN enwb_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ 'i_PRON hanner_NOUN chwaer_NOUN enwb_NOUN o_ADP lleoliad_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ rwan_ADV mae_VERB gan_ADP enwb_NOUN fwy_PRON o_ADP newyddion_NOUN i_ADP 'w_PRON rannu_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Ma'_VERBUNCT enwb_NOUN [_PUNCT saib_NOUN ]_PUNCT wedi_PART danfon_VERB negas_ADJ atat_ADJ ti_PRON ._PUNCT
O_ADP ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
A_PART mae_VERB 'di_PART sgwnnu_VERB fo_PRON yn_PART Sbaeneg_PROPN achos_CONJ dyw_AUX 'i_PRON 'm_DET yn_PART siarad_VERB Sysnag_PROPN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_X '_PUNCT dyn_NOUN ni_PRON 'di_PART ca'l_VERB
<_SYM S_NUM ?_PUNCT >_DET rywun_NOUN i_ADP gyfieithu_VERB fo_PRON [_PUNCT saib_NOUN ]_PUNCT i_ADP ni_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Fysat_VERB ti_PRON licio_VERB fi_PRON ddarlan_VERB o_ADP i_ADP chdi_NOUN ?_PUNCT
Ie_INTJ ._PUNCT
Ie_INTJ ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT Ok_X ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM "_PUNCT Helo_INTJ ._PUNCT +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB Fi_PRON yw_VERB enwb10_NOUN cyfenw4_PROPN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Merch_NOUN enwb_NOUN cyfenw_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dw_AUX i_PRON 'n_PART byw_VERB yn_ADP ninas_NOUN lleoliad_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ac_CONJ mae_VERB gen_ADP i_ADP ferch_NOUN fach_ADJ chwech_NUM oed_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Galla_X '_PUNCT i_PRON ddychmygu_VERB pa_ADV mor_ADV anodd_ADJ mae_AUX wedi_PART bod_VERB i_ADP ti_PRON chwilio_VERB am_ADP mam_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ wedyn_ADV clywad_NOUN bod_AUX hi_PRON 'di_PART marw_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Bu_VERB mam_NOUN marw_VERB yn_PART dri_NUM deg_NUM pedwar_NUM oed_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mewn_ADP +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB damwain_NOUN ffordd_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ar_ADP daith_NOUN +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB o'ddan_ADP ni_PRON gyd_NOUN yn_PART 'i_PRON neud_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mi_PRON o'dd_VERB 'i_PRON marwolaeth_NOUN hi_PRON yn_PART golled_VERB fawr_ADJ iawn_ADV ac_CONJ yn_PART rywbath_NOUN poenus_ADJ ofnadwy_ADJ i_ADP 'r_DET teulu_NOUN cyfan_ADJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Fuodd_VERB 'i_PRON 'n_PART ddynas_NOUN arbennig_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ro'dd_NOUN 'i_PRON 'n_PART gw_NOUN '_PUNCT ithio_VERB 'n_PART galed_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yn_PART licio_ADV breuddwydio_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Roedd_VERB 'i_PRON wastad_ADV yn_PART hapus_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ac_CONJ efo_ADP gwên_NOUN ar_ADP 'i_PRON gwynab_NOUN drwy_ADP 'r_DET +_PROPN
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB amser_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Roedd_VERB 'i_PRON 'n_PART hoff_ADJ iawn_ADV o_ADP ddownsio_VERB "_PUNCT ._PUNCT
O_ADP ._PUNCT
"_PUNCT Wrth_ADP 'i_PRON bodd_NOUN yn_PART darllen_VERB ac_CONJ yn_PART licio_VERB cerddori'eth_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_DET Gwraig_NOUN syml_ADJ +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB o'dd_NOUN hi_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ rhywun_NOUN efo_ADP calon_NOUN +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB fawr_ADJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X '_PUNCT 'na_ADV gym'int_ADV o_ADP betha'_VERBUNCT amdani_ADP y_DET dylsa'_NOUNUNCT +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB ti_PRON ca'l_VERB gw'bod_VERB amdanyn_ADP nhw_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ buan_ADJ gawn_VERB ni_PRON gyfla_VERB i_ADP siarad_VERB amdani_ADP efo_ADP 'n_PART +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB gilydd_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ dw_AUX i_PRON 'n_PART edrych_VERB ymlaen_ADV +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_VERB cymaint_ADV ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Fedra_X '_PUNCT i_PRON 'm_DET disgw'l_VERB i_ADP ga'l_VERB dy_DET gyfarfod_NOUN di_PRON "_PUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
2570.021_NOUN N_NUM /_PUNCT A_CONJ
2859.989_VERB
