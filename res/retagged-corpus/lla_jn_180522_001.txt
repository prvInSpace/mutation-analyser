[_PUNCT ocheneidio_VERB ]_PUNCT ._PUNCT
BeBe_AUX ti_PRON 'di_PART bod_AUX yn_PART neud_VERB heddiw_ADV '_PUNCT dê_X ?_PUNCT
Wel_INTJ '_PUNCT nes_CONJ i_ADP dechre_ADV bant_ADV trw_NOUN '_PUNCT adolygu_VERB ar_ADP gyfer_NOUN arholiad_NOUN fi_PRON sy_VERB dydd_NOUN Iau_ADJ ._PUNCT
A_PART wedyn_ADV es_VERB i_ADP mas_NOUN â_ADP enwg_VERB a_CONJ es_VERB i_ADP lan_ADV i_ADP 'r_DET lleoliad_NOUN at_ADP nain_NOUN a_CONJ wedyn_ADV i_PART casglu_VERB __NOUN ._PUNCT
Wedyn_ADV de_NOUN 's_DET i_PRON gartre_ADV â_ADP enwg_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ti_PRON 'di_PART blaenio_VERB adolygu_VERB ?_PUNCT
Wel_INTJ '_PUNCT dw_AUX i_PRON 'di_PART hela_VERB rhan_NOUN fwya_ADJ 'r_DET dydd_NOUN yn_PART adolygu_VERB ._PUNCT
O_ADP wyt_VERB ti_PRON ?_PUNCT
Pryd_ADV ti_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT ti_PRON 'n_PART blaenio_VERB adolygu_VERB '_PUNCT nawr_ADV ar_ADP gyfer_NOUN dydd_NOUN Iau_ADJ ?_PUNCT
Ydw_INTJ ._PUNCT
Ond_CONJ mama_NOUN dal_ADV lot_PRON o_ADP gwaith_NOUN '_PUNCT da_ADJ fi_PRON neud_VERB i_ADP ail_ADJ arholiad_NOUN fi_PRON sy_VERB dydd_NOUN Mawrth_PROPN nesa'_ADJUNCT ._PUNCT
Mm_INTJ ._PUNCT
Pryd_ADV ti_PRON 'n_PART mynd_VERB i_PART neud_VERB hwnna_PRON '_PUNCT ta_X ?_PUNCT
Ie_INTJ wel_NOUN [_PUNCT -_PUNCT ]_PUNCT '_PUNCT dw_VERB i_ADP 'm_DET yn_PART siŵr_ADJ ._PUNCT
Mm_INTJ ._PUNCT
Bydd_VERB rhaid_VERB ffendio_VERB amser_NOUN ._PUNCT
Bydd_VERB ._PUNCT
'_PUNCT Fory_X ?_PUNCT
Ie_INTJ ._PUNCT
Siŵr_ADV o_ADP fod_VERB ._PUNCT
A_CONJ wedyn_ADV '_PUNCT da_ADJ fi_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Wel_INTJ ie_INTJ ._PUNCT
Dydd_NOUN Iau_ADJ ._PUNCT
Yn_ADP y_DET bore_NOUN mae_VERB e_PRON '_PUNCT ?_PUNCT
Yn_ADP y_DET bore_NOUN ._PUNCT
So_PART mae_VERB 'n_PART good_VERB ._PUNCT
'_PUNCT Alla_X '_PUNCT i_PART adolygu_VERB ar_ADP ôl_NOUN yr_DET arholiad_NOUN ._PUNCT
A_CONJ dy_PRON '_PUNCT Gwener_PROPN ?_PUNCT
Ie_INTJ ._PUNCT
A_CONJ wedyn_ADV ti_PRON 'n_PART mynd_VERB i_ADP 'r_DET peth_NOUN 'na_ADV dydd_NOUN Sadwrn_PROPN a_CONJ dydd_NOUN Sul_PROPN ?_PUNCT
Ie_INTJ enw_NOUN yn_PART lleoliad_NOUN ._PUNCT
Penwythnos_NOUN mwya'_ADJUNCT yn_PART lleoliad_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Ma'_VERBUNCT nhw_PRON rhoi_VERB ticedi_VERB bant_ADV o_ADP raglenni_NOUN enwg_VERB cyfenw_NOUN ._PUNCT
Do_VERB clywes_VERB i_ADP hwnna_PRON pryd_ADV o_ADP 'n_PART i_PRON '_PUNCT da_ADJ nain_PROPN ._PUNCT
Dau_NUM docyn_NOUN bob_DET dydd_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Ti_PRON 'n_PART ennill_VERB [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
Dim_PRON ond_ADP cael_VERB chwech_NUM peth_NOUN yn_PART iawn_ADJ oedd_VERB raid_VERB ti_PRON neud_VERB ?_PUNCT
Ie_INTJ fi_PRON 'n_PART meddwl_VERB ._PUNCT
Clywes_VERB i_ADP hi_PRON 'n_PART dweud_VERB ._PUNCT
So_PART ti_PRON 'n_PART cael_VERB ticedi_VERB [_PUNCT -_PUNCT ]_PUNCT tocyn_NOUN i_ADP dydd_NOUN Sadwrn_PROPN a_CONJ dydd_NOUN Sul_PROPN ._PUNCT
Mm_INTJ ._PUNCT
Dau_NUM docyn_NOUN ._PUNCT
Ââ_INTJ ._PUNCT
Mae_AUX hwnna_PRON 'n_PART good_VERB ._PUNCT
Ââ_INTJ ._PUNCT
Tales_VERB i_ADP dros_ADP pumdeg_NOUN punt_NOUN am_ADP y_DET dou_NOUN docyn_NOUN ._PUNCT
Do_INTJ ?_PUNCT
Mm_INTJ ._PUNCT
Dau_NUM docyn_NOUN ar_ADP wahân_NOUN yw_VERB nhw_PRON ?_PUNCT
Mm_CONJ yw_VERB e_PRON '_PUNCT ._PUNCT
Pwy_PRON sy_AUX 'n_PART mynd_VERB '_PUNCT dê_NOUN ?_PUNCT
fi_PRON enwb_VERB enwb_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
A_CONJ mama_NOUN dwy_NUM ffrind_NOUN enwb_NOUN o_ADP 'r_DET gogledd_NOUN yn_PART dod_VERB lawr_ADV ._PUNCT
So_PART ga'thon_VERB ni_PRON wyth_NUM diced_NOUN +_SYM
O_ADP neis_ADJ ._PUNCT
+_VERB i_ADP 'r_DET ddydd_NOUN Sadwrn_PROPN a_CONJ '_PUNCT dan_ADP ni_PRON pedwar_NOUN tocyn_NOUN i_ADP 'r_DET dydd_NOUN Sul_PROPN ._PUNCT
So_PART bebe_NOUN [_PUNCT -_PUNCT ]_PUNCT dim_PRON ond_ADP merched_NOUN sy_AUX 'n_PART mynd_VERB dy_DET '_PUNCT Sul_PROPN ?_PUNCT
Ie_INTJ ._PUNCT
Fi_PRON enwb_NOUN enwb_NOUN a_CONJ __NOUN yn_PART mynd_VERB dy_DET '_PUNCT Sul_PROPN ._PUNCT
A_CONJ '_PUNCT dyn_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ma'_VERBUNCT rhai_DET bandie_NOUN Cymraeg_PROPN 'na_ADV ?_PUNCT
Ie_INTJ enw_NOUN a_CONJ enw_NOUN ._PUNCT
A_CONJ enw_NOUN ._PUNCT
enw_NOUN ._PUNCT
Ie_INTJ ._PUNCT
'_PUNCT Naethon_PROPN ni_PRON 'm_DET clywed_VERB bod_VERB nhw_PRON 'na_ADV nes_ADP  _SPACE [_PUNCT -_PUNCT ]_PUNCT i_ADP fi_PRON gwrando_VERB ar_ADP y_DET radio_NOUN ddoe_ADV ._PUNCT
Beth_PRON y_DET 'ch_PRON chi_PRON 'n_PART mynd_VERB [_PUNCT -_PUNCT ]_PUNCT pwy_PRON sy_AUX 'n_PART adwaen_VERB ?_PUNCT
Pwy_PRON sy_VERB 'n_PART [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
enwg_VERB cyfenw_NOUN ._PUNCT
Ââ_INTJ ._PUNCT
Yndefe_INTJ ?_PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT
Yn_PART lleoliad_NOUN ._PUNCT
'_PUNCT Naethon_PROPN ni_PRON fynd_VERB lan_ADV i_PART weld_VERB enw_NOUN ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
So_PART mas_NOUN tu_NOUN fas_ADJ mae_VERB e_PRON '_PUNCT ?_PUNCT
Ie_INTJ ._PUNCT
Ond_CONJ mae_AUX 'n_PART gweud_VERB fod_VERB mellt_NOUN ._PUNCT
Mellt_PROPN a_CONJ th'ranne_NOUN ?_PUNCT
Ie_INTJ ._PUNCT
Mae_AUX 'n_PART gweud_VERB dydd_NOUN Sadwrn_PROPN a_CONJ dydd_NOUN Sul_PROPN a_CONJ dydd_NOUN Llun_PROPN a_CONJ dydd_NOUN Mawrth_PROPN ._PUNCT
Ôô_NOUN ._PUNCT
Mae_VERB mellt_NOUN a_CONJ th'ranne_NOUN apparently_ADJ ._PUNCT
Oedd_AUX yr_DET ap_NOUN yn_PART dweud_VERB ar_ADP ffôn_NOUN fi_PRON ._PUNCT
O_ADP grêt_NOUN ._PUNCT
Ie_INTJ ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART gobeithio_VERB fe_PART aros_VERB yn_PART neis_ADJ fel_ADP mama_NOUN fe_PRON heddi_NOUN ond_CONJ na_PRON ._PUNCT
Faint_ADV o_ADP bobl_NOUN sy_AUX 'n_PART trio_ADV mynd_VERB i_ADP fan_NOUN 'na_ADV '_PUNCT dê_NOUN ?_PUNCT
Sawl_NOUN miloedd_NOUN 'w_PRON i_PRON 'n_PART meddwl_VERB ._PUNCT
Oes_VERB e_PRON '_PUNCT ?_PUNCT
Mae_AUX 'r_DET academi_NOUN yn_PART gwerthu_VERB miloedd_NOUN o_ADP docynne_NOUN ._PUNCT
Dim_PRON ond_ADP trwy_ADP 'r_DET dydd_NOUN fydd_AUX e_PRON '_PUNCT '_PUNCT dê_X ?_PUNCT
Ie_INTJ ._PUNCT
Yn_PART dechre_ADJ am_ADP deuddeg_NOUN a_CONJ wedyn_ADV mynd_VERB '_PUNCT mla_NOUN 'n_PART tan_ADP nos_NOUN ._PUNCT
Ond_CONJ enwg_VERB cyfenw_NOUN yw_VERB yr_DET act_NOUN gynta'_ADJUNCT ar_ADP y_DET dydd_NOUN Sadwrn_PROPN am_ADP deuddeg_NOUN o_ADP 'r_DET gloch_NOUN +_SYM
Ââ_VERB reit_INTJ ?_PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
+_VERB achos_CONJ mae_VERB e_PRON '_PUNCT on_X tour_NOUN ._PUNCT
Ââ_VERB reit_INTJ ._PUNCT
So_PART ma_AUX fe_PRON 'n_PART gorfod_ADV mynd_VERB fel_ADP i_ADP lleoliad_NOUN neu_CONJ rhywle_ADV yn_ADP y_DET nos_NOUN ._PUNCT
Ââ_VERB reit_INTJ ._PUNCT
'_PUNCT Chos_PROPN mae_VERB sioe_NOUN yn_ADP y_DET nos_NOUN '_PUNCT fyd_NOUN ._PUNCT
So_PART fydd_VERB raid_VERB chi_PRON fod_VERB 'na_ADV erbyn_ADP deuddeg_NOUN i_PART weld_VERB enwg_VERB cyfenw_NOUN ._PUNCT
enwg_VERB cyfenw_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Yn_PART dda_ADJ mai_DET ystod_NOUN dydd_NOUN yw_VERB e_PRON '_PUNCT ._PUNCT
Y_DET 'ch_PRON chi_PRON 'n_PART byw_VERB digon_ADV agos_ADJ ._PUNCT
Ie_INTJ ._PUNCT
'_PUNCT Na_INTJ bebe_NOUN sy_AUX 'n_PART good_VERB ._PUNCT
Ie_INTJ ._PUNCT
So_INTJ '_PUNCT ellwch_VERB chi_PRON fynd_VERB 'n_PART ôl_NOUN a_CONJ '_PUNCT mla_NOUN 'n_PART ._PUNCT
Ie_INTJ ._PUNCT
Ti_PRON 'n_PART iawn_ADJ ._PUNCT
Mm_INTJ ._PUNCT
Da_ADJ iawn_ADV ._PUNCT
So_PART fyddai_AUX 'm_DET yn_PART gweld_VERB ti_PRON '_PUNCT nawr_ADV '_PUNCT dê_NOUN tan_ADP ar_ADP ôl_NOUN arholiade_VERB ti_PRON ?_PUNCT
Ie_INTJ ._PUNCT
Wel_INTJ [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ie_INTJ ._PUNCT
So_NOUN wedyn_ADV mae_VERB e_PRON 'n_PART '_PUNCT steddfod_NOUN yw_VERB e_PRON '_PUNCT ?_PUNCT
Ie_INTJ ._PUNCT
Dydd_NOUN Mercher_PROPN yw_VERB e_PRON '_PUNCT ._PUNCT
Ie_INTJ ._PUNCT
So_PART ti_PRON 'n_PART mynd_VERB gartre_ADV '_PUNCT nos_NOUN Fawrth_NOUN neu_CONJ +_SYM
Ie_INTJ ._PUNCT
'_PUNCT Sa_NOUN i_PRON 'n_PART siŵr_ADJ achos_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
+_VERB na_INTJ '_PUNCT i_PART pigo_VERB ti_PRON lan_NOUN o_ADP lleoliad_NOUN dydd_NOUN Mercher_PROPN ?_PUNCT
Ie_INTJ ._PUNCT
'_PUNCT Sa_NOUN i_PRON 'n_PART siŵr_ADJ achos_CONJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB wythnos_NOUN nesa'_ADJUNCT mae_VERB 'r_DET refferendwm_NOUN 'ma_ADV 'n_PART digwydd_VERB +_SYM
O_ADP ie_INTJ ._PUNCT
+_VERB yn_PART Prifysgol_NOUN lleoliad_NOUN ._PUNCT
Wel_CONJ ar_ADP y_DET nos_NOUN Fawrth_NOUN mama_NOUN fel_ADP [_PUNCT -_PUNCT ]_PUNCT y_DET noswaith_NOUN 'ma_ADV fel_ADP yn_PART [_PUNCT aneglur_ADJ ]_PUNCT [_PUNCT =_SYM ]_PUNCT yn_ADP y_DET bar_NOUN [_PUNCT /=_PROPN ]_PUNCT yn_ADP y_DET bar_NOUN yn_ADP yr_DET Undeb_NOUN ._PUNCT
Fel_CONJ bod_AUX pobl_NOUN yn_PART credori_VERB '_PUNCT fe_PRON [_PUNCT -_PUNCT ]_PUNCT yn_PART gofyn_VERB cwestiyne_NOUN ._PUNCT
O_ADP reit_NOUN ._PUNCT
A_CONJ '_PUNCT falle_NOUN '_PUNCT bydd_VERB rhaid_VERB i_ADP mi_PRON fod_VERB 'na_ADV fel_ADP un_NUM o_ADP 'r_DET pobl_NOUN sy_AUX 'n_PART siarad_VERB ._PUNCT
Ie_INTJ ._PUNCT
Wrth_ADP gwrs_NOUN ._PUNCT
So_INTJ '_PUNCT falle_ADV mama_NOUN hwnna_PRON am_ADP hanner_NOUN 'di_PART pedwar_NOUN tan_ADP chwech_NUM ._PUNCT
Dim_DET ots_NOUN ._PUNCT
'_PUNCT Chos_NOUN '_PUNCT sa_PRON i_PRON 'n_PART really_ADV siŵr_ADJ bebe_AUX ni_PRON 'n_PART neud_VERB to_NOUN ._PUNCT
So_PART pryd_ADV mae_VERB arholiad_NOUN dydd_NOUN Mawrth_PROPN ?_PUNCT
Yn_ADP y_DET bore_NOUN ?_PUNCT
Yn_ADP y_DET bore_NOUN ._PUNCT
Pryd_ADV ma_VERB 'r_DET refferendwm_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ?_PUNCT
Dydd_NOUN Llun_PROPN nesa'_ADJUNCT ._PUNCT
So_INTJ ie_INTJ ._PUNCT
Dydd_NOUN Llun_PROPN nesa_ADJ 'ma_ADV 'r_DET fel_NOUN d'wrnod_PROPN o_ADP [_PUNCT -_PUNCT ]_PUNCT fel_ADP ymgyrchu_VERB ._PUNCT
A_CONJ wedyn_ADV o_ADP dydd_NOUN Mawrth_PROPN tan_ADP dydd_NOUN Gwener_PROPN mama_NOUN pobl_NOUN yn_PART gallu_ADV pleidleisio_VERB ._PUNCT
A_PART wedyn_ADV nos_NOUN Wener_NOUN mae_VERB 'r_DET canlyniad_NOUN ._PUNCT
Ond_CONJ byddai_VERB 'n_PART lleoliad_NOUN ar_ADP dydd_NOUN Gwener_PROPN ._PUNCT
Ydi_NOUN enwg_VERB yn_PART mynd_VERB gyda_ADP ti_PRON i_ADP Llundain_PROPN yw_VERB e_PRON '_PUNCT ?_PUNCT
Wel_INTJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB bod_VERB e_PRON '_PUNCT ._PUNCT
Ond_CONJ '_PUNCT sa_PRON i_PRON 'di_PART really_ADV ca'l_NOUN straight_ADJ answer_VERB to_NOUN ._PUNCT
So_PART bydd_VERB rhaid_VERB fi_PRON holi_VERB fe_PRON to_NOUN ._PUNCT
So_ADP ti_PRON 'di_PART  _SPACE bwcio_VERB 'r_DET bus_NOUN wyt_AUX ti_PRON '_PUNCT ê_PRON ?_PUNCT
Na_VERB ddim_PART to_ADJ ._PUNCT
Na_INTJ ._PUNCT
Fi_PRON 'n_PART aros_VERB i_ADP ffendio_VERB mas_NOUN wrth_ADP enwg_VERB gynta'_ADJUNCT ._PUNCT
Ond_CONJ [_PUNCT aneglur_ADJ ]_PUNCT yn_PART '_PUNCT ista_NOUN [_PUNCT -_PUNCT ]_PUNCT gwatsiad_NOUN y_DET ffôn_NOUN ._PUNCT
Na_INTJ [_PUNCT aneglur_ADJ ]_PUNCT nos_NOUN Sadwrn_PROPN ._PUNCT
Ââ_VERB na_INTJ '_PUNCT '_PUNCT dyn_NOUN ?_PUNCT
Na_INTJ ._PUNCT
So_PART meddwl_ADV mynd_VERB lan_ADV a_CONJ dal_ADV y_DET bus_VERB ?_PUNCT
Ie_INTJ ._PUNCT
Daeth_VERB neges_NOUN wrth_ADP enwb_VERB cyfenw_NOUN i_PART ofyn_VERB o_ADP 'n_PART i_PRON eisie_VERB tocyn_NOUN ._PUNCT
Reit_INTJ ._PUNCT
'_PUNCT Wedes_NOUN i_PRON "_PUNCT na_INTJ "_PUNCT [_PUNCT -_PUNCT ]_PUNCT bod_AUX fi_PRON ddim_PART yn_PART mynd_VERB i_PART fynd_VERB ar_ADP y_DET dydd_NOUN Sadwrn_PROPN ._PUNCT
Ââ_VERB okay_VERB ._PUNCT
Achos_CONJ '_PUNCT swn_NOUN i_ADP 'm_DET mynd_VERB lan_ADV [_PUNCT -_PUNCT ]_PUNCT digon_ADV hen_ADJ '_PUNCT nawr_ADV ar_ADP fy_DET hunan_VERB ._PUNCT
Dim_PART isio_ADV bod_AUX fi_PRON 'n_PART dod_VERB ta_CONJ beth_PRON ._PUNCT
'_PUNCT Sa_PROPN i_PRON 'n_PART lecio_VERB 'r_DET '_PUNCT steddfod_NOUN ar_ADP y_DET Sadwrn_PROPN d'wetha_PROPN '_PUNCT ._PUNCT
Na_INTJ ._PUNCT
Oedd_VERB e_PRON 'n_PART reminding_NOUN fi_PRON am_PART cowbois_VERB ._PUNCT
Achos_CONJ oedd_VERB ei_DET hw_CONJ {_NOUN \_PART }_VERB di_PRON fe_PRON '_PUNCT mlaen_NOUN yn_ADP y_DET '_PUNCT steddfod_NOUN ._PUNCT
So_PART ma_PRON '_PUNCT 'n_PART prynu_VERB hw_X {_PROPN \_PART }_VERB di_PRON os_CONJ di_PRON o_ADP 'n_PART mynd_VERB dydd_NOUN Mercher_PROPN ._PUNCT
Ie_INTJ hefo_ADP dydd_NOUN Mercher_PROPN [_PUNCT -_PUNCT ]_PUNCT ond_CONJ lle_NOUN oes_VERB i_PART bigo_VERB ti_PRON lan_NOUN o_ADP Abertawe_PROPN fel_CONJ bod_AUX rhaid_VERB ni_PRON fod_VERB yna_ADV 'n_PART gynnar_ADJ ._PUNCT
Na_INTJ ._PUNCT
'_PUNCT S_NUM dim_PART rhaid_VERB fi_PRON neud_VERB dim_DET byd_NOUN +_SYM
Na_INTJ ._PUNCT
+_VERB ar_ADP wahan_NOUN i_ADP [_PUNCT -_PUNCT ]_PUNCT leciwn_NOUN i_ADP fynd_VERB i_PART weld_VERB yr_DET  _SPACE gwobre_NOUN llenyddol_ADJ yn_PART cael_VERB 'i_PRON rhoi_VERB mas_NOUN am_ADP dri_NUM o_ADP 'r_DET gloch_NOUN yn_PART rhywle_ADV ._PUNCT
Ie_INTJ ._PUNCT
Ma'_VERBUNCT hynna_PRON 'n_PART iawn_ADJ '_PUNCT da_ADJ fi_PRON ._PUNCT
So_PART 's_X '_PUNCT im_PRON meddwl_NOUN bod_VERB rhaid_VERB i_ADP mi_PRON ._PUNCT
So_PART mae_VERB 'n_PART well_ADJ wedyn_ADV '_PUNCT swn_NOUN i_ADP nôl_VERB ti_PRON bore_NOUN dydd_NOUN Mercher_PROPN ?_PUNCT
Ie_INTJ ?_PUNCT
Ie_INTJ ._PUNCT
Os_CONJ 'di_PART 'n_PART iawn_ADJ '_PUNCT da_ADJ ti_PRON ?_PUNCT
Fydd_VERB rhaid_VERB fi_PRON fynd_VERB [_PUNCT -_PUNCT ]_PUNCT rhaid_VERB mynd_VERB i_ADP lleoliad_NOUN +_SYM
lleoliad_NOUN ._PUNCT
Ie_INTJ ._PUNCT
+_VERB a_CONJ lleoliad_NOUN ta_CONJ beth_PRON ._PUNCT
Rownd_ADV ffor_CONJ '_PUNCT 'na_ADV ._PUNCT
lleoliad_NOUN ._PUNCT
Ie_INTJ ._PUNCT
So_PART bydd_VERB e_PRON 'n_PART iawn_ADJ ._PUNCT
Oeddwn_AUX i_PRON 'm_DET yn_PART sylwi_VERB fel_ADP [_PUNCT -_PUNCT ]_PUNCT fel_ADP y_DET maes_NOUN [_PUNCT -_PUNCT ]_PUNCT o_ADP 'n_PART i_PRON 'n_PART meddwl_VERB fod_VERB e_PRON '_PUNCT 'n_PART agosach_ADJ i_ADP lleoliad_NOUN ffor_NOUN '_PUNCT 'na_ADV ._PUNCT
BeBe_NOUN ?_PUNCT
lleoliad_NOUN ?_PUNCT
Oe'wn_NOUN ._PUNCT
So_ADP ti_PRON 'di_PART bod_VERB 'na_ADV ?_PUNCT
'_PUNCT Dw_AUX i_PRON 'di_PART bod_VERB 'na_ADV ond_CONJ o_ADP 'n_PART i_PRON 'm_DET yn_PART sylwi_VERB ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART gw'bod_ADJ bod_VERB e_PRON '_PUNCT yn_ADP y_DET canolbarth_NOUN ond_CONJ do_AUX 'n_PART i_PRON 'm_DET yn_PART sylwi_VERB fod_VERB o_PRON mor_ADV dolennol_ADJ ._PUNCT
Ie_INTJ ._PUNCT
O_ADP reit_NOUN Okay_X ._PUNCT
Ie_INTJ ._PUNCT
Wel_INTJ ie_INTJ ._PUNCT
So_NOUN 's_X dim_DET gwahaniaeth_NOUN ._PUNCT
'_PUNCT S_NUM dim_PART rhaid_VERB fi_PRON ddod_VERB yna_ADV 'n_PART gynnar_ADJ ._PUNCT
'_PUNCT T_NUM isio_VERB fi_PRON i_ADP drio_ADV ca'l_NOUN fel_ADP ticet_VERB bwyd_NOUN neu_CONJ '_PUNCT bath_NOUN i_ADP chi_PRON fynd_VERB i_ADP lleoliad_NOUN ?_PUNCT
Fel_ADP rhyw_DET vouchers_NOUN enw_NOUN ?_PUNCT
Ie_INTJ ?_PUNCT
Ie_INTJ os_CONJ t_PRON '_PUNCT '_PUNCT isie_ADJ ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART meddwl_VERB '_PUNCT sa_PRON fo_PRON '_PUNCT pyn_NOUN bach_ADJ o_ADP arian_NOUN ?_PUNCT
Ie_INTJ ._PUNCT
I_ADP b_PRON '_PUNCT le_NOUN '_PUNCT dach_AUX chi_PRON 'n_PART ca'l_VERB y_DET tocynne_NOUN ?_PUNCT
'_PUNCT Sa_PROPN i_PRON 'n_PART bod_VERB ._PUNCT
'_PUNCT Na_VERB i_ADP edrych_VERB lan_ADV ._PUNCT
Edrych_VERB ar_ADP [_PUNCT aneglur_ADJ ]_PUNCT oll_ADV 'ma_ADV ._PUNCT
Ge_VERB 's_PROPN i_PRON docyn_NOUN iddyn_ADP nhw_PRON tro_NOUN d'wetha_ADP '_PUNCT ._PUNCT
Ti_PRON 'n_PART cofio_VERB ?_PUNCT
'_PUNCT Swn_PROPN i_PRON 'n_PART cael_VERB rhai_DET enw_NOUN [_PUNCT -_PUNCT ]_PUNCT math_NOUN hynny_DET o_ADP beth_PRON ?_PUNCT
Ie_INTJ ._PUNCT
Cofio_VERB a'thon_VERB ni_PRON i_ADP [_PUNCT aneglur_ADJ ]_PUNCT allan_ADV 'na_ADV ._PUNCT
Do_INTJ ._PUNCT
Oedd_VERB e_PRON 'n_PART bell_ADJ ._PUNCT
Oedd_VERB ._PUNCT
Oedd_VERB e_PRON yn_PART bell_ADJ ._PUNCT
Yn_PART real_ADJ bell_ADV ._PUNCT
Mae_VERB 'n_PART well_ADJ cael_VERB rhywbeth_NOUN lle_NOUN mae_VERB mwy_PRON o'nyn_ADP nhw_DET 'n_PART b'yta_VERB lle_ADV ._PUNCT
Ie_INTJ ._PUNCT
Ti_PRON 'n_PART iawn_ADJ ._PUNCT
Wel_INTJ gawn_VERB ni_PRON weld_VERB ._PUNCT
Achos_CONJ mae_VERB rhywbeth_NOUN lan_ADV '_PUNCT da_ADJ fi_PRON ar_ADP bore_NOUN dydd_NOUN Sadwrn_PROPN 'na_ADV ._PUNCT
Ond_CONJ 's_PART dim_DET rhaid_VERB fi_PRON fynd_VERB i_ADP fo_PRON ._PUNCT
Okay_INTJ ._PUNCT
Bydd_VERB rhaid_VERB fi_PRON holi_VERB enwg_VERB i_ADP gweld_VERB bebe_NOUN sy_AUX 'n_PART digwydd_VERB ._PUNCT
Ie_INTJ ._PUNCT
Achos_CONJ ma_AUX 'n_PART dod_VERB yn_PART gloy_NOUN '_PUNCT nawr_ADV ._PUNCT
Wel_INTJ penwythnos_NOUN nesa'_ADJUNCT ._PUNCT
'_PUNCT N'd_VERB 'di_PART ._PUNCT
Hollol_PROPN ._PUNCT
Ie_INTJ ._PUNCT
Ar_ADP ôl_NOUN yr_DET arholiade_NOUN a_CONJ pethe_VERB [_PUNCT -_PUNCT ]_PUNCT ar_ADP ôl_NOUN i_ADP ti_PRON trafaelio_VERB i_ADP 'r_DET '_PUNCT steddfod_NOUN o_ADP lleoliad_NOUN felly_ADV [_PUNCT -_PUNCT ]_PUNCT meddwl_VERB b_PRON '_PUNCT le_NOUN ti_PRON 'n_PART mynd_VERB i_ADP fyw_VERB blwyddyn_NOUN nesa'_ADJUNCT ?_PUNCT
Ie_INTJ ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART gw'bod_VERB ._PUNCT
Ie_INTJ ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART gweld_VERB bod_VERB rhyw_PRON fel_ADP postgraduate_NOUN [_PUNCT -_PUNCT ]_PUNCT diwrnode_ADP fel_ADP diwrnode_ADP i_PART wylio_VERB tai_NOUN ._PUNCT
Mae_AUX rheini_VERB 'n_PART costio_VERB fel_ADP pump_NUM [_PUNCT -_PUNCT ]_PUNCT ti_PRON 'n_PART talu_VERB i_ADP fynd_VERB [_PUNCT -_PUNCT ]_PUNCT fel_ADP pedwar_NOUN deg_NUM punt_NOUN ._PUNCT
BeBe_NOUN ?_PUNCT
I_PART mynd_VERB ar_ADP y_DET diwrnod_NOUN agored_ADJ ?_PUNCT
Oedd_AUX e_PRON '_PUNCT fel_ADP amser_NOUN '_PUNCT steddfod_NOUN hefyd_ADV ._PUNCT
Amser_NOUN '_PUNCT steddfod_NOUN lleoliad_NOUN ._PUNCT
Fel_CONJ bod_AUX ti_PRON 'n_PART mynd_VERB [_PUNCT -_PUNCT ]_PUNCT fe_PART weles_VERB rhywbeth_NOUN ar_ADP y_DET we_NOUN bod_AUX ti_PRON 'n_PART mynd_VERB ar_ADP y_DET nos_NOUN Iou_PROPN [_PUNCT -_PUNCT ]_PUNCT '_PUNCT dw_AUX i_ADP 'm_DET yn_PART gw'bod_VERB b_PRON '_PUNCT le_NOUN mae_VERB e_PRON '_PUNCT [_PUNCT -_PUNCT ]_PUNCT achos_CONJ be_PRON sy_VERB 'na_ADV [_PUNCT -_PUNCT ]_PUNCT potsian_VERB ._PUNCT
Reit_INTJ ._PUNCT
Hwnna_PRON ?_PUNCT
'Di_PART trio_VERB hwnna_PRON gynna_VERB ._PUNCT
'_PUNCT Falle_NOUN '_PUNCT bod_AUX fi_PRON di_PRON cael_VERB gweld_VERB e_PRON '_PUNCT ar_ADP y_DET wefan_NOUN ._PUNCT
Ar_ADP yr_DET ap_NOUN ?_PUNCT
Na_INTJ ._PUNCT
Dim_PRON ar_ADP ap_NOUN ._PUNCT
Ar_ADP y_DET we_NOUN ._PUNCT
enw_NOUN ?_PUNCT
Ie_INTJ ._PUNCT
Hwnna_PRON yw_VERB e_PRON '_PUNCT ?_PUNCT
Hwn_PRON yw_VERB e_PRON '_PUNCT '_PUNCT efe_PRON ?_PUNCT
Ia_INTJ ._PUNCT
So_PART edrych_VERB ar_ADP hwnna_PRON mae_AUX 'n_PART gweud_VERB "_PUNCT postgraduate_NOUN house_NOUN hunting_NOUN event_VERB "_PUNCT ._PUNCT
A_CONJ wedyn_ADV ti_PRON 'n_PART mynd_VERB fel_ADP [_PUNCT -_PUNCT ]_PUNCT '_PUNCT sa_PRON i_ADP 'n_PART meddwl_VERB mae_VERB fel_ADP [_PUNCT -_PUNCT ]_PUNCT dyle_NOUN fel_ADP tri_NUM diwrnod_NOUN ._PUNCT
Ti_PRON 'n_PART cyfarfod_VERB dydd_NOUN Iou_X a_CONJ '_PUNCT dyn_NOUN ti_PRON 'n_PART mynd_VERB ar_ADP y_DET dydd_NOUN Gwener_PROPN i_PRON '_PUNCT wilio_VERB am_ADP dai_NOUN ._PUNCT
A_PART wedyn_ADV ar_ADP y_DET dy_PRON '_PUNCT Sadwrn_PROPN yr_DET un_NUM peth_NOUN ._PUNCT
So_PART mama_NOUN rhaid_VERB chi_PRON aros_VERB 'na_ADV ?_PUNCT
Wel_INTJ mae_AUX 'n_PART edrych_VERB fel_ADP '_PUNCT y_DET ._PUNCT
'_PUNCT Falle_NOUN pobl_NOUN sy_AUX 'n_PART dod_VERB o_ADP bell_NOUN neu_CONJ fel_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mm_INTJ ._PUNCT
'_PUNCT Sa_NOUN 'n_PART hwyl_NOUN ._PUNCT
Mae_VERB e_PRON '_PUNCT ar_ADP gyfer_NOUN pobl_NOUN sydd_VERB [_PUNCT -_PUNCT ]_PUNCT i_PART ddod_VERB i_ADP '_PUNCT nabod_VERB pobl_NOUN sydd_VERB hefyd_ADV yn_PART postgraduates_VERB hefyd_ADV ?_PUNCT
O_ADP ie_INTJ ._PUNCT
Dyma_ADV ni_PRON nosweth_NOUN yna_ADV ._PUNCT
"_PUNCT Pedwar_NOUN deg_NUM punt_NOUN [_PUNCT -_PUNCT ]_PUNCT yn_PART cynnwys_VERB un_NUM nosweth_NOUN [_PUNCT -_PUNCT ]_PUNCT mewn_ADP neuadd_NOUN prifysgol_NOUN "_PUNCT ._PUNCT
Ar_ADP y_DET nos_NOUN Iau_ADJ [_PUNCT -_PUNCT ]_PUNCT ôô_NOUN [_PUNCT -_PUNCT ]_PUNCT nes_CONJ i_ADP 'r_DET steddfod_NOUN fel_ADP y_DET nawfed_NOUN [_PUNCT -_PUNCT ]_PUNCT y_DET degfed_VERB o_ADP Awst_PROPN ._PUNCT
Ond_CONJ  _SPACE [_PUNCT -_PUNCT ]_PUNCT gawn_VERB ni_PRON weld_VERB y_DET pisyn_NOUN 'na_ADV ._PUNCT
Os_CONJ ni_PRON 'n_PART gallu_VERB ffendio_VERB mas_NOUN pa_ADV ddiwrnodie_NOUN ma_VERB 'r_DET cwrs_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Mae_VERB eisie_VERB fi_PRON e_PRON -_PUNCT bostio_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mae_VERB rhyw_DET feddwl_NOUN '_PUNCT da_ADJ fi_PRON bod_VERB nhw_PRON 'di_PART gweud_VERB dydd_NOUN Llun_PROPN [_PUNCT -_PUNCT ]_PUNCT dydd_NOUN Mawrth_PROPN a_CONJ dydd_NOUN Iou_PROPN ._PUNCT
Ie_INTJ ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT wel_INTJ [_PUNCT =_SYM ]_PUNCT fi_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT fi_PRON 'n_PART eitha'_ADJUNCT siŵr_ADV dim_DET dydd_NOUN Gwener_PROPN ._PUNCT
Na_INTJ ._PUNCT
'_PUNCT Swn_PROPN i_ADP 'm_DET yn_PART credu_VERB bod_VERB e_PRON '_PUNCT dy_DET '_PUNCT Gwener_PROPN ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART eitha'_ADJUNCT siŵr_ADV fydd_VERB hi_PRON 'n_PART dydd_NOUN Llun_PROPN a_CONJ dydd_NOUN Iou_PROPN [_PUNCT -_PUNCT ]_PUNCT wedyn_ADV dydd_NOUN Mercher_PROPN neu_CONJ dydd_NOUN Mawrth_PROPN '_PUNCT sa_PRON i_ADP 'n_PART siŵr_ADJ o_ADP ._PUNCT
Ie_INTJ ._PUNCT
'_PUNCT Na_VERB i_PRON hefyd_ADV e_PRON -_PUNCT bostio_VERB am_ADP pa_PART diwrnode_ADP a_CONJ pa_PART amseroedd_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Achos_CONJ ie_INTJ [_PUNCT -_PUNCT ]_PUNCT '_PUNCT sa_PRON i_ADP 'n_PART siŵr_ADJ os_CONJ bydd_VERB e_PRON '_PUNCT 'n_PART diwrnod_NOUN llawn_ADJ [_PUNCT -_PUNCT ]_PUNCT fel_ADP naw_NUM tan_ADP pump_NUM [_PUNCT -_PUNCT ]_PUNCT neu_CONJ ddim_PRON ._PUNCT
Bydd_VERB rhaid_VERB i_ADP fi_PRON ffendio_VERB hwnna_PRON mas_NOUN fydd_VERB ._PUNCT
'_PUNCT Na_INTJ '_PUNCT i_PRON e_PRON -_PUNCT bostio_VERB ._PUNCT
Ie_INTJ ._PUNCT
So_PART [_PUNCT aneglur_ADJ ]_PUNCT y_DET fenyw_NOUN 'ma_ADV ._PUNCT
Ie_INTJ ._PUNCT
Okay_INTJ ._PUNCT
Well_ADJ i_ADP mi_PRON mynd_VERB i_ADP ddal_VERB y_DET trên_NOUN ._PUNCT
Brysiad_NOUN ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ie_INTJ ._PUNCT
'_PUNCT N_NUM ôl_NOUN i_ADP lleoliad_NOUN ._PUNCT
675.056_ADJ
