"_PUNCT Dwi_VERB heb_ADP glywed_VERB am_ADP y_DET Storïau_NOUN Wibli_PROPN -_PUNCT wobli_VERB yma_ADV ,_PUNCT "_PUNCT triodd_VERB Miss_PROPN Morgan_PROPN weiddi_VERB uwchben_ADP y_DET sŵn_NOUN ._PUNCT
"_PUNCT Maen_VERB nhw_PRON 'n_PART hen_ADJ ,_PUNCT hen_ADJ storïau_NOUN ac_CONJ mae_AUX pawb_PRON yn_PART gwybod_VERB amdanyn_ADP nhw_DET yng_ADP Nghymru_PROPN ._PUNCT
Dyna_DET beth_PRON ddwedodd_VERB Mam_PROPN ,_PUNCT "_PUNCT meddai_VERB Nel_NOUN ,_PUNCT gan_CONJ drio_ADV dysgu_VERB gwers_NOUN i_ADP Miss_PROPN Morgan_PROPN ._PUNCT
"_PUNCT Storïau_NOUN 'r_DET Mabinogi_PROPN mae_AUX hi_PRON 'n_PART ei_DET feddwl_VERB ,_PUNCT "_PUNCT meddai_VERB Gwern_PROPN Gwybod_VERB Popeth_PROPN ._PUNCT
"_PUNCT Diolch_INTJ ,_PUNCT Gwern_PROPN ._PUNCT ._PUNCT ._PUNCT Reit_INTJ ,_PUNCT mae_VERB 'n_PART bryd_NOUN i_ADP ni_PRON ddechrau_VERB ysgrifennu_VERB ein_DET storïau_NOUN ein_DET hunain_DET nawr_ADV ,_PUNCT "_PUNCT meddai_VERB Miss_PROPN Morgan_PROPN ,_PUNCT gan_CONJ edrych_VERB ar_ADP ei_DET horiawr_NOUN ._PUNCT
Oedd_VERB hi_PRON bron_ADV yn_PART hanner_NOUN awr_NOUN wedi_ADP tri_NUM ?_PUNCT
Edrychodd_VERB Nel_ADJ ar_ADP y_DET bwrdd_NOUN ._PUNCT ._PUNCT ._PUNCT ac_CONJ ar_ADP y_DET llawr_NOUN ._PUNCT ._PUNCT ._PUNCT ac_CONJ ar_ADP y_DET nenfwd_NOUN ._PUNCT
Agorodd_VERB ei_DET cheg_NOUN yn_PART swnllyd_ADJ ._PUNCT
"_PUNCT Miss_PROPN ,_PUNCT dwi_AUX ddim_PART yn_PART gwybod_VERB beth_PRON i_PART ysgrifennu_VERB ,_PUNCT "_PUNCT meddai_VERB ._PUNCT
"_PUNCT Wel_INTJ ,_PUNCT falle_ADV gallet_VERB ti_PRON ysgrifennu_VERB 'r_DET stori_NOUN hyfryd_ADJ yna_ADV ddwedest_VERB ti_PRON wrthon_ADP ni_PRON nawr_ADV ._PUNCT ._PUNCT ._PUNCT y_DET stori_NOUN ddwedodd_VERB dy_DET fam_NOUN ._PUNCT ._PUNCT ._PUNCT am_ADP y_DET ffordd_NOUN gest_VERB ti_PRON dy_DET greu_VERB
gan_ADP Gwydion_PROPN ,_PUNCT y_DET dyn_NOUN sydd_VERB hefyd_ADV yn_PART -_PUNCT ddewin_NOUN "_PUNCT
"_PUNCT AC_X sy_AUX 'n_PART byw_VERB drws_NOUN nesa_ADJ i_ADP Nel_NOUN ,_PUNCT "_PUNCT meddai_VERB Barti_PROPN ._PUNCT
"_PUNCT Ac_CONJ sy_AUX 'n_PART byw_VERB drws_NOUN nesa_ADJ i_ADP Nel_NOUN ,_PUNCT "_PUNCT ailadroddodd_VERB Miss_PROPN Morgan_PROPN ._PUNCT
"_PUNCT O_ADP ,_PUNCT na_INTJ ._PUNCT
Alla_VERB i_PRON ddim_PART ysgrifennu_VERB 'r_DET stori_NOUN yna_ADJ ,_PUNCT Miss_PROPN ,_PUNCT "_PUNCT meddai_VERB Nel_ADJ ._PUNCT
"_PUNCT Pam_ADV ,_PUNCT Nel_INTJ ?_PUNCT
Mae_AUX 'n_PART stori_NOUN fendigedig_ADJ ._PUNCT
Yn_PART llawn_ADJ dychymyg_NOUN ._PUNCT "_PUNCT Roedd_VERB Miss_PROPN Morgan_PROPN yn_PART methu_VERB deall_VERB ._PUNCT
"_PUNCT Achos_X ,_PUNCT "_PUNCT meddai_VERB Nel_NOUN ,_PUNCT gan_CONJ anadlu_VERB 'n_PART ddwfn_ADJ ._PUNCT
"_PUNCT Dwi_AUX wedi_PART addo_VERB i_ADP Mam_PROPN na_PART fydda_AUX i_PRON 'n_PART dweud_VERB wrth_ADP neb_PRON am_ADP Gwydion_PROPN ._PUNCT
Cris_NOUN croes_ADJ ._PUNCT
Llaw_NOUN ar_ADP fy_DET nghalon_NOUN ._PUNCT
And_NOUN no_PRON returns_VERB ._PUNCT "_PUNCT
"_PUNCT Pam_ADV ,_PUNCT Nel_INTJ ?_PUNCT "_PUNCT
Roedd_VERB pawb_PRON yn_ADP y_DET dosbarth_NOUN yn_PART dawel_ADJ ,_PUNCT yn_PART gwrando_VERB ar_ADP stori_NOUN Nel_ADJ ._PUNCT
"_PUNCT Pam_ADV ham_ADV ?_PUNCT
Achos_CONJ ,_PUNCT mae_AUX Mam_PROPN wedi_PART dweud_VERB wrtha_VERB i_ADP taw_ADP cyfrinach_NOUN yw_VERB hi_PRON ._PUNCT
Dwi_AUX 'n_PART gwybod_VERB SAWL_PROPN cyfrinach_NOUN ._PUNCT
Mae_VERB gan_ADP mam_NOUN Cai_PROPN y_DET geg_NOUN fwyaf_ADJ mae_AUX Mam_PROPN wedi_PART 'i_PRON gweld_VERB erioed_ADV ._PUNCT
Mae_VERB angen_NOUN bath_NOUN ar_ADP Dad_PROPN ac_CONJ mae_VERB angen_NOUN dysgu_VERB gwers_NOUN i_ADP chi_PRON ,_PUNCT Miss_PROPN Morgan_PROPN ._PUNCT ._PUNCT ._PUNCT Ond_CONJ shhh_VERB !_PUNCT
Dwi_VERB ddim_PART i_ADP fod_VERB i_PART ddweud_VERB wrth_ADP neb_PRON ._PUNCT ._PUNCT ._PUNCT Wps_X !_PUNCT "_PUNCT
Ysbyd_VERB y_DET Nos_NOUN
Pennod_NOUN 1_NUM
Merch_NOUN dda_ADJ oedd_VERB Nel_NOUN i_ADP fod_VERB ._PUNCT
(_PUNCT Wel_INTJ ,_PUNCT dyna_DET roedd_AUX hi_PRON 'n_PART ei_DET ddweud_VERB wrth_ADP bawb_PRON ._PUNCT )_PUNCT Merch_NOUN dda_ADJ FYDDAI_NUM hi_PRON -_PUNCT oni_CONJ bai_VERB am_ADP ddigwyddiadau_NOUN un_NUM diwrnod_NOUN arbennig_ADJ ._PUNCT
Y_DET diwrnod_NOUN a_CONJ newidiodd_VERB ei_DET bywyd_NOUN hi_PRON ._PUNCT
A_CONJ bywydau_NOUN pawb_PRON o_ADP 'i_PRON chwmpas_NOUN hi_PRON ._PUNCT
Dad_NOUN a_CONJ Mam_PROPN ._PUNCT ._PUNCT ._PUNCT
Twm_PROPN ,_PUNCT ei_DET brawd_NOUN mawr_ADJ ._PUNCT ._PUNCT ._PUNCT Anti_NOUN Gwen_PROPN drws_NOUN nesa_ADJ ._PUNCT ._PUNCT ._PUNCT y_DET plant_NOUN yn_ADP ei_DET dosbarth_NOUN hi_PRON ._PUNCT ._PUNCT ._PUNCT
Miss_NOUN Morgan_PROPN ,_PUNCT ei_DET hathrawes_NOUN ._PUNCT ._PUNCT ._PUNCT
Mrs_NOUN Puw_PROPN ,_PUNCT y_DET brifathrawes_NOUN ._PUNCT ._PUNCT ._PUNCT
a_CONJ Mr_NOUN Alun_PROPN ,_PUNCT y_DET gweinidog_NOUN ,_PUNCT ac_CONJ yn_ADP y_DET blaen_NOUN ,_PUNCT ac_CONJ yn_ADP y_DET blaen_NOUN ac_CONJ yn_ADP y_DET blaen_NOUN ._PUNCT
Nid_PART ei_DET bai_NOUN hi_PRON oedd_VERB e_PRON ._PUNCT
Roedd_AUX hi_PRON 'n_PART dweud_VERB hynny_PRON wrth_ADP bawb_PRON hefyd_ADV ._PUNCT
Bai_VERB 'r_DET corrach_NOUN oedd_VERB e_PRON ._PUNCT
Daeth_VERB y_DET corrach_NOUN i_ADP 'w_PRON bywyd_NOUN hi_PRON un_NUM diwrnod_NOUN ._PUNCT
Yn_PART sydyn_ADJ iawn_ADV ._PUNCT
PWFF_VERB !_PUNCT
Un_NUM funud_NOUN doedd_VERB e_PRON ddim_PART yno_ADV ._PUNCT
A_CONJ 'r_DET funud_NOUN nesaf_ADJ -_PUNCT ta_ADJ -_PUNCT daaaaa_ADJ !_PUNCT
Dyna_ADV fe_PRON !_PUNCT
Yn_PART sefyll_VERB o_ADP 'i_PRON blaen_NOUN hi_PRON ,_PUNCT yn_PART wên_NOUN o_ADP glust_NOUN i_ADP glust_NOUN ._PUNCT
Peth_NOUN bach_ADJ rhyfedd_ADJ oedd_VERB e_PRON hefyd_ADV ._PUNCT
Roedd_VERB ganddo_ADP fola_NOUN mawr_ADJ iawn_ADV ,_PUNCT iawn_INTJ ,_PUNCT iawn_INTJ ._PUNCT
Dychmygai_VERB Nel_CONJ y_PART byddai_VERB 'r_DET bola_NOUN mawr_ADJ yn_PART ddefnyddiol_ADJ iawn_ADV amser_NOUN cinio_NOUN -_PUNCT i_ADP roi_VERB plât_NOUN o_ADP frechdanau_VERB wy_NOUN a_CONJ diod_NOUN sgwosh_NOUN piws_ADJ arno_ADP ._PUNCT
(_PUNCT Rhaid_VERB i_ADP gorachod_NOUN fwyta_VERB hefyd_ADV ._PUNCT )_PUNCT
Nawr_ADV ,_PUNCT efallai_ADV y_PART byddech_AUX chi_PRON 'n_PART disgwyl_VERB i_ADP gorrach_NOUN ymddangos_VERB mewn_ADP lle_NOUN gwahanol_ADJ …_PUNCT
lle_NOUN hudolus_ADJ …_PUNCT
lle_NOUN hardd_ADJ ,_PUNCT hyd_NOUN yn_PART oed_NOUN …_PUNCT
Ond_CONJ ddaeth_VERB y_DET corrach_NOUN hwn_DET ddim_PART i_ADP 'n_PART byd_NOUN ni_PRON tra_CONJ oedd_VERB Nel_PROPN yn_ADP ei_DET gwely_NOUN ,_PUNCT yn_PART breuddwydio_VERB am_ADP gathod_NOUN bach_ADJ a_CONJ siocled_NOUN mawr_ADJ …_PUNCT
…_PUNCT nac_CONJ ar_ADP ben_NOUN bryn_NOUN yn_PART edrych_VERB dros_ADP y_DET môr_NOUN ag_ADP enfys_NOUN uwch_ADJ ei_DET phen_NOUN …_PUNCT
…_PUNCT nac_CONJ wrth_ADP iddi_ADP chwilio_VERB yn_ADP y_DET cwtsh_NOUN dan_ADP staer_NOUN ._PUNCT
(_PUNCT Dyna_DET ble_ADV roedd_AUX Mam_PROPN yn_PART cuddio_VERB hen_ADJ deganau_NOUN oedd_VERB
ar_ADP eu_DET ffordd_NOUN i_ADP 'r_DET siop_NOUN ail_ADJ -_PUNCT law_NOUN ._PUNCT )_PUNCT
Naddo_VERB 'n_PART wir_ADJ ._PUNCT
Mewn_ADP gwirionedd_NOUN ,_PUNCT daeth_VERB y_DET corrach_NOUN hwn_DET i_ADP 'n_PART byd_NOUN ni_PRON mewn_ADP lle_NOUN llawer_ADV llai_ADV cyffrous_ADJ ._PUNCT
Ble_ADV oedd_VERB Nel_NOUN ?_PUNCT
Ar_ADP y_DET toiled_NOUN yn_PART gwneud_VERB pi_NOUN -_PUNCT pi_NOUN !_PUNCT
Wel_INTJ ,_PUNCT roedd_AUX hi_PRON wedi_PART gorffen_VERB ar_ADP y_DET toiled_NOUN ._PUNCT
A_PART gwisgo_VERB ._PUNCT
A_PART thynnu_VERB 'r_DET tsiaen_NOUN ._PUNCT
Ac_CONJ roedd_VERB hi_PRON ar_ADP fin_NOUN gadael_VERB y_DET stafell_NOUN heb_ADP olchi_VERB ei_DET dwylo_NOUN pan_CONJ ._PUNCT ._PUNCT ._PUNCT wele_NOUN !_PUNCT
Dyna_ADV fe_PRON !_PUNCT
Y_DET dyn_NOUN bach_ADJ ei_DET hun_PRON !_PUNCT
Doedd_VERB ar_ADP Nel_NOUN ddim_PART ofn_NOUN o_ADP gwbwl_NOUN ._PUNCT
Ddim_PART hyd_NOUN yn_PART oed_NOUN y_DET tamaid_NOUN bach_ADJ lleiaf_ADJ ._PUNCT
Ond_CONJ fe_PART groesodd_VERB ei_DET meddwl_VERB ei_PRON bod_VERB ar_ADP fin_NOUN cael_VERB stŵr_NOUN am_ADP beidio_VERB â_ADP golchi_VERB ei_DET dwylo_NOUN ._PUNCT
"_PUNCT Dwi_AUX ddim_PART wedi_PART gorffen_VERB eto_ADV ,_PUNCT "_PUNCT meddai_VERB Nel_NOUN yn_PART gyflym_ADJ wrth_ADP y_DET corrach_NOUN -_PUNCT cyn_CONJ iddi_ADP gael_VERB row_NOUN ._PUNCT
"_PUNCT Wel_INTJ ,_PUNCT siapa_NOUN dy_DET stwmps_NOUN '_PUNCT te_NOUN ,_PUNCT gwd_NOUN gyrl_VERB ,_PUNCT "_PUNCT atebodd_VERB e_PRON ._PUNCT
"_PUNCT Sdim_X trwy_ADP 'r_DET dydd_NOUN '_PUNCT da_ADJ rhywun_NOUN fel_ADP fi_PRON ,_PUNCT ti_PRON 'n_PART gwbod_VERB ._PUNCT
Ti_PRON 'di_PART bod_VERB OES_X yn_PART barod_ADJ ._PUNCT
Bydde_VERB unrhyw_DET un_NUM yn_PART meddwl_VERB dy_DET fod_AUX ti_PRON EISIE_PROPN bod_VERB yn_PART hwyr_ADJ i_ADP 'r_DET wers_NOUN ffidil_NOUN ._PUNCT
Beth_PRON sy_AUX 'n_PART bod_VERB ?_PUNCT
Wyt_AUX ti_PRON 'n_PART chwarae_VERB mas_NOUN o_ADP diwn_NOUN ?_PUNCT "_PUNCT
"_PUNCT Dwi_AUX 'n_PART chwarae_VERB 'r_DET ffidil_NOUN yn_PART dda_ADJ iawn_ADV ,_PUNCT diolch_VERB yn_PART fawr_ADJ ._PUNCT
'_PUNCT Campus_ADJ '_PUNCT ,_PUNCT yn_PART ôl_NOUN Mrs_NOUN Copi_PROPN ,_PUNCT "_PUNCT meddai_VERB Nel_ADJ ._PUNCT
"_PUNCT Mmm_PROPN ._PUNCT
Mae_AUX 'n_PART gorfod_ADV dweud_VERB hynny_PRON -_PUNCT neu_CONJ fydde_VERB Dad_PROPN a_CONJ Mam_NOUN ddim_PART yn_PART talu_VERB am_ADP wersi_NOUN ._PUNCT
A_CONJ pheth_NOUN arall_ADJ ._PUNCT
Dwyt_AUX ti_PRON ddim_PART wedi_PART golchi_VERB dy_DET ddwylo_NOUN ._PUNCT "_PUNCT
Edrychodd_VERB y_DET corrach_NOUN ar_ADP ei_DET ewinedd_NOUN ._PUNCT
Sylwodd_VERB Nel_CONJ ei_PRON fod_AUX yn_PART gwisgo_VERB farnis_NOUN ewinedd_NOUN aur_NOUN ._PUNCT
Roedd_AUX yr_DET aur_NOUN yn_PART edrych_VERB yn_PART grêt_ADJ gyda_ADP 'i_PRON ddillad_NOUN ._PUNCT
Gwisgai_VERB drowsus_NOUN mawr_ADJ fel_ADP dau_NUM falŵn_NOUN coch_ADJ sgleiniog_ADJ ,_PUNCT crys_NOUN aur_NOUN a_CONJ gwasgod_NOUN streipiog_ADJ goch_ADJ a_CONJ du_ADJ sgleiniog_ADJ â_ADP seren_NOUN fawr_ADJ aur_NOUN arni_ADP ._PUNCT
Ond_CONJ nid_PART dyna_DET 'r_DET peth_NOUN rhyfeddaf_VERB amdano_ADP ._PUNCT
Y_DET peth_NOUN gwirioneddol_ADJ ryfedd_NOUN am_ADP y_DET corrach_NOUN oedd_VERB yr_DET hyn_PRON a_PART wisgai_VERB am_ADP ei_DET ben_NOUN -_PUNCT pêl_NOUN fawr_ADJ feddal_ADJ o_ADP het_NOUN ac_CONJ arni_ADP wreichionyn_ADP aur_NOUN oedd_AUX yn_PART symud_VERB '_PUNCT nôl_NOUN ac_CONJ ymlaen_ADV wrth_ADP iddo_ADP fe_PRON symud_VERB ._PUNCT
"_PUNCT Ble_ADV gest_VERB ti_PRON 'r_DET dillad_NOUN yna_ADV ?_PUNCT
Mewn_ADP siop_NOUN ddillad_NOUN ail_ADJ -_PUNCT law_NOUN ?_PUNCT "_PUNCT gofynnodd_VERB Nel_NOUN yn_PART syn_ADJ ._PUNCT
"_PUNCT Ie_INTJ ,_PUNCT 'na_ADV ti_PRON ._PUNCT
Ar_ADP ôl_NOUN i_ADP dy_DET dad_NOUN fod_VERB â_ADP 'i_PRON ddillad_NOUN i_ADP 'r_DET siop_NOUN ._PUNCT
A_CONJ beth_PRON wyt_AUX ti_PRON 'n_PART ei_DET wisgo_VERB ,_PUNCT gwd_NOUN gyrl_VERB ?_PUNCT
Gwisg_NOUN ysgol_NOUN ?_PUNCT
'_PUNCT Run_VERB peth_NOUN â_ADP phawb_NOUN arall_ADJ yn_ADP y_DET lle_NOUN 'ma_ADV ?_PUNCT
O_ADP leiaf_ADJ dwi_VERB 'n_PART wahanol_ADJ i_ADP bawb_PRON arall_ADJ ,_PUNCT "_PUNCT ochneidiodd_ADP y_DET corrach_NOUN ._PUNCT
Gallai_VERB Nel_CONJ gerdded_VERB i_ADP ffwrdd_ADV a_CONJ mynd_VERB i_ADP 'w_PRON gwers_NOUN ffidil_NOUN ._PUNCT
Ond_CONJ yn_PART lle_NOUN hynny_PRON ,_PUNCT rhoddodd_VERB y_DET plwg_NOUN yn_ADP y_DET sinc_NOUN ac_CONJ agor_VERB y_DET tap_NOUN dŵr_NOUN poeth_ADJ ._PUNCT
"_PUNCT Beth_PRON wyt_VERB ti_PRON eisie_VERB ?_PUNCT "_PUNCT Trodd_VERB ei_DET phen_NOUN i_PART edrych_VERB ar_ADP y_DET corrach_NOUN dros_ADP ei_DET hysgwydd_NOUN ._PUNCT
"_PUNCT Roedd_VERB gen_ADP i_PRON gwestiwn_NOUN i_ADP ti_PRON ._PUNCT
Cwestiwn_VERB i_ADP newid_VERB dy_DET fywyd_NOUN di_PRON ._PUNCT
Ond_CONJ ,_PUNCT mmm_INTJ ,_PUNCT dwi_VERB ddim_PART yn_PART siŵr_ADJ ydw_AUX i_PRON 'n_PART mynd_VERB i_PART ofyn_VERB i_ADP ti_PRON nawr_ADV ._PUNCT "_PUNCT
Edrychodd_VERB y_DET corrach_NOUN ar_ADP ei_DET farnis_NOUN ewinedd_NOUN aur_NOUN eto_ADV ._PUNCT
"_PUNCT Pam_ADV ?_PUNCT "_PUNCT Anghofiodd_VERB Nel_CONJ am_ADP olchi_VERB ei_DET dwylo_NOUN ._PUNCT
Trodd_VERB i_PRON edrych_VERB yn_PART iawn_ADJ ar_ADP y_DET corrach_NOUN yn_ADP ei_DET wasgod_NOUN sgleiniog_ADJ ._PUNCT
"_PUNCT Pam_ADV ham_ADV ?_PUNCT
Ro_VERB 'n_PART i_VERB ar_ADP fin_NOUN newid_VERB dy_DET fywyd_NOUN di_PRON ._PUNCT
Ond_CONJ dwi_VERB ddim_PART yn_PART siŵr_ADJ wyt_AUX ti_PRON 'n_PART haeddu_VERB fy_DET help_NOUN i_ADP nawr_ADV ._PUNCT "_PUNCT
Pennod_NOUN 2_NUM
"_PUNCT Well_ADJ i_ADP ti_PRON fynd_VERB ._PUNCT
Rwyt_VERB ti_PRON 'n_PART hwyr_ADJ i_ADP 'r_DET wers_NOUN ffidil_NOUN ._PUNCT "_PUNCT
Syllodd_VERB y_DET corrach_NOUN ar_ADP ei_DET fysedd_NOUN ._PUNCT
Roedd_VERB e_PRON wrth_ADP ei_DET fodd_NOUN gyda_ADP 'r_DET farnis_NOUN aur_NOUN ar_ADP ei_DET ewinedd_NOUN ._PUNCT
"_PUNCT Does_VERB dim_DET brys_NOUN ,_PUNCT "_PUNCT atebodd_VERB Nel_NOUN ._PUNCT
"_PUNCT Dwi_AUX 'n_PART mynd_VERB i_ADP roi_VERB 'r_DET ffidil_NOUN yn_ADP y_DET to_NOUN ._PUNCT
A_PART dechrau_ADV chwarae_VERB offeryn_NOUN mawr_ADJ ._PUNCT
Telyn_PROPN ,_PUNCT falle_ADV ._PUNCT
A_PART dwi_AUX 'n_PART mynd_VERB i_ADP roi_VERB enw_NOUN i_ADP 'r_DET delyn_NOUN -_PUNCT Ela_PROPN ._PUNCT ._PUNCT ._PUNCT neu_CONJ Arianrhod_X ._PUNCT ._PUNCT ._PUNCT neu_CONJ
Josh_PROPN ._PUNCT "_PUNCT
Cariodd_VERB Nel_ADJ ymlaen_ADV i_PART olchi_VERB ei_DET dwylo_NOUN ,_PUNCT gan_CONJ roi_VERB sebon_NOUN yn_PART ofalus_ADJ ar_ADP bob_DET un_NUM bys_NOUN ._PUNCT
Yn_PART sydyn_ADJ ,_PUNCT roedd_AUX hi_PRON 'n_PART gwybod_VERB beth_PRON i_PART 'w_PRON wneud_VERB i_PRON gael_VERB ei_DET ffordd_NOUN ei_DET hun_PRON ._PUNCT
"_PUNCT Plis_X !_PUNCT
Plis_INTJ pert_ADJ !_PUNCT
O_ADP PLIS_NOUN !_PUNCT !_PUNCT !_PUNCT !_PUNCT !_PUNCT !_PUNCT !_PUNCT "_PUNCT gwenodd_VERB Nel_ADJ gan_ADP siffrwd_ADJ ei_DET hamrannau_NOUN ._PUNCT
Roedd_AUX e_PRON 'n_PART gweithio_VERB ar_ADP Dad_PROPN a_CONJ Mam_NOUN bob_DET tro_NOUN ._PUNCT
"_PUNCT Wel_INTJ ,_PUNCT dwi_VERB 'ma_ADV nawr_ADV ,_PUNCT sbo_INTJ ,_PUNCT "_PUNCT meddai_VERB 'r_DET corrach_NOUN ,_PUNCT gan_CONJ ildio_VERB gyda_ADP 'i_PRON lais_NOUN bach_ADJ hwyliog_ADJ ._PUNCT
"_PUNCT Man_NOUN a_CONJ man_NOUN i_ADP fi_PRON wneud_VERB fy_DET ngwaith_NOUN ._PUNCT "_PUNCT
Sychodd_VERB Nel_CONJ ei_DET dwylo_NOUN 'n_PART ofalus_ADJ ac_CONJ yn_PART araf_ADJ ._PUNCT
Trodd_VERB i_PRON wynebu_VERB 'r_DET corrach_NOUN â_ADP gwên_NOUN fawr_ADJ ar_ADP ei_DET hwyneb_NOUN ._PUNCT
"_PUNCT Beth_PRON yw_VERB dy_DET enw_NOUN di_PRON '_PUNCT te_NOUN ?_PUNCT "_PUNCT
"_PUNCT Bogel_PROPN ._PUNCT "_PUNCT
"_PUNCT Bogel_NOUN !_PUNCT "_PUNCT
Dechreuodd_VERB Nel_NOUN chwerthin_VERB ._PUNCT
A_PART chwerthin_VERB ._PUNCT
A_PART chwerthin_VERB ._PUNCT
Nes_CONJ bod_AUX yn_PART rhaid_VERB iddi_ADP groesi_VERB ei_DET choesau_NOUN rhag_CONJ gwlychu_VERB ei_DET hun_PRON ._PUNCT
Yna_ADV ,_PUNCT sylwodd_VERB ar_ADP Bogel_PROPN yn_PART crychu_VERB ei_DET dalcen_NOUN yn_PART gas_NOUN ._PUNCT
Stopiodd_VERB Nel_PUNCT chwerthin_VERB ._PUNCT
Doedd_VERB hi_PRON ddim_PART am_ADP frifo_VERB teimladau_NOUN 'r_DET corrach_NOUN ._PUNCT
Roedd_AUX y_DET busnes_NOUN newid_NOUN bywyd_NOUN yn_PART swnio_VERB fel_ADP hwyl_NOUN ._PUNCT
"_PUNCT Nel_ADJ ydw_VERB i_PRON ,_PUNCT "_PUNCT gwenodd_VERB Nel_ADJ ._PUNCT
"_PUNCT Dwi_AUX 'n_PART byw_VERB yn_PART Tŷ_NOUN Gorau_ADJ ,_PUNCT Stryd_NOUN Dda_ADJ gyda_ADP fy_DET ffrind_NOUN gorau_ADJ ,_PUNCT Mister_PROPN Fflwff_PROPN ,_PUNCT y_DET gath_NOUN fawr_ADJ flewog_ADJ ,_PUNCT a_CONJ Twm_PROPN ,_PUNCT fy_DET mrawd_NOUN mawr_ADJ drewllyd_ADJ ._PUNCT "_PUNCT
"_PUNCT Dwi_AUX 'n_PART gwbod_VERB ,_PUNCT bach_ADJ ._PUNCT
Dwi_AUX 'n_PART gwbod_VERB popeth_PRON ,_PUNCT ti_PRON 'n_PART gweld_VERB ._PUNCT
Nawr_ADV ,_PUNCT ti_PRON 'n_PART barod_ADJ i_ADP glatsho_NOUN bant_ADV ?_PUNCT
Sdim_ADV trwy_ADP 'r_DET dydd_NOUN gyda_ADP fi_PRON i_PART newid_VERB dy_DET fywyd_NOUN di_PRON ._PUNCT "_PUNCT
"_PUNCT Bant_NOUN â_ADP ti_PRON !_PUNCT
Sdim_ADV trwy_ADP 'r_DET dydd_NOUN gyda_ADP fi_PRON chwaith_ADV ,_PUNCT "_PUNCT atebodd_VERB Nel_NOUN yn_PART hapus_ADJ ._PUNCT
Byddai_AUX Mrs_NOUN Copi_PROPN 'n_PART methu_ADV deall_VERB ble_ADV roedd_VERB hi_PRON ._PUNCT
Tynnodd_VERB Bogel_PROPN ffon_NOUN hud_NOUN o_ADP 'r_DET awyr_NOUN ._PUNCT
Roedd_VERB y_DET ffon_NOUN yn_PART ddu_ADJ fel_ADP y_DET nos_NOUN gyda_ADP darnau_NOUN hir_ADJ o_ADP dinsel_NOUN disglair_ADJ yn_PART chwyrlïo_VERB i_ADP bob_DET cyfeiriad_NOUN ._PUNCT
Meddai_VERB 'r_DET corrach_NOUN :_PUNCT "_PUNCT Gwranda_PROPN ar_ADP fy_DET ngeiriau_NOUN hud_NOUN ,_PUNCT I_ADP ti_PRON fe_PART rof_VERB y_DET byd_NOUN i_ADP gyd_ADP ._PUNCT
Beth_PRON bynnag_PRON a_PART ddymuni_VERB di_PRON ,_PUNCT Tydi_PROPN a_CONJ gei_PRON -_PUNCT un_NUM gwych_ADJ dw_VERB i_PRON !_PUNCT
Mewn_ADP geiriau_NOUN eraill_ADJ -_PUNCT beth_PRON ti_PRON eisie_VERB ,_PUNCT blodyn_NOUN ?_PUNCT "_PUNCT
A_PART chwarddodd_VERB Bogel_PROPN dros_ADP y_DET lle_NOUN i_ADP gyd_ADP ._PUNCT
Wrth_CONJ i_ADP 'r_DET corrach_NOUN bach_ADJ gael_VERB amser_NOUN i_PART wella_VERB o_ADP 'r_DET pwl_NOUN peswch_NOUN a_CONJ ddilynodd_VERB y_DET pwl_NOUN chwerthin_VERB ,_PUNCT cafodd_VERB Nel_NOUN amser_NOUN i_ADP feddwl_VERB ._PUNCT
"_PUNCT Waw_X !_PUNCT "_PUNCT meddyliodd_VERB wrthi_ADP hi_PRON ei_DET hun_PRON ._PUNCT
Roedd_VERB hyn_PRON yn_PART briliant_VERB ._PUNCT
Roedd_VERB e_PRON 'n_PART well_ADJ nag_CONJ anrheg_NOUN ben_NOUN -_PUNCT blwydd_NOUN ._PUNCT
Gallai_VERB ofyn_VERB am_ADP UNRHYW_NUM BETH_PROPN
roedd_VERB hi_PRON eisiau_VERB ._PUNCT
Dyma_DET 'r_DET anrheg_NOUN orau_ADJ erioed_ADV !_PUNCT
"_PUNCT Mmmm_PROPN ._PUNCT
Beth_PRON ga_ADP i_PRON ?_PUNCT "_PUNCT meddyliodd_VERB Nel_ADJ ._PUNCT
Roedd_VERB digon_PRON o_ADP bethau_NOUN ar_ADP ei_DET rhestr_NOUN :_PUNCT
"_PUNCT Dwi_AUX 'n_PART aros_VERB …_PUNCT "_PUNCT Roedd_AUX Bogel_PROPN wedi_PART gwella_VERB ar_ADP ôl_NOUN y_DET peswch_VERB ._PUNCT
Ond_CONJ doedd_VERB Nel_NOUN heb_ADP benderfynu_VERB eto_ADV !_PUNCT
"_PUNCT Dwi_AUX 'n_PART cyfri_VERB i_ADP dri_NUM ,_PUNCT "_PUNCT meddai_VERB
Bogel_NOUN ._PUNCT
"_PUNCT Un_NUM …_SYM dau_NUM …_PUNCT -_PUNCT tr_NOUN "_PUNCT
Cafodd_VERB Nel_NOUN syniad_NOUN gwell_ADJ na_CONJ 'r_DET pethau_NOUN ar_ADP y_DET rhestr_NOUN ._PUNCT
Roedd_VERB y_DET geiriau_NOUN ar_ADP flaen_NOUN ei_DET thafod_NOUN ._PUNCT
Roedd_AUX hi_PRON 'n_PART mynd_VERB i_PART ofyn_VERB am_ADP fod_VERB yn_PART ferch_NOUN dda_ADJ trwy_ADP 'r_DET amser_NOUN ._PUNCT
Ond_CONJ rywsut_ADV ,_PUNCT cawliodd_VERB y_DET geiriau_NOUN wrth_ADP ddod_VERB o_ADP 'i_PRON cheg_NOUN ._PUNCT
A_CONJ dyma_DET ddaeth_NOUN allan_ADV yn_PART un_NUM ribidirês_VERB :_PUNCT
"_PUNCT Dwi_VERB eisie_ADV bod_VERB yn_PART DDRWG_NOUN trwy_ADP 'r_DET amser_NOUN ._PUNCT "_PUNCT
"_PUNCT Tic_VERB !_PUNCT "_PUNCT meddai_VERB Bogel_PROPN gan_CONJ dynnu_VERB llinell_NOUN fawr_ADJ drwy_ADP 'r_DET awyr_NOUN â_ADP 'i_PRON ffon_NOUN hud_NOUN hyfryd_ADJ ._PUNCT
A_CONJ chyn_NOUN y_PART gallai_VERB Nel_CONJ gywiro_VERB ei_PRON
hun_INTJ -_PUNCT PWFF_X !_PUNCT
Roedd_AUX Bogel_PROPN wedi_PART diflannu_VERB yr_DET un_NUM mor_ADV sydyn_ADJ ag_ADP y_PART gwnaeth_NOUN ymddangos_VERB ._PUNCT
Daeth_VERB un_NUM peth_NOUN da_ADJ o_ADP 'r_DET diwrnod_NOUN hwnnw_DET ._PUNCT
Bob_DET tro_NOUN y_PART byddai_AUX Nel_NOUN yn_PART cael_VERB y_DET bai_NOUN am_ADP ryw_DET ddrygioni_NOUN (_PUNCT ac_CONJ roedd_AUX hynny_PRON 'n_PART digwydd_VERB yn_PART aml_ADJ )_PUNCT ,_PUNCT gallai_VERB ddweud_VERB :_PUNCT "_PUNCT Nid_PART fy_DET mai_NOUN i_PRON yw_VERB e_PRON ._PUNCT "_PUNCT Ac_CONJ roedd_VERB hynny_PRON 'n_PART wir_ADJ ._PUNCT
Wel_INTJ ,_PUNCT dyna_DET 'r_DET stori_NOUN roedd_AUX Nel_ADJ yn_PART ei_PRON dweud_VERB wrth_ADP bawb_PRON ,_PUNCT beth_PRON bynnag_PRON ._PUNCT
Ac_CONJ roedd_AUX pawb_PRON yn_PART ei_PRON chredu_VERB hi_PRON ._PUNCT
Yn_PART wir_ADJ ,_PUNCT roedd_VERB rhai_PRON o_ADP 'i_PRON ffrindiau_NOUN 'n_PART rhy_ADV ofnus_ADJ i_PART fynd_VERB i_ADP 'r_DET tŷ_NOUN bach_ADJ yn_ADP yr_DET ysgol_NOUN ar_ADP eu_DET pen_NOUN eu_DET hunain_DET ._PUNCT
Pam_ADV ?_PUNCT
Wel_INTJ ,_PUNCT rhag_ADP ofn_NOUN i_ADP gorrach_NOUN bach_ADJ ymddangos_VERB ._PUNCT
Corrach_NOUN bach_ADJ gyda_ADP bola_NOUN mawr_ADJ ac_CONJ anadl_NOUN oedd_AUX yn_PART drewi_VERB o_ADP frechdanau_VERB wy_NOUN fyddai_AUX 'n_PART eu_DET troi_VERB nhw_PRON 'n_PART ddrwg_ADJ ._PUNCT
Ond_CONJ roedd_VERB un_NUM adeg_NOUN o_ADP 'r_DET flwyddyn_NOUN pan_CONJ oedd_AUX Nel_NOUN yn_PART cael_VERB bod_VERB yn_PART ddrwg_ADJ -_PUNCT un_NUM adeg_NOUN pan_CONJ oedd_AUX yn_ADP RHAID_PROPN iddi_ADP fod_VERB yn_PART ddrwg_ADJ ._PUNCT
Calan_NOUN Gaeaf_NOUN ._PUNCT
Pennod_NOUN 3_NUM
"_PUNCT Ga_NOUN i_ADP lyffant_NOUN ,_PUNCT Mam_PROPN ?_PUNCT "_PUNCT
"_PUNCT Roeddet_VERB ti_PRON eisie_VERB ci_NOUN ddoe_ADV ._PUNCT
Pam_ADV wyt_VERB ti_PRON eisie_VERB llyffant_NOUN ?_PUNCT "_PUNCT
Gwgodd_VERB Nel_CONJ ar_ADP ei_DET mam_NOUN ._PUNCT
Roedd_VERB ci_NOUN mor_ADV '_PUNCT ddoe_ADV '_PUNCT !_PUNCT
"_PUNCT I_PART wneud_VERB hud_NOUN a_CONJ lledrith_NOUN ,_PUNCT wrth_ADP gwrs_NOUN ._PUNCT
Mae_AUX Calan_NOUN Gaeaf_NOUN wythnos_NOUN nesa_ADJ ac_CONJ mae_VERB gen_ADP i_ADP het_NOUN a_CONJ ffrog_NOUN ddu_ADJ a_CONJ brwsh_NOUN ._PUNCT
A_CONJ nawr_ADV mae_VERB gen_ADP i_PRON restr_NOUN o_ADP bethau_NOUN eraill_ADJ dwi_VERB angen_NOUN ._PUNCT "_PUNCT
Estynnodd_VERB y_DET rhestr_NOUN o_ADP 'i_PRON phoced_NOUN ._PUNCT
Rhestr_NOUN o_ADP bethau_NOUN mae_AUX 'n_PART RHAID_PROPN i_ADP fi_PRON eu_PRON cael_VERB er_ADP mwyn_NOUN mwynhau_VERB Calan_NOUN Gaeaf_NOUN :_PUNCT
1_NUM ._PUNCT
Crochan_NOUN i_PART wneud_VERB hud_NOUN a_CONJ lledrith_NOUN ._PUNCT
2_NUM ._PUNCT
Llyffant_VERB yn_PART ffrind_NOUN gorau_ADJ newydd_ADJ ac_CONJ i_PART helpu_VERB gyda_ADP 'r_DET hud_NOUN ._PUNCT
3_NUM ._PUNCT
Trwyn_NOUN mawr_ADJ hir_ADJ a_CONJ chnotiog_ADJ ._PUNCT
4_NUM ._PUNCT
Dafad_NOUN ar_ADP fy_DET moch_NOUN ._PUNCT
"_PUNCT Fyddet_VERB ti_PRON ddim_PART yn_PART hapus_ADJ iawn_ADV petaet_AUX ti_PRON 'n_PART dihuno_VERB un_NUM bore_NOUN a_CONJ ffeindio_VERB DAFAD_PROPN ar_ADP dy_DET foch_NOUN !_PUNCT
Wyt_AUX ti_PRON eisie_VERB dechrau_VERB brefu_VERB hefyd_ADV ?_PUNCT "_PUNCT chwarddodd_VERB Mam_PROPN ._PUNCT
"_PUNCT Gwell_ADJ i_ADP ti_PRON ddweud_VERB '_PUNCT dafaden_NOUN '_PUNCT ,_PUNCT
Nel_ADJ ._PUNCT "_PUNCT
"_PUNCT Roedd_VERB merched_NOUN Cymru_PROPN i_ADP gyd_NOUN yn_PART wrachod_NOUN slawer_ADJ dydd_NOUN ,_PUNCT "_PUNCT meddai_VERB Nel_ADJ ._PUNCT
"_PUNCT Dyna_DET beth_PRON ddwedodd_VERB Miss_PROPN Morgan_PROPN ._PUNCT
Dyna_DET pam_ADV r'yn_AUX ni_PRON 'n_PART gwisgo_VERB fel_ADP gwrachod_VERB amser_NOUN Calan_NOUN Gaeaf_NOUN ._PUNCT "_PUNCT
"_PUNCT Fuest_PROPN ti_PRON 'n_PART GWRANDO_VERB ar_ADP Miss_PROPN ?_PUNCT "_PUNCT Roedd_AUX Mam_PROPN wedi_PART cael_VERB sioc_NOUN ._PUNCT
Gwenodd_VERB Nel_ADJ a_CONJ siffrwd_ADJ ei_DET hamrannau_NOUN ._PUNCT
Efallai_ADV y_PART byddai_AUX Mam_PROPN yn_PART prynu_VERB 'r_DET pethau_NOUN ar_ADP y_DET rhestr_NOUN iddi_ADP nawr_ADV ._PUNCT
"_PUNCT Doedd_AUX POB_DET merch_NOUN ddim_PRON yn_PART wrach_NOUN ,_PUNCT "_PUNCT esboniodd_VERB Mam_PROPN ._PUNCT
"_PUNCT Ac_CONJ roedd_VERB llawer_PRON o_ADP wrachod_NOUN da_ADJ -_PUNCT gwrachod_NOUN gwyn_ADJ ._PUNCT
Ro_AUX 'n_PART nhw_PRON 'n_PART gwella_VERB pobol_ADJ sâl_ADJ
-_PUNCT yr_DET un_NUM peth_NOUN â_ADP doctoriaid_VERB heddiw_ADV ._PUNCT "_PUNCT Cafodd_VERB Nel_CONJ syniad_NOUN gwych_ADJ ._PUNCT
"_PUNCT Wyt_AUX ti_PRON 'n_PART meddwl_VERB y_PART byddai_VERB Anti_NOUN Gwen_PROPN drws_NOUN nesa_ADJ 'n_PART lico_NOUN help_NOUN i_ADP gael_VERB gwared_NOUN ar_ADP y_DET smotyn_NOUN mawr_ADJ coch_ADJ yna_ADV ar_ADP ei_DET boch_NOUN ?_PUNCT
Ti_PRON eisie_VERB i_ADP fi_PRON ofyn_VERB iddi_ADP ?_PUNCT "_PUNCT
"_PUNCT Na_INTJ ,_PUNCT Nel_NUM ,_PUNCT "_PUNCT atebodd_VERB Mam_PROPN yn_PART gyflym_ADJ ._PUNCT
"_PUNCT Nonsens_NOUN o_ADP America_PROPN yw_VERB Calan_NOUN Gaeaf_NOUN ,_PUNCT "_PUNCT meddai_VERB Dad_PROPN ,_PUNCT gan_CONJ ymuno_VERB â_ADP nhw_PRON ._PUNCT
"_PUNCT Nage_X ,_PUNCT Dad_PROPN ._PUNCT "_PUNCT Roedd_AUX Mam_PROPN yn_PART mwynhau_ADV cywiro_VERB Dad_PROPN ._PUNCT
"_PUNCT Gŵyl_NOUN Baganaidd_ADJ oedd_VERB Calan_NOUN Gaeaf_NOUN yn_PART wreiddiol_ADJ ._PUNCT
Roedd_AUX yr_DET hen_ADJ Gymry_PROPN 'n_PART gwisgo_VERB masgiau_NOUN i_ADP gadw_VERB ysbrydion_VERB drwg_ADJ i_ADP ffwrdd_ADV ._PUNCT
Dyna_DET pam_ADV r'yn_AUX ni_PRON 'n_PART creu_VERB wyneb_NOUN ar_ADP feipen_NOUN gyda_ADP chyllell_NOUN -_PUNCT i_PART edrych_VERB fel_ADP masg_NOUN ._PUNCT "_PUNCT
"_PUNCT Ga_NOUN i_ADP gyllell_NOUN i_ADP greu_VERB wyneb_NOUN ar_ADP feipen_NOUN ?_PUNCT "_PUNCT gofynnodd_VERB Nel_NOUN ._PUNCT
Doedd_VERB hi_PRON ddim_PART yn_ADP Galan_NOUN Gaeaf_NOUN eto_ADV ._PUNCT
Ac_CONJ roedd_VERB golwg_NOUN ofnus_ADJ ar_ADP Dad_PROPN a_CONJ Mam_PROPN ._PUNCT
Oedd_VERB ,_PUNCT roedd_VERB hi_PRON 'n_PART iawn_ADJ i_ADP chi_PRON fod_VERB yn_PART ddrwg_ADJ adeg_NOUN Calan_NOUN Gaeaf_NOUN ._PUNCT
Yn_PART wir_ADJ ,_PUNCT roedd_AUX pobol_ADJ yn_ADP DISGWYL_PROPN i_ADP chi_PRON fod_VERB yn_PART ddrwg_ADJ adeg_NOUN Calan_NOUN Gaeaf_NOUN ._PUNCT
Dyma_ADV hoff_ADJ adeg_NOUN Nel_ADJ o_ADP 'r_DET flwyddyn_NOUN (_PUNCT ar_ADP wahân_NOUN i_ADP adeg_NOUN Nadolig_PROPN a_CONJ 'i_PRON phen_NOUN -_PUNCT blwydd_NOUN hi_PRON ,_PUNCT wrth_ADP gwrs_NOUN )_PUNCT ._PUNCT
Ac_CONJ efallai_ADV FOD_PROPN yna_ADV wrachod_NOUN gwyn_ADJ yng_ADP Nghymru_PROPN slawer_ADJ dydd_NOUN ._PUNCT
Ond_CONJ roedd_VERB mwy_PRON o_ADP wrachod_NOUN drwg_ADJ ,_PUNCT roedd_VERB Nel_ADJ yn_PART siŵr_ADJ o_ADP hynny_PRON ._PUNCT
Roedd_AUX hi_PRON wedi_PART clywed_VERB y_DET storïau_NOUN amdanyn_ADP nhw_PRON 'n_PART cael_VERB eu_DET cosbi_VERB -_PUNCT yn_PART cael_VERB eu_DET hel_VERB yn_PART bell_ADJ i_ADP ffwrdd_ADV ,_PUNCT i_PART fyw_VERB mewn_ADP byd_NOUN heb_ADP dai_PROPN ,_PUNCT na_CONJ phobol_ADJ ,_PUNCT na_PART siopau_NOUN ,_PUNCT na_CONJ McDonald_NOUN 's_PROPN ._PUNCT
Roedd_AUX rhai_PRON 'n_PART cael_VERB eu_DET taflu_VERB i_ADP afon_NOUN a_CONJ 'u_PRON boddi_VERB ._PUNCT
Roedd_AUX rhai_PRON 'n_PART cael_VERB eu_DET taflu_VERB i_ADP dyllau_NOUN a_CONJ 'u_PRON claddu_VERB ._PUNCT
Dyna_DET pam_ADV mae_AUX pobol_ADJ yn_PART dweud_VERB '_PUNCT cael_VERB llond_NOUN twll_NOUN o_ADP ofn_NOUN '_PUNCT ._PUNCT
Roedd_VERB Nel_ADJ am_ADP roi_VERB llond_NOUN twll_NOUN o_ADP ofn_NOUN i_ADP bawb_PRON roedd_AUX hi_PRON 'n_PART nabod_VERB adeg_NOUN Calan_NOUN Gaeaf_NOUN -_PUNCT i_ADP Twm_PROPN ,_PUNCT ei_DET brawd_NOUN ,_PUNCT ac_CONJ i_ADP Anti_NOUN Gwen_PROPN drws_NOUN nesa_ADJ ._PUNCT
PAWB_VERB -_PUNCT ar_ADP wahân_NOUN i_ADP Ted_PROPN -_PUNCT ted_NOUN ,_PUNCT yr_DET arth_NOUN un_NUM llygad_NOUN ,_PUNCT a_CONJ Mister_PROPN Fflwff_PROPN ,_PUNCT y_DET gath_NOUN ._PUNCT
Pennod_NOUN 4_NUM
Doedd_VERB hi_PRON ddim_PART yn_PART DEG_ADJ !_PUNCT
Roedd_AUX Twm_PROPN yn_PART cael_ADV mynd_VERB gyda_ADP 'i_PRON ffrindiau_NOUN ,_PUNCT yn_ADP y_DET nos_NOUN ,_PUNCT heb_ADP ei_DET rieni_NOUN !_PUNCT
AC_CONJ roedd_AUX e_PRON 'n_PART gwisgo_VERB gwisg_NOUN wych_ADJ -_PUNCT sgerbwd_NOUN oedd_AUX yn_PART goleuo_VERB yn_ADP y_DET tywyllwch_NOUN ._PUNCT
Roedd_AUX y_DET wisg_NOUN wedi_PART costio_VERB dau_NUM fis_NOUN o_ADP arian_NOUN poced_NOUN i_ADP Twm_PROPN ._PUNCT
Ond_CONJ roedd_AUX Nel_ADJ yn_PART amau_VERB fod_VERB Dad_PROPN a_CONJ Mam_PROPN wedi_PART ei_DET helpu_VERB i_ADP 'w_PRON phrynu_VERB ._PUNCT
A_CONJ beth_PRON am_ADP Nel_NOUN ?_PUNCT
Roedd_VERB ganddi_ADP wisg_NOUN gwrach_NOUN eto_ADV eleni_ADV ._PUNCT
"_PUNCT Ga_NOUN i_ADP fynd_VERB allan_ADV heno_ADV ?_PUNCT "_PUNCT gofynnodd_VERB i_ADP Mam_PROPN ._PUNCT
"_PUNCT Na_INTJ ,_PUNCT Nel_NUM ,_PUNCT "_PUNCT chwarddodd_VERB Mam_PROPN ,_PUNCT yn_PART nerfus_ADJ iawn_ADV ._PUNCT
"_PUNCT Dwi_AUX 'n_PART addo_VERB bod_VERB yn_PART ferch_NOUN dda_ADJ ._PUNCT
Plis_INTJ !_PUNCT "_PUNCT
"_PUNCT A_CONJ beth_PRON fyddi_AUX di_PRON 'n_PART ei_DET wneud_VERB ?_PUNCT "_PUNCT
"_PUNCT Dim_DET byd_NOUN lot_ADJ ._PUNCT
Cnocio_VERB ar_ADP ddrysau_NOUN pobol_ADJ a_CONJ gweiddi_VERB '_PUNCT BW_X !_PUNCT '_PUNCT ,_PUNCT "_PUNCT chwarddodd_VERB Nel_ADJ yn_PART llon_ADJ ._PUNCT
"_PUNCT Wel_INTJ ,_PUNCT rwyt_AUX ti_PRON 'n_PART codi_VERB ofn_NOUN ar_ADP Mam_NOUN yn_PART barod_ADJ ,_PUNCT Nel_PUNCT fach_ADJ ._PUNCT "_PUNCT
Roedd_AUX llais_NOUN Mam_PROPN yn_PART crynu_VERB wrth_ADP feddwl_VERB am_ADP Nel_NOUN yn_PART rhedeg_VERB o_ADP gwmpas_NOUN yn_PART codi_VERB ofn_NOUN ar_ADP y_DET cymdogion_NOUN ._PUNCT
"_PUNCT PLIS_X !_PUNCT
PLIS_INTJ !_PUNCT
PLIS_ADJ !_PUNCT "_PUNCT
Cyfrodd_VERB Nel_CONJ iddi_ADP ddweud_VERB '_PUNCT plis_INTJ '_PUNCT fil_NUM o_ADP weithiau_ADV cyn_ADP i_ADP Dad_PROPN a_CONJ Mam_ADV gytuno_VERB ._PUNCT
Roedd_VERB Nel_ADJ yn_PART benderfynol_ADJ mai_PART hi_PRON fyddai_VERB 'r_DET wrach_NOUN fwyaf_ADV dychrynllyd_ADJ erioed_ADV ._PUNCT
Cafodd_VERB help_NOUN colur_NOUN Mam_NOUN at_ADP y_DET gwaith_NOUN hwnnw_DET ._PUNCT
A_PART rhaid_VERB ei_PRON bod_AUX hi_PRON wedi_PART gwneud_VERB job_NOUN dda_ADJ o_ADP bethau_NOUN oherwydd_ADP pan_CONJ welodd_VERB Mam_PROPN hi_PRON fe_PART sgrechiodd_VERB dros_ADP y_DET lle_NOUN i_ADP gyd_ADP :_PUNCT
"_PUNCT Na_INTJ ,_PUNCT Nel_NUM !_PUNCT
Fy_DET ngholur_NOUN Chanel_PROPN i_PRON yw_VERB hwnna_PRON !_PUNCT "_PUNCT
Chafodd_VERB Nel_CONJ ddim_PART trwyn_NOUN hir_ADV cnotiog_ADJ na_CONJ dafad_NOUN fawr_ADJ ddu_ADJ ar_ADP ei_DET boch_NOUN ._PUNCT
Ond_CONJ fe_PART gafodd_VERB hi_PRON rywbeth_NOUN gwell_ADJ na_CONJ hynny_PRON ._PUNCT
Fe_PART ddechreuodd_VERB dau_NUM ddant_NOUN ffrynt_NOUN Nel_CONJ siglo_VERB ganol_NOUN mis_NOUN Hydref_PROPN ._PUNCT
Roedd_VERB hi_PRON wrth_ADP ei_DET bodd_NOUN ._PUNCT
Roedd_VERB rhai_PRON o_ADP 'i_PRON ffrindiau_NOUN wedi_PART colli_VERB eu_DET dannedd_NOUN blaen_NOUN ers_ADP oes_VERB pys_NOUN !_PUNCT
Ac_CONJ erbyn_ADP Calan_NOUN Gaeaf_NOUN roedd_VERB y_DET ddau_NUM ddant_NOUN bron_ADV â_CONJ dod_VERB allan_ADV ._PUNCT
Roedden_VERB nhw_PRON mor_ADV gam_NOUN fel_CONJ eu_PRON bod_VERB nhw_PRON 'n_PART stico_NOUN mas_NOUN ac_CONJ yn_PART pwyntio_VERB at_ADP bobol_ADJ fel_ADP dau_NUM fys_NOUN gwyn_ADJ ,_PUNCT esgyrnog_ADJ ._PUNCT
Daeth_VERB diwrnod_NOUN olaf_ADJ mis_NOUN Hydref_PROPN o_ADP 'r_DET diwedd_NOUN !_PUNCT
Rhaid_VERB bod_VERB Nel_PROPN yn_PART wrach_NOUN ddychrynllyd_ADJ iawn_ADV oherwydd_CONJ pan_CONJ welodd_VERB Siw_PROPN Bw_X -_PUNCT Hw_VERB hi_PRON ,_PUNCT llenwodd_VERB ei_DET llygaid_NOUN â_ADP dagrau_NOUN ac_CONJ ofn_NOUN ._PUNCT
Roedd_VERB Siw_PROPN yn_PART llefain_ADJ am_ADP y_DET peth_NOUN lleiaf_ADJ ._PUNCT
"_PUNCT Beth_PRON wyt_VERB ti_PRON ?_PUNCT "_PUNCT gofynnodd_VERB Nel_NOUN i_ADP Siw_PROPN Bw_X -_PUNCT Hw_PROPN ._PUNCT
Roedd_VERB y_DET ddwy_NUM 'n_PART ffrindiau_NOUN ers_ADP y_DET Dosbarth_NOUN Derbyn_PROPN ._PUNCT
Roedd_AUX Siw_PROPN yn_PART arfer_NOUN llefain_VERB lot_PRON yn_ADP y_DET Dosbarth_NOUN Derbyn_PROPN hefyd_ADV ._PUNCT
"_PUNCT Môr_NOUN -_PUNCT leidr_NOUN ,_PUNCT "_PUNCT atebodd_VERB Siw_PROPN gan_CONJ chwifio_VERB ei_DET chleddyf_NOUN yn_PART llipa_ADJ ._PUNCT
"_PUNCT Fyddet_VERB ti_PRON ddim_PART yn_PART codi_VERB ofn_NOUN ar_ADP lygoden_NOUN ,_PUNCT "_PUNCT tynnodd_VERB Nel_NOUN ei_DET choes_NOUN ._PUNCT
Wel_INTJ ,_PUNCT roedd_VERB hi_PRON 'n_PART hawdd_ADJ gweld_VERB bod_AUX ei_DET chleddyf_NOUN wedi_PART ei_DET wneud_VERB o_ADP ffoil_NOUN ._PUNCT
Roedd_AUX Nel_ADJ yn_PART methu_ADV aros_VERB ._PUNCT
Beth_PRON fyddai_VERB 'n_PART well_ADJ na_CONJ mynd_VERB o_ADP gwmpas_NOUN yn_PART codi_VERB ofn_NOUN ar_ADP bobol_NOUN ?_PUNCT
A_PART chael_VERB losin_NOUN am_ADP
wneud_VERB hynny_PRON !_PUNCT
A_PART doedd_VERB dim_PART rhaid_VERB rhoi_VERB 'r_DET losin_NOUN i_ADP 'r_DET ysgol_NOUN chwaith_ADV ,_PUNCT fel_ADP yr_DET oedden_ADP nhw_DET 'n_PART gorfod_ADV gwneud_VERB pan_CONJ fydden_AUX nhw_PRON 'n_PART mynd_VERB i_ADP ganu_VERB carolau_NOUN adeg_NOUN y_DET Nadolig_PROPN ._PUNCT
Hw_INTJ ,_PUNCT hw_PART ,_PUNCT ha_PRON ,_PUNCT ha_PRON ,_PUNCT ha_PRON ,_PUNCT ha_PRON !_PUNCT !_PUNCT !_PUNCT
