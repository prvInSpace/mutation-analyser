0.0_NOUN
Ar_ADP mwy_PRON o_ADP enw_NOUN heno_ADV ._PUNCT
Sgwrs_NOUN gyda_ADP brenhines_NOUN y_DET bêl_NOUN gron_ADJ yng_ADP Nghymru_PROPN enwb_NOUN cyfenw_NOUN ._PUNCT
A_PART mae_VERB pethau_NOUN 'di_PART dechrau_ADV mynd_VERB ar_ADP chwaw_NOUN yn_PART llythrennol_ADJ [_PUNCT saib_NOUN ]_PUNCT yn_PART Uwch_NOUN Gynghrair_NOUN Cymru_PROPN penwythnos_NOUN yma_ADV ._PUNCT
A_PART Flick_PROPN to_NOUN Kick_PROPN ._PUNCT
Cystadleuaeth_NOUN subiwtio_VERB ger_ADP lleoliad_NOUN ._PUNCT
'_PUNCT Na_VERB chi_PRON eiriau_NOUN o_ADP 'n_PART i_ADP erioed_ADV yn_PART disgwyl_ADV gweud_VERB ar_ADP y_DET teledu_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Croeso_NOUN mawr_ADJ aton_VERB ni_PRON ._PUNCT
Santes_NOUN Dwynwen_NOUN Seisnig_ADJ hapus_ADJ i_ADP chi_PRON i_ADP gyd_ADP ._PUNCT
Digon_ADV o_ADP gariad_NOUN yn_ADP y_DET stiwdio_NOUN heno_ADV t'mod_VERB cyn_ADP ymosod_VERB ar_ADP Cymru_PROPN fel_ADV [_PUNCT aneglur_ADJ ]_PUNCT yma_ADV ._PUNCT
A_PART chyn-_VERB gapten_NOUN tîm_NOUN merched_NOUN Cymru_PROPN enwb_NOUN cyfenw_NOUN __NOUN y_DET ferch_NOUN gynta'_ADJUNCT i_PRON gael_VERB dros_ADP hanner_NOUN cant_NUM o_ADP gapiau_NOUN i_ADP Gymru_PROPN ond_CONJ yn_PART bwysicach_ADJ ga_NOUN 'th_PRON [_PUNCT saib_NOUN ]_PUNCT cap_NOUN cyntaf_ADJ [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Croeso_NOUN fawr_ADJ aton_VERB ni_PRON ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ ._PUNCT
A_CONJ digon_PRON o_ADP gerdiau_NOUN cariadus_ADJ heddi_VERB enwg_VERB ?_PUNCT
Wel_INTJ ._PUNCT
T'mod_VERB bebe_NOUN ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART teimlo_VERB sorri_VERB am_ADP y_DET dyn_NOUN post_NOUN efo_ADP 'r_DET bag_NOUN trwm_ADJ 'na_ADV bore_NOUN 'ma_ADV [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Yn_PART cario_VERB 'r_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mae_VERB jôcs_NOUN yn_PART gwell_ADJ na_CONJ dim_PRON ._PUNCT
A_CONJ ti_PRON 'n_PART gwybod_VERB bebe_NOUN ._PUNCT
Ti_PRON 'n_PART edrych_VERB yn_PART genfigennus_ADJ ._PUNCT
T'mod_VERB 'na_ADV ?_PUNCT
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Ti_PRON isho_NOUN dipyn_NOUN bach_ADJ o_ADP fitamin_NOUN V_NUM ._PUNCT Oes_VERB ?_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Tyrd_VERB yma_ADV ._PUNCT
Ty'd_NOUN yma_ADV ._PUNCT
66.795_NOUN
Wel_INTJ gallen_NOUN ni_PRON fod_AUX wedi_PART penderfynu_VERB cwrdd_NOUN â_ADP sawl_ADJ person_NOUN heddi_NOUN a_CONJ '_PUNCT fallai_VERB mai_PART rhai_PRON amlwg_ADJ byddai_VERB enwg_VERB cyfenw_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ond_CONJ fi_PRON 'di_PART dod_VERB i_ADP gwrdd_VERB â_ADP rhywun_NOUN o'dd_NOUN yn_PART chwaraewr_NOUN broffesiynnol_ADJ am_ADP dros_ADP dair_NUM blynedd_NOUN ar_ADP ddeg_NOUN ac_CONJ o'dd_NOUN sgorio_VERB dros_ADP dwy_NUM gant_NOUN o_ADP goliau_NOUN ._PUNCT
Fe_PART enillodd_VERB chwech_NUM deg_NUM un_NUM cap_NOUN i_ADP Gymru_PROPN ._PUNCT
Tri_NOUN deg_NUM wyth_NUM fel_ADP capten_NOUN ._PUNCT
Ac_CONJ ers_ADP tair_NUM blynedd_NOUN mae_AUX wedi_PART bod_VERB yn_PART rheolwr_NOUN ar_ADP ein_DET tîm_NOUN cenedlaethol_ADJ ._PUNCT
Dewch_VERB gyda_ADP fi_PRON i_PART gwrdd_VERB â_ADP enwb_NOUN cyfenw_NOUN ._PUNCT
112.331_PROPN
Fe_PART ymunodd_VERB â_ADP thîm_NOUN Arsenal_PROPN yn_ADP y_DET flwyddyn_NOUN dwy_NUM fil_NUM a_CONJ chwaraeodd_VERB dros_ADP gyfnod_NOUN o_ADP dair_NUM blynedd_NOUN ar_ADP ddeg_NOUN i_ADP 'r_DET clwb_NOUN ._PUNCT
Yn_ADP nwy_NOUN fil_NUM a_CONJ saith_NUM roedd_VERB hi_PRON 'n_PART rhan_NOUN allwyddol_ADJ o_ADP 'r_DET tîm_NOUN hanesyddol_ADJ gyflawnodd_VERB Y_DET Gamp_NOUN Lawn_PROPN y_DET flwyddyn_NOUN honno_PRON ._PUNCT
159.064_NOUN
enwb_VERB cyfenw_NOUN gyda_ADP __NOUN __NOUN __NOUN __NOUN fan_NOUN 'na_ADV ._PUNCT
Oedd_VERB yn_ADP yr_DET un_NUM tîm_NOUN a_CONJ 'r_DET un_NUM carfanau_NOUN â_ADP enwb_NOUN cyfenw_NOUN fan_NOUN hyn_DET ._PUNCT
ymm_PART enwb_NOUN ydy_VERB ferch_NOUN o_ADP lleoliad_NOUN __NOUN merch_NOUN o_ADP __NOUN __NOUN __NOUN ._PUNCT
Sut_ADV gymeriad_NOUN yw_VERB hi_PRON ?_PUNCT
O_ADP ie_INTJ ._PUNCT
Cymeriad_NOUN  _SPACE ch'mod_VERB  _SPACE eitha'_ADJUNCT caled_ADJ ond_CONJ brwdfrydig_ADJ [_PUNCT =_SYM ]_PUNCT a_CONJ [_PUNCT /=_PROPN ]_PUNCT a_CONJ mae_VERB 'n_PART mo'yn_NOUN ennill_VERB ac_CONJ ym_ADP mae_AUX 'n_PART wneud_VERB job_NOUN dda_ADJ ar_ADP hyn_PRON o_ADP bryd_NOUN ._PUNCT
Ond_CONJ maen_AUX nhw_PRON wedi_PART bod_VERB yn_PART ennill_ADJ ._PUNCT
Wedi_PART llwyddo_VERB eu_DET carfan_NOUN ar_ADP gyfer_NOUN Cwpan_NOUN Cyprus_ADJ wythnos_NOUN yma_DET ._PUNCT
Digon_ADV o_ADP enwau_NOUN cyfarwydd_ADJ mewn_ADP 'na_ADV ._PUNCT
Ambell_ADJ un_NUM enw_NOUN newydd_ADJ hefyd_ADV wrth_ADP i_ADP ni_PRON weld_VERB yr_DET  _SPACE enwau_NOUN fan_NOUN hyn_DET ._PUNCT
Ond_CONJ  _SPACE cystadleuaeth_NOUN gyfeillgar_ADJ yw_VERB hon_DET ._PUNCT
Paratoi_VERB ar_ADP gyfer_NOUN y_DET grwpiau_NOUN rhagbrofol_ADJ nesa'_ADJUNCT ._PUNCT
Neu_PART 'r_DET gemau_NOUN rhagbrofol_ADJ nesa'_ADJUNCT ._PUNCT
Achos_CONJ mae_AUX pethau_NOUN 'n_PART mynd_VERB yn_PART wych_ADJ fel_ADP o't_ADP ti_PRON 'n_PART sôn_VERB enwb_NOUN o_ADP ran_NOUN Cymru_PROPN ._PUNCT
Mae_VERB 'na_ADV frig_NOUN i_ADP grŵp_NOUN rhagbrofol_ADJ hyd_NOUN yn_PART hyn_PRON ._PUNCT
O_ADP weld_VERB hwnna_PRON ac_CONJ o_ADP glywed_VERB beth_PRON o'dd_NOUN hi_PRON 'n_PART gweud_VERB bod_AUX -_PUNCT ni_PRON 'n_PART ch'mod_VERB ni_PRON ddim_PART ymysg_ADP [_PUNCT =_SYM ]_PUNCT y_DET [_PUNCT /=_PROPN ]_PUNCT y_DET timau_NOUN mwya'_ADJUNCT yn_ADP Ewrop_PROPN o_ADP ran_NOUN y_DET gwledydd_NOUN ._PUNCT
Traean_NOUN o_ADP 'r_DET garfan_NOUN yn_PART unig_ADJ yn_PART broffesiynnol_ADJ ._PUNCT
Faint_ADV o_ADP gamp_NOUN yw_VERB hi_PRON bod_VERB Cymru_PROPN ar_ADP y_DET brig_ADJ felly_ADV ?_PUNCT
O_ADP -_PUNCT mae_VERB maen_AUX nhw_PRON 'di_PART dechrau_VERB 'n_PART dda_ADJ iawn_ADV a_CONJ momentwm_NOUN gydan_VERB nhw_PRON nawr_ADV i_ADP fynd_VERB mewn_ADP i_ADP gêm_NOUN Lloegr_PROPN ac_CONJ wrth_ADP gwrs_NOUN mae_VERB gwynebu_VERB Lloegr_PROPN yn_PART her_NOUN yn_ADP ei_DET hun_PRON ._PUNCT
Ie_INTJ faint_NOUN o_ADP hynna_ADP 'r_DET her_NOUN mae_VERB 'n_PART siŵr_ADJ enwg_VERB ._PUNCT
Ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Faint_ADV o_ADP anodd_ADJ ydy_VERB oherwydd_CONJ gydan_VERB ni_PRON gyn'lliad_NOUN o_ADP chwaraewyr_NOUN sydd_VERB ddim_PART llawn_ADJ amser_NOUN ?_PUNCT
Wel_ADV mae_VERB rhaid_VERB ystyried_VERB bod_VERB ch'mod_DET merched_NOUN Lloegr_PROPN yn_PART ymarfer_VERB '_PUNCT fallai_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Nhw_NOUN i_ADP gyd_NOUN ?_PUNCT
Ie_INTJ mae_VERB nhw_PRON i_ADP gyd_NOUN yn_PART broffesiynnol_ADJ o_ADP ran_NOUN chwarae_VERB Loegr_PROPN ._PUNCT
A_CONJ wedyn_ADV wrth_ADP gwrs_NOUN maen_NOUN -_PUNCT nhw_PRON mae_VERB amser_NOUN paratoi_VERB nhw_PRON 'n_PART lot_ADV fwy_ADJ 'na_ADV ni_PRON felly_ADV ie_INTJ mae_VERB fe_PRON ar_ADP lefel_NOUN gwahanol_ADJ iddynt_ADP ._PUNCT
A_PART mae_VERB rhaid_VERB ystyried_VERB pan_CONJ y_DET 'ch_AUX chi_PRON 'n_PART gwylio_VERB nhw_PRON yn_PART fyw_VERB pa_ADV mor_ADV dda_ADJ ydyn_VERB nhw_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB i_PART gweld_VERB nhw_PRON ._PUNCT
Cyn_CONJ bo_VERB hir_ADJ wrth_ADP gwrs_NOUN yn_ADP erbyn_ADP Cymru_PROPN ._PUNCT
Mae_VERB newidiadau_NOUN i_ADP fod_VERB fan_NOUN 'na_ADV hefyd_ADV gyda_ADP Lloegr_PROPN enwg_VERB cyfenw_NOUN yn_PART dod_VERB mewn_ADP i_PART fforddi_VERB 'r_DET tîm_NOUN ._PUNCT
Lot_PRON o_ADP ffys_NOUN 'di_PART bod_VERB am_ADP hwnna_PRON ._PUNCT
-_PUNCT Be_DET beth_PRON wyt_AUX ti_PRON 'n_PART meddwl_VERB o_ADP 'r_DET penodiad_NOUN ?_PUNCT
Wel_CONJ ch'mod_PRON mae_VERB '_PUNCT nny_NOUN 'n_PART ddiddorol_ADJ a_CONJ gewn_VERB ni_PRON weld_VERB [_PUNCT =_SYM ]_PUNCT sut_ADV [_PUNCT /=_PROPN ]_PUNCT sut_ADV aiff_VERB e_PRON ond_ADP  _SPACE ar_ADP ddiwedd_NOUN y_DET -_PUNCT ddy_DET dydd_NOUN y_DET ffocws_NOUN yw_VERB y_DET merched_NOUN ._PUNCT
A_CONJ 'r_DET canlyniadau_NOUN mae_VERB 'r_DET merched_NOUN yn_PART cael_VERB a_CONJ mae_VERB 'r_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Eto_ADV sut_ADV maen_AUX nhw_PRON 'n_PART wneud_VERB ._PUNCT
Felly_CONJ ym_ADP ._PUNCT
Dyw_VERB e_PRON ddim_PART really_ADV am_PART enwg_VERB cyfenw_NOUN i_ADP ddweud_VERB y_DET gwir_NOUN ._PUNCT
Mae_VERB am_ADP y_DET tîm_NOUN ._PUNCT
403.067_NOUN
Penwythnos_NOUN cyffrous_ADJ arall_ADJ yn_PART Uwch_NOUN Gynghrair_NOUN Cymru_PROPN ._PUNCT
Fel_ADP arfer_NOUN nawr_ADV ni_PRON 'n_PART edrych_VERB yn_PART ôl_NOUN ar_ADP y_DET goliau_NOUN gorau_ADJ ond_CONJ mae_AUX rhaid_VERB i_ADP ni_PRON edrych_VERB ar_ADP y_DET dathliad_NOUN gorau_ADJ enwg_VERB cyfenw_NOUN ._PUNCT
525.551_VERB
enwb_VERB Oedd_VERB '_PUNCT da_ADJ ti_PRON unrhyw_DET ddathliadau_NOUN dramatig_ADJ fel_ADP hwnna_PRON pan_CONJ oeddet_AUX ti_PRON 'n_PART chwarae_VERB i_ADP Gymru_PROPN ?_PUNCT
Wel_CONJ dim_PRON ond_ADP tair_NUM gôl_NOUN wnes_VERB i_PRON sgorio_VERB felly_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT mwy_ADJ na_INTJ +_SYM
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
+_SYM tair_NUM gôl_NOUN ond_CONJ ie_INTJ ._PUNCT
Y_DET ddwy_NUM o_ADP chi_PRON 'di_PART sgorio_VERB fwy_PRON o_ADP goliau_NOUN i_ADP 'ch_PRON gwlad_NOUN na_PART ydw_VERB i_PRON 'di_PART gwneud_VERB ._PUNCT
Credwch_VERB chi_PRON fi_PRON ._PUNCT
560.211_NOUN
Mae_VERB 'na_ADV lot_PRON o_ADP gemau_NOUN cyffrous_ADJ yn_PART mynd_VERB i_PART ddod_VERB ._PUNCT
Ond_CONJ faint_ADV o_ADP wahaniaeth_NOUN mae_AUX hynny_PRON 'n_PART digwydd_VERB fod_VERB rheolwyr_NOUN newydd_ADJ yn_PART dod_VERB i_ADP fewn_ADP ?_PUNCT
'Di_PART bod_VERB [_PUNCT =_SYM ]_PUNCT yn_ADP y_DET [_PUNCT /=_PROPN ]_PUNCT yn_ADP y_DET Premiership_PROPN yn_PART <_SYM aneglur_ADJ 2_NUM >_SYM efo_ADP Everton_PROPN efo_ADP West_PROPN Ham_PROPN efo_ADP Crystal_NOUN Palace_PROPN ._PUNCT
Ond_CONJ [_PUNCT =_SYM ]_PUNCT mwy_ADJ na_INTJ [_PUNCT /=_PROPN ]_PUNCT mwy_ADJ na_CONJ dim_DET byd_NOUN efo_ADP Abertawe_PROPN ._PUNCT
Ie_INTJ [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
cyfenw_NOUN ._PUNCT
T'mod_ADV mae_VERB o_PRON 'di_PART hollol_ADV newid_ADJ ._PUNCT
Ond_CONJ oedden_ADP nhw_PRON 'n_PART -_PUNCT st_VERB stryglo_NOUN ._PUNCT
O_ADP 'n_PART i_PRON yn_PART stryglo_VERB i_PART weld_VERB lle_NOUN o'dden_AUX nhw_PRON 'n_PART mynd_VERB i_ADP ennill_VERB nesa'_ADJUNCT ._PUNCT
Ond_CONJ rŵan_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT colli_VERB eto_ADV ._PUNCT
Ti_PRON 'n_PART gwybod_VERB [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
-_PUNCT Ffo_ADP ffordd_NOUN mae_VERB pêl_NOUN -_PUNCT droed_NOUN [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART chwarae_VERB efo_ADP pennau_NOUN pobl_NOUN a_CONJ rheolwyr_NOUN yn_PART [_PUNCT =_SYM ]_PUNCT dod_VERB [_PUNCT /=_PROPN ]_PUNCT dod_VERB i_ADP fewn_ADP a_CONJ wneud_VERB -_PUNCT cad_NOUN dylanwad_NOUN mawr_ADJ ._PUNCT
Ie_INTJ neu_CONJ ydy_VERB e_PRON [_PUNCT -_PUNCT ]_PUNCT [_PUNCT =_SYM ]_PUNCT Beth_PRON [_PUNCT /=_PROPN ]_PUNCT beth_PRON am_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP ife_ADV jyst_ADV llais_NOUN gwahanol_ADJ yw_VERB e_PRON weithiau_ADV mewn_ADP ystafell_NOUN newydd_ADJ ?_PUNCT
Yw_VERB e_PRON mor_ADV syml_ADJ â_ADP '_PUNCT nny_NOUN gyda_ADP 'r_DET rheolwyr_NOUN enwb_NOUN ?_PUNCT
Wel_AUX fi_PRON 'n_PART credu_VERB bod_VERB e_PRON 'n_PART bwysig_ADJ bod_VERB y_DET -_PUNCT bo_AUX y_DET chwaraewyr_NOUN yn_PART gwybod_VERB bod_VERB nhw_PRON 'n_PART uniaethu_VERB gydan_VERB nhw_PRON a_CONJ bod_VERB nhw_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Bod_VERB ffydd_NOUN gydan_VERB nhw_PRON ynyn_ADP nhw_PRON ._PUNCT
A_CONJ wedyn_ADV os_CONJ byddwch_VERB chi_PRON yn_PART un_NUM garfan_NOUN agos_ADJ wedyn_ADV gewch_VERB chi_PRON canlyniadau_NOUN ._PUNCT
O'dden_AUX ni_PRON 'n_PART siarad_VERB gynnar_ADJ a_CONJ enwb_VERB yn_PART d'eud_VERB am_ADP y_DET rheolwyr_NOUN yn_PART dod_VERB i_ADP fewn_ADP a_CONJ sarthu_VERB eu_DET personoliaeth_NOUN [_PUNCT saib_NOUN ]_PUNCT [_PUNCT =_SYM ]_PUNCT ar_ADP [_PUNCT /=_PROPN ]_PUNCT ar_ADP glwb_NOUN [_PUNCT =_SYM ]_PUNCT Ar_ADP ar_ADP [_PUNCT /=_PROPN ]_PUNCT ar_ADP y_DET chwaraewyr_NOUN ar_ADP y_DET timau_NOUN ._PUNCT
A_CONJ wedyn_ADV cael_VERB y_DET tryst_NOUN ganddyn_ADP nhw_PRON ._PUNCT
A_PART mae_VERB 'n_PART mor_ADV mor_ADV bwysig_ADJ bod_AUX nhw_PRON yn_PART wneud_VERB hynna_PRON ._PUNCT
Ti_PRON 'n_PART gwybod_VERB ._PUNCT
A_CONJ [_PUNCT =_SYM ]_PUNCT mae_VERB [_PUNCT /=_PROPN ]_PUNCT mae_AUX cyfenw_NOUN wedi_PART wneud_VERB hynna_PRON ._PUNCT
Ydy_VERB ._PUNCT
Mae_AUX fe_PRON 'n_PART gweithio_VERB yn_PART bendant_ADJ lawr_ADV Liberty_PROPN ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ iawn_ADV am_ADP y_DET tro_NOUN ._PUNCT
ymm_VERB dim_PRON sathru_VERB ar_ADP ddim_PART byd_NOUN pan_CONJ ddown_VERB ni_PRON nôl_ADV achos_ADP ni_PRON 'n_PART trafod_ADV subiwtio_VERB wedi_ADP 'r_DET egwyl_NOUN We_PROPN really_X wanna_VERB see_NOUN those_NOUN fingers_NOUN ._PUNCT
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
665.989_VERB
Mae_VERB gemau_NOUN cyfrifiadurol_ADJ mor_ADV realistig_ADJ y_DET dyddiau_NOUN 'ma_ADV cyn_ADP bo_VERB hir_ADV byddwch_AUX chi_PRON 'n_PART gallu_VERB camu_VERB mewn_ADP i_ADP 'r_DET gêm_NOUN a_PART chwarae_VERB gyda_ADP 'ch_PRON arwyr_NOUN ._PUNCT
Pan_CONJ o_ADP 'n_PART i_PRON 'n_PART blentyn_NOUN [_PUNCT saib_NOUN ]_PUNCT pethau_NOUN tipyn_NOUN mwy_ADV syml_ADJ ._PUNCT
'_PUNCT Na_VERB i_ADP gyd_CONJ oedd_VERB angen_NOUN oedd_VERB un_NUM bys_NOUN a_CONJ 'r_DET gallu_NOUN i_PART wneud_VERB hyn_PRON ._PUNCT
A_CONJ heddi_NOUN yn_PART lleoliad_NOUN dw_AUX i_PRON 'n_PART cael_VERB camu_VERB nôl_ADV i_ADP 'm_DET mhlentyndod_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN hapus_ADJ ]_PUNCT ._PUNCT
Cystadleuaeth_NOUN subiwtio_VERB ._PUNCT
Dw_AUX i_PRON wedi_PART denu_VERB timau_NOUN o_ADP 'r_DET ardal_NOUN Bwcle_PROPN Caerdydd_PROPN a_CONJ Wolverhampton_PROPN i_ADP 'r_DET canolbarth_NOUN ._PUNCT
Ac_CONJ fe_PART ges_VERB i_ADP gyfle_NOUN i_PART hel_VERB atgofion_VERB gyda_ADP un_NUM o_ADP gymeriadau_NOUN 'r_DET tîm_NOUN lleol_ADJ ._PUNCT
Pan_CONJ o_ADP 'n_PART i_PRON 'n_PART fach_ADJ o'dd_NOUN 'na_ADV lot_PRON o_ADP 'm_DET ffrindiau_NOUN 'n_PART chwarae_VERB fo_PRON hefyd_ADV ._PUNCT
Oedd_VERB +_SYM
Ie_INTJ ._PUNCT
+_VERB o_ADP 'n_PART gêm_NOUN mawr_ADJ yn_ADP y_DET saith_NUM degau_NOUN ._PUNCT
Enfawr_NOUN ._PUNCT
A_CONJ bod_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ti_PRON 'n_PART cael_VERB yr_DET un_NUM flick_ADJ â_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP dyna_ADV fe_NOUN Flick_PROPN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT wrth_ADP gwrs_NOUN ._PUNCT
ym_ADP ._PUNCT
Pwy_PRON oedd_VERB yn_PART ennill_VERB ?_PUNCT
Ti_PRON neu_CONJ enwg_VERB ?_PUNCT
ym_ADP +_SYM
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
+_SYM oedd_AUX yn_PART eitha'_ADJUNCT gyfartal_ADJ ar_ADP y_DET pryd_NOUN ._PUNCT
Ond_CONJ dyddiau_NOUN 'ma_ADV os_CONJ '_PUNCT dyn_NOUN ni_PRON 'n_PART gael_VERB gêm_NOUN rŵan_ADV baswn_VERB i_ADP 'n_PART ennill_ADJ t'mod_VERB ._PUNCT
Beth_PRON sy_AUX 'n_PART cael_VERB fi_PRON fel_ADP ti_PRON 'n_PART gweud_VERB ._PUNCT
Chi_PRON 'n_PART cael_VERB un_NUM ergyd_NOUN a_CONJ chi_PRON 'n_PART cael_VERB amddiffyn_VERB hefyd_ADV ._PUNCT
Mae_AUX lot_PRON o_ADP symud_VERB ._PUNCT
Mae_VERB rhai_PRON +_SYM
Bois_INTJ ._PUNCT
Bois_INTJ ._PUNCT
+_VERB [_PUNCT aneglur_ADJ ]_PUNCT yn_PART chwysu_VERB [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
O_ADP mae_VERB nhw_PRON 'n_PART dda_ADJ ._PUNCT
Maen_VERB nhw_PRON 'n_PART dda_ADJ ._PUNCT
Nawr_ADV dyw_VERB 'r_DET gêm_NOUN ddim_PART mor_ADV boblogaidd_ADJ ym_ADP Mhrydain_PROPN ag_CONJ oedd_VERB hi_PRON yn_ADP y_DET saith_NOUN degau_NOUN ond_CONJ mae_AUX subiwtio_VERB yn_PART parhau_VERB yn_PART boblogaidd_ADJ iawn_ADV mewn_ADP ambell_ADJ un_NUM wlad_NOUN hyd_NOUN heddi_NOUN ._PUNCT
741.664_NOUN
Dw_AUX i_PRON 'n_PART colli_VERB ar_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP ._PUNCT Mae_VERB mewn_ADP ._PUNCT
Goal_NOUN ._PUNCT
O_ADP gôl_NOUN dda_ADJ ._PUNCT
Dyw_VERB hi_PRON ddim_PART ._PUNCT
Bois_INTJ ._PUNCT
BeBe_ADV ddigwyddodd_VERB mas_NOUN 'na_ADV ._PUNCT
Dw_AUX i_PRON 'di_PART bod_VERB trwy_ADP popeth_PRON ._PUNCT
Tactegau_PROPN ._PUNCT
Patrwm_PROPN ._PUNCT
Popeth_PROPN ._PUNCT
Fi_PRON 'di_PART gweud_VERB wrthych_ADP pwy_PRON i_ADP 'w_PRON farcio_VERB Beth_PRON wyt_VERB ti_PRON yfed_VERB ?_PUNCT
795.126_NOUN
1438.997_NOUN
