0.0_NOUN
76.996_NOUN
Dyma_DET 'r_DET peiriant_NOUN pasta_NOUN hud_NOUN sy_AUX 'n_PART dweud_VERB wrtha'_VERBUNCT i_PRON am_ADP broblemau_NOUN plant_NOUN bach_ADJ y_DET byd_NOUN ._PUNCT
'_PUNCT Sgwn_VERB os_CONJ oes_VERB angen_NOUN fy_DET helpu_VERB ar_ADP unrhyw_DET un_NUM heddiw_ADV ?_PUNCT
Gawn_VERB ni_PRON weld_VERB ._PUNCT
Ie_INTJ ?_PUNCT
Ac_CONJ edrychwch_VERB [_PUNCT Sillafu_VERB enw_NOUN ]_PUNCT enwg_VERB ._PUNCT
Ac_CONJ mae_AUX enwg_VERB yn_PART byw_VERB mewn_ADP fflat_ADJ ar_ADP y_DET stryd_NOUN yma_ADV ._PUNCT
Ar_ADP eich_DET marciau_NOUN ._PUNCT
Barod_ADJ [_PUNCT Sŵn_NOUN gwn_AUX yn_PART tanio_VERB ]_PUNCT ._PUNCT
Ewch_PROPN ._PUNCT
Iei_INTJ ._PUNCT
Iei_INTJ ._PUNCT
Iei_INTJ ._PUNCT
Iei_INTJ ._PUNCT
'_PUNCT Na_INTJ '_PUNCT i_ADP fyth_ADV ennill_VERB ras_NOUN go_ADV iawn_ADJ ._PUNCT
Mae_AUX rhywbeth_NOUN yn_PART poeni_VERB enwg_VERB a_CONJ '_PUNCT dw_VERB i_PRON am_ADP ei_DET helpu_VERB ._PUNCT
Drws_NOUN pasta_NOUN hud_NOUN ._PUNCT
Cer_VERB â_ADP fi_PRON i_ADP 'r_DET stryd_NOUN mewn_ADP pryd_NOUN <_SYM eid_VERB >_SYM Ciao_PROPN <_SYM /_SYM eid_VERB >_SYM enwg_VERB bach_ADJ ._PUNCT
BeBe_PRON sy_AUX 'n_PART bod_VERB ?_PUNCT
Ti_PRON 'n_PART edrych_VERB mor_ADV drist_ADJ ._PUNCT
Dere_VERB '_PUNCT nawr_ADV ._PUNCT
'_PUNCT Elli_PROPN di_PRON ddweud_VERB unrhyw_DET beth_NOUN yn_ADP y_DET byd_NOUN wrtha'_VERBUNCT i_PRON ._PUNCT
Mae_VERB mabolgampau_NOUN 'r_DET ysgol_NOUN yr_DET wythnos_NOUN nesaf_ADJ ._PUNCT
Hwre_INTJ ._PUNCT
Na_VERB dim_PRON hoi_NOUN ._PUNCT
'_PUNCT Dw_VERB i_PRON ddim_PART mo'yn_NOUN gwneud_VERB hwnna_PRON ._PUNCT
Pam_ADV ?_PUNCT
Ydi_NOUN dy_DET goes_NOUN di_PRON 'n_PART gwneud_VERB dolur_NOUN ?_PUNCT
Ydi_NOUN dy_DET sgidie_NOUN 'di_PART 'n_PART rhy_ADV fach_ADJ ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Na_INTJ [_PUNCT /=_PROPN ]_PUNCT na_INTJ ._PUNCT
Beth_PRON '_PUNCT tê_PRON ?_PUNCT
'_PUNCT Dw_VERB i_PRON 'n_PART ofn_NOUN colli_VERB ._PUNCT
Ôô_NOUN enwg_VERB bach_ADJ ._PUNCT
Ma'_VERBUNCT 'n_PART rhaid_VERB i_ADP rywun_NOUN golli_VERB ._PUNCT
Dim_PRON ond_ADP un_NUM sy_AUX 'n_PART gallu_VERB ennill_VERB ._PUNCT
Dyna_DET bebe_NOUN sy_VERB mor_ADV gyffrous_ADJ am_ADP redeg_VERB ras_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON wir_ADV eisiau_VERB ennill_VERB ond_CONJ mae_AUX enwg_VERB yn_PART gynt_VERB na_CONJ fi_PRON ._PUNCT
Felly_CONJ ti_PRON am_ADP roi_VERB gorau_ADJ iddi_ADP ?_PUNCT
Ti_PRON 'm_DET hyd_NOUN yn_PART oed_NOUN yn_PART trio_VERB ?_PUNCT
Mae_VERB 'n_PART well_ADJ na_CONJ cholli_VERB ._PUNCT
Ôô_NOUN <_SYM eid_VERB >_SYM mama_NOUN mia_NOUN <_SYM /_PUNCT eid_VERB >_SYM ._PUNCT
Dydi_VERB hyn_PRON ddim_PART yn_PART dda_ADJ ._PUNCT
Ddim_PART yn_PART dda_ADJ o_ADP gwbl_NOUN ._PUNCT
Rhaid_VERB i_ADP ti_PRON drio_VERB dy_DET ore_NOUN '_PUNCT bob_DET tro_NOUN ._PUNCT
Ddim_PART rhoi_VERB 'r_DET gore_NOUN '_PUNCT iddi_ADP cyn_ADP dechre_ADV '_PUNCT ._PUNCT
Ydach_AUX chi_PRON wedi_PART ennill_VERB neu_CONJ golli_VERB rhywbeth_NOUN o_ADP 'r_DET blaen_NOUN ?_PUNCT
'_PUNCT Dw_AUX i_PRON 'di_PART ennill_VERB yn_PART rhedeg_VERB ._PUNCT
'_PUNCT Nes_ADV i_ADP sgorio_VERB y_DET cais_NOUN olaf_ADJ un_NUM diwrnod_NOUN yn_PART rygbi_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON 'di_PART colli_VERB gêm_NOUN o_ADP sgrabl_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON 'di_PART ennill_VERB gêm_NOUN monopoli_NOUN ._PUNCT
Rydw_AUX i_ADP wedi_PART ennill_VERB y_DET mabolgampau_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON wedi_PART colli_VERB ras_NOUN yn_ADP yr_DET ysgol_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON yn_PART colli_VERB rhai_PRON o_ADP weithiau_ADV ond_CONJ dim_PRON llawer_NOUN ._PUNCT
Reit_VERB '_PUNCT ta_CONJ enwg_VERB ._PUNCT
Tybed_ADV bebe_NOUN alla'_VERBUNCT i_PRON neud_VERB i_ADP dy_DET helpu_VERB di_PRON beidio_ADV ofni_VERB colli_VERB ?_PUNCT
326.872_NOUN
Ti_PRON 'n_PART gweld_VERB enwg_VERB ._PUNCT
Mae_AUX canlyniad_NOUN ras_NOUN yn_PART gallu_VERB newid_NOUN ._PUNCT
Mae_VERB dy_DET falwoden_NOUN di_PRON wedi_PART ennill_VERB un_NUM ras_NOUN a_CONJ cholli_VERB un_NUM ras_NOUN ._PUNCT
A_CONJ fel_ADP 'na_ADV mama_NOUN pob_DET ras_NOUN ._PUNCT
Dwyt_VERB ti_PRON byth_ADV yn_PART gwybod_VERB pwy_PRON sy_AUX 'n_PART mynd_VERB i_ADP ennill_VERB a_CONJ phwy_PRON sy_AUX 'n_PART mynd_VERB i_ADP golli_VERB ._PUNCT
Gall_VERB unrhyw_DET beth_PRON ddigwydd_VERB ar_ADP y_DET diwrnod_NOUN ._PUNCT
Ti_PRON wir_ADV yn_PART credu_VERB galla'_VERBUNCT i_PRON guro_VERB enwg2_ADV ?_PUNCT
'_PUNCT Nei_PROPN di_PRON byth_ADV w'bod_VERB oni_CONJ bai_NOUN dy_PRON fod_AUX ti_PRON 'n_PART trio_VERB ._PUNCT
A_CONJ thrio_NOUN yw_VERB 'r_DET peth_NOUN pwysig_ADJ ._PUNCT
'_PUNCT All_NOUN '_PUNCT nad_PART wyt_AUX ti_PRON 'n_PART ennill_VERB bydd_VERB 'na_ADV ras_NOUN arall_ADJ i_ADP ti_PRON drio_VERB rhywbryd_ADV arall_ADJ ._PUNCT
Trio_VERB sy_VERB 'n_PART bwysig_ADJ ._PUNCT
A_CONJ joio_NOUN ._PUNCT
'_PUNCT Na_VERB i_ADP fyth_ADV yn_PART gw'bod_VERB os_CONJ na_PART '_PUNCT dw_VERB i_PRON 'n_PART ail_ADJ ._PUNCT
Yn_PART union_ADJ ._PUNCT
A_CONJ ti_PRON 'n_PART gw'bod_VERB beth_PRON ?_PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART meddwl_VERB fod_VERB angen_NOUN anrheg_NOUN fach_ADJ arnat_ADP ti_PRON i_PART helpu_VERB ti_PRON redeg_VERB y_DET ras_NOUN ._PUNCT
Anrheg_NOUN ?_PUNCT
I_ADP fi_PRON ?_PUNCT
Waw_VERB ._PUNCT
Sut_ADV yn_ADP y_DET byd_NOUN ?_PUNCT
Beth_PRON yw_VERB 'r_DET neges_NOUN ar_ADP yr_DET anrheg_NOUN ?_PUNCT
"_PUNCT Pob_DET lwc_NOUN enwg_NOUN ._PUNCT
Rheda_VERB nerth_NOUN dy_DET traed_NOUN "_PUNCT ._PUNCT
[_PUNCT Agor_PROPN yr_DET anrheg_NOUN ]_PUNCT ._PUNCT
Waw_VERB ._PUNCT
Sgidie_NOUN '_PUNCT rhedeg_VERB ._PUNCT
Diolch_VERB enwb_NOUN ._PUNCT
Croeso_INTJ ._PUNCT
Ond_CONJ cofia_VERB enwg_VERB trio_ADV sy_VERB 'n_PART bwysig_ADJ ._PUNCT
Nid_PART ennill_VERB ._PUNCT
Bydd_VERB ennill_VERB yn_PART neis_ADJ ._PUNCT
Pob_DET lwc_NOUN enwg1_ADJ ._PUNCT
403.152_NOUN
407.999_VERB
463.928_NOUN
464.025_ADJ
464.725_NOUN
548.399_VERB
781.995_NOUN
