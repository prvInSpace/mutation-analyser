Crynodeb_NOUN Rownd_PROPN a_CONJ Rownd_X W_NUM 51_NUM
Mae_VERB diwrnod_NOUN yr_DET arwerthiant_NOUN corachod_NOUN wedi_PART cyrraedd_VERB ,_PUNCT ac_CONJ mae_AUX pawb_PRON yn_PART mwynhau_VERB bwrlwm_NOUN a_CONJ miri_VERB 'r_DET Nadolig_PROPN ._PUNCT
Yr_DET unig_ADJ un_NUM sydd_VERB ddim_PART yn_PART fodlon_ADJ ymuno_VERB yn_ADP yr_DET hwyl_NOUN ydy_VERB David_PROPN ._PUNCT
Mi_PART ddaw_VERB hi_PRON 'n_PART amlwg_ADJ wrth_ADP i_ADP 'r_DET noswaith_NOUN fynd_VERB rhagddi_VERB bod_VERB ganddo_ADP fo_PRON bethau_NOUN eraill_ADJ ar_ADP ei_DET feddwl_VERB ._PUNCT
Daw_VERB Sophie_PROPN i_PART wybod_VERB bod_AUX Wyn_PROPN wedi_PART ei_PRON thwyllo_VERB yn_ADP y_DET trafodaethau_VERB ynglŷn_ADP â_CONJ phrynu_VERB les_NOUN y_DET caffi_NOUN ,_PUNCT siop_NOUN a_CONJ 'r_DET salon_NOUN ,_PUNCT ac_CONJ er_CONJ iddi_ADP hi_PRON wylltio_VERB a_CONJ thynnu_VERB ei_DET chynnig_NOUN yn_PART ôl_NOUN ,_PUNCT erbyn_ADP diwedd_NOUN y_DET dydd_NOUN llwydda_VERB Wyn_PROPN a_CONJ hithau_PRON i_PART ddod_VERB i_ADP gytundeb_NOUN ._PUNCT
Draw_ADV yn_ADP nhŷ_NOUN Llio_PROPN ac_CONJ Iolo_PROPN ,_PUNCT gwelwn_VERB Erin_PROPN yn_PART gweithredu_VERB cynllwyn_NOUN sinistr_ADJ iawn_ADV ,_PUNCT sy_VERB 'n_PART siŵr_ADJ o_ADP gael_VERB oblygiadau_NOUN difrifol_ADJ ._PUNCT
Dydy_VERB pethau_NOUN ddim_PART yn_PART edrych_VERB yn_PART rhy_ADV dda_ADJ i_ADP Philip_PROPN chwaith_ADV wrth_ADP iddo_ADP gyrraedd_VERB Alwena_PROPN yn_ADP Sbaen_PROPN ._PUNCT
Daw_VERB i_PRON ddeall_VERB bod_VERB gan_ADP Alwena_PROPN gynlluniau_NOUN cwbl_ADV wahanol_ADJ ar_ADP gyfer_NOUN y_DET dyfodol_NOUN ac_CONJ mae_AUX hyn_PRON yn_PART arwain_VERB at_ADP dor_NOUN -_PUNCT calon_NOUN llwyr_ADJ ._PUNCT
Rownd_NOUN a_CONJ Rownd_PROPN
Mawrth_NOUN ac_CONJ Iau_VERB 7.30_SYM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Omnibws_VERB dydd_NOUN Sul_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Gwefan_NOUN :_PUNCT s4c.cymru_NOUN
Cynhyrchiad_NOUN Rondo_NOUN Media_PROPN ar_ADP gyfer_NOUN S4C_PROPN
