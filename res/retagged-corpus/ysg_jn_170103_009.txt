Rhif_NOUN adnabod_VERB y_DET Rhaglen_PROPN :_PUNCT
Teitl_NOUN y_DET Rhaglen_PROPN :_PUNCT
CYTUNDEB_VERB TRWYDDEDU_X AR_X GYFER_PROPN RHAGLENNI_PROPN A_X GOMISIYNWYD_VERB GAN_ADP S4C_PROPN DAN_PROPN GO_PROPN ̂_PUNCT D_NUM YMARFER_NUM A_X THELERAU_X MASNACH_PROPN S4C_PROPN 2007_NUM
Gwnaed_VERB y_DET Cytundeb_NOUN hwn_DET ar_ADP y_DET
[_PUNCT Ond_CONJ bernir_VERB bod_VERB y_DET Cytundeb_NOUN hwn_DET yn_PART weithredol_ADJ ers_ADP ]_PUNCT
RHWNG_PROPN
(_PUNCT 1_NUM )_PUNCT S4C_PROPN o_ADP Barc_PROPN Ty_PROPN ̂_PUNCT Glas_PROPN ,_PUNCT Llanisien_PROPN ,_PUNCT Caerdydd_PROPN ,_PUNCT CF_PROPN 14_NUM 5_NUM DU_PROPN (_PUNCT "_PUNCT S4C_PROPN "_PUNCT )_PUNCT ;_PUNCT a_CONJ
(_PUNCT 2_NUM )_PUNCT
CYTUNIR_NOUN FEL_X A_X GANLYN_NUM :_PUNCT
1_NUM ._PUNCT
Mewn_ADP cydnabyddiaeth_NOUN am_ADP yr_DET ymrwymiadau_NOUN ar_ADP ran_NOUN y_DET Cynhyrchydd_NOUN a_CONJ gynhwysir_VERB yn_ADP y_DET Cytundeb_NOUN ,_PUNCT mae_AUX S4C_PROPN yn_PART comisiynu_VERB 'r_DET Cynhyrchydd_PROPN ac_CONJ mae_AUX 'r_DET Cynhyrchydd_NOUN yn_PART gwarantu_VERB cytuno_VERB ac_CONJ yn_PART ymrwymo_VERB i_ADP gynhyrchu_VERB a_CONJ Chyfleu_PROPN Rhaglen_PROPN S4C_PROPN i_ADP S4C_PROPN yn_PART unol_ADJ a_CONJ ̂_SYM thelerau_NOUN 'r_DET Cytundeb_NOUN ._PUNCT
Mae_VERB 'r_DET term_NOUN "_PUNCT y_DET Cytundeb_PROPN "_PUNCT yn_PART golygu_VERB ar_ADP y_DET cyd_NOUN (_PUNCT 1_NUM )_PUNCT y_DET Cytundeb_NOUN hwn_DET (_PUNCT 2_NUM )_PUNCT yr_DET Atodiad_NOUN sydd_VERB ynghlwm_ADV a_CONJ ̂_VERB 'r_DET Cytundeb_NOUN hwn_DET (_PUNCT 3_NUM )_PUNCT y_DET Telerau_NOUN Cyffredinol_ADJ sydd_AUX i_ADP 'w_PRON cael_VERB ar_ADP http_X :_PUNCT //_X www.s4c.co.uk_ADJ /_PUNCT production_VERB /_PUNCT downloads_PRON /_PUNCT forms_NOUN /_PUNCT c.pdf_NOUN ac_CONJ sydd_AUX wedi_PART 'u_PRON hymgorffori_VERB yn_ADP y_DET Cytundeb_NOUN fel_ADP pe_CONJ baent_VERB wedi_PART 'u_PRON hatgynhyrchu_VERB yn_PART llawn_ADJ isod_ADV a_CONJ (_PUNCT 4_NUM )_PUNCT yr_DET holl_DET ddogfennau_NOUN eraill_ADJ sydd_AUX wedi_PART 'u_PRON hymgorffori_VERB yn_ADP y_DET Cytundeb_NOUN trwy_ADP gyfeiriadaeth_NOUN ._PUNCT
2_NUM ._PUNCT
Mewn_ADP cydnabyddiaeth_NOUN am_ADP i_ADP S4C_PROPN dalu_VERB 'r_DET Ffi_NOUN Drwydded_PROPN i_ADP 'r_DET Cynhyrchydd_PROPN ,_PUNCT mae_AUX 'r_DET Cynhyrchydd_NOUN yn_PART ddi-_NOUN alw_VERB 'n_PART o_ADJ ̂_SYM l_PRON yn_PART rhoi_VERB Hawliau_NOUN S4C_ADJ i_ADP S4C_PROPN a_CONJ 'i_PRON Chwmni_NOUN ̈_VERB au_DET Teulu_PROPN yn_ADP ystod_NOUN Cyfnod_PROPN y_DET Drwydded_NOUN ac_CONJ unrhyw_DET Gyfnod_NOUN Estynedig_ADJ y_DET Drwydded_PROPN ._PUNCT
3_NUM ._PUNCT
Dehonglir_VERB yr_DET ymadroddion_NOUN a_CONJ ddefnyddir_VERB yn_ADP y_DET Cytundeb_NOUN yn_PART unol_ADJ a_CONJ ̂_AUX 'r_DET Telerau_NOUN Cyffredinol_ADJ a_CONJ 'r_DET manylion_NOUN yn_ADP yr_DET Atodiad_NOUN ._PUNCT
Arwyddwyd_VERB a_CONJ chymeradwywyd_VERB gan_ADP gynrychiolydd_NOUN awdurdodedig_ADJ dros_ADP ac_CONJ ar_ADP ran_NOUN S4C_PROPN :_PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT (_PUNCT teitl_NOUN )_PUNCT
Arwyddwyd_VERB a_CONJ chymeradwywyd_VERB gan_ADP gynrychiolydd_NOUN awdurdodedig_ADJ dros_ADP ac_CONJ ar_ADP ran_NOUN y_DET Cynhyrchydd_NOUN :_PUNCT
._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT (_PUNCT teitl_NOUN )_PUNCT
YR_DET ATODIAD_PROPN
RHAN_NOUN I_NUM
BRI_X ̂_ADP FF_PROPN GOLYGYDDOL_PROPN A_CONJ BUSNES_NUM Gweler_PROPN y_DET ddogfen_NOUN sydd_VERB ynghlwm_ADV
5_NUM ._PUNCT
Amcangyfrif_NOUN Cyfanswm_PROPN y_DET Gost_PROPN
Atodlen_PROPN
Categori_NOUN
Cost_NOUN i_ADP 'r_DET Gyllideb_PROPN
03_NUM Ffi_NOUN Gynhyrchu_PROPN
05_NUM Fformat_PROPN /_PUNCT Stori_PROPN /_PUNCT Sgript_PROPN
06_NUM Cynhyrchydd_PROPN a_CONJ Chyfarwyddwr_PROPN
07_NUM Artistiaid_PROPN
08_NUM Cyflwynwyr_PROPN /_PUNCT Cyfranwyr_PROPN
09_NUM Cyflogau_NOUN 'r_DET Uned_NOUN Gynhyrchu_PROPN
10_NUM Cyfarwyddo_NOUN Cynorthwyol_ADJ a_CONJ Dilyniant_PROPN
11_NUM Criw_NOUN -_PUNCT Camera_PROPN
12_NUM Criw_NOUN -_PUNCT Sain_PROPN
13_NUM Criw_NOUN -_PUNCT Goleuo_PROPN
14_NUM Criw_NOUN -_PUNCT Adran_NOUN Gelf_NOUN
15_NUM Criw_NOUN -_PUNCT Gwisgoedd_NOUN /_PUNCT Colur_NOUN /_PUNCT Gwallt_PROPN
16_NUM Criw_NOUN -_PUNCT Golygu_VERB
17_NUM Criw_NOUN -_PUNCT Eraill_PROPN
18_NUM Gorbenion_NOUN Perthnasol_ADJ i_ADP Gyflogau_NOUN
19_NUM Defnyddiau_PROPN -_PUNCT Adran_NOUN Gelf_NOUN
20_NUM Defnyddiau_PROPN -_PUNCT Gwisgoedd_NOUN /_PUNCT Colur_NOUN /_PUNCT Gwallt_PROPN
21_NUM Cynhyrchu_PROPN -_PUNCT Cyfarpar_PROPN
22_NUM Adnoddau_NOUN -_PUNCT Criwiau_PROPN /_PUNCT Cyfarpar_PROPN
23_NUM Stiwdio_PROPN /_PUNCT Darlledu_NOUN Allanol_ADJ
24_NUM Adnoddau_NOUN Cynhyrchu_NOUN Eraill_PROPN
25_NUM Stoc_PROPN Ffilm_PROPN /_PUNCT Ta_CONJ ̂_SYM p_ADJ
26_NUM Ffilm_PROPN -_PUNCT O_ADP ̂_SYM l_PRON -_PUNCT gynhyrchu_VERB
27_NUM Ta_CONJ ̂_SYM p_INTJ -_PUNCT O_ADP ̂_SYM l_PRON -_PUNCT gynhyrchu_VERB
28_NUM Defnydd_NOUN Archif_PROPN
29_NUM Rostrwm_PROPN /_PUNCT Graffeg_PROPN
30_NUM Cerddoriaeth_NOUN
31_NUM Teithio_PROPN a_CONJ Chludo_PROPN
32_NUM Gwesty_NOUN a_CONJ Chynhaliaeth_NOUN
33_NUM Costau_NOUN Cynhyrchu_PROPN Eraill_PROPN
34_NUM Yswiriant_NOUN /_PUNCT Cyllid_PROPN /_PUNCT Cyffredinol_ADJ
35_NUM Gorbenion_NOUN Cynhyrchu_PROPN
36_NUM Perfformiadau_NOUN Theatr_PROPN /_PUNCT Is_X Deitlo_PROPN
Amcangyfrif_NOUN Cyfanswm_PROPN y_DET Gost_PROPN
Cyfanswm_NOUN yr_DET Elfennau_NOUN Allweddol_ADJ
Ffi_NOUN Drwydded_NOUN S4C_PROPN i_ADP 'r_DET Cynhyrchydd_NOUN Yswiriant_PROPN
Cyfraniad_NOUN i_ADP Cyfle_PROPN
Blaendaliadau_NOUN Eraill_PROPN
Cyfanswm_NOUN Ffi_NOUN Drwydded_PROPN S4C_PROPN
Blaendaliadau_NOUN 'r_DET Cynhyrchydd_PROPN
RHAN_NOUN 2_NUM
TELERAU_NOUN PENODOL_X Y_DET RHAGLEN_PROPN
A_CONJ ._PUNCT
Hawliau_NOUN S4C_PROPN
Er_ADP mwyn_NOUN osgoi_VERB unrhyw_DET amheuaeth_NOUN ,_PUNCT bydd_VERB Hawliau_NOUN S4C_ADJ fel_ADP a_NOUN -_PUNCT ganlyn_NOUN :_PUNCT Yr_DET Hawliau_NOUN Craidd_ADJ [_PUNCT heblaw_ADP fel_ADP yr_PART amrywir_VERB hwy_PRON isod_ADV :_PUNCT ]_PUNCT
B_NUM Cliriadau_PROPN
1_NUM ._PUNCT
2_NUM ._PUNCT
Bydd_VERB gan_ADP S4C_PROPN yr_DET hawl_NOUN i_ADP We_PROPN -_PUNCT ddarlledu_VERB Rhaglen_PROPN S4C_PROPN yn_PART unol_ADJ a_CONJ ̂_AUX 'r_DET Telerau_NOUN Cyffredinol_ADJ yn_ADP ystod_NOUN Cyfnod_PROPN y_DET Drwydded_NOUN ac_CONJ unrhyw_DET Gyfnod_NOUN Estynedig_ADJ y_DET Drwydded_NOUN yn_PART rhydd_ADJ ac_CONJ yn_PART glir_ADJ o_ADP unrhyw_DET a_CONJ phob_DET hawliad_NOUN ,_PUNCT cyfyngiad_NOUN a_CONJ llyffethair_NOUN a_CONJ heb_ADP da_ADJ ̂_ADV l_PART pellach_ADJ i_ADP 'r_DET Cynhyrchydd_PROPN neu_CONJ unrhyw_DET gyfrannwr_NOUN i_ADP Raglen_PROPN S4C_PROPN neu_CONJ i_ADP drydydd_NOUN parti_NOUN AC_X EITHRIO_X :_PUNCT
C_NUM Cyfnod_NOUN y_DET Drwydded_PROPN
5_NUM mlynedd_NOUN o_ADP ddyddiad_NOUN Darllediad_NOUN cyntaf_ADJ Rhaglen_PROPN S4C_PROPN ar_ADP Wasanaethau_PROPN S4C_PROPN ._PUNCT
CH_NOUN Atodlen_NOUN y_DET Deunydd_NOUN Cyfleu_PROPN
Cyfle_PROPN ̈_VERB ir_ADP y_DET deunydd_NOUN canlynol_ADJ i_ADP S4C_PROPN erbyn_ADP y_DET dyddiadau_NOUN canlynol_ADJ ,_PUNCT ac_CONJ os_CONJ methir_VERB a_CONJ ̂_ADV gwneud_VERB hynny_PRON (_PUNCT mewn_ADP unrhyw_DET achosion_NOUN unigol_ADJ )_PUNCT bydd_VERB gan_ADP S4C_PROPN yr_DET hawl_NOUN i_ADP ddidynnu_VERB iawndal_NOUN ariannol_ADJ penodedig_ADJ o_ADP y_DET diwrnod_NOUN o_ADP ran_NOUN y_DET Copi_NOUN Darlledu_VERB a_CONJ £_SYM 50.00_NUM y_DET diwrnod_NOUN o_ADP ran_NOUN y_DET gweddill_NOUN o_ADP unrhyw_DET arian_NOUN sy_VERB 'n_PART ddyledus_ADJ i_ADP 'r_DET Cynhyrchydd_PROPN neu_CONJ i_ADP 'w_PRON hawlio_VERB fel_CONJ dyled_NOUN sy_VERB 'n_PART daladwy_ADJ gan_ADP y_DET Cynhyrchydd_PROPN yn_PART syth_ADJ ar_ADP hawliad_NOUN S4C_PROPN :_PUNCT
Co_NOUN ̀_VERB d_DET
Deunydd_PROPN
Pennod_PROPN
Dyddiad_NOUN
Amser_PROPN
Cyfleu_PROPN
D_NUM Unrhyw_DET amodau_NOUN arbennig_ADJ eraill_ADJ
RHAN_NOUN 3_NUM
DYLIF_NOUN ARIANNOL_PROPN A_CONJ CHERRIG_NUM MILLTIR_SYM
Gweler_VERB y_DET Dylif_NOUN Ariannol_ADJ sydd_VERB ynghlwm_ADV
Cerrig_VERB Milltir_PROPN
Cymal_NOUN neu_CONJ elfen_NOUN i_ADP 'w_PRON g_NOUN /_PUNCT chymeradwyo_VERB
Dyddiad_NOUN ei_DET g_NOUN /_PUNCT chyflwyno_VERB i_ADP 'r_DET Golygydd_NOUN Cynnwys_PROPN
y_DET Cyfnod_NOUN Cymeradwyo_PROPN
a_CONJ ._PUNCT
Braslun_NOUN o_ADP 'r_DET Rhaglen_PROPN
b_PRON ._PUNCT
Trefn_NOUN y_DET Rhaglen_NOUN /_PUNCT Triniaeth_PROPN
c_ADJ ._PUNCT
Amserlen_PROPN Gynhyrchu_PROPN Fras_NOUN Persone_NOUN ́_NOUN l_ADP Allweddol_PROPN
Prif_NOUN Artistiaid_PROPN
ch_ADJ ._PUNCT
Sgript_PROPN
d_ADJ ._PUNCT
Amserlen_PROPN Saethu_PROPN
Cynhyrchu_PROPN
Rushes_PROPN
dd_VERB ._PUNCT
Toriad_NOUN Bras_PROPN
e_PRON ._PUNCT
Toriad_NOUN Cain_PROPN /_PUNCT Fersiwn_PROPN '_PUNCT On_X -_PUNCT Line_NOUN '_PUNCT Terfynol_PROPN
f_PRON ._PUNCT
Cydnabyddiaethau_VERB ar_ADP y_DET sgrin_NOUN
ff_VERB ._PUNCT
Y_PART troslais_VERB Terfynol_PROPN
g_NOUN ._PUNCT
Copi_PART Darlledu_VERB /_PUNCT Derbyn_PROPN yn_PART Dechnegol_ADJ
Yn_PART unol_ADJ a_CONJ ̂_X Chymal_PROPN 3_NUM yr_DET Amodau_NOUN Cyffredinol_ADJ ac_CONJ Adran_NOUN 6_NUM y_DET Telerau_NOUN Masnach_PROPN
