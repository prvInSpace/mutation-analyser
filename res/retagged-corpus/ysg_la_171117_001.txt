Annwyl_NOUN Riant_X /_PUNCT Warcheidwad_PROPN ,_PUNCT
27_NUM ain_NOUN Tachwedd_NOUN ,_PUNCT 2017_NUM
Ysgrifennwn_VERB atoch_ADP yngly_VERB ̂_ADV n_PRON a_CONJ ̂_ADJ chau_VERB 'r_DET ysgol_NOUN petai_VERB 'r_DET tywydd_NOUN yn_PART troi_VERB 'n_PART wael_ADJ ._PUNCT
Mae_VERB 'n_PART amlwg_ADJ nad_PART ydym_AUX yn_PART gallu_VERB nodi_VERB 'n_PART bendant_ADJ yr_DET hyn_PRON fydd_AUX yn_PART digwydd_VERB ond_CONJ o_ADP leiaf_ADJ gallwn_VERB roi_VERB rhybudd_NOUN o_ADP 'r_DET hyn_PRON all_ADP ddigwydd_NOUN petai_VERB eira_NOUN 'n_PART cwympo_VERB ._PUNCT
Unwaith_ADV i_ADP 'r_DET Prifathro_NOUN benderfynu_AUX bod_AUX yn_PART rhaid_VERB cau_VERB 'r_DET ysgol_NOUN (_PUNCT gwneir_VERB hyn_PRON ar_ADP sail_NOUN Iechyd_NOUN a_CONJ Diogelwch_PROPN wedi_PART ymgynghori_VERB a_CONJ ̂_ADJ Chadeirydd_NOUN y_DET Llywodraethwyr_NOUN a_CONJ 'r_DET Sir_NOUN )_PUNCT yna_ADV ,_PUNCT rydym_AUX yn_PART ffonio_VERB bob_DET cwmni_NOUN bws_NOUN ._PUNCT
Tair_NOUN llinell_NOUN ffo_NOUN ̂_SYM n_PRON yn_PART unig_ADJ sydd_VERB i_ADP 'r_DET ysgol_NOUN felly_ADV ar_ADP adeg_NOUN fel_ADP hyn_PRON mae_AUX 'n_PART gallu_VERB bod_VERB yn_PART anodd_ADJ i_PART gysylltu_VERB a_CONJ ̂_AUX 'r_DET ysgol_NOUN ac_CONJ rydym_VERB yn_PART ddiolchgar_ADJ am_ADP eich_DET amynedd_NOUN ._PUNCT
Gofynnwn_VERB ichi_ADP ein_PRON cynorthwyo_VERB ni_PRON drwy_ADP
•_NOUN Sicrhau_ADV bod_VERB allwedd_NOUN i_ADP 'r_DET ty_NOUN ̂_ADJ gan_ADP eich_DET plentyn_NOUN neu_CONJ ei_DET fod_VERB ef_PRON neu_CONJ hi_PRON yn_PART gwybod_VERB lle_ADV gall_VERB ddod_VERB o_ADP hyd_NOUN i_ADP allwedd_NOUN ._PUNCT
•_NOUN Os_CONJ nad_PART yw_VERB 'r_DET uchod_NOUN yn_PART addas_ADJ ,_PUNCT sicrhau_ADV bod_AUX eich_DET plentyn_NOUN yn_PART gwybod_VERB at_ADP bwy_PRON y_PART dylent_NOUN fynd_VERB os_CONJ ydynt_AUX yn_PART cael_VERB eu_DET danfon_VERB adref_ADV ._PUNCT
Cofiwch_VERB ,_PUNCT gall_VERB bws_PRON Ysgol_PROPN ond_CONJ cludo_VERB disgyblion_NOUN arbennig_ADJ ac_CONJ fe_PART fyddant_VERB yn_PART anhebyg_ADJ o_ADP fodloni_NOUN ar_ADP gludo_VERB plant_NOUN sydd_VERB ddim_PART ar_ADP eu_DET rhestri_NOUN ._PUNCT
•_NOUN Sicrhau_X ,_PUNCT yn_PART arbennig_ADJ os_CONJ yw_VERB eich_DET plentyn_NOUN yn_PART ifanc_ADJ ,_PUNCT ei_PRON fod_VERB /_PUNCT bod_AUX yn_PART gwybod_VERB eich_DET rhif_NOUN ffo_ADP ̂_SYM n_PRON yn_ADP y_DET gwaith_NOUN neu_CONJ rif_NOUN ffo_ADJ ̂_SYM n_PART symudol_ADJ ._PUNCT
Oherwydd_CONJ yr_DET amgylchiadau_NOUN efallai_ADV na_PART fyddant_VERB yn_PART ffonio_VERB tan_ADP ar_ADP o_PRON ̂_SYM l_CONJ iddynt_ADP gyrraedd_VERB adref_ADV ._PUNCT
Yn_PART olaf_ADJ ,_PUNCT fel_CONJ y_DET gwyddoch_VERB mae_VERB 'r_DET safle_NOUN hon_DET yn_PART agored_ADJ iawn_ADV ac_CONJ os_CONJ yw_VERB 'r_DET tywydd_NOUN yn_PART oer_ADJ gofynnwn_VERB ichi_ADP sicrhau_ADV bod_AUX eich_DET plentyn_NOUN yn_PART gwisgo_VERB 'n_PART addas_ADJ ac_CONJ o_ADP fewn_ADP rheolau_NOUN 'r_DET ysgol_NOUN ._PUNCT
Dylai_VERB fod_VERB scarffiau_NOUN ,_PUNCT menyg_NOUN a_CONJ hetiau_NOUN yn_PART cydymffurfio_VERB a_CONJ ̂_ADJ lliwiau_VERB 'r_DET ysgol_NOUN ,_PUNCT sef_CONJ ,_PUNCT gwyrdd_ADJ ,_PUNCT glas_ADJ tywyll_ADV neu_CONJ du_ADJ ,_PUNCT a_CONJ dylai_VERB cotiau_NOUN hefyd_ADV fod_VERB yn_PART dywyll_ADJ ._PUNCT
Diolch_VERB am_ADP eich_DET cydweithrediad_NOUN ._PUNCT
Yr_DET eiddoch_PRON yn_PART gywir_ADJ
enwb_VERB cyfenw_NOUN __NOUN
Dirprwy_NOUN Bennaeth_PROPN
