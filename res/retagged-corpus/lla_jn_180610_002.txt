Bore_NOUN da_ADJ ._PUNCT
Y_DET bore_NOUN 'ma_ADV mae_VERB gennym_ADP achos_NOUN gwirioneddol_ADJ i_ADP ddathlu_VERB ._PUNCT
Anghofiwch_VERB y_DET briodas_NOUN fawr_ADJ dair_NUM w'thnos_NOUN yn_PART ôl_NOUN ._PUNCT
Heddiw_ADV rydyn_AUX ni_PRON 'n_PART dathlu_VERB tri_NUM gŵr_NOUN arwrol_ADJ o_ADP Orllewin_NOUN Cymru_PROPN a_CONJ newidiodd_VERB gwrs_NOUN gwlad_NOUN gyfan_ADJ ben_NOUN arall_ADJ y_DET byd_NOUN ._PUNCT
Mae_AUX 'n_PART stori_NOUN mor_ADV wefreiddiol_ADJ mae_AUX 'n_PART heiddi_VERB film_NOUN blocbyster_NOUN ar_ADP raddfa_NOUN Hollywood_PROPN amdani_ADP ._PUNCT
One_ADV efalla_ADV '_PUNCT mewn_ADP ffordd_NOUN nodweddiadol_ADJ Gymreig_ADJ mae_AUX 'n_PART stori_NOUN sy_AUX 'n_PART parhau_VERB yn_PART un_NUM anhysbys_ADJ i_ADP 'r_DET rhan_NOUN fwyaf_ADJ ohonom_ADP ._PUNCT
Heddiw_ADV yng_ADP nghapel_NOUN bach_ADJ gwledig_ADJ capel_NOUN ger_ADP lleoliad_NOUN mi_PRON fydd_VERB yr_DET ystafell_NOUN dan_ADP ei_DET siang_NOUN gyda_ADP phobl_NOUN yn_PART cofio_VERB am_ADP gyfraniad_NOUN bechgyn_NOUN o_ADP 'r_DET ardal_NOUN ac_CONJ yn_PART moli_VERB 'r_DET duw_NOUN a_CONJ 'u_PRON symbylodd_VERB i_ADP weithredu_VERB 'u_PRON aberthyn_NOUN fawr_ADJ ac_CONJ i_PART newid_VERB y_DET byd_NOUN ._PUNCT
Am_ADP bebe_NOUN dw_AUX i_PRON 'n_PART sôn_VERB ?_PUNCT
Wel_CONJ am_ADP fenter_DET dau_NUM Gardi_PROPN ifanc_ADJ yn_PART eu_DET hugainia'_VERBUNCT cynnar_ADJ enwg_VERB cyfenw_NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN eu_DET gwragedd_NOUN a_CONJ 'u_PRON plant_NOUN a_CONJ adawodd_VERB eu_PRON cartrefi_VERB yng_ADP nghefn_NOUN gwlad_NOUN Cymru_PROPN a_CONJ hwylio_VERB am_ADP bum_ADJ mis_NOUN cyn_ADP cyrra'dd_NOUN ynys_NOUN belledig_ADJ ynys_NOUN fawr_ADJ __NOUN __NOUN __NOUN __NOUN __NOUN union_ADJ ddau_NUM gan_ADP mlynedd_NOUN yn_PART ôl_NOUN ._PUNCT
Y_DET trydydd_ADJ arwr_NOUN o'dd_NOUN enwg_VERB cyfenw_NOUN o_ADP __NOUN __NOUN __NOUN __NOUN a_CONJ deithiodd_VERB ddwy_NUM waith_NOUN i_ADP 'r_DET ynys_NOUN i_ADP barhau_VERB a_CONJ 'r_DET genhad'eth_NOUN ._PUNCT
Ar_ADP y_DET bore_NOUN 'ma_ADV byddwn_AUX nhw_PRON 'n_PART hefyd_ADV yn_PART ystyried_VERB eu_DET gwaith_NOUN a_CONJ 'i_PRON hymateb_NOUN fyddlon_ADJ i_ADP orchymyn_NOUN Iesu_X Grist_PROPN i_ADP fynd_VERB i_ADP holl_DET genhedloedd_NOUN y_DET byd_NOUN ._PUNCT
I_PART ddweud_VERB wrth_ADP eraill_PRON am_ADP eu_DET fawredd_NOUN a_CONJ 'i_PRON gariad_NOUN ._PUNCT
[_PUNCT Clip_NOUN newydd_ADJ ]_PUNCT ._PUNCT
Y_DET llynedd_NOUN yn_ADP ystod_NOUN mis_NOUN Hydref_PROPN fe_PART ges_VERB i_ADP 'r_DET fraint_NOUN o_ADP deithio_VERB gyda_ADP dau_NUM arall_ADJ i_ADP lleoliad_NOUN a_CONJ ran_VERB undeb_NOUN yr_DET sefydliad_NOUN ._PUNCT
Roeddwn_VERB '_PUNCT inne_NOUN y_DET parchedig_NOUN enwg_VERB cyfenw_NOUN a_CONJ 'r_DET ffotograffydd_NOUN __NOUN __NOUN __NOUN __NOUN yno_ADV i_PART weld_VERB gwaith_NOUN yr_DET eglwys_NOUN sef_CONJ eglwys_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN a_CONJ chasglu_VERB gwybodaeth_NOUN ar_ADP gyfer_NOUN apêl_NOUN fawr_ADJ yr_DET undeb_NOUN ._PUNCT
Apêl_NOUN sydd_VERB newydd_ADV cael_VERB 'i_PRON lansio_VERB yr_DET w'thnos_NOUN hon_DET lleoliad_NOUN yw_VERB degfed_VERB gwlad_NOUN dlota_VERB 'r_DET byd_NOUN ac_CONJ  _SPACE gwaetha_ADJ 'i_PRON ei_DET chyfoeth_NOUN naturiol_ADJ y_DET beioamrywiaeth_NOUN anhygoel_ADJ a_CONJ 'r_DET anifeiliad_NOUN gwyllt_ADJ unigryw_ADJ mama_NOUN rhan_NOUN fwyaf_ADJ o_ADP bobl_NOUN y_DET wlad_NOUN yn_PART byw_VERB mewn_ADP tlodi_VERB enbyd_NUM ._PUNCT
Mae_VERB o_ADP ddeiti_NOUN pum_NUM miliwn_NUM o_ADP drigolion_NOUN y_DET wlad_NOUN yn_PART mynychu_VERB eglwysi_VERB 'r_DET eglwys_NOUN ffigwr_NOUN sy_VERB 'n_PART swm_NOUN aruthrol_ADJ ac_CONJ yn_PART dystiola_VERB 'th_NOUN o_ADP waddol_NOUN y_PART cenhadon_VERB ifanc_ADJ o_ADP lleoliad_NOUN ._PUNCT
Cafon_VERB ni_PRON 'r_DET anrhydedd_NOUN o_ADP fynychu_NOUN oedfa_NOUN 'r_DET y_DET Sul_PROPN yno_ADV hefyd_ADV gydag_ADP eglwysi_NOUN mawr_ADJ y_DET brif_ADJ ddinas_NOUN yn_PART orlawn_ADJ a_CONJ gwasanaeth_NOUN yn_PART para_VERB am_ADP dair_NUM awr_NOUN a_CONJ rhagor_PRON ._PUNCT
Mae_VERB 'r_DET oedfa_NOUN hon_DET dipyn_NOUN yn_PART llai_PRON o_ADP hyd_NOUN gyda_ADP llaw_NOUN ._PUNCT
Ma'_VERBUNCT Cristnogaeth_NOUN felly_ADV i_ADP 'r_DET hen_ADJ ac_CONJ i_ADP 'r_DET ifanc_ADJ i_ADP 'r_DET cefnog_NOUN a_CONJ 'r_DET tlawd_NOUN yn_PART lleoliad_NOUN yn_PART ffordd_NOUN o_ADP fyw_VERB ._PUNCT
Yn_PART fydd_VERB sy_VERB 'n_PART real_ADJ ac_CONJ mae_AUX arnai_VERB ofn_NOUN yn_PART atsain_NOUN pell_ADJ o_ADP 'n_PRON hanes_NOUN ni_PRON fel_ADP cenedl_NOUN ._PUNCT
Tybed_ADV a_PART oedd_VERB enwg_VERB cyfenw_NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN y_DET genhadon_NOUN ifanc_ADJ o_ADP __NOUN __NOUN __NOUN __NOUN __NOUN yn_PART sylweddoli_VERB ar_ADP y_DET pryd_NOUN gymaint_ADV fyddai_VERB dylanwad_NOUN ar_ADP __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Tybed_ADV wrth_ADP iddynt_ADP hwylio_VERB draw_ADV i_ADP 'r_DET ynys_NOUN ym_ADP mis_NOUN Chwefror_PROPN un_NUM wyth_NUM un_NUM wyth_NUM gyda_ADP 'u_PRON teuluoedd_NOUN tybed_ADV oedden_ADP nhw_PRON 'n_PART dychmygu_VERB y_PART bydde_VERB '_PUNCT ni_PRON heddiw_ADV yn_PART cofio_VERB eu_DET cyfraniad_NOUN ddwy_NUM ganrif_NOUN yn_PART diweddarach_ADJ ?_PUNCT
A_PART phobl_NOUN lleoliad_NOUN yn_PART diolch_NOUN iddynt_ADP o_ADP waelod_NOUN eu_DET calonnau_NOUN ._PUNCT
Roeddent_AUX yn_PART gadael_VERB Cymru_PROPN yn_ADP eu_DET gwendid_NOUN ar_ADP fordaith_NOUN ansicr_NOUN a_CONJ gwlad_NOUN ddie_NOUN th_NOUN '_PUNCT bell_NOUN ._PUNCT
Doedd_VERB ganddyn_ADP nhw_PRON ddim_PART byd_NOUN nodedig_ADJ yn_ADP eu_DET llaw_NOUN i_ADP 'w_PRON gynnig_NOUN i_ADP bobl_NOUN lleoliad_NOUN dim_PRON ond_ADP eu_DET ffydd_NOUN di-_NOUN ysgog_PUNCT yn_ADP Iesu_PROPN â_ADP 'r_DET ysbryd_NOUN pendefynol_ADJ hynny_PRON i_PART 'w_PRON rhannu_VERB ef_PRON ag_ADP eraill_PRON ._PUNCT
Roeddent_VERB mewn_ADP sefyllfa_NOUN debyg_ADJ iawn_ADV i_ADP 'r_DET bachgen_NOUN bach_ADJ yn_ADP yr_DET hanes_NOUN hwn_DET ._PUNCT
A_PART ddarllenir_VERB gan_ADP enwb_NOUN cyfenw_NOUN ._PUNCT
[_PUNCT Clip_NOUN newydd_ADJ ]_PUNCT ._PUNCT
Mae_AUX 'r_DET byd_NOUN yn_PART cynhyrchu_VERB digon_PRON o_ADP fwyd_NOUN i_PART fwydo_VERB pawb_PRON ._PUNCT
yyy_ADV bod_VERB dros_ADP hanner_NOUN biliwn_NUM o_ADP bobl_NOUN yn_PART dioddef_VERB o_ADP ddiffyg_NOUN maeth_NOUN heddiw_ADV ._PUNCT
Ond_CONJ ar_ADP yr_DET un_NUM pryd_NOUN mama_NOUN tua_ADV traean_NOUN o_ADP 'r_DET bwyd_NOUN sy_VERB 'n_PART cae'l_ADJ ei_PRON gynhyrchu_VERB yn_PART ca'l_VERB ei_PRON wastraffu_VERB neu_CONJ ei_PRON golli_VERB ._PUNCT
Ac_CONJ wrth_ADP ystyried_VERB newyn_NOUN a_CONJ diffyg_NOUN maeth_NOUN pobl_NOUN lleoliad_NOUN 'r_DET heddiw_NOUN mi_PRON ddyle_NOUN gorchymyn_NOUN Iesu_PROPN i_ADP beidio_VERB a_CONJ gadael_VERB i_ADP ddim_PART ga'l_VERB ei_PRON wastraffu_VERB ei_DET 'n_PART cyfwrdd_VERB ni_PRON yn_PART unigol_ADJ ac_CONJ yn_PART genedlaethol_ADJ ._PUNCT
A_CONJ beth_PRON bynnag_PRON sy_AUX 'n_PART ca'l_VERB ei_PRON roi_VERB yn_PART nwylo_NOUN Iesu_X yn_PART tyfu_VERB ar_ADP ei_DET ganfed_VERB meddai_VERB 'r_DET apostl_NOUN Paul_PROPN ._PUNCT
Os_CONJ mae_AUX ychydig_ADV '_PUNCT ych_AUX chi_PRON 'n_PART ei_DET hau_NOUN bach_ADJ fydd_VERB yn_PART cynhaeaf_NOUN ._PUNCT
Ond_CONJ os_CONJ '_PUNCT dych_VERB chi_PRON 'n_PART hau_NOUN yn_PART hael_NOUN cewch_VERB gynhaeaf_NOUN mawr_ADJ ._PUNCT
Mi_PART ddyle_VERB '_PUNCT ni_PRON i_ADP gyd_ADP anelu_VERB at_ADP fod_VERB y_DET person_NOUN mwya'_ADJUNCT hael_NOUN ry_AUX 'n_PART ni_PRON 'n_PART ei_PRON 'n_PART anabod_ADJ ._PUNCT
Hael_NOUN gyda_ADP 'r_DET harian_NOUN hael_NOUN gyda_ADP 'n_PART heiddo_NOUN ein_DET hamser_NOUN a_CONJ 'n_PART cariad_NOUN ._PUNCT
Gallwn_VERB ni_PRON ddim_PART rhoi_VERB mwy_ADJ na_CONJ 'r_DET hyn_DET mama_NOUN Duw_NOUN wedi_PART 'i_PRON roi_VERB i_ADP ni_PRON ._PUNCT
A_CONJ 'r_DET mwyaf_NOUN ry_VERB '_PUNCT ni_PRON 'n_PART eu_DET rhoi_VERB y_DET mwyaf_NOUN fydd_VERB ein_DET cynheaf_NOUN a_CONJ bendith_NOUN Duw_NOUN ar_ADP eu_DET 'n_PART bywyda'_VERBUNCT ._PUNCT
Yn_PART union_ADJ wedi_ADP 'r_DET wyrth_NOUN anhygoel_ADJ yma_DET ddigwydd_VERB gyda_ADP 'r_DET disgyblion_NOUN ar_ADP ben_NOUN eu_DET digon_ADV fe_PART ddigwyddodd_VERB rhywbeth_NOUN a_CONJ fyddai_AUX 'n_PART eu_DET cynhyrfu_VERB i_ADP 'r_DET byw_VERB ._PUNCT
[_PUNCT Clip_NOUN newydd_ADJ ]_PUNCT ._PUNCT
Efallai_ADV bod_VERB 'i_PRON 'n_PART haws_ADJ dilyn_VERB Iesu_PROPN a_CONJ bod_AUX yn_PART Gristion_NOUN ymhlith_ADP tyrfa_NOUN fawr_ADJ gyda_ADP phopeth_PRON yn_PART mynd_VERB yn_PART hwyliog_ADJ a_CONJ dathliada'_VERBUNCT yn_PART amlwg_ADJ ond_CONJ beth_PRON am_ADP pan_CONJ ma_VERB 'r_DET storm_NOUN yn_PART cyrra'dd_NOUN ?_PUNCT
A_PART 'r_DET dyrfa_NOUN wedi_PART gada'l_VERB erbyn_ADP hyn_PRON ar_ADP nos_NOUN yn_PART cau_VERB yndanom_VERB ._PUNCT
Ydy_VERB '_PUNCT e_PRON 'n_PART hawdd_ADJ dilyn_VERB Iesu_PROPN yn_ADP y_DET sefyllfa_NOUN hon_DET ?_PUNCT
Yn_PART sicr_ADJ nag_CONJ yw_VERB ._PUNCT
Ond_CONJ mae_AUX Iesu_PROPN yn_PART galw_VERB arnom_ADP yn_PART bersonol_ADJ i_PART wrando_VERB ar_ADP ei_DET lais_NOUN ac_CONJ i_PART dawelu_VERB ein_DET hofnau_NOUN ._PUNCT
Fi_PRON yw_VERB '_PUNCT e_PRON ._PUNCT
Paid_VERB a_CONJ bod_VERB yn_PART ofnus_ADJ [_PUNCT saib_NOUN ]_PUNCT Yn_PART fuan_ADJ wedi_ADP 'r_DET ddau_NUM deulu_NOUN gyrraedd_VERB arfordir_NOUN dwyreiniol_ADJ lleoliad_NOUN a_CONJ glanio_VERB yn_PART ardal_NOUN lleoliad_NOUN fe_PART drawyd_VERB y_DET cenhadon_VERB Cymreig_ADJ ag_ADP ergyd_NOUN fawr_ADJ ._PUNCT
Roeddent_AUX wedi_PART goroesi_VERB 'r_DET antur_NOUN fawr_ADJ ar_ADP draws_ADJ y_DET dŵr_NOUN a_CONJ bwriad_NOUN enwg_VERB cyfenw_NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN nawr_ADV oedd_VERB sefydlu_VERB ysgolion_NOUN lleol_ADJ ._PUNCT
Yn_PART hytrach_ADV nag_CONJ efengylu_VERB a_CONJ sefydlu_VERB eglwysi_NOUN yn_PART syth_ADJ eu_DET hamcan_NOUN oedd_VERB addysgu_VERB y_DET bobl_NOUN leol_ADJ ._PUNCT
Dysgu_VERB 'r_DET enw_NOUN ei_DET huna_VERB 'n_PART ac_CONJ yn_ADP y_DET pendraw_NOUN sefydlu_VERB orgraffuriaeth_NOUN a_CONJ hyd_NOUN yn_PART oed_NOUN cyfieithu_VERB 'r_DET Beibl_NOUN i_ADP enw_NOUN ._PUNCT
Y_DET genedl_NOUN Africanaidd_ADJ gyntaf_ADJ i_ADP ga'l_VERB y_DET Beibl_NOUN yn_ADP ei_DET hiaith_NOUN ei_DET huna_VERB 'n_PART ._PUNCT
Ond_CONJ fe_PART ddaeth_VERB storm_NOUN ar_ADP eu_DET bywyda'_VERBUNCT ._PUNCT
Fe_PART fu_VERB farw_ADV enwg_VERB cyfenw_NOUN ei_DET wraig_NOUN __NOUN a_CONJ 'u_PRON plentyn_NOUN a_CONJ __NOUN __NOUN __NOUN gwraig_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN o_ADP glefyd_NOUN malaria_NOUN ._PUNCT
Dim_PRON ond_ADP rhai_DET wythnosa'_VERBUNCT wedi_PART glanio_VERB ar_ADP yr_DET ynys_NOUN ._PUNCT
Roedd_VERB y_DET storm_NOUN ddigon_ADJ i_ADP lethu_VERB enwg_VERB cyfenw_NOUN mae_VERB 'n_PART siŵr_ADJ ._PUNCT
Yr_DET unig_ADJ genhadwr_NOUN oedd_VERB ar_ADP ôl_NOUN ._PUNCT
Nid_PART yw_VERB dilyn_VERB Iesu_PROPN yn_PART hawdd_ADJ bob_DET amser_NOUN ._PUNCT
I_ADP enwg_VERB cyfenw_NOUN ac_CONJ i_ADP 'r_DET cenhadon_NOUN roedd_AUX yn_PART golygu_VERB 'r_DET aberth_NOUN fwya'_ADJUNCT ._PUNCT
Fe_PART dda_ADJ 'th_NOUN y_DET storm_NOUN fwyaf_ADV dychrynllyd_ADJ i_ADP fywyd_NOUN enwg_VERB cyfenw_NOUN ond_CONJ drwy_ADP ffydd_NOUN fe_PART glywodd_VERB laid_NOUN Iesu_PROPN yn_PART dweud_VERB wrtho_ADP '_PUNCT fi_PRON yw_VERB '_PUNCT e_PRON ._PUNCT
Paid_VERB ag_CONJ ofni_VERB ._PUNCT
Yn_PART wyrthiol_ADJ fe_PART barhaodd_VERB â_ADP 'r_DET gwaith_NOUN gan_CONJ sefydlu_VERB ysgol_NOUN lleol_ADJ derbyn_VERB sêl_NOUN bedith_NOUN brenin_NOUN y_DET rhanbarth_NOUN cyn_CONJ teithio_VERB i_ADP brif_ADJ ddinas_NOUN y_DET wlad_NOUN lleoliad_NOUN a_CONJ sefydlu_VERB rhagor_PRON o_ADP ysgolion_NOUN ac_CONJ eglwysi_NOUN ._PUNCT
Fe_PART dda_ADJ 'th_PRON enwg_VERB cyfenw_NOUN yn_ADP ei_DET dro_NOUN i_ADP 'r_DET wlad_NOUN gan_CONJ helpu_VERB sefydlu_VERB gwasg_NOUN argraffu_VERB gan_CONJ gyhoeddi_VERB 'r_DET Beibl_NOUN a_CONJ deunyddia'_VERBUNCT addysgiadol_ADJ gwerthfawr_ADJ i_ADP 'r_DET wlad_NOUN ._PUNCT
Wrth_PART rhoi_VERB eu_DET bywyda'_VERBUNCT yn_PART nwylo_NOUN eu_DET gwaredwr_NOUN fe_PART ddaeth_VERB bendith_NOUN a_CONJ chanlyniada'_VERBUNCT mawr_ADJ i_ADP 'r_DET wlad_NOUN ._PUNCT
Heddiw_ADV rydym_AUX yn_PART cofio_VERB gorchest_NOUN arwrol_ADJ y_DET bechgyn_NOUN ifanc_ADJ ._PUNCT
1625.621_ADJ
