"_PUNCT Rhaglen_PROPN Cylch_NOUN Llyfryddol_ADJ Caerdydd_PROPN 2019_NUM -_SYM 20_NUM "_PUNCT ,_PUNCT "_PUNCT Gweld_VERB yr_DET ebost_NOUN yn_ADP eich_DET porwr_NOUN /_PUNCT View_X this_VERB email_NOUN in_ADP your_NOUN browser_NOUN enw_NOUN
Gweld_VERB yr_DET ebost_NOUN yn_ADP eich_DET porwr_NOUN /_PUNCT View_X this_VERB email_NOUN in_ADP your_NOUN browser_NOUN enw_NOUN
enw_NOUN
Cylch_NOUN Llyfryddol_PROPN Caerdydd_PROPN ,_PUNCT Gaeaf_VERB 2019_NUM -_SYM 20_NUM
Ymunwch_VERB ag_ADP un_NUM o_ADP gymdeithasau_NOUN diwylliannol_ADJ Cymraeg_PROPN mwyaf_ADV ffyniannus_ADJ y_DET brifddinas_NOUN ._PUNCT
Dim_PRON ond_ADP £_SYM 10_NUM yw_VERB 'r_DET tâl_NOUN aelodaeth_NOUN am_ADP y_DET flwyddyn_NOUN gyfan_ADJ -_PUNCT bargen_NOUN !_PUNCT
Mae_VERB Cylch_NOUN Llyfryddol_ADJ Caerdydd_PROPN yn_PART cyfarfod_VERB ar_ADP nosweithiau_NOUN Gwener_PROPN unwaith_ADV y_DET mis_NOUN rhwng_ADP mis_NOUN Medi_PROPN a_CONJ mis_NOUN Mawrth_PROPN ._PUNCT
Cynhelir_VERB y_DET cyfarfodydd_NOUN am_ADP 7.00_NUM o_ADP 'r_DET gloch_NOUN yn_ADP Ystafell_PROPN 0.31_NUM ,_PUNCT Adeilad_PROPN John_PROPN Percival_PROPN
DYMA_DET RAGLEN_VERB GAEAF_NOUN 2019_NUM -_SYM 20_NUM
Gwener_NOUN ,_PUNCT 27_NUM Medi_NOUN 2019_NUM
Sgwrs_NOUN gan_CONJ y_DET cyn-_VERB Archdderwydd_PROPN a_CONJ 'r_DET Prifardd_NOUN Jim_NOUN Parc_NOUN Nest_PROPN ar_ADP y_DET testun_NOUN '_PUNCT Iolo_PROPN a_CONJ 'i_PRON Orwelion_NOUN :_PUNCT Cerdd_NOUN y_DET Gadair_NOUN 2019_NUM '_PUNCT ._PUNCT
Gwener_NOUN ,_PUNCT 18_NUM Hydref_X 2019_NUM
Bydd_VERB enwg_VERB cyfenw_NOUN yn_PART trafod_VERB ei_DET gyfrol_NOUN ,_PUNCT Dim_DET Croeso_NOUN '_PUNCT 69_NUM :_PUNCT Gwrthsefyll_NOUN yr_DET Arwisgo_PROPN ._PUNCT
Gwener_NOUN ,_PUNCT 15_NUM Tachwedd_NOUN 2019_NUM
'_PUNCT Pori_NOUN yn_ADP Archif_PROPN Merêd_PROPN a_CONJ Phyllis_PROPN '_PUNCT yw_VERB testun_NOUN y_PART delynores_VERB enwb_NOUN cyfenw_NOUN wrth_ADP iddi_ADP drafod_VERB cyfraniad_NOUN Meredydd_NOUN Evans_PROPN a_CONJ Phyllis_PROPN Kinney_PROPN i_ADP fyd_NOUN canu_VERB gwerin_NOUN ._PUNCT
Gwener_NOUN ,_PUNCT 17_NUM Ionawr_PROPN 2020_NUM
'_PUNCT Myfyrio_PROPN ar_ADP y_DET Meddwl_NOUN Cymraeg_PROPN a_CONJ Chymreig_ADJ '_PUNCT yw_VERB pwnc_NOUN yr_DET athronydd_NOUN ,_PUNCT Dr_ADV enwg_VERB cyfenw_NOUN ,_PUNCT awdur_NOUN y_DET gyfrol_NOUN Credoau_PROPN 'r_DET Cymry_PROPN ._PUNCT
Gwener_NOUN ,_PUNCT 21_NUM Chwefror_PROPN 2020_NUM
Sgwrs_NOUN gan_ADP y_DET Prif_NOUN Lenor_PROPN ,_PUNCT Dr_NOUN enwb_NOUN cyfenw_NOUN ,_PUNCT ar_ADP y_DET testun_NOUN '_PUNCT Siampên_PROPN yn_ADP Stuttgart_PROPN :_PUNCT Medal_NOUN Ryddiaith_PROPN 2019_NUM '_PUNCT ._PUNCT
Gwener_NOUN ,_PUNCT 20_NUM Mawrth_PROPN 2020_NUM
Bydd_AUX Dr_PROPN enwg_ADV enwg_VERB __NOUN yn_PART trafod_VERB ei_DET gyfrol_NOUN ,_PUNCT Pris_PROPN Cydwybod_NOUN :_PUNCT T_NUM ._PUNCT H_INTJ ._PUNCT Parry_PROPN -_PUNCT Williams_PROPN a_CONJ Chysgod_PROPN y_DET Rhyfel_NOUN Mawr_ADJ ._PUNCT
CROESO_NOUN CYNNES_SYM I_ADP BAWB_SYM
Am_ADP fanylion_NOUN pellach_ADJ ,_PUNCT cysylltwch_VERB â_ADP 'r_DET Ysgrifennydd_NOUN :_PUNCT
Yr_DET Athro_NOUN enwg_VERB cyfenw_NOUN ,_PUNCT __NOUN
(_PUNCT Ffôn_PROPN :_PUNCT rhifôn_NOUN ;_PUNCT e_PRON -_PUNCT bost_NOUN :_PUNCT ebost_NOUN )_PUNCT ._PUNCT
Cydnabyddir_VERB nawdd_NOUN Ysgol_NOUN y_DET Gymraeg_PROPN ,_PUNCT Prifysgol_NOUN Caerdydd_PROPN ._PUNCT
