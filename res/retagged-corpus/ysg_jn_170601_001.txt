Crynodeb_NOUN Pobol_ADJ y_DET Cwm_NOUN W_SYM 24_NUM
Gyda_ADP pherswâd_NOUN gan_ADP Sioned_PROPN ,_PUNCT mae_AUX Dani_PROPN yn_PART gwneud_VERB penderfyniad_NOUN amhoblogaidd_ADJ iawn_ADV ._PUNCT
Caiff_VERB Siôn_PROPN a_CONJ gweddill_NOUN y_DET pentref_NOUN sioc_NOUN pan_CONJ mae_AUX Gwyneth_PROPN yn_PART dychwelyd_VERB gyda_ADP 'i_PRON chariad_NOUN newydd_ADJ ._PUNCT
Mae_AUX Sioned_PROPN yn_PART trio_VERB ei_DET gorau_ADJ i_PART gorddi_VERB 'r_DET dyfroedd_NOUN trwy_ADP ddweud_VERB wrth_ADP Gwyneth_PROPN fod_VERB aelod_NOUN newydd_ADJ o_ADP deulu_NOUN 'r_DET Monk_NOUN ar_ADP y_DET ffordd_NOUN ._PUNCT
Gydag_ADP achos_NOUN llys_NOUN mabwysiadu_VERB Esther_PROPN ar_ADP y_DET gorwel_NOUN ,_PUNCT mae_AUX Sheryl_PROPN yn_PART penderfynu_VERB gwahodd_VERB Wil_PROPN i_ADP Gwmderi_PROPN ,_PUNCT ond_CONJ mae_AUX Hywel_PROPN yn_PART poeni_VERB am_ADP y_DET sefyllfa_NOUN ._PUNCT
Er_ADP mwyn_NOUN ceisio_VERB gwarchod_NOUN Vicky_PROPN rhag_CONJ torri_VERB ei_DET chalon_NOUN eto_ADV ,_PUNCT mae_AUX Mark_PROPN yn_PART ymyrryd_VERB ac_CONJ yn_PART cael_VERB sgwrs_NOUN gyda_ADP Mathew_PROPN ,_PUNCT ond_CONJ pam_ADV bod_AUX Josh_PROPN yn_PART penderfynu_VERB cymryd_VERB y_DET bai_NOUN ?_PUNCT
Pobol_ADJ y_DET Cwm_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 8.00_NUM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Pob_DET prynhawn_NOUN Sul_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Ar_ADP alw_VERB :_PUNCT s4c.cymru_NOUN ;_PUNCT BBC_X iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cynhyrchiad_NOUN BBC_NOUN Cymru_PROPN
