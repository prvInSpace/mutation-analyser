"_PUNCT Gwybodaeth_PROPN ,_PUNCT Cyngor_PROPN ,_PUNCT Arweiniad_NOUN #_ADJ Covid19_PROPN "_PUNCT ,_PUNCT "_PUNCT Cael_VERB trafferth_NOUN gweld_VERB yr_DET e_PRON -_PUNCT bost_NOUN hwn_DET ?_PUNCT
Darllenwch_VERB yn_ADP eich_DET porwr_NOUN enw_NOUN
27_NUM Mawrth_PROPN 2020_NUM
Cyllid_VERB enw_NOUN |_SYM
Dechrau_VERB a_CONJ Chynllunio_PROPN Busnes_PROPN enw_NOUN |_SYM
Marchnata_VERB enw_NOUN |_SYM
Sgiliau_NOUN a_CONJ Hyfforddiant_NOUN enw_NOUN |_SYM
Syniadau_NOUN Busnes_PROPN enw_NOUN |_SYM
TG_VERB enw_NOUN
Coronafeirws_NOUN :_PUNCT y_DET wybodaeth_NOUN a_CONJ 'r_DET cyngor_NOUN busnes_NOUN diweddaraf_ADJ enw_NOUN
Cyngor_VERB i_ADP gyflogwyr_NOUN a_CONJ manylion_NOUN y_DET pecyn_NOUN cymorth_NOUN sydd_VERB ar_ADP gael_VERB i_ADP fusnesau_NOUN yn_PART sgil_NOUN yr_DET achosion_NOUN o_ADP coronafeirws_NOUN
enw_NOUN
Cymorth_VERB ar_ADP gyfer_NOUN yr_DET Hunangyflogedig_ADJ enw_NOUN
Mae_AUX Llywodraeth_PROPN y_DET DU_PROPN wedi_PART cyhoeddi_VERB pecyn_NOUN cymorth_VERB i_ADP 'r_DET bobl_NOUN Hunangyflogedig_ADJ sydd_AUX wedi_PART 'u_PRON heffeithio_VERB gan_ADP y_DET Coronafeirws_PROPN
enw_NOUN
3_NUM mis_NOUN ychwanegol_ADJ i_ADP chi_PRON ffeilio_VERB 'ch_PRON cyfrifon_VERB enw_NOUN
Bydd_AUX busnesau_NOUN 'n_PART cael_VERB 3_NUM mis_NOUN yn_PART ychwanegol_ADJ i_PART ffeilio_VERB cyfrifon_VERB gyda_ADP Th_X ?_PUNCT 'r_DET Cwmnïau_NOUN i_ADP helpu_VERB cwmnïau_NOUN i_PART osgoi_VERB cosbau_ADP wrth_ADP ddelio_VERB ag_ADP effaith_NOUN COVID_PROPN -_PUNCT 19_NUM ._PUNCT
enw_NOUN
Coronafeirws_NOUN (_PUNCT COVID_PROPN 19_NUM )_PUNCT :_PUNCT cau_VERB busnesau_NOUN ac_CONJ adeiladau_NOUN enw_NOUN
Mae_AUX Llywodraeth_PROPN y_DET DU_PROPN wedi_PART cyhoeddi_VERB canllawiau_NOUN ynglŷn_ADP â_ADP chau_VERB 'r_DET holl_DET fanwerthwyr_NOUN sy_AUX 'n_PART gwerthu_VERB nwyddau_NOUN nad_PART ydynt_VERB yn_PART hanfodol_ADJ ac_CONJ adeiladau_NOUN eraill_ADJ nad_PART ydynt_VERB yn_PART hanfodol_ADJ ,_PUNCT fel_ADP rhan_NOUN o_ADP 'r_DET mesurau_NOUN cadw_VERB pellter_NOUN cymdeithasol_ADJ ._PUNCT
enw_NOUN
Coronafeirws_NOUN :_PUNCT Diogelu_VERB busnesau_NOUN sy_AUX 'n_PART methu_VERB taliadau_NOUN rhent_NOUN enw_NOUN
Cyhoeddodd_VERB Llywodraeth_PROPN y_DET DU_PROPN y_PART bydd_VERB tenantiaid_NOUN masnachol_ADJ na_PART allant_VERB dalu_VERB eu_DET rhent_NOUN oherwydd_ADP y_DET coronafeirws_NOUN yn_PART cael_VERB eu_PRON diogelu_VERB rhag_CONJ cael_VERB eu_DET troi_VERB allan_ADV ._PUNCT
enw_NOUN
Cyngor_VERB i_ADP gyflogwyr_NOUN ar_ADP weithio_VERB gartref_NOUN yn_ADP ystod_NOUN COVID_PROPN -_PUNCT 19_NUM enw_NOUN
Oes_VERB gennych_ADP chi_PRON bobl_NOUN yn_PART gweithio_VERB gartref_NOUN dros_ADP dro_NOUN o_ADP ganlyniad_NOUN i_ADP 'r_DET achosion_NOUN o_ADP 'r_DET Coronaferiws_NOUN ?_PUNCT
Fel_ADP cyflogwr_NOUN ,_PUNCT mae_VERB gennych_VERB chi_PRON 'r_DET un_NUM cyfrifoldebau_NOUN iechyd_NOUN a_CONJ diogelwch_NOUN dros_ADP y_DET rheini_NOUN sy_AUX 'n_PART gweithio_VERB gartref_NOUN ag_ADP unrhyw_DET weithwyr_NOUN eraill_ADJ ._PUNCT
enw_NOUN
Coronafeirws_NOUN :_PUNCT Arweiniad_NOUN i_ADP berchnogion_VERB a_CONJ gweithredwyr_NOUN parciau_NOUN gwyliau_NOUN yng_ADP Nghymru_PROPN enw_NOUN
Mae_AUX Llywodraeth_PROPN Cymru_PROPN wedi_PART llunio_VERB arweiniad_NOUN sy_AUX 'n_PART egluro_VERB beth_PRON mae_VERB perchnogion_NOUN a_CONJ gweithredwyr_NOUN parciau_NOUN gwyliau_NOUN angen_NOUN ei_PRON wneud_VERB wrth_ADP ymateb_VERB i_ADP 'r_DET Coronafeirws_PROPN ._PUNCT
