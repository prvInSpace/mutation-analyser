Crynodeb_NOUN Rownd_PROPN a_CONJ Rownd_X W_NUM 52_NUM
Mae_AUX 'n_PART ŵyl_NOUN y_DET banc_NOUN a_CONJ phawb_PRON yn_PART diogi_VERB ar_ADP ôl_NOUN y_DET Nadolig_PROPN ._PUNCT
Tra_CONJ bod_VERB Philip_PROPN yn_ADP Sbaen_PROPN ,_PUNCT mae_AUX Mathew_PROPN â_ADP 'i_PRON fryd_NOUN ar_ADP wneud_VERB y_DET mwyaf_NOUN o_ADP 'r_DET llonydd_NOUN yn_ADP y_DET fflat_NOUN ond_CONJ daw_VERB 'n_PART amlwg_ADJ iddo_ADP fod_VERB meddwl_NOUN Sophie_PROPN ar_ADP bethau_NOUN eraill_ADJ ._PUNCT
Mae_VERB David_PROPN yn_PART awyddus_ADJ iawn_ADV i_ADP siarad_VERB hefo_ADP Rhys_PROPN a_CONJ bydd_VERB yn_PART fodlon_ADJ dweud_VERB celwydd_NOUN wrth_ADP Dani_PROPN i_ADP sicrhau_VERB hynny_PRON ond_CONJ tybed_ADV a_PART fydd_VERB hi_PRON 'n_PART werth_ADJ yr_DET holl_DET ymdrech_NOUN ?_PUNCT
Wrth_CONJ i_ADP Kelvin_PROPN edrych_VERB ymlaen_ADV at_ADP brynhawn_NOUN i_ADP 'r_DET brenin_NOUN hefo_ADP Terry_PROPN ,_PUNCT bydd_AUX rhywbeth_NOUN neu_CONJ rywun_NOUN yn_PART amharu_VERB ar_ADP ei_DET gynlluniau_NOUN ._PUNCT
Ac_CONJ mae_AUX Carys_PROPN yn_PART dychwelyd_VERB i_ADP 'r_DET dref_NOUN sy_AUX 'n_PART tarfu_VERB ar_ADP lonyddwch_VERB Iolo_PROPN a_CONJ Llio_PROPN ._PUNCT
Mae_VERB John_PROPN mewn_ADP penbleth_NOUN ynglŷn_ADP ag_ADP ymddygiad_NOUN Siân_PROPN ,_PUNCT sy_AUX 'n_PART cael_VERB noson_NOUN annisgwyl_ADJ gyda_ADP chriw_NOUN 'r_DET salon_NOUN yn_ADP Copa_PROPN ._PUNCT
Wrth_CONJ i_ADP hanner_NOUN nos_NOUN nesáu_VERB ,_PUNCT ynghanol_ADP y_DET dathlu_VERB ,_PUNCT bydd_VERB ambell_ADJ gusan_NOUN ,_PUNCT ambell_ADJ ddeigryn_NOUN ac_CONJ ambell_ADJ i_ADP sioc_NOUN i_ADP drigolion_NOUN Glanrafon_PROPN ._PUNCT
Rownd_NOUN a_CONJ Rownd_PROPN
Mawrth_NOUN ac_CONJ Iau_VERB 7.30_SYM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Omnibws_VERB dydd_NOUN Sul_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Gwefan_NOUN :_PUNCT s4c.cymru_NOUN
Cynhyrchiad_NOUN Rondo_NOUN Media_PROPN ar_ADP gyfer_NOUN S4C_PROPN
