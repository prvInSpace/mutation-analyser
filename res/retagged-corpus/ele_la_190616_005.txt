Haia_NOUN enwb_VERB
Dwi_VERB 'n_PART iawn_ADJ diolch_INTJ ._PUNCT
A_CONJ thithau_PRON ?_PUNCT
Sori_PART ges_VERB i_ADP ddim_PART cyfle_NOUN i_ADP dy_DET ffonio_VERB di_PRON :_PUNCT mae_VERB o_PRON 'n_PART cyfnod_NOUN prysur_ADJ iawn_ADV ar_ADP hyn_PRON o_ADP bryd_NOUN ._PUNCT
Plygain_VERB yn_ADP y_DET capel_NOUN neithiwr_ADV :_PUNCT profiad_NOUN gwych_ADJ ,_PUNCT hyd_NOUN yn_PART oed_NOUN os_CONJ roedd_VERB rhaid_VERB i_ADP mi_PRON chwarae_VERB 'r_DET organ_NOUN hefyd_ADV (_PUNCT pianydd_NOUN anobeithiol_ADJ ydw_VERB i_PRON !_PUNCT !_PUNCT )_PUNCT
Mae_AUX gan_ADP enwb_NOUN (_PUNCT sydd_AUX yn_PART gweithii_VERB efo_ADP fi_PRON )_PUNCT syniad_NOUN ardderchog_ADJ :_PUNCT ffordd_NOUN i_ADP dy_DET gynnwys_VERB di_PRON yng_ADP ngwaith_NOUN y_DET cwmni_NOUN ._PUNCT
Ond_CONJ cwestiwn_NOUN cyntaf_ADJ :_PUNCT faset_VERB ti_PRON ar_ADP gael_VERB i_ADP ddod_VERB i_PART weld_VERB sioe_NOUN ym_ADP lleoliad_NOUN ar_ADP dyddiad_NOUN ?_PUNCT
Rhywbeth_NOUN cyffrous_ADJ ac_CONJ hefyd_ADV efallai_VERB cyfarwydd_ADJ i_ADP ti_PRON ,_PUNCT ond_CONJ pwysicaf_ADJ ,_PUNCT yn_PART theatr_NOUN newydd_ADV sbon_ADV (_PUNCT Pontio_PROPN )_PUNCT dy_DET brifysgol_NOUN di_PRON ._PUNCT
Syniad_NOUN gwych_ADJ ,_PUNCT n'est_NOUN -_PUNCT ce_ADP pas_NOUN ?_PUNCT
Beth_PRON am_ADP sceipio_VERB dros_ADP y_DET penwythnos_NOUN ?_PUNCT
Hwyl_NOUN am_ADP y_DET tro_NOUN ,_PUNCT
enwg_VERB
