CYNLLUN_NOUN GOFAL_PROPN PERSONOL_PROPN :_PUNCT FFURFLEN_VERB GYTUNDEB_X A_CONJ CHYDSYNIO_PROPN
Diben_NOUN y_DET Ffurflen_NOUN Gytundeb_PROPN a_CONJ Chydsynio_PROPN yw_VERB sicrhau_ADV bod_VERB rhieni_NOUN /_PUNCT gofalwyr_NOUN a_CONJ gweithwyr_NOUN proffesiynol_ADJ yn_PART cytuno_VERB â_ADP pha_NOUN ofal_NOUN sydd_VERB i_ADP 'w_PRON roi_VERB a_CONJ bod_AUX staff_NOUN wedi_PART derbyn_VERB unrhyw_DET hyfforddiant_NOUN priodol_ADJ a_PART allai_VERB fod_VERB yn_PART berthnasol_ADJ ._PUNCT
Gall_VERB y_DET rhiant_NOUN /_PUNCT gofalwr_NOUN neu_CONJ 'r_DET gweithiwr_NOUN proffesiynol_ADJ sydd_VERB â_ADP phrofiad_NOUN o_ADP 'r_DET weithdrefn_NOUN gofal_NOUN wneud_VERB y_DET gwaith_NOUN o_ADP addysgu_VERB rhai_PRON o_ADP 'r_DET gweithdrefnau_NOUN gofal_NOUN hynny_PRON ._PUNCT
Pan_CONJ fo_VERB 'r_DET rhiant_NOUN /_PUNCT gofalwr_NOUN a_CONJ /_PUNCT neu_CONJ 'r_DET gweithwyr_NOUN proffesiynol_ADJ yn_PART gytûn_ADJ bod_AUX y_DET weithdrefn_NOUN wedi_PART 'i_PRON dysgu_VERB neu_CONJ pan_CONJ fydd_VERB gofal_NOUN personol_ADJ arferol_ADJ yn_PART cael_VERB ei_PRON ddarparu_VERB ,_PUNCT bydd_AUX y_DET manylion_NOUN yn_PART cael_VERB eu_DET cofnodi_VERB 'n_PART llawn_ADJ isod_ADV a_CONJ rhaid_VERB i_ADP bob_DET parti_NOUN lofnodi_VERB 'r_DET cofnod_NOUN hwn_DET a_PART chael_VERB copi_NOUN ohono_ADP ._PUNCT
Mae_VERB copi_NOUN ychwanegol_ADJ i_ADP 'w_PRON gadw_VERB yn_PART ffeil_NOUN y_DET disgyblion_NOUN yn_ADP yr_DET ysgol_NOUN a_PART darperir_VERB copi_NOUN ar_ADP gyfer_NOUN cofnod_NOUN meddygol_ADJ y_DET plentyn_NOUN (_PUNCT os_CONJ yw_VERB 'n_PART briodol_ADJ )_PUNCT ._PUNCT
Enw_NOUN 'r_DET Plentyn_PROPN
Dyddiad_NOUN Geni_PROPN
Dyddiad_NOUN a_PART gytunwyd_VERB
Dyddiad_NOUN adolygu_VERB a_CONJ gytunwyd_VERB
Y_DET rhesymau_NOUN dros_ADP ddarparu_VERB gofal_NOUN personol_ADJ :_PUNCT (_PUNCT e_PRON ._PUNCT e_PRON ._PUNCT diffyg_NOUN hyfforddiant_NOUN /_PUNCT oedi_NOUN datblygiadol_ADJ /_PUNCT angen_NOUN meddygol_ADJ )_PUNCT
Pwy_PRON fydd_AUX yn_PART darparu_VERB 'r_DET gofal_NOUN :_PUNCT (_PUNCT enwau_NOUN a_CONJ rolau_NOUN staff_NOUN )_PUNCT
Manylion_NOUN y_DET gofal_NOUN sydd_VERB i_ADP 'w_PRON ddarparu_VERB (_PUNCT ble_ADV ,_PUNCT pryd_ADV ,_PUNCT trefniadau_NOUN ar_ADP gyfer_NOUN preifatrwydd_NOUN ac_CONJ ati_ADP )_PUNCT :_PUNCT
Cysyniad_NOUN wedi_ADP 'i_PRON roi_VERB gan_ADP :_PUNCT
Enwau_NOUN 'r_DET rhieni_NOUN /_PUNCT gofalwyr_NOUN
Llofnodion_PROPN
Dyddiad_NOUN
Ysgol_INTJ :_PUNCT
Enwau_NOUN 'r_DET Aelodau_NOUN Staff_NOUN
Rolau_PROPN
Llofnodion_PROPN
Dyddiad_NOUN
Adolygiadau_NOUN :_PUNCT
Dyddiad_NOUN Adolygu_PROPN
Canlyniad_NOUN yr_DET Adolygiad_NOUN
