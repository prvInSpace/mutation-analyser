<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 06_NUM :_PUNCT 12_NUM -_PUNCT 00_NUM :_PUNCT 09_NUM :_PUNCT 12_NUM
enwb_VERB ac_CONJ '_PUNCT Ein_DET Breuddwydion_NOUN '_PUNCT ._PUNCT
enw_NOUN ._PUNCT
Ie_INTJ shwt_NOUN ichi_ADP bobl_NOUN ._PUNCT
Prynhawn_NOUN da_ADJ ichi_ADP gyd_ADP ._PUNCT
Mae_VERB hi_PRON 'n_PART brynhawn_ADJ dydd_NOUN Mercher_PROPN ac_CONJ ôô_NOUN mae_VERB hi_PRON 'n_PART braf_ADJ ._PUNCT
Mae_AUX 'r_DET haul_NOUN allan_ADV ._PUNCT
Mae_VERB 'r_DET heat_NOUN wave_NOUN 'di_PART cyrraedd_VERB ._PUNCT
O'dd_AUX nhw_PRON ddim_PART yn_PART dweud_VERB celwydd_NOUN wedi_ADP 'r_DET cwbwl_NOUN ._PUNCT
O_ADP ni_PRON 'n_PART meddwl_VERB bod_AUX nhw_PRON 'n_PART rhaffo_VERB hi_PRON ddoe_NOUN yng_ADP nghanol_NOUN y_DET gwynt_NOUN a_CONJ 'r_DET glaw_NOUN ond_CONJ na_PRON ._PUNCT
Mae_AUX 'r_DET tywydd_NOUN braf_ADJ wedi_PART cyrraedd_VERB ac_CONJ o_ADP ma_VERB 'i_PRON yn_PART braf_ADJ ._PUNCT
A_PART mae_VERB hi_PRON 'n_PART braf_ADJ bod_VERB yma_ADV yn_PART gwmni_NOUN ichi_ADP tan_ADP pump_NUM o_ADP 'r_DET gloch_NOUN heddi_NOUN ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN ._PUNCT
A_PART fel_ADP wedes_NOUN i_ADP fan_NOUN 'na_ADV "_PUNCT haul_NOUN y_DET gwanwyn_NOUN yn_PART waeth_ADJ na_CONJ gwenwyn_NOUN ._PUNCT "_PUNCT 'Na_ADV chi_PRON ddywediad_NOUN da_ADJ yndyfe_VERB ?_PUNCT
I_ADP gydfynd_VERB efo_ADP 'r_DET tywydd_NOUN ._PUNCT
Gymaint_ADV [_PUNCT =_SYM ]_PUNCT o_ADP [_PUNCT /=_PROPN ]_PUNCT o_ADP ddywediade_NOUN tywydd_NOUN i_PART gael_VERB yndoes_VERB e_PRON ?_PUNCT
"_PUNCT Haul_NOUN y_DET gwanwyn_NOUN yn_PART waeth_ADJ na_CONJ gwenwyn_NOUN ._PUNCT "_PUNCT '_PUNCT W_NUM i_PRON yn_PART licio_VERB honna_NOUN ._PUNCT
Chi_PRON 'n_PART gallu_VERB meddwl_VERB am_ADP fwy_PRON ?_PUNCT
Mwy_PRON o_ADP ddywediadau_NOUN i_ADP gydfynd_VERB efo_ADP 'r_DET tywydd_NOUN ._PUNCT
ym_ADP ._PUNCT
"_PUNCT Eira_NOUN man_NOUN ._PUNCT
Eira_NOUN mawr_ADJ ._PUNCT "_PUNCT Mae_VERB honna_VERB 'n_PART un_NUM dda'_ADJUNCT fyd_NOUN ._PUNCT
Ydi_PRON mae_VERB honna_VERB 'n_PART wir_ADJ '_PUNCT fyd_NOUN ._PUNCT
Mae_VERB honna_VERB 'n_PART yn_PART wir_ADJ ._PUNCT
ymm_CONJ a_PART ie_INTJ ini_X 'n_PART siŵr_ADJ o_ADP glywed_VERB y_DET gwcw_NOUN yn_PART nawr_ADV yndyn_NOUN ni_PRON ?_PUNCT
Nawr_ADV bod_VERB yr_DET haul_NOUN mas_ADJ ._PUNCT
Mae_AUX 'n_PART mynd_VERB '_PUNCT mlaen_NOUN ._PUNCT
Sa_NOUN 'i_PRON 'di_PART clywed_VERB y_DET gwcw_NOUN to_NOUN ._PUNCT
Fel_ADP arfer_NOUN dw_AUX i_PRON 'n_PART clywed_VERB hi_PRON diwedd_NOUN mis_NOUN Mawrth_PROPN ._PUNCT
Dechrau_VERB mis_NOUN Ebrill_PROPN ond_CONJ na_PART mae_AUX 'n_PART tynnu_VERB at_ADP ddiwedd_NOUN Ebrill_PROPN ._PUNCT
Dw_VERB i_PRON dal_ADV ddim_PART 'di_PART clywed_VERB y_DET gwcw_NOUN ._PUNCT
Ydach_VERB chi_PRON 'di_PART clywed_VERB y_DET gwcw_NOUN ?_PUNCT
Gobeithio_VERB bobo_VERB chi_PRON a_PART gobeithio_VERB bobo_VERB arian_NOUN yn_ADP eich_DET poced_NOUN chi_PRON pan_CONJ glywoch_VERB chi_PRON iddi_ADP ._PUNCT
Ie_INTJ felly_ADV hmm_CONJ difyr_ADJ iawn_ADV ._PUNCT
Ni_PART 'n_PART licio_VERB trafod_NOUN tywydd_NOUN yndyn_ADP ni_PRON ?_PUNCT
Ni_PART 'r_DET Cymry_PROPN ._PUNCT
Pawb_PRON yn_PART licio_VERB trafod_VERB y_DET tywydd_NOUN ._PUNCT
Ond_CONJ sa_AUX 'i_PRON yma_DET i_ADP siarad_VERB am_ADP y_DET tywydd_NOUN ._PUNCT
'_PUNCT W_NUM i_ADP yma_ADV i_PART weuthoch_VERB chi_PRON be_ADP sy_VERB '_PUNCT da_ADJ fi_PRON ar_ADP eich_DET cyfer_NOUN chi_PRON heddi_NOUN ._PUNCT
Yma_ADV tan_ADP bump_NUM o_ADP 'r_DET gloch_NOUN fel_ADP wedes_NOUN i_PRON a_CONJ mi_PRON fyddai_AUX yn_PART cyflwyno_VERB tlws_NOUN cynta'_ADJUNCT "_PUNCT Diolch_NOUN o_ADP Galon_NOUN "_PUNCT y_DET rhaglen_NOUN i_ADP berson_NOUN gweithgar_ADJ iawn_ADV yn_ADP Nyffryn_PROPN Cothi_PROPN a_CONJ 'w_PRON i_PRON 'n_PART gyffrous_ADJ iawn_ADV am_ADP yr_DET eitem_NOUN newydd_ADJ yma_ADV sy_VERB '_PUNCT da_ADJ ni_PRON yn_ADP y_DET prynhawn_NOUN ._PUNCT
Felly_CONJ mae_AUX hwnnw_PRON i_ADP ddod_VERB ._PUNCT
Gawn_VERB ni_PRON her_NOUN go_ADV iawn_ADJ wedyn_ADV ichi_ADP bobl_NOUN sy_AUX 'n_PART byw_VERB yng_ADP nghefn_NOUN gwlad_NOUN ._PUNCT
Y_DET rhai_PRON hynny_PRON ohonoch_ADP chi_PRON sy_AUX 'n_PART meddwl_VERB bobo_VERB chi_PRON 'n_PART dipyn_PRON o_ADP giamstars_NOUN '_PUNCT da_ADJ 'r_DET peirianne_NOUN amaethyddol_ADJ 'ma_ADV ._PUNCT
Wythnos_NOUN dwetha'_VERBUNCT rhoiodd_VERB enwg_VERB her_NOUN i_PART enwg_VERB 2_NUM >_SYM a_CONJ gawn_VERB ni_PRON weld_VERB a_PART fydd_AUX enwg_VERB yn_PART llwyddo_VERB i_PART weithio_VERB mas_NOUN pa_ADV beiriant_NOUN sy_VERB '_PUNCT da_ADJ ni_PRON heddi_VERB ?_PUNCT
Bach_ADJ o_ADP sbort_NOUN ac_CONJ ni_PRON 'n_PART licio_ADV cael_VERB bach_ADJ o_ADP sbort_NOUN ._PUNCT
Wedyn_ADV ar_ADP ôl_NOUN hanner_NOUN awr_NOUN wedi_ADP pedwar_NOUN fydda_AUX i_PRON 'n_PART cael_VERB cwmni_NOUN yr_DET newyddiadurwraig_NOUN <_SYM /_PUNCT anon_VERB >_SYM enwb_NOUN 2_NUM >_SYM yn_PART rhoi_VERB cip_NOUN olwg_NOUN ini_DET ar_ADP be_PRON sy_AUX 'n_PART digwydd_VERB ar_ADP wefan_NOUN Cymru_PROPN Fyw_PROPN wythnos_NOUN yma_DET ._PUNCT
Clic_NOUN bach_ADJ ar_ADP Cymru_PROPN Fyw_PROPN ._PUNCT
Ac_CONJ mi_PART fydden_AUX ni_PRON 'n_PART chwarae_VERB 'r_DET gystadleuaeth_NOUN hefyd_ADV "_PUNCT Gwed_VERB y_DET Gair_NOUN "_PUNCT unwaith_ADV eto_ADV ar_ADP ôl_NOUN pedwar_NOUN o_ADP 'r_DET gloch_NOUN ._PUNCT
Gsth_NOUN enwb_NOUN o_ADP Fryn_PROPN Teg_PROPN ._PUNCT
Ynys_PROPN Môn_PROPN ._PUNCT
Un_NUM ar_ADP bymtheg_ADJ mlwydd_NOUN oed_NOUN yw_VERB hi_PRON ._PUNCT
Gath_NOUN hi_PRON deg_NUM gair_NOUN yn_PART iawn_ADJ ddoe_ADV ._PUNCT
Do_VERB gath_NOUN hi_PRON deg_NUM yn_PART gywir_ADJ ._PUNCT
'_PUNCT Nath_PROPN hi_PRON 'n_PART ryfeddol_ADJ o_ADP dda_ADJ ._PUNCT
Tybed_ADV fydd_AUX rhywun_NOUN yn_PART maeddu_VERB ei_DET sgôr_NOUN hi_PRON heddi_NOUN ?_PUNCT
Cawn_VERB weld_VERB nes_ADP ymlaen_ADV ._PUNCT
Cofiwch_VERB mae_VERB yna_ADV groeso_VERB ichi_ADP gysylltu_VERB ar_ADP dim_DET tri_NUM saith_NOUN dim_PRON tri_NUM [_PUNCT =_SYM ]_PUNCT pump_NUM cant_NUM [_PUNCT /=_PROPN ]_PUNCT pump_NUM cant_NUM ._PUNCT
Dyna_DET 'r_DET rhif_NOUN ffôn_NOUN ._PUNCT
Dim_DET tri_NUM saith_NOUN dim_PRON tri_NUM [_PUNCT =_SYM ]_PUNCT pump_NUM cant_NUM [_PUNCT /=_PROPN ]_PUNCT pump_NUM cant_NUM neu_CONJ allawch_VERB chi_PRON ein_DET textio_VERB ni_PRON ar_ADP chwe_NUM deg_NUM saith_NUM pump_NUM cant_NUM ._PUNCT
Dyna_ADV yw_VERB 'r_DET rhif_NOUN text_VERB Chwech_PROPN saith_NUM pump_NUM cant_NUM ._PUNCT
A_CONJ nawr_ADV dyma_DET Angylion_NOUN Stanely_PROPN â_ADP "_PUNCT Carol_X "_PUNCT ._PUNCT
Ie_INTJ haul_NOUN y_DET gwanwyn_NOUN yn_PART waeth_ADJ na_CONJ gwenwyn_NOUN ._PUNCT
Yr_PART hen_ADJ gwcw_NOUN ._PUNCT
Ble_ADV mae_VERB 'r_DET gwcw_NOUN nawr_ADV ?_PUNCT
Os_CONJ chi_PRON 'di_PART clywed_VERB y_DET gwcw_NOUN ._PUNCT
Gadewch_VERB mi_PRON wybod_VERB ._PUNCT
Ac_CONJ yn_PART ble_ADV glywoch_VERB chi_PRON hi_PRON ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 13_NUM :_PUNCT 23_NUM -_SYM 00_NUM :_PUNCT 14_NUM :_PUNCT 43_NUM
Felly_CONJ beth_PRON yw_VERB 'r_DET peth_NOUN cynta'_ADJUNCT chi_PRON 'n_PART neud_VERB pan_CONJ mae_VERB hi_PRON 'n_PART braf_ADJ ?_PUNCT
Ydach_AUX chi_PRON 'n_PART mynd_VERB i_ADP 'r_DET dafarn_NOUN ?_PUNCT
Achos_CONJ dyna_DET bebe_NOUN mae_AUX rhywun_NOUN wedi_PART gweutha_VERB 'i_PRON ar_ADP Facebook_PROPN Mai_PROPN un_NUM o_ADP 'r_DET petha'_NOUNUNCT cynta'_ADJUNCT mae_AUX nhw_PRON 'n_PART wneud_VERB os_CONJ yw_VERB hi_PRON 'n_PART braf_ADJ yw_ADV mynd_VERB i_PART cael_VERB peint_NOUN mewn_ADP beer_ADJ garden_NOUN [_PUNCT chwerthin_VERB ]_PUNCT Pam_ADV ddim_PART ?_PUNCT
ymm_PART ydach_AUX chi_PRON 'n_PART mynd_VERB i_ADP 'r_DET traeth_NOUN ?_PUNCT
Oes_VERB '_PUNCT da_ADJ chi_PRON rhyw_DET hoff_ADJ le_AUX chi_PRON 'n_PART hoffi_ADV mynd_VERB i_ADP os_CONJ yw_VERB hi_PRON 'n_PART braf_ADJ ?_PUNCT
Neu_PART ydach_VERB chi_PRON jest_ADV yn_PART rhoi_VERB pâr_NOUN o_ADP siorts_NOUN arno_ADP fel_ADP arwydd_NOUN bod_VERB yr_DET haf_NOUN ar_ADP y_DET ffordd_NOUN a_CONJ 'r_DET gwanwyn_NOUN wedi_PART cyrraedd_VERB ?_PUNCT
Bydd_VERB hi_PRON 'n_PART braf_ADJ iawn_ADV gwybod_VERB beth_PRON ichi_ADP 'n_PART neud_VERB ._PUNCT
O_ADP ni_PRON 'n_PART gofyn_VERB wedyn_ADV ichi_ADP am_ADP y_DET gwcw_NOUN yn_PART do_VERB 'n_PART i_PRON ._PUNCT
ymm_ADV enwg_VERB a_CONJ enwg_VERB 4_NUM >_SYM ._PUNCT
Mae_AUX nhw_PRON 'di_PART clywed_VERB y_DET gwcw_NOUN yn_ADP y_DET coed_NOUN tu_NOUN ôl_NOUN i_ADP 'r_DET Foelas_NOUN wythnos_NOUN dwetha'_ADJUNCT ._PUNCT
Do_VERB fe_PRON wir_ADJ ?_PUNCT
Gobeithio_VERB bod_AUX pres_VERB yn_ADP eich_DET poced_NOUN chi_PRON bois_VERB ._PUNCT
Achos_CONJ ym_ADP mae_VERB hwnna_PRON 'n_PART hen_ADJ ystrydeb_NOUN yn_PART dyw_VERB e_PRON ?_PUNCT
Os_CONJ oes_VERB arian_NOUN yn_ADP eich_DET poced_NOUN chi_PRON pan_CONJ i_ADP chi_PRON 'n_PART clywed_VERB y_DET gwcw_NOUN bobo_VERB chi_PRON 'n_PART mynd_VERB i_PART gael_VERB blwyddyn_NOUN lewyrchus_ADJ o_ADP ran_NOUN  _SPACE arian_NOUN hynny_DET yw_VERB yndyfe_VERB ._PUNCT
Felly_CONJ difyr_ADJ iawn_ADV ._PUNCT
Ie_INTJ gadwch_VERB fi_PRON wybod_VERB os_CONJ chi_PRON 'di_PART clywed_VERB yr_DET hen_ADJ gwcw_NOUN ._PUNCT
Sa_AUX 'i_PRON wedi_PART clywed_VERB hi_PRON to_NOUN ._PUNCT
Dim_DET to_NOUN ._PUNCT
Ie_INTJ 'w_PRON i_ADP yma_ADV tan_ADP bump_NUM fel_ADP wedes_NOUN i_PRON ._PUNCT
Llwyth_PRON o_ADP gerddoriaeth_NOUN gwych_ADJ ar_ADP y_DET ffordd_NOUN ._PUNCT
A_CONJ nesa'_VERBUNCT dyma_ADV enwg_VERB "_PUNCT Pethau_NOUN 'n_PART Mynd_VERB Yn_ADP Galed_PROPN Dros_ADP Ben_PROPN "_PUNCT ._PUNCT
Oh_INTJ ._PUNCT
Cân_VERB o_ADP albwm_NOUN <_SYM /_PUNCT anon_VERB >_SYM enwg_VERB 5_NUM >_SYM o_ADP 'r_DET flywddyn_NOUN mil_NUM naw_NUM wyth_NUM naw_NUM yw_VERB hon_DET ._PUNCT
Enw_NOUN 'r_DET albwm_NOUN oedd_VERB "_PUNCT Milltiroedd_PROPN "_PUNCT ._PUNCT
"_PUNCT Pethau_NOUN 'n_PART Mynd_VERB Yn_ADP Galed_PROPN Dros_ADP Ben_PROPN "_PUNCT ._PUNCT
Mmm_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 22_NUM :_PUNCT 30_NUM -_SYM 00_NUM :_PUNCT 24_NUM :_PUNCT 56_NUM
Ond_CONJ nawr_ADV a_CONJ hithau_PRON bron_ADV iawn_ADV yn_PART hanner_NOUN awr_NOUN wedi_ADP dau_NUM mae_VERB 'n_PART amser_NOUN ini_CONJ gyfri_VERB 'r_DET defed_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN ]_PUNCT
Cyfri_NOUN 'r_DET Defaid_NOUN gyda_ADP [_PUNCT aneglur_ADJ ]_PUNCT ac_CONJ enwg_VERB ._PUNCT
Ie_INTJ cyfle_NOUN ini_ADP gael_VERB bach_ADJ o_ADP sbort_NOUN fan_NOUN hyn_DET yn_ADP y_DET prynhawn_NOUN ._PUNCT
ymm_ADP le_NOUN 'w_PRON i_PRON 'n_PART gofyn_VERB i_ADP rhywun_NOUN adnabyddus_ADJ i_ADP gyfri_VERB 'r_DET defed_NOUN ._PUNCT
Achos_CONJ 'w_PRON i_ADP fan_NOUN hyn_DET yn_PART dyw_VERB i_PRON yn_ADP y_DET prynhawn_NOUN ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN ._PUNCT
Ishe_VERB rhywun_NOUN i_ADP checkio_VERB [_PUNCT =_SYM ]_PUNCT ar_ADP y_DET [_PUNCT /=_PROPN ]_PUNCT ar_ADP y_DET praidd_NOUN fel_ADP petai_VERB i_ADP neud_VERB siŵr_ADV fod_VERB popeth_PRON yn_PART iawn_ADJ ._PUNCT
A_PART 'w_PRON i_PRON 'n_PART gofyn_VERB i_ADP 'n_PART ffrindia'_VERBUNCT adnabyddus_ADJ i_ADP i_PRON neud_VERB hynny_PRON a_CONJ mae_VERB 'na_ADV glip_NOUN llais_NOUN '_PUNCT da_ADJ ni_PRON yn_PART rhedeg_VERB ers_ADP  _SPACE bron_ADV i_ADP bythefnos_NOUN nawr_ADV ._PUNCT
Ac_CONJ ôô_NOUN mae_VERB o_PRON 'di_PART creu_VERB lot_PRON o_ADP [_PUNCT -_PUNCT ]_PUNCT mae_AUX 'di_PART neud_VERB ichi_ADP grafu_VERB 'ch_PRON pen_NOUN bois_VERB ._PUNCT
Yndyw_VERB e_PRON ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Mae_VERB 'di_PART [_PUNCT /=_PROPN ]_PUNCT mae_VERB 'di_PART creu_VERB lot_PRON [_PUNCT =_SYM ]_PUNCT o_ADP [_PUNCT /=_PROPN ]_PUNCT o_ADP drafferth_NOUN ichi_ADP ._PUNCT
Dyma_ADV oedd_VERB y_DET clip_NOUN cynta'_ADJUNCT glywoch_VERB chi_PRON ._PUNCT
Cant_VERB a_CONJ hanner_NOUN ._PUNCT
Hmm_INTJ ._PUNCT
Cant_VERB a_CONJ hanner_NOUN ._PUNCT
Dyna_ADV oedd_VERB y_DET clip_NOUN cynta'_ADJUNCT glywoch_VERB chi_PRON ._PUNCT
Wedyn_ADV glywoch_VERB chi_PRON hwn_DET ._PUNCT
Cant_VERB a_CONJ hanner_NOUN o_ADP ddefaid_NOUN ._PUNCT
A_PART heddi_NOUN ichi_ADP 'n_PART mynd_VERB i_PART gael_VERB y_DET clip_NOUN ola_ADP '_PUNCT ._PUNCT
A_CONJ 'r_DET cyfle_NOUN ola'_ADJUNCT ._PUNCT
Os_CONJ nad_PART ichi_VERB 'n_PART gael_VERB e_DET tro_NOUN hyn_DET wel_NOUN s'dim_PART mwy_ADJ alla_VERB i_PART rhoi_VERB ichi_ADP heb_ADP i_ADP fi_PRON rhoi_VERB yr_DET ateb_NOUN ._PUNCT
Felly_CONJ ie_INTJ pwy_PRON sydd_AUX yn_PART cyfri_VERB 'r_DET defed_NOUN ?_PUNCT
Ini_NOUN 'di_PART cael_VERB gymaint_ADV o_ADP gynigion_VERB enwb_NOUN enwb_NOUN 5_NUM >_SYM enwb_NOUN ._PUNCT
ymm_VERB enwb_NOUN 7_NUM >_SYM  _SPACE ni_PRON 'di_PART cael_VERB [_PUNCT =_SYM ]_PUNCT llwythi_VERB [_PUNCT /=_PROPN ]_PUNCT llwythi_VERB o_ADP enwau_NOUN enwb_NOUN wedyn_ADV enwb_VERB 8_NUM >_SYM ._PUNCT
Na_PART oedd_VERB hi_PRON 'n_PART anghywir_ADJ ._PUNCT
Felly_CONJ ie_INTJ pwy_PRON sydd_AUX yn_PART cyfri_VERB y_DET defed_NOUN ?_PUNCT
Dyma_DET y_DET clip_NOUN de_NOUN ._PUNCT
Y_DET trydydd_ADJ clip_NOUN ._PUNCT
Chi_PRON 'n_PART barod_ADJ ?_PUNCT
Gwrandwch_VERB yn_PART astud_ADJ ._PUNCT
Co_INTJ fe_PRON ._PUNCT
Cant_VERB a_CONJ hanner_NOUN o_ADP ddefaid_NOUN enwg_VERB ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT Wedodd_VERB hi_PRON 'n_ADP enw_NOUN i_PRON ._PUNCT
A_CONJ 'i_PRON [_PUNCT -_PUNCT ]_PUNCT a_CONJ [_PUNCT -_PUNCT ]_PUNCT co_ADP ni_PRON ._PUNCT
Cant_VERB a_CONJ hanner_NOUN o_ADP ddefaid_NOUN enwg_VERB ._PUNCT
Hmm_INTJ cant_NUM a_CONJ hanner_NOUN o_ADP ddefaid_NOUN enwg_VERB ._PUNCT
Felly_CONJ pwy_PRON sydd_AUX yn_PART cyfri_VERB y_DET defaid_NOUN ?_PUNCT
Dim_DET tri_NUM saith_NOUN dim_PRON tri_NUM [_PUNCT =_SYM ]_PUNCT pump_NUM cant_NUM [_PUNCT /=_PROPN ]_PUNCT pump_NUM cant_NUM yw_VERB 'r_DET rhif_NOUN ffôn_NOUN ._PUNCT
Llais_VERB pwy_PRON sy_VERB '_PUNCT da_ADJ ni_PRON fan_NOUN 'na_ADV ?_PUNCT
Chwe_NOUN deg_NUM saith_NUM pump_NUM cant_NUM ini_NOUN wedyn_ADV ar_ADP text_VERB Chwech_PROPN saith_NUM pump_NUM cant_NUM ._PUNCT
Ini_NOUN 'di_PART cael_VERB cynigion_NOUN fel_ADP enwb_NOUN 9_NUM >_SYM enwb_NOUN enwb_NOUN 11_NUM >_SYM [_PUNCT chwerthin_VERB ]_PUNCT enwg_VERB  _SPACE ni_PRON 'di_PART cael_VERB enwb_NOUN 12_NUM >_SYM enwb_NOUN fel_ADP wedes_NOUN i_ADP enwb_VERB 13_NUM >_SYM enwb_NOUN enwb_VERB 15_NUM >_SYM enwb_NOUN enwb_NOUN 17_NUM >_SYM ond_CONJ na_PART mae_VERB rheina_NOUN i_ADP gyd_NOUN yn_PART anghywir_ADJ felly_ADV am_ADP y_DET tro_NOUN ola_ADP '_PUNCT heddi_NOUN ichi_ADP 'n_PART clywed_VERB e_PRON am_ADP y_DET tro_NOUN ola_ADP '_PUNCT ._PUNCT
Pwy_PRON sydd_AUX yn_PART cyfri_VERB 'r_DET defed_NOUN ?_PUNCT
Cant_VERB a_CONJ hanner_NOUN o_ADP ddefaid_NOUN enwg_VERB ._PUNCT
A_CONJ nawr_ADV dyma_DET enwb_NOUN ._PUNCT
Cân_VERB o_ADP 'r_DET archif_NOUN o_ADP mil_NUM naw_NUM wyth_NUM dim_PRON hon_PRON ._PUNCT
O_ADP 'r_DET albwm_NOUN "_PUNCT Canaf_VERB Gân_PROPN "_PUNCT gan_ADP y_DET diweddar_ADJ <_SYM /_PUNCT anon_VERB >_SYM enwb_NOUN 18_NUM >_SYM ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 27_NUM :_PUNCT 15_NUM -_PUNCT 00_NUM :_PUNCT 30_NUM :_PUNCT 17_NUM
Mae_VERB 'na_ADV negeseuon_NOUN ._PUNCT
Mae_VERB 'na_ADV hysbysys_NOUN yn_ADP fy_DET nghyrraedd_VERB i_PRON ._PUNCT
Mae_VERB 'na_ADV gynigion_VERB da_ADJ yn_PART dod_VERB fewn_ADP ._PUNCT
ymm_CONJ a_PART mae_VERB llais_NOUN heddi_NOUN a_CONJ 'i_PRON drwy_ADP rheini_NOUN nes_CONJ ymlaen_ADV ._PUNCT
Daliwch_VERB ati_ADP i_PART gysylltu_VERB pwy_PRON sy_AUX 'n_PART cyfri_VERB 'r_DET defed_NOUN ?_PUNCT
ymm_CONJ ie_ADP cynigion_NOUN da_ADJ iawn_ADV mae_VERB rhaid_VERB fi_PRON '_PUNCT weud_VERB ._PUNCT
Da_ADJ iawn_ADV chi_PRON ._PUNCT
ymm_ADV enwg_VERB mi_PRON fydd_VERB CFfI_NOUN Llanymddyfri_PROPN yn_PART perfformio_VERB eu_DET pantomeim_NOUN nos_NOUN Sadwrn_PROPN yma_ADV yn_PART neuadd_NOUN Ysgol_NOUN Pantycelyn_PROPN Llanymddyfri_PROPN ._PUNCT
I_ADP ddechre_VERB am_ADP saith_NUM o_ADP 'r_DET gloch_NOUN yr_DET hwyr_NOUN ._PUNCT
Alli_VERB di_PRON rhoi_VERB hysbys_NOUN ar_ADP y_DET radio_NOUN plis_INTJ ?_PUNCT
Wel_CONJ dyna_DET fi_PRON wedi_PART neud_VERB ._PUNCT
Pob_DET hwyl_NOUN ichi_ADP efo_ADP 'r_DET noson_NOUN yna_DET ._PUNCT
ymm_VERB neges_NOUN fan_NOUN hyn_DET ._PUNCT
"_PUNCT Helo_INTJ <_SYM /_PUNCT anon_VERB >_SYM enwg_VERB 6_NUM >_SYM ._PUNCT
Oes_VERB bosib_NOUN ichi_ADP ddweud_VERB bod_VERB 'na_ADV ddiwrnod_NOUN o_ADP hwyl_NOUN yn_ADP Llwynmawr_PROPN ._PUNCT
Dinbych_VERB dydd_NOUN Sadwrn_PROPN yma_ADV ._PUNCT
Fydd_VERB 'na_ADV gystadleuaeth_NOUN tynnu_VERB tractorau_NOUN yno_ADV a_CONJ [_PUNCT =_SYM ]_PUNCT lot_PRON [_PUNCT /=_PROPN ]_PUNCT lot_PART mwy_ADJ ._PUNCT
Croeso_NOUN i_ADP unrhyw_DET un_NUM ddod_VERB a_CONJ tractor_NOUN i_ADP dynnu_VERB ._PUNCT
I_ADP ddechrau_VERB am_ADP un_NUM ar_ADP ddeg_NOUN o_ADP 'r_DET gloch_NOUN y_DET bore_NOUN ._PUNCT
Diolch_INTJ ._PUNCT "_PUNCT A_CONJ mai_PART 'n_PART addo_VERB tywydd_NOUN braf_ADJ felly_ADV  _SPACE aidial_ADJ ar_ADP gyfer_NOUN y_DET tynnu_VERB tractorau_NOUN 'na_ADV ._PUNCT
Diolch_NOUN ichi_ADP am_ADP y_DET neges_NOUN yna_ADV ._PUNCT
A_CONJ ie_INTJ mae_VERB 'na_ADV groeso_VERB ichi_ADP gysylltu_VERB os_CONJ '_PUNCT da_ADJ chi_PRON hysbys_ADJ ._PUNCT
Unhryw_DET gyfarchion_NOUN ._PUNCT
Unrhyw_DET beth_PRON fel_ADP 'na_ADV ._PUNCT
I_ADP ni_PRON 'ma_ADV fel_ADP wedes_NOUN i_PRON tan_ADP bump_NUM ._PUNCT
Chi_PRON 'n_PART gwybod_VERB y_DET rhif_NOUN erbyn_ADP hyn_PRON yndych_VERB chi_PRON ._PUNCT
Dim_DET tri_NUM saith_NOUN dim_PRON tri_NUM [_PUNCT =_SYM ]_PUNCT pump_NUM cant_NUM [_PUNCT /=_PROPN ]_PUNCT pump_NUM cant_NUM neu_CONJ chwe_NUM deg_NUM saith_NUM pump_NUM cant_NUM i_ADP ni_PRON wedyn_ADV ar_ADP y_DET testun_NOUN ._PUNCT
Nawr_ADV ta_ADJ a_CONJ hithau_PRON bron_ADV iawn_ADV yn_PART bum_ADJ munud_NOUN ar_ADP hugain_NOUN i_ADP dri_NUM o_ADP 'r_DET gloch_NOUN ._PUNCT
Bob_DET mis_NOUN ar_ADP y_DET rhaglen_NOUN newydd_ADJ 'ma_ADV sydd_VERB '_PUNCT da_ADJ fi_PRON yn_ADP y_DET prynhawn_NOUN ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN ._PUNCT
Mi_PART fyddai_AUX 'n_PART cyflwyno_VERB tlws_NOUN "_PUNCT Diolch_NOUN o_ADP Galon_NOUN "_PUNCT i_ADP rywun_NOUN arbennig_ADJ iawn_ADV sy_VERB 'di_PART gwneud_VERB cyfraniad_NOUN gwerthfawr_ADJ i_ADP 'r_DET gymuned_NOUN leol_ADJ ._PUNCT
Cymdeithas_NOUN arbennig_ADJ ._PUNCT
Clwb_NOUN pêl_NOUN -_PUNCT droed_NOUN neu_CONJ rygbi_NOUN ._PUNCT
Clwb_NOUN Ffermwyr_PROPN Ifanc_PROPN neu_CONJ rhywun_NOUN sydd_VERB 'di_PART helpu_VERB rhywun_NOUN yn_ADP ystod_NOUN cyfnod_NOUN o_ADP waeledd_NOUN ._PUNCT
Felly_CONJ cofiwch_VERB os_CONJ yw_VERB chi_PRON ishe_VERB enwebu_VERB rhywun_NOUN i_ADP dderbyn_VERB tlws_NOUN "_PUNCT Diolch_NOUN o_ADP Galon_NOUN "_PUNCT a_CONJ sypreis_NOUN bach_ADJ '_PUNCT da_ADJ fi_PRON a_CONJ 'r_DET criw_NOUN yna_ADV cofiwch_VERB gysylltu_VERB ._PUNCT
Felly_CONJ wythnos_NOUN diwetha'_ADJUNCT es_VERB i_ADP draw_ADV i_ADP Ddyffryn_PROPN Cothi_PROPN i_ADP rhoi_VERB sypreis_NOUN i_ADP 'r_DET person_NOUN cynta'_ADJUNCT i_PRON gael_VERB ei_PRON enwebu_VERB ar_ADP gyfer_NOUN tlws_NOUN "_PUNCT Diolch_NOUN o_ADP Galon_NOUN "_PUNCT a_CONJ dyma_PRON sut_ADV aeth_VERB hi_PRON ._PUNCT
[_PUNCT Cerddoriaeth_NOUN ]_PUNCT
"_PUNCT Diolch_NOUN o_ADP Galon_NOUN "_PUNCT ar_ADP enw_NOUN ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN ._PUNCT
Wel_CONJ bois_VERB mae_VERB hi_PRON off_NOUN 'ma_ADV ._PUNCT
'_PUNCT W_NUM i_PRON 'di_PART dod_VERB i_ADP Bumsaint_PROPN i_ADP Neuadd_PROPN y_DET Coroniad_NOUN achos_CONJ mae_VERB 'na_ADV noson_NOUN arbennig_ADJ iawn_ADV 'ma_ADV heno_ADV ._PUNCT
I_ADP ni_PRON 'di_PART dod_VERB i_PRON '_PUNCT weud_VERB diolch_NOUN o_ADP galon_NOUN i_ADP rhywun_NOUN sy_VERB 'di_PART neud_VERB gwahaniaeth_NOUN arbennig_ADJ iawn_ADV yn_ADP y_DET gymuned_NOUN ._PUNCT
Yn_ADP y_DET gymdeithas_NOUN ac_CONJ yn_PART enwedig_ADJ i_ADP 'r_DET Clwb_NOUN Ffermwyr_PROPN Ifanc_PROPN lleol_ADJ ._PUNCT
Wel_CONJ a_CONJ 'i_PRON fewn_ADP fan_NOUN 'na_ADV nawr_ADV ._PUNCT
Mae_VERB hi_PRON bron_ADV iawn_ADV yn_PART hanner_NOUN amser_NOUN a_CONJ fyddwn_AUX i_PRON 'n_PART mynd_VERB fewn_ADP i_PRON rhoi_VERB sypreis_NOUN bach_ADJ iddo_ADP fe_PRON ._PUNCT
A_PART mae_VERB '_PUNCT da_ADJ ni_PRON rhywbeth_NOUN bach_ADJ i_ADP rhoid_VERB iddo_ADP fe_PRON i_ADP gofio_VERB ac_CONJ i_PRON '_PUNCT weud_VERB diolch_NOUN o_ADP galon_NOUN ._PUNCT
Bois_VERB 'w_PRON i_PRON 'n_PART nerfus_ADJ ._PUNCT
Dewch_VERB '_PUNCT da_ADJ fi_PRON ._PUNCT
[_PUNCT Cerddoriaeth_NOUN ]_PUNCT
Wel_ADP un_NUM sy_VERB 'di_PART neud_VERB mor_ADV gymaint_ADV dros_ADP y_DET clwb_NOUN chi_PRON 'n_PART gwybod_VERB ers_CONJ blynydde_VERB cyn_ADP ini_NOUN sydd_VERB fel_ADP aelode_ADP nawr_ADV i_ADP ddechre_VERB ._PUNCT
Chi_PRON 'n_PART gwybod_VERB allwch_VERB chi_PRON ddim_PART a_CONJ cael_VERB gwell_ADJ person_NOUN ._PUNCT
Chi_PRON 'n_PART gwybod_VERB mama_NOUN fe_PRON 'n_PART ._PUNCT
Mae_AUX e_PRON 'di_PART rhoi_VERB gwaith_NOUN dychrynllyd_ADJ i_ADP 'r_DET clwb_NOUN dros_ADP y_DET blynydde_VERB ._PUNCT
Pob_DET sgets_NOUN ._PUNCT
ymm_VERB pob_DET cystadleuaeth_NOUN adloniant_ADJ ._PUNCT
Efallai_ADV rhyw_DET damed_VERB yn_PART llai_ADJ dros_ADP y_DET blynyddoedd_NOUN diwetha'_ADJUNCT ond_CONJ ym_ADP mae_AUX e_PRON 'n_PART gweud_VERB ers_ADP rhyw_DET dair_NUM mlynedd_NOUN bellach_ADV bod_AUX e_PRON yn_PART cymeryd_VERB cam_NOUN nôl_NOUN a_PART gallwn_VERB ni_PRON ond_CONJ diolch_NOUN iddo_ADP fe_PRON am_ADP y_DET chwerthin_VERB [_PUNCT =_SYM ]_PUNCT a_CONJ 'r_DET [_NOUN /=_PROPN ]_PUNCT a_CONJ 'r_DET llefen_NOUN weithie_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 42_NUM :_PUNCT 19_NUM -_PUNCT 00_NUM :_PUNCT 44_NUM :_PUNCT 35_NUM
Sôn_VERB am_ADP y_DET Super_NOUN Furries_PROPN mae_VERB gan_CONJ enwg_VERB [_PUNCT -_PUNCT ]_PUNCT mae_AUX rhaid_VERB fi_PRON watchiad_NOUN bobo_VERB fi_PRON ddim_PART yn_PART cael_VERB pwl_NOUN [_PUNCT -_PUNCT ]_PUNCT mae_VERB gan_CONJ enwg_VERB 8_NUM >_SYM ._PUNCT
Prif_NOUN leisydd_NOUN y_DET grŵp_NOUN wrth_ADP gwrs_NOUN albwm_NOUN newydd_ADJ ar_ADP y_DET gweill_NOUN ._PUNCT
Mi_PART fydd_AUX e_PRON 'n_PART lansio_VERB yng_ADP Nghanolfan_NOUN y_DET Mileniwm_PROPN ar_ADP Fehefin_NOUN y_DET degfed_VERB a_CONJ by_VERB '_PUNCT Radio_NOUN Cymru_PROPN yno_ADV bois_VERB ._PUNCT
Byddwn_VERB ._PUNCT
Bydden_VERB ni_PRON yna_ADV yn_PART recordio_VERB 'r_DET cyngerdd_NOUN gyda_ADP Cherddorfa_NOUN Genedlaethol_ADJ Gymreig_ADJ y_DET BBC_PRON felly_ADV mae_VERB 'n_PART siŵr_ADJ fydd_VERB 'na_ADV dracie_NOUN gwych_ADJ i_ADP 'w_PRON '_PUNCT hwarae_ADJ o_ADP 'r_DET noson_NOUN yna_ADV 'n_PART fuan_ADJ iawn_ADV yma_ADV ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN Super_NOUN Furries_PROPN hmm_CONJ beth_PRON i_ADP 'w_PRON '_PUNCT weud_VERB gwedwch_NOUN ._PUNCT
ymm_ADP nôl_ADV at_ADP y_DET llais_NOUN ta_ADJ ._PUNCT
A_PART mae_VERB 'r_DET cynigion_NOUN [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART rai_PRON difyr_ADJ heddi_NOUN to_NOUN ._PUNCT
Dau_NUM yn_PART cynnig_ADV enwb_NOUN ._PUNCT
Na_VERB dim_PRON yr_DET hen_ADJ enwb_NOUN 19_NUM >_SYM yw_VERB hi_PRON enwg_VERB o_ADP Borthaethwy_PROPN yn_PART cynnig_ADJ enwb_NOUN 20_NUM >_SYM o_ADP sioe_NOUN enwg_VERB wrth_ADP gwrs_NOUN ._PUNCT
Na_VERB dim_PRON yr_DET hen_ADJ <_SYM /_PUNCT anon_VERB >_SYM enwb_NOUN 20_NUM >_SYM yw_VERB hi_PRON ._PUNCT
"_PUNCT Shwmae_X enwg_VERB enwb_NOUN 21_NUM >_SYM o_ADP Aberystwyth_PROPN sy_VERB 'ma_ADV ._PUNCT "_PUNCT Sut_ADV wyt_VERB ti_PRON enwb_NOUN ?_PUNCT
"_PUNCT Efe_PROPN <_SYM /_PUNCT anon_VERB >_SYM enwb_NOUN 22_NUM >_SYM yw_VERB 'r_DET llais_NOUN ?_PUNCT
Ni_PART 'di_PART bod_AUX yn_PART crafu_VERB pen_NOUN 'ma_ADV ers_ADP wythnos_NOUN diwetha'_ADJUNCT ._PUNCT "_PUNCT [_PUNCT chwerthin_VERB ]_PUNCT Nage_X ._PUNCT
Diolch_INTJ iti_NOUN enwb_NOUN ._PUNCT
Na_VERB dim_PRON <_SYM /_PUNCT anon_VERB >_SYM enwb_NOUN 22_NUM >_SYM yw_VERB e_PRON ._PUNCT
Nage_ADV enwg_VERB o_ADP Langwyryfon_PROPN yn_PART cynnig_ADJ beirniad_NOUN Fferm_NOUN Ffactor_PROPN ._PUNCT
Sef_ADV <_SYM /_PUNCT anon_VERB >_SYM enwb_NOUN 23_NUM >_SYM ._PUNCT
Na_VERB dim_PRON yr_DET hen_ADJ enwb_NOUN sydd_VERB yna_ADV enwg_VERB 12_NUM >_SYM o_ADP Bumsaint_PROPN yn_PART cynnig_ADV enwb_NOUN traffic_ADP a_CONJ tywydd_NOUN ._PUNCT
ymm_ADP bardd_NOUN y_DET mis_NOUN yma_DET ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN hefyd_ADV ._PUNCT
Na_VERB dim_PRON <_SYM /_PUNCT anon_VERB >_SYM enwb_NOUN 24_NUM >_SYM sydd_VERB yna_ADV ._PUNCT
Yr_PART hyfryd_ADJ enwb_NOUN ._PUNCT
ymm_INTJ "_PUNCT p'nawn_NOUN da_ADJ <_SYM /_PUNCT anon_VERB >_SYM enwg_VERB 6_NUM >_SYM ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB i_ADP gynnig_NOUN enwb_NOUN ._PUNCT
Diolch_VERB am_ADP y_DET rhaglen_NOUN ._PUNCT
Mwynhau_VERB yn_PART fawr_ADJ a_CONJ phob_DET lwc_NOUN iti_NOUN gan_ADP enwb_NOUN 26_NUM >_SYM ._PUNCT "_PUNCT Diolch_VERB ichi_ADP enwb_NOUN ._PUNCT
Na_VERB dim_PRON yr_DET actores_NOUN <_SYM /_PUNCT anon_VERB >_SYM enwb_NOUN 25_NUM >_SYM sydd_VERB yna_ADV ._PUNCT
A_CONJ yr_DET un_NUM dwetha_ADJ am_ADP y_DET tro_NOUN achos_CONJ mae_VERB 'na_ADV gymaint_ADV [_PUNCT =_SYM ]_PUNCT wedi_PART [_PUNCT /=_PROPN ]_PUNCT wedi_PART cysylltu_VERB ._PUNCT
A_CONJ 'i_PRON drwy_ADP 'r_DET lleill_NOUN yn_ADP y_DET man_NOUN ._PUNCT
Ond_CONJ yr_DET un_NUM nesa'_ADJUNCT ._PUNCT
Neu_PART yr_DET un_NUM dwetha_VERB gen_ADP i_PRON "_PUNCT efe_PRON Mam_ADV enwg_VERB sydd_VERB yna_ADV ?_PUNCT "_PUNCT meddai_VERB enwg_VERB 13_NUM >_SYM o_ADP Fachynlleth_PROPN ._PUNCT
Na_INTJ [_PUNCT =_SYM ]_PUNCT dim_DET enwb_NOUN sydd_VERB 'na_ADV [_PUNCT /=_PROPN ]_PUNCT dim_DET enwb_NOUN 27_NUM >_SYM sydd_VERB 'na_ADV heddi_NOUN '_PUNCT ._PUNCT
Ââ_VERB gwych_ADV bois_VERB ._PUNCT
Diolch_VERB am_ADP yr_DET holl_DET gynigion_NOUN ._PUNCT
Reit_VERB albwm_NOUN yr_DET wythnos_NOUN sydd_VERB '_PUNCT da_ADJ fi_PRON nawr_ADV ._PUNCT
"_PUNCT Ladi_X Wen_X "_PUNCT ._PUNCT
Albwm_NOUN unigol_ADJ cynta'_ADJUNCT enwb_NOUN o_ADP dan_ADP yr_DET enw_NOUN enwb_NOUN 28_NUM >_SYM a_CONJ 'r_DET Band_PROPN ._PUNCT
Gan_ADP bobo_VERB enwb_NOUN 'di_PART dathlu_VERB pen_NOUN -_PUNCT blwydd_NOUN arbennig_ADJ wrth_ADP gwrs_NOUN dechrau_VERB 'r_DET wythnos_NOUN ._PUNCT
Hon_PRON 'di_PART cael_VERB ei_PRON ryddhau_VERB gan_ADP Recordiau_NOUN Gwerin_NOUN ym_ADP mil_NUM naw_NUM wyth_NUM tri_NUM mewn_ADP cydweithrediad_NOUN ag_ADP S4C_PROPN ._PUNCT
Wedi_PART cael_VERB [_PUNCT =_SYM ]_PUNCT ei_DET [_PUNCT /=_PROPN ]_PUNCT ei_DET gyfansoddi_VERB hefyd_ADV gan_CONJ <_SYM enwn_VERB 28_NUM >_SYM a_CONJ 'i_PRON gŵr_NOUN <_SYM /_PUNCT anon_VERB >_SYM enwg_VERB 14_NUM >_SYM ._PUNCT
Wedi_PART recordio_VERB 'n_PART Stiwdio_VERB Sain_PROPN ._PUNCT
Llandwrog_PROPN a_CONJ heddi_VERB y_DET gân_NOUN oddi_ADP ar_ADP yr_DET albwm_NOUN yw_VERB hon_DET ._PUNCT
"_PUNCT Yr_PART Oes_VERB O_ADP 'r_DET Blaen_NOUN "_PUNCT ._PUNCT
O_ADP 'r_DET flwyddyn_NOUN mil_NUM naw_NUM wyth_NUM tri_NUM ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 55_NUM :_PUNCT 16_NUM -_PUNCT 00_NUM :_PUNCT 58_NUM :_PUNCT 16_NUM
Diolch_INTJ enwg_VERB ._PUNCT
P'nawn_VERB da_ADJ ichi_ADP ._PUNCT
Bydd_VERB aelodau_NOUN cynulliad_NOUN yn_PART cynnal_VERB dadl_NOUN yn_PART ddiweddarach_ADJ ar_ADP alwad_NOUN gan_ADP y_DET gwrthbleidiau_NOUN i_ADP gyhoeddi_VERB adroddiad_NOUN i_ADP amgylchiadau_NOUN diswyddo_VERB <_SYM /_PUNCT anon_VERB >_SYM enwg_VERB 15_NUM >_SYM ._PUNCT
Mae_AUX 'r_DET adroddiad_NOUN wedi_PART canfod_VERB na_PART chafodd_VERB gwybodaeth_NOUN ei_PRON rhannu_VERB 'n_PART gynnar_ADJ am_ADP ei_DET ddiswyddiad_NOUN cyn_ADP i_PART enwg_VERB ei_DET hun_PRON cael_ADV gwybod_VERB ._PUNCT
Mae_VERB 'r_DET Prif_NOUN Weinidog_PROPN <_SYM /_PUNCT anon_VERB >_SYM enwg_VERB 16_NUM >_SYM wedi_PART dweud_VERB y_PART bydd_AUX yn_PART cymryd_VERB camau_NOUN cyfreithiol_ADJ os_CONJ bydd_VERB gorchymyn_NOUN i_ADP 'w_PRON gyhoeddi_VERB ._PUNCT
Mae_VERB dyn_NOUN wnaeth_VERB ladd_VERB y_DET ferch_NOUN yr_PART oedd_AUX yn_PART rhannu_VERB tŷ_NOUN â_ADP hi_PRON a_PART thrywanu_VERB tri_NUM aelod_NOUN o_ADP 'i_PRON theulu_NOUN wedi_PART ei_PRON garcharu_VERB am_ADP leiafswm_NOUN o_ADP dri_NUM deg_NUM o_ADP flynyddoedd_NOUN ._PUNCT
Roedd_AUX enwg_VERB wedi_PART trywannu_VERB enwb_NOUN 29_NUM >_SYM oedd_VERB yn_PART bump_ADJ ar_ADP hugain_NOUN oed_NOUN ar_ADP stryd_NOUN yn_ADP Y_NOUN Rhyl_PROPN ._PUNCT
Ac_CONJ fe_PART wnaeth_VERB o_ADP anafu_VERB aelodau_NOUN o_ADP 'i_PRON theulu_NOUN wrth_ADP iddyn_ADP nhw_PRON geisio_ADV atal_VERB yr_DET ymosodiad_NOUN ._PUNCT
Mae_VERB dyn_NOUN oedd_AUX yn_PART trin_VERB gwallt_NOUN wedi_PART cael_VERB dedfryd_NOUN o_ADP garchar_NOUN am_ADP oes_VERB am_ADP fwriadol_ADJ geisio_ADV heintio_VERB deg_NUM dyn_NOUN gyda_ADP HIV_PROPN Bydd_AUX enwg_VERB sy_VERB 'n_PART saith_VERB ar_ADP hugain_PRON oes_AUX yn_PART gorfod_ADV treulio_VERB o_ADP leiaf_ADJ ddeuddeg_NUM mlynedd_NOUN gan_ADP glo_NOUN ._PUNCT
Clywodd_VERB Llys_PROPN y_DET Goron_NOUN Lewes_PROPN yn_ADP Nwyrain_PROPN Sussex_PROPN ei_PRON fod_AUX o_ADP wedi_PART gwrthod_VERB triniaeth_NOUN ac_CONJ wedi_PART anwybyddu_VERB cyngor_NOUN meddygon_NOUN yn_PART ystod_NOUN ei_DET ymgyrch_NOUN i_ADP heintio_VERB ._PUNCT
Mae_AUX chwyddiant_NOUN wedi_PART gostwng_VERB i_ADP 'w_PRON lefel_NOUN isaf_ADJ mewn_ADP blwyddyn_NOUN ._PUNCT
Roedd_AUX costau_NOUN byw_AUX wedi_PART cynyddu_VERB ddau_NUM pwynt_NOUN pump_NUM y_DET cant_NUM yn_ADP y_DET flwyddyn_NOUN hyd_NOUN at_ADP fis_NOUN Mawrth_PROPN ._PUNCT
yyy_CONJ gwaetha_ADJ 'r_DET gostyngiad_NOUN mae_AUX arbenigwyr_NOUN yn_PART dal_VERB i_ADP ddarogan_NOUN y_PART bydd_AUX banc_NOUN Lloegr_PROPN yn_PART codi_VERB cyfraddau_NOUN llog_NOUN fis_NOUN nesa'_ADJUNCT ._PUNCT
Mae_AUX 'r_DET arlywydd_NOUN cyfenw_NOUN wedi_PART cadarnhau_VERB bod_VERB cyfarwyddwr_NOUN y_DET CIA_NOUN enwg_VERB 19_NUM >_SYM wedi_PART ymweld_VERB â_ADP Gogledd_NOUN Korea_PROPN yr_DET wythnos_NOUN ddiwethaf_ADJ i_PART gyfarfod_VERB â_ADP 'r_DET arweinydd_NOUN enwg_VERB ._PUNCT
Yn_PART ôl_NOUN yr_DET arlywydd_NOUN fe_PART gafodd_VERB perthynas_NOUN dda_ADJ ei_DET sefydlu_VERB yn_ADP ystod_NOUN y_DET cyfarfod_NOUN ac_CONJ mae_VERB disgwyl_VERB i_ADP Mr_NOUN cyfenw_NOUN ei_DET hun_DET gynnal_VERB trafodaethau_VERB uniongyrchol_ADJ gyda_ADP Mr_NOUN enwg_VERB 20_NUM >_SYM erbyn_ADP mis_NOUN Mehefin_PROPN ._PUNCT
Mae_AUX cadeirydd_NOUN Pwyllgor_NOUN Diwylliant_NOUN y_DET Cynulliad_NOUN enwb_NOUN yn_PART dweud_VERB ei_PRON bod_VERB hi_PRON 'n_PART siomedig_ADJ iawn_ADV fod_AUX y_DET Llywodraeth_NOUN wedi_PART penderfynu_VERB rhoi_VERB 'r_DET gorau_NOUN i_PART gefnogi_VERB 'r_DET Proms_PROPN Cymreig_ADJ ._PUNCT
Dywedodd_VERB nad_PART oedd_AUX hi_PRON 'n_PART deall_VERB eglurhad_NOUN y_DET Llywodraeth_PROPN ._PUNCT
Nad_PART oedd_AUX y_DET digwyddiad_NOUN yn_PART bodloni_VERB 'r_DET meini_NOUN prawf_NOUN ar_ADP gyfer_NOUN cefnogaeth_NOUN ariannol_ADJ mewn_ADP cyfnod_NOUN o_ADP gynni_VERB ._PUNCT
Mae_VERB <_SYM /_PUNCT anon_VERB >_SYM enwg_VERB 21_NUM >_SYM sylfaenydd_NOUN y_DET Proms_PROPN Cymreig_ADJ yn_PART dweud_VERB ei_PRON fod_AUX o_ADP wedi_PART ei_PRON frifo_VERB gan_ADP benderfyniad_NOUN Llywodraeth_NOUN Cymru_PROPN i_ADP roi_VERB 'r_DET gorau_NOUN i_PART gefnogi_VERB 'r_DET ŵyl_NOUN ._PUNCT
Y_DET chwaraeon_NOUN sydd_VERB nesa'_ADJUNCT ._PUNCT
Dyma_ADV enwg_VERB ._PUNCT
Mae_VERB prif_ADJ hyfforddwr_NOUN y_DET Gleision_NOUN Caerdydd_PROPN enwg_VERB wedi_PART dweud_VERB bod_VERB o_PRON 'n_PART ffyddiog_ADJ y_PART bydd_AUX y_DET chwaraewr_NOUN sydd_AUX wedi_PART ennill_VERB y_DET nifer_NOUN fwyaf_ADJ o_ADP gapiau_NOUN i_ADP Gymru_PROPN ._PUNCT
Y_DET prop_NOUN enwg_VERB 24_NUM >_SYM a_CONJ 'r_DET bachwr_NOUN enwg_VERB yn_PART ffit_ADJ ar_ADP gyfer_NOUN y_DET gêm_NOUN yn_ADP erbyn_ADP Pau_PROPN yn_PART rownd_ADV gynderfynol_ADJ Cwpan_NOUN Her_PROPN Ewrop_PROPN ar_ADP Barc_NOUN yr_DET Arfau_NOUN brynhawn_NOUN Sadwrn_PROPN ._PUNCT
'_PUNCT Nath_PROPN enwg_VERB 24_NUM >_SYM a_CONJ enwg_VERB ddim_PART teithio_VERB i_ADP Dde_NOUN Affrica_PROPN ar_ADP gyfer_NOUN dwy_NUM gêm_NOUN ddwetha_VERB 'r_DET Gleision_NOUN yn_PART erbyn_ADP y_DET Cheetahs_PROPN a_CONJ 'r_DET Southern_PROPN Kings_PROPN yn_ADP y_DET Pro_X un_NUM deg_NUM pedwar_NUM oherwydd_CONJ mân_ADJ anafiadau_NOUN ._PUNCT
Bydd_VERB enwg_VERB 26_NUM >_SYM yn_PART cystadlu_VERB ym_ADP mhencampwriaeth_NOUN snwcer_ADJ y_DET byd_NOUN fydd_AUX yn_PART dechrau_VERB yn_PART Theatr_NOUN y_DET Crucible_VERB ddydd_NOUN Sadwrn_PROPN ar_ADP ôl_NOUN iddo_ADP fo_PRON guro_VERB enwg_VERB o_ADP ddeg_NOUN i_ADP ddwy_NUM yn_ADP y_DET rownd_NOUN ragbrofol_ADJ ola'_NOUNUNCT yn_ADP Sheffield_PROPN A_CONJ bydd_VERB pedwar_NOUN Cymro_PROPN arall_ADJ yn_PART ceisio_VERB sicrhau_VERB eu_DET llefydd_NOUN nhw_PRON yn_ADP y_DET Crucible_PROPN ar_ADP ôl_NOUN ailddechrau_VERB eu_DET gemau_NOUN nhw_PRON yn_ADP y_DET rownd_NOUN rhagbrofol_ADJ ola_ADJ am_ADP bump_NUM o_ADP 'r_DET gloch_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 01_NUM :_PUNCT 08_NUM :_PUNCT 40_NUM -_SYM 1_NUM :_PUNCT 11_NUM :_PUNCT 08_NUM
Chi_PRON 'n_PART gwrando_VERB arna_ADP i_PART enwg_VERB yma_ADV 'n_PART fyw_VERB ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN o_ADP stiwdio_NOUN Aberystwyth_PROPN ._PUNCT
Bron_ADV iawn_ADV yn_PART chwarter_NOUN wedi_PART tri_NUM erbyn_ADP hyn_PRON a_CONJ 'w_PRON i_PRON 'n_PART gw'bod_ADJ bod_AUX lot_NOUN fawr_ADJ o_ADP ffermwyr_NOUN ._PUNCT
Contractwyr_INTJ ._PUNCT
Peirianwyr_PROPN ._PUNCT
Ffermwyr_NOUN ifanc_ADJ ac_CONJ ati_ADP 'n_PART hoff_ADJ iawn_ADV o_ADP wrando_VERB ar_ADP Radio_NOUN Cymru_PROPN felly_ADV her_NOUN fach_ADJ i_ADP chi_PRON nawr_ADV yw_VERB 'r_DET eitem_NOUN fach_ADJ nesa_ADJ reit_ADV ._PUNCT
Sef_ADV Cnoi_PROPN Cil_PROPN ._PUNCT
Felly_CONJ dewch_VERB ini_CONJ ffeindio_VERB mas_NOUN faint_NOUN o_ADP arbenigwyr_NOUN ichi_ADP go_ADV iawn_ADJ ar_ADP eich_DET peiriannau_NOUN amaethyddol_ADJ ._PUNCT
Fydda_VERB i_PRON 'n_PART '_PUNCT ware_VERB sŵn_NOUN amaethyddol_ADJ ichi_ADP ac_CONJ yna_ADV mae_AUX 'n_PART rhaid_VERB ichi_ADP '_PUNCT weud_VERB '_PUNCT tha_PRON i_ADP sŵn_NOUN beth_DET yw_VERB e_PRON ._PUNCT
Y_PART make_NOUN a_CONJ 'r_DET model_NOUN hefyd_ADV os_CONJ ichi_ADP 'n_PART gwybod_VERB eich_DET stwff_NOUN ._PUNCT
Mae_VERB 'n_PART dipyn_PRON o_ADP her_NOUN 'w_PRON i_PRON 'n_PART gw'bod_VERB <_SYM /_PUNCT anon_VERB >_SYM enwg_VERB 1_NUM >_SYM oedd_VERB '_PUNCT da_ADJ ni_PRON wythnos_NOUN dwetha'_ADJUNCT ._PUNCT
Gath_NOUN e_PRON bach_ADJ o_ADP drafferth_NOUN yn_PART dyfalu_VERB 'r_DET sŵn_NOUN ond_CONJ mi_PRON '_PUNCT nath_NOUN e_PRON enwebu_VERB rhywun_NOUN arall_ADJ wedyn_ADV i_PART ymgymryd_VERB â_ADP 'r_DET her_NOUN wythnos_NOUN 'ma_ADV ._PUNCT
Cnoi_VERB Cil_ADJ gydag_ADP enwg_VERB ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN ._PUNCT
Ac_CONJ yn_PART cnoi_VERB cil_NOUN '_PUNCT da_ADJ fi_PRON heddi_NOUN ._PUNCT
Wedi_PART 'i_PRON enwebu_VERB wythnos_NOUN dwetha'_ADJUNCT gan_CONJ enwg_ADV enwg_VERB 2_NUM >_SYM o_ADP Bontsian_PROPN enwg_VERB shwt_NOUN wyt_VERB ti_PRON boi_NOUN ?_PUNCT
Prynhawn_NOUN da_ADJ ._PUNCT
Good_VERB diolch_NOUN enwg_VERB ._PUNCT
Shwr_PROPN wyt_VERB ti_PRON ?_PUNCT
'_PUNCT W_NUM i_PRON 'n_PART dda_ADJ iawn_ADV diolch_INTJ ._PUNCT
Wyt_AUX ti_PRON yn_PART ei_DET chanol_ADJ hi_PRON fan_NOUN 'na_ADV yn_ADP y_DET gwaith_NOUN darlithio_VERB ?_PUNCT
Llongyfarchiadau_NOUN iti_VERB ar_ADP y_DET swydd_NOUN [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART darlithio_VERB yn_ADP Gelli_PROPN Aur_PROPN enwg_VERB ?_PUNCT
Ie_INTJ 'na_ADV fe_PRON ._PUNCT
So_PART dechreuais_VERB i_PRON [_PUNCT -_PUNCT ]_PUNCT fi_PRON 'di_PART dechrau_VERB 'ma_ADV nawr_ADV ers_ADP ym_ADP mis_NOUN Awst_PROPN dwetha_ADJ ._PUNCT
So_PART fi_PRON 'n_PART  _SPACE gweithio_VERB 'ma_ADV [_PUNCT =_SYM ]_PUNCT '_PUNCT da_ADJ 'r_DET [_NOUN /=_PROPN ]_PUNCT '_PUNCT da_ADJ 'r_DET Coleg_NOUN Cymraeg_PROPN Cenedlaethol_ADJ  _SPACE darparu_VERB  _SPACE cyrsiau_NOUN addysg_NOUN uwch_ADJ i_ADP fyfyrwyr_NOUN Gelli_PROPN Aur_PROPN wedyn_ADV ._PUNCT
Ac_CONJ yn_PART mwynhau_VERB y_DET gwaith_NOUN ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Odw_X [_PUNCT /=_PROPN ]_PUNCT Odw_X ._PUNCT
Na_INTJ mae_AUX 'n_PART  _SPACE good_VERB lle_NOUN i_PART weithio_VERB chwarae_VERB teg_ADJ ._PUNCT
Dw_VERB i_PRON reit_NOUN yn_PART ganol_NOUN ffarm_NOUN fan_NOUN hyn_DET ._PUNCT
Mae_VERB digon_PRON o_ADP adnoddau_NOUN '_PUNCT da_ADJ ni_PART ambwyti_VERB 'r_DET lle_NOUN os_CONJ ti_PRON +_SYM
Ie_INTJ ._PUNCT
+_VERB moyn_ADV dangos_VERB stwff_NOUN i_ADP 'r_DET myfyrwyr_NOUN a_CONJ pethe_NOUN a_CONJ wedyn_ADV mae_AUX 'r_DET myfyrwyr_NOUN sy_VERB '_PUNCT da_ADJ ni_PRON [_PUNCT -_PUNCT ]_PUNCT wel_PART rhai_PRON fi_PRON 'n_PART dysgu_VERB i_ADP gyd_ADP trwy_ADP gyfrwng_NOUN y_DET Gymraeg_PROPN anyway_NOUN a_CONJ [_PUNCT =_SYM ]_PUNCT mae_VERB nhw_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT mae_VERB nhw_PRON 'n_PART hawdd_ADJ iawn_ADV i_PART ddod_VERB '_PUNCT mlaen_NOUN '_PUNCT da'_ADJUNCT ware_VERB teg_ADJ ._PUNCT
A_CONJ hefyd_ADV enwg_VERB mae_VERB fe_PRON 'n_PART gyfleus_ADJ i_ADP 'r_DET ffarm_NOUN adre_ADV sydd_VERB gen_ADP ti_PRON achos_CONJ mae_AUX lot_PRON o_ADP waith_NOUN yn_PART fan_NOUN 'na_ADV gen_ADP ti_PRON '_PUNCT fyd_NOUN yn_PART does_VERB ar_ADP y_DET ffarm_NOUN deuluol_ADJ ?_PUNCT
Oes_VERB ._PUNCT
Ydw_VERB fi_PRON dal_AUX yn_PART trio_ADV cadw_VERB mys_NOUN i_ADP fewn_ADP fan_NOUN '_PUNCT ny_PRON ._PUNCT
Fi_PRON 'n_PART helpu_VERB gartre_ADV '_PUNCT ._PUNCT
Ni_PART 'n_PART godro_VERB rhyw_DET chwe_NUM deg_NUM o_ADP wartheg_NOUN a_CONJ +_SYM
Ie_INTJ ._PUNCT
+_VERB t'wel_NOUN lwcus_ADJ '_PUNCT da_ADJ 'r_DET job_NOUN hyn_DET nawr_ADV ._PUNCT
Fi_PRON 'n_PART cael_ADV pigo_VERB lan_ADV mwy_PRON o_ADP syniadau_NOUN ._PUNCT
Fi_PRON 'n_PART cael_VERB  _SPACE dysgu_VERB mwy_ADV fy_DET hunan_VERB '_PUNCT fyd_NOUN ._PUNCT
Fi_PRON 'n_PART cael_VERB dysgu_VERB wrth_ADP y_DET myfyrwyr_NOUN a_CONJ +_SYM
Hmm_INTJ ._PUNCT
+_AUX fi_PRON 'n_PART dysgu_VERB wrth_ADP y_DET staff_NOUN eraill_ADJ a_CONJ 'r_DET gwahanol_ADJ brosiectau_NOUN sy_AUX 'n_PART mynd_VERB '_PUNCT mlaen_NOUN ar_ADP y_DET campws_NOUN '_PUNCT da_ADJ 'r_DET ffarm_NOUN ._PUNCT
Wedi_PART mynd_VERB fewn_ADP i_ADP 'r_DET diwydiant_NOUN godro_VERB [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART gymharol_ADV ddiweddar_ADJ ynde_ADP ._PUNCT
Yn_ADP y_DET blynyddoedd_NOUN dwetha'_ADJUNCT ._PUNCT
Shwt_PROPN mae_AUX pethe_NOUN 'n_PART mynd_VERB de_NOUN [_PUNCT =_SYM ]_PUNCT â_ADP 'r_DET [_NOUN /=_PROPN ]_PUNCT â_ADP 'r_DET newid_NOUN byd_NOUN ?_PUNCT
Ichi_VERB gyd_ADP wedi_PART setlo_VERB fewn_ADP i_ADP 'r_DET gwaith_NOUN yn_PART iawn_ADJ ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Odi_X [_PUNCT /=_PROPN ]_PUNCT Odi_X ._PUNCT
Mae_AUX fe_PRON 'n_PART good_VERB Real_PROPN strwythr_NOUN ffarm_NOUN deuluol_ADJ sy_VERB '_PUNCT da_ADJ ni_PRON ._PUNCT
10516.117_NOUN
