Fi_PRON sy_AUX 'n_PART cael_ADV mynd_VERB i_ADP 'r_DET bag_NOUN fory_ADV ._PUNCT
Dwi_VERB wrth_ADP fy_DET modd_NOUN ar_ADP y_DET ngwylie_NOUN '_PUNCT ._PUNCT
Y_DET cwis_NOUN amdani_ADP rwan_ADV ._PUNCT
Dyma_DET 'r_DET cwestiwn_NOUN cynta_ADJ ._PUNCT
Ydach_AUX chi_PRON 'n_PART cofio_VERB 'r_DET bag_NOUN mawr_ADJ oedd_AUX yn_PART dal_VERB y_DET bwyd_NOUN ?_PUNCT
Pa_PART liw_NOUN oedd_VERB o_PRON ?_PUNCT
Glas_PROPN oedd_VERB o_PRON ?_PUNCT
Melyn_ADV oedd_VERB o_PRON ?_PUNCT
Coch_ADJ oedd_VERB o_PRON ?_PUNCT
Porffor_PROPN oedd_VERB o_PRON ?_PUNCT
Neu_CONJ gwyrdd_ADJ oedd_VERB o_PRON ?_PUNCT
Dyma_DET 'r_DET ail_ADJ gwestiwn_NOUN ._PUNCT
Tro_PRON pwy_PRON oedd_VERB o_PRON i_PART fynd_VERB i_ADP 'r_DET bag_NOUN ?_PUNCT
Ai_PART tro_NOUN enwg_VERB oedd_VERB o_PRON ?_PUNCT
Neu_PART dro_NOUN enwg_VERB ._PUNCT
Ai_PART tro_NOUN enwg_VERB oedd_VERB o_PRON ?_PUNCT
Neu_PART fy_DET nhro_NOUN i_PRON ?_PUNCT
Dewch_VERB inni_PRON wylio_VERB eto_ADV i_PART weld_VERB os_CONJ gawn_VERB ni_PRON 'r_DET atebion_NOUN ._PUNCT
Gawn_VERB ni_PRON ganu_VERB 'r_DET gân_NOUN y_DET tro_NOUN yma_DET hefyd_ADV ._PUNCT
Fi_PRON sy_AUX 'n_PART cael_ADV mynd_VERB i_ADP 'r_DET bag_NOUN fory_ADV ._PUNCT
Dwi_VERB wrth_ADP fy_DET modd_NOUN ar_ADP y_DET ngwylie_NOUN '_PUNCT ._PUNCT
Y_DET cwis_NOUN amdani_ADP rwan_ADV ._PUNCT
Dyma_DET 'r_DET cwestiwn_NOUN cynta_ADJ ._PUNCT
Ydach_AUX chi_PRON 'n_PART cofio_VERB 'r_DET bag_NOUN mawr_ADJ oedd_AUX yn_PART dal_VERB y_DET bwyd_NOUN ?_PUNCT
Pa_PART liw_NOUN oedd_VERB o_PRON ?_PUNCT
Glas_PROPN oedd_VERB o_PRON ?_PUNCT
Melyn_ADV oedd_VERB o_PRON ?_PUNCT
Coch_ADJ oedd_VERB o_PRON ?_PUNCT
Porffor_PROPN oedd_VERB o_PRON ?_PUNCT
Neu_CONJ gwyrdd_ADJ oedd_VERB o_PRON ?_PUNCT
Dyma_DET 'r_DET ail_ADJ gwestiwn_NOUN ._PUNCT
Tro_PRON pwy_PRON oedd_VERB o_PRON i_PART fynd_VERB i_ADP 'r_DET bag_NOUN ?_PUNCT
Ai_PART tro_NOUN enwg_VERB oedd_VERB o_PRON ?_PUNCT
Neu_PART dro_NOUN enwg_VERB ._PUNCT
Ai_PART tro_NOUN enwg_VERB oedd_VERB o_PRON ?_PUNCT
Neu_PART fy_DET nhro_NOUN i_PRON ?_PUNCT
Dewch_VERB inni_PRON wylio_VERB eto_ADV i_PART weld_VERB os_CONJ gawn_VERB ni_PRON 'r_DET atebion_NOUN ._PUNCT
Gawn_VERB ni_PRON ganu_VERB 'r_DET gân_NOUN y_DET tro_NOUN yma_DET hefyd_ADV ._PUNCT
[_PUNCT Cân_NOUN yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
Roedd_VERB hyna_VERB 'n_PART wych_ADJ ._PUNCT
Rwan_ADV sut_ADV aeth_VERB hi_PRON efo_ADP 'r_DET cwestiynne_NOUN ?_PUNCT
Roedd_VERB yr_DET un_NUM cynta'_ADJUNCT am_ADP y_DET bag_NOUN 'na_ADV oedd_AUX yn_PART dal_VERB y_DET bwyd_NOUN ._PUNCT
Pa_PART liw_NOUN oedd_VERB o_PRON ?_PUNCT
Go_ADV dda_ADJ os_CONJ naethoch_VERB chi_PRON ateb_VERB mai_PART gwyrdd_ADJ oedd_VERB o_PRON ._PUNCT
Yr_PART ail_ADJ gwestiwn_NOUN oedd_VERB tro_NOUN pwy_PRON oedd_VERB o_PRON i_PART fynd_VERB i_ADP 'r_DET bag_NOUN ?_PUNCT
Tro_PRON enwg_VERB oedd_VERB o_PRON ._PUNCT
Da_ADJ iawn_ADV chi_PRON os_CONJ gafoch_VERB chi_PRON hwnna_PRON 'n_PART gywir_ADJ ._PUNCT
Fi_PRON gafodd_VERB fynd_VERB i_ADP 'r_DET bag_NOUN y_DET diwrnod_NOUN wedyn_ADV ._PUNCT A_CONJ cheoliwch_VERB chi_PRON byth_ADV be_ADP ddigwyddodd_VERB ._PUNCT
Yn_PART syth_ADJ ar_ADP ôl_NOUN imi_ADP fynd_VERB i_ADP mewn_ADP iddo_ADP fo_PRON dyma_DET 'r_DET ffôsn_NOUN yn_PART canu_VERB ac_CONJ roedd_AUX rhaid_VERB i_ADP enwb_NOUN fynd_VERB i_ADP 'w_PRON ateb_VERB o_ADP ._PUNCT
R'on_NOUN i_ADP mewn_ADP yno_ADV fo_PRON am_ADP oriau_NOUN ._PUNCT
Roedd_VERB o_PRON 'n_PART hyfryd_ADJ ._PUNCT
Tybed_ADV pam_ADV fod_VERB y_DET bwyd_NOUN yn_ADP y_DET bag_NOUN wastad_ADV yn_PART blasu_VERB 'n_PART well_ADJ na_CONJ 'r_DET bwyd_NOUN sy_VERB yn_PART mhowlen_NOUN i_PRON ?_PUNCT
Tan_VERB tro_NOUN nesa'_ADJUNCT ._PUNCT Hwyl_INTJ ._PUNCT
422.955_ADJ
