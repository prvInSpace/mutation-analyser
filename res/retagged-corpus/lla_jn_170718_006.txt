[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Pawb_PRON arall_ADJ i_PART redeg_VERB o_ADP gwmpas_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
OK_ADV [_PUNCT sgrechian_VERB ]_PUNCT ._PUNCT
Off_VERB you_NOUN go_ADV ._PUNCT
Dw_AUX i_PRON '_PUNCT sio_ADV gweld_AUX chi_PRON 'n_PART rhedeg_VERB o_ADP gwmpas_NOUN Go_ADV ._PUNCT
Ti_PRON angen_NOUN tynnu_VERB dy_DET sana'_NOUNUNCT i_PRON ffwrdd_ADV plis_INTJ ._PUNCT
BeBe_NOUN ?_PUNCT
Nei_VERB di_PRON dynnu_VERB dy_DET sana'_NOUNUNCT i_PRON ffwrdd_ADV os_CONJ gwelwch_VERB yn_PART dda_ADJ ?_PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ia_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
jest_ADV mama_AUX nhw_PRON 'n_PART recordio_VERB  _SPACE chi_PRON 'n_PART siarad_VERB a_CONJ ballu_VERB 'ma_ADV ._PUNCT
Ydy_VERB <_SYM aneglur_ADJ 1_NUM >_SYM recordio_VERB fi_PRON ?_PUNCT
Wel_CONJ dy_DET llais_NOUN di_PRON a_CONJ ballu_VERB '_PUNCT ndi_NOUN ._PUNCT
Iawn_INTJ ?_PUNCT
A_CONJ '_PUNCT da_ADJ chi_PRON ddim_PART yn_PART cael_ADV mynd_VERB yn_PART rhydd_ADJ '_PUNCT chos_NOUN bobo_VERB fi_PRON 'n_PART anifal_NOUN '_PUNCT wan_ADJ ._PUNCT
BeBe_VERB enwb_NOUN ?_PUNCT
enwb_VERB cyfenw_NOUN ._PUNCT
Ia_INTJ ?_PUNCT
ym_ADP ma'-ydy-'da_NOUN ni_PRON -_PUNCT dw_VERB i_PRON -_PUNCT '_PUNCT sio_VERB dw_AUX i_PRON '_PUNCT sio_VERB   _SPACE ti_PRON -_PUNCT ti_PRON '_PUNCT fo_PRON 'r_DET peth_NOUN 'na_ADV 'n_DET poced_NOUN chdi_NOUN ?_PUNCT
Yndw_VERB jest_ADV yn_PART '_PUNCT y_DET mhoced_NOUN i_PRON 'n_PART fana_ADV ._PUNCT
Pam_ADV ?_PUNCT
Ma'_VERBUNCT nhw_PRON jest_ADV yn_PART recordio_VERB '_PUNCT fo_PRON 'r_DET iaith_NOUN ._PUNCT
Haia_INTJ ._PUNCT
Tomorrow_NOUN do_PRON you_NOUN want_NOUN food_VERB brought_NOUN to_X the_X <_SYM aneglur_ADJ 1_NUM >_SYM ?_PUNCT
ym_ADP [_PUNCT -_PUNCT ]_PUNCT
Money_NOUN is_ADJ it_ADP ?_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT +_SYM
Yeah_VERB  _SPACE I_NUM think_NOUN we_NOUN should_VERB be_ADP alright_NOUN ._PUNCT
+_VERB Reit_PROPN ._PUNCT
Yeah_ADV I_ADP think_NOUN we_NOUN should_VERB be_ADP alright_NOUN [_PUNCT -_PUNCT ]_PUNCT
You_NOUN don't_NOUN want_NOUN to_NOUN bring_NOUN any_NOUN food_VERB or_CONJ anything_ADJ ?_PUNCT +_SYM
No_NOUN I_ADP think_NOUN we_NOUN should_VERB be_ADP alright_NOUN ._PUNCT
Yeah_INTJ ._PUNCT
We_VERB '_PUNCT re_NOUN bringing_ADJ some_VERB stuff_NOUN and_CONJ stuff_NOUN but_NOUN <_SYM /_PUNCT en_DET >_SYM [_PUNCT -_PUNCT ]_PUNCT
+_VERB Reit_PROPN ._PUNCT
Yeah_VERB  _SPACE we_NOUN bring_ADJ a_CONJ pack_X of_X marshmallows_NOUN or_X something_NOUN that_NOUN 's_X -_PUNCT always_ADJ things_NOUN like_VERB that_NOUN are_VERB always_ADJ ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ iawn_ADV Thank_PROPN you_X ta_ADJ -_PUNCT ra_NOUN enwg_VERB symud_VERB o_ADP fana_ADV ia_PRON ._PUNCT
Pwrpas_PROPN ydy_VERB c'nesu_NOUN fyny_ADV ddim_PART i_ADP guddiad_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Pwy_PRON sy_VERB '_PUNCT mlaen_NOUN ?_PUNCT
enwb_VERB ._PUNCT
Jest_ADV enwb_NOUN ?_PUNCT
Tag_ADJ ._PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gwbobo_VERB bebe_NOUN 'di_PART 'r_DET gêm_NOUN ._PUNCT
OK_ADV a_CONJ stop_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
BeBe_PRON sy_AUX 'n_PART digwydd_VERB os_CONJ '_PUNCT da_ADJ ni_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT
<_SYM Aneglur_PROPN 1_NUM >_SYM os_CONJ '_PUNCT da_ADJ ni_PRON 'n_PART cyffwrdd_VERB nhw_PRON mama_NOUN rhaid_VERB iddyn_ADP nhw_PRON jumpio_NOUN a_CONJ 'di_PART nhw_PRON 'm_DET yn_PART ca'l_ADV mynd_VERB yn_PART rhydd_ADJ nes_CONJ bod_AUX rhywun_NOUN yn_PART neidio_VERB o_ADP flaen_NOUN nhw_PRON [_PUNCT -_PUNCT ]_PUNCT
So_INTJ '_PUNCT da_ADJ ni_PRON 'n_PART fod_VERB i_PART neidio_VERB fel'ma_SYM -_PUNCT fel'ma_SYM -_PUNCT fel'ma_SYM [_PUNCT -_PUNCT ]_PUNCT
Ty'd_INTJ ._PUNCT
Tig_NOUN [_PUNCT sgrechian_VERB ]_PUNCT ._PUNCT
Tig_INTJ ._PUNCT
Tig_INTJ ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
'_PUNCT Dach_VERB chi_PRON gyd_ADP 'di_PART gwatchad_NOUN Ninja_PROPN Warrior_X ?_PUNCT
Na_INTJ ._PUNCT
Na_INTJ ?_PUNCT
O_ADP dw_VERB i_ADP jest_ADV yn_PART mynd_VERB i_ADP [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
OK_ADV so_VERB '_PUNCT da_ADJ chi_PRON 'n_PART gwybod_VERB bebe_NOUN '_PUNCT da_ADJ chi_PRON 'n_PART goro_VERB '_PUNCT neud_VERB ar_ADP fama_ADV ?_PUNCT
Yndach_NOUN ?_PUNCT
Iawn_ADV enwb_NOUN off_ADP you_NOUN go_ADV ._PUNCT
Ara'_VERBUNCT deg_NUM yn_PART fana_ADV OK_X ._PUNCT
Ti_PRON 'di_PART neud_VERB un_NUM <_SYM aneglur_ADJ 1_NUM >_SYM neu_CONJ dau_NUM ?_PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
enwb_VERB ffwr_NOUN '_PUNCT â_ADP chdi_NOUN enwb_NOUN 'na_ADV chdi_NOUN ._PUNCT
Ti_PRON 'n_PART -_PUNCT iawn_ADV ti_PRON 'n_PART dallt_VERB ?_PUNCT
enwb_AUX ti_PRON 'n_PART gw'bod_VERB bebe_NOUN ti_PRON 'n_PART goro_VERB '_PUNCT neud_VERB ?_PUNCT
Na_INTJ ?_PUNCT
OK_INTJ ._PUNCT
Yr_DET unig_ADJ beth_PRON ti_PRON 'n_PART goro_VERB '_PUNCT neud_VERB de_CONJ bownsio_VERB -_PUNCT bownsio_VERB -_PUNCT bownsio_VERB cer'ad_SYM ar_ADP draws_ADJ y_DET beam_NOUN de_CONJ dringo_VERB 'r_DET ffram_NOUN a_CONJ pwsio_VERB 'r_DET botwm_NOUN OK_X ti_PRON 'n_PART dod_VERB yn_PART ôl_NOUN i_ADP famama_NOUN wan_ADJ enwb_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Ffwr_NOUN '_PUNCT â_ADP chdi_NOUN enwb_NOUN ._PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
Off_VERB you_NOUN go_ADV ._PUNCT
Iawn_INTJ ?_PUNCT
Gei_VERB di_PRON fynd_VERB enwb_NOUN ._PUNCT
BeBe_VERB 'di_PART pose_NOUN chdi_ADJ ?_PUNCT
BeBe_AUX ti_PRON am_PART neud_VERB ?_PUNCT
Ti_PRON am_ADP dabio_VERB ?_PUNCT
Ti_PRON am_ADP jumpio_NOUN ?_PUNCT
BeBe_AUX ti_PRON am_PART neud_VERB ?_PUNCT
Gei_VERB 'di_PART neud_VERB '_PUNCT wbath_NOUN ._PUNCT
'_PUNCT Na_INTJ chdi_NOUN ._PUNCT
Gei_VERB di_PRON neud_VERB naid_NOUN seren_NOUN off_ADP os_CONJ ti_PRON '_PUNCT sio_X ._PUNCT
Ia_INTJ ?_PUNCT
Gen_ADP ti_PRON pose_NOUN enwb_NOUN ?_PUNCT
Ninja_PROPN Warrior_PROPN pose_NOUN ?_PUNCT
Na_INTJ ?_PUNCT
Go_ADV on_X '_PUNCT ta_X ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'n_PART teimio_VERB '_PUNCT hein_PRON wedyn_ADV ?_PUNCT
Yndan_PROPN ._PUNCT
enwb_VERB ffwr_NOUN '_PUNCT â_ADP chdi_NOUN ._PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
enwb_VERB OK_X ar_ADP ôl_NOUN dau_NUM ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Gei_VERB di_PRON os_CONJ ti_PRON '_PUNCT sio_X ._PUNCT
Ti_PRON 'm_DET yn_PART goro_VERB '_PUNCT enwg_VERB o_ADP da_ADJ enwg_VERB dw_AUX i_PRON 'n_PART licio_VERB hwna_NOUN I_ADP like_VERB that_NOUN one_NOUN __NOUN ._PUNCT
Neis_NOUN enwg_VERB ._PUNCT
'_PUNCT Dach_VERB chi_PRON '_PUNCT sio_ADV trio_ADV neud_VERB o_ADP mor_ADV sydyn_ADJ â_ADP '_PUNCT da_ADJ chi_PRON 'n_PART gallu_VERB '_PUNCT chos_NOUN fydd_AUX y_DET ni_PRON 'n_PART amseru_VERB chi_PRON wedyn_ADV ._PUNCT
Ara'_VERBUNCT deg_NUM yn_PART saff_ADJ enwg_VERB enwb_NOUN da_ADJ iawn_ADV ._PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
enwb_VERB da_ADJ iawn_ADV enwb_VERB ti_PRON am_ADP trio_VERB pose_NOUN tro_NOUN 'ma_ADV ?_PUNCT
Na_INTJ ?_PUNCT
'_PUNCT Na_INTJ chdi_NOUN ._PUNCT
'_PUNCT Na_INTJ chdi_NOUN enwb_NOUN 'na_ADV chdi_NOUN da_ADJ iawn_ADV enwb_NOUN ._PUNCT
Gen_VERB y_DET chi_PRON poses_NOUN da_ADJ heddiw_ADV [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
O_ADP 'di_PART o_ADP ar_ADP y_DET petha'_NOUNUNCT symud_VERB yr_DET [_PUNCT -_PUNCT ]_PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
enwb_VERB enwg_VERB dw_AUX i_PRON 'n_PART gweld_VERB chdi_NOUN [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Sbia_NOUN ar_ADP enwg_VERB ._PUNCT
Ma_AUX 'n_PART cogio_VERB bach_ADJ smocio_VERB 'n_PART fana_ADV mae_VERB o_PRON ._PUNCT
BeBe_NOUN ?_PUNCT
enwg_VERB [_PUNCT chwerthin_VERB ]_PUNCT enwg_VERB yn_PART gweld_VERB o_PRON ._PUNCT
Dyn_NOUN drwg_ADJ yn_ADP y_DET gongol_ADJ yn_PART fana_ADV ._PUNCT
O_ADP witchad_NOUN i_ADP fi_PRON deutha'_VERBUNCT mam_NOUN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Na_PART enwg_VERB ._PUNCT
Neis_NOUN enwg_VERB enwg_VERB o'dd_NOUN __NOUN yn_PART gweld_VERB o_ADP yn_PART ista_VERB ar_ADP y_DET rolar_NOUN de_NOUN jest_ADV yn_PART mynd_VERB fel_ADP hyn_PRON ._PUNCT
OK_ADV newni_NOUN swapio_VERB ._PUNCT
So_PART wal_NOUN at_ADP y_DET beam_NOUN Beam_PROPN at_ADP y_DET bar_NOUN ._PUNCT
Bar_NOUN at_ADP y_DET wal_NOUN ._PUNCT
Iawn_INTJ ._PUNCT
'Di_PART pawb_PRON yn_PART gw'bod_VERB be_ADP mama_NOUN nhw_PRON 'n_PART neud_VERB ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yndan_PROPN ._PUNCT
Faint_ADV o_PRON 'n_ADP y_DET chi_PRON sy_VERB 'na_ADV ?_PUNCT
Iawn_ADV dw_AUX i_PRON '_PUNCT sio_ADV gweld_VERB bob_DET un_NUM o_ADP 'n_PART -_PUNCT ohonoch_ADP chi_PRON 'n_PART '_PUNCT g'neud_VERB pose_NOUN iawn_ADJ ._PUNCT
A_PART dw_VERB i_PRON '_PUNCT sio_ADV gweld_VERB poses_NOUN da_ADJ ._PUNCT
Nes_CONJ i_PART neud_VERB -_PUNCT nes_CONJ i_PART neud_VERB o_ADP ddoe_ADV [_PUNCT -_PUNCT ]_PUNCT
Fama_ADV ._PUNCT
A_PART fydd_AUX y_DET ni_PRON w'rach_VERB yn_PART teimio_VERB chi_PRON pan_CONJ '_PUNCT da_ADJ ni_PRON 'n_PART neud_VERB o_ADP un_NUM grŵp_NOUN mawr_ADJ so_ADJ '_PUNCT da_ADJ chi_PRON '_PUNCT sio_ADV trio_ADV neud_VERB o_PRON mor_ADV gyflym_ADJ â_ADP '_PUNCT da_ADJ chi_PRON 'n_PART gallu_VERB ond_CONJ yn_PART saff_ADJ ._PUNCT
'_PUNCT Da_ADJ chi_PRON 'm_DET yn_PART goro_VERB '_PUNCT ._PUNCT
Gei_VERB di_PRON iwshio_VERB un_NUM OK_X tri_NUM dau_NUM un_NUM go_ADV ._PUNCT
Neis_NOUN enwb_VERB ffwr_NOUN '_PUNCT â_ADP chdi_NOUN ._PUNCT
Ffwr_NOUN '_PUNCT â_ADP chdi_NOUN enwb_NOUN ._PUNCT
Neis_PROPN ._PUNCT
Da_ADJ iawn_ADV ._PUNCT
Ww_ADP ._PUNCT
Ffwr_NOUN â_ADP chdi_NOUN ._PUNCT
Neis_PROPN ._PUNCT
Ffwr_NOUN â_ADP chdi_NOUN enwb_NOUN da_ADJ iawn_ADV enwg_VERB ._PUNCT
Na_INTJ ._PUNCT
Ia_INTJ ._PUNCT
Na_INTJ ._PUNCT
Ia_INTJ ._PUNCT
Nes_CONJ i_PART anghofio_VERB neud_VERB pose_NOUN ._PUNCT
Do_AUX o_PRON 'n_PART i_PRON 'n_PART meddwl_VERB enwb_NOUN ._PUNCT
Neis_NOUN enwb_VERB enwb_NOUN da_ADJ iawn_ADV __NOUN ._PUNCT
Neis_PROPN ._PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
'_PUNCT Na_INTJ chdi_NOUN enwb_NOUN ._PUNCT
Two_PART more_NOUN minutes_NOUN a_CONJ '_PUNCT da_ADJ ni_PRON 'n_PART stopio_VERB ._PUNCT
Ti_PRON 'm_DET yn_PART goro_VERB '_PUNCT mynd_VERB fela_VERB na_INTJ ?_PUNCT
'_PUNCT Da_ADJ ni_PRON -_PUNCT '_PUNCT da_ADJ ni_PRON 'n_PART amseru_VERB nhw_PRON dydan_VERB ?_PUNCT
'_PUNCT Da_ADJ ni_PRON 'n_PART amseru_VERB chi_PRON so_VERB y_DET cyflymama_NOUN sy_AUX 'n_PART neud_VERB o_ADP sy_VERB 'n_PART ennill_VERB ._PUNCT
Neis_NOUN enwb_VERB enwb_NOUN ._PUNCT
Ffwr_NOUN '_PUNCT â_ADP chdi_NOUN enwb_NOUN ._PUNCT
617.58_ADJ
OK_ADV newni_NOUN swapio_VERB +_SYM
enwg_VERB lle_ADV ti_PRON 'n_PART ca'l_VERB y_PART lolipops_VERB 'ma_ADV o_PRON ?_PUNCT
Lle_ADV ti_PRON 'n_PART ca'l_VERB nhw_PRON o_ADP ?_PUNCT
Reit_VERB dw_VERB i_PRON '_PUNCT sio_ADV gweld_VERB pose_NOUN gan_ADP bob_DET un_NUM o_ADP 'n_ADP y_DET chi_PRON OK_X ._PUNCT
I_ADP fi_PRON ti_PRON 'n_PART goro_VERB '_PUNCT d'eud_VERB strike_NOUN a_CONJ pose_NOUN ._PUNCT
OK_INTJ ._PUNCT
Ti_PRON 'n_PART goro_VERB '_PUNCT rhoi_VERB un_NUM i_ADP fi_PRON ia_ADJ [_PUNCT -_PUNCT ]_PUNCT
Reit_VERB '_PUNCT da_ADJ chi_PRON 'n_PART barod_ADJ ?_PUNCT
One_ADP last_NOUN strike_ADJ a_CONJ pose_NOUN ._PUNCT
Jest_ADV cerdded_VERB y_DET beam_NOUN ._PUNCT
Ti_PRON 'm_DET yn_PART goro_VERB '_PUNCT neud_VERB y_DET ddau_NUM ._PUNCT
+_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT i_PART neud_VERB y_DET stopwatch_VERB ._PUNCT
Nawni_ADV neud_VERB dau_NUM yr_DET un_NUM pryd_NOUN so_VERB unwaith_ADV mama_NOUN un_NUM yn_PART cyrra'dd_NOUN y_DET <_SYM aneglur_ADJ 2_NUM >_SYM [_PUNCT -_PUNCT ]_PUNCT
Dechra'_VERBUNCT un_NUM arall_ADJ enwg_VERB strike_NOUN a_CONJ pose_NOUN ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART licio_VERB hwnna_PRON enwg_VERB enwb_NOUN strike_NOUN a_CONJ pose_NOUN ._PUNCT
Da_ADJ ._PUNCT
Edrych_VERB yn_PART very_VERB fierce_NOUN yn_PART fana_ADV enwb_NOUN ._PUNCT
Ti_PRON 'm_DET yn_PART goro_VERB '_PUNCT neud_VERB y_DET ddau_NUM os_CONJ ti_PRON 'm_DET isio_VERB enwb_NOUN enwb_NOUN strike_NOUN a_CONJ pose_NOUN ._PUNCT
O_ADP da_ADJ enwb_NOUN ._PUNCT
Ti_PRON 'm_DET yn_PART goro_VERB '_PUNCT neud_VERB y_DET ddau_NUM os_CONJ ti_PRON 'm_DET isio_VERB enwb_NOUN enwb_NOUN strike_NOUN a_CONJ pose_NOUN go_ADV on_X ._PUNCT
Ty'd_NOUN come_VERB on_X ._PUNCT
'_PUNCT Neud_VERB un_NUM bach_ADJ ._PUNCT
'_PUNCT Na_INTJ chdi_NOUN enwb_VERB strike_NOUN a_CONJ pose_NOUN ._PUNCT
BeBe_AUX ti_PRON 'n_PART mynd_VERB i_PART neud_VERB ?_PUNCT
Gei_VERB di_PRON jest_ADV neud_VERB hyna_VERB os_CONJ ti_PRON '_PUNCT sio_VERB enwb_NOUN strike_NOUN a_CONJ pose_NOUN strike_ADJ a_CONJ pose_NOUN ._PUNCT
Oh_X yeah_ADP ._PUNCT
enwg_VERB strike_NOUN a_CONJ pose_NOUN enwb_VERB strike_NOUN a_CONJ pose_NOUN ._PUNCT
O_ADP dw_AUX i_PRON 'n_PART licio_VERB hwna_NOUN enwb_NOUN dylsa_ADP '_PUNCT chdi_NOUN 'di_PART gweld_VERB pose_NOUN enwb_NOUN '_PUNCT wan_ADJ ._PUNCT
O'dd_NOUN o_ADP 'n_PART un_NUM da_ADJ ._PUNCT
Two_PART more_NOUN minutes_NOUN ._PUNCT
Dw_VERB i_PRON '_PUNCT sio_VERB chdi_NOUN neud_VERB hwna_NOUN pan_CONJ ti_PRON 'n_PART neud_VERB o_ADP iawn_ADV enwb_NOUN ?_PUNCT
Dw_VERB i_PRON '_PUNCT sio_VERB chdi_NOUN neud_VERB y_DET pose_NOUN yna_ADV pan_CONJ ti_PRON 'n_PART neud_VERB o_ADP enwb_NOUN strike_NOUN a_CONJ pose_NOUN ._PUNCT
Neis_NOUN enwb_VERB strike_NOUN a_CONJ pose_NOUN ._PUNCT
Da_ADJ enwb_VERB strike_NOUN a_CONJ pose_NOUN enwb_VERB strike_NOUN a_CONJ pose_NOUN ._PUNCT
Da_ADJ ._PUNCT
Neis_ADJ enwg_VERB strike_NOUN a_CONJ pose_NOUN ._PUNCT
Neis_PROPN ._PUNCT
Dw_VERB i_PRON 'n_PART really_ADV licio_NOUN hwna_NOUN enwb_NOUN ._PUNCT
Ww_ADP ._PUNCT
Neis_NOUN enwb_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT Go_X on_X enwb_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ma_VERB 'n_PART iawn_ADJ ._PUNCT
OK_NOUN stop_NOUN ._PUNCT
Dewch_VERB i_PRON '_PUNCT ista_NOUN 'n_PART fama_ADV ._PUNCT
Ti_PRON 'n_PART gw'bod_VERB bebe_NOUN ?_PUNCT
Ma'_VERBUNCT enwa'_VERBUNCT ni_PRON ar_ADP yr_DET [_NOUN aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
O'dd_NOUN y_DET nhw_PRON 'm_DET yn_PART bothered_NOUN ddoe_ADV nagoedd_NOUN so_VERB na_CONJ i_ADP sgriblo_VERB ni_PRON allan_ADV ._PUNCT
Ti_PRON 'n_PART goro_VERB '_PUNCT mynd_VERB i_ADP teimio_VERB ._PUNCT
Iawn_INTJ ._PUNCT
'_PUNCT Da_ADJ ni_PRON am_ADP neud_VERB y_DET cwrs_NOUN cyfa'_NOUNUNCT '_PUNCT wan_ADJ ._PUNCT
So_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Fyny_ADV 'r_DET wal_NOUN ._PUNCT
Ar_ADP draws_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Dan_VERB y_DET twnel_NOUN ._PUNCT
Trwy_ADP 'r_DET twnel_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT You_NOUN get_NOUN three_NOUN goes_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
So_NOUN os_CONJ '_PUNCT da_ADJ chi_PRON 'n_PART cyffwrdd_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT '_PUNCT da_ADJ ni_PRON 'n_PART stopio_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Dw_AUX i_PRON 'n_PART rwbio_VERB un_NUM fi_PRON ._PUNCT
Na_INTJ ._PUNCT
Oo_INTJ ._PUNCT
Iawn_INTJ OK_X ._PUNCT
I_ADP won't_NOUN be_ADP able_VERB to_NOUN do_ADP this_VERB '_PUNCT cause_X of_X my_NOUN knee_VERB ._PUNCT
Oh_X that_NOUN 's_X fine_NOUN ._PUNCT
Someone_ADJ in_X your_NOUN team_NOUN going_NOUN twice_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Un_NUM o_ADP 'n_ADP y_DET chi_PRON 'n_PART mynd_VERB ddwywaith_ADV ._PUNCT
yyy_ADV enwg_VERB do_PRON it_ADP properly_NOUN Go_ADV on_X enwg_VERB ._PUNCT
yyy_ADV -_PUNCT err_NOUN Go_ADV on_X genod_VERB [_PUNCT chwerthin_VERB ]_PUNCT enwb_VERB  _SPACE na_CONJ enwg_VERB ._PUNCT
[_PUNCT Sgrechian_NOUN ]_PUNCT
What_NOUN was_NOUN that_ADJ ?_PUNCT
Well_ADJ it_ADP 's_X safe_X to_NOUN say_NOUN ._PUNCT
It_ADP 's_X safe_X to_NOUN say_NOUN that_NOUN wasn't_X the_X cleanest_X of_X races_X ._PUNCT
Both_VERB of_X you_X are_NOUN technically_CONJ disqualified_VERB OK_X ?_PUNCT <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT rules_NOUN and_ADJ regulations_NOUN [_PUNCT -_PUNCT ]_PUNCT
Yndw_VERB ar_ADP gyfer_NOUN arbrawf_NOUN ._PUNCT
You_NOUN didn't_SYM even_VERB say_NOUN rules_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
That_ADJ 's_X true_X OK_X ._PUNCT
You_ADV said_NOUN all_VERB we_NOUN had_CONJ to_NOUN do_DET was_NOUN get_X to_X the_X other_ADP side_VERB [_PUNCT -_PUNCT ]_PUNCT
So_PART tro_NOUN 'ma_ADV ._PUNCT
Ma'_VERBUNCT hyna_VERB 'n_PART wir_ADJ ._PUNCT
Tro_VERB 'ma_ADV you_NOUN '_PUNCT re_NOUN gonna_NOUN do_PART forward_NOUN rolls_NOUN with_NOUN your_NOUN space_ADJ hopper_NOUN OK_X   _SPACE sh_NUM ._PUNCT
You_ADV still_NOUN go_ADV through_ADJ the_X hoop_X <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
'_PUNCT Dach_VERB chi_PRON dal_ADV yn_PART mynd_VERB trwy_ADP 'r_DET hoop_VERB  _SPACE a_CONJ wedyn_ADV '_PUNCT da_ADJ chi_PRON 'n_PART dod_VERB yn_PART ôl_NOUN OK_X ?_PUNCT
No_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
That_ADJ 's_X a_CONJ bit_DET unfair_NUM ._PUNCT
enwg_VERB you_NOUN go_ADV twice_ADJ ._PUNCT
Iawn_INTJ '_PUNCT da_ADJ ni_PRON 'n_PART barod_ADJ ?_PUNCT
'_PUNCT Da_ADJ ni_PRON 'n_PART barod_ADJ ?_PUNCT
Ar_ADP eich_DET marcia'_VERBUNCT ._PUNCT
Barod_PROPN ._PUNCT
Ewch_VERB You_ADV have_CONJ to_NOUN hold_NOUN it_ADP ._PUNCT
Hold_NOUN the_X ears_NOUN enwg_VERB hold_NOUN the_X ears_NOUN enwg_VERB try_VERB and_ADJ hold_NOUN the_X ears_NOUN __NOUN ._PUNCT
ymm_NOUN don't_NOUN go_ADV out_ADP the_X door_NOUN ._PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
Everyone_NOUN in_ADP your_NOUN team_NOUN needs_NOUN to_NOUN go_ADV twice_ADJ ._PUNCT
Someone_ADJ needs_NOUN to_NOUN go_ADV three_NOUN times_NOUN in_ADP your_NOUN team_NOUN ._PUNCT
Go_ADV on_X <_SYM /_PUNCT en_PRON >_SYM enwb_NOUN da_ADJ iawn_ADV ._PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART mynd_VERB i_PART dd'eud_VERB de_NOUN '_PUNCT sa_AUX ni_PRON 'n_PART gallu_VERB os_CONJ '_PUNCT sa_VERB nhw_PRON efo_ADP dau_NUM crash_NOUN mat_PRON yr_DET un_NUM ._PUNCT
Fysa'_VERBUNCT nhw_PRON 'n_PART gallu_ADV mynd_VERB ar_ADP y_DET crash_NOUN mat_NOUN rhoid_VERB yr_DET un_NUM arall_ADJ lawr_ADV mynd_VERB ar_ADP hwna_NOUN ._PUNCT
Ond_CONJ '_PUNCT da_ADJ ni_PRON '_PUNCT mond_ADJ efo_ADP dau_NUM ._PUNCT
So_PART mama_NOUN hyna_ADV 'n_PART annoying_ADJ ._PUNCT
'_PUNCT Sa_AUX ni_PRON 'n_PART gallu_ADV neud_VERB o_ADP efo_ADP 'r_DET hoops_VERB enwb_NOUN ._PUNCT
BeBe_PRON sy_AUX 'n_PART mynd_VERB ymlaen_ADV ?_PUNCT
Eh_PART eh_NOUN no_PRON no_PRON no_PRON ._PUNCT
Don't_NOUN break_ADJ them_ADJ ._PUNCT
Don't_NOUN break_ADJ them_ADJ ._PUNCT
They_VERB belong_NOUN to_NOUN someone_ADJ ._PUNCT
OK_INTJ ?_PUNCT <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Ti_PRON 'n_PART mynd_VERB i_ADP nôl_NOUN hoops_PRON ?_PUNCT
Can_VERB I_NUM [_PUNCT -_PUNCT ]_PUNCT
OK_INTJ ._PUNCT
BeBe_VERB '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB i_PART neud_VERB '_PUNCT wan_ADJ ?_PUNCT
Dw_AUX i_PRON '_PUNCT sio_VERB chi_PRON ff'indio_VERB partner_NOUN Find_PROPN a_CONJ partner_NOUN ._PUNCT
So_PART mama_VERB nhw_PRON '_PUNCT isio_VERB dau_NUM yr_DET un_NUM ond_CONJ mama_VERB nhw_PRON mewn_ADP partner_NOUN OK_X boys_NOUN do_ADP you_NOUN mind_NOUN putting_X the_X space_ADJ hoppers_NOUN to_NOUN  _SPACE in_X the_X office_X please_ADJ ?_PUNCT
Iawn_INTJ in_X each_X of_X your_X partners_NOUN you_NOUN need_NOUN two_PART hoops_PRON ._PUNCT
Dim_DET ots_NOUN os_CONJ 'di_PART nhw_PRON 'n_PART fach_ADJ enwg_VERB  _SPACE ._PUNCT
Pwy_PRON ti_PRON efo_ADP ?_PUNCT
enwb_VERB ?_PUNCT
OK_ADV girls_VERB ._PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
Iawn_ADV chi_PRON bia'_ADJUNCT hwna_NOUN ._PUNCT
Iawn_ADV genod_VERB ?_PUNCT
Genod_PROPN ?_PUNCT
Girls_VERB you_NOUN come_VERB to_NOUN this_VERB one_NOUN enwb_NOUN a_CONJ enwb_VERB i_ADP fama_ADV ._PUNCT
Genod_VERB  _SPACE enwb_NOUN a_CONJ enwb_VERB i_ADP fama_ADV ._PUNCT
Fama_ADV enwg_VERB a_CONJ enwg_VERB you_NOUN in_ADP partners_NOUN ?_PUNCT
'_PUNCT Dach_VERB chi_PRON '_PUNCT fo_PRON 'ch_PRON gilydd_NOUN ?_PUNCT
Boys_NOUN are_VERB you_NOUN together_NOUN ?_PUNCT
OK_INTJ ._PUNCT
You_ADV go_ADV there_VERB ._PUNCT
OK_ADV <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Dos_VERB di_PRON i_ADP fana_ADV It_ADP 's_X not_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Just_NOUN stand_NOUN with_NOUN your_ADJ hoops_VERB for_CONJ a_CONJ minute_VERB ._PUNCT
'_PUNCT Da_ADJ ni_PRON '_PUNCT sio_X dau_NUM arall_ADJ i_PART enwg_VERB a_CONJ <_SYM aneglur_ADJ 1_NUM >_SYM ._PUNCT
yyy_ADV enwg_VERB OK_PROPN na_CONJ i_ADP jest_ADV egluro_VERB 'n_PART sydyn_ADJ i_ADP chi_PRON bebe_NOUN da_ADJ ni_PRON 'n_PART mynd_VERB i_PART neud_VERB felly_ADV ._PUNCT
Yn_ADP eich_DET partner_NOUN bebe_NOUN fydd_AUX y_DET chi_PRON 'n_PART neud_VERB ._PUNCT
'_PUNCT Da_ADJ chi_PRON 'n_PART mynd_VERB i_ADP roi_VERB nhw_PRON lawr_ADV Boys_PROPN are_VERB you_NOUN listening_X ?_PUNCT ._PUNCT
Fewn_ADP i_ADP fama_ADV Pass_PROPN fela_VERB And_PROPN then_NOUN all_ADP the_X way_NOUN to_X the_X other_ADP end_NOUN ._PUNCT
End_CONJ and_X back_NOUN OK_X ?_PUNCT
Did_VERB you_NOUN listen_NOUN ?_PUNCT <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
No_INTJ ._PUNCT
Oh_INTJ ._PUNCT
Where_VERB 's_X your_NOUN partner_NOUN ?_PUNCT
OK_ADV <_SYM /_PUNCT en_PRON >_SYM '_PUNCT da_ADJ chi_PRON 'n_PART fana_ADV ?_PUNCT
BeBe_VERB '_PUNCT da_ADJ ni_PRON 'n_PART goro_VERB '_PUNCT neud_VERB ?_PUNCT
Dw_VERB i_PRON newi_VERB '_PUNCT -_PUNCT wrand_NOUN dw_VERB i_ADP newi_VERB '_PUNCT dd'eud_VERB ._PUNCT
I_ADP don't_NUM know_VERB what_NOUN we_NOUN '_PUNCT re_NUM doing_NOUN ._PUNCT
OK_ADV come_VERB here_VERB then_NOUN ._PUNCT
We_VERB '_PUNCT ll_NOUN get_NOUN a_CONJ practice_NOUN first_NOUN ._PUNCT
Practice_PROPN ._PUNCT
You_ADV stand_NOUN there_ADV ._PUNCT
Put_NOUN that_NOUN down_VERB jump_X into_VERB it_PRON ._PUNCT
Stand_NOUN oh_X we_NOUN need_NOUN three_NOUN <_SYM /_PUNCT en_PRON >_SYM enwb_NOUN enwb_NOUN __NOUN ga'l_NOUN mwy_ADJ ._PUNCT
O_ADP 's_X '_PUNCT a_CONJ mwy_ADJ ?_PUNCT
OK_ADV wait_VERB a_CONJ minute_VERB ._PUNCT
You_ADV '_PUNCT re_NOUN getting_NOUN three_NOUN hoops_PRON each_ADJ ._PUNCT
Sorry_VERB it_ADP 's_X my_NOUN miscalculation_NOUN we_NOUN need_NOUN three_NOUN each_ADJ ._PUNCT
OK_INTJ ?_PUNCT <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
'_PUNCT Dach_VERB chi_PRON angen_NOUN tri_NUM You_NOUN need_VERB three_NOUN ._PUNCT
No_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Iawn_ADV na_PART i_PRON ddangos_VERB i_ADP chi_PRON '_PUNCT wan_ADJ ._PUNCT
So_INTJ [_PUNCT -_PUNCT ]_PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
pawb_PART ga_NOUN i_PRON jest_ADV pawb_PRON yn_PART gwrando_VERB enwg_VERB I_ADP just_ADV feel_NOUN like_VERB everyone_NOUN 's_DET talking_ADJ over_VERB me_ADP shwsh_NOUN ._PUNCT
Shwsh_PROPN ._PUNCT
Boys_PROPN ._PUNCT
Sometimes_NOUN I_ADP do_DET think_NOUN we_NOUN waste_NOUN more_NOUN time_NOUN '_PUNCT cause_NOUN I_ADP feel_NOUN like_ADJ I_ADP 'm_DET talking_NOUN and_CONJ there_VERB 's_X always_X people_NOUN fiddling_ADJ OK_X and_X playing_X around_X ._PUNCT
It_ADP 's_X really_X frustrating_NOUN OK_X ?_PUNCT
When_ADV I_ADP explain_NOUN please_ADJ just_ADJ listen_NOUN so_VERB we_NOUN can_NOUN just_NOUN get_PRON it_ADP over_VERB with_NOUN ._PUNCT
Put_NOUN the_X hoop_X on_X the_X floor_NOUN ._PUNCT
In_INTJ <_SYM /_PUNCT en_PRON >_SYM enwg_VERB ti_PRON 'n_ADP yr_DET un_NUM nesa'_ADJUNCT ._PUNCT
Ti_PRON 'n_PART pasio_VERB hwn_PRON i_ADP fi_PRON ._PUNCT
Lawr_ADV ._PUNCT
Ww_ADP ._PUNCT
Fewn_ADP ._PUNCT
Codi_VERB Across_PROPN ._PUNCT
Lawr_ADV OK_X quick_ADJ as_ADP you_NOUN can_NOUN from_NOUN one_X side_X to_X the_X other_ADP and_NOUN back_NOUN OK_X ?_PUNCT Lle_ADV mama_NOUN enwb_NOUN ?_PUNCT
You_ADV '_PUNCT re_NOUN gonna_NOUN have_CONJ to_NOUN wait_VERB for_CONJ enwb_VERB OK_X ?_PUNCT
._PUNCT
If_VERB you_NOUN go_ADV outside_ADJ of_X the_X thing_NOUN do_ADP you_NOUN have_CONJ to_NOUN go_ADV back_ADJ to_X the_X start_X ?_PUNCT
Yes_DET but_NOUN you_NOUN won't_NOUN probably_NOUN fall_ADJ out_ADP the_X hoop_X ._PUNCT
It_ADP 's_DET quite_NOUN hard_ADJ ._PUNCT
You_ADV can_NOUN have_NOUN a_CONJ practice_NOUN first_NOUN ._PUNCT
Off_ADP you_NOUN go_ADV <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
'Di_PART nhw_PRON 'n_PART iawn_ADJ ?_PUNCT
Yndi_INTJ ._PUNCT
'Di_PART 'r_DET toilet_VERB yna_ADV 'n_PART iawn_ADJ '_PUNCT wan_ADJ ?_PUNCT
Fyddai_VERB '_PUNCT isio_ADV mynd_VERB yn_PART munud_NOUN ._PUNCT
'Di_PART hwna_NOUN 'n_PART iawn_ADJ ?_PUNCT
Dw_VERB i_PRON 'n_PART siŵr_ADJ bod_VERB enwb_NOUN yn_PART un_NUM o_ADP '_PUNCT nhw_PRON ond_ADP [_PUNCT -_PUNCT ]_PUNCT
So_PART bebe_NOUN sy_VERB 'n_PART -_PUNCT  _SPACE bebe_NOUN 'di_PART 'r_DET broblem_NOUN ?_PUNCT
Ia_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
O_ADP dan_ADP yndi_ADJ ._PUNCT
ôô_NOUN ._PUNCT
Yn_ADP y_DET peips_NOUN yn_PART fama_ADV ._PUNCT
Ond_CONJ o_ADP 'n_PART i_PRON 'n_PART siarad_VERB efo_ADP bobl_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM a_CONJ mama_VERB nhw_PRON 'di_PART bod_AUX yn_PART gael_VERB trafferth_NOUN ers_ADP misoedd_NOUN ._PUNCT
O_ADP na_PART ._PUNCT
Cyn_CONJ y_DET ni_PRON t'bo_NOUN ?_PUNCT
Ond_CONJ fedra'_VERBUNCT chi_PRON 'm_PART dechra'_VERBUNCT tynnu_VERB fo_PRON gyd_ADV allan_ADV na_PART ar_ADP hyd_NOUN o_ADP bryd_NOUN na_ADV ?_PUNCT
Gen_VERB y_DET ni_PRON fatha_VERB  _SPACE [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
OK_INTJ ._PUNCT
Jest_ADV ca'l_VERB gwared_NOUN o_ADP 'n_ADP y_DET fo_PRON ._PUNCT
Ia_INTJ [_PUNCT -_PUNCT ]_PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT bebe_NOUN sy_VERB 'na_ADV mama_NOUN siŵr_ADJ 'di_PART bobo_VERB 'na_ADV  _SPACE y_DET peipia'_VERBUNCT 'di_PART mynd_VERB lawr_ADV ffor_CONJ '_PUNCT 'na_ADV ._PUNCT
Ia_INTJ ._PUNCT
Ia_INTJ ._PUNCT
Ma_INTJ '_PUNCT 'na_ADV root_NOUN coeden_NOUN neu_CONJ '_PUNCT wbath_NOUN 'di_PART blocio_VERB fo_PRON reit_ADV lawr_ADV +_SYM
Did_VERB you_X block_X the_X gate_X or_X the_X door_NOUN with_NOUN your_NOUN car_NOUN ?_PUNCT
I_ADP thought_X I_NUM forgot_NOUN to_NOUN tell_NOUN you_NOUN earlier_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
The_ADP garage_NOUN ?_PUNCT
Not_ADP this_VERB one_NOUN ._PUNCT
Our_NOUN one_NOUN ._PUNCT
You_ADV know_VERB the_X one_CONJ next_VERB to_NOUN it_PRON ?_PUNCT
Don't_NOUN park_VERB your_NOUN car_NOUN over_ADP it_ADP <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Oh_X why_NOUN ?_PUNCT
'_PUNCT Cause_PROPN there_ADV are_VERB cones_VERB there_ADP and_ADJ [_PUNCT -_PUNCT ]_PUNCT
Who_ADV -_PUNCT own_X I_NUM thought_X it_ADP was_NOUN ga_ADJ -_PUNCT  _SPACE empty_NOUN ?_PUNCT
No_NOUN somebody_NOUN parked_NOUN over_VERB it_ADP last_NOUN week_ADJ on_X a_X Thursday_X and_X somebody_ADJ needed_NOUN to_NOUN get_NOUN into_ADP it_ADP with_NOUN like_VERB a_CONJ hundred_NOUN and_ADJ fifty_NOUN grand_NOUN car_NOUN and_ADJ like_VERB a_CONJ brand_NOUN new_ADJ Rolls_PROPN Royce_X and_X I_NUM couldn't_NUM get_NOUN a_CONJ hold_NOUN of_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM to_NOUN move_NOUN the_X car_NOUN '_PUNCT cause_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT gone_ADJ for_CONJ a_CONJ run_NOUN ._PUNCT
Oh_INTJ so_INTJ -_PUNCT someone_ADJ does_NOUN someone_ADJ need_VERB me_ADP to_NOUN move_VERB it_PRON ?_PUNCT
Not_VERB now_VERB I_ADP don't_NOUN think_NOUN ._PUNCT
No_INTJ ._PUNCT
Oh_INTJ ._PUNCT
I_ADP thought_NOUN it_ADP was_NOUN OK_X to_NOUN park_VERB there_ADP see_NOUN <_SYM /_PUNCT en_PRON >_SYM +_SYM
You_ADV know_VERB what_NOUN I_ADP mean_NOUN though_NOUN ?_PUNCT
+_VERB Yeah_X where_VERB I_NUM park_VERB now_ADJ +_SYM
I_ADP think_NOUN it_ADP is_CONJ OK_X ._PUNCT
Can_VERB you_NOUN see_DET them_X <_SYM /_PUNCT en_PRON >_SYM ?_PUNCT
+_VERB I_ADP '_PUNCT ll_NOUN move_VERB it_ADP then_NOUN yeah_ADV [_PUNCT -_PUNCT ]_PUNCT
Don't_ADJ -_PUNCT think_NOUN I_ADP think_NOUN it_ADP '_PUNCT ll_NOUN be_ADP alright_NOUN now_VERB '_PUNCT cause_NOUN nobody_NOUN 's_X gonna_NOUN come_VERB now_PROPN at_ADP this_VERB time_NOUN +_SYM
No_INTJ ._PUNCT
Plus_ADJ they_VERB can_NOUN come_ADP in_X and_X I_ADP can_NOUN move_VERB it_ADP yeah_VERB ?_PUNCT
I_ADP didn't_NUM know_VERB ._PUNCT
If_VERB it_ADP 's_X OK_X I_ADP '_PUNCT ll_NOUN come_VERB after_VERB you_NOUN ._PUNCT
Haha_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Na_VERB i_ADP sortio_VERB fo_PRON allan_ADV '_PUNCT wan_ADJ ._PUNCT
Iawn_INTJ ._PUNCT
Barod_PROPN ?_PUNCT
Lle_ADV mama_NOUN enwb_NOUN ?_PUNCT
She_NOUN goes_NOUN for_X the_X longest_X breaks_NOUN ._PUNCT
Dos_VERB i_PRON '_PUNCT nôl_ADV hi_PRON '_PUNCT ta_CONJ enwb_NOUN '_PUNCT cause_NOUN ti_PRON methu_VERB chwara'_VERBUNCT tan_CONJ mama_NOUN hi_PRON 'n_PART '_PUNCT nôl_NOUN ._PUNCT
enwb_VERB ?_PUNCT
Ia_INTJ ?_PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
No_NOUN we_NOUN can_NOUN leave_ADV it_ADP as_VERB it_ADP it_PRON ._PUNCT
I_ADP know_VERB your_ADJ problem_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT excercise_VERB ball_ADJ after_ADV ?_PUNCT
Cei_INTJ ._PUNCT
Yes_INTJ ._PUNCT
Iawn_INTJ ._PUNCT
yyy_CONJ rh'wbath_NOUN iddyn_ADP nhw_PRON ._PUNCT
Ma'_VERBUNCT nhw_PRON '_PUNCT sio_ADV recordio_VERB fi_PRON neu_CONJ '_PUNCT wbath_NOUN ._PUNCT
Iawn_INTJ '_PUNCT da_ADJ ni_PRON 'n_PART barod_ADJ ?_PUNCT
You_ADV need_VERB all_ADP of_X your_NOUN hoops_ADP in_ADP your_NOUN hands_NOUN to_X start_NOUN ._PUNCT
But_NOUN when_NOUN you_NOUN turn_X around_X you_X '_PUNCT re_X OK_X to_NOUN leave_NOUN them_NOUN OK_X ?_PUNCT
As_PART long_NOUN as_ADJ one_NOUN hoop_VERB touches_NOUN that_NOUN line_NOUN you_NOUN can_NOUN come_VERB back_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Ar_ADP eich_DET marciau_NOUN ._PUNCT
yyy_ADV start_NOUN from_X the_X beginning_NOUN beginning_ADJ ._PUNCT
You_ADV need_VERB to_ADJ hold_NOUN your_NOUN first_NOUN hoop_VERB <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Barod_PROPN Go_X ._PUNCT
[_PUNCT Plant_PROPN yn_PART chwarae_VERB ]_PUNCT ._PUNCT
Ti_PRON 'n_PART iawn_ADJ enwg_VERB ?_PUNCT
Yndw_INTJ ._PUNCT
[_PUNCT Gweiddi_VERB ]_PUNCT ._PUNCT
OK_ADV that_NOUN was_NOUN also_NOUN a_CONJ practice_NOUN '_PUNCT cause_NOUN I_ADP noticed_NOUN a_CONJ lot_NOUN of_X you_X were_VERB rushing_ADJ it_ADP and_NOUN stepping_X out_ADP of_X your_NOUN hoops_VERB ._PUNCT
So_NOUN I_NUM could_VERB '_PUNCT ve_VERB made_NOUN you_X start_X from_X the_X beginning_NOUN ._PUNCT
So_NOUN a_CONJ lot_PRON of_X people_X were_VERB throwing_NOUN their_NOUN hoops_VERB and_NOUN then_NOUN kinda_NOUN going_ADP ww_PRON and_NOUN going_X back_ADV into_ADP it_PRON ._PUNCT
If_VERB I_ADP see_NOUN that_NOUN now_ADJ I_NUM '_PUNCT ll_ADV make_VERB you_NOUN go_ADV back_ADJ to_X the_X start_X unfortunately_NOUN ._PUNCT
OK_ADV so_VERB no_PRON cheating_NOUN ._PUNCT
OK_ADV so_VERB the_X tactic_NOUN is_ADJ do_PART them_VERB short_ADJ but_NOUN quick_ADJ and_ADJ that_NOUN 's_X what_X I_ADP think_NOUN OK_X ?_PUNCT
So_PART are_VERB we_NOUN ready_NOUN ?_PUNCT <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Ar_ADP eich_DET marciau_NOUN ._PUNCT
Barod_PROPN ._PUNCT
Ewch_VERB No_NOUN back_NOUN to_X the_X start_X ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT we_NOUN couldn't_ADJ do_ADP it_PRON ._PUNCT
Back_NOUN to_X the_X start_X ._PUNCT
Back_NOUN to_X the_X start_X boys_NOUN right_NOUN to_X the_X start_NOUN enwb_NOUN start_NOUN ._PUNCT
[_PUNCT Gweiddi_VERB a_CONJ chwarae_VERB ]_PUNCT ._PUNCT
Hand_VERB on_X the_X floor_NOUN ._PUNCT
Hand_VERB on_X the_X floor_NOUN enwg_VERB start_NOUN again_NOUN ._PUNCT
Haha_NOUN ._PUNCT
Da_ADJ iawn_ADV That_X 's_X good_VERB ._PUNCT
I_ADP would_VERB say_NOUN you_NOUN '_PUNCT re_NUM more_NOUN than_ADJ welcome_VERB to_NOUN get_NOUN any_NOUN equipment_NOUN out_ADP that_NOUN you_NOUN want_NOUN ._PUNCT
Obviously_VERB there_VERB isn't_NOUN that_NOUN much_NOUN OK_X '_PUNCT cause_NOUN everything_VERB 's_X in_X there_VERB ._PUNCT
So_PART put_NOUN the_X hoops_X back_NOUN for_CONJ me_X and_NOUN then_NOUN you_CONJ can_NOUN go_ADV and_ADJ get_ADV whatever_VERB you_NOUN want_NOUN but_NOUN obviously_NOUN there_ADP isn't_NOUN that_NOUN much_ADJ ._PUNCT
There_VERB 's_X spring_X boards_X crash_NOUN mat_PRON ._PUNCT
There_VERB 's_X a_CONJ trampet_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Helo_INTJ ._PUNCT
Helo_INTJ -_PUNCT o_ADP -_PUNCT o_ADP -_PUNCT o_PRON ._PUNCT
I_ADP don't_NOUN get_ADV what_CONJ they_VERB '_PUNCT re_NUM doing_NOUN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ww_ADP ._PUNCT
How_VERB do_PART you_NOUN know_VERB it_ADP makes_NOUN any_NOUN sounds_NOUN ?_PUNCT
Helo_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
No_NOUN it_ADP records_NOUN it_ADP and_NOUN then_NOUN they_VERB '_PUNCT ll_NOUN play_NOUN it_ADP back_NOUN later_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Helo_INTJ ._PUNCT
How_VERB old_NOUN are_VERB you_NOUN ?_PUNCT
Me_INTJ ?_PUNCT
You_ADV '_PUNCT re_NOUN fourteen_NOUN ?_PUNCT
I_ADP 'm_DET thirteen_NOUN ._PUNCT
I_ADP 'm_DET fourteen_NOUN in_ADP like_VERB twenty_NOUN two_PART days_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Oh_INTJ so_X you_X '_PUNCT re_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Don't_NOUN throw_NOUN or_ADV kick_ADJ the_X balls_X '_PUNCT cause_NOUN if_NOUN those_NOUN lights_NOUN shatter_NOUN then_NOUN this_VERB floor_NOUN can't_ADJ be_ADP used_NOUN again_NOUN ._PUNCT
Or_CONJ that_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Ti_PRON 'n_PART iawn_ADJ enwg_VERB ?_PUNCT
Oes_VERB 'na_ADV bloc_NOUN ?_PUNCT
Ella_ADV ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB mama_VERB nhw_PRON gyd_ADP yn_PART gael_VERB ei_PRON ddefnyddio_VERB ._PUNCT
Ia_INTJ sori_VERB ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB mama_VERB nhw_PRON 'n_PART dechra_VERB 'n_PART fana_ADV sori_VERB ._PUNCT
'_PUNCT Mond_PROPN y_DET petha'_NOUNUNCT sy_VERB 'n_PART fana_ADV ._PUNCT
BeBe_AUX ti_PRON '_PUNCT sio_ADV neud_VERB ?_PUNCT
jumpio_VERB ar_ADP rywbeth_NOUN ?_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT Plant_PROPN yn_PART chwarae_VERB ]_PUNCT ._PUNCT
OK_ADV yn_PART hytrach_ADV 'na_ADV jest_ADV bownsio_VERB 'r_DET peli_NOUN 'ma_ADV bebe_NOUN fysa'_VERBUNCT chi_PRON 'n_PART gallu_ADV neud_VERB i_PART neud_VERB o_ADP 'n_PART gymnasteg_NOUN ?_PUNCT
Na_PART enwg_VERB What_NOUN could_ADJ you_NOUN do_ADP with_NOUN them_NOUN ?_PUNCT
You_ADV want_NOUN to_NOUN show_VERB me_ADP something_NOUN ?_PUNCT
Ww_ADP try_VERB again_ADJ ._PUNCT
Hmm_INTJ I_ADP 'm_DET not_NOUN sure_VERB <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
Da_ADJ iawn_ADV Who_CONJ can_VERB sit_ADP on_X the_X ball_X for_X the_X longest_X without_NOUN pu_PRON -_PUNCT putting_NOUN your_ADJ feet_VERB on_X the_X floor_NOUN ?_PUNCT
OK_X let_X 's_X have_NOUN a_CONJ competition_NOUN then_NOUN ._PUNCT
Feet_NOUN off_ADP the_X floor_NOUN ._PUNCT
Go_INTJ ._PUNCT
Oh_INTJ ._PUNCT
No_NOUN hands_NOUN off_ADP the_X floor_NOUN as_ADV well_ADJ ._PUNCT
Hands_NOUN and_X feet_NOUN ._PUNCT
Oh_AUX everyone_NOUN 's_X out_VERB except_NOUN for_CONJ enwg_VERB ._PUNCT
Can_VERB we_NOUN do_ADP that_NOUN again_ADJ ?_PUNCT
OK_ADV off_ADP you_NOUN go_ADV again_ADJ ._PUNCT
Ww_PART enwg_VERB be_ADP careful_NOUN ._PUNCT
OK_X ac_CONJ eto_ADV Go_INTJ ._PUNCT
Come_VERB on_X enwb_NOUN do_PRON it_ADP properly_NOUN yeah_ADP ._PUNCT
[_PUNCT Plant_PROPN yn_PART chwarae_VERB ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Yndi_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Yndi_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Yndi_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ddigon_ADJ o_ADP sgilia'_VERBUNCT i_PRON gallu_ADV neud_VERB o_ADP '_PUNCT does_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ia_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ia_INTJ ._PUNCT
Na_INTJ ._PUNCT
Ma'_VERBUNCT nhw_PRON '_PUNCT isio_ADV neud_VERB y_DET petha'_NOUNUNCT anodd_ADJ 'ma_ADV i_ADP gyd_NOUN yn_PART enwedig_ADJ yr_DET hogia'_NOUNUNCT '_PUNCT da_ADJ chi_PRON 'n_PART gweld_VERB "_PUNCT o_ADP ga_NOUN i_ADP neud_VERB dy_DET -_PUNCT dy_DET -_PUNCT dy_DET -_PUNCT dy_DET "_PUNCT '_PUNCT da_ADJ chi_PRON 'n_PART me'l_ADJ hmm_ADV ._PUNCT
[_PUNCT Plant_PROPN yn_PART chwarae_VERB ]_PUNCT ._PUNCT
A_PART weithia'_VERBUNCT 'di_PART nhw_PRON jest_ADV ddim_PART digon_ADV da_ADJ i_PART neud_VERB y_DET petha'_NOUNUNCT 'ma_ADV na_INTJ ._PUNCT
Ond_CONJ 'di_PART nhw_PRON 'm_DET '_PUNCT isio_ADV neud_VERB y_DET petha'_NOUNUNCT hawdd_ADJ chwaith_ADV so_PART mae_AUX  _SPACE '_PUNCT isio_VERB rhywbeth_NOUN yn_ADP y_DET canol_NOUN ._PUNCT
Ma'_VERBUNCT enwg_VERB 'di_PART bod_AUX yn_PART dangos_VERB po_PRON -_PUNCT po_VERB ar_ADP troed_NOUN fo_PRON i_ADP pawb_PRON [_PUNCT -_PUNCT ]_PUNCT
'_PUNCT Di_AUX nhw_PRON 'di_PART neud_VERB yr_DET  _SPACE <_SYM aneglur_ADJ 1_NUM >_SYM ?_PUNCT
Naddo_VERB ._PUNCT
'_PUNCT Di_VERB nhw_PRON heb_ADP ._PUNCT
Naddo_VERB ._PUNCT
'_PUNCT Swn_PROPN i_ADP 'm_DET yn_PART meindio_VERB i_ADP chi_PRON ddangos_VERB i_ADP fi_PRON sut_ADV i_PART neud_VERB o_ADP actually_VERB OK_X so_ADJ <_SYM aneglur_ADJ 1_NUM >_SYM i_ADP un_NUM ochr_NOUN jest_ADV am_ADP eiliad_NOUN ._PUNCT
Y_DET gêm_NOUN  _SPACE chicken_NOUN or_ADV  _SPACE chicken_NOUN in_X a_CONJ hen_ADJ house_NOUN mama_VERB nhw_PRON 'n_PART licio_VERB Show_PROPN me_ADP later_NOUN ._PUNCT
We_VERB '_PUNCT ll_NOUN play_NOUN this_VERB game_NOUN first_NOUN yeah_ADV ?_PUNCT
Ac_CONJ '_PUNCT ista_NOUN lawr_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
2326.256_ADJ
