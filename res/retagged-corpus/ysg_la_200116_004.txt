ADVERT_X TEMPLATE_PROPN
Directorate_NOUN
Cyfarwyddiaeth_NOUN
Yr_DET Amgylchedd_NOUN a_CONJ Thai_PROPN
Service_PROPN
Gwasanaeth_NOUN
Gwasanaethau_NOUN Rheoliadol_ADJ a_CONJ Rennir_PROPN
Post_NOUN Title_PROPN
Teitl_NOUN y_DET swydd_NOUN
Swyddog_NOUN Technegol_ADJ Gwasanaethau_X Cymdogaeth_NOUN
Post_NOUN Reference_PROPN
Cyfeirnod_NOUN y_DET swydd_NOUN
D_NUM -_PUNCT SRS_ADJ -_PUNCT NS050_NOUN
Location_PROPN
Lleoliad_NOUN
Pen_NOUN -_PUNCT y_DET -_PUNCT bont_X ar_ADP Ogwr_X ,_PUNCT Caerdydd_PROPN a_CONJ Bro_NOUN Morgannwg_PROPN
Pay_NOUN Details_NOUN
Manylion_NOUN am_ADP gyflog_NOUN
Gradd_NOUN G_NOUN
Days_NOUN /_PUNCT Hours_X of_X Work_X
Diwrnodau_NOUN /_PUNCT Oriau_NOUN Gwaith_PROPN
37_NUM awr_NOUN
Permanent_VERB /_PUNCT Temporary_PROPN
Parhaol_ADJ /_PUNCT Dros_ADP Dro_NOUN
Gweithio_VERB dros_ADP dro_NOUN dros_ADP gyfnod_NOUN mamolaeth_NOUN tymor_NOUN penodol_ADJ 12_NUM mis_NOUN
Rheswm_NOUN dros_ADP gynnig_NOUN swydd_NOUN dros_ADP dro_NOUN
Brief_VERB Description_PROPN of_X Post_X
Disgrifiad_NOUN byr_ADJ o_ADP 'r_DET swydd_NOUN
Mae_AUX swydd_NOUN Swyddog_NOUN Technegol_ADJ Gwasanaethau_X Cymdogaeth_PROPN ar_ADP gael_VERB ._PUNCT
Bydd_VERB yr_DET ymgeisydd_NOUN llwyddiannus_ADJ yn_PART ymgymryd_VERB ag_ADP ystod_NOUN lawn_ADJ o_ADP ddyletswyddau_NOUN gorfodi_VERB gan_CONJ gynnwys_VERB ymchwilio_VERB i_ADP Niwsans_NOUN Statudol_ADJ ,_PUNCT Gorfodaeth_NOUN Rheoli_PROPN Plâu_PROPN ,_PUNCT Tipio_X Anghyfreithlon_PROPN a_CONJ chwynion_VERB Iechyd_NOUN y_DET Cyhoedd_NOUN ._PUNCT
DBS_NOUN Check_VERB Required_PROPN :_PUNCT
Oes_VERB angen_NOUN gwiriad_NOUN y_DET Gwasanaeth_NOUN Datgelu_PROPN a_CONJ Gwahardd_PROPN ?_PUNCT
None_NOUN
Dim_NOUN
For_NOUN Further_X Information_X Contact_X :_PUNCT
Am_ADP ragor_NOUN o_ADP wybodaeth_NOUN cysylltwch_VERB â_ADP :_PUNCT
Application_NOUN Pack_X Available_X By_PRON :_PUNCT
Cewch_VERB becyn_NOUN cais_NOUN oddi_ADP wrth_ADP :_PUNCT
SYLWER_NOUN MAI_PROPN HYSBYSEBU_NUM 'N_DET GRYNO_PROPN YW_PROPN POLISI_ADP 'R_DET CYNGOR_NOUN ._PUNCT
Yr_DET unig_ADJ wybodaeth_NOUN a_CONJ fydd_VERB yn_ADP yr_DET hysbyseb_NOUN yw_VERB teitl_NOUN y_DET swydd_NOUN ,_PUNCT y_DET cyfeirnod_NOUN a_CONJ 'r_DET cyflog_NOUN ._PUNCT
Mae_AUX 'n_PART rhaid_VERB cael_VERB cymeradwyaeth_NOUN i_ADP hysbysebu_VERB 'n_PART allanol_ADJ neu_CONJ i_PART ddangos_VERB hysbysebion_NOUN llawn_ADJ gan_ADP bennaeth_NOUN AD_PROPN wrth_ADP gyflwyno_VERB 'r_DET Ffurflen_NOUN Rheoli_VERB Swydd_NOUN Wag_PROPN i_ADP 'w_PRON chymeradwyo_VERB ._PUNCT
Ni_PART ddefnyddir_VERB y_DET disgrifiad_NOUN cryno_VERB o_ADP 'r_DET swydd_NOUN ond_CONJ ar_ADP y_DET fewnrwyd_VERB staff_NOUN a_CONJ 'r_DET rhyngrwyd_NOUN ac_CONJ ni_PART ddylai_VERB fod_VERB yn_PART hirach_ADJ na_CONJ 5_NUM /_SYM 6_NUM llinell_NOUN ._PUNCT
Caiff_VERB unrhyw_DET wybodaeth_NOUN y_PART tybir_VERB ei_PRON bod_VERB yn_PART fwy_ADJ na_CONJ hyn_PRON ei_DET dileu_VERB a_CONJ chaiff_VERB hysbyseb_NOUN fyrrach_ADJ ei_PRON rhoi_VERB yn_ADP y_DET drafft_NOUN a_CONJ 'r_DET bwletin_NOUN swyddi_NOUN gwag_ADJ terfynol_ADJ ._PUNCT
Dylai_VERB 'r_DET disgrifiad_NOUN hwn_DET ddisgrifio_VERB 'r_DET gwaith_NOUN a_CONJ wneir_VERB ac_CONJ nid_PART cymwysterau_NOUN na_CONJ rhinweddau_NOUN personol_ADJ yr_DET unigolyn_NOUN ._PUNCT
Ceir_VERB hyn_PRON o_ADP 'r_DET Disgrifiad_NOUN Swydd_NOUN a_CONJ 'r_DET Fanyleb_NOUN Person_PROPN yn_ADP yr_DET hysbyseb_NOUN ._PUNCT
