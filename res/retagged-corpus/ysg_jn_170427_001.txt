Crynodeb_NOUN Pobol_ADJ y_DET Cwm_NOUN W_ADP 18_NUM
Mae_AUX DJ_PROPN ac_CONJ Anita_PROPN yn_PART mynd_VERB i_ADP dŷ_NOUN Linda_PROPN er_ADP mwyn_NOUN ceisio_ADV gwneud_VERB newidiadau_NOUN i_ADP 'r_DET tŷ_NOUN fel_ADP syrpreis_NOUN ._PUNCT
Ond_CONJ ar_ADP ôl_NOUN cyrraedd_VERB ,_PUNCT maen_AUX nhw_PRON 'n_PART cael_ADV gwybod_VERB gan_ADP gymydog_NOUN bod_AUX Linda_PROPN wedi_PART gorfod_ADV gadael_VERB gan_ADP nad_PART yw_AUX hi_PRON wedi_PART gallu_VERB talu_VERB 'r_DET rhent_NOUN ._PUNCT
Wrth_PART ystyried_VERB yr_DET arian_NOUN oedd_VERB gan_ADP Linda_PROPN i_PART fynd_VERB ar_ADP wyliau_NOUN i_ADP America_PROPN ,_PUNCT mae_AUX DJ_PROPN yn_PART dechrau_VERB amau_ADP mai_PART ei_DET fam_NOUN oedd_AUX wedi_PART dwyn_VERB arian_NOUN o_ADP APD_ADJ ._PUNCT
Caiff_VERB Gaynor_PROPN gadarnhad_NOUN gan_ADP Hywel_PROPN mai_PART Chester_PROPN sydd_AUX wedi_PART bod_AUX yn_PART gwerthu_VERB traethodau_NOUN dros_ADP y_DET we_NOUN ac_CONJ mae_VERB Ffion_PROPN yn_PART benderfynol_ADJ bod_VERB rhaid_VERB iddo_ADP gael_VERB ei_DET wahardd_NOUN o_ADP 'r_DET ysgol_NOUN ._PUNCT
Ond_CONJ daw_VERB Hannah_PROPN i_PART achub_VERB ei_DET gam_NOUN ._PUNCT
Mae_VERB Kelly_PROPN yn_PART cwrdd_NOUN â_ADP Morgan_PROPN mewn_ADP bar_NOUN ac_CONJ mae_AUX 'r_DET ddau_NUM yn_PART trafod_VERB eu_DET hanes_NOUN a_CONJ 'u_PRON cysylltiadau_NOUN gyda_ADP grwpiau_NOUN protest_NOUN ._PUNCT
Mae_AUX Morgan_PROPN yn_PART holi_VERB am_ADP Gethin_PROPN ond_CONJ nid_PART oes_VERB gan_ADP Kelly_X syniad_NOUN o_ADP orffennol_ADJ tywyll_ADV Moc_PROPN ._PUNCT
Mae_AUX Sioned_PROPN yn_PART cymryd_VERB prawf_NOUN -_PUNCT a_CONJ fydd_VERB hyn_PRON yn_PART ddigon_ADJ iddi_ADP rwydo_VERB Garry_PROPN ?_PUNCT
Pobol_ADJ y_DET Cwm_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 8.00_NUM ,_PUNCT S4C_PROPN ._PUNCT
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Pob_DET prynhawn_NOUN Sul_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Ar_ADP alw_VERB :_PUNCT s4c.cymru_NOUN ;_PUNCT BBC_X iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cynhyrchiad_NOUN BBC_NOUN Cymru_PROPN
