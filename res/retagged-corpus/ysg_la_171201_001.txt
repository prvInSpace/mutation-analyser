Prynhawn_NOUN da_ADJ ,_PUNCT
Ar_ADP drothwy_NOUN 'r_DET penwythnos_NOUN ,_PUNCT hoffwn_VERB eich_DET hatgoffa_NOUN fod_VERB gan_ADP eich_DET plentyn_NOUN ffug_NOUN arholiadau_NOUN mis_NOUN yma_ADV ac_CONJ y_PART dylent_PRON fod_AUX yn_PART cwblhau_VERB nifer_NOUN o_ADP sesiynau_NOUN adolygu_VERB 'n_PART feunyddiol_ADJ ._PUNCT
Atodaf_VERB 2_NUM Amserlen_PROPN Adolygu_PROPN gallwch_VERB ddefnyddio_VERB gyda_ADP 'ch_PRON plentyn_NOUN ._PUNCT
Yn_PART ogystal_ADJ ,_PUNCT oddi_ADP tano_ADP mae_VERB 2_NUM gyswllt_NOUN ar_ADP gyfer_NOUN gwefannau_NOUN fydd_AUX yn_PART gymorth_NOUN i_ADP chi_PRON gyda_ADP 'ch_PRON plentyn_NOUN yn_PART ystod_NOUN y_DET cyfnod_NOUN anodd_ADJ yma_ADV ._PUNCT
