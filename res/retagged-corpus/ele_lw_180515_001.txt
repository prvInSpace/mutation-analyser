<_SYM img_VERB /_PUNCT >_SYM
Digwyddiadau_NOUN enw_NOUN
Nos_NOUN Wener_NOUN yma_ADV !_PUNCT !_PUNCT
Dewch_VERB i_ADP joio_NOUN gyda_ADP ni_PRON yn_PART ein_DET noson_NOUN meic_NOUN agored_ADJ !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Ap_VERB Cwtsh_NOUN
-_PUNCT Ap_PROPN sy_AUX 'n_PART helpu_VERB chi_PRON i_PART ddod_VERB i_PART adnabod_VERB eich_DET hun_PRON drwy_ADP sesiynau_NOUN myfyrio_VERB gynhenid_ADV Gymreig_ADJ !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Wedi_PART 'i_PRON gyllido_VERB gan_ADP Grant_NOUN Cymraeg_PROPN 2050_NUM Llywodraeth_NOUN Cymru_PROPN ,_PUNCT a_CONJ 'i_PRON greu_VERB gan_ADP Fenter_PROPN Iaith_NOUN Abertawe_PROPN ,_PUNCT mae_AUX ap_NOUN '_PUNCT Cwtsh_NOUN '_PUNCT yn_PART arwain_VERB defnyddwyr_NOUN drwy_ADP dair_NUM sesiwn_NOUN myfyrio_VERB (_PUNCT bore_NOUN ,_PUNCT yn_PART ystod_NOUN y_DET dydd_NOUN a_CONJ chyda_VERB 'r_DET hwyr_NOUN )_PUNCT yn_ADP Gymraeg_PROPN ._PUNCT
Mae_AUX Cwtsh_NOUN hefyd_ADV yn_PART cynnwys_VERB cyfeiriadur_NOUN cenedlaethol_ADJ ar_ADP gyfer_NOUN sesiynau_NOUN myfyrdod_NOUN a_CONJ dosbarthiadau_NOUN ioga_NOUN yn_ADP yr_DET iaith_NOUN Gymraeg_PROPN o_ADP 'r_DET enw_NOUN 'r_DET Llyfr_NOUN Lles_NOUN ._PUNCT
Gallwch_VERB weld_VERB y_DET Llyfr_NOUN Lles_NOUN ar_ADP
cyfeiriad_NOUN
a_PART chofiwch_VERB lawrlwytho_VERB 'r_DET ap_NOUN !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Mae_AUX 'r_DET fenter_NOUN yn_PART edrych_VERB am_ADP eich_DET adborth_NOUN ar_ADP y_DET gwaith_NOUN yr_PART ydym_AUX yn_PART ei_PRON wneud_VERB ._PUNCT
Byddwn_VERB yn_PART ddiolchgar_ADJ iawn_ADV pe_CONJ byddech_AUX yn_PART gallu_ADV llenwi_VERB 'r_DET holiadur_NOUN
yma_ADV
._PUNCT
Diolch_NOUN o_ADP galon_NOUN am_ADP eich_DET cefnogaeth_NOUN ._PUNCT
enw_NOUN
Bwyd_VERB a_CONJ diod_NOUN !_PUNCT
Coffi_NOUN Coaltown_PROPN ,_PUNCT
Tê_PRON
Morgan_VERB 's_X Brew_PROPN a_CONJ
Sudd_NOUN Afal_PROPN gan_ADP gwmni_NOUN Aber_NOUN Valley_PROPN
yn_PART ogystal_ADJ a_CONJ bisgedi_VERB ,_PUNCT siocled_NOUN a_CONJ teisienod_NOUN gan_ADP gwmni_NOUN Blas_PROPN ar_ADP fwyd_NOUN ._PUNCT
Dewch_VERB i_ADP flasu_VERB bwyd_NOUN a_CONJ diod_NOUN Cymreig_ADJ ._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Llestri_NOUN a_CONJ nwyddau_NOUN hyfryd_ADJ i_ADP 'r_DET cartref_NOUN ar_ADP gael_VERB nawr_ADV ._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Cardiau_NOUN Sul_PROPN y_DET Tadau_PROPN ac_CONJ anrhegion_NOUN ar_ADP gael_VERB nawr_ADV yn_ADP y_DET siop_NOUN !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Printiau_VERB o_ADP drên_NOUN y_DET Mwmbwls_NOUN -_PUNCT dim_PRON ond_ADP £_SYM 4.99_NUM !_PUNCT
Llyfrau_NOUN a_CONJ theganau_NOUN i_ADP blant_NOUN -_PUNCT dewch_VERB i_ADP gael_VERB golwg_NOUN !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Llyfrau_NOUN Mis_NOUN Mai_PROPN !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Llyfrau_NOUN y_DET mis_NOUN ar_ADP gyfer_NOUN Mai_PROPN ._PUNCT ._PUNCT ._PUNCT '_PUNCT Cicio_VERB 'r_DET Bar_NOUN '_PUNCT gan_ADP Sioned_NOUN Wiliam_PROPN a_CONJ '_PUNCT The_X Last_X Big_X One_X '_PUNCT ,_PUNCT llyfr_NOUN i_ADP blant_NOUN gan_ADP Dan_PROPN Anthony_PROPN ._PUNCT
Ar_ADP gael_VERB nawr_ADV o_ADP 'r_DET siop_NOUN ._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Academi_NOUN Radio_NOUN 1_NUM
Bydd_VERB gweithdy_NOUN cyfansoddi_VERB cân_NOUN gyda_ADP 'r_DET band_NOUN Adwaith_ADJ ar_ADP brynhawn_NOUN Ddydd_NOUN Llun_PROPN 21_NUM ain_NOUN (_PUNCT 3_NUM -_SYM 4_NUM pm_X )_PUNCT yn_PART ogystal_ADJ â_ADP phanel_NOUN yn_PART trafod_VERB cyfryngau_NOUN Cymru_PROPN a_CONJ 'r_DET cyfloeodd_NOUN sydd_VERB ar_ADP gael_VERB i_ADP bobl_NOUN ifanc_ADJ i_PART weithio_VERB ynddi_ADP ar_ADP Ddydd_NOUN Mawrth_PROPN 22_NUM ain_NOUN (_PUNCT 4_NUM -_SYM 5_NUM pm_NOUN )_PUNCT ._PUNCT
Ar_ADP y_DET panel_NOUN mi_PART fydd_VERB y_DET cyfarwyddwr_NOUN Euros_PROPN Lyn_NOUN sydd_AUX wedi_PART gweithio_VERB ar_ADP rhaglenni_NOUN megis_ADP Doctor_NOUN Who_PROPN ,_PUNCT Sherlock_PROPN ,_PUNCT Black_PROPN Mirror_PROPN a_CONJ Happy_PROPN Valley_PROPN ,_PUNCT Elen_PROPN Rhys_PROPN ,_PUNCT Comisiynydd_PROPN Adloniant_PROPN a_CONJ Cherddoriaeth_NOUN S4C_PROPN a_CONJ Cai_PROPN Morgan_PROPN ,_PUNCT cynhyrchydd_NOUN a_CONJ chyfarwyddwr_NOUN y_DET sianel_NOUN digidol_ADJ Hansh_PROPN ._PUNCT
Bydd_VERB cyfle_NOUN ar_ADP ddiwedd_NOUN y_DET sesiwn_NOUN i_ADP 'r_DET gynulleidfa_NOUN i_PART ofyn_VERB cwestiynau_NOUN ac_CONJ i_ADP gwrdd_VERB a_CONJ 'r_DET panel_NOUN ._PUNCT
Yn_PART ogystal_ADJ ,_PUNCT mi_PART fydd_VERB cwmniau_NOUN lleol_ADJ yn_ADP yr_DET Academi_NOUN bob_DET dydd_NOUN yn_PART cynnig_ADJ cyngor_NOUN am_ADP sut_ADV i_PART ddechrau_VERB gyrfa_NOUN o_ADP fewn_ADP y_DET diwydiant_NOUN a_CONJ rhai_PRON yn_PART cynnig_ADJ profiad_NOUN gwaith_NOUN yn_PART ogystal_ADJ â_ADP chyfle_NOUN i_ADP ennill_VERB tocynnau_NOUN i_ADP The_X Biggest_X Weekend_X ._PUNCT
Ar_ADP nos_NOUN Iau_ADJ ,_PUNCT Mai_PART 24_NUM ,_PUNCT o_ADP 7_NUM pm_NUM ,_PUNCT bydd_VERB gig_NOUN Cymraeg_PROPN gyda_ADP bandiau_NOUN Gorwelion_NOUN a_CONJ Huw_PROPN Stephens_PROPN yn_PART cyflwyno_VERB yn_PART Fyw_PROPN ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN ._PUNCT
Tocynnau_VERB ar_ADP gael_VERB yma_ADV -_PUNCT
cyfeiriad_NOUN
Mae_AUX 'r_DET Academi_NOUN yn_PART gyfle_NOUN gwych_ADJ i_ADP bobl_NOUN ifanc_ADJ ddysgu_VERB mwy_PRON am_ADP y_DET diwydiannau_NOUN creadigol_ADJ a_CONJ chwrdd_NOUN â_ADP phobl_NOUN profiadol_ADJ ._PUNCT
Mae_VERB tocynnau_NOUN i_ADP 'r_DET Academi_NOUN am_ADP ddim_PRON -_PUNCT os_CONJ oes_VERB gan_ADP rhywun_NOUN ddiddordeb_NOUN i_ADP fod_VERB yn_PART rhan_NOUN ohoni_ADP mae_VERB 'r_DET wybodaeth_NOUN i_ADP gyd_NOUN ar_ADP
cyfeiriad_NOUN
._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Mae_AUX 'r_DET Penwythnos_NOUN Mwyaf_PROPN yn_PART dod_VERB i_ADP Abertawe_PROPN ac_CONJ bydd_VERB BBC_PROPN Radio_VERB Cymru_PROPN yn_ADP ei_DET chanol_ADJ hi_PRON !_PUNCT
Fe_PART fydd_VERB Ifan_PROPN Davis_PROPN a_CONJ Lisa_PROPN Gwilym_PROPN yn_PART crwydro_VERB 'r_DET maes_NOUN yn_PART dod_VERB a_CONJ holl_DET gyffro_VERB yr_DET wyl_NOUN -_PUNCT yn_PART cael_VERB sgwrs_NOUN gyda_ADP 'r_DET bandiau_NOUN Cymraeg_PROPN yn_PART mwynhau_VERB llwyfan_NOUN BBC_PROPN introducing_VERB fel_ADP Band_PROPN pres_VERB Llareggub_PROPN ,_PUNCT Serol_ADJ Serol_ADJ ,_PUNCT Mellt_PROPN a_CONJ Chroma_PROPN ._PUNCT
Cyn_CONJ hynny_PRON fe_PART fydd_AUX Huw_PROPN Stephens_PROPN yn_PART dod_VERB yn_PART fyw_VERB o_ADP Hwb_NOUN BBC_NOUN Cymru_PROPN yn_ADP Unit_PROPN 19_NUM yn_ADP Abertawe_PROPN ,_PUNCT ac_CONJ yn_PART cael_VERB cwmni_NOUN bandiau_NOUN newydd_ADJ Gorwelion_PROPN ar_ADP y_DET noson_NOUN ,_PUNCT ychydig_ADV o_ADP fwyd_NOUN y_DET stryd_NOUN ,_PUNCT clywed_VERB gan_ADP myfyrwyr_NOUN Coleg_NOUN Gwyr_PROPN ,_PUNCT a_CONJ comediwr_NOUN newydd_ADJ o_ADP 'r_DET BBC_NOUN Sesh_NOUN Josh_PROPN Elton_PROPN ._PUNCT
Cliciwch_VERB yma_ADV
am_ADP fwy_PRON o_ADP wybodaeth_NOUN ._PUNCT
Cliciwch_VERB yma_ADV
i_PART archebu_VERB eich_DET tocynnau_NOUN !_PUNCT
Cyfle_NOUN cyffroes_VERB i_ADP fod_VERB yn_PART rhan_NOUN o_ADP rhaglen_NOUN deledu_NOUN newydd_ADJ i_ADP 'r_DET BBC_NOUN !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Estron_NOUN -_PUNCT sioe_NOUN newydd_ADJ Theatr_NOUN Genedlaethol_ADJ Cymru_PROPN
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
