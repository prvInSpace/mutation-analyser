Ydych_AUX chi_PRON 'n_PART chwilio_VERB am_ADP wybodaeth_NOUN i_PART gefnogi_VERB disgyblion_NOUN ag_ADP anableddau_NOUN neu_CONJ anghenion_NOUN ychwanegol_ADJ a_CONJ 'u_PRON teuluoedd_NOUN ?_PUNCT
Gall_VERB Y_DET Mynegai_PROPN gynnal_VERB bore_NOUN coffi_NOUN yn_ADP eich_DET ysgol_NOUN i_PART gefnogi_VERB rhieni_NOUN /_PUNCT gofalwyr_NOUN gyda_ADP gwybodaeth_NOUN am_ADP wasanaeth_NOUN a_CONJ chymorth_NOUN gydag_ADP anghenion_NOUN ychwanegol_ADJ ._PUNCT
(_PUNCT Gweler_VERB y_DET poster_NOUN drafft_NOUN sydd_AUX wedi_PART 'i_PRON atodi_VERB )_PUNCT
Y_DET Mynegai_PROPN yw_VERB cofrestr_NOUN wirfoddol_ADJ Caerdydd_PROPN a_CONJ Bro_NOUN Morgannwg_PROPN o_ADP blant_NOUN a_CONJ phobl_NOUN ifanc_ADJ sydd_VERB ag_ADP anableddau_NOUN neu_CONJ anghenion_NOUN ychwanegol_ADJ ._PUNCT
Gall_VERB teuluoedd_NOUN a_CONJ gweithwyr_NOUN proffesiynol_ADJ gofrestru_VERB i_ADP 'r_DET Mynegai_PROPN i_PRON :_PUNCT
•_NUM Gael_VERB gwybodaeth_NOUN am_ADP wasanaethau_VERB a_CONJ gweithgareddau_NOUN newydd_ADJ
•_PART Cael_VERB cylchlythyron_NOUN chwarterol_ADJ a_CONJ phost_NOUN penodol_ADJ
•_NUM Cael_VERB y_DET cyfle_NOUN i_ADP gyfrannu_VERB erthyglau_NOUN ,_PUNCT newyddion_NOUN a_CONJ digwyddiadau_NOUN i_ADP 'w_PRON rhannu_VERB ag_ADP eraill_ADJ ar_ADP Y_DET Mynegai_PROPN
•_NUM Helpu_NOUN a_CONJ dylanwadu_VERB ar_ADP y_DET math_NOUN o_ADP ddarpariaeth_NOUN sy_AUX 'n_PART cael_VERB ei_DET chynnig_NOUN a_CONJ 'i_PRON datblygu_VERB
•_ADJ Defnyddio_VERB 'r_DET Mynegai_PROPN fel_ADP teclyn_NOUN cynllunio_VERB i_ADP ddysgu_VERB mwy_PRON am_ADP anghenion_NOUN plant_NOUN yn_ADP eich_DET ardal_NOUN chi_PRON -_PUNCT Cymerwch_VERB olwg_NOUN ar_ADP Adroddiad_NOUN Blynyddol_ADJ 2017_NUM /_SYM 18_NUM y_DET Mynegai_PROPN ar_ADP -_PUNCT lein_NOUN
Os_CONJ nad_PART ydych_AUX eisoes_ADV wedi_PART cofrestru_VERB ar_ADP gyfer_NOUN Y_DET Mynegai_PROPN ac_CONJ yr_DET hoffech_NOUN gael_VERB eich_PRON ychwanegu_VERB at_ADP ein_DET rhestr_NOUN bostio_VERB ,_PUNCT rhowch_VERB wybod_VERB i_ADP ni_PRON ._PUNCT
Cofiwch_VERB :_PUNCT nid_PART dim_PRON ond_ADP "_PUNCT Boreau_PROPN Coffi_PROPN "_PUNCT yw_VERB 'r_DET sesiynau_NOUN hyn_DET -_PUNCT rydym_AUX yn_PART croesawu_VERB unrhyw_DET awgrymiadau_NOUN ar_ADP ffyrdd_NOUN o_ADP gysylltu_VERB â_ADP theuluoedd_NOUN yn_ADP eich_DET ysgol_NOUN ._PUNCT
Rhowch_VERB wybod_VERB i_ADP ni_PRON dros_ADP e_PRON -_PUNCT bost_NOUN os_CONJ hoffech_NOUN gynnal_ADJ sesiwn_NOUN wybodaeth_NOUN Y_DET Mynegai_PROPN yn_ADP eich_DET ysgol_NOUN chi_PRON !_PUNCT
cyfeiriad_NOUN -_PUNCT bost_NOUN
Edrychwn_VERB ymlaen_ADV at_PART glywed_VERB gennych_ADP ._PUNCT
