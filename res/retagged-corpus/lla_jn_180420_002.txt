W'thnos_NOUN yma_ADV artist_NOUN hip_NOUN -_PUNCT hop_NOUN mwya'_ADJUNCT poblogaidd_ADJ Cymru_PROPN sy_AUX 'n_PART derbyn_VERB her_NOUN enw_NOUN [_PUNCT anadlu_VERB ]_PUNCT Mae_AUX o_PRON 'n_PART gyn-_VERB aelod_NOUN o_ADP enwbg_NOUN a_CONJ __NOUN [_PUNCT anadlu_VERB ]_PUNCT A_CONJ llynedd_ADV fe_PRON 'na_ADV 'th_NOUN o_ADP berfformio_VERB hefo_ADP __NOUN __NOUN __NOUN yn_PART gig_NOUN fwya_ADJ 'r_DET flwyddyn_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Dyma_ADV enwg_VERB neu_CONJ enwg_VERB ._PUNCT
Mae_VERB 'r_DET byd_NOUN hip_NOUN -_PUNCT hop_NOUN yn_PART un_NUM cystadleuol_ADJ ac_CONJ ar_ADP adege_NOUN medru_VERB bod_VERB yn_PART ymosodol_ADJ ond_CONJ mae_AUX enwg_VERB yn_PART trio_ADV byw_VERB ei_DET fywyd_NOUN mewn_ADP ffordd_NOUN bositif_ADJ ac_CONJ egwyddorol_ADJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN '_PUNCT hip_NOUN -_PUNCT hop_NOUN '_PUNCT yn_PART chwarae_VERB yn_ADP yr_DET cefndir_NOUN ]_PUNCT ._PUNCT
I_ADP fi_PRON [_PUNCT saib_NOUN ]_PUNCT mae_VERB crefydd_NOUN yn_PART rhywbeth_NOUN sy_VERB 'n_PART [_PUNCT saib_NOUN ]_PUNCT positif_NOUN a_CONJ ma_VERB 'n_PART rhywbeth_NOUN dw_AUX i_PRON 'n_PART rhoid_VERB i_ADP mewn_ADP i_ADP bob_DET dim_DET mewn_ADP bywyd_NOUN fi_PRON [_PUNCT =_SYM ]_PUNCT Dyna_DET [_PUNCT /=_PROPN ]_PUNCT dyna_DET bebe_NOUN '_PUNCT dy_DET o_ADP jest_NOUN yn_PART neges_NOUN yna_DET o_ADP fod_VERB yn_PART [_PUNCT -_PUNCT ]_PUNCT [_PUNCT =_SYM ]_PUNCT O_ADP [_PUNCT /=_PROPN ]_PUNCT o_ADP fyw_VERB ._PUNCT
Pwysig_NOUN byw_VERB yn'dy_NOUN ?_PUNCT
[_PUNCT Cerddoriaeth_NOUN '_PUNCT hip_NOUN -_PUNCT hop_NOUN '_PUNCT yn_PART chwarae_VERB ]_PUNCT ._PUNCT
O_ADP dyna_ADV ni_PART te_NOUN enwg_VERB ._PUNCT
Dw_AUX i_PRON 'di_PART cael_VERB dod_VERB i_ADP sefydliad_NOUN o_ADP 'r_DET diwedd_NOUN ._PUNCT
Diolch_NOUN o_ADP galon_NOUN am_ADP y_DET croeso_NOUN ._PUNCT
ymm_ADV rho_VERB '_PUNCT ni_PRON ar_ADP y_DET map_NOUN ._PUNCT
Lle_ADV yn_PART union_ADJ ydyn_VERB ni_PRON yng_ADP Nghymru_PROPN ?_PUNCT
'_PUNCT dan_ADP ni_PRON yn_PART meca_ADJ hip_NOUN -_PUNCT hop_NOUN gogledd_NOUN Cymru_PROPN fan_NOUN hyn_DET sef_ADV lleoliad_NOUN a_CONJ dyma_DET lle_NOUN mama_NOUN rap_AUX yn_PART bodoli_VERB ac_CONJ yn_PART dod_VERB o_ADP [_PUNCT =_SYM ]_PUNCT Hwn_PRON '_PUNCT dy_DET 'r_DET [_NOUN /=_PROPN ]_PUNCT hwn_DET '_PUNCT dy_DET 'r_DET meca_NOUN lleoliad_NOUN [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Felly_CONJ enwg_VERB yn_PART cael_VERB ei_DET wahodd_VERB i_ADP dderbyn_VERB her_NOUN enw_NOUN ._PUNCT
Sut_ADV wyt_AUX ti_PRON 'n_PART ymateb_VERB i_ADP 'r_DET sialens_NOUN ?_PUNCT
BeBe_AUX ti_PRON 'n_PART feddwl_VERB o_ADP 'r_DET syniad_NOUN ?_PUNCT
ym_ADP ma_PRON 'n_PART syniad_NOUN diddorol_ADJ ydy_VERB ._PUNCT
Ti_PRON '_PUNCT bod_VERB tra_CONJ bod_VERB fi_PRON 'di_PART o_ADP hyd_NOUN yn_PART lico_NOUN neud_VERB pethau_NOUN 'n_PART wahanol_ADJ ac_CONJ ym_ADP mae_VERB eisiau_VERB clywed_VERB am_ADP yr_DET her_NOUN yma_ADV ac_CONJ yn_PART ni_PRON 'n_PART  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ac_CONJ roedd_VERB gen_ADP i_PRON diddordeb_NOUN yn_PART syth_ADJ ._PUNCT
A_PART wnes_VERB i_PRON bach_ADJ o_ADP ymchwil_NOUN i_ADP mewn_ADP i_ADP emynau_NOUN a_CONJ be_AUX so_AUX ni_PRON 'n_PART gallu_VERB trio_VERB remicso_VERB mewn_ADP ffordd_NOUN hip_ADJ -_PUNCT hop_NOUN ac_CONJ  _SPACE [_PUNCT saib_NOUN ]_PUNCT [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ie_INTJ '_PUNCT nes_CONJ i_PART pigo_VERB cwpwl_ADP o_ADP ganeuon_NOUN o_ADP 'n_PART i_PRON efo_ADP lot_PRON o_ADP diddordeb_NOUN gw'ithio_VERB ar_ADP so_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ie_INTJ dw_AUX i_PRON 'n_PART excited_VERB ._PUNCT
Excited_NOUN <_SYM /_PUNCT en_PRON >_SYM iawn_INTJ ._PUNCT
Achos_CONJ i_ADP bawb_PRON adre_ADJ dw_VERB i_PRON 'n_PART siŵr_ADJ bobo_AUX nhw_PRON yn_PART rhyddeddu_VERB hip_ADP -_PUNCT hop_NOUN ag_ADP emynau_NOUN ._PUNCT
'_PUNCT Dy_DET nhw_PRON ddim_PART yn_PART mynd_VERB law_NOUN a_CONJ llaw_NOUN nad_PART ydyn_VERB ?_PUNCT
Wel_INTJ na_INTJ ._PUNCT
Ond_CONJ eto_ADV dyna_PRON be_PRON sy_VERB 'n_PART cŵl_ADJ am_ADP hip_NOUN -_PUNCT hop_NOUN 'ma_ADV ._PUNCT
Ti_PRON '_PUNCT mo_PRON '_PUNCT unwaith_ADV ti_PRON 'n_PART [_PUNCT saib_NOUN ]_PUNCT  _SPACE defnyddio_VERB hip_NOUN -_PUNCT hop_NOUN a_CONJ meddwl_VERB amdano_ADP fo_PRON yn_PART greadigol_ADJ ti_PRON 'n_PART gallu_ADV clymu_VERB fo_PRON efo_ADP unrhywbeth_NOUN really_ADV ._PUNCT
ymm_ADP felly_ADV mae_VERB hip_NOUN -_PUNCT hop_NOUN   _SPACE [_PUNCT -_PUNCT ]_PUNCT [_PUNCT =_SYM ]_PUNCT Mae_VERB mama_NOUN 'na_ADV [_PUNCT /=_PROPN ]_PUNCT mae_VERB o_PRON 'n_PART cerbyd_NOUN da_ADJ [_PUNCT =_SYM ]_PUNCT i_ADP i_PRON [_PUNCT /=_PROPN ]_PUNCT  _SPACE i_ADP ffitio_VERB mewn_ADP efo_ADP pethau_NOUN gwahanol_ADJ efo_ADP <_SYM fr_VERB >_SYM genres_VERB <_SYM /_PUNCT fr_VERB >_SYM gwahanol_ADJ ._PUNCT
ymm_ADP felly_ADV dw_VERB i_PRON me'l_ADJ unwaith_ADV dw_AUX i_PRON 'n_PART neud_VERB y_DET remix_NOUN bydd_AUX pobol_ADJ yn_PART dallt_ADV okay_VERB ti_PRON yn_PART gallu_ADV cymysgu_VERB hip_NOUN -_PUNCT hop_NOUN efo_ADP emynau_NOUN efallai_ADV ._PUNCT
So_NOUN hwn_PRON '_PUNCT dy_DET 'r_DET plan_NOUN anyway_NOUN [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN '_PUNCT hip_NOUN -_PUNCT hop_NOUN '_PUNCT yn_PART chwarae_VERB ]_PUNCT ._PUNCT
Ac_CONJ bebe_NOUN sy_VERB 'n_PART diddorol_ADJ iawn_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Dyma_ADV be_AUX ti_PRON 'n_PART neud_VERB fel_ADP enwg_VERB pan_CONJ ti_PRON 'n_PART chwarae_VERB o_ADP gwmpas_NOUN efo_ADP sampl_NOUN ydy_VERB ail-_NOUN fampio_VERB nhw_PRON  _SPACE mwyn_NOUN creu_VERB rhywbeth_NOUN sydd_VERB o_ADP ddiddordeb_NOUN i_ADP ti_PRON ond_ADP ti_PRON '_PUNCT mod_ADV defnyddio_VERB emyn_NOUN fel_ADP sample_NOUN ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Ti_PRON 'di_PART neud_VERB hynny_PRON o_ADP blaen_NOUN ?_PUNCT
1616.384_NOUN
