'_PUNCT Ŵan_NOUN '_PUNCT ta_CONJ enwb_VERB o_ADP 'n_PART i_PRON 'n_PART deud_VERB fod_VERB maint_NOUN y_DET mwyafrif_NOUN yn_PART syndod_NOUN ond_CONJ faint_ADV o_ADP syndod_NOUN ?_PUNCT
A_CONJ bebe_NOUN mae_VERB hyn_PRON i_ADP gyd_NOUN yn_PART mynd_VERB i_PART ddeud_VERB am_ADP Iwerddon_PROPN erbyn_ADP hyn_PRON ?_PUNCT
O_ADP syndod_NOUN anhygoel_ADJ ._PUNCT
Mi_PART o'dd_NOUN pawb_PRON yn_PART disgwyl_VERB '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB y_PART bydde_VERB y_DET refferendwm_NOUN yn_PART cael_VERB ei_PRON basio_VERB a_CONJ bydde_AUX pobl_NOUN yn_PART pleidleisio_VERB "_PUNCT ie_INTJ "_PUNCT ._PUNCT
Ond_CONJ mi_PRON o'dd_NOUN 'na_ADV ganran_NOUN eitha'_ADJUNCT sylweddol_ADJ o_ADP bobl_NOUN hyd_ADP at_ADP y_DET bleidlais_NOUN yn_PART deud_VERB bod_VERB nhw_PRON heb_ADP benderfynu_VERB ._PUNCT
A_CONJ mi_PRON o'dd_NOUN 'na_ADV rhywfaint_NOUN o_ADP ddisgw'l_VERB y_PART bydden_VERB nhw_PRON hwyrach_ADP [_PUNCT -_PUNCT ]_PUNCT a_CONJ bod_AUX nhw_PRON wedi_PART cadw_VERB 'n_PART dawel_VERB am_ADP eu_DET bod_VERB nhw_PRON am_ADP bleidleisio_VERB "_PUNCT na_INTJ "_PUNCT a_CONJ ddim_ADV isio_ADV deud_VERB ._PUNCT
Ond_CONJ yn_ADP y_DET diwedd_NOUN mi_PRON 'na_ADV 'th_NOUN y_DET rhan_NOUN fwyaf_ADJ o_ADP bobl_NOUN o'dd_PUNCT wedi_PART deud_VERB bod_VERB nhw_PRON heb_ADP benderfynu_VERB [_PUNCT -_PUNCT ]_PUNCT '_PUNCT na'thon_VERB nhw_PRON bleidleisio_VERB "_PUNCT ie_INTJ "_PUNCT ._PUNCT
A_CONJ '_PUNCT dw_VERB i_PRON [_PUNCT -_PUNCT ]_PUNCT mi_PRON oedd_AUX neb_PRON yn_PART disgw'l_VERB y_DET fath_NOUN canlyniad_NOUN ._PUNCT
Ai_PART awgrymu_VERB mae_VERB hynna_PRON bod_AUX nhw_PRON jest_ADV yn_PART aros_VERB i_PART weld_VERB sut_ADV oedd_VERB y_DET gwynt_NOUN yn_PART chw'thu_VERB ?_PUNCT
Yn_PART bosib_ADJ i_PART weld_VERB oedd_VERB gynnon_VERB nhw_DET hawl_NOUN i_ADP bleidleiso_NOUN ffordd_NOUN 'na_ADV ?_PUNCT
'_PUNCT Dw_AUX i_PRON ddim_PART yn_PART gw'bod_ADJ ._PUNCT
'_PUNCT Dw_AUX i_PRON 'm_DET yn_PART gw'bod_ADJ pam_ADV bod_AUX nhw_PRON 'di_PART cadw_VERB 'n_PART dawel_ADJ ._PUNCT
Achos_CONJ yn_PART ôl_NOUN [_PUNCT -_PUNCT ]_PUNCT mi_PRON '_PUNCT naethon_VERB ni_PRON an_DET sefydliad_NOUN exit_ADJ poll_DET ar_ADP y_DET noson_NOUN a_CONJ mi_PRON ddywedodd_VERB tri_NUM chwarter_NOUN o_ADP bobl_NOUN a_CONJ holwyd_VERB [_PUNCT -_PUNCT ]_PUNCT saith_NUM deg_NUM pump_NUM y_DET cant_NUM ohonyn_ADP nhw_PRON [_PUNCT -_PUNCT ]_PUNCT bod_AUX nhw_PRON wedi_PART penderfynu_VERB yn_PART hir_ADV [_PUNCT -_PUNCT ]_PUNCT yn_PART bell_NOUN yn_PART ôl_NOUN sut_ADV oedden_ADP nhw_PRON 'n_PART mynd_VERB i_ADP bleidleisio_VERB ._PUNCT
Felly_CONJ mi_PART oedden_AUX nhw_DET wedi_PART penderfynu_VERB cyn_ADP yr_DET ymgyrch_NOUN ond_CONJ jest_ADV bod_VERB rhei_VERB ohonyn_ADP nhw_PRON ddim_PART isio_ADV deud_VERB ._PUNCT
A_CONJ bebe_NOUN oedd_VERB yn_PART ddiddorol_ADJ am_ADP y_DET canlyniad_NOUN o_ADP safbwynt_NOUN Iwerddon_PROPN a_CONJ pa_PART fath_NOUN o_ADP Iwerddon_PROPN ydan_VERB ni_PRON '_PUNCT rŵan_ADV ydi_VERB  _SPACE bod_VERB [_PUNCT -_PUNCT ]_PUNCT chi_PRON 'n_PART '_PUNCT bod_VERB [_PUNCT -_PUNCT ]_PUNCT bod_AUX pobl_NOUN [_PUNCT -]'d_PUNCT oedd_VERB 'na_ADV ddim_ADP rhaniade_NOUN ._PUNCT
'_PUNCT D_NUM oedd_VERB 'na_ADV ddim_VERB rhaniade_NOUN daearyddol_ADJ ._PUNCT
'_PUNCT D_NUM oedd_VERB 'na_ADV ddim_PART rhaniade_VERB rhwng_ADP merched_NOUN a_CONJ dynion_NOUN ar_ADP y_DET bleidlais_NOUN ._PUNCT
Hynny_PRON ydi_ADV bod_VERB  _SPACE [_PUNCT -_PUNCT ]_PUNCT chi_PRON 'n_PART '_PUNCT bod_VERB [_PUNCT -_PUNCT ]_PUNCT y_DET mwyafrif_NOUN o_ADP ferched_VERB wedi_PART pleidleisio_VERB "_PUNCT ie_INTJ "_PUNCT ._PUNCT
Y_DET mwyafrif_NOUN o_ADP ddynion_NOUN ._PUNCT
Mwyafrif_NOUN o_ADP pobl_NOUN cefn_NOUN gwlad_NOUN ._PUNCT
Mwyafrif_NOUN o_ADP pobl_NOUN yn_ADP y_DET dinasoedd_NOUN ._PUNCT
Pob_DET rhan_NOUN o_ADP 'r_DET ynys_NOUN wedi_PART pleidleisio_VERB "_PUNCT ie_INTJ "_PUNCT ._PUNCT
Y_DET mwyafrif_NOUN ohonyn_ADP nhw_PRON heblaw_ADP am_ADP Donegal_PROPN i_ADP fyny_ADV yn_ADP y_DET gogledd_NOUN [_PUNCT -_PUNCT ]_PUNCT gogledd_NOUN orllewin_NOUN ._PUNCT
Ag_NOUN [_PUNCT =_SYM ]_PUNCT o_ADP feddwl_NOUN [_PUNCT /=_PROPN ]_PUNCT o_ADP feddwl_VERB am_ADP bleidleisie_NOUN yyy'ill_ADV sy_VERB 'di_PART bod_VERB yn_ADP y_DET blynyddoedd_NOUN dwytha_ADJ yn_ADP Iwerddon_PROPN mama_NOUN hyn_PRON yn_PART newid_VERB [_PUNCT =_SYM ]_PUNCT mawr_ADJ mawr_ADJ [_PUNCT /=_PROPN ]_PUNCT mawr_ADJ o_ADP ran_NOUN diwylliant_NOUN y_DET wlad'n'd_NOUN ydi_VERB ?_PUNCT
'_PUNCT N_NUM '_PUNCT d_PRON 'i_PRON ._PUNCT
A_PART mae_VERB o_PRON ar_ADP hyd_NOUN yr_DET un_NUM llwybr_NOUN mae_AUX Iwerddon_PROPN wedi_PART bod_AUX yn_PART dilyn_VERB dros_ADP y_DET blynyddoedd_NOUN dwytha_ADJ 'ma_ADV ._PUNCT
ym_ADP mi_PRON oedd_VERB pobl_NOUN arfer_NOUN a_CONJ sôn_VERB am_ADP y_DET drindod_NOUN arall_ADJ yn_ADP Iwerddon_PROPN sef_CONJ atal_ADV cenhedlu_VERB [_PUNCT -_PUNCT ]_PUNCT ysgariad_NOUN ac_CONJ erthylu_NOUN ._PUNCT
Pethe_PROPN oedd_AUX yr_DET eglwys_NOUN babyddol_ADJ yn_ADP eu_DET herbyn_ADP [_PUNCT -_PUNCT ]_PUNCT yn_PART chwyrn_ADJ yn_ADP eu_DET herbyn_ADP ._PUNCT
Ac_CONJ erbyn_ADP '_PUNCT rŵan_ADV wrth_ADP gwrs_NOUN mama_NOUN pobl_NOUN Iwerddon_PROPN wedi_PART pleidleisio_VERB dros_ADP y_DET pethe_NOUN [_PUNCT -_PUNCT ]_PUNCT dros_ADP ganiatáu_NOUN y_DET pethe_NOUN [_PUNCT -_PUNCT ]_PUNCT y_DET siboleths_NOUN mawr_ADJ 'ma_ADV [_PUNCT -_PUNCT ]_PUNCT a_CONJ wedi_PART caniatáu_VERB iddyn_ADP nhw_PRON fod_VERB ._PUNCT
Ond_CONJ wedi_PART deud_VERB hynny_PRON i_ADP gyd_CONJ mae_AUX 'n_PART rhaid_VERB i_ADP ni_PRON gofio_VERB bod_AUX Iwerddon_PROPN dal_ADV i_ADP fod_VERB yn_PART wlad_NOUN babyddol_ADJ [_PUNCT -_PUNCT ]_PUNCT yn_PART grefyddol_ADJ ag_CONJ yn_PART ddiwylliannol_ADJ ._PUNCT
273.162_NOUN
292.041_ADJ
'_PUNCT Naethoch_NOUN chi_PRON sôn_VERB bod_VERB Donegal_PROPN [_PUNCT -_PUNCT ]_PUNCT yr_DET dalaith_NOUN sydd_VERB nesa'_ADJUNCT at_ADP Ogledd_X Iwerddon_PROPN mewn_ADP ffordd_NOUN [_PUNCT -_PUNCT ]_PUNCT bod_AUX honna_VERB wedi_PART pleidleisio_VERB yn_PART erbyn_ADP ._PUNCT
Ydi_NOUN hynna_PRON '_PUNCT dach_AUX chi_PRON 'n_PART meddwl_VERB yn_PART arwydd_NOUN o_ADP bebe_NOUN allai_VERB ddigwydd_VERB tase_VERB '_PUNCT ne_NOUN refferendwn_VERB yng_ADP Ngogledd_NOUN Iwerddon_PROPN neu_CONJ yn_ADP Donegal_PROPN ar_ADP ei_DET phen_NOUN ei_DET hun_DET ?_PUNCT
Ma'_VERBUNCT [_PUNCT -_PUNCT ]_PUNCT mama_NOUN Donegal_PROPN yn_PART wahanol_ADJ oherwydd_ADP bob_DET tro_NOUN mae_VERB '_PUNCT ne_NOUN bleidlais_NOUN neu_CONJ mama'_NOUNUNCT ne_NOUN refferendwm_NOUN neu_CONJ mae_VERB '_PUNCT ne_NOUN etholiad_NOUN mama_NOUN Donegal_PROPN yn_PART pleidleisio_VERB 'n_PART wahanol_ADJ i_ADP weddill_NOUN y_DET weriniaeth_NOUN ._PUNCT
A_PART mae_VERB 'n_PART daearyddol_ADJ yn_PART bell_ADJ i_ADP ffwrdd_ADV ._PUNCT
Ma_VERB 'r_DET pobl_NOUN yn_PART teimlo_VERB eu_PRON bod_VERB nhw_PRON yn_PART wahanol_ADJ yne_NOUN ._PUNCT
Mae_AUX nhw_PRON hefyd_ADV [_PUNCT -_PUNCT ]_PUNCT mae_VERB '_PUNCT ne_NOUN boblogaeth_NOUN [_PUNCT -_PUNCT ]_PUNCT yn_ADP y_DET refferendwm_NOUN yma_ADV [_PUNCT -_PUNCT ]_PUNCT mi_PRON oedd_VERB y_DET ffaith_NOUN bod_VERB '_PUNCT ne_NOUN lot_NOUN fwy_PRON o_ADP bobl_NOUN hŷn_ADJ yn_PART byw_VERB yne_NOUN na_CONJ pobl_NOUN ifanc_ADJ ._PUNCT
Mae_VERB 'r_DET canran_NOUN yn_PART wahanol_ADJ yn_ADP Donegal_PROPN ._PUNCT
Mae_VERB 'na_ADV gymaint_ADV o_ADP bobl_NOUN ifanc_ADJ yn_PART gadael_VERB Donegal_PROPN ._PUNCT
Fell_VERB mi_PRON oedd_VERB hyn_PRON yn_PART ffactor_NOUN hefyd_ADV ._PUNCT
Ond_CONJ hefyd_ADV '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB bod_VERB y_DET ffactor_NOUN [_PUNCT -_PUNCT ]_PUNCT fel_CONJ oedd_AUX enwg_VERB yn_PART sôn_VERB [_PUNCT -_PUNCT ]_PUNCT yn_ADP y_DET gogledd_NOUN bod_VERB 'na_ADV ddylanwad_NOUN y_DET Presbyteriaid_PROPN yno_ADV hefyd_ADV yn_PART ogystal_ADJ â_ADP dylanwad_NOUN yr_DET Eglwys_NOUN Babyddol_ADJ ._PUNCT
Felly_CONJ enwg_VERB '_PUNCT dach_VERB chi_PRON isio_VERB trio_ADV dyfalu_VERB tase_VERB 'na_ADV refferendwm_NOUN yn_PART digwydd_VERB yng_ADP Ngogledd_NOUN Iwerddon_PROPN [_PUNCT -_PUNCT ]_PUNCT sut_ADV fysa_VERB 'i_PRON 'n_PART mynd_VERB ?_PUNCT
Wel_INTJ 'w_PRON i_PRON 'n_PART meddwl_VERB petai_VERB refferendwm_NOUN gydan_ADJ ni_PRON [_PUNCT -_PUNCT ]_PUNCT fel_ADP gyda_ADP Brexit_PROPN byddai_VERB 'r_DET rhan_NOUN fwyaf_ADJ o_ADP bobl_NOUN yn_PART pleidleisio_VERB dros_ADP creu_VERB mesur_NOUN newydd_ADJ a_CONJ bydd_AUX erthylu_NOUN yn_PART cael_VERB ei_DET derbyn_VERB a_CONJ dan_ADP amodau_NOUN sy_VERB 'n_PART lot_ADV gwell_ADJ na_CONJ 'r_DET amodau_NOUN presennol_ADJ ._PUNCT
1042.489_NOUN
1061.415_NOUN
A_PART sut_ADV brofiad_NOUN oedd_VERB cael_VERB eich_DET magu_VERB yng_ADP lleoliad_NOUN a_CONJ mynd_VERB i_ADP ysgol_NOUN Gymraeg_PROPN hefyd_ADV ?_PUNCT
oedd_VERB y_DET profiad_NOUN yn_PART un_NUM wych_ADJ ._PUNCT
O'dd_NOUN [_PUNCT -_PUNCT ]_PUNCT  _SPACE o'dd_NOUN dim_DET lot_PRON o_ADP blant_NOUN oedd_VERB yn_PART gymysg_ADJ neu_CONJ o_ADP ddwy_NUM hil_VERB yng_ADP lleoliad_NOUN oeddwn_AUX i_PRON wastad_ADV yn_PART teimlo_VERB fel_ADP ge_NOUN 's_DET i_ADP fy_DET nghroeso_NOUN i_ADP 'r_DET ddinas_NOUN ac_CONJ i_ADP 'r_DET gymuned_NOUN Cymreig_ADJ hefyd_ADV ._PUNCT
Ac_CONJ oedd_VERB o_PRON ddim_PART actually_VERB '_PUNCT nes_CONJ i_ADP fi_PRON actually_ADV gadael_VERB lleoliad_NOUN a_CONJ mynd_VERB i_ADP 'r_DET prifysgol_NOUN yn_PART lleoliad_NOUN '_PUNCT nes_CONJ i_PART actually_ADV sylwi_VERB pa_ADV mor_ADV wahanol_ADJ oeddwn_VERB i_PRON i_PART gymharu_VERB â_ADP ffrindiau_NOUN fi_PRON [_PUNCT -_PUNCT ]_PUNCT i_PART gymharu_VERB â_ADP 'r_DET athrawon_NOUN a_CONJ pa_ADV mor_ADV wahanol_ADJ o_ADP 'n_PART i_PRON o_ADP ran_NOUN sut_ADV o_ADP 'n_PART i_PRON 'n_PART edrych_VERB ._PUNCT
Oeddech_AUX chi_PRON 'n_PART deud_VERB yn_ADP y_DET darn_NOUN '_PUNCT naethoch_NOUN chi_PRON i_ADP radio_NOUN Saesneg_PROPN [_PUNCT -_PUNCT ]_PUNCT oeddech_AUX chi_PRON 'n_PART deud_VERB [_PUNCT -_PUNCT ]_PUNCT oeddech_AUX chi_PRON 'n_PART sôn_VERB am_ADP wlad_NOUN oedd_VERB weithiau_ADV fel_ADP tase_VERB hi_PRON ddim_PART cweit_ADV yn_ADP eich_DET derbyn_VERB chi_PRON ._PUNCT
O'dd_NOUN 'na_ADV deimlad_NOUN felly_ADV ?_PUNCT
Jest_ADV teimlad_NOUN a_CONJ ge_NOUN 's_DET i_ADP cwpl_NOUN o_ADP weithie_NOUN tra_CONJ o_ADP 'n_PART i_PRON 'n_PART tyfu_VERB fyny_ADV ._PUNCT
A_CONJ '_PUNCT nes_CONJ i_ADP ddim_PART cael_VERB y_DET teimlad_NOUN yma_ADV trwy_ADP 'r_DET amser_NOUN ._PUNCT
O'dd_NOUN 'na_ADV ddigwyddiade_VERB 'n_PART bendant_ADJ b_ADP '_PUNCT le_NOUN oedd_AUX pobl_NOUN yn_PART rhoi_VERB enw_NOUN penodol_ADJ i_ADP fi_PRON ._PUNCT
Neu_PART yn_PART edrych_VERB arna_ADP '_PUNCT i_PRON mewn_ADP ffordd_NOUN penodol_ADJ heb_ADP gwestiynu_VERB yr_DET ochr_NOUN arall_ADJ ohona'_NOUNUNCT i_PRON ._PUNCT
Neu_PART heb_ADP hyd_NOUN yn_PART oed_NOUN gofyn_VERB i_ADP fi_PRON sut_ADV faswn_VERB i_ADP 'n_PART hoffi_ADV ca'l_VERB fy_DET ngweld_VERB ._PUNCT
Dyna_ADV oedd_VERB fy_DET nghwestiwn_NOUN nesa'_ADJUNCT i_PRON ._PUNCT
Sut_ADV [_PUNCT -_PUNCT ]_PUNCT gan_CONJ bod_VERB pobl_NOUN wastad_ADV yn_PART rhoid_VERB labeli_NOUN ar_ADP [_PUNCT -_PUNCT ]_PUNCT '_PUNCT dan_ADP ni_PRON 'n_PART rhoid_VERB nhw_PRON ar_ADP ein_DET gilydd_NOUN [_PUNCT -_PUNCT ]_PUNCT sut_ADV fysech_VERB chi_PRON 'n_PART lecio_ADV cael_VERB eich_PRON gweld_VERB ?_PUNCT
BeBe_AUX fysach_ADV chi_PRON 'n_PART lecio_ADV cael_VERB eich_DET galw_VERB ?_PUNCT
Wel_ADV oedd_VERB mam_NOUN a_CONJ dad_NOUN wastad_ADV 'di_PART deud_VERB wrth_ADP fi_PRON a_CONJ fy_DET chwiorydd_NOUN [_PUNCT -_PUNCT ]_PUNCT ti_PRON 'n_PART '_PUNCT bod_VERB [_PUNCT -_PUNCT ]_PUNCT '_PUNCT dan_ADP ni_PRON 'n_PART o_ADP ddwy_NUM hil_NUM ._PUNCT
'_PUNCT Dan_PROPN ni_PRON 'n_PART blant_NOUN cymysg_ADJ ._PUNCT
Dan_ADP ni_PRON gyda_ADP gwreiddiau_NOUN Cymreig_ADJ a_CONJ gwreiddiau_NOUN o_ADP lleoliad_NOUN ._PUNCT
So_PART yn_PART tyfu_VERB fyny_ADV o_ADP 'n_PART i_PRON wastad_ADV yn_PART deud_VERB '_PUNCT tha_PART pobl_NOUN "_PUNCT '_PUNCT dw_VERB i_PRON 'n_PART gymysg_ADJ "_PUNCT [_PUNCT -_PUNCT ]_PUNCT yn_ADP Saesneg_PROPN mixed_NOUN race_ADJ ._PUNCT
Oedd_VERB mam_NOUN a_CONJ dad_NOUN wastad_ADV yn_PART deud_VERB '_PUNCT tha_PART ni_PRON i_ADP ddathlu_VERB 'r_DET ddwy_NUM ochr_NOUN [_PUNCT -_PUNCT ]_PUNCT ti_PRON 'n_PART '_PUNCT bod_VERB [_PUNCT -_PUNCT ]_PUNCT i_ADP ddathlu_VERB yr_DET ochr_NOUN Cymraeg_PROPN ._PUNCT
Ac_CONJ oedden_AUX ni_PRON 'n_PART neud_VERB hwnna_PRON [_PUNCT -_PUNCT ]_PUNCT ti_PRON 'n_PART '_PUNCT bod_VERB [_PUNCT -_PUNCT ]_PUNCT oedden_ADP ni_PRON 'n_PART byw_VERB yng_ADP Nghymru_PROPN ac_CONJ yn_PART siarad_VERB Cymraeg_PROPN ._PUNCT
Mae_AUX grŵp_NOUN o_ADP ffrindiau_NOUN '_PUNCT da_ADJ fi_PRON o_ADP 'r_DET ysgol_NOUN ._PUNCT
'_PUNCT Dan_PROPN ni_PRON gyd_ADV yn_PART siarad_VERB Cymraeg_PROPN hefo_ADP 'n_PART gilydd_NOUN '_PUNCT nawr_ADV ._PUNCT
'_PUNCT Nawr_ADV bod_VERB fi_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT ti_PRON 'n_PART '_PUNCT bod_VERB [_PUNCT -_PUNCT ]_PUNCT dau_NUM ddeg_NOUN tri_NUM a_CONJ fi_PRON 'di_PART ca'l_VERB amser_NOUN i_PART ddechre_VERB trafaelu_VERB ac_CONJ edrych_VERB i_ADP fewn_ADP i_ADP 'r_DET ochr_NOUN arall_ADJ ohona'_NOUNUNCT i_PRON '_PUNCT dw_AUX i_PRON hefyd_ADV yn_PART dathlu_VERB 'r_DET ffaith_NOUN bod_AUX fi_PRON 'n_PART dod_VERB o_ADP lleoliad_NOUN ._PUNCT
Mae_AUX gen_ADP '_PUNCT i_ADP deulu_NOUN yna_DET ._PUNCT
Mae_VERB gen_ADP '_PUNCT i_ADP ddiddordeb_NOUN yn_ADP y_DET ddwy_NUM ochr_NOUN '_PUNCT nawr_ADV ._PUNCT
1236.547_PROPN
1330.683_NOUN
1456.904_NOUN
Ac_CONJ ydach_VERB chi_PRON o_ADP ddifri_NOUN yn_PART credu_VERB fod_VERB gan_ADP bobl_NOUN fel_ADP chi_PRON sydd_VERB â_ADP dwy_NUM hil_NUM [_PUNCT -_PUNCT ]_PUNCT dau_NUM gefndir_NOUN [_PUNCT -_PUNCT ]_PUNCT bod_VERB gynnoch_VERB chi_PRON rhywbeth_NOUN arbenning_VERB i_ADP 'w_PRON gynnig_NOUN y_DET dyddiau_NOUN 'ma_ADV ?_PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART meddwl_VERB mae_VERB 'n_PART bwysig_ADJ i_ADP ni_PRON '_PUNCT nawr_ADV i_ADP fod_VERB mewn_ADP safle_NOUN eglur_ADJ gall_VERB plant_NOUN yn_PART gymysg_ADJ weld_VERB ei_DET hun_PRON yn_PART cael_VERB ei_PRON adlewyrchu_VERB yn_ADP y_DET gymuned_NOUN ac_CONJ yn_ADP y_DET gymdeithas_NOUN ._PUNCT
A_CONJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB dyna_DET bebe_NOUN ge_NOUN 's_DET i_PRON ddim_PART pan_CONJ o_ADP 'n_PART i_PRON 'n_PART ifanc_ADJ ._PUNCT
'_PUNCT S_NUM gen_ADP '_PUNCT i_PRON ddim_PART atgofion_VERB o_ADP edrych_VERB ar_ADP y_DET teledu_NOUN a_CONJ gweld_VERB rhywun_NOUN yn_PART edrych_VERB fel_ADP fi_PRON ar_ADP y_DET teledu_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART cofio_VERB gweld_VERB y_DET pobl_NOUN wyn_ADJ a_CONJ '_PUNCT dw_AUX i_PRON 'n_PART cofio_VERB gweld_VERB pobl_NOUN ddu_ADJ ._PUNCT
Ond_CONJ '_PUNCT dw_AUX i_ADP ddim_PART yn_PART cofio_VERB gweld_VERB rhywun_NOUN oedd_VERB yn_PART gymysg_ADJ ._PUNCT
A_CONJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB dyna_DET beth_PRON sy_VERB 'n_PART bwysig_ADJ i_PART neud_VERB '_PUNCT nawr_ADV ._PUNCT
A_CONJ dyna_DET un_NUM o_ADP 'r_DET rhesyme_PRON pam_ADV '_PUNCT dw_VERB i_ADP isie_ADV [_PUNCT -_PUNCT ]_PUNCT pam_ADV '_PUNCT nes_CONJ i_ADP ddewis_ADV neud_VERB newyddiaduriaeth_NOUN achos_ADP '_PUNCT dw_VERB isie_ADJ rhoi_VERB llais_NOUN i_ADP gynulleidfaoedd_NOUN sydd_VERB heb_ADP [_PUNCT -_PUNCT ]_PUNCT ti_PRON 'n_PART '_PUNCT bod_VERB [_PUNCT -_PUNCT ]_PUNCT lle_NOUN i_PART rhoid_VERB eu_DET barn_NOUN o_ADP 'r_DET blaen_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON eisie_ADV adlewyrchu_VERB y_DET gymdeithas_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON eisie_ADV adlewyrchu_VERB y_DET Cymru_PROPN newydd_ADJ yma_ADV ._PUNCT
A_CONJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB dyna_DET yw_VERB dyletswydd_NOUN ni_PRON '_PUNCT nawr_ADV ._PUNCT
1602.901_VERB
