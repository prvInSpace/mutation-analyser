[_PUNCT Sŵn_NOUN cerddoriaeth_NOUN yn_ADP y_DET cefndir_NOUN a_CONJ symud_VERB llestri_NOUN ]_PUNCT
Haia_INTJ ._PUNCT
Hai_NOUN '_PUNCT da_ADJ chi_PRON 'n_PART iawn_ADJ ?_PUNCT
Yndw_VERB diolch_INTJ ._PUNCT
'_PUNCT Da_ADJ chi_PRON ?_PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT Dal_VERB i_ADP fynd_VERB de_NOUN ._PUNCT
Ia_ADJ [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
O_ADP rai_PRON '_PUNCT '_PUNCT ni_PRON bydd_VERB ?_PUNCT
Rhaid_VERB ._PUNCT
Bydd_VERB ._PUNCT
Ôô_NOUN ._PUNCT
'_PUNCT S_NUM '_PUNCT im_NUM llawar_NUM o_ADP ddewis_NOUN ym_ADP [_PUNCT -_PUNCT ]_PUNCT
Nagoes_VERB wir_ADV Dduw_PROPN ._PUNCT
Tywydd_NOUN 'ma_ADV reit_NOUN afiach_ADJ hefyd_ADV 'di_PART ?_PUNCT
Ma_AUX 'i_PRON 'di_PART oeri_VERB yndydy_ADV ?_PUNCT
Yndi_VERB [_PUNCT -_PUNCT ]_PUNCT
<_SYM SB_SYM >_SYM Ma_X 'i_PRON fod_VERB altro_NOUN ._PUNCT
O_ADP gobeithio_VERB 'r_DET nefoedd_NOUN ._PUNCT
O_ADP 'n_PART -_PUNCT ni_PRON O_ADP 'n_PART ni_PRON 'di_PART cael_VERB ein_DET dyfetha_VERB 'n_PART do_VERB ?_PUNCT
Do_INTJ ._PUNCT
Ga_NOUN i_ADP un_NUM sandwij_NOUN egg_ADJ mayonnaise_NOUN a_CONJ bacon_NOUN ie_INTJ ?_PUNCT
Wŷ_VERB a_CONJ bacon_NOUN +_SYM
Ar_ADP frown_ADJ ._PUNCT
+_VERB Ar_ADP frown_ADJ ._PUNCT
Ar_ADP frown_ADJ a_CONJ dim_PRON '_PUNCT nionyn_NOUN yn_ADP y_DET salad_NOUN plîs_INTJ ._PUNCT
Ma_VERB coleslaw_NOUN 'm_DET iawn_NOUN ?_PUNCT
Yndi_VERB ond_CONJ  _SPACE dydi_AUX o_ADP ddim_PART yn_PART b'yta_NOUN coleslaw_NOUN so_AUX 's_X '_PUNCT im_ADJ isho_NOUN coleslaw_NOUN ar_ADP hwnna_PRON chwaith_ADV '_PUNCT sti_NOUN [_PUNCT -_PUNCT ]_PUNCT
Isho_NOUN crisps_ADJ yn_PART lle_NOUN +_SYM
Ia_INTJ ._PUNCT
+_VERB coleslaw_NOUN ?_PUNCT
Ia_INTJ ._PUNCT
Ia_INTJ ia_PRON plîs_INTJ [_PUNCT saib_NOUN ]_PUNCT A_CONJ ga_NOUN i_ADP un_NUM sandwij_NOUN chicken_NOUN a_CONJ cranberry_NOUN ar_ADP frown_ADJ plîs_INTJ ?_PUNCT
Ydi_NOUN '_PUNCT hein_PRON efo_ADP 'r_DET side_NOUN salad_NOUN yndi_ADJ ?_PUNCT
Yn'dyn_NOUN ._PUNCT
O_ADP ia_PRON ._PUNCT
Ia_INTJ ._PUNCT
A_PART dw_VERB i_ADP 'm_DET isho_NOUN nionyn_NOUN chwaith_ADV diolch_INTJ ._PUNCT
Nagoes_VERB ?_PUNCT
Ocê_PROPN ._PUNCT
Ond_CONJ '_PUNCT da_ADJ chi_PRON 'n_PART iawn_ADJ efo_ADP 'r_DET coleslaw_NOUN ?_PUNCT
Yndw_VERB dw_VERB i_PRON 'n_PART iawn_ADJ efo_ADP 'r_DET coleslaw_NOUN yndw_VERB [_PUNCT saib_NOUN ]_PUNCT Diolch_NOUN yn_ADP fowr_NOUN ._PUNCT
A_CONJ te_NOUN plis_INTJ de_NOUN ._PUNCT
dyh_VERB cymryd_VERB yn_PART ganiatol_ADJ bobo_VERB fi_PRON 'n_PART mynd_VERB i_ADP gal_NOUN diod_NOUN efo_ADP '_PUNCT fo_PRON ._PUNCT
Te_VERB i_ADP ddau_NUM ie_INTJ ?_PUNCT
Ia_INTJ plîs_INTJ ._PUNCT
'_PUNCT Na_VERB chi_PRON ._PUNCT
<_SYM pesychu_VERB >_SYM
Seinio_VERB hwn_PRON ._PUNCT
Dala_VERB 'i_PRON '_PUNCT fo_PRON hwnna_PRON [_PUNCT saib_NOUN ]_PUNCT diolch_INTJ [_PUNCT ochneidio_VERB ]_PUNCT ._PUNCT
Ti_PRON 'n_PART g'bo_NOUN dw_VERB i_PRON 'n_PART llawn_ADJ annwyd_NOUN de_NOUN ._PUNCT
Ôô_NOUN yn'dach_NOUN ?_PUNCT
Diflas_PROPN +_PROPN
Ers_ADP mis_NOUN Chwefror_PROPN cofia_VERB ._PUNCT
+_VERB Iesgob_PROPN annwyl_ADJ ._PUNCT
Ers_ADP February_PROPN dw_AUX i_PART methu_VERB 'n_PART glir_ADJ a_CONJ cal_VERB gwarad_NOUN oho_NOUN [_PUNCT -_PUNCT ]_PUNCT dwi_AUX 'n_PART cymryd_VERB pethe_NOUN hay_CONJ fever_VERB a_CONJ bob_DET dim_PRON ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'di_PART bod_VERB at_ADP doctor_NOUN ?_PUNCT
Naddo_VERB ._PUNCT
E'lla'_VERBUNCT bo_VERB chi_PRON angen_NOUN ryw_DET antiobiotic_NOUN bach_ADJ ._PUNCT
Ôô_NOUN god_NOUN ma_VERB 'na_ADV ofn_NOUN bod_VERB fi_PRON ._PUNCT
Fydd_VERB rhaid_VERB fi_PRON fynd_VERB yn_PART siŵr_ADJ [_PUNCT -_PUNCT ]_PUNCT
Yn_PART diwadd_ADJ ._PUNCT
Cau_VERB rhoi_VERB nhw_PRON 'ma_ADV nhw_PRON n'de_SYM ?_PUNCT +_SYM
Ie_INTJ ._PUNCT
+_VERB Dw_AUX i_PRON 'di_PART cal_NOUN unwaith_ADV '_PUNCT sti_NOUN de_NOUN ._PUNCT [_PUNCT -_PUNCT ]_PUNCT
Dy_DET 'n_PART nhw_PRON 'm_DET -_PUNCT yn_PART Ia_ADJ ._PUNCT
Ma_AUX rywun_NOUN yn_PART mynd_VERB yn_PART immune_NOUN gormod_ADV iddyn_ADP nhw_PRON '_PUNCT dydy_VERB ?_PUNCT +_SYM
Yndi_INTJ ._PUNCT
+_VERB Dyna_ADV 'di_PART 'r_DET -_PUNCT bro_NOUN ._PUNCT
Fydda_VERB i_ADP 'm_DET yn_PART cal_VERB nhw_PRON chwaith_ADV de_NOUN ._PUNCT
Hwn_PRON yn_PART barod_ADJ '_PUNCT wan_ADJ '_PUNCT ta_CONJ be_PRON ?_PUNCT
Yndi_VERB +_SYM
Dw_AUX i_PRON 'm_DET yn_PART gweld_VERB '_PUNCT sti_NOUN ._PUNCT
Yndi_VERB ,_PUNCT yndi_VERB ._PUNCT
twelve_VERB -_PUNCT twelve_VERB +_SYM
Gweld_VERB uffar_ADJ o_ADP '_PUNCT im_NUM byd_NOUN ._PUNCT
+_VERB seventy_NOUN ._PUNCT
Dim_PRON ond_ADP y_DET nymbyrs_NOUN 'ma_ADV ._PUNCT
twelve_VERB seventy_NOUN ._PUNCT
Ocê_PROPN ._PUNCT
Rhaid_VERB '_PUNCT chi_PRON f'yta_VERB oranges_NOUN ._PUNCT
Meddan_VERB nhw_PRON de_NOUN ._PUNCT
Wel_CONJ ia_PRON dw_AUX i_PRON 'di_PART bod_AUX yn_PART cymryd_VERB '_PUNCT wsti_VERB tabledi_NOUN vitamins_NOUN a_CONJ vitamin_VERB c_PRON a_CONJ bob_DET dim_PRON '_PUNCT sti_NOUN ._PUNCT
Ma_VERB 'n_PART hir_ADJ iawn_ADV '_PUNCT dydy_VERB ?_PUNCT
D'iom_VERB yn_PART clirio_VERB de_NOUN [_PUNCT sŵn_NOUN resit_NOUN yn_PART printio_VERB ]_PUNCT ._PUNCT
Dw_AUX i_PRON 'n_PART licio_VERB torri_VERB 'n_PART nhrwyn_NOUN i_ADP ffwr_NOUN ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Anifyr_VERB de_NOUN ._PUNCT
E'lla'_VERBUNCT hay_NOUN fever_VERB dio_NOUN ?_PUNCT
O_ADP dw_VERB i_PRON 'di_PART trio_VERB '_PUNCT sti_NOUN cymryd_VERB petha'_NOUNUNCT hay_CONJ fever_VERB '_PUNCT sti_NOUN ._PUNCT
D_NUM '_PUNCT io_PRON 'm_DET yn_PART g'neud_VERB gwahaniaeth_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART rhoi_VERB give_X up_NUM arnyn_ADP nhw_PRON heddi_NOUN 'ma_ADV s'gen_NOUN i_ADP 'm_DET mynadd_NOUN '_PUNCT fo_VERB nhw_PRON ._PUNCT
Ôô_NOUN ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Diolch_VERB yn_PART fowr_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT nesa_ADJ ._PUNCT
Haia_INTJ ._PUNCT
Haia_INTJ ._PUNCT
Dau_NUM latte_ADJ plis_INTJ ._PUNCT
Dw_AUX i_PRON 'n_ADP y_DET bwrdd_NOUN wrth_ADP ymyl_NOUN y_DET drws_NOUN yn_PART fan'na_ADJ ._PUNCT
Oce_INTJ ._PUNCT
[_PUNCT Sŵn_NOUN llestri_NOUN ]_PUNCT ._PUNCT
[_PUNCT Sŵn_NOUN plentyn_NOUN bach_ADJ yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
<_SYM SB_SYM >_SYM Haio_X [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT Sŵn_NOUN cefndirol_ADJ plant_NOUN ]_PUNCT
Gen'o_VERB fi_DET pwrs_NOUN tro_NOUN 'ma_ADV [_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
-_PUNCT Gai_VERB '_PUNCT da_ADJ ni_PRON 'di_PART ordro_VERB te_NOUN yn_PART barod_ADJ ond_CONJ  _SPACE newn_ADP ni_PRON gal_NOUN o_ADP -_PUNCT efo_ADP 'r_DET efo_ADP 'r_DET pwdin_NOUN '_PUNCT wan_ADJ ._PUNCT
Iawn_INTJ ._PUNCT
gai_VERB carrot_NOUN cake_NOUN 'di_PART hwnna_PRON +_SYM
Ie_INTJ ._PUNCT
+_VERB yn_PART cefn_NOUN ?_PUNCT
gai_VERB carrot_NOUN cake_NOUN plîs_INTJ a_CONJ un_NUM o_ADP 'r_DET rice_NOUN crispie_ADJ cakes_NOUN 'na_ADV plîs_INTJ [_PUNCT saib_NOUN ]_PUNCT '_PUNCT Dyn_NOUN nai_PART talu_VERB am_ADP bob_DET dim_PRON wedyn_NOUN ._PUNCT
twenty_VERB pound_VERB fifty_NOUN five_X plis_INTJ ._PUNCT
[_PUNCT Sŵn_NOUN arian_ADJ yn_PART mynd_VERB i_ADP 'r_DET til_NOUN ]_PUNCT ._PUNCT
hmmm_PRON hang_ADV on_CONJ two_NOUN four_ADJ fifty_NOUN five_NOUN dd'edoch_VERB 'di_PART ie_INTJ ?_PUNCT
Ia_INTJ plîs_INTJ ._PUNCT
O_ADP nai_NOUN neud_VERB o_ADP fel'a_NOUN haws_ADJ ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ iawn_ADV ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB bod_AUX fi_PRON 'di_PART gwagio_VERB 'r_DET five_NOUN ps_NOUN i_ADP gyd_NOUN o_ADP 'ma_ADV ._PUNCT
Iawn_INTJ ?_PUNCT
Diolch_INTJ ._PUNCT
Helo_INTJ ._PUNCT
Haia_INTJ ._PUNCT
Ti_PRON 'n_PART iawn_ADJ ?_PUNCT
Iawn_INTJ ?_PUNCT
'_PUNCT Da_ADJ chi_PRON 'n_PART cymryd_VERB flyers_NOUN o_ADP gwbl_NOUN ?_PUNCT
O_ADP 'r_DET +_SYM
._PUNCT
+_VERB sefydliad_NOUN mama_NOUN nhw_DET ._PUNCT
O_ADP 'r_DET sefydliad_NOUN ._PUNCT
ymmm_VERB planio_VERB cymryd_VERB rhai_DET de_NOUN ._PUNCT
'_PUNCT Da_ADJ ni_PRON jyst_ADV gadal_VERB nhw_PRON 'n_PART fan_NOUN '_PUNCT cw_PRON +_SYM
Ia_INTJ ?_PUNCT
+_VERB Wrth_ADP ymyl_NOUN y_DET drws_NOUN ._PUNCT
Oce_INTJ ?_PUNCT
<_SYM SS_X >_SYM Diolch_INTJ ._PUNCT
Diolch_INTJ ._PUNCT
Sa_NOUN 'm_DET byd_NOUN yn_ADP Gymraeg_PROPN arnyn_ADP nhw_PRON ._PUNCT
587.186_ADJ
