0.0_NOUN
Helo_INTJ enwg_VERB ._PUNCT
[_PUNCT hisian_VERB ]_PUNCT ._PUNCT
Ti_PRON 'n_PART iawn_ADJ enwg_VERB ?_PUNCT
[_PUNCT hisian_VERB ]_PUNCT ._PUNCT
Wyt_AUX ti_PRON 'n_PART gallu_ADV d'eud_VERB unrhyw_DET lythyren_NOUN arall_ADJ ?_PUNCT
Neu_CONJ dim_PRON ond_ADP [_PUNCT hisian_VERB ]_PUNCT ?_PUNCT
[_PUNCT hisian_VERB ]_PUNCT ._PUNCT
enwg_VERB pam_ADV ti_PRON 'n_PART neud_VERB y_DET sŵn_NOUN yna_ADV ?_PUNCT
Achos_CONJ fasai_VERB dim_PRON ots_NOUN gen_ADP i_PRON fod_VERB yn_PART neidr_ADJ am_ADP un_NUM diwrnod_NOUN [_PUNCT hisian_VERB ]_PUNCT ._PUNCT
Ti_PRON 'n_PART gweld_VERB ?_PUNCT
Pam_ADV ?_PUNCT
Achos_CONJ mae_VERB 'na_ADV fotwm_NOUN 'di_PART hedfan_VERB oddi_ADP ar_ADP nghrys_NOUN nos_NOUN i_ADP a_CONJ mae_AUX hwnnw_PRON 'di_PART rholio_VERB yn_PART [_PUNCT aneglur_ADJ ]_PUNCT i_PRON ._PUNCT
Felly_ADV wnes_VERB i_PRON feddwl_VERB taswn_VERB i_ADP 'n_PART neidr_NOUN mi_PRON allwn_VERB i_ADP lithro_VERB a_CONJ [_PUNCT aneglur_ADJ ]_PUNCT a_CONJ chyrlio_VERB i_ADP bob_DET man_NOUN a_CONJ fasai_VERB gen_ADP i_PRON batrymau_NOUN lliwgar_ADJ ar_ADP fy_DET ngwisg_NOUN i_PRON ._PUNCT
A_CONJ mi_PART faswn_VERB i_ADP 'n_PART gwneud_VERB y_DET sŵn_NOUN yma_ADV [_PUNCT hisian_VERB ]_PUNCT ._PUNCT
Hoffwn_VERB i_PRON fod_VERB yn_PART geffyl_NOUN enwg_VERB ._PUNCT
[_PUNCT Gwnaiff_PROPN S_NUM 4_NUM synau_NOUN gweryru_VERB a_CONJ chamau_NOUN ceffyl_NOUN ]_PUNCT ._PUNCT
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Mi_PART faswn_VERB i_ADP 'n_PART hoffi_VERB bod_AUX yn_PART froga_NOUN ._PUNCT
[_PUNCT Clywir_ADP S_NUM 1_NUM yn_PART neidio_VERB ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
beth_PRON sy_VERB bod_VERB arnoch_ADP chi_PRON i_ADP gyd_NOUN ?_PUNCT
O_ADP tyrd_VERB yn_ADP dy_DET flaen_NOUN enwb_VERB bebe_NOUN hoffet_VERB ti_PRON i_ADP fod_VERB ?_PUNCT
'_PUNCT Mond_PROPN am_ADP un_NUM diwrnod_NOUN ._PUNCT
'_PUNCT Na_VERB i_ADP gyd_ADP ._PUNCT
Wel_INTJ '_PUNCT swn_AUX i_PRON 'n_PART gorfod_ADV dewis_VERB byddwn_AUX i_PRON 'n_PART hoffi_VERB bod_VERB yn_PART hwyaden_NOUN [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Mae_VERB hwyaid_NOUN yn_PART gymaint_ADV o_ADP hwyl_NOUN ._PUNCT
Maen_AUX nhw_PRON 'n_PART cerdded_VERB yn_PART ddoniol_ADJ [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Mae_VERB pig_NOUN hardd_ADJ gydan_VERB nhw_PRON a_CONJ mae_AUX nhw_PRON 'n_PART gwneud_VERB sŵn_NOUN rhyfedd_ADJ ._PUNCT
Fel_ADP hyn_PRON cwac_NOUN cwac_NOUN cwac_ADJ ._PUNCT
[_PUNCT Gwnaif_PROPN S_NUM 4_NUM synau_NOUN gweryru_VERB a_CONJ chamau_NOUN ceffyl_NOUN eto_ADV ]_PUNCT ._PUNCT
Crawc_NOUN crawc_NOUN ._PUNCT
[_PUNCT hisian_VERB ]_PUNCT ._PUNCT
Cwac_NOUN cwac_NOUN [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
[_PUNCT anadlu_VERB ]_PUNCT ._PUNCT
Ffrindiau_NOUN ._PUNCT
Mae_AUX nhrwyn_NOUN i_PRON 'n_PART cosi_VERB ._PUNCT
Mae_AUX llygaid_NOUN i_ADP 'n_PART rholio_VERB ._PUNCT
Mae_AUX nghoesau_NOUN i_ADP 'n_PART dawnsio_VERB a_CONJ mae_VERB ngliniau_NOUN 'n_PART [_PUNCT aneglur_ADJ ]_PUNCT mae_AUX 'n_PART rhaid_ADV mynd_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT Gwnaiff_PROPN S_NUM 2_NUM sŵn_NOUN iodlan_NOUN ]_PUNCT ._PUNCT
Mae_AUX rhaid_VERB i_ADP ni_PRON fynd_VERB at_ADP y_DET [_NOUN aneglur_ADJ ]_PUNCT ar_ADP frys_NOUN ._PUNCT
Mae_VERB gân_NOUN ar_ADP y_DET ffordd_NOUN ._PUNCT
Dowch_VERB ._PUNCT
[_PUNCT Clywir_PROPN pawb_PRON yn_PART cerdded_VERB ]_PUNCT ._PUNCT
Brysiwch_VERB bawb_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB i_PART gyfansoddi_VERB cân_NOUN am_ADP yr_DET anifeiliaid_NOUN fasech_VERB chi_PRON a_CONJ fi_PRON 'n_PART hoffi_VERB bod_VERB ._PUNCT
Chi_PRON 'n_PART barod_ADJ ?_PUNCT
Un_NUM dau_NUM tri_NUM ._PUNCT
337.594_NOUN
[_PUNCT giglan_VERB ]_PUNCT O_ADP wel_NOUN dyna_DET ydy_VERB cân_NOUN dda_ADJ on'd_X e_DET ?_PUNCT
Mi_PART fasai_VERB 'r_DET gân_NOUN yna_ADV 'di_PART gallu_ADV mynd_VERB ymlaen_ADV am_ADP byth_NOUN ._PUNCT
Allen_VERB ni_PRON fod_VERB i_PART gael_VERB cath_NOUN yn_PART mewian_VERB ._PUNCT
[_PUNCT Gwnaiff_PROPN S_NUM 3_NUM sŵn_NOUN mewian_VERB ]_PUNCT ._PUNCT
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
A_CONJ llygoden_NOUN yn_PART gwichian_VERB ._PUNCT
[_PUNCT Gwnaiff_PROPN S_NUM 3_NUM synau_NOUN gwichian_VERB ]_PUNCT ._PUNCT
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
A_CONJ llew_NOUN yn_PART rhuo_VERB ._PUNCT
Rawr_INTJ ._PUNCT
Neu_PART aderyn_NOUN yn_PART canu_VERB ._PUNCT
Tweet_VERB tweet_VERB tweet_VERB tweet_VERB tweet_VERB tweet_ADP ._PUNCT
<_SYM SS_X >_SYM [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
Pan_CONJ gaeth_ADJ y_DET plant_NOUN bach_ADJ bach_ADJ y_DET gân_NOUN 'ma_ADV allan_ADV nhw_PRON roi_VERB penillion_NOUN eu_DET hunain_DET ynddi_ADP ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
Syniad_NOUN bendi_ADJ <_SYM aneglur_ADJ 2_NUM >_SYM ._PUNCT
Mi_PART cawn_VERB nhw_PRON ddewis_NOUN pa_ADV bynnag_PRON anifail_NOUN maen_VERB nhw_PRON eisiau_VERB [_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
Werth_ADV i_ADP ni_PRON droi_VERB nôl_NOUN mewn_ADP i_PART enwg_VERB cyfenw_NOUN rwan_ADV ._PUNCT
Mae_AUX 'n_PART amser_NOUN i_ADP ni_PRON fynd_VERB ._PUNCT
ôô_NOUN mor_ADV fuan_ADJ ?_PUNCT
Ie_INTJ ._PUNCT
Ond_CONJ diolch_VERB am_ADP yr_DET holl_DET hwyl_NOUN ges_VERB i_ADP heddiw_ADV fel_ADP broga_NOUN a_CONJ mi_PRON fydda_VERB i_ADP nôl_NOUN cyn_ADP hir_ADJ ._PUNCT
758.955_ADJ
