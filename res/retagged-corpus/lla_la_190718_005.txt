Be_PRON ydy_VERB perlysiau_NOUN ?_PUNCT
ymm_ADP perlysyn_NOUN ydy_VERB cardomom_NOUN neu_CONJ rhywbeth_NOUN ?_PUNCT
Cardomom_NOUN ?_PUNCT Beth_PRON yw_VERB nhw_PRON ?_PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
'_PUNCT Swn_PROPN i_ADP 'm_DET yn_PART meindio_VERB bod_AUX yn_PART plismon_NOUN mewn_ADP rhyw_DET bentref_NOUN bach_ADJ  _SPACE [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT  _SPACE ym_ADP Mhatagonia_PROPN ._PUNCT
Ââ_VERB [_PUNCT =_SYM ]_PUNCT ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ie_INTJ ._PUNCT
[_PUNCT Ceir_VERB yn_PART mynd_VERB heibio_ADV ]_PUNCT
D'oes_AUX ddim_PART yn_PART digwydd_VERB ._PUNCT
Ddim_PART byd_NOUN yn_PART digwydd_VERB o_ADP gwbwl_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Jest_ADV patio_VERB plant_NOUN <_SYM aneglur_ADJ 2_NUM >_SYM ._PUNCT
Ww_ADP neis_ADJ iawn_ADV cyfenw_NOUN oedd_VERB o_PRON ._PUNCT
Gennai_VERB ym_ADP [_PUNCT -_PUNCT ]_PUNCT gennai_VERB hen_ADJ hen_ADJ ewythr_NOUN yn_ADP Patagonia_PROPN de_NOUN ._PUNCT
Ie_INTJ ._PUNCT
o_ADP  _SPACE o_ADP ardal_NOUN Bethesda_PROPN sy_AUX 'n_PART byw_VERB yn_ADP Patagonia_PROPN ._PUNCT
'_PUNCT Nes_NOUN i_ADP gyfarfod_VERB ei_DET fab_NOUN o_ADP cwpwl_ADP o_ADP flynyddoedd_NOUN yn_PART ôl_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Pan_CONJ nes_CONJ i_PART fynd_VERB yna_ADV ._PUNCT
Ac_CONJ plismon_NOUN oedd_VERB o_PRON ._PUNCT
Ia_INTJ ?_PUNCT
Ganol_NOUN nunlla_NOUN ._PUNCT
Ac_CONJ o_ADP ni_PRON 'n_PART gofyn_VERB "_PUNCT wel_NOUN bebe_NOUN da_ADJ chi_PRON 'n_PART neud_VERB ?_PUNCT "_PUNCT A_CONJ ddudodd_VERB "_PUNCT O_ADP jest_ADV  _SPACE jest_ADV [_PUNCT aneglur_ADJ ]_PUNCT o_ADP gwmpas_NOUN ._PUNCT +_SYM
[_PUNCT chwerthin_VERB ]_PUNCT
+_VERB Edrych_VERB o_ADP gwmpas_NOUN ._PUNCT "_PUNCT
Gwisgo_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
A_CONJ 'na_ADV fo_PRON ._PUNCT
Gwisgo_VERB het_NOUN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT
Cysgu_VERB allan_ADV dan_ADP y_DET sêr_NOUN a_CONJ ballu_VERB ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT
Ia_INTJ ._PUNCT
Dyna_ADV oedd_AUX o_PRON 'n_PART ddeud_VERB ._PUNCT
Dyna_ADV oedd_VERB ei_DET waith_NOUN o_ADP fel_ADP plismon_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Fel_CONJ ym_ADP [_PUNCT -_PUNCT ]_PUNCT fel_ADP y_DET plismon_NOUN yn_PART  _SPACE Dan_PROPN y_DET Wenallt_PROPN ._PUNCT
Pwy_PRON oedd_VERB o_PRON ?_PUNCT
ym_ADP ._PUNCT
Ââ_VERB ia_PRON ._PUNCT
ym_ADP ._PUNCT
Dim_DET [_PUNCT aneglur_ADJ ]_PUNCT ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Na_VERB na_INTJ [_PUNCT /=_PROPN ]_PUNCT na_INTJ ._PUNCT
ym_ADP ._PUNCT
PC_INTJ [_PUNCT -_PUNCT ]_PUNCT
PC_VERB Attila_NOUN Rees_PROPN ._PUNCT
Attila_PROPN Rees_PROPN ._PUNCT
Ufflawn_ADJ o_ADP enw_NOUN ._PUNCT
Ââ_VERB [_PUNCT =_SYM ]_PUNCT lyfli_NOUN [_PUNCT /=_PROPN ]_PUNCT lyfli_NOUN ._PUNCT
Stori_NOUN berffaith_NOUN ._PUNCT
There_VERB is_CONJ consternation_NOUN on_X Coronation_NOUN Street_X Ia_X ?_PUNCT
There_VERB is_CONJ perturbation_NOUN
Pertubation_PROPN
Consternation_NOUN a_CONJ pertubation_VERB Mae_VERB gen_ADP i_PRON lyfr_NOUN  _SPACE Dan_PROPN y_DET Wenallt_PROPN yn_ADP Gymraeg_PROPN ond_CONJ dw_AUX i_ADP ddim_PART wedi_PART cael_VERB ym_ADP [_PUNCT -_PUNCT ]_PUNCT dw_AUX i_PRON ddim_PART wedi_PART adeiladu_VERB digon_ADV o_ADP hyder_NOUN i_ADP drio_VERB ei_DET darllen_VERB hi_PRON +_SYM
Ââ_VERB ia_PRON ._PUNCT
+_VERB eto_ADV ._PUNCT
Achos_CONJ mae_VERB [_PUNCT =_SYM ]_PUNCT tipyn_NOUN yn_PART -_PUNCT gym_NOUN gymhleth_ADJ [_PUNCT /=_PROPN ]_PUNCT tipyn_NOUN yn_PART gymhleth_ADJ ._PUNCT
'_PUNCT Nawn_NOUN ni_PART edrych_VERB tipyn_NOUN bach_ADJ arna_ADP fo_PRON ._PUNCT
[_PUNCT Ci_NOUN yn_PART cyfarth_VERB ]_PUNCT
Mae_VERB o_ADP reit_NOUN ddiddorol_ADJ i_PART ddarllen_VERB ._PUNCT
Yndi_ADV ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ie_INTJ ._PUNCT
Yn_PART arbennig_ADJ os_CONJ ti_PRON 'n_PART gyfarwydd_ADJ â_ADP 'r_DET Saesneg_PROPN ._PUNCT
Ie_INTJ ._PUNCT
Ydw_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Wrth_ADP gwrs_NOUN ._PUNCT
Ond_CONJ dim_PRON yn_PART cyfieithu_VERB yn_PART llythrennol_ADJ wrth_ADP gwrs_NOUN ._PUNCT
Na_VERB ac_CONJ a_CONJ [_PUNCT -_PUNCT ]_PUNCT ac_CONJ addasiad_NOUN ydy_VERB o_PRON 'n_PART swyddogol_ADJ ynde_ADV ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_INTJ [_PUNCT /=_PROPN ]_PUNCT ia_INTJ [_PUNCT =_SYM ]_PUNCT Mae_VERB 'n_PART normal_ADJ [_PUNCT /=_PROPN ]_PUNCT mae_VERB 'n_PART normal_ADJ ._PUNCT
I_ADP neud_VERB hynny_PRON  _SPACE pob_DET amser_NOUN ac_CONJ i_ADP bob_DET cyfeiriad_NOUN [_PUNCT -_PUNCT ]_PUNCT
[_PUNCT Pobl_PROPN yn_PART siarad_VERB ar_ADP draws_ADJ ei_DET gilydd_NOUN ]_PUNCT
Mewn_ADP blodau_NOUN ?_PUNCT
Yn_PART blodeuo_VERB ?_PUNCT
Yn_PART blodeuo_VERB ?_PUNCT
Mae_VERB 'na_ADV arwydd_NOUN rhywle_ADV ._PUNCT
Ie_INTJ ._PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
ym_ADP ._PUNCT
Heb_ADP unrhyw_DET [_PUNCT aneglur_ADJ ]_PUNCT mae_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_INTJ [_PUNCT /=_PROPN ]_PUNCT ia_INTJ ._PUNCT
[_PUNCT Sŵn_NOUN traffig_NOUN ]_PUNCT
'_PUNCT Da_ADJ chi_PRON 'n_PART gyfarwydd_ADJ iawn_ADV efo_ADP Dinbych_PROPN felly_ADV ?_PUNCT
[_PUNCT Sŵn_NOUN traffig_NOUN ]_PUNCT
Ella_ADV bobo_VERB ni_PRON [_PUNCT -_PUNCT ]_PUNCT fyddan_VERB ni_PRON yna_ADV mewn_ADP munud_NOUN ._PUNCT
Wel_INTJ ._PUNCT
Llew_NOUN Aur_PROPN ._PUNCT
Ydy_VERB 'r_DET dafarn_NOUN [_PUNCT -_PUNCT ]_PUNCT
Ââ_VERB ia_PRON ._PUNCT
Llew_NOUN Gwyn_ADJ 'di_PART hwnna_PRON ._PUNCT
Llew_NOUN Aur_PROPN ._PUNCT
Ââ_VERB oce_SYM [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT Sŵn_NOUN traffig_NOUN ]_PUNCT
Wel_CONJ roedd_VERB 'na_ADV un_NUM [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT
[_PUNCT Sŵn_NOUN traffig_NOUN ]_PUNCT
Na_INTJ ._PUNCT
Oedd_VERB 'na_ADV rhywun_NOUN yn_PART gyfarwydd_ADJ gyda_ADP ?_PUNCT
[_PUNCT Sŵn_NOUN gwynt_NOUN ]_PUNCT
Ella_ADV 'na_ADV dyna_PRON pam_ADV bod_VERB genod_VERB ym_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP ni_PRON [_PUNCT -_PUNCT ]_PUNCT Ddes_VERB i_ADP yma_ADV ar_ADP gwrs_NOUN pan_CONJ o_ADP ni_PRON yn_ADP fy_DET arddegaga_NOUN ._PUNCT
So_NOUN dyna_DET pryd_NOUN nes_CONJ i_PART wario_VERB rhanfywaf_VERB amser_NOUN ._PUNCT
Ond_CONJ wan_ADJ mama_NOUN mrawd_NOUN yn_PART byw_VERB yn_ADP Henllan_PROPN so_ADV fydda_VERB i_ADP 'n_PART dod_VERB yma_ADV am_ADP dro_NOUN ._PUNCT
Henllan_PROPN ?_PUNCT
Ia_INTJ ._PUNCT
Achos_CONJ twenty_NOUN minutes_NOUN <_SYM aneglur_ADJ 2_NUM >_SYM ._PUNCT
[_PUNCT Sŵn_NOUN gwynt_NOUN ]_PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
Ochr_NOUN arall_ADJ mae_VERB 'r_DET un_NUM yna_DET ._PUNCT
Drws_NOUN [_PUNCT =_SYM ]_PUNCT yn_ADP y_DET [_PUNCT /=_PROPN ]_PUNCT yn_ADP y_DET blaen_NOUN yndi_ADJ ?_PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
Ffor_PROPN ._PUNCT
Yn_PART neud_VERB efo_ADP dafarn_NOUN hefyd_ADV yn_PART [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ââ_VERB ia_PRON ?_PUNCT
O_ADP 'r_DET enw_NOUN Talbot_PROPN ._PUNCT
Jest_ADV drws_NOUN cefn_NOUN yma_DET ._PUNCT
Be_ADP di_PRON 'r_DET fforwm_NOUN ta_CONJ ?_PUNCT
ym_ADP ._PUNCT
Wel_ADV oedd_VERB caffi_VERB ._PUNCT
Ââ_INTJ ._PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
Pwy_PRON -_PUNCT ysgrifenno_VERB ysgrifennodd_VERB ?_PUNCT
Tê_PRON yn_ADP y_DET Grug_PROPN ._PUNCT
Ââ_VERB ie_INTJ [_PUNCT =_SYM ]_PUNCT  _SPACE [_PUNCT /=_PROPN ]_PUNCT ym_ADP ._PUNCT
Dw_AUX i_PRON 'di_PART prynu_VERB llyfr_NOUN yn_PART diweddaraf_ADJ [_PUNCT =_SYM ]_PUNCT  _SPACE [_PUNCT /=_PROPN ]_PUNCT ym_ADP ._PUNCT
Ah_INTJ [_PUNCT =_SYM ]_PUNCT Dynes_NOUN dynes_ADJ [_PUNCT /=_PROPN ]_PUNCT dynes_NOUN [_PUNCT =_SYM ]_PUNCT   _SPACE [_PUNCT /=_PROPN ]_PUNCT ym_ADP ._PUNCT
Kate_PROPN ._PUNCT
Mae_VERB ym_ADP [_PUNCT -_PUNCT ]_PUNCT Kate_PROPN ?_PUNCT
A_CONJ girl_NOUN
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT Dw_AUX i_PRON wedi_PART dod_VERB a_CONJ 'i_PRON ochr_NOUN hi_PRON o_ADP 'r_DET byd_NOUN efo_ADP hi_PRON yma_ADV [_PUNCT chwerthin_VERB ]_PUNCT
[_PUNCT Sŵn_NOUN gwynt_NOUN ]_PUNCT
Ie_INTJ [_PUNCT chwerthin_VERB ]_PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
Felly_CONJ Llew_NOUN Gwyn_ADJ ._PUNCT
Llew_NOUN Aur_PROPN <_SYM aneglur_ADJ 4_NUM >_SYM ._PUNCT
Ââ_VERB Reit_PROPN ._PUNCT
Wela'_VERBUNCT i_PRON ._PUNCT
Do_VERB dw_AUX i_PRON 'di_PART bod_AUX yn_PART recordio_VERB efo_ADP <_SYM aneglur_ADJ 2_NUM >_SYM [_PUNCT chwerthin_VERB ]_PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
Ti_PRON 'di_PART clywed_VERB am_ADP Robert_PROPN Everett_PROPN ?_PUNCT
Na_INTJ ._PUNCT
Dw_VERB i_PRON ddim_PART yn_PART gyfarwydd_ADJ ynde_ADP ._PUNCT
[_PUNCT =_SYM ]_PUNCT Gweinidog_NOUN [_PUNCT /=_PROPN ]_PUNCT gweinidog_NOUN yn_ADP Capel_PROPN Mawr_ADJ ._PUNCT
Capel_PROPN Lôn_NOUN Swan_PROPN lawr_ADV y_DET ffordd_NOUN ._PUNCT
Ond_CONJ ymfudo_VERB i_ADP America_PROPN [_PUNCT -_PUNCT ]_PUNCT
Ââ_VERB reit_INTJ ._PUNCT
Hanner_NOUN canrif_NOUN ._PUNCT
A_PART gryf_ADJ iawn_ADV yn_ADP y_DET byd_NOUN Gymraeg_PROPN [_PUNCT -_PUNCT ]_PUNCT
Ââ_INTJ ._PUNCT
Yn_ADP erbyn_ADP caethwasiaeth_NOUN am_ADP y_DET rhyfel_NOUN [_PUNCT -_PUNCT ]_PUNCT yn_PART erbyn_ADP caethwasiaeth_NOUN ._PUNCT
Yn_PART lle_NOUN ?_PUNCT
Efrog_NOUN Newydd_ADJ ?_PUNCT
Efrog_NOUN Newydd_ADJ ._PUNCT
Byw_VERB yn_ADP Efrog_PROPN Newydd_ADJ ._PUNCT
Ond_CONJ o_ADP gwmpas_NOUN y_DET Gogledd_NOUN Ddwyrain_PROPN ._PUNCT
A_CONJ '_PUNCT sgwennu_VERB  _SPACE cylchgrawn_NOUN ._PUNCT
Wel_INTJ papur_NOUN i_PART ddeud_VERB y_DET gwir_NOUN ._PUNCT
Papur_NOUN newydd_ADJ ._PUNCT
Am_ADP flynyddoedd_NOUN maith_ADJ ._PUNCT
Yn_ADP Gymraeg_PROPN ._PUNCT
Yn_PART cael_VERB ei_DET gyhoeddi_VERB yn_ADP Efrog_PROPN Newydd_ADJ ?_PUNCT
Ie_INTJ ._PUNCT
Oce_INTJ ._PUNCT
Wel_INTJ [_PUNCT -_PUNCT ]_PUNCT ar_ADP y_DET [_PUNCT -_PUNCT ]_PUNCT yn_ADP y_DET  _SPACE talaith_NOUN ._PUNCT
Nid_PART yn_ADP yr_DET dinas_NOUN ._PUNCT
Mae_AUX 'n_PART byw_VERB yn_ADP y_DET cefn_NOUN gwlad_NOUN [_PUNCT =_SYM ]_PUNCT I_ADP fyny_ADV [_PUNCT /=_PROPN ]_PUNCT i_ADP fyny_ADV ._PUNCT
Ond_CONJ rhoi_VERB  _SPACE y_DET papur_NOUN newydd_ADJ o_ADP gwmpas_NOUN [_PUNCT =_SYM ]_PUNCT Gogledd_NOUN [_PUNCT /=_PROPN ]_PUNCT Gogledd_NOUN Ddwyrain_NOUN America_PROPN ._PUNCT
Lle_ADV Pennsylvania_PROPN a_CONJ Ohio_PROPN ._PUNCT
Diddorol_ADJ iawn_ADV ar_ADP ôl_NOUN idda_VERB fo_PRON fod_VERB yn_PART gweinidog_NOUN [_PUNCT chwerthin_VERB ]_PUNCT am_ADP dipyn_PRON ._PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
Wel_INTJ ._PUNCT
Mae_VERB 'na_ADV ddau_NUM gapel_NOUN [_PUNCT =_SYM ]_PUNCT jest_ADV [_PUNCT /=_PROPN ]_PUNCT jest_CONJ rownd_ADV y_DET gornel_NOUN yn_PART fan_NOUN 'ma_ADV ._PUNCT
Capel_PROPN Lôn_NOUN Swan_PROPN ._PUNCT
Mhm_INTJ ?_PUNCT
Capel_VERB be_PRON ?_PUNCT
Lôn_NOUN Swan_PROPN ._PUNCT
Lôn_NOUN Swan_PROPN ia_ADV ?_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT O_ADP ni_PRON 'n_PART meddwl_VERB bod_VERB fi_PRON 'n_PART clwad_NOUN Swan_PROPN ._PUNCT
Mae_AUX 'n_PART deud_VERB Chapel_PROPN Street_PROPN wan_ADV ._PUNCT
Achos_CONJ mae_VERB 'r_DET capel_NOUN yn_PART fan_NOUN yno_ADV ._PUNCT
Ond_CONJ yn_PART wreiddiol_ADJ ar_ADP y_DET cornel_NOUN roedd_VERB yna_ADV dafarn_NOUN o_ADP 'r_DET enw_NOUN [_PUNCT -_PUNCT ]_PUNCT
Y_DET Swan_NOUN [_PUNCT chwerthin_VERB ]_PUNCT
Felly_ADV Capel_PROPN Lôn_NOUN Swan_PROPN oedd_VERB pobl_NOUN [_PUNCT -_PUNCT ]_PUNCT
Diddorol_ADJ sut_ADV mae_AUX pethau_NOUN wedi_PART cael_VERB eu_DET ailenwi_VERB ar_ADP ôl_NOUN ei_DET gilydd_NOUN a_CONJ mae_VERB 'r_DET pethau_NOUN gwreiddiol_ADJ yn_PART diflannu_VERB ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT
Dim_PRON ond_ADP y_DET lôn_NOUN sydd_VERB r'un_NOUN fath_NOUN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
enwg_VERB '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB i_ADP lawr_ADV i_ADP Lôn_NOUN Swan_PROPN i_PART weld_VERB  _SPACE y_DET dwy_NUM [_PUNCT -_PUNCT ]_PUNCT y_DET dau_NUM gapel_NOUN ._PUNCT
Lôn_NOUN Swan_PROPN ?_PUNCT
Capel_NOUN Mawr_ADJ a_CONJ Capel_PROPN Lôn_NOUN Swan_PROPN ._PUNCT
Capel_PROPN Lôn_NOUN Swan_PROPN ._PUNCT
Capel_PROPN Lôn_NOUN Swan_X [_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ia_INTJ ._PUNCT
A_CONJ [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Yr_DET un_NUM ar_ADP y_DET r'un_NOUN lôn_NOUN ._PUNCT
Ââ_VERB reit_INTJ ._PUNCT
Oce_VERB lawr_ADV yn_PART fan_NOUN 'ma_ADV ._PUNCT
Ââ_VERB ia_PRON [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ââ_VERB ia_PRON ._PUNCT
Ddoi_VERB '_PUNCT innau_NOUN '_PUNCT fyd_NOUN ._PUNCT
Ti_PRON 'di_PART bod_VERB ?_PUNCT
Naddo_VERB ._PUNCT
A_PART dw_AUX i_PRON '_PUNCT sho_NOUN mynd_VERB i_ADP gael_VERB pres_VERB o_ADP banc_NOUN beth_PRON bynnag_PRON ._PUNCT
Wel_INTJ dw_AUX i_PRON 'di_PART tiwnio_VERB piano_NOUN a_CONJ jest_ADV wedi_PART sylweddoli_VERB [_PUNCT =_SYM ]_PUNCT mai_PART [_PUNCT /=_PROPN ]_PUNCT mai_PART allwedd_NOUN bwrdd_NOUN ._PUNCT
Bwrdd_NOUN allwedd_NOUN yn_PART symud_VERB i_ADP fyny_ADV ac_CONJ i_ADP lawr_ADV ._PUNCT
[_PUNCT =_SYM ]_PUNCT Bwrdd_PROPN allwedd_NOUN ?_PUNCT [_PUNCT /=_PROPN ]_PUNCT bwrdd_NOUN allwedd_NOUN ?_PUNCT
I_ADP fod_VERB ie_INTJ ._PUNCT
Ââ_VERB ia_PRON oce_NOUN [_PUNCT =_SYM ]_PUNCT Allwedd_NOUN [_PUNCT /=_PROPN ]_PUNCT allwedd_NOUN y_DET bwrdd_NOUN dw_AUX i_PRON 'n_PART meddwl_VERB ia_PRON ._PUNCT
Dw_VERB i_PRON 'n_PART siŵr_ADJ ia_ADV ._PUNCT
Allwedd_NOUN fod_VERB [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART  _SPACE [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
Ac_CONJ oedd_VERB ym_ADP [_PUNCT -_PUNCT ]_PUNCT rhaid_VERB fynd_VERB i_PART archebu_VERB  _SPACE saer_NOUN coed_NOUN [_PUNCT =_SYM ]_PUNCT i_ADP i_PRON [_PUNCT /=_PROPN ]_PUNCT i_PART newid_VERB  _SPACE [_PUNCT aneglur_ADJ ]_PUNCT newydd_ADJ [_PUNCT -_PUNCT ]_PUNCT
Ia_INTJ ._PUNCT
I_ADP sortio_VERB allan_ADV ._PUNCT
Oedd_VERB o_PRON 'n_PART da_ADJ iawn_ADV cyn_ADP i_ADP mi_PRON tiwnio_VERB ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ia_INTJ ._PUNCT
Dw_AUX i_PRON 'n_PART tiwnio_VERB unwaith_ADV y_DET flwyddyn_NOUN ._PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
Banc_VERB ar_ADP y_DET ffordd_NOUN i_ADP lawr_ADV ._PUNCT
Oes_VERB 'na_ADV HSBC_NOUN yn_PART fan_NOUN 'na_ADV oes_VERB ?_PUNCT
[_PUNCT Sŵn_NOUN traffig_NOUN ]_PUNCT
Dyma_ADV oedd_VERB [_PUNCT =_SYM ]_PUNCT y_DET [_PUNCT /=_PROPN ]_PUNCT y_DET gair_NOUN cyntaf_ADJ erioed_ADV i_ADP fi_PRON  _SPACE dysgu_VERB oedd_VERB "_PUNCT hi_PRON "_PUNCT Hi_INTJ darling_NOUN
[_PUNCT chwerthin_VERB ]_PUNCT
[_PUNCT =_SYM ]_PUNCT Dw_AUX i_PRON 'di_PART [_PUNCT /=_PROPN ]_PUNCT dw_AUX i_PRON 'di_PART <_SYM aneglur_ADJ 2_NUM >_SYM deall_VERB yr_DET ystyr_NOUN [_PUNCT aneglur_ADJ ]_PUNCT achos_ADP  _SPACE oedd_AUX fy_DET mam_NOUN yn_PART pwyntio_VERB arnai_VERB ._PUNCT
"_PUNCT Paid_VERB enwg_VERB "_PUNCT ._PUNCT
Rhywbeth_NOUN negyddol_ADJ ._PUNCT
Ia_ADJ mae_VERB 'n_PART ddiddorol_ADJ dydy_ADJ ._PUNCT
Achos_ADP  _SPACE [_PUNCT =_SYM ]_PUNCT o_ADP ni_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT o_ADP ni_PRON 'n_PART byw_VERB yn_ADP Ffrainc_PROPN am_ADP '_PUNCT chydig_ADJ a_CONJ mae_AUX nhw_PRON wedi_PART mabwysiadu_VERB gair_NOUN stop_NOUN o_ADP 'r_DET Saesneg_PROPN ._PUNCT
Jest_PART pethau_NOUN sydd_VERB mor_ADV amlwg_ADJ fel_ADP 'na_ADV ynde_VERB ._PUNCT
[_PUNCT Sŵn_NOUN traffig_NOUN ]_PUNCT
[_PUNCT Mân_ADJ siarad_VERB ]_PUNCT
1073.941_NOUN
