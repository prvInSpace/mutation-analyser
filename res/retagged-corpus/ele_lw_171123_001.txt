Newyddion_NOUN diweddara'_VERBUNCT gan_ADP enw_NOUN
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Facebook_PROPN
<_SYM img_VERB /_PUNCT >_SYM
Twitter_PROPN
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
#_ADJ enw_NOUN
Ysgol_NOUN Aeaf_NOUN enw_NOUN
Mae_AUX enw_NOUN yn_PART cynnal_VERB Ysgol_NOUN Aeaf_PROPN yn_ADP Aberystwyth_PROPN ar_ADP 8_NUM Rhagfyr_ADJ ._PUNCT
Byddai_VERB 'n_PART wych_ADJ gweld_VERB cynifer_PROPN o_ADP bobl_NOUN ifanc_ADJ yno_ADV â_ADP phosib_NOUN ._PUNCT
Bydd_AUX enw_NOUN yn_PART cynnal_VERB trafodaeth_NOUN ar_ADP sut_ADV gall_VERB ein_DET plaid_NOUN gynnal_ADJ a_CONJ meithrin_VERB diddordeb_NOUN pobl_NOUN ifanc_ADJ ._PUNCT
Cadeirir_VERB y_DET sesiwn_NOUN gan_ADP gyd-_NUM gadeirydd_NOUN enw_NOUN ,_PUNCT enwb1_SYM __NOUN __NOUN __NOUN ._PUNCT
Y_DET siaradwyr_NOUN gwadd_ADJ fydd_VERB enwg2_SYM cyfenw2_SYM __NOUN ,_PUNCT y_DET Cynghorydd_PROPN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ,_PUNCT y_DET Cynghorydd_PROPN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN (_PUNCT Conwy_PROPN )_PUNCT ,_PUNCT a_CONJ 'r_DET Cynghorydd_PROPN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ,_PUNCT felly_CONJ mae_AUX 'n_PART argoeli_VERB i_ADP fod_VERB yn_PART chwip_NOUN o_ADP drafodaeth_NOUN ._PUNCT
Os_CONJ oes_VERB gennych_ADP ddiddordeb_NOUN ,_PUNCT
cysylltwch_VERB â_ADP
cyfeiriad_NOUN -_PUNCT bost_NOUN
i_PART archebu_VERB lle_ADV ._PUNCT
Dilynwch_VERB y_DET
linc_NOUN
i_ADP 'r_DET digwyddiad_NOUN ar_ADP Facebook_PROPN am_ADP fwy_PRON o_ADP fanylion_NOUN ._PUNCT
Digwyddiadau_ADV i_PART ddod_VERB
Er_CONJ mor_ADV ddefnyddiol_ADJ ydi_VERB 'r_DET cyfryngau_NOUN cymdeithasol_ADJ ,_PUNCT does_VERB dim_DET byd_NOUN gwell_ADJ na_CONJ chyfarfod_VERB wyneb_NOUN yn_PART wyneb_NOUN bob_DET hyn_PRON a_CONJ hyn_PRON ._PUNCT
Rydym_VERB yn_PART annog_NOUN pob_DET cangen_NOUN i_ADP gyfarfod_VERB yn_PART rheolaidd_ADJ ._PUNCT
Dyma_DET rai_DET cyfarfodydd_NOUN /_PUNCT socials_ADJ fydd_AUX yn_PART cael_VERB eu_PRON cynnal_VERB dros_ADP yr_DET wythnosau_NOUN nesaf_ADJ :_PUNCT
Cangen_NOUN Aberystwyth_PROPN :_PUNCT
23_NUM /_SYM 11_NUM -_PUNCT cyfarfod_NOUN am_ADP 19.00_NUM yn_ADP enw_NOUN ._PUNCT
Cangen_NOUN Ynys_PROPN Môn_PROPN
:_PUNCT 28_NUM /_SYM 11_NUM -_PUNCT Peint_PROPN a_CONJ Sgwrs_PROPN yn_ADP enw_NOUN Llangefni_PROPN ._PUNCT
Cangen_NOUN Caerdydd_PROPN
:_PUNCT 1_NUM /_SYM 12_NUM -_PUNCT Noson_PROPN Ffilm_PROPN am_ADP 19.30_NUM enw_NOUN Cathays_PROPN
