enwg_VERB ._PUNCT
O_ADP mama_NOUN honna_VERB 'n_PART wych_ADJ gwych_ADV deilen_NOUN sych_ADJ ac_CONJ yn_PART hollol_ADV bendibombom_ADJ ._PUNCT
Ond_CONJ paid_ADV gweud_VERB wrth_ADP neb_PRON ._PUNCT
O_ADP ._PUNCT 'na_ADV 'i_PRON ddim_PART siŵr_ADJ ._PUNCT
enwb_VERB
[_PUNCT =_SYM ]_PUNCT enwb_NOUN [_PUNCT /=_PROPN ]_PUNCT enwb_NOUN ma_VERB 'r_DET tylwyth_NOUN teg_ADJ 'di_PART dod_VERB o_ADP hyd_NOUN i_ADP drysor_NOUN ._PUNCT
O_ADP mama_NOUN rhaid_VERB ti_PRON weld_VERB o_PRON ._PUNCT
Wir_ADV ?_PUNCT
Be_INTJ 'di_PART o_ADP ?_PUNCT
Deilan_INTJ ._PUNCT
Ond_CONJ mae_VERB 'r_DET ddeilan_NOUN yma_DET yn_PART wahanol_ADJ ._PUNCT
[_PUNCT sain_NOUN rhaglen_NOUN ]_PUNCT
'_PUNCT S'dim_ADV deilen_NOUN tebyg_ADJ iddi_ADP yn_ADP y_DET byd_NOUN i_ADP gyd_ADP ._PUNCT
A_CONJ ni_PRON yw_VERB 'r_DET unig_ADJ dylwyth_NOUN teg_ADJ erio'd_PUNCT i_ADP fod_VERB yn_PART berchen_NOUN ar_ADP ddeilen_NOUN goch_ADJ ._PUNCT
Ââ_INTJ ._PUNCT
Hmm_VERB y_DET peth_NOUN ydy_VERB '_PUNCT da_ADJ chi_PRON 'n_PART gweld_VERB ma_VERB 'r_DET hydref_NOUN wedi_PART dod_VERB ._PUNCT
Dewch_VERB efo_ADP fi_PRON i_ADP 'r_DET gegin_NOUN a_CONJ mi_PRON esbonia'_VERBUNCT i_PRON rwbath_NOUN i_ADP chi_PRON ._PUNCT
[_PUNCT sain_NOUN rhaglen_NOUN ]_PUNCT
enwb_VERB bebe_NOUN 'di_PART rhain_PRON ?_PUNCT
Wel_INTJ ._PUNCT
Dail_NOUN ._PUNCT
Ond_CONJ mae_VERB 'r_DET rhain_PRON yn_PART hen_ADJ ac_CONJ yn_PART frown_ADJ ac_CONJ yn_PART wlyb_ADJ ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART hoffi_VERB dail_NOUN ifanc_ADJ ._PUNCT
Gwyrdd_ADJ ._PUNCT
Aros_VERB funud_NOUN nawr_ADV enwb_NOUN ._PUNCT
Os_CONJ yw_AUX 'r_DET dail_NOUN yma_ADV 'n_PART frown_ADJ a_CONJ deilen_NOUN enwb_NOUN yn_PART goch_ADJ a_CONJ phob_DET deilen_NOUN arall_ADJ yn_PART wyrdd_ADJ ._PUNCT
Wel_CONJ pa_PART liw_NOUN mama_NOUN deilen_NOUN i_ADP fod_VERB te_NOUN ?_PUNCT
Ah_PART enwg_VERB mama_NOUN pawb_PRON yn_PART gw'bod_ADJ bod_AUX dail_NOUN i_ADP 'w_PRON cael_VERB ym_ADP mhob_DET lliw_NOUN ._PUNCT
Pinc_PROPN ._PUNCT
Glas_PROPN ._PUNCT
Du_INTJ ._PUNCT
Coch_ADJ a_CONJ smotiau_NOUN piws_ADJ ._PUNCT
enwg_VERB '_PUNCT dy_DET hynny_PRON ddim_PART yn_PART wir_ADJ nac_CONJ '_PUNCT dy_PRON ._PUNCT
Ma_INTJ '_PUNCT dail_NOUN yn_PART dechra'_VERBUNCT fel_ADP blagur_NOUN ._PUNCT
Yna_ADV 'n_PART troi_VERB 'n_PART wyrdd_ADJ yn_ADP yr_DET haf_NOUN ._PUNCT
Yn_PART goch_ADJ yn_ADP yr_DET hydref_NOUN ._PUNCT
Yna_ADV mama_AUX nhw_PRON 'n_PART disgyn_VERB oddi_ADP ar_ADP y_DET coed_NOUN yn_ADP y_DET gaeaf_NOUN a_CONJ wedyn_ADV mama_NOUN popeth_PRON yn_PART dechre_ADJ eto_ADV ._PUNCT
Ma_INTJ '_PUNCT dail_NOUN yn_PART tyfu_VERB bob_DET blwyddyn_NOUN ._PUNCT
Yn_ADP y_DET gwanwyn_NOUN mae_VERB 'n_PART flaguryn_NOUN ._PUNCT
Yn_ADP yr_DET haf_NOUN mae_VERB 'n_PART wyrdd_ADJ ar_ADP frigyn_NOUN ._PUNCT
Yn_ADP yr_DET hydref_NOUN coch_ADJ yw_VERB wedyn_ADV ._PUNCT
Yn_ADP y_DET gaeaf_NOUN mae_AUX hi_PRON 'n_PART disgyn_VERB ._PUNCT
O_ADP enwb_NOUN dyna_DET bennill_NOUN hyfryd_ADJ ._PUNCT
Mae_AUX 'n_PART rhaid_VERB imi_ADP gyfansoddi_VERB alaw_NOUN i_ADP 'r_DET geiriau_NOUN 'na_ADV i_ADP ni_PRON gyd_CONJ gofio_VERB sut_ADV mae_AUX dail_NOUN yn_PART mynd_VERB a_CONJ dod_VERB bob_DET blwyddyn_NOUN ._PUNCT
At_ADP y_DET Lwlilw_PROPN ._PUNCT
Pawb_PRON yn_PART barod_ADJ i_ADP alw_VERB 'r_DET Lwlilw_PROPN ?_PUNCT
Ydyn_VERB ._PUNCT
Ar_ADP ôl_NOUN tri_NUM ._PUNCT
Un_NUM dau_NUM tri_NUM
[_PUNCT canu_VERB ]_PUNCT [_PUNCT =_SYM ]_PUNCT Lwlilw_PROPN [_PUNCT /=_PROPN ]_PUNCT Lwlilw_PROPN ._PUNCT
Tyrd_AUX i_PRON 'n_PART gweld_VERB ni_PRON [_PUNCT =_SYM ]_PUNCT Lwlilw_PROPN [_PUNCT /=_PROPN ]_PUNCT Lwlilw_PROPN ._PUNCT
Su'mae_VERB Lwlilw_PROPN Lwli_PROPN ._PUNCT
Heddiw_ADV '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB i_PART gyfansoddi_VERB cân_NOUN am_ADP ddeilen_NOUN ._PUNCT
Mae_AUX 'n_PART newid_VERB drwy_ADP 'r_DET flwyddyn_NOUN i_ADP gyd_ADP ._PUNCT
Be_DET s'gin_NOUN ti_PRON i_ADP mi_PRON ?_PUNCT
[_PUNCT canu_VERB ]_PUNCT Yn_ADP y_DET gwanwyn_NOUN mae_VERB 'n_PART flaguryn_NOUN ._PUNCT
Yn_ADP yr_DET haf_NOUN mae_VERB 'n_PART wyrdd_ADJ ar_ADP frigyn_NOUN ._PUNCT
Yn_ADP yr_DET hydref_NOUN coch_ADJ yw_VERB wedyn_ADV ._PUNCT
Yn_ADP y_DET gaeaf_NOUN mae_AUX hi_PRON 'n_PART disgyn_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT canu_VERB ]_PUNCT Yn_ADP y_DET gwanwyn_NOUN mae_VERB 'n_PART flaguryn_NOUN ._PUNCT
Yn_ADP yr_DET haf_NOUN mae_VERB 'n_PART wyrdd_ADJ ar_ADP frigyn_NOUN ._PUNCT
Yn_ADP yr_DET hydref_NOUN coch_ADJ yw_VERB wedyn_ADV ._PUNCT
Yn_ADP y_DET gaeaf_NOUN mae_AUX hi_PRON 'n_PART disgyn_VERB ._PUNCT
760.960_VERB
