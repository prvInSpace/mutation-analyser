Crynodeb_NOUN Pobol_ADJ y_DET Cwm_NOUN W_ADP 01_NUM
Mae_AUX Kath_PROPN yn_PART dychwelyd_VERB i_ADP Gwmderi_PROPN ac_CONJ mae_VERB ganddi_ADP gyfrinach_NOUN ._PUNCT
Ar_ADP ôl_NOUN clywed_VERB sut_ADV mae_AUX Jim_PROPN wedi_PART trin_VERB Ricky_PROPN mae_AUX 'n_PART rhybuddio_VERB Jim_PROPN i_PART gadw_VERB draw_ADV ._PUNCT
Ond_CONJ buan_ADJ y_DET daw_NOUN i_PART glywed_VERB am_ADP feichiogrwydd_NOUN Courtney_PROPN a_CONJ 'r_DET ffaith_NOUN mai_PART Ricky_PROPN oedd_VERB tad_NOUN y_DET babi_NOUN ._PUNCT
A_PART fydd_AUX hi_PRON 'n_PART difaru_VERB ymddwyn_VERB mor_ADV fyrbwyll_ADJ ?_PUNCT
Mae_AUX Gaynor_PROPN yn_PART gwneud_VERB camgymeriad_NOUN mawr_ADJ wrth_ADP anfon_VERB e_PRON -_PUNCT bost_NOUN cyfrinachol_ADJ am_ADP Chester_PROPN i_ADP holl_DET rieni_NOUN 'r_DET ysgol_NOUN ._PUNCT
Er_CONJ i_ADP Ffion_PROPN weld_VERB yr_DET e_PRON -_PUNCT bost_NOUN ,_PUNCT mae_AUX Gethin_PROPN yn_PART ei_PRON pherswadio_VERB i_PART beidio_VERB â_ADP dweud_VERB dim_PRON ._PUNCT
Mae_VERB 'n_PART bosib_ADJ y_PART bydd_VERB camgymeriad_NOUN Gaynor_PROPN yn_PART gweithio_VERB o_ADP blaid_NOUN Ffion_PROPN yn_ADP ei_DET chyfweliad_NOUN ar_ADP gyfer_NOUN swydd_NOUN Pennaeth_PROPN yr_DET Ysgol_NOUN Gydol_ADJ Oes_VERB ._PUNCT
Daw_VERB Kelly_PROPN yn_PART ôl_NOUN o_ADP 'i_PRON gwyliau_NOUN a_CONJ chlywed_VERB am_ADP yr_DET hyn_PRON wnaeth_VERB Kevin_PROPN a_CONJ Nansi_PROPN i_ADP Anita_PROPN ._PUNCT
Er_CONJ bod_VERB Kelly_X 'n_PART awyddus_ADJ i_PART ffonio_VERB 'r_DET heddlu_NOUN ,_PUNCT mae_AUX Anita_PROPN yn_PART erfyn_VERB arni_ADP i_PART beidio_VERB â_CONJ gwneud_VERB hynny_PRON ._PUNCT
Y_DET peth_NOUN ola'_ADJUNCT mae_AUX Kelly_X 'n_PART disgwyl_VERB yw_VERB gweld_VERB Kevin_PROPN '_PUNCT nôl_NOUN yn_ADP Rhif_PROPN 10_NUM ._PUNCT
Sut_ADV y_PART bydd_AUX Anita_PROPN yn_PART ymateb_VERB y_DET tro_NOUN hwn_DET ?_PUNCT
Pobol_ADJ y_DET Cwm_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 8.00_NUM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 6.00_NUM ,_PUNCT S4C_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Ar_ADP alw_VERB :_PUNCT s4c.cymru_NOUN ;_PUNCT BBC_X iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cynhyrchiad_NOUN BBC_NOUN Cymru_PROPN
