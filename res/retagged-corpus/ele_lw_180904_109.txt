Byddaf_AUX yn_PART mynd_VERB heibio_ADV i_ADP fferm_NOUN fy_DET modryb_NOUN yn_ADP enw_NOUN ar_ADP y_DET ffordd_NOUN at_ADP enw1_DET __NOUN ,_PUNCT ond_CONJ cadwaf_VERB draw_ADV o_ADP 'r_DET traeth_NOUN !_PUNCT
Gobeithio_VERB y_PART bydd_VERB hi_PRON 'n_PART sych_ADJ ta_CONJ p'un_NOUN ._PUNCT
Roedd_VERB enw2_NOUN yn_PART dal_ADV i_ADP sôn_VERB am_ADP y_DET noson_NOUN ,_PUNCT ac_CONJ am_ADP ddigwyddiadau_NOUN chwarter_NOUN canrif_NOUN yn_PART ôl_NOUN yn_ADP y_DET enw_NOUN __NOUN !_PUNCT
Dim_PRON ond_CONJ rhoi_ADV gwybod_VERB bod_AUX popeth_PRON yn_PART mynd_VERB yn_PART dda_ADJ ar_ADP hyn_PRON o_ADP bryd_NOUN ;_PUNCT dylai_VERB fod_VERB drafft_NOUN cyntaf_ADJ gyda_ADP fi_PRON erbyn_ADP dydd_NOUN Iauu_PROPN ._PUNCT
Yn_ADP enw_NOUN ddydd_NOUN Gwener_PROPN ond_CONJ bydd_VERB dydd_NOUN Llun_PROPN a_CONJ dydd_NOUN Mawrth_PROPN gyda_ADP fi_PRON wedyn_ADV i_PART edrych_VERB dros_ADP bethau_NOUN ._PUNCT
Ambell_ADJ beth_PRON wedi_PART '_PUNCT mewnforio_VERB '_PUNCT yn_PART rhyfedd_ADJ i_ADP 'r_DET prosiect_NOUN ,_PUNCT felly_CONJ byddaf_AUX yn_PART croesi_VERB bysedd_NOUN bod_VERB popeth_PRON yn_PART '_PUNCT allforio_VERB 'n_PART iawn_ADJ hefyd_ADV ._PUNCT
Wedi_PART dechrau_VERB dogfen_NOUN Saesneg_PROPN Newidiadau_NOUN 3_NUM -_PUNCT ac_CONJ yn_PART nodi_VERB unrhyw_DET beth_PRON rwyf_AUX wedi_PART 'i_PRON weld_VERB o_ADP ran_NOUN prawfddarllen_NOUN yn_PART felyn_ADJ ._PUNCT
Mae_AUX ambell_ADJ batshyn_NOUN lle_NOUN nad_PART yw_VERB 'r_DET bylchau_NOUN rhwng_ADP y_DET llinellau_NOUN 'n_PART gyson_ADJ ,_PUNCT felly_ADV arhosaf_VERB nes_CONJ y_PART byddaf_AUX yn_PART cymharu_VERB 'r_DET cyfieithiad_NOUN â_ADP 'r_DET gwreiddiol_NOUN cyn_ADP anfon_VERB honno_PRON atat_ADJ ._PUNCT
Gobeithio_VERB bod_AUX popeth_PRON yn_PART mynd_VERB yn_PART dda_ADJ gyda_ADP ti_PRON ._PUNCT
