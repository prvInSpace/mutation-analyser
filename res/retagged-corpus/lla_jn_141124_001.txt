Ow_DET rhew_NOUN dw_VERB i_PRON mor_ADV oer_ADJ ._PUNCT
Sa_CONJ i_PRON 'n_PART credu_VERB bydda_AUX i_PRON 'n_PART gallu_VERB hedfan_VERB enwb_NOUN [_PUNCT =_SYM ]_PUNCT fydd_VERB [_PUNCT /=_PROPN ]_PUNCT fydd_VERB adenydd_PROPN i_PRON 'n_PART rhewi_VERB ._PUNCT
Mm_CONJ Mae_AUX wedi_PART bod_VERB yn_PART oer_ADJ trwy_ADP 'r_DET dydd_NOUN heddiw_ADV ond_CONJ yw_VERB hi_PRON ?_PUNCT
Dere_VERB rhedeg_VERB '_PUNCT da_ADJ fi_PRON yn_ADP yr_DET unfan_NOUN a_CONJ wedyn_ADV byddi_AUX di_PRON 'n_PART teimlo_VERB 'n_PART gynnes_VERB ._PUNCT
Ow_PART gwnaf_VERB diolch_NOUN enwb_NOUN [_PUNCT swn_NOUN rhedeg_ADJ ]_PUNCT [_PUNCT sŵn_NOUN cerddoriaeth_NOUN ]_PUNCT brr_NOUN ow_NOUN dw_VERB i_ADP bron_ADV â_ADP rhewi_NOUN ._PUNCT
Ow_INTJ pam_ADV fod_VERB rhaid_VERB i_ADP 'n_PRON nhrwyn_NOUN i_ADP fod_VERB yn_PART fanna_VERB ?_PUNCT
Reit_VERB yng_ADP nghanol_NOUN yng_ADP wyneb_NOUN i_ADP tasa_VERB fe_PRON ar_ADP yn_PART llaw_NOUN i_ADP neu_CONJ neu_CONJ ar_ADP y_DET mol_NOUN i_ADP sen_PROPN i_PRON 'n_PART gallu_ADV tynnu_VERB 'r_DET dillad_NOUN gwely_NOUN dros_ADP fy_DET mhen_NOUN i_ADP ond_CONJ na_PART mae_AUX 'n_PART rhaid_VERB i_ADP 'r_DET hen_ADJ drwyn_NOUN sefyll_VERB yn_PART stond_ADJ yng_ADP nghanol_NOUN yng_ADP wyneb_NOUN i_ADP ac_CONJ mae_VERB hynna_PRON yn_PART ofnadsa_ADJ 'n_PART anghyfleus_ADJ ._PUNCT
Ow_INTJ pam_ADV bod_VERB hi_PRON mor_ADV oer_ADJ 'ma_ADV ?_PUNCT
ow_PRON mae_VERB 'n_PART haf_NOUN yn_ADP y_DET byd_NOUN mawr_ADJ mawr_ADJ medden_NOUN nhw_PRON ._PUNCT
Dw_AUX i_PRON ddim_PART yn_PART gwybod_VERB os_CONJ alla_VERB i_PART adael_VERB y_DET gwely_NOUN heddiw_ADV ._PUNCT
Brr_NOUN brr_NOUN [_PUNCT sŵn_NOUN cerddoriaeth_NOUN ]_PUNCT
Na_VERB dim_PRON diolch_NOUN enwg_VERB dim_PRON heddiw_ADV ._PUNCT
Mae_VERB hi_PRON 'n_PART rhy_ADV oer_ADJ i_ADP mi_PRON dynnu_VERB 'n_PART nghôt_NOUN ._PUNCT
Tybed_ADV ble_ADV mae_VERB enwg_VERB heddiw_ADV ?_PUNCT
Yn_ADP y_DET cloc_NOUN falle_ADV ?_PUNCT
O_ADP helo_ADV enwg_VERB [_PUNCT sŵn_NOUN cloc_NOUN ]_PUNCT [_PUNCT sŵn_NOUN gwichian_VERB ]_PUNCT Beth_PRON pam_ADV dw_AUX i_ADP wedi_PART gwisgo_VERB fel_ADP hyn_PRON ?_PUNCT
Wel_CONJ achos_ADP ei_PRON bod_VERB hi_PRON mor_ADV oer_ADJ tu_NOUN allan_ADJ ._PUNCT
Mae_VERB hynny_PRON 'n_PART beth_PRON od_DET adeg_NOUN yma_DET 'r_DET flwyddyn_NOUN ._PUNCT
Mae_VERB hi_PRON fel_ADP arfer_NOUN yn_PART gynnes_NOUN braf_ADJ felly_CONJ dw_AUX i_PRON ddim_PART yn_PART dallt_VERB pam_ADV ei_PRON bod_VERB hi_PRON mor_ADV oer_ADJ ond_CONJ mae_AUX hi_PRON brr_NOUN enwg_VERB enwb_NOUN __NOUN
Yn_ADP fy_DET ngwely_NOUN enwb_NOUN mae_VERB hi_PRON 'n_PART rhy_ADV oer_ADJ i_PART ddod_VERB lawr_ADV grisiau_NOUN brr_AUX i_ADP ni_PRON 'n_PART dau_NUM yn_PART tŷ_NOUN teg_ADJ ._PUNCT
<_SYM a_CONJ 2_NUM >_SYM Ni_PRON 'n_PART aros_VERB mewn_ADP heddi_NOUN enwb_NOUN mae_VERB 'n_PART rhy_ADV oer_ADJ i_PART ddod_VERB mas_ADV ._PUNCT
Mm_CONJ wel_VERB beth_PRON am_ADP i_PRON mi_PRON wneud_VERB cawl_NOUN poeth_ADJ braf_ADJ i_ADP 'n_PART cynhesu_VERB ni_PRON i_ADP gyd_ADP ie_INTJ ?_PUNCT
Oh_INTJ ie_INTJ ._PUNCT
<_SYM s_INTJ ?_PUNCT >_SYM Dw_VERB i_PRON 'n_PART [_PUNCT aneglur_ADJ ]_PUNCT brr_NOUN [_PUNCT sŵn_NOUN tamborin_ADJ ]_PUNCT Mmm_PROPN ._PUNCT
Mmm_VERB enwb_NOUN roedd_VERB y_DET cawl_NOUN 'na_ADV yn_PART fendibompom_VERB mmm_NOUN ._PUNCT
A_PART mae_AUX fe_PRON wedi_PART cynhesi_VERB 'r_DET mewn_ADP tu_NOUN fewn_ADP ni_PRON 'n_PART braf_ADJ enwb_NOUN ._PUNCT
A_PART dw_AUX i_ADP dal_ADV methu_ADV deall_VERB pam_ADV bod_VERB hi_PRON mor_ADV oer_ADJ ._PUNCT
Ah_INTJ oh_VERB beth_PRON yw_VERB 'r_DET golau_NOUN rhyfedd_ADJ 'na_ADV ?_PUNCT
Dyna_DET beth_PRON od_DET ._PUNCT
Beth_PRON yw_VERB e_PRON sgwn_VERB i_PRON ?_PUNCT
Oh_INTJ arhoswch_VERB funud_NOUN tybed_ADV os_CONJ mae_VERB ?_PUNCT
Naci_NOUN siŵr_ADJ ._PUNCT
Ie_INTJ enwg_VERB sy_VERB 'na_ADV ._PUNCT
Ah_INTJ
enwg_VERB bachgen_NOUN bach_ADJ Mr_NOUN cyfenw_NOUN ?_PUNCT
Na_VERB ti_PRON fo_VERB helo_VERB enwg_VERB sut_ADV wyt_VERB ti_PRON ers_ADP talwm_NOUN ?_PUNCT
O_ADP mae_VERB 'n_PART braf_ADJ iawn_ADV dy_DET weld_VERB di_PRON eto_ADV ._PUNCT
Helo_INTJ ffrindiau_NOUN ._PUNCT
Oh_INTJ enwg_VERB ti_PRON ddim_PART yn_PART edrych_VERB yn_PART rhy_ADV hapus_ADJ ._PUNCT
Beth_PRON sy_AUX 'n_PART bod_VERB ?_PUNCT
212.067_NOUN
Ac_CONJ  _SPACE ei_PRON fod_AUX e_PRON wedi_PART trio_VERB ei_DET orau_NOUN i_PART wenu_VERB a_CONJ thwymo_VERB doedd_AUX o_ADP ddim_PART yn_PART gallu_VERB druan_NOUN bach_ADJ ._PUNCT
Ond_CONJ fe_PART wedodd_VERB enwb_NOUN wrthom_VERB feddwl_VERB am_ADP bethau_NOUN sy_AUX 'n_PART gwneud_VERB iddo_ADP deimlo_VERB 'n_PART gynnes_VERB tu_NOUN mewn_ADP ._PUNCT
Ac_CONJ os_CONJ ti_PRON 'n_PART meddwl_VERB ni_PART allwn_VERB ni_PRON gyd_ADP feddwl_VERB am_ADP bethau_NOUN cynnes_ADJ ._PUNCT
A_PART mae_AUX gen_ADP enwg_VERB wen_NOUN fawr_ADJ ar_ADP ei_DET wyneb_NOUN eto_ADV [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Lyfli_NOUN lyfli_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Beth_PRON sy_VERB 'n_PART dy_DET wneud_VERB di_PRON 'n_PART hapus_ADJ enwb_NOUN ?_PUNCT
<_SYM aneglur_ADJ 4_NUM >_SYM
Oh_VERB holl_DET offerynnau_NOUN y_DET byd_NOUN yn_PART chwarae_VERB dy_DET hoff_ADJ gân_NOUN di_PRON ie_VERB pha_DET un_NUM ydy_VERB honna_NOUN ?_PUNCT
<_SYM aneglur_ADJ 4_NUM >_SYM
Oh_AUX ti_PRON ddim_PART yn_PART gwybod_VERB ._PUNCT
Falle_VERB yr_DET un_NUM dan_ADP ni_PRON ar_ADP fin_NOUN ei_DET chyfansoddi_VERB ._PUNCT
Well_ADJ i_ADP mi_PRON ddechre_VERB felly_ADV ._PUNCT
Wel_CONJ beth_PRON am_ADP gyfansoddi_VERB cân_NOUN am_ADP yr_DET holl_DET bethau_NOUN sydd_AUX yn_PART gwneud_VERB i_ADP ni_PRON deimlo_VERB 'n_PART gynnes_VERB ?_PUNCT
Oh_INTJ gwych_INTJ <_SYM aneglur_ADJ 2_NUM >_SYM fach_NOUN ._PUNCT
A_CONJ beth_PRON am_ADP fynd_VERB i_PART dechre_VERB fel_ADP hyn_PRON ?_PUNCT
Os_CONJ yw_VERB dy_DET galon_NOUN yn_PART drwm_ADJ ac_CONJ anfodlon_ADJ yn_PART teimlo_VERB 'n_PART oer_ADJ meddwl_VERB yn_PART gynnes_VERB fy_DET ffrind_NOUN yw_VERB fy_DET neges_NOUN mae_AUX 'n_PART gweithio_VERB bob_DET tro_NOUN ._PUNCT
760.96_NOUN
