0.0_NOUN
Ie_INTJ croeso_NOUN aton_VERB ni_PRON i_ADP dre_NOUN lleoliad_NOUN ._PUNCT
Dŵr_NOUN croyw_ADJ llyn_NOUN naturiol_ADJ mwya'_ADJUNCT ni_PRON fydd_VERB canolbwynt_NOUN y_DET cyffro_NOUN wrth_ADP i_ADP athletwyr_NOUN gore_X '_PUNCT Cymru_PROPN a_CONJ thu_NOUN hwnt_ADV nofio_VERB [_PUNCT -_PUNCT ]_PUNCT beicio_VERB a_CONJ rhedeg_VERB o_ADP gwmpas_NOUN glannau_NOUN Llyn_NOUN enw_NOUN ._PUNCT
Ma_VERB 'r_DET ras_NOUN hon_DET yng_ADP nghyfres_NOUN enw_NOUN yn_PART rhan_NOUN o_ADP ŵyl_NOUN aml_ADJ chwaraeon_NOUN [_PUNCT -_PUNCT ]_PUNCT Gŵyl_NOUN enw_NOUN __NOUN ._PUNCT
Ma_VERB 'r_DET buzz_NOUN fyny_ADV fan_NOUN hyn_DET yn_PART wych_ADJ ._PUNCT
Ma_VERB 'r_DET tywydd_NOUN yn_PART boeth_ADJ a_CONJ mama_NOUN pawb_PRON yn_PART gyfeillgar_ADJ iawn_ADV fyny_ADV fan_NOUN hyn_DET yn_PART gogledd_NOUN Cymru_PROPN ._PUNCT
Am_ADP tirwedd_NOUN i_ADP gael_VERB y_DET fath_NOUN 'ma_ADV o_ADP beth_NOUN ._PUNCT
'_PUNCT Sa_NOUN chi_PRON 'di_PART gofyn_VERB am_ADP well_ADJ ._PUNCT
A_CONJ pam_ADV ma_VERB 'r_DET tywydd_NOUN fel_ADP hyn_PRON '_PUNCT dach_VERB chi_PRON wedyn_ADV yn_PART gweud_VERB "_PUNCT whiw_NOUN "_PUNCT ._PUNCT
Ma_VERB 'n_PART jest_ADV brilliant_VERB i_ADP gweld_VERB llwyth_PRON o_ADP pobl_NOUN yma_DET ._PUNCT
Ma_AUX 'r_DET haul_NOUN yn_PART helpu_VERB hefyd_ADV ._PUNCT
Ma_VERB 'n_PART ffantastig_ADJ ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART cael_VERB blast_NOUN ar_ADP y_DET penwythnos_NOUN yma_DET ._PUNCT
Ma_VERB 'n_PART gwych_ADJ gweld_VERB cyn_ADP gymaint_ADV o_ADP bobl_NOUN ._PUNCT
Ma_VERB 'r_DET rhei_NOUN sydd_AUX yn_PART cystadlu_VERB heddiw_ADV yn_PART athletwyr_NOUN o_ADP fri_NOUN ._PUNCT
Ma'_VERBUNCT criw_NOUN cryf_ADJ '_PUNCT dan_ADP ni_PRON 'n_PART cymryd_VERB rhan_NOUN o_ADP ran_NOUN y_DET dynion_NOUN a_CONJ 'r_DET merched_NOUN ._PUNCT
So_PART bydd_VERB hi_PRON 'n_PART ddiddorol_ADJ i_PART weld_VERB sut_ADV mae_AUX 'r_DET ras_NOUN yn_PART mynd_VERB ._PUNCT
100.335_PROPN
Diolch_INTJ yn_PART fawr_ADJ enwb_NOUN ._PUNCT
Wel_CONJ treiathlon_VERB pellter_NOUN olympaidd_ADJ ydi_VERB hi_PRON i_ADP fod_VERB yma_ADV 'n_PART lleoliad_NOUN ._PUNCT
Nofio_VERB am_ADP bymtheg_DET can_NOUN metr_NOUN ._PUNCT
Tri_NOUN deg_NUM naw_NUM cilometr_NOUN ar_ADP y_DET beic_NOUN i_ADP gyfeiriad_NOUN lleoliad_NOUN cyn_CONJ dychwelyd_VERB i_ADP 'r_DET lleoliad_NOUN ._PUNCT
Ac_CONJ yna_ADV deg_NUM cilometr_NOUN am_ADP y_DET cymal_NOUN ola'_ADJUNCT a_CONJ hynny_PRON yn_PART redeg_VERB o_ADP gwmpas_NOUN Llyn_NOUN enw_NOUN ._PUNCT
A_CONJ fel_ADV oedd_AUX enwb_NOUN yn_PART deud_VERB y_DET merched_NOUN a_CONJ 'r_DET dynion_NOUN yn_PART cychwyn_VERB gyda_ADP 'i_PRON gilydd_NOUN yn_ADP y_DET don_NOUN gynta'_ADJUNCT ._PUNCT
Ag_ADP  _SPACE fel_ADP sy_VERB 'di_PART cael_VERB ei_DET grybwyll_VERB eisoes'd_PUNCT ydyn_AUX nhw_PRON ddim_PART yn_PART gwisgo_VERB siwtia_VERB nofio_VERB llawn_ADJ ._PUNCT
Ac_CONJ fe_PART all_VERB hynny_PRON fod_VERB yn_PART fanteisiol_ADJ wedi_PART iddyn_ADP nhw_PRON gyrra'dd_NOUN yr_DET ardal_NOUN bontio_VERB cyn_CONJ mynd_VERB ar_ADP gefn_NOUN y_DET beic_NOUN ._PUNCT
A_CONJ ma_VERB 'r_DET nofwyr_NOUN cryfa'_NOUNUNCT wedi_PART agor_VERB dipyn_PRON o_ADP fwlch_NOUN yn_PART barod_ADJ a_CONJ mama_NOUN 'na_ADV enwa_VERB cyfarwydd_NOUN ymhlith_ADP y_DET rhai_PRON sy_AUX 'n_PART arwain_VERB y_DET ffordd_NOUN ._PUNCT
Ma_VERB 'r_DET ffefryn_NOUN enwg_VERB cyfenw_NOUN yn_ADP y_DET trydydd_ADJ safle_NOUN __NOUN __NOUN __NOUN __NOUN sydd_AUX yn_PART arwain_VERB y_DET ffordd_NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN yn_PART ail_ADJ ar_ADP hyn_PRON o_ADP bryd_NOUN yn_ADP y_DET top_NOUN melyn_ADJ ._PUNCT
334.541_VERB
Ma'_VERBUNCT cyfres_NOUN Cymru_PROPN yn_PART chwe_NUM ras_NOUN i_ADP gyd_ADP ._PUNCT
Hon_PRON yn_PART lleoliad_NOUN yw_VERB 'r_DET bedwaredd_NOUN gyda_ADP 'r_DET ddwy_NUM ola_ADP '_PUNCT  _SPACE lleoliad_NOUN a_CONJ __NOUN ._PUNCT
Ond_CONJ sut_ADV meddech_VERB chi_PRON mae_AUX 'r_DET gyfres_NOUN yn_PART gweithio_VERB a_CONJ 'r_DET pwyntie_NOUN yn_PART cael_VERB eu_DET hennill_NOUN ?_PUNCT
Wel_CONJ i_PART egluro_VERB mwy_PRON dyma_DET i_ADP chi_PRON enwg_VERB cyfenw_NOUN o_ADP __NOUN ._PUNCT
O_ADP ran_NOUN y_DET gyfres_NOUN ni_PRON rhoid_VERB lot_PRON o_ADP feddwl_VERB mewn_ADP i_ADP 'r_DET system_NOUN pwyntie_NOUN 'ma_ADV ._PUNCT
So_PART mae_VERB 'n_PART bwysig_ADJ i_ADP ni_PRON bod_VERB y_DET gyfres_NOUN yn_PART gryf_ADJ o_ADP 'r_DET cymal_NOUN gynta_ADJ a_CONJ 'r_DET cymal_NOUN ola'_ADJUNCT ._PUNCT
Mae_VERB 'r_DET pwyntie_NOUN 'n_PART seiliedig_ADJ ar_ADP amser_NOUN ._PUNCT
Felly_CONJ pe_CONJ bai_NOUN athletwr_NOUN fel_ADP enwg_VERB cyfenw_NOUN yn_PART ennill_VERB yma_ADV heddiw_ADV gyda_ADP amser_NOUN gyflym_ADJ ac_CONJ yn_PART ennill_VERB y_DET ddwy_NUM ras_NOUN ola_NOUN 'n_ADP y_DET gyfres_NOUN fe_PRON gall_VERB e_PRON '_PUNCT ennill_VERB gyda_ADP mwy_PRON o_ADP bwyntia_VERB na_CONJ __NOUN dros_ADP y_DET gyfres_NOUN gyfan_ADJ ._PUNCT
Annhebygol_ADJ bod_AUX hwnna_PRON 'n_PART mynd_VERB i_PART gymryd_VERB lle_NOUN gyda_ADP 'r_DET fform_NOUN mae_AUX enwg_VERB yn_PART dangos_VERB ar_ADP hyn_PRON o_ADP bryd_NOUN ond_CONJ mae_VERB 'na_ADV fformiwla_NOUN gydan_VERB ni_PRON sy_AUX 'n_PART cadw_VERB 'r_DET gyfres_NOUN yn_PART fyw_VERB tan_ADP y_DET cymal_NOUN ola'_ADJUNCT ._PUNCT
Cysondeb_NOUN enwb_NOUN cyfenw_NOUN sy_AUX 'n_PART cadw_VERB hi_PRON ar_ADP y_DET brig_ADJ y_DET tabl_NOUN ar_ADP hyn_PRON o_ADP bryd_NOUN ._PUNCT
Dyma_ADV ail_ADJ ras_NOUN enwb_NOUN cyfenw_NOUN 'ma_ADV heddiw_ADV yn_PART __NOUN ond_CONJ gall_VERB hi_PRON ennill_VERB yma_ADV heddiw_ADV a_CONJ 'r_DET ddwy_NUM ola_ADP '_PUNCT lawr_ADV  _SPACE __NOUN __NOUN __NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN a_CONJ gall_VERB hi_PRON gipio_VERB 'r_DET gyfres_NOUN Okay_PROPN so_ADV yn_PART syml_ADJ [_PUNCT -_PUNCT ]_PUNCT y_DET pedair_NUM ras_NOUN o_ADP chwech_NUM sy_AUX 'n_PART cyfri_VERB '_PUNCT so_ADV ma_PRON '_PUNCT 'n_PART bwysig_ADJ iawn_ADV bod_AUX pob_DET athletwr_NOUN yn_PART mynd_VERB yn_PART gyflym_ADJ â_ADP sy_VERB phosib_NOUN [_PUNCT -_PUNCT ]_PUNCT o_ADP 'r_DET gwn_NOUN i_ADP 'r_DET tâp_NOUN olaf_ADJ ._PUNCT
602.059_PUNCT
Efalle_ADV '_PUNCT taw_ADP 'r_DET ras_NOUN perffeth_NOUN olympaidd_ADJ sy_AUX 'n_PART denu_VERB 'r_DET athletwyr_NOUN elît_NOUN yma_DET i_ADP lannau_NOUN Llyn_X enw_NOUN ._PUNCT
Ond_CONJ mae_VERB Gŵyl_NOUN enw_NOUN lleoliad_NOUN yn_PART llawer_ADV mwy_ADJ na_CONJ hynny_PRON ._PUNCT
Ymhlith_NOUN y_DET rasus_NOUN dydd_NOUN Sadwrn_PROPN oedd_VERB ras_NOUN treiathlon_VERB yng_ADP nghyfres_NOUN gemau_NOUN Cymru_PROPN sefydliad_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X 'r_DET sefydliad_NOUN yn_ADP gefnogwr_NOUN pwysig_ADJ i_ADP chwaraeon_NOUN o_ADP bob_DET math_NOUN a_CONJ 'i_PRON gweithgaredde_VERB yn_PART feithrinfa_NOUN bwysig_ADJ i_PART ddatblygu_VERB y_DET to_NOUN nesaf_ADJ o_ADP bencampwyr_NOUN mewn_ADP cym'ent_NOUN o_ADP gampe_NOUN ._PUNCT
Sefydlwyd_VERB gemau_NOUN Cymru_PROPN yn_PART ôl_NOUN yn_PART nwy_NOUN fil_NUM ac_CONJ un_NUM ar_ADP ddeg_NOUN ac_CONJ  _SPACE taw_ADP lleoliad_NOUN yw_VERB 'r_DET cartre_NOUN 'r_DET gemau_NOUN i_ADP 'r_DET mwyafrif_NOUN o_ADP 'r_DET campe_NOUN [_PUNCT -_PUNCT ]_PUNCT addas_ADJ taw_ADP yma_ADV [_PUNCT -_PUNCT ]_PUNCT dafliad_NOUN carreg_NOUN o_ADP wersyll_NOUN awyr_NOUN agored_VERB sefydliad_NOUN [_PUNCT -_PUNCT ]_PUNCT __NOUN [_PUNCT -_PUNCT ]_PUNCT mae_VERB 'r_DET ras_NOUN treiathlon_AUX yn_PART cael_VERB ei_DET chynnal_VERB ._PUNCT
'_PUNCT Dan_VERB ni_PRON yn_PART lleoliad_NOUN am_ADP y_DET tro_NOUN cynta'_ADJUNCT '_PUNCT leni_VERB yn_PART cynnig_ADV treiathlon_VERB ddoe_ADV ag_CONJ  _SPACE nofio_VERB agored_VERB heddiw_ADV ._PUNCT
Mae_VERB gemau_NOUN Cymru_PROPN yn_PART ei_PRON wythfed_VERB blwyddyn_NOUN rŵan_ADV ac_CONJ mae_AUX niferoedd_NOUN o_ADP 'r_DET cystadlaethau_NOUN lawr_ADV yng_ADP lleoliad_NOUN ag_ADP ochrau_NOUN lleoliad_NOUN ._PUNCT
So_PART mae_VERB 'n_PART braf_ADJ cael_VERB gweld_VERB a_CONJ ymestyn_VERB cystadlaethau_VERB i_ADP fyny_ADV i_ADP gogledd_NOUN Cymru_PROPN i_ADP safle_NOUN mor_ADV wych_ADJ â_ADP Llyn_NOUN enw_NOUN ._PUNCT
Ma_INTJ '_PUNCT 'n_PART rhoi_VERB cyfle_NOUN iddyn_ADP nhw_PRON arddangos_VERB eu_DET sgiliau_NOUN ar_ADP lefel_NOUN genedlaethol_ADJ a_CONJ '_PUNCT dyn_NOUN gobeithio_VERB yn_ADP y_DET dyfodol_NOUN symyd_VERB ymlaen_ADV i_PART cynrychioli_VERB Cymru_PROPN yn_PART gemau_NOUN 'r_DET Gymanwlad_PROPN [_PUNCT -_PUNCT ]_PUNCT ydi_VERB 'r_DET gam_NOUN nesa'_ADJUNCT o_ADP fa'_NOUNUNCT 'ma_ADV ._PUNCT
ym_ADP mama_NOUN fe_PRON 'n_PART dda_ADJ bod_VERB fel_ADP [_PUNCT -_PUNCT ]_PUNCT bod_AUX nhw_PRON 'n_PART trio_ADV ca'l_VERB mwy_PRON o_ADP plant_NOUN fewn_ADP i_PRON treiathlon_VERB achos_CONJ mae_VERB fel_ADP [_PUNCT -_PUNCT ]_PUNCT os_CONJ chi_PRON 'n_PART hoffi_VERB nofio_VERB a_CONJ beicio_NOUN '_PUNCT fyd_NOUN [_PUNCT -_PUNCT ]_PUNCT a_CONJ mae_VERB fe_PRON 'n_PART [_PUNCT =_SYM ]_PUNCT berffeth_NOUN [_PUNCT /=_PROPN ]_PUNCT berffeth_NOUN i_ADP chi_PRON ._PUNCT
Ar_ADP bnawn_ADJ dydd_NOUN Sadwrn_PROPN hefyd_ADV mi_PRON oedd_VERB 'na_ADV ras_NOUN newydd_ADJ sbon_ADV ._PUNCT
Ras_ADV gyfnewid_VERB super_ADP sbrint_NOUN i_ADP dime_NOUN ._PUNCT
Mae_VERB '_PUNCT dan_ADP ni_PRON time_NOUN o_ADP 'r_DET gogledd_NOUN a_CONJ 'r_DET de_NOUN yn_PART mynd_VERB yn_PART erbyn_ADP ei_DET gilydd_NOUN ._PUNCT
Mae_VERB fath_NOUN â_ADP state_NOUN of_X origin_X fel_ADP petai_VERB [_PUNCT =_SYM ]_PUNCT o_ADP [_PUNCT /=_PROPN ]_PUNCT o_ADP treiathlon_VERB ._PUNCT
'_PUNCT Sa_PROPN i_PRON 'n_PART bod_VERB [_PUNCT -_PUNCT ]_PUNCT gawn_VERB ni_PRON weld_VERB siw_NOUN {_PROPN \_ADP }_NOUN d_PART mae_AUX hwnna_PRON 'n_PART mynd_VERB ._PUNCT
Gobeithio_VERB '_PUNCT nawr_ADV gallwn_VERB dyfu_VERB o_ADP nerth_NOUN i_ADP nerth_NOUN a_CONJ '_PUNCT falle_NOUN '_PUNCT gawn_VERB ni_PRON cyrff_NOUN yyy'ill_VERB [_PUNCT =_SYM ]_PUNCT o_ADP 'r_DET [_NOUN /=_PROPN ]_PUNCT o_ADP 'r_DET byd_NOUN chwaraeon_PUNCT yn_PART dod_VERB lan_ADV yma_ADV i_PART gefnogi_VERB ni_PRON a_CONJ neud_VERB yr_DET gŵyl_NOUN yn_PART fyth_NOUN to_NOUN ._PUNCT
Ac_CONJ yna_ADV ar_ADP bnawn_NOUN Sul_PROPN [_PUNCT -_PUNCT ]_PUNCT ras_NOUN nofio_VERB dŵr_NOUN agored_ADV ddenodd_VERB rai_PRON o_ADP nofwyr_NOUN gorau_ADJ Cymru_PROPN a_CONJ thu_NOUN hwnt_ADV i_PART fwynhau_VERB dyfroedd_NOUN croyw_ADJ Llyn_DET enw_NOUN ._PUNCT
I_ADP gweld_VERB rh'wbath_NOUN fel_ADP hyn_PRON  _SPACE yma_DET yn_PART gogledd_NOUN Cymru_PROPN [_PUNCT -_PUNCT ]_PUNCT yn_PART jest_ADV dangos_VERB y_DET petha'_NOUNUNCT dan_ADP ni_PRON 'n_PART gallu_ADV neud_VERB fan_NOUN hyn_DET hefo_ADP 'r_DET nofio_NOUN [_PUNCT -_PUNCT ]_PUNCT hefo_ADP 'r_DET beicio_NOUN [_PUNCT -_PUNCT ]_PUNCT hefo_ADP rhedeg_VERB ._PUNCT
Ia_INTJ ._PUNCT
Mae_AUX 'n_PART brilliant_VERB ._PUNCT
Yn_PART sicr_ADJ '_PUNCT dan_ADP ni_PRON 'di_PART gweld_VERB treiathlon_VERB [_PUNCT -_PUNCT ]_PUNCT y_DET niferoedd_NOUN yn_PART codi_VERB o_ADP flwyddyn_NOUN i_ADP flwyddyn_NOUN ._PUNCT
A_PART gobeithio_VERB fydd_VERB gemau_NOUN Cymru_PROPN yn_PART tyfu_VERB blwyddyn_NOUN nesa'_ADJUNCT hefyd_ADV ._PUNCT
1129.782_NOUN
Wel_ADP ras_NOUN am_ADP yr_DET ail_ADJ safle_NOUN ydi_VERB hi_PRON rhwng_ADP enwg_VERB cyfenw_NOUN ac_CONJ __NOUN __NOUN __NOUN __NOUN ._PUNCT
A_CONJ cyfenw_NOUN yn_PART mynd_VERB heibio_ADV i_ADP cyfenw_NOUN ond'd_NOUN oes_VERB 'na_ADV 'r_DET un_NUM o_ADP 'r_DET ddau_NUM yn_PART mynd_VERB i_ADP ddal_VERB y_DET gŵr_NOUN sydd_VERB ar_ADP y_DET blaen_NOUN __NOUN __NOUN __NOUN __NOUN yn_PART cyrraedd_VERB y_DET llinell_NOUN derfyn_NOUN ac_CONJ yn_PART ennill_VERB am_ADP y_DET pedwerydd_NOUN dro_NOUN yn_PART olynol_ADJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN sydd_AUX yn_PART gorffen_VERB yn_ADP yr_DET ail_ADJ safle_NOUN ag_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN [_PUNCT -_PUNCT ]_PUNCT ar_ADP ôl_NOUN cymal_NOUN trychinebus_ADJ yn_ADP y_DET nofio_VERB [_PUNCT -_PUNCT ]_PUNCT y_DET fo_PRON sy_AUX 'n_PART gorffen_VERB yn_PART drydydd_ADJ ._PUNCT
Ond'd_NOUN oes_VERB 'na_ADV ddim_PART amheuaeth_VERB mai_PART brenin_NOUN y_DET treiathlon_VERB yng_ADP Nghymru_PROPN ar_ADP hyn_PRON o_ADP bryd_NOUN ydi_ADV enwg_VERB cyfenw_NOUN ._PUNCT
1442.987_VERB
