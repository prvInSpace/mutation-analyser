"_PUNCT Hawliau_NOUN traws_ADJ /_PUNCT Trans_NOUN rights_X "_PUNCT ,_PUNCT "_PUNCT Newyddion_X diweddara'_VERBUNCT gan_ADP Plaid_PROPN Ifanc_PROPN
Annwyl_NOUN gymrawd_NOUN ,_PUNCT
Dyma_ADV ddiweddariad_NOUN byr_ADJ :_PUNCT
YSGOL_PROPN HAF_X PLAID_NOUN CYMRU_PROPN
Hoffwn_VERB dy_DET atgoffa_VERB y_PART bydd_AUX Ysgol_NOUN Haf_NOUN Plaid_NOUN Cymru_PROPN 'n_PART cyrmyd_VERB lle_NOUN rhwng_ADP '_PUNCT fory_ADV a_CONJ dydd_NOUN Sul_PROPN ,_PUNCT yn_ADP Galeri_PROPN Caernarfon_PROPN ._PUNCT
Bydd_VERB y_DET rali_NOUN annibyniaeth_NOUN yn_PART digwydd_VERB ddydd_NOUN Sadwrn_PROPN ,_PUNCT ac_CONJ yn_PART rhan_NOUN o_ADP amserlen_NOUN yr_DET Ysgol_NOUN Haf_NOUN ._PUNCT
*_SYM 1_NUM pm_X '_PUNCT fory_ADV -_PUNCT 1_NUM pm_ADP dydd_NOUN Sul_PROPN ._PUNCT
HAWLIAU_NOUN POBL_PROPN TRAWSRYWEDDOL_PROPN
Fel_ADP rhan_NOUN o_ADP 'r_DET Ysgol_NOUN Haf_NOUN ,_PUNCT bydd_AUX Plaid_PROPN Ifanc_PROPN yn_PART cynnal_VERB sesiwn_NOUN ar_ADP y_DET cyd_NOUN gyda_ADP Plaid_PROPN Pride_NOUN ,_PUNCT ble_ADV fyddwn_AUX yn_PART ysgrifennu_VERB llythyrau_NOUN yn_PART cefnogi_VERB hawliau_NOUN traws_ADJ ac_CONJ yn_PART trafod_VERB bebe_NOUN mae_AUX Pride_NOUN yn_PART ei_DET olygu_VERB i_ADP ni_PRON ._PUNCT
Mae_VERB 'n_PART hollbywsig_ADJ ein_PRON bod_VERB fel_ADP mudiad_NOUN yn_PART cyd-_ADJ sefyll_VERB gyda_ADP 'r_DET gymuned_NOUN draws_ADJ sydd_VERB wastad_ADV dan_ADP warchae_NOUN ._PUNCT
*_SYM 5_NUM pm_X '_PUNCT fory_ADV ,_PUNCT Stiwdio_VERB 1_NUM Galeri_PROPN Caernarfon_PROPN ._PUNCT
Gobeithiwn_VERB y_PART welwn_VERB ni_PRON di_PRON yn_PART ystod_NOUN y_DET penwythnos_NOUN !_PUNCT
Dros_ADP Gymru_PROPN rydd_NOUN ,_PUNCT gynhwysol_ADJ ac_CONJ eco_NOUN -_PUNCT sosialaidd_ADJ ,_PUNCT
Plaid_NOUN Ifanc_PROPN ._PUNCT
