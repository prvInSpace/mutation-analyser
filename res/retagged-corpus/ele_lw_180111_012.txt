enwg_VERB
Gigiau_PROPN
Newyddion_PROPN
Cerddoriaeth_NOUN
Amdanom_PROPN
Y_DET Grŵp_PROPN
Oriel_PROPN
Ardal_NOUN y_DET Wasg_X
Cymraeg_NOUN
Chwaraeodd_VERB enw_NOUN cyfenw_NOUN Baban_PROPN __NOUN ar_ADP Ddydd_NOUN Gŵyl_PROPN __NOUN __NOUN __NOUN ar_ADP The_X Folk_X Show_X ,_PUNCT BBC_PROPN Radio_VERB 2_NUM ._PUNCT
Mae_AUX 'r_DET trac_NOUN yn_PART cychwyn_VERB 47.45_NUM munud_NOUN i_ADP fewn_ADP i_ADP 'r_DET rhaglen_NOUN ._PUNCT
"_PUNCT A_CONJ really_ADV serene_NOUN sound_VERB "_PUNCT enw_NOUN cyfenw_NOUN ._PUNCT
enw_NOUN
Cartref_PROPN
Gigs_ADJ
Newyddion_PROPN
Cerddoriaeth_NOUN
Am_ADP
Oriel_PROPN
Ardal_NOUN y_DET Wasg_X
e_PRON -_PUNCT bost_NOUN
enw_NOUN enw_NOUN
cyfenw_NOUN Lansio_PROPN Aur_PROPN |_SYM
cyfenw_NOUN Lansio_PROPN Aur_PROPN
Byddem_VERB yn_PART lansio_VERB 'r_DET albwm_NOUN newydd_ADJ yn_PART swyddogol_ADJ ar_ADP nos_NOUN Wener_NOUN dyddiad_NOUN ,_PUNCT yn_ADP Old_PROPN lle_NOUN Library_PROPN ,_PUNCT __NOUN Road_NOUN ,_PUNCT __NOUN __NOUN __NOUN ._PUNCT
Mae_VERB 'r_DET digwyddiad_NOUN yn_PART rhydd_ADJ ac_CONJ am_ADP ddim_PRON gyda_ADP 'r_DET drysau_NOUN yn_PART agor_VERB am_ADP 7.30_PRON yh_VERB ._PUNCT
Byddem_VERB yn_PART perfformio_VERB alawon_VERB o_ADP 'r_DET albwm_NOUN ,_PUNCT sydd_AUX yn_PART mynd_VERB i_PART fod_VERB ar_ADP gael_VERB i_ADP brynu_VERB am_ADP y_DET pris_NOUN arbennig_ADJ o_ADP £_SYM 10.00_NUM ._PUNCT
Ewch_VERB i_ADP 'r_DET dolenni_NOUN isod_ADV am_ADP ragor_NOUN o_ADP wybodaeth_NOUN a_CONJ sut_ADV i_ADP sicrhau_VERB eich_DET lle_NOUN yn_ADP y_DET digwyddiad_NOUN ._PUNCT
Digwyddiad_NOUN Facebook_PROPN
EventBrite_VERB Tocyn_PROPN am_ADP ddim_PRON
Olion_NOUN Byw_VERB |_PUNCT
Olion_NOUN Byw_PROPN
Newyddion_NOUN :_PUNCT Bydd_VERB y_DET deuawd_NOUN arbennig_ADJ go_ADV dda_ADJ ,_PUNCT Olion_PROPN Byw_PROPN ,_PUNCT yn_PART ymuno_VERB gyda_ADP ni_PRON yn_ADP ein_DET cyfenw_NOUN lawnsio_VERB Aur_PROPN -_PUNCT "_PUNCT The_X intoxicating_NOUN vocal_NOUN and_CONJ intense_VERB fiddle_NOUN of_X enwb_NOUN __NOUN and_ADJ the_X hypnotic_NOUN guitar_NOUN /_PUNCT mandolin_NOUN of_X Dan_X __NOUN __NOUN __NOUN ._PUNCT
Together_NOUN they_NOUN present_NOUN traditional_NOUN cyfenw_NOUN songs_NOUN and_X melodies_VERB with_NOUN a_CONJ beguiling_ADJ simplicity_NOUN of_X arrangement_X ._PUNCT "_PUNCT enw_NOUN
Datagniad_NOUN y_DET Wasg_NOUN -_PUNCT Aur_PROPN |_SYM
Datagniad_NOUN y_DET Wasg_X -_PUNCT Aur_PROPN
Cist_VERB drysor_NOUN o_ADP gerddoriaeth_NOUN newydd_ADJ a_CONJ wnaed_NOUN o_ADP alawon_NOUN hynafol_ADJ ,_PUNCT mae_VERB Carreg_PROPN Lafar_PROPN yn_PART fand_NOUN sy_AUX 'n_PART torri_VERB 'r_DET ffiniau_NOUN hunaniaeth_NOUN cerddorol_ADJ Cymreig_ADJ ._PUNCT
Gyda_ADP 'u_PRON halbwm_NOUN newydd_ADJ Aur_PROPN ,_PUNCT mae_AUX Carreg_PROPN Lafar_PROPN yn_PART amlygu_VERB hen_ADJ draddodiadau_NOUN ac_CONJ ail-_ADV ddychmygu_VERB nhw_PRON ar_ADP gyfer_NOUN y_DET 21_NUM ain_NOUN ganrif_NOUN ._PUNCT
Mae_VERB pibau_NOUN ,_PUNCT ffidil_NOUN ,_PUNCT ffliwt_NOUN ac_CONJ alawon_VERB hynafol_ADJ yn_PART cael_VERB eu_PRON cymysgu_VERB gyda_ADP lleisiau_NOUN deinamig_ADJ a_CONJ gitâr_NOUN rhythmig_ADJ ,_PUNCT am_ADP dro_NOUN cyfoes_NOUN ar_ADP gerddoriaeth_NOUN draddodiadol_ADJ ._PUNCT
Darllenwch_VERB yr_DET holl_DET ddatganiad_NOUN fan_NOUN hyn_DET :_PUNCT Datganiad_NOUN gwasg_NOUN Aur_PROPN
Adolygiad_NOUN Folkworld_X |_SYM
Adolygiad_NOUN Folkworld_PROPN
enw_NOUN
Amdanom_VERB |_SYM
Amdanom_PROPN
Ffurfiwyd_VERB Carreg_PROPN Lafar_PROPN yng_ADP Nghaerdydd_PROPN yn_ADP 1993_NUM gydag_ADP Antwn_PROPN ,_PUNCT enw_NOUN ,_PUNCT enwb_NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN Ymunodd_VERB __NOUN __NOUN __NOUN __NOUN __NOUN y_DET __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN yn_PART '_PUNCT 94_NUM i_PART gwblhau_VERB 'r_DET grwp_NOUN ._PUNCT
Yn_PART ystod_NOUN yr_DET haf_NOUN '_PUNCT 95_NUM gwnaethom_NOUN ein_DET halbwm_NOUN cyntaf_ADJ '_PUNCT Ysbryd_NOUN y_DET Werin_NOUN '_PUNCT gyda_ADP Sain_NOUN Records_PROPN ._PUNCT
Cafodd_VERB yr_DET albwm_NOUN ei_PRON rhyddhau_VERB ym_ADP mis_NOUN Tachwedd_NOUN gydag_ADP adolygiadau_NOUN gwych_ADJ gan_ADP y_DET wasg_NOUN gerddoriaeth_NOUN werin_ADJ yn_ADP y_DET DU_PROPN a_CONJ Gogledd_NOUN America_PROPN ._PUNCT
Yn_PART ystod_NOUN yr_DET haf_NOUN 1996_NUM wnaethon_VERB ni_PRON perfformio_VERB yn_ADP ein_DET gŵyl_NOUN fawr_ADJ gyntaf_ADJ yn_ADP Lorient_PROPN ,_PUNCT Llydaw_PROPN ,_PUNCT yn_PART cynrychioli_VERB Cymru_PROPN yn_ADP yr_DET Ŵyl_NOUN Ryng_PROPN -_PUNCT Geltaidd_ADJ ._PUNCT
Roed_VERB hi_PRON 'n_PART ddeg_NOUN diwrnod_NOUN o_ADP berfformiadau_NOUN ,_PUNCT yn_PART diweddu_VERB gyda_ADP 'r_DET cyfenw_NOUN yn_PART cefnogi_VERB 'r_DET cyngerdd_NOUN Dan_ADP Ar_ADP Braz_PROPN '_PUNCT Treftadaeth_PROPN y_DET Celtiaid_PROPN '_PUNCT i_ADP gynulleidfa_NOUN o_ADP ryw_DET 10,000_NUM o_ADP bobl_NOUN ._PUNCT
Yn_ADP 1997_NUM cafodd_VERB '_PUNCT Ysbryd_NOUN y_DET Werin_NOUN '_PUNCT ei_PRON rhyddhau_VERB yng_ADP Ngogledd_NOUN America_PROPN gan_ADP cyfenw_NOUN cyfenw_NOUN Records_PROPN a_CONJ lansiwyd_VERB yr_DET albwm_NOUN gyda_ADP thaith_NOUN dwy_NUM wythnos_NOUN yn_ADP yr_DET __NOUN Daleithiau_NOUN ._PUNCT
Ers_ADP hynny_PRON ,_PUNCT mae_AUX 'r_DET cyfenw_NOUN wedi_PART dychwelyd_VERB i_ADP Ogledd_NOUN America_PROPN am_ADP dair_NUM daith_NOUN arall_ADJ yn_PART 1998_NUM ,_PUNCT 2000_NUM a_CONJ 2001_NUM ,_PUNCT yn_PART ogystal_ADJ â_ADP daith_NOUN i_ADP Atlanta_PROPN ar_ADP gyfer_NOUN yr_DET Ŵyl_NOUN Geltaidd_ADJ yno_ADV yn_PART 1999_NUM a_CONJ 'r_DET Ŵyl_NOUN Celtic_NOUN Colours_PROPN ,_PUNCT Nova_X Scotia_NOUN yn_PART 2009_NUM ._PUNCT
Yn_ADP 199_NUM dyddiad_NOUN hreuon_VERB ni_PRON gwaith_NOUN ar_ADP ein_PRON ail_ADJ albwm_NOUN '_PUNCT Hyn_PRON '_PUNCT a_CONJ gafodd_VERB ei_PRON recordio_VERB yng_ADP Nghaerdydd_PROPN yn_ADP Albany_VERB Studios_PROPN ._PUNCT
Cafodd_VERB yr_DET albwm_NOUN ei_PRON rhyddhau_VERB ar_ADP label_NOUN Sain_PROPN ym_ADP mis_NOUN Awst_PROPN '_PUNCT 98_NUM ac_CONJ hefyd_ADV wedi_PART derbyn_VERB adolygiadau_NOUN gwych_ADJ yn_ADP yr_DET holl_DET cylchgronau_NOUN cerddoriaeth_NOUN werin_ADJ a_CONJ gwreiddiau_NOUN mawr_ADJ yn_ADP y_DET DU_PROPN ac_CONJ ar_ADP draws_ADJ Gogledd_NOUN America_PROPN ._PUNCT
Wnaethom_VERB fideos_NOUN hefyd_ADV ar_ADP gyfer_NOUN dau_NUM trac_NOUN oddi_ADP ar_ADP '_PUNCT Hyn_PRON '_PUNCT a_CONJ gafodd_VERB eu_DET cynnwys_VERB ar_ADP y_DET rhaglen_NOUN celfyddydau_NOUN ,_PUNCT '_PUNCT Sioe_NOUN Gelf_NOUN '_PUNCT ._PUNCT
Ar_ADP ddydd_NOUN enwb_NOUN 1999_NUM ,_PUNCT gadawodd_VERB enw_NOUN y_DET __NOUN er_ADP mwyn_NOUN canolbwyntio_VERB ar_ADP ei_DET waith_NOUN gelf_NOUN weledol_ADJ ac_CONJ ymunodd_VERB __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN fel_ADP gitarydd_NOUN newydd_ADJ ._PUNCT
Yn_PART hwyrach_ADJ yn_ADP y_DET flwyddyn_NOUN honno_PRON aethom_VERB yn_PART ôl_NOUN i_ADP Lorient_PROPN i_ADP gynrychioli_VERB Cymru_PROPN eto_ADV yn_ADP yr_DET hefyd_ADV Ŵyl_NOUN Ryng_PROPN -_PUNCT Geltaidd_ADJ ._PUNCT
Rhyddhawyd_VERB '_PUNCT Hyn_PRON '_PUNCT gan_ADP Marquis_NOUN Classics_PROPN yng_ADP Ngogledd_NOUN America_PROPN ym_ADP dyddiad_NOUN a_CONJ chafodd_VERB yr_DET albwm_NOUN ei_PRON lansio_VERB gyda_ADP thaith_NOUN llwyddiannus_ADJ iawn_ADV dros_ADP tair_NUM wythnos_NOUN yng_ADP Nghanada_NOUN ac_CONJ America_PROPN yn_ADP ystod_NOUN dyddiad_NOUN ._PUNCT
Dechreuodd_VERB y_DET cyfenw_NOUN recordio_VERB ein_DET trydydd_ADJ albwm_NOUN '_PUNCT Profiad_NOUN '_PUNCT ym_ADP mis_NOUN dyddiad_NOUN ,_PUNCT hefyd_ADV yn_ADP Albany_VERB Studios_PROPN ._PUNCT
Mae_AUX 'r_DET albwm_NOUN yn_PART cynnwys_VERB tri_NUM cherddor_NOUN gwadd_ADJ ,_PUNCT enw_NOUN enwg_VERB __NOUN ar_ADP __NOUN __NOUN __NOUN deires_ADJ ,_PUNCT __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ar_ADP piano_NOUN a_CONJ gitâr_NOUN ychwanegol_ADJ a_CONJ Claudine_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ar_ADP Soddgrwth_PROPN ._PUNCT
Cafodd_VERB yr_DET albwm_NOUN ei_PRON rhyddhau_VERB gan_ADP Sain_PROPN ym_ADP mis_NOUN dyddiad_NOUN a_CONJ lawnsio_VERB yn_ADP yr_DET Ŵyl_NOUN Ryng_PROPN -_PUNCT Geltaidd_ADJ Lorient_PROPN '_PUNCT Blwyddyn_NOUN Cymru_PROPN '_PUNCT y_DET flwyddyn_NOUN honno_PRON ._PUNCT
Penderfynodd_VERB enw_NOUN gadael_VERB y_DET cyfenw_NOUN ar_ADP ôl_NOUN Lorient_PROPN ,_PUNCT er_ADP mwyn_NOUN canolbwyntio_VERB ar_ADP ei_DET waith_NOUN ei_DET hun_PRON ._PUNCT
Yn_ADP 2006_NUM ymunodd_VERB Danny_PROPN KilBride_PUNCT fel_ADP gitarydd_NOUN a_CONJ dychwelyd_VERB y_DET cyfenw_NOUN yn_PART ôl_NOUN i_ADP Lorient_PROPN ,_PUNCT hefyd_ADV gyda_ADP Gafin_NOUN enw_NOUN ar_ADP pibgorn_NOUN a_CONJ phibau_NOUN ychwanegol_ADJ ._PUNCT
Rydym_AUX wedi_PART parhau_VERB perfformio_VERB bob_DET blwyddyn_NOUN ,_PUNCT yng_ADP y_DET DU_PROPN ac_CONJ yn_PART rhyngwladol_ADJ ,_PUNCT rhwng_ADP bywydau_NOUN prysur_ADJ teulu_NOUN ac_CONJ ymrwymiadau_NOUN gwaith_NOUN eraill_ADJ ._PUNCT
Wnaethon_VERB ni_PRON recordio_VERB EP_PROPN newydd_ADJ ,_PUNCT Y_DET Cadno_NOUN yn_PART dyddiad_NOUN ,_PUNCT ac_CONJ rydym_VERB yn_ADP y_DET broses_NOUN o_ADP recordio_VERB albwm_NOUN newydd_ADJ ,_PUNCT i_PART gael_VERB eu_PRON rhyddhau_VERB yn_PART 2016_NUM i_PART nodi_VERB ein_PRON 20_NUM fed_VERB pen_NOUN -_PUNCT blwydd_NOUN ,_PUNCT sydd_AUX yn_PART cael_VERB ei_PRON ddilyn_VERB gan_ADP rai_DET perfformiadau_NOUN arbennig_ADJ a_CONJ thaith_NOUN ._PUNCT
Oriel_PROPN |_PUNCT
Oriel_PROPN
Lluniau_NOUN Byw_PROPN -_PUNCT enwb_VERB enw_NOUN /_PUNCT Lluniau_NOUN Ffurfiol_ADJ -_PUNCT __NOUN __NOUN __NOUN __NOUN
Gigs_NOUN |_SYM
Gigs_ADJ
Dim_DET Gigs_NOUN ar_ADP hyn_PRON o_ADP bryd_NOUN
Hen_ADJ Gigs_ADJ
dyddiad_NOUN cyfenw_NOUN Lansio_PROPN Aur_PROPN
dyddiad_NOUN lle_NOUN Tavern_PROPN
dyddiad_NOUN Gŵyl_NOUN Lorient_PROPN
dyddiad_NOUN Theatr_NOUN lle_NOUN
dyddiad_NOUN lle_NOUN
dyddiad_NOUN Tafwyl_PROPN 2016_NUM
dyddiad_NOUN Cwlwm_NOUN Celtaidd_ADJ Festival_PROPN ,_PUNCT lle_ADV
dyddiad_NOUN Cwpwrdd_NOUN Nansi_PROPN ,_PUNCT Gwdihw_PROPN
Cerddoriaeth_NOUN |_SYM
Cerddoriaeth_NOUN
AUR_PROPN
Ein_DET halbwm_NOUN newydd_ADJ ,_PUNCT wedi_PART 'u_PRON recordio_VERB yn_PART Stiwdio_PROPN Felin_PROPN Fach_X yn_PART 2016_NUM ac_CONJ yn_PART rhyddhawyd_VERB gan_ADP Recordiadau_PROPN Sain_PROPN ._PUNCT
£_SYM 12.00_NUM (_PUNCT inc_NOUN P&amp;P_PROPN )_PUNCT UK_X Only_X :_PUNCT
-_PUNCT
prynu_VERB o_ADP itunes_NOUN
Prynu_VERB o_ADP Sain_PROPN
PRYNU_NOUN O_ADP 'R_DET SIOP_NOUN
Profiad_NOUN
£_SYM 12.00_NUM (_PUNCT inc_NOUN P&amp;P_PROPN )_PUNCT UK_X Only_X :_PUNCT
HYN_PROPN
YSBRYD_VERB Y_DET WERIN_PROPN
Newyddion_NOUN |_PUNCT
Newyddion_PROPN
Datagniad_NOUN y_DET Wasg_X -_PUNCT Aur_PROPN Thursday_X dyddiad_NOUN
Cist_VERB drysor_NOUN o_ADP gerddoriaeth_NOUN newydd_ADJ a_CONJ wnaed_NOUN o_ADP alawon_NOUN hynafol_ADJ ,_PUNCT mae_VERB Carreg_PROPN Lafar_PROPN yn_PART fand_NOUN sy_AUX 'n_PART torri_VERB 'r_DET ffiniau_NOUN hunaniaeth_NOUN cerddorol_ADJ Cymreig_ADJ ._PUNCT
Gyda_ADP 'u_PRON halbwm_NOUN newydd_ADJ Aur_PROPN ,_PUNCT mae_AUX Carreg_PROPN Lafar_PROPN yn_PART amlygu_VERB hen_ADJ draddodiadau_NOUN ac_CONJ ail-_ADV ddychmygu_VERB nhw_PRON ar_ADP gyfer_NOUN y_DET 21_NUM ain_NOUN ganrif_NOUN ._PUNCT
Mae_VERB pibau_NOUN ,_PUNCT ffidil_NOUN ,_PUNCT ffliwt_NOUN ac_CONJ alawon_VERB hynafol_ADJ yn_PART cael_VERB eu_PRON cymysgu_VERB gyda_ADP lleisiau_NOUN deinamig_ADJ a_CONJ [_PUNCT …_PUNCT ]_PUNCT
Olion_NOUN Byw_VERB cyfenw_NOUN dyddiad_NOUN
Newyddion_NOUN :_PUNCT Bydd_VERB y_DET deuawd_NOUN arbennig_ADJ go_ADV dda_ADJ ,_PUNCT Olion_PROPN Byw_PROPN ,_PUNCT yn_PART ymuno_VERB gyda_ADP ni_PRON yn_ADP ein_DET cyfenw_NOUN lawnsio_VERB Aur_PROPN -_PUNCT "_PUNCT The_X intoxicating_NOUN vocal_NOUN and_CONJ intense_VERB fiddle_NOUN of_X enwb_NOUN __NOUN and_ADJ the_X hypnotic_NOUN guitar_NOUN /_PUNCT mandolin_NOUN of_X Dan_X __NOUN __NOUN __NOUN ._PUNCT
Together_NOUN they_NOUN present_NOUN traditional_NOUN cyfenw_NOUN songs_NOUN and_X melodies_VERB with_NOUN a_CONJ beguiling_ADJ simplicity_NOUN of_X arrangement_X ._PUNCT "_PUNCT enw_NOUN
cyfenw_NOUN Lansio_PROPN Aur_PROPN Tuesday_PROPN dyddiad_NOUN
Byddem_VERB yn_PART lansio_VERB 'r_DET albwm_NOUN newydd_ADJ yn_PART swyddogol_ADJ ar_ADP nos_NOUN Wener_NOUN dyddiad_NOUN ,_PUNCT yn_ADP Old_PROPN lle_NOUN Library_PROPN ,_PUNCT __NOUN Road_NOUN ,_PUNCT __NOUN __NOUN __NOUN ._PUNCT
Mae_VERB 'r_DET digwyddiad_NOUN yn_PART rhydd_ADJ ac_CONJ am_ADP ddim_PRON gyda_ADP 'r_DET drysau_NOUN yn_PART agor_VERB am_ADP 7.30_PRON yh_VERB ._PUNCT
Byddem_VERB yn_PART perfformio_VERB alawon_VERB o_ADP 'r_DET albwm_NOUN ,_PUNCT sydd_AUX yn_PART mynd_VERB i_PART fod_VERB ar_ADP gael_VERB i_ADP brynu_VERB am_ADP y_DET pris_NOUN arbennig_ADJ o_ADP £_SYM 10.00_NUM ._PUNCT
Ewch_VERB [_PUNCT …_PUNCT ]_PUNCT
Chwaraeodd_VERB enw_NOUN cyfenw_NOUN Baban_PROPN __NOUN ar_ADP Ddydd_NOUN Gŵyl_PROPN __NOUN __NOUN __NOUN ar_ADP The_X Folk_X Show_X ,_PUNCT BBC_PROPN Radio_VERB 2_NUM ._PUNCT
Mae_AUX 'r_DET trac_NOUN yn_PART cychwyn_VERB 47.45_NUM munud_NOUN i_ADP fewn_ADP i_ADP 'r_DET rhaglen_NOUN ._PUNCT
"_PUNCT A_CONJ really_ADV serene_NOUN sound_VERB "_PUNCT enw_NOUN cyfenw_NOUN ._PUNCT
enw_NOUN
Lluniau_NOUN Datrys_PROPN Uchel_PROPN (_PUNCT Cliciwch_VERB i_ADP weld_VERB y_DET delwedd_NOUN res_NOUN llawn_ADJ )_PUNCT
Bywgraffiad_NOUN bach_ADJ
Cliciwch_VERB yma_ADV i_PART weld_VERB mwy_PRON o_ADP Adolygiadau_NOUN
Cliciwch_VERB yma_ADV i_PART weld_VERB ein_DET Cynllun_NOUN Llwyfan_PROPN
Y_DET Grŵp_NOUN |_SYM
Y_DET Grŵp_PROPN
enw_NOUN enw_NOUN
Danny_VERB Kilbride_PROPN
"_PUNCT Dyma_ADV be_PRON ydi_VERB cerddoriaeth_NOUN Geltaidd_ADJ go_ADV iawn_ADJ "_PUNCT (_PUNCT Golwg_PROPN )_PUNCT
NEWYDDION_PROPN
Datagniad_NOUN y_DET Wasg_X -_PUNCT Aur_PROPN Thursday_X dyddiad_NOUN
Cist_VERB drysor_NOUN o_ADP gerddoriaeth_NOUN newydd_ADJ a_CONJ wnaed_NOUN o_ADP alawon_NOUN hynafol_ADJ ,_PUNCT mae_VERB Carreg_PROPN Lafar_PROPN yn_PART fand_NOUN sy_AUX 'n_PART torri_VERB 'r_DET ffiniau_NOUN hunaniaeth_NOUN cerddorol_ADJ Cymreig_ADJ ._PUNCT
Gyda_ADP 'u_PRON halbwm_NOUN newydd_ADJ Aur_PROPN ,_PUNCT mae_AUX Carreg_PROPN Lafar_PROPN yn_PART amlygu_VERB …_PUNCT
Olion_NOUN Byw_VERB cyfenw_NOUN dyddiad_NOUN
Newyddion_NOUN :_PUNCT Bydd_VERB y_DET deuawd_NOUN arbennig_ADJ go_ADV dda_ADJ ,_PUNCT Olion_PROPN Byw_PROPN ,_PUNCT yn_PART ymuno_VERB gyda_ADP ni_PRON yn_ADP ein_DET cyfenw_NOUN lawnsio_VERB Aur_PROPN -_PUNCT "_PUNCT The_X intoxicating_NOUN vocal_NOUN and_CONJ intense_VERB fiddle_NOUN of_X enwb_NOUN __NOUN and_X the_X …_PUNCT
cyfenw_NOUN Lansio_PROPN Aur_PROPN Tuesday_PROPN dyddiad_NOUN
Byddem_VERB yn_PART lansio_VERB 'r_DET albwm_NOUN newydd_ADJ yn_PART swyddogol_ADJ ar_ADP nos_NOUN Wener_NOUN dyddiad_NOUN ,_PUNCT yn_ADP Old_PROPN lle_NOUN Library_PROPN ,_PUNCT __NOUN Road_NOUN ,_PUNCT __NOUN __NOUN __NOUN ._PUNCT
Mae_VERB 'r_DET digwyddiad_NOUN yn_PART rhydd_ADJ ac_CONJ am_ADP ddim_PRON gyda_ADP 'r_DET drysau_NOUN yn_PART …_PUNCT
Chwaraeodd_VERB enw_NOUN cyfenw_NOUN Baban_PROPN __NOUN ar_ADP Ddydd_NOUN Gŵyl_PROPN __NOUN __NOUN __NOUN ar_ADP The_X Folk_X Show_X ,_PUNCT BBC_PROPN Radio_VERB 2_NUM ._PUNCT
Mae_AUX 'r_DET trac_NOUN yn_PART cychwyn_VERB 47.45_NUM munud_NOUN i_ADP fewn_ADP i_ADP 'r_DET rhaglen_NOUN ._PUNCT
"_PUNCT A_CONJ really_ADV serene_NOUN sound_VERB "_PUNCT …_PUNCT
Mwy_ADJ
Gigs_ADJ
Dim_DET Gigs_NOUN Ar_ADP Hyn_PRON O_ADP Bryd_PROPN
