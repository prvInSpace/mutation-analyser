enwb_VERB ydw_VERB i_PRON ._PUNCT
Beth_PRON yw_VERB [_PUNCT saib_NOUN ]_PUNCT dy_DET enw_NOUN di_PRON ?_PUNCT
O_ADP enwb_NOUN ._PUNCT
Sorri_VERB bobo_VERB fi_PRON 'n_PART hwyr_ADJ I_ADP thought_NOUN they'd_ADJ be_ADP here_VERB by_VERB now_PRON ._PUNCT
No_NOUN English_NOUN enwb_NOUN ._PUNCT
Dim_DET Saesneg_PROPN ._PUNCT
Dw_VERB i_PRON angen_NOUN ymarfer_NOUN ._PUNCT
Heddiw_ADV dw_AUX i_PRON 'n_PART siarad_VERB Gymraeg_PROPN ._PUNCT
Dw_VERB i_PRON angen_NOUN gwneud_VERB argraff_NOUN dda_ADJ ._PUNCT
Were_VERB you_X  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Wyt_VERB ti_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
enwb_VERB ._PUNCT
Mae_AUX 'r_DET border_NOUN agency_NOUN yn_PART dod_VERB I_ADP need_VERB all_ADP help_NOUN I_ADP can_VERB get_NOUN ._PUNCT
134.401_PROPN
Reit_ADV cer_VERB di_PRON nôl_NOUN i_ADP 'r_DET gwely_NOUN a_CONJ fi_PRON [_PUNCT aneglur_ADJ ]_PUNCT e_DET mas_NOUN nawr_ADV ._PUNCT
[_PUNCT Clywir_NOUN baban_NOUN yn_PART crio_VERB ]_PUNCT ._PUNCT
Na_INTJ ._PUNCT
So_ADP ti_PRON i_ADP fod_VERB i_ADP off_ADV wneud_VERB duty_NOUN ._PUNCT
Hei_INTJ ._PUNCT
Wedodd_VERB [_PUNCT =_SYM ]_PUNCT y_DET y_DET [_PUNCT /=_PROPN ]_PUNCT y_PART ffisio_VERB bod_VERB e_PRON 'n_PART bwysig_ADJ i_ADP fi_PRON gadw_VERB 'n_PART heini_ADJ on'd_ADJ do_ADP fe_PRON ?_PUNCT
Galla_VERB i_PART wneud_VERB y_DET waciau_NOUN yma_ADV ta_CONJ beth_PRON ._PUNCT
Hei_INTJ hei_PRON hei_X hei_X hei_X ._PUNCT
Dere_VERB nawr_ADV C'mon_NOUN ._PUNCT
O_ADP 'n_PART ni_PRON 'n_PART dau_NUM 'n_PART iawn_ADJ Okay_X ?_PUNCT
Fi_PRON 'n_PART gwybod_VERB oeddech_VERB chi_PRON ._PUNCT
Ti_PRON yw_VERB ei_DET dad_NOUN e_PRON ._PUNCT
Hmm_INTJ ._PUNCT
Reit_VERB de_NOUN mwnci_NOUN bach_ADJ ._PUNCT
'_PUNCT Dyn_NOUN ni_PRON 'n_PART mynd_VERB i_PART cael_VERB gweld_VERB os_CONJ all_VERB dad_NOUN gael_VERB ti_PRON i_ADP ddysgu_VERB ife_ADV ?_PUNCT
[_PUNCT Dechreua_VERB S_NUM 3_NUM ganu_VERB Gee_PROPN Geffyl_PROPN Bach_ADJ ]_PUNCT ._PUNCT
190.031_NOUN
Annwyl_ADJ enwg_VERB ._PUNCT
Os_CONJ wyt_AUX ti_PRON 'n_PART darllen_VERB hwn_DET [_PUNCT saib_NOUN ]_PUNCT yna_ADV mae_AUX 'n_PART golygu_VERB bod_VERB yr_DET hyn_PRON o_ADP 'n_PART i_PRON 'n_PART ofni_VERB fyddai_AUX 'n_PART digwydd_VERB [_PUNCT saib_NOUN ]_PUNCT wedi_PART digwydd_VERB ._PUNCT
322.632_NOUN
Hei_PRON enwb_NOUN ._PUNCT
Fi_PRON 'n_PART okay_VERB ._PUNCT
Sa_NOUN i_PRON 'n_PART siŵr_ADJ am_ADP heddi_NOUN ._PUNCT
Fi_PRON 'n_PART dal_ADV i_ADP botsio_VERB 'r_DET stori_NOUN fer_ADJ ar_ADP gyfer_NOUN y_DET gystadleuaeth_NOUN t'mod_VERB [_PUNCT ocheneidio_VERB ]_PUNCT ._PUNCT
[_PUNCT Clywir_NOUN ffôn_NOUN yn_PART derbyn_VERB neges_NOUN destun_NOUN ]_PUNCT ._PUNCT
Paid_VERB bod_VERB fel_ADP 'na_ADV ._PUNCT
Wnewn_ADP ni_PRON rywbeth_NOUN yn_PART glou_ADJ ._PUNCT
Wi_AUX 'n_PART addo_VERB okay_VERB ?_PUNCT
Okay_VERB tara_PRON ._PUNCT
Tara_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
406.9_NOUN
Wi_VERB ddim_PRON yn_PART dy_DET feio_VERB di_PRON ._PUNCT
Allai_VERB pethau_NOUN ddim_PART wedi_PART cadw_VERB fynd_VERB heb_ADP bod_AUX rhywbeth_NOUN ofnadwy_ADJ yn_PART digwydd_VERB ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART gwybod_VERB ddigwyddai_VERB fe_PRON ._PUNCT
Yn_PART hwyr_ADJ neu_CONJ 'n_PART hwyrach_ADJ ._PUNCT
437.713_ADJ
[_PUNCT Clywir_VERB cnoc_NOUN ar_ADP y_DET drws_NOUN ]_PUNCT ._PUNCT
So_ADP ni_PRON ar_ADP agor_VERB ._PUNCT
[_PUNCT Clywir_VERB cnoc_NOUN eto_ADV ]_PUNCT ._PUNCT
O_ADP diawl_NOUN ._PUNCT
[_PUNCT Gellir_VERB clywed_VERB camau_NOUN S_NUM ]_PUNCT ._PUNCT
Ti_PRON yw_VERB e_PRON ._PUNCT
Cardy_NOUN fel_ADP ti_PRON ar_ADP gau_VERB fore_NOUN Sadwrn_PROPN ._PUNCT
[_PUNCT giglan_VERB ]_PUNCT ie_INTJ wel_NOUN ._PUNCT
Mae_VERB 'r_DET blinking_NOUN boy_PRON o_ADP 'r_DET border_NOUN agency_NOUN i_ADP fod_ADV dod_VERB â_ADP hi_PRON bob_DET bore_NOUN 'ma_ADV ._PUNCT
A_CONJ gweud_VERB y_DET gwir_NOUN o'dd_NOUN e_PRON '_PUNCT da_ADJ enwb_NOUN nawr_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT wedodd_VERB e_PRON ._PUNCT
Routine_PROPN yw_VERB e_PRON siŵr_ADJ o_ADP fod_VERB ._PUNCT
Gobeithiwn_VERB ni_PRON <_SYM Anelglur_PROPN ?_PUNCT >_SYM ._PUNCT
O_ADP 'n_PART ni_PRON 'n_PART fod_VERB i_PART fynd_VERB bant_ADV ar_ADP y_DET stag_NOUN prynhawn_NOUN yma_ADV on'd_NOUN ydyn_VERB ni_PRON ?_PUNCT
Ie_INTJ ._PUNCT
Wi_VERB eisiau_ADV siarad_VERB â_ADP ti_PRON amboutu_ADV hwnna_PRON ._PUNCT
Mae_VERB 'r_DET trefniadau_NOUN 'di_PART cwympo_VERB trwyddo_ADP ._PUNCT
ââ_INTJ ._PUNCT
Oedd_VERB y_DET lle_NOUN Archer_X hynny_PRON 'di_PART double_VERB bwco_NOUN rhyw_DET stag_NOUN arall_ADJ neu_CONJ rhywbeth_NOUN ._PUNCT
'_PUNCT Sdim_X ots_NOUN bychan_ADJ enwb_NOUN oedd_AUX yn_PART mynnu_VERB bod_AUX fi_PRON 'n_PART cael_VERB stag_NOUN ._PUNCT
O_ADP 'n_PART i_PRON 'm_DET very_VERB fussed_NOUN ._PUNCT
Beth_PRON yw_VERB 'r_DET pwynt_NOUN priodi_VERB os_CONJ nad_PART wyt_AUX ti_PRON 'n_PART cael_VERB stag_NOUN ?_PUNCT
'_PUNCT Na_VERB hanner_NOUN y_DET sbort_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Byddwn_VERB ni_PRON 'n_PART sorto_NOUN 'r_DET peth_NOUN mas_NOUN erbyn_ADP amser_NOUN cino_ADJ ._PUNCT
542.335_NOUN
Y_DET dynion_NOUN yn_ADP y_DET mywyd_NOUN i_PRON yn_PART diflannu_VERB ._PUNCT
Neu_PART fynd_VERB i_PART ddelio_VERB '_PUNCT da_ADJ 'r_DET meirw_NOUN ._PUNCT
Paid_VERB mynd_VERB yn_PART sentimental_VERB i_ADP gyd_ADP nawr_ADV ._PUNCT
'_PUNCT Sdim_PROPN lle_NOUN i_ADP sentiment_VERB pan_CONJ mae_AUX 'n_PART dod_VERB i_ADP fusnes_NOUN ._PUNCT
Byddwn_AUX ni_PRON 'n_PART cadw_VERB 'r_DET enw_NOUN ._PUNCT
Wrth_ADP gwrs_NOUN enwg_VERB cyfenw_NOUN fydd_VERB enw_NOUN 'r_DET cwmni_NOUN o_ADP hyd_NOUN ._PUNCT
Byddan_AUX nhw_PRON ddim_PART yn_PART cael_ADV enwg_VERB byddan_VERB nhw_PRON ?_PUNCT
Na_INTJ ._PUNCT
Byddwn_AUX ni_PRON 'n_PART trio_ADV cynnal_VERB yr_DET un_NUM safon_NOUN ._PUNCT
Yr_DET un_NUM weledigaeth_NOUN ._PUNCT
Wi_AUX 'n_PART addo_VERB 'na_ADV i_ADP ti_PRON ._PUNCT
[_PUNCT ocheneidio_VERB ]_PUNCT ._PUNCT
Wi_VERB dal_ADV ddim_PART yn_PART siŵr_ADJ ._PUNCT
Mae_VERB 'n_PART gam_NOUN mowr_NOUN ._PUNCT
Busnes_VERB teuluol_ADJ ._PUNCT
Fydd_AUX yn_PART newid_VERB byd_NOUN ._PUNCT
Wedyn_ADV '_PUNCT ny_PRON mae_VERB newid_NOUN byd_NOUN yn_PART gwmws_NOUN beth_PRON sydd_VERB eisiau_VERB arnot_ADP ti_PRON enwb_NOUN ._PUNCT
Ydw_VERB i_PRON 'n_PART wrong_NOUN ?_PUNCT
A_CONJ wi_AUX ddim_PART wedi_PART gweud_VERB wrtho_ADP fe_PRON to_PROPN ._PUNCT
Dy_DET fusnes_NOUN di_PRON yw_VERB e_PRON enwb_NOUN ._PUNCT
Cofia_VERB di_PRON 'na_ADV ._PUNCT
664.989_VERB
Pam_ADV '_PUNCT sai_VERB unrhyw_DET un_NUM yn_PART esgus_ADJ bod_VERB nhw_PRON eisiau_VERB priodi_VERB ta_CONJ beth_PRON ?_PUNCT
Mae_VERB 'n_PART ddigon_ADV anodd_ADJ cadw_VERB priodas_NOUN i_ADP fynd_VERB os_CONJ mai_PART dy_DET ddewis_NOUN di_PRON o'dd_NOUN e_PRON ._PUNCT
Wnes_VERB i_ADP 'r_DET dewis_NOUN iawn_ADV gweud_VERB ?_PUNCT
Neu_CONJ ai_PART ti_PRON sy_AUX 'n_PART rhoi_VERB gormod_ADV o_ADP ddewis_NOUN i_ADP ni_PRON mewn_ADP bywyd_NOUN fyw_VERB ?_PUNCT
Twlu_VERB pobl_NOUN newydd_ADJ aton_VERB ni_PRON ar_ADP hyd_NOUN  _SPACE mwyn_NOUN gweld_VERB bebe_NOUN wnewn_ADP ni_PRON ._PUNCT
720.671_NOUN
enwb_VERB ._PUNCT
[_PUNCT Drws_NOUN yn_PART cau_VERB ]_PUNCT ._PUNCT
Sut_ADV wyt_VERB ti_PRON ?_PUNCT
Alla_VERB i_PART weld_VERB chi_PRON rhyw_DET bryd_NOUN to_NOUN os_CONJ chi_PRON 'n_PART mwyn_NOUN ._PUNCT
Fi_PRON ddim_PART really_ADV styrbo_NOUN chi_PRON ar_ADP Ddydd_NOUN Sadwrn_PROPN ._PUNCT
O_ADP ie_X achos_NOUN dw_VERB i_ADP wrthi_VERB 'n_PART joio_NOUN fy_DET hun_DET fan_NOUN yma_DET on'd_NOUN ydw_VERB ?_PUNCT
O_ADP 'n_PART i_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mae_AUX cael_VERB saib_NOUN o_ADP hein_DET i_ADP gyd_ADP i_PART ddarllen_VERB dy_DET waith_NOUN di_PRON [_PUNCT saib_NOUN ]_PUNCT wel_CONJ mae_VERB yn_PART falmi_NOUN 'r_DET enaid_NOUN ._PUNCT
Wir_ADV yn_PART ._PUNCT
So_PART chi_PRON 'n_PART meddwl_VERB bod_VERB e_PRON 'n_PART ddigon_ADV da_ADJ i_ADP hala_VERB i_ADP mewn_ADP ?_PUNCT
Yndw_VERB tad_NOUN ._PUNCT
A_PART dw_AUX i_PRON 'n_PART meddwl_VERB bod_VERB ganddo_ADP siawns_NOUN go_ADV lew_ADJ ._PUNCT
Ambell_ADJ un_NUM wall_NOUN bach_ADJ yma_ADV ac_CONJ acw_ADV ond_CONJ ddim_PART byd_NOUN mawr_ADJ ._PUNCT
Dw_AUX i_PRON 'di_PART gwneud_VERB nodiadau_NOUN ar_ADP y_DET papur_NOUN ._PUNCT
Gei_VERB di_PRON ddarllen_VERB e_PRON wedyn_ADV enwb_VERB mae_VERB 'r_DET ddwy_NUM o_ADP ni_PRON 'n_PART gwybod_VERB mae_VERB nid_PART dyma_PRON pam_ADV ti_PRON yma_ADV go_ADV iawn_ADV okay_VERB ._PUNCT
ym_ADP ._PUNCT
Nag_PART oedd_VERB e_PRON ?_PUNCT
Tyrd_VERB i_ADP 'r_DET tŷ_NOUN am_ADP goffi_VERB ie_INTJ ?_PUNCT
Fydd_VERB enwb_NOUN ddim_PART yna_ADJ ._PUNCT
978.331_VERB
enwg_VERB ?_PUNCT
Welodd_VERB rhywun_NOUN i_ADP ti_PRON gadael_VERB ?_PUNCT
Na_INTJ ._PUNCT
Sa_CONJ i_PRON 'n_PART credu_VERB ._PUNCT
Oedd_VERB wers_NOUN rydd_NOUN '_PUNCT da_ADJ fi_PRON ta_CONJ beth_PRON ._PUNCT
Beth_PRON sy_AUX 'n_PART bod_VERB ?_PUNCT
Sa_CONJ i_PRON 'n_PART credu_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Sa_CONJ i_PRON 'n_PART credu_VERB ddylen_VERB ni_PART wneud_VERB hyn_PRON rhagor_NOUN ._PUNCT
[_PUNCT Cylwir_PROPN camau_NOUN yn_PART cerdded_VERB i_ADP fwrdd_NOUN ]_PUNCT ._PUNCT
Ti_PRON 'n_PART gweld_VERB enwb_NOUN ?_PUNCT
Ar_ADP y_DET pwynt_NOUN 'ma_ADV mae_VERB fe_PART dal_VERB yn_PART rhydd_ADJ on'd_ADJ yw_VERB e_PRON ?_PUNCT
Dal_VERB mor_ADV berffaith_NOUN ._PUNCT
Mor_ADV ddiniwed_ADJ ._PUNCT
Mor_ADV ddieuog_ADJ â_ADP 'r_DET ddydd_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ni_PRON ._PUNCT
Mae_AUX fa_PRON 'n_PART troddi_VERB hwnna_PRON nawr_ADV ._PUNCT
Does_VERB dim_DET rhaid_VERB iddo_ADP fe_PART groesu_VERB fe_PRON ._PUNCT
Alla_VERB fe_PRON jyst_ADV gerdded_VERB bant_ADV ._PUNCT
Gyda_ADP pwy_PRON ti_PRON 'n_PART siarad_VERB ?_PUNCT
enwb_VERB ._PUNCT
So_PART ti_PRON 'n_PART nabod_VERB hi_PRON to_NOUN ._PUNCT
Ti_PRON 'n_PART acto_VERB 'n_PART od_DET enwb_NOUN Okay_X ?_PUNCT
So_PART ti_PRON fod_VERB dy_DET hunan_VERB ._PUNCT
Sa_CONJ i_PRON 'n_PART credu_VERB alla_VERB i_ADP handlo_VERB hyn_PRON rhagor_NOUN ._PUNCT
Paid_VERB â_ADP ngadael_VERB i_PRON ._PUNCT
Dw_VERB i_PRON 'n_PART dy_DET garu_VERB di_PRON enwg_VERB ._PUNCT
Jyst_ADV gad_VERB fi_PRON fod_VERB Okay_PROPN ?_PUNCT
Gad_VERB fi_PRON fod_VERB ._PUNCT
1760.952_ADJ
BeBe_ADV oedd_AUX yr_DET instructor_NOUN yn_PART mynd_VERB ymlaen_ADV ambyty_NOUN ?_PUNCT
Bod_VERB rhyw_DET foi_NOUN '_PUNCT da_ADJ ti_PRON ._PUNCT
Bod_AUX e_PRON 'di_PART rhedeg_VERB bant_ADV ._PUNCT
O_ADP na_PART ._PUNCT
Paid_VERB gweud_VERB taw_CONJ enwg_ADV oedd_VERB e_PRON ._PUNCT
Fe_PART wnaeth_VERB hyn_PRON i_ADP ti_PRON ?_PUNCT
Wnaeth_VERB ddim_PART byd_NOUN i_ADP fi_PRON ._PUNCT
Dim_PRON ond_ADP siarad_VERB â_ADP fi_PRON ._PUNCT
A_PART trio_ADV esbonio_VERB pethau_NOUN i_ADP fi_PRON ._PUNCT
Sy_VERB 'n_PART fwy_ADJ na_CONJ beth_PRON wyt_AUX ti_PRON 'di_PART trio_ADV wneud_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
BeBe_AUX ti_PRON 'n_PART gwardo_ADV wrthaf_VERB fi_PRON ?_PUNCT
2116.861_NOUN
O_ADP ._PUNCT Shwd_PROPN ti_PRON ?_PUNCT
Ti_PRON bach_ADJ yn_PART gynnar_ADJ nag_CONJ '_PUNCT yt_VERB ti_PRON ?_PUNCT
-_PUNCT Ne_NOUN newydd_ADJ gyrraedd_VERB y_DET 'n_PART ni_PRON ._PUNCT
Ie_INTJ wel_NOUN [_PUNCT ocheneidio_VERB ]_PUNCT '_PUNCT sdim_NOUN eisiau_VERB i_PART enwg_VERB aros_VERB mas_NOUN rhy_ADV hir_ADJ oes_VERB e_PRON ?_PUNCT
Newydd_ADJ e_PRON mas_NOUN o_ADP 'r_DET '_PUNCT sbyty_NOUN mae_VERB fe_PRON [_PUNCT ocheneidio_VERB ]_PUNCT ._PUNCT
So_PART fe_PRON 'n_PART yfed_VERB gobeitho_VERB ._PUNCT
Nad_INTJ '_PUNCT y_DET ._PUNCT
Nad_INTJ '_PUNCT y_DET ._PUNCT
Mae_VERB fe_PRON 'n_PART [_PUNCT aneglur_ADJ ]_PUNCT ei_DET hunan_VERB ._PUNCT
Ble_ADV mae_VERB fe_PART de_NOUN ?_PUNCT
O_ADP   _SPACE ._PUNCT
Wel_INTJ ._PUNCT
Ti_PRON 'n_PART nabod_ADV enwg_VERB ._PUNCT
Cael_VERB mwgyn_NOUN bach_ADJ siŵr_ADV o_ADP fod_VERB ._PUNCT
Ie_INTJ wel_ADV dylai_VERB fe_PRON ddim_PART fod_AUX yn_PART wneud_VERB 'na_ADV chwaith_ADV ._PUNCT
enwg_VERB ._PUNCT
Rho_VERB brêc_NOUN iddo_ADP fe_PRON [_PUNCT =_SYM ]_PUNCT Mae_VERB [_PUNCT /=_PROPN ]_PUNCT mae_VERB 'di_PART bod_VERB trwy_ADP lot_PRON nag_CONJ yw_VERB e_PRON ?_PUNCT
Dere_VERB i_ADP fi_PRON gael_ADV prynu_VERB drink_NOUN bach_ADJ i_ADP ti_PRON ie_INTJ ?_PUNCT
Ginger_VERB beer_VERB neu_CONJ rywbeth_NOUN ._PUNCT
Sa_CONJ i_PRON 'di_PART gweud_VERB wrthot_ADP ti_PRON ambyty_NOUN 'r_DET gynhadledd_NOUN 'na_ADV to_NOUN ._PUNCT
O_ADP ie_INTJ ._PUNCT
Sa_NOUN i_PRON 'n_PART mwyn_NOUN gadael_VERB y_DET plant_NOUN a_CONJ enwb_NOUN rhy_ADV hir_ADJ ._PUNCT
Ie_INTJ ._PUNCT
Wnaiff_VERB un_NUM bach_ADJ ddim_PART niwed_NOUN sbobo_NOUN ._PUNCT
2530.807_NOUN
[_PUNCT llefain_ADJ ]_PUNCT ._PUNCT
Ti_PRON 'n_PART iawn_ADJ ._PUNCT
Diolch_NOUN i_ADP chi_PRON am_ADP bopeth_PRON ._PUNCT
Mae_VERB 'r_DET Iesu_PROPN 'n_PART lwcus_ADJ i_PART gael_VERB ficer_NOUN fel_ADP chi_PRON ._PUNCT
[_PUNCT llefain_ADJ ]_PUNCT ._PUNCT
Mr_NOUN cyfenw_NOUN ._PUNCT
Mynd_VERB i_ADP fi_PRON i_ADP ti_PRON 'n_PART well_ADJ on'd_NOUN ife_ADP enwb_NOUN ?_PUNCT
Wela_VERB i_ADP chi_PRON [_PUNCT saib_NOUN ]_PUNCT pan_CONJ wela_VERB i_ADP chi_PRON ._PUNCT
[_PUNCT llefain_ADJ ]_PUNCT ._PUNCT
-_PUNCT Am_ADP ambwlans_NOUN ar_ADP ei_DET ffordd_NOUN enwb_NOUN ._PUNCT
Mae_AUX 'r_DET bachan_NOUN yma_DET 'n_PART nyrs_NOUN enwb_NOUN ._PUNCT
Gad_VERB iddo_ADP gael_VERB pip_NOUN ._PUNCT
[_PUNCT llefain_ADJ ]_PUNCT ._PUNCT
Nawr_ADV rwyt_AUX ti_PRON 'n_PART gollwng_VERB dy_DET was_NOUN yn_PART rhudd_ADJ o_ADP 'r_DET arglwydd_NOUN [_PUNCT anadlu_VERB ]_PUNCT ._PUNCT
Mewn_ADP tyngnefedd_NOUN ._PUNCT
Ymuno_VERB â_ADP thaer_NOUN ._PUNCT
2796.387_ADJ
2902.955_ADJ
