Newyddion_NOUN diweddara'_VERBUNCT gan_ADP enw_NOUN
<_SYM img_VERB /_PUNCT >_SYM
Annwyl_NOUN Gyfeillion_PROPN ,_PUNCT
Hyfryd_ADJ oedd_VERB gweld_VERB nifer_NOUN ohonoch_ADP o_ADP gwmpas_NOUN yr_DET Eisteddfod_PROPN ;_PUNCT yn_ADP y_DET sosial_NOUN ,_PUNCT mewn_ADP sgyrsiau_NOUN a_CONJ gweithdai_VERB ,_PUNCT ym_ADP Maes_NOUN B_NUM ,_PUNCT yn_PART cystadlu_VERB ,_PUNCT ac_CONJ â_ADP chlipfwrdd_NOUN yn_ADP eich_DET llaw_NOUN yn_PART llenwi_VERB arolygon_ADV ._PUNCT
Gwelwch_VERB isod_ADV wybodaeth_NOUN am_ADP ddigwyddiadau_NOUN nesaf_ADJ enw_NOUN a_CONJ fydd_AUX yn_PART ein_PRON cymryd_VERB i_ADP mewn_ADP i_ADP 'r_DET Hydref_NOUN gyda_ADP 'r_DET un_NUM egni_NOUN a_CONJ gwelsom_VERB dros_ADP yr_DET Haf_NOUN ._PUNCT
YSGOL_NOUN HAF_X A_X CYNGOR_NOUN CENEDLAETHOL_NOUN enw_NOUN
MEDI_NOUN 8_NUM -_SYM 9_NUM
Wyt_AUX ti_PRON 'n_PART dod_VERB i_ADP Gyngor_NOUN Cenedlaethol_ADJ ac_CONJ Ysgol_NOUN Haf_DET enw_NOUN yng_ADP Nghaerdydd_PROPN ar_ADP 8_NUM Medi_PROPN ?_PUNCT
Os_CONJ nad_PART wyt_AUX ti_PRON wedi_PART cofrestru_VERB yn_PART barod_ADJ ,_PUNCT cofia_VERB fod_VERB angen_NOUN cofrestru_VERB erbyn_ADP DYDD_PROPN GWENER_PROPN (_PUNCT 17_NUM Awst_PROPN )_PUNCT er_ADP mwyn_NOUN sicrhau_VERB llety_NOUN ._PUNCT
Clicia_VERB yma_ADV i_PART gofrestru_VERB :_PUNCT
cyfeiriad_NOUN
Dydd_NOUN Gwener_PROPN yw_VERB 'r_DET dyddiad_NOUN cau_VERB hefyd_ADV ar_ADP gyfer_NOUN anfon_VERB cynigion_NOUN ar_ADP gyfer_NOUN y_DET Cyngor_NOUN Cenedlaethol_ADJ ._PUNCT
Clicia_PROPN
yma_ADV
i_PART weld_VERB enghraifft_NOUN o_ADP gynnig_NOUN :_PUNCT
Mae_VERB 'r_DET manylion_NOUN yn_PART llawn_ADJ ar_ADP gael_VERB ar_ADP y_DET digwyddiad_NOUN Facebook_PROPN :_PUNCT
cyfeiriad_NOUN
Os_CONJ hoffet_VERB ti_PRON fynychu_VERB 'r_DET Cyngor_NOUN Cenedlaethol_ADJ fel_ADP cynrychiolydd_NOUN ar_ADP ran_NOUN dy_DET gangen_NOUN leol_ADJ ,_PUNCT cysyllta_VERB â_ADP ni_PRON ar_ADP
cyfeiriad_NOUN -_PUNCT bost_NOUN
._PUNCT
Wedi_ADP 'r_DET Cyngor_NOUN Cenedlaethol_ADJ ,_PUNCT bydd_VERB yr_DET ymgyrchydd_NOUN Sahar_PROPN Al_X -_PUNCT Faifi_PROPN yn_PART cynnal_VERB gweithdy_NOUN ar_ADP ymgyrchu_VERB cymunedol_ADJ ._PUNCT
Clicia_PROPN
yma_ADV
i_PART weld_VERB Sahar_PROPN yn_PART ymateb_VERB i_ADP sylwadau_NOUN diweddar_ADJ Boris_PROPN Johnson_PROPN ar_ADP raglen_NOUN
Good_VERB Morning_PROPN Britain_PROPN ._PUNCT
I_ADP 'r_DET dyddiadur_NOUN :_PUNCT
Wyt_AUX ti_PRON 'n_PART byw_VERB yn_ADP Ynys_PROPN Môn_PROPN ?_PUNCT
Ymuna_VERB â_ADP chriw_NOUN enw_NOUN yn_PART eu_DET
digwyddiad_NOUN
nos_NOUN Iau_ADJ yma_ADV (_PUNCT fory_ADV )_PUNCT !_PUNCT
enw_NOUN ,_PUNCT Llangefni_PROPN ,_PUNCT 7:30_NUM pm_PRON
Dewch_VERB i_ADP
gyfarfod_VERB cangen_NOUN
enw_NOUN Caerdydd_PROPN am_ADP drafodaeth_NOUN a_CONJ drinc_NOUN ._PUNCT
Nos_NOUN Wener_NOUN yma_ADV ,_PUNCT dechrau_VERB yn_PART enw_NOUN am_ADP 7:30_NOUN pm_PRON
Ymunwch_VERB â_ADP gorymdaith_NOUN mwya_ADJ 'r_DET flwyddyn_NOUN gyda_ADP Pride_NOUN Cymru_PROPN ar_ADP ddydd_NOUN Sadwrn_PROPN 25_NUM ain_NOUN o_ADP Awst_PROPN ._PUNCT
Manylion_PROPN
yma_ADV !_PUNCT
