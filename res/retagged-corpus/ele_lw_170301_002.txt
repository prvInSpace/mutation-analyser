Pwyllgor_VERB Apêl_NOUN Tyllgoed_PROPN ,_PUNCT Trelai_PROPN a_CONJ Chaerau_NOUN Eisteddfod_NOUN Genedlaethol_ADJ Caerdydd_PROPN 2018_NUM
Ymunwch_VERB â_ADP phwyllgor_NOUN apêl_NOUN Tyllgoed_PROPN ,_PUNCT Trelai_PROPN a_CONJ Chaerau_PROPN er_ADP mwyn_NOUN i_ADP chi_PRON fod_VERB yn_PART rhan_NOUN o_ADP 'r_DET bwrlwm_NOUN wrth_ADP inni_ADP baratoi_VERB at_ADP Eisteddfod_NOUN Genedlaethol_ADJ Caerdydd_PROPN 2018_NUM ._PUNCT
Mae_VERB angen_NOUN i_ADP 'r_DET dair_NUM ardal_NOUN godi_VERB £_SYM 10,000_NUM ar_ADP gyfer_NOUN yr_DET ŵyl_NOUN !_PUNCT
Cadwch_VERB lygaid_NOUN am_ADP ddigwyddiadau_NOUN lleol_ADJ ac_CONJ os_CONJ oes_VERB ganddo_ADP chi_PRON unrhyw_DET argymhellion_NOUN neu_CONJ syniadau_NOUN -_PUNCT yna_ADV ebostiwch_VERB :_PUNCT
cyfeiriad_NOUN -_PUNCT bost_NOUN
Dewch_VERB yn_PART llu_NOUN ,_PUNCT mae_VERB croeso_NOUN i_ADP bawb_PRON !_PUNCT
Grŵp_NOUN Facebook_PROPN :_PUNCT Pwyllgor_NOUN Apêl_NOUN Tyllgoed_PROPN ,_PUNCT Trelai_PROPN a_CONJ Chaerau_PROPN
