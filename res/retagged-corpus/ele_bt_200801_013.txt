"_PUNCT Gwybodaeth_PROPN ,_PUNCT Cyngor_PROPN ,_PUNCT Arweiniad_NOUN #_ADJ Covid19_PROPN "_PUNCT ,_PUNCT "_PUNCT Cael_VERB trafferth_NOUN gweld_VERB yr_DET e_PRON -_PUNCT bost_NOUN hwn_DET ?_PUNCT
Darllenwch_VERB yn_ADP eich_DET porwr_NOUN enw_NOUN
1_NUM Mai_PROPN 2020_NUM
Cyllid_VERB enw_NOUN |_SYM
Dechrau_VERB a_CONJ Chynllunio_PROPN Busnes_PROPN enw_NOUN |_SYM
Marchnata_VERB enw_NOUN |_SYM
Sgiliau_NOUN a_CONJ Hyfforddiant_NOUN enw_NOUN |_SYM
Syniadau_NOUN Busnes_PROPN enw_NOUN |_SYM
TG_VERB enw_NOUN
Coronafeirws_NOUN :_PUNCT y_DET wybodaeth_NOUN a_CONJ 'r_DET cymorth_NOUN busnes_NOUN diweddaraf_ADJ enw_NOUN
Mae_AUX Llywodraeth_PROPN Cymru_PROPN wedi_PART cyhoeddi_VERB bron_ADV i_ADP £_SYM 2bn_NUM o_ADP gymorth_NOUN i_ADP fusnesau_NOUN yng_ADP Nghymru_PROPN ,_PUNCT yn_PART ogystal_ADJ â_ADP 'r_DET cymorth_NOUN a_CONJ ddarperir_VERB gan_ADP Lywodraeth_NOUN y_DET DU_PROPN ._PUNCT
Cewch_VERB yma_DET 'r_DET wybodaeth_NOUN diweddaraf_ADJ am_ADP y_DET pecynnau_NOUN cymorth_NOUN hyn_PRON sydd_VERB ar_ADP gael_VERB i_ADP fusnesau_NOUN yng_ADP Nghymru_PROPN
enw_NOUN
Cronfa_VERB 'r_DET Dyfodol_NOUN coronafeirws_NOUN -_PUNCT meini_NOUN prawf_NOUN cymhwysedd_NOUN enw_NOUN
Gallai_VERB 'r_DET benthyciadau_NOUN trosi_VERB hyn_PRON fod_VERB yn_PART opsiwn_NOUN addas_ADJ i_ADP fusnesau_NOUN sy_AUX 'n_PART dibynnu_VERB ar_ADP fuddsoddiadau_NOUN ecwiti_NOUN ac_CONJ sy_AUX 'n_PART methu_ADV cael_VERB mynediad_NOUN at_ADP y_DET Cynllun_NOUN Benthyciadau_NOUN Tarfu_PROPN ar_ADP Fusnes_PROPN yn_PART sgil_NOUN y_DET Coronafeirws_PROPN ._PUNCT
enw_NOUN
Cymorth_VERB ffyrlo_NOUN -_PUNCT dysgu_VERB ar_ADP -_PUNCT lein_NOUN enw_NOUN
Beth_PRON am_PART gymryd_VERB rhan_NOUN mewn_ADP dysgu_NOUN ar_ADP -_PUNCT lein_NOUN ?_PUNCT
Gall_VERB dysgu_VERB ar_ADP -_PUNCT lein_NOUN eich_DET helpu_VERB i_ADP wella_VERB eich_DET sgiliau_NOUN ,_PUNCT dysgu_VERB sgiliau_NOUN newydd_ADJ ,_PUNCT a_CONJ 'ch_ADV helpu_VERB gyda_ADP 'ch_PRON lles_NOUN ._PUNCT
enw_NOUN
COVID_NOUN -_PUNCT 19_NUM :_PUNCT Cadw_NOUN pellter_NOUN cymdeithasol_ADJ a_CONJ diogelu_VERB gweithwyr_NOUN unigol_ADJ enw_NOUN
Yn_ADP sgil_NOUN gofynion_NOUN cadw_VERB pellter_NOUN cymdeithasol_ADJ ,_PUNCT mae_VERB gweithio_VERB ar_ADP eich_DET pen_NOUN eich_DET hun_PRON wedi_PART dod_VERB yn_PART llawer_ADV mwy_ADV cyffredin_ADJ ._PUNCT
Mae_VERB gan_ADP yr_DET Awdurdod_NOUN Gweithredol_ADJ Iechyd_NOUN a_CONJ Diogelwch_VERB ganllawiau_NOUN ar_ADP gyfer_NOUN cyflogwyr_NOUN i_ADP gadw_VERB gweithwyr_NOUN unigol_ADJ yn_PART iach_ADJ ac_CONJ yn_PART ddiogel_ADJ ._PUNCT
enw_NOUN
Canllawiau_NOUN Yswiriant_NOUN Busnes_PROPN Covid_PROPN -_PUNCT 19_NUM enw_NOUN
Mae_AUX Cymdeithas_NOUN Yswirwyr_NOUN Prydain_PROPN wedi_PART darparu_VERB 'r_DET canllawiau_NOUN canlynol_ADJ ar_ADP y_DET cwestiynau_NOUN cyffredin_ADJ sy_AUX 'n_PART ymwneud_VERB ag_CONJ yswiriant_VERB a_CONJ materion_NOUN busnes_NOUN eraill_ADJ ._PUNCT
enw_NOUN
Darparu_VERB cyfarpar_NOUN critigol_ADJ a_CONJ chyfarpar_NOUN diogelu_VERB personol_ADJ (_PUNCT PPE_NUM )_PUNCT enw_NOUN
Os_CONJ allwch_VERB ddarparu_VERB unrhyw_DET gymorth_NOUN ar_ADP ddarparu_VERB cyfarpar_NOUN critigol_ADJ a_CONJ chyfarpar_NOUN PPE_NUM neu_CONJ unrhyw_DET gymorth_NOUN arall_ADJ ,_PUNCT nad_PART yw_VERB 'n_PART uniongyrchol_ADJ gysylltiedig_ADJ ag_CONJ offer_NOUN meddygol_ADJ neu_CONJ PPE_NUM ,_PUNCT cysylltwch_VERB â_ADP ni_PRON ._PUNCT
enw_NOUN
COVID_NOUN -_PUNCT 19_NUM :_PUNCT podlediad_NOUN cymorth_NOUN i_ADP fusnesau_NOUN Busnes_PROPN Cymru_PROPN enw_NOUN
Tanysgrifiwch_VERB a_CONJ gwrandewch_VERB ar_ADP ein_DET podlediadau_NOUN ar_ADP gyfer_NOUN diweddariadau_NOUN a_CONJ gwybodaeth_NOUN i_ADP fusnesau_NOUN yng_ADP Nghymru_PROPN ._PUNCT
