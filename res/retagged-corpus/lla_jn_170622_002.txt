<_SYM S_NUM ?_PUNCT >_SYM Dere_PROPN am_ADP dro_NOUN !_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM I_ADP ganu_VERB a_CONJ dawnsio_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ti_PRON a_CONJ fi_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yn_PART ti_PRON -_PUNCT pi_NOUN ni_PRON !_PUNCT
rhaglen_NOUN
[_PUNCT Cerddoriaeth_NOUN ]_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Hiya_X heddiw_ADV ma_AUX rhaglen_NOUN yn_PART teithio_VERB i_ADP le_NOUN o_ADP 'r_DET enw_NOUN Tonyrefail_PROPN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mae_VERB rhaglen_NOUN o_ADP le_NOUN i_ADP le_NOUN yng_ADP Nghymru_PROPN ,_PUNCT o_ADP lan_NOUN y_DET môr_NOUN i_ADP ben_NOUN y_DET mynydd_NOUN ._PUNCT
I_ADP drefi_VERB a_CONJ phentrefi_VERB i_PART groesawu_VERB pawb_PRON ._PUNCT
Ymhobman_VERB ,_PUNCT mae_VERB yna_ADV ffrindiau_NOUN newydd_ADJ i_PART helpu_VERB creu_VERB cân_NOUN am_ADP eu_DET hardal_NOUN ac_CONJ i_PART ddod_VERB i_ADP ganu_VERB a_CONJ dawnsio_VERB yn_PART <_SYM /_PUNCT anon_VERB >_SYM enwrhaglen_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Pwy_PRON fydd_VERB yn_PART ffrindie_NOUN '_PUNCT newydd_ADJ inni_ADP fan_NOUN hyn_DET ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Helo_INTJ !_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Helo_INTJ fi_PRON yw_VERB enwg_VERB
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ fi_PRON yw_VERB enwb_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ fi_PRON yw_VERB enwb_NOUN
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ fi_PRON yw_VERB enwg_VERB
<_SYM S_NUM ?_PUNCT >_SYM enwb_NOUN ydw_VERB i_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Un_NUM o_ADP 'r_DET llefydd_NOUN mwyaf_ADV diddorol_ADJ yn_ADP Nhonyrefail_PROPN yw_VERB theatr_NOUN y_DET Savoy_NOUN ._PUNCT
Amser_NOUN maith_ADJ yn_PART ôl_NOUN ,_PUNCT roedd_VERB yr_DET adeilad_NOUN yma_DET 'n_PART sinema_NOUN ,_PUNCT cyn_CONJ cau_VERB a_CONJ mynd_VERB â_ADP 'i_PRON ben_NOUN iddo_ADP ._PUNCT
Heddi_VERB '_PUNCT ,_PUNCT mae_VERB 'n_PART theatr_NOUN braf_ADJ ._PUNCT
Pentref_NOUN glofaol_ADJ yw_VERB Tonyrefail_PROPN ,_PUNCT lle_ADV roedd_AUX glo_NOUN yn_PART cael_VERB ei_PRON gloddio_VERB ,_PUNCT i_ADP 'w_PRON losgi_VERB i_PART gynhyrchu_VERB trydan_ADP ._PUNCT
Roedd_VERB hon_PRON yn_PART broses_NOUN fudr_ADJ iawn_ADV ._PUNCT
Heddi_VERB '_PUNCT rydyn_AUX ni_PRON eisiau_VERB trydan_ADP glân_ADJ ac_CONJ un_NUM ffordd_NOUN o_ADP gael_VERB hwnnw_PRON yw_VERB gyda_ADP melinau_NOUN fel_ADP rhain_PRON sydd_AUX yn_PART harneisio_VERB ynni_NOUN gwynt_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Heddi_X '_PUNCT fi_PRON enwb_NOUN sydd_AUX yn_PART dweud_VERB y_DET stori_NOUN ._PUNCT
Ni_PART 'n_PART cwrdd_NOUN â_ADP enwb_NOUN a_PART enwg_VERB yn_ADP yr_DET Eglwys_PROPN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Fi_PRON 'di_PART clywed_VERB bod_VERB theatr_NOUN yma_DET yn_ADP Tonyrefail_PROPN ._PUNCT
Pwy_PRON sy_AUX wedi_PART bod_VERB i_ADP 'r_DET theatr_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT o_ADP waw_NOUN !_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dwywaith_NOUN !_PUNCT
-_PUNCT Dwy_PROPN
<_SYM S_NUM ?_PUNCT >_SYM Waw_X !_PUNCT
Beth_PRON chi_PRON wedi_PART gweld_VERB yn_ADP y_DET theatr_NOUN ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Waw_X mama_NOUN 'na_ADV llwyth_PRON o_ADP egni_NOUN gydach_ADJ chi_PRON plant_NOUN !_PUNCT
[_PUNCT Chwerthin_PROPN ]_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP !_PUNCT
Chi_PRON 'n_PART gw'bod_VERB beth_PRON arall_ADJ sydd_VERB angen_NOUN egni_NOUN ?_PUNCT
I_ADP gweithio_VERB ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Teledu_X ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Teledu_X ie_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Tegell_PROPN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ie_INTJ tegell_NOUN a_CONJ beth_PRON yw_VERB enw_NOUN 'r_DET egni_NOUN sy_AUX 'n_PART gwneud_VERB i_ADP 'r_DET teledu_NOUN gweithio_VERB ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Trydan_PROPN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ o_ADP lle_NOUN ydyn_AUX ni_PRON 'n_PART cael_VERB trydan_NOUN ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM  _SPACE [_PUNCT aneglur_ADJ ]_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Chi_PRON 'n_PART gw'bod_VERB ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Fan_NOUN 'na_ADV ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Melin_PROPN gwynt_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Da_ADJ iawn_ADV ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mae_AUX 'n_PART cymryd_VERB yr_DET aer_NOUN a_CONJ gwneud_VERB trydan_ADP ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_PART symudwch_VERB eich_DET breichiau_NOUN fel_ADP melin_NOUN gwynt_NOUN ._PUNCT
Rydyn_AUX ni_PRON 'n_PART creu_VERB trydan_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Chi_PRON 'n_PART gallu_ADV gweld_VERB melinau_NOUN gwynt_NOUN fel_ADP hyn_PRON ar_ADP ben_NOUN mynyddoedd_NOUN ar_ADP draws_ADJ Cymru_PROPN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ni_PART wedi_PART dod_VERB â_ADP enwb_NOUN a_PART enwg_VERB i_PART weld_VERB ein_DET rhai_DET ni_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ maen_AUX nhw_PRON wedi_PART cael_VERB syniadau_NOUN am_ADP y_DET gân_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ nawr_ADV mae_VERB 'n_PART hwyr_ADJ ac_CONJ mae_AUX 'n_PART rhaid_VERB inni_ADP fynd_VERB gartre_ADV '_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mae_VERB diwrnod_NOUN mawr_ADJ ni_PRON wedi_PART cyrraedd_VERB ._PUNCT
Ni_PART wedi_PART dysgu_VERB sut_ADV i_ADP twymo_VERB llais_NOUN a_CONJ nawr_ADV ni_PRON 'n_PART twymo_VERB cyrff_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ar_ADP gyfer_NOUN canu_VERB a_CONJ dawnsio_VERB yn_PART rhaglen_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Mae_VERB angen_NOUN tacluso_VERB rhaglen_NOUN cyn_ADP i_ADP pawb_PRON gyrraedd_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP dyna_ADV ni_PRON ,_PUNCT popeth_NOUN yn_PART barod_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ dyma_DET nhw_PRON !_PUNCT
Ein_DET ffrindiau_NOUN ni_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ar_ADP eu_DET ffordd_NOUN drwy_ADP 'r_DET brwyn_NOUN a_CONJ 'r_DET bubbles_NOUN ._PUNCT
Mae_VERB hyn_PRON mor_ADV gyffrous_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM enwb_NOUN a_PART enwg_VERB !_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Croeso_NOUN mawr_ADJ ichi_ADP gyd_ADP i_ADP 'r_DET tipi_NOUN ._PUNCT
Gawn_VERB ni_PRON amser_NOUN gwych_ADJ te_NOUN ._PUNCT
[_PUNCT CERDDORIAETH_NUM ]_PUNCT
833.941_NOUN
