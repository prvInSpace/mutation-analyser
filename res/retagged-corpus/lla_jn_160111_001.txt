0.0_NOUN N_NUM /_PUNCT A_CONJ
Y_DET bach_ADJ a_CONJ 'r_DET mawr_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Y_DET dôf_NOUN [_PUNCT saib_NOUN ]_PUNCT a_CONJ 'r_DET gwyll_NOUN '_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Yr_DET annwyl_ADJ [_PUNCT saib_NOUN ]_PUNCT a_CONJ 'r_DET blîn_NOUN ._PUNCT
Mae_VERB o_PRON ._PUNCT
Wp_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT enwg_VERB ._PUNCT
naughty_ADV enwg_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Bore_NOUN da_ADJ sefydliad_NOUN ._PUNCT
Ma'_VERBUNCT nhw_PRON i_ADP gyd_NOUN ar_ADP lyfra'_VERBUNCT milfeddygfa_NOUN sefydliad_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yn_ADP y_DET gyfres_NOUN yma_DET cawn_NOUN ddilyn_VERB hynt_NOUN a_CONJ helyntion_NOUN y_DET milfeddygon_NOUN a_CONJ 'u_PRON cleientau_VERB pedair_NUM coes_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_PART chael_VERB golwg_NOUN go_ADV iawn_ADJ ar_ADP enw_NOUN ._PUNCT
Chi_PRON 'n_PART '_PUNCT ala_NOUN fi_PRON nerfus_ADJ nawr_ADV ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Instinct_NOUN cynta'_ADJUNCT milfeddyg_NOUN ydy_VERB gwella_VERB r'wbath_NOUN o_ADP le_NOUN ac_CONJ '_PUNCT isho_NOUN fo_PRON wella_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM W_X mama_NOUN nhw_PRON reit_NOUN goch_ADJ a_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ydyn_VERB ._PUNCT
O_ADP 's_X e_DET unrhyw_DET hanes_NOUN o_ADP scab_NOUN acw_ADV ?_PUNCT
'_PUNCT Dan_PROPN ni_PRON 'n_PART meddwl_VERB bod_VERB o_PRON 'n_PART [_PUNCT anadlu_VERB ]_PUNCT tumour_NOUN so_ADJ os_CONJ '_PUNCT dy_DET o_ADP 'n_PART [_PUNCT anadlu_VERB ]_PUNCT  _SPACE benign_NOUN ne_CONJ '_PUNCT malignant_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
O_ADP 'r_DET del_NUM [_PUNCT saib_NOUN ]_PUNCT i_ADP 'r_DET diddorol_NOUN ._PUNCT
enwg_VERB y_DET '_PUNCT enw_NOUN hwn_PRON ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ma_INTJ '_PUNCT hwn_PRON yn_PART dŵl_NOUN ffantastig_ADJ i_ADP gym'yd_PUNCT gwaed_NOUN o_ADP gathod_NOUN ._PUNCT
Ma_INTJ '_PUNCT tail_NOUN yn_PART positif_ADJ am_ADP e_PRON -_PUNCT coli_VERB dyna_DET mama_NOUN hwnna_PRON 'n_PART dangos_ADJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
Mae_VERB 'n_PART swydd_NOUN sy_AUX 'n_PART gofyn_VERB am_ADP stumog_NOUN go_ADV gry_ADJ '_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Synnwyr_NOUN digrifwch_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ dipyn_X go_ADV lew_ADJ o_ADP fôn_ADP braich_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ac_CONJ mama_AUX rhain_PRON yn_PART rhinwedda'_VERBUNCT sy_VERB gan_ADP bob_DET un_NUM o_ADP filfeddygon_NOUN sefydliad_NOUN wrth_ADP iddyn_ADP nhw_PRON wasanaethu_VERB anifeiliaid_NOUN o_ADP bob_DET math_NOUN ar_ADP hyd_NOUN a_CONJ lled_VERB lleoliad_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART unig_ADJ ]_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ti_PRON mo'yn_NOUN dal_VERB 'i_PRON ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Hei_NOUN enwb_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Hei_INTJ gw_NOUN '_PUNCT +_SYM
Good_VERB girl_NOUN ._PUNCT
+_VERB girl_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ma'_VERBUNCT enwb_NOUN ar_ADP gyfnod_NOUN o_ADP brofiad_NOUN gwaith_NOUN yn_PART sefydliad_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Tra_CONJ 'i_PRON bod_AUX yn_PART gorffen_VERB ei_DET chwrs_NOUN milfeddygaeth_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_DET Heddiw_X mama_NOUN hi_PRON 'n_PART cynorthwyo_VERB enwb_NOUN sydd_AUX yn_PART brechi_VERB dau_NUM geffyl_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Shh_NOUN shh_NOUN [_PUNCT /=_PROPN ]_PUNCT shh_NOUN ._PUNCT
Wel_INTJ ni_PRON jyst_ADV yn_PART sedatio_VERB hi_PRON i_PART clipio_VERB hi_PRON so_VERB mama_NOUN hwnna_DET fatha_NOUN jest_ADV shavio_VERB gwallt_NOUN hi_PRON so_AUX mama_NOUN hi_PRON 'n_PART edrych_VERB yn_PART mwy_ADV smart_VERB i_ADP competitions_NOUN a_CONJ phethe_NOUN '_PUNCT fel_ADP 'na_ADV ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM On_ADP mama_NOUN perchennog_NOUN hi_PRON methu_VERB   _SPACE clipio_VERB coesa'_VERBUNCT hi_PRON a_CONJ pen_VERB hi_PRON '_PUNCT c'os_NOUN '_PUNCT dy_DET 'i_PRON 'm_DET yn_PART licio_VERB fo_PRON so_VERB dyna_PRON pam_ADV '_PUNCT dan_ADP ni_PRON go'o_VERB '_PUNCT sedatio_VERB 'i_PRON ._PUNCT
Wel_INTJ yn_PART yn_PART sedatio_VERB anifail_NOUN mor_ADV fawr_ADJ ._PUNCT
Ie_INTJ ._PUNCT
Sut_ADV ti_PRON 'n_PART mynd_VERB o_ADP gwmpas_NOUN gw'bod_VERB faint_ADV i_ADP roid_VERB a_CONJ petha'_NOUNUNCT fel_ADP 'na_ADV ?_PUNCT
133.995_NUM N_NUM /_PUNCT A_CONJ
Mae_VERB enwg_VERB un_NUM o_ADP bartneriaid_NOUN sefydliad_NOUN wedi_PART dod_VERB draw_ADV i_ADP fferm_NOUN y_DET practis_NOUN ar_ADP gyrion_NOUN __NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yma_DET mama_VERB nhw_PRON 'n_PART cynnig_ADJ gwasanaeth_NOUN bridio_VERB gwartheg_NOUN pedigri_NOUN ble_ADV mama_NOUN modd_NOUN rheoli_VERB ffrwythlondeb_NOUN y_DET gwartheg_NOUN a_CONJ throsglwyddo_VERB 'r_DET embrios_ADJ pedigri_NOUN i_ADP ga'l_VERB eu_PRON cario_VERB gan_ADP wartheg_NOUN cyffredin_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X '_PUNCT hyn_PRON yn_PART golygu_VERB bod_VERB modd_NOUN i_ADP ffarmwr_NOUN g'el_NOUN sawl_ADJ ciw_NOUN o_ADP frid_VERB mewn_ADP blwyddyn_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yn_PART hytrach_ADV na_CONJ dim_PRON ond_ADP yr_DET un_NUM a_PART fyddai_AUX 'n_PART dod_VERB yn_PART naturiol_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_DET Heddiw_NOUN mama_NOUN enwg_VERB yn_PART profi_VERB gwaed_NOUN y_DET gwartheg_NOUN i_PART weld_VERB pa_PART rai_PRON sydd_VERB yn_PART barod_ADJ i_ADP gael_VERB 'u_PRON ffrwythlonni_VERB a_CONJ pha_DET rai_PRON sydd_VERB eisioes_VERB yn_PART beichiog_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
So_PART mama_NOUN 'na_ADV sawl_ADJ lle_NOUN ni_PRON 'n_PART gallu_VERB ga'l_VERB vein_NOUN ar_ADP fuwch_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ond_CONJ y_DET lle_NOUN hawsa'_ADJUNCT yw_VERB 'i_PRON chynffon_NOUN hi_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM ym_ADP ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Bechod_PROPN yn_PART  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ma_X '_PUNCT 'na_ADV lot_PRON rhwydda_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT os_CONJ nad_ADV '_PUNCT yn_PART nhw_PRON 'n_PART gweld_VERB chi_PRON so_ADJ
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Hefyd_ADV os_CONJ ti_PRON codi_VERB cynffon_NOUN buwch_NOUN mama_NOUN fel_ADP handbrake_NOUN on'd_X yw_VERB e_PRON ?_PUNCT
Stopio_VERB 'i_PRON mwyn_NOUN neu_CONJ lai_ADJ ._PUNCT
-_PUNCT F_PART rhan_NOUN amla_ADJ mama_VERB nhw_PRON 'n_PART sefyll_VERB ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Woah_X bach_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Woah_X bach_ADJ ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ti_PRON 'di_PART ga'l_VERB cic_NOUN erioed_ADV ?_PUNCT
ydw_VERB ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Unwaith_NOUN yr_DET w'thnos_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT yn_PART gofyn_VERB tarw_NOUN mama_VERB nhw_PRON 'n_PART neidio_VERB ar_ADP cefn_NOUN 'u_PRON gilydd_NOUN so_VERB ma_VERB 'r_DET fuwch_NOUN ar_ADP y_DET gwaelod_NOUN ._PUNCT
741.740_VERB N_NUM /_PUNCT A_CONJ
1421.952_ADJ
