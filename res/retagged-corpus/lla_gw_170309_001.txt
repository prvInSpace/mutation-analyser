Fasech_AUX chi_PRON 'n_PART neud_VERB cymwynas_NOUN hefo_ADP fi_PRON a_PART arddangos_VERB poster_NOUN os_CONJ gwelwch_VERB yn_PART dda_ADJ ?_PUNCT
Dibynnu_VERB beth_PRON yw_VERB 'r_DET poster_NOUN am_ADP ._PUNCT
Cymmrodorion_VERB Caerdydd_PROPN ._PUNCT
O_ADP shwr_NOUN o_ADP fod_VERB [_PUNCT chwerthin_VERB ]_PUNCT
Cwmni_NOUN gwbl_ADV barchus_ADJ ._PUNCT
Ond_CONJ ma_VERB rhaid_VERB cael_VERB gofyn_VERB beth_PRON yw_VERB e_PRON gynta_PART yn_PART does_VERB e_DET achos_ADP chimod_ADV sai_VERB ishe_VERB cytuno_VERB i_PART arddangos_VERB rhywbeth_NOUN sydd_VERB ddim_PART yn_PART barchus_ADJ ._PUNCT
Na_INTJ does_VERB y_DET [_PUNCT saib_NOUN ]_PUNCT neb_PRON yn_PART torri_VERB 'r_DET gyfraith_NOUN nag_CONJ yn_PART [_PUNCT saib_NOUN ]_PUNCT [_PUNCT -_PUNCT ]_PUNCT
Chi_PRON gyd_ADP yn_PART gwbod_VERB fel_CONJ i_PART fihafio_VERB [_PUNCT -_PUNCT ]_PUNCT
Cymryd_VERB rhan_NOUN mewn_ADP unrhyw_DET weithgareddau_NOUN amwys_ADJ ._PUNCT
Ie_INTJ na_PART ma_VERB hwnna_PRON 'n_PART iawn_ADJ [_PUNCT chwerthin_VERB ]_PUNCT ma_VERB hwnna_PRON 'n_PART dair_NUM punt_NOUN a_CONJ thrideg_NUM chwech_NUM ceiniog_NOUN de_NOUN plis_INTJ ._PUNCT
Reit_PROPN
yn_PART enwedig_ADJ gan_CONJ fod_VERB enwg_VERB yn_PART mmm_PRON darlithio_VERB ,_PUNCT fydd_VERB e_PRON 'n_PART [_PUNCT saib_NOUN ]_PUNCT boi_NOUN lleol_ADJ fel_ADP ,_PUNCT bydd_VERB e_PRON 'n_PART dishgwl_ADJ ar_ADP ei_DET enw_NOUN yn_ADP y_DET ffenest_NOUN ife_ADV ._PUNCT
Bydd_VERB bydd_VERB [_PUNCT saib_NOUN ]_PUNCT [_PUNCT anadlu_VERB ]_PUNCT rwy_VERB 'n_PART siŵr_ADJ fydd_AUX o_PRON 'n_PART siarad_VERB gydag_ADP arddeliad_NOUN [_PUNCT saib_NOUN ]_PUNCT na_CONJ ni_PRON
Na_VERB chi_PRON ,_PUNCT chi_PRON ishe_VERB cwdyn_NOUN bach_ADJ ?_PUNCT
Chi_INTJ ishe_VERB ?_PUNCT
Na_INTJ mae_VERB 'n_PART iawn_ADJ ._PUNCT
Chi_PRON 'n_PART siŵr_ADJ ?_PUNCT
Diolch_INTJ yn_PART fawr_ADJ [_PUNCT saib_NOUN ]_PUNCT [_PUNCT til_NOUN yn_PART agor_VERB a_CONJ chau_NOUN ]_PUNCT dw_AUX i_PRON 'n_PART credu_VERB fy_PRON mod_AUX wedi_PART derbyn_VERB e_DET bost_NOUN gynnoch_VERB chi_PRON ar_ADP ran_NOUN y_DET Cymmrodorion_PROPN yn_PART gofyn_VERB byddech_VERB chi_PRON 'n_PART cael_VERB recordio_VERB un_NUM o_ADP 'n_PART <_SYM til_NOUN ac_CONJ arian_NOUN >_DET pwyllgorau_NOUN ni_PRON ._PUNCT
Ie_INTJ ie_INTJ ie_INTJ [_PUNCT -_PUNCT ]_PUNCT dyna_DET fe_NOUN +_SYM
+_VERB Mi_PART fyddai_VERB 'n_PART ._PUNCT
Fi_PRON 'n_PART falch_ADJ bo_AUX chi_PRON wedi_PART clywed_VERB amdanom_ADP ni_PRON ,_PUNCT on_ADP ni_PRON mynd_VERB i_PART rhoi_VERB taflen_NOUN wybodaeth_NOUN i_ADP chi_PRON +_SYM
+_VERB O_ADP iawn_ADV ._PUNCT
Ond_CONJ os_CONJ chi_PRON 'n_PART gwbod_VERB +_SYM
+_VERB Ny_ADP ni_PRON ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT
Mi_PART fyddai_AUX 'n_PART ei_DET drafod_VERB o_ADP efo_ADP 'r_DET aelodau_NOUN ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ iawn_ADV ._PUNCT
Achos_CONJ ie_INTJ enghreifftie_VERB o_ADP 'r_DET Gymraeg_PROPN yn_PART cael_VERB ei_PRON siarad_VERB yn_PART naturiol_ADJ i_ADP chi_PRON moin_NOUN yndyfe_VERB ._PUNCT
Ie_INTJ ie_PRON yn_PART union_ADJ ie_INTJ ._PUNCT
Na_VERB chi_PRON ,_PUNCT ond_CONJ y_DET ,_PUNCT nai_PART rhoi_VERB nhw_PRON lan_ADV yn_ADP y_DET ffenest_NOUN ,_PUNCT mae_VERB 'r_DET [_NOUN aneglur_ADJ ]_PUNCT lawr_ADV achos_CONJ ma_VERB hwnna_PRON 'di_PART bod_VERB yndiwe_NOUN so_ADV [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Dyna_ADV ni_PRON ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ i_ADP chi_PRON ._PUNCT
Wel_CONJ nes_CONJ i_ADP ddim_PART rhegi_VERB ._PUNCT
Naddo_VERB ._PUNCT
Digon_ADV o_ADP amser_NOUN ,_PUNCT so_VERB chi_PRON mas_NOUN o_ADP 'r_DET drws_NOUN to_ADJ [_PUNCT chwerthin_VERB ]_PUNCT welai_VERB chi_PRON ,_PUNCT diolch_VERB yn_PART fawr_ADJ +_SYM
+_VERB Hwyl_INTJ ._PUNCT
Hwyl_INTJ +_SYM
+_VERB Diolch_INTJ ._PUNCT
Ta_CONJ ra_NOUN ._PUNCT
104.815_NOUN
