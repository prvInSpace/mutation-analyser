﻿_VERB s_DET 0.0_NOUN S1_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ydi_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT washed_VERB out_ADP ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT bod_VERB wrthi_ADP 'n_PART ddygn_ADJ ._PUNCT
Hmm_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT eto_ADV w'snos_NOUN yma_ADV [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Dyn_NOUN sy_VERB 'di_PART ca'l_VERB y_DET job_NOUN /_PUNCT en_DET >_SYM ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT bag_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Rhaid_VERB mi_PRON fynd_VERB â_ADP bag_NOUN newydd_ADJ ._PUNCT
Ia_INTJ ._PUNCT
Ge_VERB 's_PROPN i_PRON gadw_VERB llall_ADV bora_VERB 'ma_ADV torri_VERB hyd_NOUN y_DET llawr_NOUN ._PUNCT
Ma_AUX 'n_PART gweithio_VERB i_ADP [_PUNCT saib_NOUN ]_PUNCT gwmni_NOUN o_ADP 'r_DET enw_NOUN sefydliad_NOUN ar_ADP y_DET funud_NOUN sy_AUX 'n_PART delio_VERB efo_ADP bobol_ADJ +_SYM
Ââ_VERB ia_PRON ?_PUNCT
+_SYM ifanc_ADJ  _SPACE heb_ADP '_PUNCT n'unlla_PUNCT i_ADP fyw_VERB a_CONJ ballu_VERB ._PUNCT
Ia_INTJ ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART meddwl_VERB bobo_VERB enwb_NOUN 'di_PART d'eud_VERB bod_VERB o_PRON 'n_PART dŵad_NOUN o_ADP lleoliad_NOUN ._PUNCT
[_PUNCT Rhywbeth_NOUN yn_PART coginio_VERB mewn_ADP padell_NOUN ]_PUNCT ._PUNCT
<_SYM aneglur_ADJ 3_NUM >_SYM ._PUNCT
Ia_INTJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
A_CONJ wedyn_ADV  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ond_CONJ fo_PRON sy_AUX 'n_PART neud_VERB referrals_NOUN a_CONJ ballu_VERB fan'no_SYM [_PUNCT saib_NOUN ]_PUNCT So_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ma'_VERBUNCT enwb_NOUN 'di_PART gofyn_ADV dibynnu_VERB pryd_ADV ma_AUX 'n_PART gweithio_VERB  _SPACE dechra'_VERBUNCT efo_ADP ni_PRON '_PUNCT lly_X ydi_ADV [_PUNCT =_SYM ]_PUNCT bod_VERB o_PRON [_PUNCT /=_PROPN ]_PUNCT bobo_AUX fi_PRON 'n_PART mynd_VERB i_ADP '_PUNCT ista'_NOUNUNCT ati_ADP hi_PRON drws_NOUN nesa'_ADJUNCT [_PUNCT saib_NOUN ]_PUNCT a_CONJ fo_PRON 'n_PART ca'l_VERB '_PUNCT ista'_NOUNUNCT mewn_ADP efo_ADP 'r_DET genod_NOUN de_NOUN i_PART ddechra'_VERBUNCT arni_ADP de_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Nes_CONJ '_PUNCT dan_ADP ni_PRON 'n_PART gw'bod_VERB lle_ADV mama_NOUN desgs_NOUN yn_PART mynd_VERB ._PUNCT
Ond_CONJ '_PUNCT nes_CONJ i_PART feddwl_VERB de_NOUN mama_NOUN enwb_NOUN cyfenw_NOUN wedi_PART bod_VERB yn_PART '_PUNCT ista'_NOUNUNCT yn_PART desg_ADJ hi_PRON ._PUNCT
Ma'_VERBUNCT [_PUNCT aneglur_ADJ ]_PUNCT 'di_PART bod_VERB wrth_ADP desg_NOUN enwb_NOUN enwb_NOUN desg_ADJ hi_PRON ._PUNCT
Ah_ADP lle_NOUN '_PUNCT dw_VERB i_PRON am_PART fynd_VERB '_PUNCT ta_NOUN ?_PUNCT
Rhaid_VERB mi_PRON fynd_VERB i_ADP lle_NOUN enwb_NOUN bydd_VERB ?_PUNCT
[_PUNCT Caead_PROPN yn_PART cael_VERB ei_DET osod_VERB ar_ADP bot_NOUN ]_PUNCT ._PUNCT
Wel_INTJ [_PUNCT =_SYM ]_PUNCT 'di_PART [_PUNCT /=_PROPN ]_PUNCT 'di_PART [_PUNCT aneglur_ADJ ]_PUNCT ddim_PART yn_PART dŵad_NOUN yna_ADV ond_CONJ y_DET chdi_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT mama_NOUN stiwdant_NOUN yn_ADP 'i_PRON  _SPACE desg_ADJ hi_PRON '_PUNCT dydi_VERB ?_PUNCT
Ââ_INTJ ._PUNCT
Ynde_ADV ?_PUNCT
E'lla'_VERBUNCT gei_PRON di_PRON dy_DET redundance_NOUN <_SYM aneglur_ADJ 2_NUM >_SYM ._PUNCT
[_PUNCT Curo_PROPN dwylo_NOUN ]_PUNCT ._PUNCT
'_PUNCT S'na_NOUN 'm_DET lle_NOUN i_ADP fi_PRON '_PUNCT elly_ADV ?_PUNCT
Gwna_AUX fi_PRON 'n_PART redundant_VERB ._PUNCT
Ww_ADP ._PUNCT
enwg_VERB ._PUNCT
[_PUNCT Bwyd_PROPN yn_PART ffrïo_NOUN ]_PUNCT ._PUNCT
Ond_CONJ y_DET peth_NOUN ydi_VERB de_NOUN +_SYM
Ydi_NOUN bebe_NOUN ?_PUNCT
+_VERB desg_NOUN enwb_NOUN petha_VERB computers_NOUN bach_ADJ 'na_ADV sy_VERB 'na_ADV ._PUNCT
Rhaid_VERB ti_PRON ga'l_VERB  _SPACE [_PUNCT -_PUNCT ]_PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ydi_VERB un_NUM person_NOUN newydd_ADJ de_NOUN ._PUNCT
Hmm_INTJ ._PUNCT
Ond_CONJ ti_PRON 'n_PART gw'bod_VERB efo_ADP 'r_DET busnas_NOUN symud_VERB ffôn_NOUN a_CONJ ballu_VERB '_PUNCT ŵan_NOUN ?_PUNCT
Ia_INTJ ._PUNCT
D'eud_VERB bobo_VERB fi_PRON yn_PART mynd_VERB i_ADP desg_NOUN arall_ADJ '_PUNCT dw_AUX i_PRON 'n_PART mynd_VERB â_ADP ffôn_NOUN efo_ADP fi_PRON '_PUNCT dydw_VERB '_PUNCT ?_PUNCT
'_PUNCT Swn_PROPN i_PRON 'n_PART feddwl_VERB de_NOUN ._PUNCT
Mae_VERB o_PRON 'n_PART linked_NOUN i_ADP computer_NOUN chdi_NOUN '_PUNCT dydi_PRON ?_PUNCT
Ydi_NOUN ._PUNCT
Ond_CONJ '_PUNCT dydi_AUX 'r_DET computer_NOUN ddim_PART yn_PART linked_NOUN '_PUNCT ta_CONJ ti_PRON 'n_PART iwsio_VERB 'r_DET un_NUM un_NUM ?_PUNCT
[_PUNCT Bwyd_PROPN yn_PART ffrïo_NOUN ]_PUNCT ._PUNCT
'_PUNCT M'bobo_NOUN ._PUNCT
[_PUNCT Paratoi_VERB bwyd_NOUN mewn_ADP cegin_NOUN ]_PUNCT ._PUNCT
Ac_CONJ o'dd_NOUN 'na_ADV dynas_NOUN  _SPACE hyn_PRON hefyd_ADV 'di_PART trio_VERB ac_CONJ o'dd_VERB hi_PRON 'n_PART gwitsiad_NOUN pan_CONJ '_PUNCT nes_CONJ i_PART fynd_VERB â_ADP 'r_DET post_NOUN ._PUNCT
Dyma_DET enwb_NOUN yn_PART d'eud_VERB "_PUNCT Oes_VERB 'na_ADV r'wun_NOUN arall_ADJ 'di_PART cyrra'dd_NOUN ?_PUNCT "_PUNCT ._PUNCT
Ah_INTJ [_PUNCT =_SYM ]_PUNCT mama_NOUN [_PUNCT /=_PROPN ]_PUNCT mama_NOUN enwg_VERB yn_PART neud_VERB y_DET post_NOUN me_ADP '_PUNCT fi_PRON ._PUNCT
'_PUNCT Isio_PROPN '_PUNCT fi_PRON ofyn_VERB '_PUNCT ddo_CONJ fo_PRON ?_PUNCT
Ia_INTJ ._PUNCT
[_PUNCT Curo_PROPN dwylo_NOUN ]_PUNCT ._PUNCT
Ti_PRON 'm_DET yn_PART cofio_VERB enw_NOUN 'r_DET pantomeim_NOUN ?_PUNCT
Hmm_INTJ ._PUNCT
So_INTJ '_PUNCT nes_CONJ i_ADP  _SPACE fynd_VERB yn_PART ôl_NOUN deu_X '_PUNCT '_PUNCT thi_PRON a_CONJ '_PUNCT nes_CONJ i_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Swn_PROPN i_PRON 'n_PART d'eud_VERB bod_VERB hi_PRON fath_NOUN â_ADP '_PUNCT r'un_NOUN oed_NOUN a_CONJ fi_PRON neu_CONJ 'n_PART hŷn_ADJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
A_CONJ '_PUNCT dyn_NOUN  _SPACE [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT hi_PRON '_PUNCT chos_NOUN dyma_DET hi_PRON 'n_PART d'eud_VERB "_PUNCT Ââ_X '_PUNCT dw_VERB i'sio_VERB un_NUM arall_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT "_PUNCT [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
[_PUNCT Paratoi_VERB bwyd_NOUN mewn_ADP cegin_NOUN ]_PUNCT ._PUNCT
Da_ADJ 'th_PRON rh_NOUN 'w_PRON foi_NOUN ifanc_ADJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Cadw_VERB 'r_DET wy_NOUN ?_PUNCT
+_VERB  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Eh_PART ?_PUNCT
Cadw_VERB 'r_DET wy_NOUN ia_DET ?_PUNCT
BeBe_AUX ti_PRON 'n_PART dd'eud_VERB ?_PUNCT
'_PUNCT T'isio_VERB '_PUNCT cadw_VERB 'r_DET wy_NOUN 'na_ADV ?_PUNCT
Ia_INTJ ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART meddwl_VERB na_PART cyllall_VERB ne_NOUN '_PUNCT '_PUNCT wbath_NOUN dd_VERB '_PUNCT ud_X '_PUNCT is_CONJ di_PRON ._PUNCT
[_PUNCT Paratoi_VERB bwyd_NOUN ]_PUNCT ._PUNCT
ym_ADP ._PUNCT
Boi_NOUN ifanc_ADJ oedd_VERB o_PRON 'n_PART ditsyr_ADJ ._PUNCT
[_PUNCT Sŵn_NOUN hulio_VERB bwrdd_NOUN ]_PUNCT ._PUNCT
'_PUNCT Na'thon_VERB ni_PRON 'm_DET gweld_VERB neb_PRON arall_ADJ ._PUNCT
Helo_INTJ ._PUNCT
Pump_NUM o_ADP 'n_PART ca'l_VERB interview_ADV ._PUNCT
Ia_INTJ ._PUNCT
'_PUNCT Oeddat_PROPN ti_PRON 'n_PART d'eud_VERB amsar_NOUN cinio_NOUN ._PUNCT
A_CONJ wedyn_ADV o_ADP 'n_PART i_PRON '_PUNCT isio_VERB '_PUNCT holi_VERB sud_PROPN hwyl_NOUN oedd_VERB enwb_NOUN 'di_PART gael_VERB on_X '_PUNCT ge_NOUN 's_DET i_ADP 'm_DET chance_VERB '_PUNCT c'os_NOUN o'dd_NOUN y_DET ddynas_NOUN  _SPACE sefydliad_NOUN efo_ADP nhw_PRON de_NOUN ._PUNCT
Ia_INTJ ._PUNCT
"_PUNCT Wel_CONJ bydd_VERB r'aid_NOUN i_ADP fi_PRON ddod_VERB '_PUNCT ista'_NOUNUNCT mewn_ADP a_CONJ gweld_VERB bebe_NOUN '_PUNCT dach_AUX chi_PRON 'n_PART neud_VERB "_PUNCT meddai_VERB ._PUNCT
Ia_INTJ ._PUNCT
A_CONJ fi_PRON 'n_PART meddwl_VERB w_ADP 'th_NOUN yn_PART hun_NOUN "_PUNCT ia_ADP ti_PRON 'n_PART mynd_VERB i_ADP ga'l_VERB dipyn_PRON o_ADP sioc_NOUN "_PUNCT ._PUNCT
Medda'_VERBUNCT pwy_PRON ?_PUNCT
Medda_VERB 'r_DET hogan_NOUN sefydliad_NOUN '_PUNCT mama_NOUN c'os_NOUN '_PUNCT dw_AUX i_ADP 'm_DET yn_PART gw'bod_VERB os_CONJ 'di_PART hi_PRON 'n_PART meddwl_VERB bod_AUX hi_PRON 'n_PART mynd_VERB i_PART dd'eud_VERB +_SYM
[_PUNCT Rhywun_PROPN yn_PART symud_VERB cadair_NOUN ]_PUNCT ._PUNCT
+_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT bebe_NOUN i_ADP neud_VERB '_PUNCT ta_CONJ bebe_NOUN ._PUNCT
Ia_INTJ ._PUNCT
[_PUNCT Sŵn_NOUN metel_NOUN yn_PART taro_VERB rhywbeth_NOUN ]_PUNCT ._PUNCT
Hmm_INTJ [_PUNCT saib_NOUN ]_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Achos_CONJ os_CONJ 'di_PART hwn_PRON yn_PART dynamic_NOUN a_CONJ ballu_VERB de_NOUN [_PUNCT saib_NOUN ]_PUNCT '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB '_PUNCT ydd_NOUN y_DET genod_VERB yn_PART teimlo_VERB bach_ADJ  _SPACE [_PUNCT saib_NOUN ]_PUNCT no_PRON [_PUNCT -_PUNCT ]_PUNCT noses_VERB out_ADP of_X joint_X de_NOUN ._PUNCT
[_PUNCT Llestri_PROPN yn_PART tincial_ADJ ]_PUNCT ._PUNCT
Sud_NOUN oedd_VERB gwynab_NOUN enwb_NOUN ?_PUNCT
O_ADP 'n_PART i_PRON 'm_DET yn_PART gweld_VERB ._PUNCT
[_PUNCT Diod_NOUN yn_PART cael_VERB ei_DET dywallt_VERB ]_PUNCT ._PUNCT
Dyn_NOUN ._PUNCT
Ww_ADP ._PUNCT
Hmm_INTJ ._PUNCT
Ma_AUX 'i_PRON 'n_PART d'eud_VERB bod_VERB 'na_ADV  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
T'isio_VERB '_PUNCT fi_PRON gym'yd_SYM hwnna_PRON 'n_PART ôl_NOUN '_PUNCT ŵan_NOUN ?_PUNCT
Bod_VERB 'na_ADV  _SPACE [_PUNCT saib_NOUN ]_PUNCT fisticuffs_VERB 'di_PART bod_AUX yn_PART  _SPACE swyddfa_NOUN enwg_VERB +_SYM
Ia_INTJ ?_PUNCT ._PUNCT
+_VERB 'di_PART gorfod_ADV separetio_VERB 'r_DET ddau_NUM '_PUNCT nyn_NOUN nhw_PRON ._PUNCT
BeBe_VERB 'na_ADV 'th_PRON ddigwydd_NOUN ?_PUNCT
Jest_ADV '_PUNCT wbath_NOUN am_ADP r_NOUN 'w_PRON bres_VERB te_NOUN ne_NOUN '_PUNCT '_PUNCT wbath_NOUN ._PUNCT Ro_AUX 'n_PART nhw_PRON 'n_PART gorfod_ADV rhoi_VERB pum_NUM punt_NOUN se_PRON '_PUNCT [_PUNCT =_SYM ]_PUNCT -_PUNCT pun_VERB [_PUNCT /=_PROPN ]_PUNCT punt_NOUN y_DET d'wrnod_NOUN a_CONJ mama_NOUN 'na_ADV tua_ADV tri_NUM deg_NUM o_ADP bobol_NOUN yn_ADP yr_DET un_NUM swyddfa_NOUN [_PUNCT cnoi_VERB ]_PUNCT ._PUNCT
A'd_VERB ydi_ADV enwg_VERB ddim_PART yn_PART -_PUNCT tor_NOUN  _SPACE yn_PART talu_VERB '_PUNCT c'os_NOUN [_PUNCT =_SYM ]_PUNCT 'di_PART o_ADP [_PUNCT /=_PROPN ]_PUNCT 'di_PART o_ADP 'm_DET yn_PART yfad_NOUN te_NOUN [_PUNCT saib_NOUN ]_PUNCT na_CONJ coffi_NOUN ._PUNCT
A_CONJ [_PUNCT cnoi_VERB ]_PUNCT ma_VERB 'r_DET ddynas_NOUN 'ma_ADV sy_AUX 'n_PART gweithio_VERB yna_ADV [_PUNCT aneglur_ADJ ]_PUNCT yn_PART codi_VERB twrw_NOUN o_ADP hyd_NOUN de_NOUN ._PUNCT
Hmm_INTJ ._PUNCT
A_CONJ dyma_ADV hi_PRON 'n_PART d'eud_VERB w_ADP 'th_NOUN y_DET boi_NOUN 'ma_ADV ah_X o'dd_NOUN hi_PRON '_PUNCT isio_VERB '_PUNCT 'i_PRON -_PUNCT ph_VERB pum_NUM punt_NOUN [_PUNCT =_SYM ]_PUNCT am_ADP [_PUNCT /=_PROPN ]_PUNCT am_ADP yr_DET  _SPACE [_PUNCT saib_NOUN ]_PUNCT te_NOUN a_CONJ coffi_NOUN ._PUNCT
So_PART dyma_PRON fo_PRON 'n_PART deu_VERB '_PUNCT '_PUNCT thi_PRON [_PUNCT saib_NOUN ]_PUNCT "_PUNCT '_PUNCT Dw_VERB i_PRON '_PUNCT m'ond_NOUN yn_PART gweithio_VERB tri_NUM d'wrnod_NOUN yr_DET w'snos_NOUN ._PUNCT
Tri_PART punt_AUX ti_PRON 'n_PART ga'l_VERB ._PUNCT "_PUNCT
Ââ_VERB ia_PRON ._PUNCT
Wedyn_ADV ma_AUX 'i_PRON 'di_PART mynd_VERB i_PART dd'eud_VERB w_ADP 'th_PRON r_ADP 'w_PRON foi_NOUN arall_ADJ  _SPACE "_PUNCT Dos_VERB i_ADP nol_NOUN pum_NUM punt_NOUN  _SPACE te_NOUN a_CONJ coffi_NOUN gin_NOUN y_DET boi_NOUN 'ma_ADV "_PUNCT ne_NOUN '_PUNCT r'wbath_NOUN ._PUNCT
A_PART ma_AUX 'i_PRON 'di_PART mynd_VERB yn_PART ffrae_ADJ [_PUNCT cnoi_VERB ]_PUNCT ._PUNCT
Hmm_INTJ ._PUNCT
Anodd_ADJ gw'bod_VERB bebe_NOUN i_ADP neud_VERB de_NOUN ._PUNCT
A_CONJ '_PUNCT dyn_NOUN ma_AUX 'i_PRON 'di_PART rhoi_VERB cwyn_NOUN i_ADP mewn_ADP '_PUNCT ŵan_NOUN ne_NOUN '_PUNCT '_PUNCT ba_PRON '_PUNCT [_PUNCT cnoi_VERB ]_PUNCT ._PUNCT
A_CONJ ma_VERB 'r_DET hogan_NOUN arall_ADJ 'ma_ADV 'di_PART d'eud_VERB '_PUNCT wbath_NOUN w_ADP 'th_NOUN enwg_VERB a_CONJ '_PUNCT dyn_NOUN ma_AUX 'n_PART d'eud_VERB "_PUNCT '_PUNCT Dw_AUX i_PRON 'di_PART wornio_VERB chdi_NOUN e_PRON 's_DET flwyddyn_NOUN dwytha_VERB na_CONJ hon_PRON sy_AUX 'n_PART codi_VERB twrw_NOUN mynd_VERB tu_NOUN ôl_NOUN i_ADP cefn_NOUN bobol_ADJ ._PUNCT
Ti_PRON 'm_DET 'di_PART g'rando_NOUN arnaf_ADP fi_PRON ._PUNCT
Ti_PRON 'n_PART gweld_VERB o_ADP '_PUNCT chdi_NOUN dy_DET hun_NOUN '_PUNCT ŵan_NOUN ._PUNCT "_PUNCT [_PUNCT saib_NOUN ]_PUNCT Pw_NOUN '_PUNCT bynnag_PRON ydi_VERB ._PUNCT
Medda'_VERBUNCT enwg_VERB ._PUNCT
Ia_INTJ ._PUNCT
[_PUNCT Swn_PROPN crafu_VERB plât_NOUN ]_PUNCT ._PUNCT
[_PUNCT sniffian_VERB ]_PUNCT '_PUNCT Nei_PROPN di_PRON dd'eud_VERB w_ADP 'th_PRON enwb_NOUN +_SYM
Ia_ADJ [_PUNCT cnoi_VERB ]_PUNCT ._PUNCT
+_SYM <_SYM pesychu_VERB >_DET mama_NOUN 'na_ADV Volvo_VERB XE_X Forty_X allan_ADV '_PUNCT ŵan_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Ma_VERB 'n_PART llai_ADJ na_PART un_NUM enwb_NOUN ._PUNCT
Ia_INTJ ._PUNCT
So_INTJ '_PUNCT sa_PRON 'n_PART [_PUNCT saib_NOUN ]_PUNCT brafiach_NOUN peth_NOUN de_NOUN ._PUNCT
Hmm_INTJ [_PUNCT cnoi_VERB ]_PUNCT ._PUNCT
Rhai_PRON '_PUNCT ti_PRON atgoffa_VERB fi_PRON i_PART ddeu_VERB '_PUNCT '_PUNCT thi_PRON hi_PRON  _SPACE [_PUNCT saib_NOUN ]_PUNCT w'snos_NOUN nesa'_ADJUNCT ._PUNCT
[_PUNCT Swn_PROPN crafu_VERB plât_NOUN ]_PUNCT ._PUNCT
'_PUNCT Oi_NOUN computer_NOUN di_PRON off_ADP de_NOUN ._PUNCT
Ti_PRON 'n_PART cadw_VERB cwpana_VERB ._PUNCT
Cadw_VERB llefrith_NOUN [_PUNCT aneglur_ADJ ]_PUNCT 'na_ADV off_NOUN enwb_NOUN jest_ADV '_PUNCT ista'_NOUNUNCT 'na_ADV w_DET 'th_NOUN nesg_ADJ i_PRON [_PUNCT saib_NOUN ]_PUNCT "_PUNCT Sori_X "_PUNCT medda_VERB 'i_PRON ._PUNCT
"_PUNCT Neidio_VERB mewn_ADP i_ADP 'ch_PRON lle_NOUN chi_PRON "_PUNCT ._PUNCT
"_PUNCT Ia_INTJ 'di_PART 'n_PART sêt_NOUN i_ADP 'm_DET yn_PART oer_ADJ hyd_NOUN 'n_PART oed_NOUN eto_ADV "_PUNCT medda'_VERBUNCT fi_PRON ._PUNCT
[_PUNCT Swn_PROPN crafu_VERB plât_NOUN ]_PUNCT ._PUNCT
A_CONJ '_PUNCT dyn_NOUN p_ADJ '_PUNCT n_PRON '_PUNCT awn_VERB 'ma_ADV [_PUNCT =_SYM ]_PUNCT o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT o'dd_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Pan_CONJ ddois_VERB i_PRON 'n_PART ôl_NOUN o_ADP nghinio_NOUN "_PUNCT Ah_X dio_VERB 'ch_PRON byth_ADV ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'n_PART ôl_NOUN "_PUNCT ._PUNCT
"_PUNCT BeBe_NOUN sy_VERB ?_PUNCT "_PUNCT ._PUNCT
"_PUNCT 'Di_PART 'r_DET printar_NOUN '_PUNCT im_PRON yn_PART gweithio_VERB "_PUNCT ._PUNCT
[_PUNCT Swn_PROPN crafu_VERB plât_NOUN ]_PUNCT ._PUNCT
Ag_ADP o'dd_NOUN hi_PRON '_PUNCT isio_VERB '_PUNCT [_PUNCT saib_NOUN ]_PUNCT printio_VERB 'r_DET cwestiyna_ADJ ar_ADP gyfar_NOUN yr_DET interviews_NOUN de_NOUN ._PUNCT
Ââ_VERB ia_PRON enwb_NOUN '_PUNCT ŵan_NOUN ?_PUNCT
Ia_INTJ ._PUNCT
So_NOUN pan_CONJ '_PUNCT nes_CONJ i_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
"_PUNCT Ah_ADP gwitsiad_NOUN [_PUNCT =_SYM ]_PUNCT -_PUNCT Ga_PROPN [_PUNCT /=_PROPN ]_PUNCT ga_ADP 'i_PRON dynnu_VERB nghôt_NOUN ?_PUNCT "_PUNCT me_ADP '_PUNCT fi_PRON de_NOUN ._PUNCT
Iawn_INTJ '_PUNCT ta_X Off_PROPN a_CONJ fi_PRON ._PUNCT
Y_DET ddwy_NUM 'n_PART sefyll_VERB yna_ADV enwb_NOUN a_CONJ hi_PRON ._PUNCT
'_PUNCT M'ond_NOUN jest_ADV neud_VERB hyn_PRON 'na_ADV ._PUNCT
So_INTJ '_PUNCT nes_CONJ i_PART bwyso_VERB r_ADP 'w_PRON fotyma_VERB [_PUNCT saib_NOUN ]_PUNCT a_CONJ '_PUNCT im_AUX byd_NOUN yn_PART digwydd_VERB ._PUNCT
[_PUNCT Swn_PROPN crafu_VERB plât_NOUN ]_PUNCT ._PUNCT
So_INTJ '_PUNCT nes_CONJ i_PART roid_VERB o_ADP off_NOUN [_PUNCT saib_NOUN ]_PUNCT ar_ADP y_DET machine_NOUN ._PUNCT
A_CONJ [_PUNCT saib_NOUN ]_PUNCT dyma_DET hi_PRON 'n_PART d'eud_VERB  _SPACE [_PUNCT saib_NOUN ]_PUNCT "_PUNCT Troi_VERB o_ADP off_NOUN yn_PART fan'na_PUNCT ia_INTJ ?_PUNCT "_PUNCT medda_VERB 'i_PRON ._PUNCT
"_PUNCT Na_INTJ paid_VERB "_PUNCT medda'_VERBUNCT fi_PRON ._PUNCT
"_PUNCT Dim_DET i_ADP fod_VERB i_PART roi_VERB o_ADP off_NOUN yn_PART wal_NOUN ._PUNCT "_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
A_CONJ [_PUNCT =_SYM ]_PUNCT dyma_ADV hi_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT dyma_DET hi_PRON 'n_PART d'eud_VERB '_PUNCT wbath_NOUN ._PUNCT
A_CONJ dyma_ADV fi_PRON 'n_PART d'eud_VERB "_PUNCT Wel_PART gwna_VERB di_PRON '_PUNCT ta_X "_PUNCT medda'_VERBUNCT fi_PRON ._PUNCT
"_PUNCT Wedyn_X fingerprints_NOUN chdi_NOUN fydd_VERB arno_ADP fo_PRON dim_PART fi_PRON ._PUNCT "_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
'_PUNCT Na_VERB 'th_PRON hi_PRON 'm_DET neud_VERB ._PUNCT
A_CONJ wedyn_ADV Duw_NOUN ge_NOUN 's_DET i_PRON o_ADP i_PART weithio_VERB [_PUNCT chwerthin_VERB ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Hmm_INTJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
O_ADP '_PUNCT 'i_PRON 'di_PART printio_VERB tua_ADV igian_VERB ac_CONJ o_ADP '_PUNCT 'na_ADV gwalla_ADV sillafu_VERB '_PUNCT n'o_NOUN fo_PRON ._PUNCT
"_PUNCT BeBe_VERB 'na_ADV 'i_PRON efo_ADP rhein_NOUN ?_PUNCT "_PUNCT medda_VERB 'i_PRON ._PUNCT
"_PUNCT Ah_INTJ doro_VERB nhw_PRON ar_ADP desg_NOUN fi_PRON ._PUNCT
'_PUNCT Wna_VERB i_PRON iwsio_VERB nhw_PRON fath_NOUN a_CONJ sgrap_NOUN "_PUNCT me_ADP '_PUNCT fi_PRON ._PUNCT
[_PUNCT clirio_VERB ]_PUNCT ._PUNCT
[_PUNCT Clirio_VERB llestri_NOUN ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT y_DET becyn_NOUN [_PUNCT aneglur_ADJ ]_PUNCT 'na_ADV ._PUNCT
Un_NUM da'd_ADJ oedd_VERB ?_PUNCT
Mmm_VERB ._PUNCT
Lle_ADV ge_NOUN 's_X di_PRON hwn_PRON 'na_ADV [_PUNCT aneglur_ADJ ]_PUNCT ?_PUNCT
Tesco_VERB ._PUNCT
Finest_NOUN '_PUNCT ta_X +_SYM
Ah_INTJ na_PRON ._PUNCT
+_SYM normal_ADJ ?_PUNCT
'_PUNCT El_DET normal_ADJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB ._PUNCT
Trwch_VERB da'_ADJUNCT no_PRON fo_PRON ._PUNCT
O'dd_NOUN '_PUNCT m'ond_NOUN chwech_NUM pishin_NOUN yn_PART fan'na_ADJ ._PUNCT
Ia_INTJ ia_PRON ._PUNCT
O'dd_NOUN o_ADP werth_NOUN hynny_PRON [_PUNCT chwythu_VERB ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Eh_PART ?_PUNCT
'_PUNCT Dw_AUX i_PRON 'di_PART ca'l_VERB text_VERB yn_PART d'eud_VERB bobo_VERB nhw_PRON 'n_PART dwad_NOUN '_PUNCT fory_ADV ._PUNCT
Aye_NOUN ._PUNCT
[_PUNCT Swn_PROPN person_NOUN yn_PART symud_VERB o_ADP gwmpas_NOUN ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Ti_PRON bebe_NOUN ._PUNCT
'_PUNCT Na_VERB 'th_NOUN enwb_NOUN cym'yd_SYM referral_ADJ w'snos_NOUN yma_ADV ar_ADP ffôn_NOUN [_PUNCT saib_NOUN ]_PUNCT O'dd_NOUN enwb_NOUN bach_ADJ yn_PART stressed_VERB '_PUNCT c'os_NOUN o'dd_NOUN '_PUNCT isio_VERB '_PUNCT [_PUNCT saib_NOUN ]_PUNCT  _SPACE agor_VERB ar_ADP y_DET plentyn_NOUN 'ma_ADV t_PRON '_PUNCT '_PUNCT bobo_VERB [_PUNCT saib_NOUN ]_PUNCT ac_CONJ '_PUNCT im_PRON 'di_PART ca'l_VERB chance_VERB de_NOUN ._PUNCT
A_CONJ [_PUNCT saib_NOUN ]_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT "_PUNCT ah_ADP fy_PRON '_PUNCT raid_VERB ichi_ADP ofyn_VERB i_ADP rhieni_NOUN "_PUNCT medda_VERB 'i_PRON fel'a_SYM de_NOUN ._PUNCT
Pwy_PRON '_PUNCT ŵan_NOUN ?_PUNCT
enwb_VERB ?_PUNCT
enwb_VERB ._PUNCT
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
"_PUNCT We_NOUN '_PUNCT fedra_VERB 'i_PRON ddim_PART "_PUNCT medda'_VERBUNCT fo_PRON ._PUNCT
ym_ADP ._PUNCT
"_PUNCT '_PUNCT C'os_NOUN mae_VERB 'n_PART Pasg_ADJ '_PUNCT ŵan'd_PROPN ydi_ADV ?_PUNCT "_PUNCT medda'_VERBUNCT fo_PRON ._PUNCT
Ffonio_VERB ha_PRON 'w_PRON 'di_PART tri_NUM de_NOUN ._PUNCT
'Di_PART ca'l_VERB trw_NOUN '_PUNCT dydd_NOUN i_ADP ffonio'd_ADV oedd_VERB ?_PUNCT
Y_DET [_PUNCT aneglur_ADJ ]_PUNCT ?_PUNCT
Ia_INTJ ._PUNCT
Fucking_NOUN hell_ADV ._PUNCT
'_PUNCT el_DET bai_NOUN nhw_PRON felly_ADV de_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Yn_PART union_ADJ [_PUNCT cnoi_VERB ]_PUNCT ._PUNCT
O'dd_NOUN powb_NOUN arall_ADJ yn_PART d'eud_VERB  _SPACE bod_VERB 'i_PRON thad_NOUN hi_PRON 'di_PART bod_VERB yn_PART gas_NOUN efo_ADP 'i_PRON [_PUNCT =_SYM ]_PUNCT BeBe_PRON mae_VERB 'di_PART [_PUNCT /=_PROPN ]_PUNCT bebe_NOUN mae_VERB 'di_PART dd'eud_VERB ?_PUNCT
Ah_CONJ oedd_AUX hi_PRON 'n_PART cau_VERB a_CONJ d'eud_VERB '_PUNCT c'os_NOUN o'dd_NOUN o_PRON 'di_PART iwsio_VERB  _SPACE "_PUNCT nasty_NOUN words_NOUN "_PUNCT ._PUNCT
Wel_INTJ '_PUNCT dyn_NOUN nhw_PRON 'm_DET 'di_PART gofyn_VERB '_PUNCT im_NOUN mwy_ADJ na_CONJ 'm_DET byd_NOUN ._PUNCT
'Di_PART nhw_PRON 'm_DET yn_PART gwatsiad_NOUN  _SPACE petha_NOUN Coronation_NOUN Street_X nag_CONJ '_PUNCT dyn_NOUN ?_PUNCT
Wel_INTJ mama_NOUN plant_NOUN yn_PART ca'l_VERB syniada_NOUN o_ADP betha_VERB fel'na'd_NOUN ydi_ADV ?_PUNCT
Yndi_INTJ ._PUNCT
A_CONJ rheini_VERB '_PUNCT r'un_NOUN fath_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
A_CONJ wedyn_ADV +_SYM
'_PUNCT Dw_AUX i_PRON 'm_DET yn_PART lecio_VERB fo_PRON ._PUNCT
+_VERB [_PUNCT =_SYM ]_PUNCT ma_VERB 'i_PRON [_PUNCT /=_PROPN ]_PUNCT ma_AUX 'i_PRON 'di_PART roid_VERB o_ADP drwadd_NOUN i_ADP cyfeiriada_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
So_PART heno_ADV enwb_NOUN enwb_NOUN a_CONJ __NOUN dal_ADJ yna_ADV -_PUNCT y_DET teipio_VERB full_NOUN speed_NOUN ._PUNCT
So_PART fydd_VERB y_DET [_PUNCT -_PUNCT ]_PUNCT '_PUNCT heina_VERB gyd_CONJ yna_ADV dan_ADP bump_NUM heno_ADV bydd_VERB ?_PUNCT
Bydd_VERB ._PUNCT
[_PUNCT Swn_PROPN crafu_VERB plât_NOUN ]_PUNCT ._PUNCT
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
Os_CONJ '_PUNCT dw_VERB i_PRON ar_ADP day_NOUN off_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Os_CONJ '_PUNCT dw_VERB i_ADP yna_ADV a_CONJ ti_PRON ar_ADP day_NOUN off_ADP a_CONJ mama_NOUN enwb_NOUN a_CONJ enwb_NOUN yna_ADV '_PUNCT dw_AUX i_PRON 'n_PART mynd_VERB i_PART ofyn_VERB i_ADP __NOUN "_PUNCT Duw_AUX ti_PRON 'n_PART dod_VERB efo_ADP fi_PRON am_ADP dro_NOUN rownd_ADV y_DET bloc_NOUN ?_PUNCT "_PUNCT +_SYM
Hmm_INTJ ._PUNCT
+_VERB i_ADP cha'l_VERB hi_PRON o_ADP 'na_ADV +_SYM
Amsar_VERB cinio_NOUN ia_ADJ ?_PUNCT
+_VERB '_PUNCT chos_NOUN 'di_PART o_ADP 'm_DET yn_PART iach_ADJ bot_NOUN hi_PRON 'm_DET yn_PART mynd_VERB allan_ADV o_ADP gwbwl_NOUN de_NOUN ._PUNCT
[_PUNCT Swn_PROPN crafu_VERB plât_NOUN ]_PUNCT ._PUNCT
'_PUNCT Dw_AUX i_PRON 'di_PART ca'l_VERB chocolate_NOUN muffin_NOUN p_ADP '_PUNCT n_PRON '_PUNCT awn_VERB 'ma_ADV [_PUNCT saib_NOUN ]_PUNCT enwg_VERB 'di_PART prynu_VERB rhei_VERB i_ADP ni_PRON ._PUNCT
enwg_VERB ?_PUNCT
Ia_INTJ ._PUNCT
Pwy_PRON 'di_PART hwnnw_DET ?_PUNCT
Gwr_NOUN enwb_NOUN ._PUNCT
Ââ_INTJ ._PUNCT
Ma_AUX 'n_PART ca'l_VERB 'i_PRON benblwydd_NOUN '_PUNCT fory_ADV ._PUNCT
'_PUNCT Nes_CONJ di_PRON ddeu'd_PUNCT w_ADP 'th_NOUN enwb_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Penblwydd_NOUN nos_NOUN '_PUNCT fory_ADV ._PUNCT
'_PUNCT Dyn_NOUN mama_AUX nhw_PRON 'n_PART mynd_VERB de_NOUN ar_ADP yr_DET peth_NOUN roller_NOUN coaster_NOUN newydd_ADJ 'na_ADV yn_ADP ym_ADP lleoliad_NOUN ia_ADJ ?_PUNCT
Ââ_INTJ ._PUNCT
Yn_ADP y_DET goedwig_NOUN 'na_ADV ._PUNCT
Hmm_INTJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Wedyn_ADV mama_VERB nhw_PRON 'n_PART ca'l_VERB cinio_NOUN 'n_PART r'wla_NOUN a_CONJ wedyn_ADV mama_AUX nhw_PRON 'n_PART mynd_VERB i_ADP pictiwrs_NOUN yn_PART p_ADJ '_PUNCT n_PRON '_PUNCT awn_VERB ._PUNCT
Y_DET pedwar_NOUN o'nyn_ADP nhw_DET ?_PUNCT
Ia_ADJ [_PUNCT cnoi_VERB ]_PUNCT ._PUNCT
Ac_CONJ oedd_VERB 'di_PART d'eud_VERB w_ADP 'th_NOUN y_DET plant_NOUN [_PUNCT saib_NOUN ]_PUNCT ma_VERB 'r_DET hamster_NOUN 'di_PART marw_VERB ._PUNCT
Sy_VERB '_PUNCT [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ydi_PRON o_ADP 'di_PART marw_VERB ?_PUNCT
Do_INTJ ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
A_PART 'th_VERB hi_PRON a_CONJ bocs_NOUN tissues_NOUN bach_ADJ ciwb_NOUN adra_ADV '_PUNCT fo_PRON hi_PRON i_PART roid_VERB o_ADP 'n_PART hwnna_PRON i_ADP gladdu_VERB ._PUNCT
A_CONJ wedyn_ADV dyma_DET 'i_PRON d'eud_VERB w_ADP 'th_PRON enwb_NOUN ._PUNCT
'_PUNCT Na_VERB 'th_NOUN enwb_NOUN ddechra'_VERBUNCT crio_VERB 'n_PART syth_ADJ de_NOUN ._PUNCT
So_PART dyma_ADV hi_PRON 'n_PART d'eud_VERB '_PUNCT tho_CONJ fo_PRON "_PUNCT Ah_X ia_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT "_PUNCT [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ââ_INTJ ._PUNCT
'_PUNCT El_DET gawn_VERB nhw_PRON un_NUM arall_ADJ can_NOUN '_PUNCT ?_PUNCT
Hmm_INTJ ._PUNCT
Merch_NOUN enwb_NOUN 'di_PART gofyn_VERB os_CONJ geith_VERB hi_PRON gi_NOUN ._PUNCT
Ci_INTJ ?_PUNCT
Mmm_VERB ._PUNCT
Ma'_VERBUNCT gynnyn_ADP nhw_PRON gi_NOUN ._PUNCT
Oes_VERB ._PUNCT
Ma_VERB 'i_PRON '_PUNCT isio_VERB '_PUNCT ci_NOUN 'i_PRON hun_NOUN ._PUNCT
[_PUNCT clirio_VERB ]_PUNCT [_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
Ia_INTJ ._PUNCT
Dyna_ADV ddu'dodd_VERB enwb_NOUN de_NOUN ._PUNCT
"_PUNCT Dim_DET ots_NOUN ._PUNCT
Ga_NOUN 'i_PRON un_NUM arall_ADJ caf_ADV ?_PUNCT "_PUNCT
Hmm_INTJ ._PUNCT
[_PUNCT Tawelwch_PROPN ]_PUNCT ._PUNCT
[_PUNCT ocheneidio_VERB ]_PUNCT ._PUNCT
[_PUNCT Tawelwch_PROPN ]_PUNCT ._PUNCT
Ga_NOUN 'th_PRON [_PUNCT aneglur_ADJ ]_PUNCT dipyn_NOUN bach_ADJ o_ADP rash_NOUN medda'_VERBUNCT hi_PRON ._PUNCT
Mmm_VERB Jest_PROPN bod_AUX o_ADP ddim_PART yn_PART +_SYM
Mmm_VERB ?_PUNCT
+_SYM gwaethygu_VERB ddim_PART yn_PART gwella_VERB ._PUNCT
O'dd_NOUN enwg_VERB adra_ADV efo_ADP fo_PRON heddiw_ADV ._PUNCT
[_PUNCT torri_VERB ]_PUNCT [_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT 'na_ADV 'th_VERB hi_PRON gym'yd_PUNCT heddiw_ADV ._PUNCT
[_PUNCT Tawelwch_PROPN ]_PUNCT ._PUNCT
[_PUNCT ocheneidio_VERB ]_PUNCT ._PUNCT
[_PUNCT Tawelwch_PROPN ]_PUNCT ._PUNCT
'_PUNCT M_NUM bebe_NOUN o'dd_NOUN y_DET stiwdant_NOUN bach_ADJ 'na_ADV 'n_PART neud_VERB heddiw_ADV de_NOUN ._PUNCT
Pwy_PRON ?_PUNCT
enwg_VERB ?_PUNCT
O'dd_NOUN o_ADP 'n_PART teipio_VERB fel_ADP diawl_ADJ +_SYM
<_SYM pesychu_VERB >_SYM ._PUNCT
+_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT hefyd_ADV mama_NOUN teipio_VERB asesiad_NOUN i_ADP fyny_ADV mae_VERB o_PRON ._PUNCT
Wedyn_ADV o_ADP 'n_PART mynd_VERB i_ADP fag_NOUN o_ADP dan_ADP bwr_NOUN '_PUNCT ag_CONJ yn_PART estyn_VERB pishin_NOUN o_ADP bapur_NOUN ac_CONJ fel_ADP hyn_PRON o_ADP dan_ADP bwr_NOUN '_PUNCT efo_ADP 'r_DET papur_NOUN ._PUNCT
Cau_AUX y_DET papur_NOUN a_CONJ r'oid_VERB o_ADP 'n_PART ôl_NOUN yn_ADP 'i_PRON fag_NOUN a_CONJ wedyn_ADV teipio_VERB ._PUNCT
On_INTJ '_PUNCT 'na_ADV 'th_NOUN o_ADP neud_VERB hynna_PRON tua_ADP pedwar_NUM gwaith_NOUN '_PUNCT st_VERB 'i_PRON ._PUNCT
'_PUNCT M'bobo_DET bebe_NOUN mae_AUX 'n_PART neud_VERB de_NOUN ._PUNCT
So_INTJ 'di_PART 'r_DET boi_NOUN 'ma_ADV 'n_PART gor'o_VERB '_PUNCT [_PUNCT saib_NOUN ]_PUNCT gwitsiad_NOUN '_PUNCT ŵan_NOUN dan_ADP geith_NOUN o_ADP formal_NOUN llythyr_NOUN gin_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ydi_NOUN ._PUNCT
Ma_AUX 'n_PART rhoi_VERB 'i_PRON notice_NOUN i_ADP fewn_ADP heddiw_ADV ._PUNCT
Heno_ADV ?_PUNCT
Hmm_INTJ ._PUNCT
Be_DET mama'_NOUNUNCT isio_VERB '_PUNCT neud_VERB dy_DET '_PUNCT Llun_NOUN '_PUNCT fyd_NOUN ?_PUNCT
Ah_INTJ '_PUNCT m'bobo_NOUN ._PUNCT
A_CONJ wedyn_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
-_PUNCT Ill_PROPN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ah_INTJ ia_PRON sefydliad_NOUN ma_PRON 'n_PART gweithio_VERB '_PUNCT ŵan_NOUN ?_PUNCT
Ia_INTJ ._PUNCT
'_PUNCT Dyn_NOUN o'dd_NOUN o_PRON 'n_PART d'eud_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ma_INTJ '_PUNCT '_PUNCT heini_ADJ 'n_PART lleoliad_NOUN Street_PROPN '_PUNCT d'ydi_PRON ?_PUNCT
Ma'_VERBUNCT nhw_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT tua_ADV lleoliad_NOUN mama_NOUN 'na_ADV rei_VERB de_NOUN ._PUNCT
Reit_VERB agos_ADJ i_ADP siop_NOUN sefydliad_NOUN 'n_PART fan'na_PUNCT ?_PUNCT
Ia_INTJ ._PUNCT
A_PART wedyn_ADV o'dd_AUX o_PRON 'n_PART d'eud_VERB os_CONJ o_ADP '_PUNCT gynno_VERB fo_PRON [_PUNCT saib_NOUN ]_PUNCT wylia_VERB [_PUNCT saib_NOUN ]_PUNCT o'dd_NOUN o_PRON 'n_PART cym'yd_SYM '_PUNCT heini_ADJ de_NOUN ._PUNCT
Yn_PART lle_NOUN gweithio_VERB notice_NOUN ._PUNCT
Hmm_INTJ ._PUNCT
Os_CONJ o_ADP 'n_PART ca'l_VERB '_PUNCT lly_X ._PUNCT
Gynno_VERB fo_PRON bedair_NUM w'snos_SYM +_SYM
Mmm_VERB ._PUNCT
+_VERB o_ADP notice_NOUN ma_PRON 'n_PART siŵr_ADJ ?_PUNCT
Fi_PRON sy_AUX 'n_PART meddwl_VERB ._PUNCT
'_PUNCT Dw_VERB i_PRON 'm_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Dw_AUX i_PRON 'm_DET yn_PART gw'bod_ADJ ._PUNCT
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
Wel'is_VERB i_PRON byth_ADV mo_ADP 'r_DET boi_NOUN IT_PROPN 'na_ADV ._PUNCT
Pan_CONJ ddes_VERB i_PRON a_CONJ 'r_DET post_NOUN '_PUNCT nes_CONJ i_PART weld_VERB y_DET ddynas_NOUN 'ma_ADV [_PUNCT =_SYM ]_PUNCT sy_VERB 'di_PART [_PUNCT /=_PROPN ]_PUNCT sy_AUX 'n_PART co_ADJ -_PUNCT ordinetio_VERB ac_CONJ '_PUNCT isio_VERB '_PUNCT ordro_VERB 'r_DET petha_NOUN 'ma_ADV ._PUNCT
So_INTJ '_PUNCT nes_CONJ i_ADP dd'eu_NOUN '_PUNCT '_PUNCT thi_PRON hi_PRON de_NOUN ._PUNCT
'_PUNCT C'ofn_NOUN i_ADP r'wun_NOUN droi_VERB rownd_ADV a_CONJ d'eud_VERB ._PUNCT
Mmm_VERB [_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
A_CONJ dyma_ADV fi_PRON 'n_PART -_PUNCT deu_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Na_VERB 'th_NOUN o_ADP ofyn_VERB [_PUNCT =_SYM ]_PUNCT i_ADP fi_PRON [_PUNCT /=_PROPN ]_PUNCT i_ADP mi_PRON bebe_NOUN o'dd_NOUN yr_DET enw_NOUN o'dd_NOUN o_ADP fod_VERB i_PART roid_VERB ar_ADP y_DET form_NOUN ._PUNCT
So_INTJ '_PUNCT nes_CONJ i_PART dd'eud_VERB "_PUNCT Wel_INTJ '_PUNCT dan_ADP i_PRON 'm_DET yn_PART gw'bod_VERB eto_ADV a_CONJ +_SYM
[_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
+_SYM pam_ADV ti_PRON 'm_DET yn_PART gw'bod_VERB ?_PUNCT
Achos_CONJ heddiw_ADV mama_NOUN cyfweliada_NOUN "_PUNCT medda'_VERBUNCT fi_PRON ._PUNCT
Ah_INTJ [_PUNCT -_PUNCT ]_PUNCT a_CONJ '_PUNCT dyn_NOUN o_ADP 'n_PART i_PRON 'n_PART chwerthin_VERB de_NOUN ._PUNCT
A_CONJ '_PUNCT dyn_NOUN "_PUNCT Ah_ADP lle_NOUN [_PUNCT -_PUNCT ]_PUNCT pa_PART ddesg_NOUN 'di_PART o_VERB ?_PUNCT "_PUNCT "_PUNCT Wel_INTJ 'di_PART desg_NOUN ddim_ADV 'di_PART cyrra'dd_NOUN eto_ADV "_PUNCT medda'_VERBUNCT fi_PRON ._PUNCT
"_PUNCT So_PART alla'_VERBUNCT i_PRON 'm_DET atab_VERB hynny_PRON '_PUNCT chwaith_ADV ._PUNCT "_PUNCT
<_SYM pesychu_VERB >_SYM ._PUNCT
"_PUNCT Ah_X ddo_VERB '_PUNCT i_PRON fyny_ADV '_PUNCT ŵan_NOUN "_PUNCT ._PUNCT
'_PUNCT Dyn_NOUN o'dd_NOUN o_ADP '_PUNCT isio_VERB '_PUNCT gw'bod_VERB lle_ADV o_ADP 'n_PART i_PRON ._PUNCT
"_PUNCT '_PUNCT Dw_AUX i_PRON 'di_PART cymeryd_VERB bod_VERB o_PRON 'n_PART fan_NOUN hyn_DET yn_PART r'wla_NOUN "_PUNCT me_ADP '_PUNCT fi_PRON ._PUNCT
'_PUNCT C'os_NOUN 'na_ADV 'th_NOUN o_ADP dd'eud_VERB "_PUNCT Ddo_VERB '_PUNCT i_PRON yna_ADV rŵan_ADV ._PUNCT
A_CONJ lle_NOUN '_PUNCT dach_VERB chi_PRON ?_PUNCT
Yn_ADP yr_DET hen_ADJ adeilad_NOUN [_PUNCT saib_NOUN ]_PUNCT lle_ADV ma_VERB 'r_DET ystafall_NOUN gyfarfod_NOUN ?_PUNCT "_PUNCT A_CONJ '_PUNCT nes_CONJ i_PART dd'eud_VERB "_PUNCT Ia_INTJ ._PUNCT
Ond_CONJ '_PUNCT dan_ADP ni_PRON uwchben_ADP y_DET '_PUNCT stafall_NOUN cyfarfod_NOUN ._PUNCT
So_PART ti_PRON 'n_PART dwad_NOUN i_ADP fyny_ADV 'r_DET grisia_NOUN a_CONJ troi_VERB i_ADP 'r_DET dde_NOUN "_PUNCT ._PUNCT
A_CONJ hyn_PRON naw_NUM o_ADP '_PUNCT gloch_NOUN [_PUNCT =_SYM ]_PUNCT '_PUNCT Dw_VERB i_PRON [_PUNCT /=_PROPN ]_PUNCT '_PUNCT dw_VERB i_PRON byth_ADV 'di_PART weld_VERB o_ADP me_ADP '_PUNCT fi_PRON ._PUNCT
Hmm_INTJ ._PUNCT
Ag_NOUN o_ADP 'n_PART i_PRON 'n_PART siarad_VERB efo_ADP 'r_DET cleaner_NOUN de_NOUN ._PUNCT
'_PUNCT Nes_ADV i_PART ofyn_VERB iddi_ADP faint_ADV oedd_VERB yn_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Nes_NOUN i_ADP dd'eu_NOUN '_PUNCT '_PUNCT tha_PART chdi_ADJ ?_PUNCT
Faint_ADV o'dd_NOUN yn_PART '_PUNCT stafall_VERB ni_PRON cyn_ADP i_ADP ni_PRON ddod_VERB yna_ADV llynadd_ADJ ._PUNCT
Ah_INTJ ._PUNCT
Pedwar_VERB ia_PRON ?_PUNCT
Pedwar_PROPN ._PUNCT
A_PART dau_NUM o'dd_NOUN drws_NOUN nesa'_ADJUNCT ._PUNCT
Lle_ADV o'dd_NOUN drws_NOUN nesa'_ADJUNCT ?_PUNCT
Lle_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Lle_ADV o'dd_NOUN [_PUNCT -_PUNCT ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
A_CONJ dyma_ADV hi_PRON 'n_PART d'eud_VERB '_PUNCT oeddan_VERB nhw_PRON fod_VERB i_PART roid_VERB powerpoints_NOUN i_ADP fewn_ADP cyn_ADP i_ADP ni_PRON symud_VERB i_ADP fewn_ADP yna_ADV ._PUNCT
'_PUNCT C'os_NOUN '_PUNCT dan_ADP ni_PRON 'm_DET i_ADP fod_VERB i_PART fod_AUX yn_PART gweithio_VERB w_ADP 'th_PRON extension_VERB leads_ADJ nag_CONJ '_PUNCT dan_ADP ?_PUNCT
Dim_PRON os_CONJ '_PUNCT dyn_NOUN nhw_PRON 'n_PART trip_NOUN hazard_ADJ de_NOUN ._PUNCT
Ah_INTJ ._PUNCT
Wel_INTJ na_INTJ ._PUNCT
Ma'_VERBUNCT nhw_PRON rhwng_ADP dau_NUM ddesg_NOUN ._PUNCT
Hmm_INTJ ._PUNCT
'_PUNCT El_DET mama_NOUN hynny_PRON 'n_PART iawn_ADJ ._PUNCT
ym_ADP ._PUNCT
Gin_VERB i_ADP sbot_NOUN i_ADP gwaelod_NOUN '_PUNCT y_DET nghefn_NOUN ._PUNCT
Hmm_INTJ ?_PUNCT
Cym'a_NOUN look_VERB sydyn_ADJ '_PUNCT no_PRON fo_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ww_ADP ._PUNCT
Na_INTJ [_PUNCT anadlu_VERB ]_PUNCT Fucking_NOUN hell_ADV ma_PRON 'n_PART brifo_VERB ._PUNCT
Hwn_PRON 'na_ADV ?_PUNCT
Ia_INTJ ._PUNCT
O_ADP 's_X 'na_ADV pen_NOUN '_PUNCT no_PRON fo_PRON ?_PUNCT
ym_ADP ._PUNCT
Dos_VERB i_ADP ffenast_NOUN '_PUNCT ._PUNCT
'_PUNCT Dw_AUX i_PRON 'm_DET yn_PART gweld_VERB [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Oes_VERB ._PUNCT
Un_NUM bach_ADJ ._PUNCT
'_PUNCT Sio_NOUN '_PUNCT fi_PRON wasgu_VERB fo_PRON ?_PUNCT
Ah_INTJ ma_AUX 'n_PART brifo_VERB de_NOUN ._PUNCT
Ah_INTJ [_PUNCT griddfan_NOUN ]_PUNCT ._PUNCT
Wel_INTJ ti_PRON 'm_PART yn_PART rhoi_VERB chance_VERB i_ADP fi_PRON ._PUNCT
[_PUNCT griddfan_NOUN ]_PUNCT ._PUNCT
Ah_INTJ ._PUNCT
Ww_PART ma_PRON 'n_PART brifo_VERB '_PUNCT ŵan_NOUN ._PUNCT
Ah_INTJ ._PUNCT
Ti_PRON 'di_PART ca'l_VERB '_PUNCT bath_NOUN ?_PUNCT
Naddo_VERB '_PUNCT c'os_NOUN w't_ADJ ti_PRON 'n_PART +_SYM
ym_ADP ._PUNCT
+_VERB symud_VERB ._PUNCT
Os_CONJ gin_VERB ti_PRON tweezers_NOUN de_NOUN nipia_NOUN pen_NOUN yr_DET [_PUNCT -_PUNCT ]_PUNCT efo_ADP [_PUNCT saib_NOUN ]_PUNCT -_PUNCT s_DET tweezers_NOUN chdi_NOUN de_NOUN ._PUNCT
'_PUNCT El_INTJ '_PUNCT dw_AUX i_ADP 'm_DET 'di_PART gorffan_NOUN '_PUNCT y_DET mhanad_NOUN eto_ADV ._PUNCT
Ah_INTJ ._PUNCT
A_CONJ '_PUNCT g_NOUN '_PUNCT w_PRON 'ma_ADV 'n_PART brifo_VERB '_PUNCT st_VERB 'i_PRON ._PUNCT
[_PUNCT Tawelwch_PROPN ]_PUNCT ._PUNCT
<_SYM pesychu_VERB >_SYM ._PUNCT
A_CONJ '_PUNCT i_PRON 'n_PART ôl_NOUN '_PUNCT ŵan_NOUN ia_PRON ?_PUNCT
Ia_INTJ ._PUNCT
Sbecdol_ADJ di_PRON '_PUNCT im_NOUN <_SYM aneglur_ADJ 2_NUM >_SYM [_PUNCT sniffian_VERB ]_PUNCT ._PUNCT
[_PUNCT Metel_PROPN yn_PART cael_VERB ei_PRON daro_VERB ]_PUNCT ._PUNCT
Reit_INTJ ._PUNCT
Plyga_VERB drosodd_ADV ._PUNCT
T'isio_VERB '_PUNCT rhoi_VERB sbecdol_ADJ on_X ?_PUNCT
Wel_INTJ '_PUNCT dw_AUX i_PRON 'n_PART weld_VERB o_ADP de_NOUN ._PUNCT
[_PUNCT Tawelwch_PROPN ]_PUNCT ._PUNCT
[_PUNCT swnian_VERB ]_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ah_INTJ ._PUNCT
Ma_AUX 'n_PART gwaedu_VERB '_PUNCT ŵan_NOUN ._PUNCT
'_PUNCT Ti_PRON disiw_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART meddwl_VERB [_PUNCT =_SYM ]_PUNCT geith_NOUN [_PUNCT /=_PROPN ]_PUNCT gei_CONJ di_PRON fwy_ADV allan_ADV ohono_ADP fo_PRON ._PUNCT
Gwasga_VERB [_PUNCT aneglur_ADJ ]_PUNCT eto_ADV '_PUNCT ta_X [_PUNCT griddfan_NOUN ]_PUNCT ._PUNCT
'_PUNCT Ew_X '_PUNCT ._PUNCT
[_PUNCT Tawelwch_PROPN ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
BeBe_NOUN ?_PUNCT
BeBe_VERB 'di_PART hwn_DET ?_PUNCT
ym_ADP [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
'_PUNCT Nes_ADV i_PART weipio_VERB te_NOUN fel'a_ADJ ._PUNCT
Amsar_VERB te_NOUN '_PUNCT ddiw_ADV [_PUNCT sniffian_VERB ]_PUNCT [_PUNCT chwythu_VERB ]_PUNCT ._PUNCT
[_PUNCT Swn_PROPN traed_VERB yn_PART cerdded_VERB ]_PUNCT ._PUNCT
'_PUNCT T'isio_VERB '_PUNCT gweld_VERB llun_NOUN mama_NOUN [_PUNCT aneglur_ADJ ]_PUNCT 'di_PART roid_VERB  _SPACE [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
<_SYM pesychu_VERB >_SYM ._PUNCT
enwb_VERB ._PUNCT
1292.408_VERB
