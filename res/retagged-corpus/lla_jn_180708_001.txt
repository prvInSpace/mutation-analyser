<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 00_NUM :_PUNCT 01_NUM -_PUNCT 00_NUM :_PUNCT 03_NUM :_PUNCT 02_NUM
Am_ADP dros_ADP bedair_NUM degawd_NOUN mae_VERB Ras_PROPN Cefnfor_PROPN Colvo_PROPN wedi_PART cydio_VERB 'n_PART dynn_ADJ yn_PART nychymyg_NOUN ac_CONJ yng_ADP nghalonnau_NOUN rhai_PRON o_ADP forwyr_NOUN gorau_ADJ 'r_DET byd_NOUN ._PUNCT
Marathon_NOUN ar_ADP y_DET môr_NOUN ._PUNCT
Dros_ADP bedwar_NUM deg_NUM pum_NUM mil_NUM o_ADP ffiltiroedd_NOUN ._PUNCT
Croesi_VERB pedwar_NOUN cefnfor_NOUN gan_CONJ gyffwrdd_NOUN â_ADP chwe_NUM chyfandir_NOUN ._PUNCT
Mae_VERB 'n_PART ras_NOUN sy_VERB 'n_PART fwy_ADJ na_CONJ ennill_VERB a_CONJ cholli_VERB ._PUNCT
Goroesi_VERB yw_VERB 'r_DET peth_NOUN pwysica'_NOUNUNCT wrth_ADP i_ADP 'r_DET cystadleuwyr_NOUN wynebu_VERB rhai_PRON o_ADP 'r_DET amodau_NOUN hwylio_VERB mwya'_ADJUNCT garw_NOUN sy_AUX 'n_PART bodoli_VERB ._PUNCT
'_PUNCT Dach_VERB chi_PRON allan_ADV yna_ADV ._PUNCT
Yn_ADP canol_ADJ nunlle_NOUN ._PUNCT
Miloedd_NOUN o_ADP filltiroedd_NOUN o_ADP unrhyw_DET lan_NOUN ._PUNCT
Gwyntoedd_NOUN cry_NOUN '_PUNCT ._PUNCT
Tonnau_NOUN enfawr_ADJ ._PUNCT
Mae_AUX 'n_PART bwrw_VERB eira_NOUN ._PUNCT
Mae_VERB 'n_PART oer_ADJ ._PUNCT
Mae_VERB 'n_PART anodd_ADJ dychmygu_VERB pa_ADV mor_ADV anodd_ADJ ydy_VERB hynna_PRON ._PUNCT
Eleni_ADV mae_AUX 'r_DET Cymro_PROPN enwg_VERB yn_PART cystadlu_VERB yn_ADP y_DET ras_NOUN am_ADP y_DET tro_NOUN cynta'_ADJUNCT ._PUNCT
Ac_CONJ falle_VERB ei_PRON fod_AUX wedi_PART hwylio_VERB ers_ADP yn_PART blentyn_NOUN ond_CONJ d'oes_VERB dim_PRON wedi_PART 'i_PRON baratoi_VERB am_ADP amodau_NOUN a_CONJ pheryglon_VERB y_DET ras_NOUN yma_DET ._PUNCT
Mae_VERB 'n_PART sialens_NOUN seicolegol_ADJ ._PUNCT
'_PUNCT Dach_VERB chi_PRON allan_ADV ar_ADP y_DET dŵr_NOUN am_ADP cyfnodau_NOUN [_PUNCT =_SYM ]_PUNCT hir_ADV [_PUNCT /=_PROPN ]_PUNCT hir_ADJ iawn_ADV ._PUNCT
'Di_PART o_ADP 'm_DET yn_PART lle_NOUN cyfforddus_ADJ i_PART byw_VERB ._PUNCT
Mae_AUX 'n_PART pedwar_NOUN awr_NOUN a_CONJ pedwar_NOUN awr_NOUN off_ADJ am_ADP wythnosau_NOUN ._PUNCT
Pan_CONJ '_PUNCT da_ADJ chi_PRON yna_ADV '_PUNCT da_ADJ chi_PRON methu_ADV gallu_ADV dychmygu_VERB pa_ADV mor_ADV ddrwg_ADJ ydy_VERB o_PRON ._PUNCT
Mae_AUX 'r_DET cychod_NOUN yn_PART rasio_VERB am_ADP wythnosau_NOUN ar_ADP y_DET tro_NOUN ._PUNCT
O_ADP ddinas_NOUN i_ADP ddinas_NOUN ac_CONJ ar_ADP draws_ADJ y_DET moroedd_NOUN ._PUNCT
Byddant_AUX yn_PART wynebu_VERB mawredd_NOUN moroedd_NOUN y_DET de_NOUN ._PUNCT
Ddwy_NUM waith_NOUN yn_PART croesi_VERB 'r_DET cyhydedd_NOUN ac_CONJ yn_PART mentro_VERB o_ADP gwmpas_NOUN Cape_NOUN Horn_PROPN y_DET lle_NOUN mwya'_ADJUNCT peryglus_ADJ i_PART hwylio_VERB yn_ADP y_DET byd_NOUN ._PUNCT
Dros_ADP gyfnod_NOUN y_DET ras_NOUN bydd_AUX y_DET fflid_VERB yn_PART ymweld_VERB a_CONJ rhai_PRON o_ADP barthladdoedd_NOUN mwya'_ADJUNCT adnabyddus_ADJ y_DET byd_NOUN ._PUNCT
Ac_CONJ eleni_ADV am_ADP y_DET tro_NOUN cyntaf_ADJ mae_AUX hynny_PRON yn_PART cynnwys_VERB Caerdydd_PROPN ._PUNCT
Ac_CONJ mae_AUX 'n_PART gyfle_NOUN i_ADP hwyliwr_NOUN ifanc_ADJ wireddu_VERB breuddwyd_NOUN ._PUNCT
Hwylio_VERB gartre_ADV '_PUNCT i_ADP Gymru_PROPN ._PUNCT
Ond_CONJ cyn_CONJ cychwyn_VERB mae_AUX angen_NOUN i_PART enwg_VERB fod_VERB yn_PART barod_ADJ ._PUNCT
Yn_PART barod_ADJ i_ADP herio_VERB 'r_DET elfennau_NOUN a_CONJ 'r_DET emosiynau_NOUN ._PUNCT
Be_INTJ oeddan_VERB ni_PRON fwyaf_ADV bryderus_ADJ am_ADP dan_ADP oedd_VERB y_DET ffaith_NOUN '_PUNCT da_ADJ ni_PRON heb_ADP neu_CONJ o_ADP o_ADP 'r_DET oblaen_NOUN ._PUNCT
Oddan_AUX ni_PRON 'n_PART mynd_VERB fewn_ADP i_ADP 'r_DET unknown_ADJ mewn_ADP ffordd_NOUN ._PUNCT
A_CONJ hwnna_PRON oedd_VERB be_ADP oeddan_AUX ni_PRON 'n_PART pryderu_VERB amdano_ADP fwya'_ADJUNCT ._PUNCT
[_PUNCT Cerddoriaeth_NOUN ]_PUNCT
Alicante_INTJ ._PUNCT
Arfordir_NOUN dwyreiniol_ADJ Sbaen_PROPN ._PUNCT
Lawr_ADV y_DET ffordd_NOUN o_ADP Bendidorm_PROPN ._PUNCT
Mae_VERB 'n_PART ddinas_NOUN sy_AUX 'n_PART denu_VERB twristiaid_NOUN yn_ADP eu_DET miloedd_NOUN bob_DET blwyddyn_NOUN sy_AUX 'n_PART heidio_VERB yno_ADV ar_ADP gyfer_NOUN hwyl_NOUN yn_ADP yr_DET haul_NOUN ._PUNCT
Ond_CONJ am_ADP bythefnos_NOUN ym_ADP mis_NOUN Hydref_PROPN mae_VERB 'r_DET ddinas_NOUN yn_PART ganolbwynt_NOUN i_ADP garnifal_NOUN Ras_PROPN Cefnfor_PROPN Volvo_PROPN Grand_PROPN Prix_PROPN y_DET byd_NOUN hwylio_VERB ._PUNCT
Mae_AUX 'r_DET Volvo_X Ocean_X Race_PROPN mae_VERB ganddo_ADP hanes_NOUN enfawr_ADJ yn_ADP y_DET byd_NOUN hwylio_VERB a_CONJ mae_AUX o_PRON 'di_PART weld_VERB fel_ADP un_NUM o_ADP uchafbwyntiau_NOUN y_DET fath_NOUN o_ADP hwylio_VERB yma_ADV ._PUNCT
Sef_ADV hwylio_VERB pellter_NOUN hir_ADJ dros_ADP cefnforoedd_NOUN ._PUNCT
Pan_CONJ mae_AUX rhywun_NOUN yn_PART deud_VERB "_PUNCT Volvo_X Ocean_X Race_X "_PUNCT fydda_AUX i_PRON 'n_PART deud_VERB "_PUNCT waw_X '_PUNCT da_ADJ chi_PRON 'n_PART aelod_NOUN o_ADP 'r_DET criw_NOUN yna_DET ._PUNCT
O_ADP 'r_DET tîm_NOUN yna_DET ._PUNCT "_PUNCT Mae_VERB 'na_ADV lot_PRON o_ADP elfennau_NOUN [_PUNCT =_SYM ]_PUNCT caled_ADJ [_PUNCT /=_PROPN ]_PUNCT caled_ADJ iawn_ADV ._PUNCT
D'oes_VERB 'na_ADV ddim_PART sialens_NOUN fwy_ADV heriol_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 05_NUM :_PUNCT 20_NUM -_SYM 00_NUM :_PUNCT 06_NUM :_PUNCT 17_NUM
Mae_VERB 'r_DET paratoadau_NOUN ar_ADP ben_NOUN ._PUNCT
Mae_VERB 'r_DET cwch_NOUN yn_PART barod_ADJ ._PUNCT
Mae_AUX 'r_DET criw_NOUN gobeithio_VERB yn_PART barod_ADJ hefyd_ADV ._PUNCT
Cymal_NOUN cynta'_ADJUNCT y_DET ras_NOUN ._PUNCT
Alicante_VERB i_ADP Lisbon_PROPN ._PUNCT
Mae_AUX 'r_DET cychod_NOUN yn_PART cychwyn_VERB ._PUNCT
Y_DET daith_NOUN yn_PART dechrau_VERB o_ADP 'r_DET diwedd_NOUN ._PUNCT
Cyn_CONJ cychwyn_VERB y_DET ras_NOUN ymama_NOUN da_ADJ ni_PRON byth_ADV 'di_PART rasio_VERB am_ADP cyfnodau_NOUN hir_ADJ allan_ADV ar_ADP y_DET môr_NOUN ._PUNCT
Y_DET ffaith_NOUN bod_VERB y_DET ras_NOUN mor_ADV hir_ADJ ._PUNCT
Yn_PART cymeryd_VERB gymaint_ADV o_ADP amser_NOUN  _SPACE oedd_AUX hwnna_PRON 'n_PART ychwanegu_VERB mewn_ADP ffordd_NOUN at_ADP pa_ADV mor_ADV fawr_ADJ oedd_VERB y_DET ras_NOUN a_CONJ pa_ADV mor_ADV fawr_ADJ oedd_VERB y_DET sialens_NOUN o_ADP 'n_PRON blaenau_NOUN ni_PRON ._PUNCT
bod_AUX y_DET ras_NOUN yn_PART farathon_VERB mae_VERB 'r_DET dechrau_NOUN 'n_PART debycach_ADJ i_ADP sbrint_NOUN ._PUNCT
Mae_AUX pob_DET tîm_NOUN yn_PART ceisio_VERB sicrhau_VERB 'r_DET fantais_NOUN gan_CONJ gipio_VERB 'r_DET blaen_NOUN a_CONJ gwibio_VERB 'n_PART glir_ADJ ._PUNCT
Mae_VERB pethau_NOUN 'n_PART gyflym_ADJ ac_CONJ yn_PART gyffrous_ADJ ._PUNCT
Mewn_ADP porthladd_NOUN prysur_ADJ ._PUNCT
Ddim_PART yr_DET amodau_NOUN ar_ADP y_DET môr_NOUN yw_VERB 'r_DET her_NOUN fwya'_ADJUNCT ._PUNCT
Yn_PART hytrach_ADV ._PUNCT
Y_DET ffaith_NOUN bod_VERB 'na_ADV gychod_VERB eraill_PRON yn_PART gwibio_VERB 'n_ADP mhob_DET man_NOUN ._PUNCT
Mae_VERB hyn_PRON fel_ADP dechrau_VERB Grand_PROPN Prix_PROPN mewn_ADP maes_NOUN parcio_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 06_NUM :_PUNCT 52_NUM -_PUNCT 00_NUM :_PUNCT 08_NUM :_PUNCT 52_NUM
Mae_AUX 'r_DET cymal_NOUN cynta_ADJ 'n_PART mynd_VERB a_CONJ 'r_DET cychod_NOUN drwy_ADP gilfor_NOUN Gibraltar_PROPN ._PUNCT
Allan_ADV i_ADP 'r_DET môr_NOUN mawr_ADJ o_ADP gwmpas_NOUN Ynys_PROPN Porto_PROPN Santo_PROPN cyn_CONJ anelu_VERB yn_PART ôl_NOUN am_ADP Lisbon_PROPN ._PUNCT
yyy_ADV taw_ADP dyma_DET 'r_DET cymal_NOUN byrraf_ADJ yn_ADP yr_DET holl_DET gystadleuaeth_NOUN ._PUNCT
Mae_VERB 'n_PART hen_ADJ ddigon_ADJ i_ADP roi_VERB blas_NOUN yr_DET hyn_PRON sydd_AUX i_PART ddod_VERB ._PUNCT
Lot_PRON o_ADP 'r_DET amser_NOUN [_PUNCT =_SYM ]_PUNCT mae_VERB o_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT mae_VERB o_PRON 'n_PART sialens_DET kind_VERB of_NOUN seicolegol_ADJ ._PUNCT
'_PUNCT Dach_VERB chi_PRON allan_ADV ar_ADP y_DET dŵr_NOUN am_ADP cyfnodau_NOUN [_PUNCT =_SYM ]_PUNCT hir_ADV [_PUNCT /=_PROPN ]_PUNCT hir_ADJ iawn_ADV  _SPACE hefo_ADP ddim_PART cyswllt_NOUN i_ADP 'r_DET lan_NOUN so_ADJ 'di_PART o_ADP ddim_PART yn_PART lle_NOUN cyfforddus_ADJ i_PART byw_VERB ._PUNCT
Mae_VERB 'n_PART relentless_NOUN
There_VERB 's_X no_PRON real_ADJ toilet_NOUN ._PUNCT
You_NOUN don't_NOUN shower_NOUN ._PUNCT
There_VERB 's_X no_X time_NOUN to_NOUN yourself_NOUN ._PUNCT
It_ADP 's_X an_DET incredibly_NOUN tough_NOUN environment_NOUN [_PUNCT =_SYM ]_PUNCT and_INTJ [_PUNCT /=_PROPN ]_PUNCT and_CONJ there_VERB 's_X peo_NOUN [_PUNCT -_PUNCT ]_PUNCT basically_ADV people_NOUN that_NOUN can_VERB do_PART what_NOUN I_ADP think_NOUN and_ADJ people_VERB [_PUNCT =_SYM ]_PUNCT that_NOUN [_PUNCT /=_PROPN ]_PUNCT that_NOUN can't_NOUN do_ADP it_PRON
'_PUNCT Da_ADJ ni_PRON 'n_PART gweithio_VERB fewn_ADP bebe_NOUN da_ADJ ni_PRON 'n_PART galw_VERB 'n_PART watch_VERB system_NOUN ._PUNCT
'_PUNCT Da_ADJ chi_PRON 'n_PART hwylio_VERB am_ADP pedwar_NUM awr_NOUN ._PUNCT
Cael_VERB [_PUNCT =_SYM ]_PUNCT oddi_ADP ar_ADP [_PUNCT /=_PROPN ]_PUNCT oddi_ADP ar_ADP y_DET watch_VERB '_PUNCT Da_ADJ chi_PRON 'n_PART -_PUNCT ne_NOUN newid_NOUN ._PUNCT '_PUNCT Da_ADJ chi_PRON 'n_PART bwyta_VERB a_CONJ '_PUNCT da_ADJ chi_PRON syth_ADJ mewn_ADP i_ADP 'r_DET bunk_NOUN mor_ADV gyflym_ADJ a_CONJ sy_VERB 'n_PART bosib_ADJ ._PUNCT
Ond_CONJ os_CONJ mae_VERB 'na_ADV unrhyw_DET newid_NOUN cyfeiriad_NOUN neu_CONJ newid_ADV hwylio_VERB 'n_PART digwydd_VERB ._PUNCT
Mae_AUX pawb_PRON yn_PART cael_VERB eu_DET deffro_VERB ._PUNCT
Yn_PART annhebygol_ADJ '_PUNCT da_ADJ chi_PRON 'n_PART cael_VERB pedwar_NOUN awr_NOUN i_ADP ffwrdd_ADV ._PUNCT
I_ADP think_NOUN the_X whole_NOUN race_ADJ is_ADJ about_NOUN dealing_ADJ with_CONJ being_NOUN uncomfortable_VERB [_PUNCT =_SYM ]_PUNCT You_NOUN '_PUNCT re_NUM [_PUNCT /=_PROPN ]_PUNCT you_X '_PUNCT re_NOUN never_NOUN comfortable_VERB in_ADP this_VERB race_ADJ ._PUNCT
You_ADV '_PUNCT re_NOUN too_ADP hot_ADP ._PUNCT
Too_VERB cold_NOUN ._PUNCT
Wet_INTJ ._PUNCT
It_ADP 's_X one_X of_X those_NOUN environments_NOUN that_NOUN you_X '_PUNCT ve_VERB just_ADJ got_NOUN to_NOUN be_ADP used_ADP to_NUM being_NOUN uncomfortable_VERB
O_ADP ni_PRON trwy_ADP 'r_DET amser_NOUN yn_PART meddwl_VERB am_ADP be_PRON oedd_AUX yn_PART digwydd_VERB ar_ADP y_DET deck_NOUN Oeddan_PROPN ni_PRON ar_ADP yr_DET hwyl_NOUN iawn_ADV ?_PUNCT
Oeddan_AUX ni_PRON 'n_PART hwylio_VERB 'n_PART gyflym_ADJ ?_PUNCT
Ac_CONJ oedd_VERB hyn_PRON i_ADP gyd_NOUN yn_PART mynd_VERB o_ADP gwmpas_NOUN fy_DET meddwl_VERB  _SPACE tra_CONJ o_ADP ni_PRON fod_AUX yn_PART cysgu_VERB ._PUNCT
So_PART mae_AUX 'n_PART sgil_NOUN fawr_ADJ yn_ADP y_DET cychod_NOUN yma_DET ydi_VERB '_PUNCT da_ADJ chi_PRON [_PUNCT -_PUNCT ]_PUNCT pan_CONJ '_PUNCT da_ADJ chi_PRON ar_ADP y_DET deck_NOUN [_PUNCT =_SYM ]_PUNCT '_PUNCT da_ADJ chi_PRON 'n_PART gweithio_VERB 'n_PART galed_ADJ [_PUNCT /=_PROPN ]_PUNCT '_PUNCT da_ADJ chi_PRON 'n_PART gweithio_VERB 'n_PART galedd_NOUN a_CONJ wedyn_ADV '_PUNCT da_ADJ chi_PRON 'n_PART mynd_VERB lawr_ADV grisiau_NOUN a_CONJ mae_VERB 'n_PART bwysig_ADJ iawn_ADV bod_AUX chi_PRON 'n_PART switchio_VERB i_ADP ffwrdd_ADV [_PUNCT =_SYM ]_PUNCT a_CONJ [_PUNCT /=_PROPN ]_PUNCT a_CONJ ymlacio_VERB ._PUNCT
Oedd_VERB hynna_PRON 'n_PART rhywbeth_NOUN oeddan_AUX ni_PRON 'n_PART gorfod_ADV dysgu_VERB 'n_PART gyflym_ADJ iawn_ADV oedd_VERB jest_ADV ymlacio_VERB a_CONJ switchio_VERB off_ADP
Ond_CONJ nid_PART diffyg_NOUN cwsg_NOUN yw_VERB 'r_DET unig_ADJ her_NOUN ._PUNCT
Dyma_ADV hwylio_VERB ar_ADP raddfa_NOUN gwbl_ADV eithafol_ADJ ._PUNCT
A_CONJ 'r_DET gwaith_NOUN yn_PART galed_ADJ ac_CONJ yn_PART gorfforol_ADJ dros_ADP ben_NOUN ._PUNCT
Yn_ADP y_DET ras_NOUN yma_DET mae_VERB angen_NOUN bod_VERB yn_PART athletwr_NOUN yn_PART ogystal_ADJ a_CONJ morwr_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 09_NUM :_PUNCT 34_NUM -_SYM 00_NUM :_PUNCT 12_NUM :_PUNCT 23_NUM
Pedwar_VERB diwrnod_NOUN wedi_PART gadael_VERB Alicante_PROPN ac_CONJ mae_AUX Turn_PROPN the_X Tide_PROPN wedi_PART llithro_VERB i_ADP 'r_DET safle_NOUN ola'_ADJUNCT ac_CONJ yn_PART ceisio_ADV dal_ADV gafael_VERB ar_ADP weddill_NOUN y_DET cychod_NOUN ._PUNCT
Dw_VERB i_PRON ddim_PART yn_PART ofnadwy_ADJ o_ADP hapus_ADJ efo_ADP lle_NOUN '_PUNCT da_ADJ ni_PRON ar_ADP y_DET funud_NOUN ._PUNCT
Pan_CONJ '_PUNCT da_ADJ ni_PRON mewn_ADP ras_NOUN fel_ADP hyn_PRON mae_VERB o_PRON [_PUNCT -_PUNCT ]_PUNCT mae_VERB 'n_PART ras_NOUN hir_ADJ iawn_ADV felly_CONJ mae_VERB 'n_PART bwysig_ADJ bod_AUX ni_PRON 'n_PART cario_VERB '_PUNCT mlaen_NOUN jest_ADV  _SPACE gweithio_VERB 'n_PART galed_ADJ a_CONJ neud_VERB bebe_NOUN da_ADJ ni_PRON 'n_PART gwybod_VERB yn_PART iawn_ADJ ._PUNCT
ymm_INTJ [_PUNCT =_SYM ]_PUNCT peidio_VERB [_PUNCT /=_PROPN ]_PUNCT peidio_VERB kind_NOUN of_CONJ poeni_VERB gormod_ADV am_ADP be_PRON sy_AUX 'n_PART mynd_VERB ymlaen_ADV o_ADP gwmpas_NOUN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN ]_PUNCT
Gyda_ADP gweddill_NOUN y_PART fflid_VERB yn_PART diflannu_VERB dros_ADP y_DET gorwel_NOUN ._PUNCT
Dyma_ADV wers_NOUN gynnar_ADJ i_ADP 'r_DET criw_NOUN ifanc_ADJ dibrofiad_NOUN ._PUNCT
Mae_VERB angen_NOUN dygymod_VERB yn_PART gyflym_ADJ gyda_ADP dwyster_NOUN y_DET ras_NOUN fawr_ADJ ._PUNCT
So_INTJ '_PUNCT da_ADJ ni_PRON  _SPACE can_NOUN milltir_NOUN i_ADP ffwrdd_ADV o_ADP Lisbon_PROPN ._PUNCT
ymm_CONJ so_VERB '_PUNCT da_ADJ ni_PRON ar_ADP y_DET ffordd_NOUN  _SPACE yn_PART ôl_NOUN i_PRON [_PUNCT =_SYM ]_PUNCT diwedd_NOUN y_DET [_PUNCT /=_PROPN ]_PUNCT diwedd_NOUN y_DET cymal_NOUN ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'di_PART bod_VERB allan_ADV ar_ADP y_DET dŵr_NOUN wan_ADJ am_ADP bron_ADV i_ADP chwech_NUM diwrnod_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART edrych_VERB ymlaen_ADV i_PART cael_VERB cysgu_VERB 'n_PART gwely_NOUN heno_ADV 'ma_ADV a_CONJ cael_VERB cawod_NOUN cynnes_VERB  _SPACE a_CONJ dipyn_NOUN bach_ADJ o_ADP bwyd_NOUN ._PUNCT
Tîm_NOUN Vestas_PROPN a_CONJ 'i_PRON griw_NOUN profiadol_ADJ yw_VERB 'r_DET cynta'_NOUNUNCT i_PRON gyrraedd_VERB am_ADP gawod_NOUN ._PUNCT
Mae_AUX oriau_NOUN 'n_PART pasio_VERB cyn_ADP i_ADP Turn_PROPN the_X Tide_NOUN gyrraedd_VERB y_DET lan_NOUN ._PUNCT
I_ADP think_NOUN we_NOUN drifted_ADV over_VERB that_NOUN finish_ADJ line_NOUN rather_NOUN than_NOUN actually_VERB sail_NOUN over_VERB the_X finish_NUM line_NOUN
Oedd_VERB gorffen_VERB y_DET cymal_NOUN cynta'_ADJUNCT yn_PART Lisbon_PROPN y_DET tro_NOUN cyntaf_ADJ i_ADP fi_PRON kind_VERB of_CONJ bod_VERB allan_ADV ar_ADP y_DET cwch_NOUN am_ADP ras_NOUN a_CONJ wedyn_ADV dod_VERB fewn_ADP a_CONJ cael_VERB [_PUNCT =_SYM ]_PUNCT yr_DET [_PUNCT /=_PROPN ]_PUNCT yr_DET contrast_NOUN mewn_ADP ffordd_NOUN o_ADP bod_VERB allan_ADV ar_ADP y_DET dŵr_NOUN hefo_ADP deg_NUM person_NOUN am_ADP chwech_NUM diwrnod_NOUN a_CONJ wedyn_ADV dod_VERB fewn_ADP a_CONJ gweld_VERB cannoedd_NOUN o_ADP pobl_NOUN a_CONJ cael_VERB ffôn_NOUN yn_PART ôl_NOUN a_CONJ cael_ADV mynd_VERB i_ADP gwesty_NOUN a_CONJ cael_VERB cawod_NOUN a_CONJ gwely_NOUN yn_PART profiad_NOUN reit_NOUN arbennig_ADJ ._PUNCT
Mae_VERB Turn_PROPN the_X Tide_X yn_PART defnyddio_VERB carfan_NOUN o_ADP hwylwyr_NOUN ._PUNCT
System_NOUN sy_AUX 'n_PART golygu_VERB eu_PRON bod_AUX yn_PART gorffwys_VERB rhai_DET aelodau_NOUN o_ADP 'r_DET criw_NOUN ar_ADP rai_DET cymalau_NOUN ._PUNCT
A_PART blino_VERB neu_CONJ beidio_ADV mae_VERB tro_NOUN enwg_VERB i_PART ymlacio_VERB wedi_PART cyrraedd_VERB yn_PART barod_ADJ ._PUNCT
Ar_ADP y_DET pryd_NOUN o_ADP ni_PRON '_PUNCT chydig_ADJ bach_ADJ yn_PART anhapus_ADJ mewn_ADP ffordd_NOUN bod_AUX ni_PRON ddim_PART yn_PART cael_VERB neud_VERB yr_DET ail_ADJ cymal_NOUN ._PUNCT
Ac_CONJ oeddem_AUX ddim_PART yn_PART teimlo_VERB [_PUNCT =_SYM ]_PUNCT fod_VERB fod_VERB [_PUNCT /=_PROPN ]_PUNCT fod_VERB ni_PRON angen_NOUN cael_VERB ymlacip_NOUN ._PUNCT
ymm_VERB dim_PRON ond_ADP un_NUM cymal_NOUN o_ADP ni_PRON wedi_PART neud_VERB a_CONJ dim_PRON ond_ADP wythnos_NOUN oedd_VERB hynna_PRON ._PUNCT
Oeddan_VERB ni_PRON 'n_PART barod_ADJ i_PART mynd_VERB allan_ADV eto_ADV ._PUNCT
Oeddan_VERB ni_PRON eisiau_ADV mynd_VERB allan_ADV eto_ADV a_PART hwylio_VERB 'r_DET cwch_NOUN o_ADP Lisbon_PROPN [_PUNCT =_SYM ]_PUNCT i_PRON [_PUNCT /=_PROPN ]_PUNCT i_ADP Cape_NOUN Town_PROPN ._PUNCT
Pythefnos_NOUN yn_ADP yr_DET harbwr_NOUN sydd_VERB cyn_ADP y_DET cymal_NOUN nesa'_ADJUNCT o_PRON Lisbon_PROPN i_ADP Cape_NOUN Town_PROPN ._PUNCT
Saith_VERB mil_NUM o_ADP filltiroedd_NOUN ar_ADP draws_ADJ Môr_NOUN yr_DET Iwerydd_PROPN ._PUNCT
Tair_NOUN wythnos_NOUN ar_ADP y_DET môr_NOUN ._PUNCT
Ac_CONJ wrth_ADP i_ADP 'r_DET criw_NOUN baratoi_VERB ar_ADP gyfer_NOUN yr_DET her_NOUN nesaf_ADJ ._PUNCT
Mae_AUX enwg_VERB yn_PART pacio_VERB 'i_PRON fagiau_NOUN ac_CONJ yn_PART anelu_VERB am_ADP adre_ADV ._PUNCT
Fydda_VERB i_ADP dal_ADV yn_PART cadw_VERB llygad_NOUN ar_ADP sut_ADV mae_AUX 'r_DET cwch_NOUN yn_PART mynd_VERB ._PUNCT
Mae_VERB 'na_ADV lot_PRON o_ADP gwybodaeth_NOUN yn_PART dod_VERB oddi_ADP ar_ADP y_DET cychod_NOUN  _SPACE so_VERB mae_VERB 'n_PART cyfle_NOUN dda_ADJ i_ADP cael_VERB look_VERB ar_ADP be_PRON mae_AUX cychod_NOUN eraill_ADJ yn_PART neud_VERB ._PUNCT
Ac_CONJ yn_PART gobeithio_ADV rhoi_VERB rhyw_DET imput_NOUN defnyddiol_ADJ iawn_ADV pan_CONJ '_PUNCT da_ADJ ni_PRON 'n_PART cyrraedd_VERB y_DET cam_NOUN nesa'_ADJUNCT [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART Cape_NOUN Town_PROPN ._PUNCT
<_SYM 1_NUM >_SYM Wrth_CONJ i_ADP 'r_DET fflid_DET droi_VERB i_ADP 'r_DET de_NOUN i_ADP groesi_VERB 'r_DET gyhydedd_NOUN am_ADP Cape_NOUN Town_PROPN ._PUNCT
Mae_AUX enwg_VERB yn_PART troi_VERB tua_ADP 'r_DET gogledd_NOUN ._PUNCT
A_CONJ 'r_DET lle_NOUN ble_ADV dechreuodd_VERB y_DET daith_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 14_NUM :_PUNCT 26_NUM -_SYM 00_NUM :_PUNCT 15_NUM :_PUNCT 28_NUM
Dw_AUX i_PRON 'n_PART meddwl_VERB pan_CONJ o_ADP ni_PRON tua_ADV un_NUM deg_NUM tri_NUM ._PUNCT
Un_NUM deg_NUM pedwar_NUM mlwydd_NOUN oed_NOUN ._PUNCT
Dyna_DET pryd_NOUN oddo_NOUN dechrau_ADV mynd_VERB yn_PART fwy_ADJ  _SPACE yn_PART fwy_ADV serious_ADJ Dyma_DET fi_PRON 'n_PART dechrau_VERB cymeryd_VERB rhan_NOUN mewn_ADP cystadlaethau_VERB o_ADP gwmpas_NOUN prydain_ADJ a_CONJ wedyn_ADV o_ADP gwmpas_NOUN y_DET byd_NOUN ._PUNCT
So_PART dyma_ADV fo_PRON 'n_PART kind_VERB of_X rampio_VERB fyny_ADV [_PUNCT =_SYM ]_PUNCT reit_INTJ [_PUNCT /=_PROPN ]_PUNCT reit_NOUN sydyn_ADJ ._PUNCT
The_CONJ beginning_X of_X a_CONJ defining_NOUN moment_NOUN for_CONJ enwg_VERB was_NOUN winning_X the_X Topper_X National_PROPN Championships_PROPN back_ADJ in_ADP two_NUM thousand_NOUN and_NOUN five_X ._PUNCT
He_PART was_NOUN a_CONJ fourteen_NOUN year_NOUN old_NOUN Topper_ADP sailor_NOUN ._PUNCT
He_PART won_NOUN that_NOUN championship_NOUN and_NOUN then_NOUN he_PART came_VERB back_ADJ the_X next_NOUN year_ADJ ._PUNCT
Two_PART thousand_NOUN and_ADJ six_NOUN to_NOUN win_NOUN it_ADP again_NOUN ._PUNCT
So_PART basically_ADV as_PRON a_CONJ junior_NOUN sailor_NOUN winning_ADJ a_CONJ championship_NOUN with_NOUN close_NOUN to_NOUN three_NOUN hundred_NOUN boats_ADJ ._PUNCT
ymm_ADP that_NOUN was_NOUN really_X the_X start_X of_X his_X elite_NOUN performance_ADJ ._PUNCT
Dw_AUX i_PRON 'n_PART cofio_VERB trafod_VERB efo_ADP mam_NOUN a_CONJ dad_NOUN pan_CONJ o_ADP ni_PRON 'n_PART ifanc_ADJ bod_VERB o_ADP ni_PRON eisiau_VERB bod_VERB yn_PART hwyliwr_NOUN proffesiynol_ADJ ._PUNCT
Oedd_VERB mam_NOUN yn_PART '_PUNCT chydig_ADJ bach_ADJ yn_PART bryderus_ADJ dw_AUX i_PRON 'n_PART meddwl_VERB amdan_CONJ yr_DET holl_DET beth_NOUN ._PUNCT
A_PART mae_AUX hi_PRON hefyd_ADV yn_PART career_VERB advisor_NOUN so_PART oedd_VERB hi_PRON trwy_ADP 'r_DET amser_NOUN yn_PART kind_VERB of_CONJ neud_VERB yn_PART siŵr_ADJ bod_VERB fi_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT hefo_ADP plan_NOUN arall_ADJ jest_ADV rhag_ADP ofn_NOUN ._PUNCT
I_ADP 'r_DET coleg_NOUN felly_ADV ger_ADP y_DET lli_NOUN yn_ADP Southampton_PROPN Ond_CONJ wedi_PART ennill_VERB gradd_NOUN mewn_ADP peirianneg_NOUN ._PUNCT
Yn_PART ôl_NOUN at_ADP y_DET cynllun_NOUN gwreiddiol_ADJ ._PUNCT
Yn_PART ôl_NOUN at_ADP yr_DET hwylio_VERB a_CONJ 'r_DET tro_NOUN yma_DET ._PUNCT
Ar_ADP y_DET lefel_NOUN ucha'_ADJUNCT ._PUNCT
Rhan_NOUN o_ADP dîm_NOUN yr_DET America_PROPN 's_X Cup_PROPN
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 16_NUM :_PUNCT 29_NUM -_PUNCT 00_NUM :_PUNCT 17_NUM :_PUNCT 47_NUM
Ugain_NUM mlynedd_NOUN ers_ADP mentro_NOUN i_ADP 'r_DET môr_NOUN ar_ADP draeth_NOUN bychan_ADJ ._PUNCT
Mae_VERB hi_PRON 'n_PART amser_NOUN codi_VERB pac_NOUN am_ADP Cape_NOUN Town_PROPN ._PUNCT
Ac_CONJ yng_ADP nghysgod_NOUN Table_PROPN Mountain_PROPN mae_AUX 'n_PART cyrraedd_VERB mewn_ADP pryd_ADV i_PART weld_VERB mai_PART tîm_NOUN Mapfre_PROPN sy_AUX 'n_PART croesi_VERB 'r_DET llinell_NOUN yn_PART gynta'_ADJUNCT ._PUNCT
Tîm_NOUN enwg_VERB Turn_PROPN the_X Tide_X bron_ADV i_ADP bedwar_NUM cant_NUM a_CONJ hanner_NOUN o_ADP filltiroedd_NOUN ar_ADP ei_DET ôl_NOUN yn_PART gorffen_VERB yn_PART seithfed_ADJ ._PUNCT
Oedd_VERB o_PRON yn_PART neis_ADJ cael_VERB cwrdd_NOUN â_ADP 'r_DET tîm_NOUN eto_ADV a_CONJ bod_VERB yn_PART rhan_NOUN o_ADP 'r_DET tîm_NOUN ._PUNCT
'_PUNCT Dach_VERB chi_PRON off_ADP y_DET cwch_NOUN am_ADP dau_NUM ._PUNCT
Tri_INTJ ._PUNCT
Pedwar_VERB wythnos_NOUN ._PUNCT
ymm_INTJ '_PUNCT da_ADJ chi_PRON eisiau_ADV cael_VERB allan_ADV yna_ADV eto_ADV ._PUNCT
Cael_VERB allan_ADV yn_PART rasio_VERB ._PUNCT
Yn_PART gwybod_VERB 'na_ADV 'r_DET cymal_NOUN nesa'_ADJUNCT i_ADP fi_PRON neud_VERB oedd_VERB y_DET cymal_NOUN [_PUNCT =_SYM ]_PUNCT o_ADP [_PUNCT /=_PROPN ]_PUNCT o_ADP Cape_NOUN Town_PROPN i_ADP Melbourne_PROPN ._PUNCT
Cape_NOUN Town_PROPN i_ADP Melbourne_PROPN ._PUNCT
Chwe_VERB mil_NUM a_CONJ hanner_NOUN o_ADP filltiroedd_NOUN trwy_ADP foroedd_NOUN y_DET de_NOUN ._PUNCT
Heb_ADP os_CONJ dyma_DET un_NUM o_ADP 'r_DET cymalau_NOUN mwya'_ADJUNCT o_ADP ran_NOUN pellter_NOUN ac_CONJ o_ADP ran_NOUN problemau_NOUN ._PUNCT
Mae_VERB 'n_PART her_NOUN enfawr_ADJ ._PUNCT
A_PART bydd_VERB enwg_VERB a_CONJ 'r_DET rhanfwya'_NOUNUNCT o_ADP 'i_PRON griw_NOUN yn_PART ei_PRON brofi_VERB am_ADP y_DET tro_NOUN cynta'_ADJUNCT erioed_ADV ._PUNCT
When_ADV you_X '_PUNCT re_NOUN starting_NOUN a_CONJ leg_NOUN into_ADP the_X Southern_X Ocean_X just_X has_NOUN a_CONJ little_NOUN bit_DET more_NOUN atmosphere_ADP about_NOUN it_ADP ._PUNCT
That_ADJ kind_VERB of_X tension_NOUN that_NOUN 's_X hanging_ADJ over_VERB everyone_DET 's_X heads_X <_SYM /_PUNCT en_PRON >_SYM
'_PUNCT Da_ADJ chi_PRON 'n_PART clywed_VERB yr_DET holl_DET straeon_NOUN am_ADP pa_ADV mor_ADV wyntog_ADJ a_CONJ pa_ADV mor_ADV fawr_ADJ 'di_PART 'r_DET tonnau_NOUN ._PUNCT
Mor_ADV anodd_ADJ ydy_VERB hwylio_VERB yna_ADV [_PUNCT =_SYM ]_PUNCT D'oes_VERB [_PUNCT /=_PROPN ]_PUNCT d'oes_VERB neb_PRON byth_ADV yn_PART mynd_VERB lawr_ADV yna_ADV ._PUNCT
So_PART d'oes_VERB dim_DET llongau_NOUN yn_PART mynd_VERB lawr_ADV yna_ADV ._PUNCT
'_PUNCT Dach_VERB chi_PRON allan_ADV yn_PART canol_ADJ y_DET môr_NOUN ._PUNCT
Mil_NOUN o_ADP filltiroedd_NOUN i_ADP ffwrdd_ADV o_ADP unrhyw_DET lan_NOUN ar_ADP ben_NOUN eich_DET hun_NOUN mewn_ADP ffordd_NOUN achos_ADP os_CONJ mae_VERB unrhyw_DET beth_NOUN yn_PART [_PUNCT =_SYM ]_PUNCT mynd_VERB [_PUNCT /=_PROPN ]_PUNCT mynd_VERB o_ADP 'i_PRON le_NOUN ._PUNCT
D'oes_VERB neb_PRON yna_ADV i_PART helpu_VERB chi_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM 00_NUM :_PUNCT 19_NUM :_PUNCT 23_NUM -_SYM 00_NUM :_PUNCT 20_NUM :_PUNCT 21_NUM
Mae_VERB hi_PRON 'n_PART ornest_NOUN gwbl_ADV anheg_ADJ ._PUNCT
Dydi_VERB 'r_DET môr_NOUN byth_ADV angen_NOUN gorffwys_VERB ._PUNCT
Wedi_ADP dyddiau_NOUN o_ADP frwydro_VERB a_CONJ gyda_ADP rhan_NOUN fwya'_ADJUNCT o_ADP 'r_DET criw_NOUN yn_PART ymochel_VERB yng_ADP nghrombil_NOUN y_DET cwch_NOUN ._PUNCT
Mae_AUX 'r_DET môr_NOUN yn_PART dangos_VERB ei_DET dyngedd_NOUN ._PUNCT
Ar_ADP y_DET pryd_NOUN dim_PRON ond_ADP tri_NUM ohonan_VERB ni_PRON oedd_VERB ar_ADP y_DET deck_NOUN [_PUNCT =_SYM ]_PUNCT Oedda_VERB [_PUNCT /=_PROPN ]_PUNCT oeddan_AUX ni_PRON enwg_VERB a_CONJ enwb_NOUN ._PUNCT
Oeddan_AUX ni_PRON 'n_PART sefyll_VERB tu_NOUN ôl_NOUN enwg_VERB [_PUNCT =_SYM ]_PUNCT oedd_VERB yn_PART [_PUNCT /=_PROPN ]_PUNCT oedd_AUX yn_PART gyrru_VERB ar_ADP y_DET pryd_NOUN ac_CONJ digwydd_VERB yr_DET holl_DET peth_NOUN [_PUNCT -_PUNCT ]_PUNCT digwydd_VERB [_PUNCT -_PUNCT ]_PUNCT digwyddodd_VERB -_PUNCT o_ADP o_ADP 'n_PART blaenau_NOUN fi_PRON ._PUNCT
Dyma_DET bŵer_NOUN y_DET Cefnfor_NOUN Deheuol_ADJ ._PUNCT
Un_NUM don_NOUN yn_PART gorchuddio_VERB 'r_DET cwch_NOUN i_ADP gyd_ADP ._PUNCT
Ac_CONJ o_ADP fewn_ADP dim_PRON o_ADP 'i_PRON ddymchwel_VERB yn_PART gyfan_ADJ gwbwl_NOUN ._PUNCT
Mae_VERB 'na_ADV eiliad_NOUN neu_CONJ ddau_NUM lle_NOUN '_PUNCT da_ADJ chi_PRON meddwl_VERB "_PUNCT hang_ADJ on_X a_CONJ minute_VERB tydi_VERB hyn_PRON ddim_ADV [_PUNCT -_PUNCT ]_PUNCT dydan_AUX ni_PRON ddim_PART eisiau_VERB bod_VERB yma_ADV ._PUNCT "_PUNCT
Some_VERB people_NOUN were_VERB sort_NOUN of_X in_X their_NOUN bunks_X and_CONJ now_CONJ they_VERB '_PUNCT re_X on_X the_X wrong_X side_X of_X the_X boat_X with_NOUN all_ADP the_X stuff_NOUN on_X top_NOUN of_X them_X ._PUNCT "_PUNCT
2898.987_NOUN
