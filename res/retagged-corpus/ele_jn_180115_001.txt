Cynigion_NOUN Bwyd_PROPN Amser_NOUN Cinio_NOUN yn_ADP enw_NOUN
Dim_PRON ond_ADP ar_ADP gyfer_NOUN deiliaid_NOUN Cerdyn_NOUN Teyrngarwch_PROPN y_DET mae_VERB 'r_DET cynigion_NOUN hyn_PRON ar_ADP gael_VERB ._PUNCT
Cliciwch_VERB ar_ADP y_DET botwm_NOUN isod_ADV i_PART wneud_VERB cais_NOUN am_ADP Gerdyn_PROPN Teyrngarwch_PROPN
Gwnewch_VERB gais_VERB am_ADP gerdyn_NOUN teyrngarwch_NOUN
COFIWCH_NOUN DANGOS_NOUN EICH_DET CERDYN_NOUN TEYRNGARWCH_SYM IR_NOUN STAFF_NOUN CYN_PROPN RHOI_ADP EICH_DET ARCHEB_X FEL_X A_X GALLENT_NUM GWEITHREDU_PROPN EICH_X DISGOWNT_X TEYRNGARWCH_X ._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Yr_PART ydym_AUX yn_PART hoffi_ADV gwobrwyo_VERB ein_DET cwsmeriaid_NOUN ._PUNCT ._PUNCT ._PUNCT
Ar_ADP gyfer_NOUN ein_DET gwesteion_NOUN gyda_ADP chardiau_NOUN teyrngarwch_VERB ,_PUNCT mae_VERB gennym_ADP nifer_NOUN o_ADP gynigion_VERB ar_ADP fwyd_NOUN a_CONJ diodydd_NOUN yn_ADP enw_NOUN yr_DET wythnos_NOUN hon_DET ._PUNCT
Cask_VERB ale_NOUN of_X the_X day_NUM -_PUNCT £_SYM 2.50_NUM
Pint_NOUN of_X Aspalls_X -_PUNCT £_SYM 3.50_NUM
Pint_NOUN of_X Estrella_X -_PUNCT £_SYM 3.50_NUM
Bottle_NOUN of_X porta_NOUN wine_NOUN -_PUNCT £_SYM 8.00_NUM
Bottle_NOUN of_X red_DET wine_NOUN -_PUNCT shiraz_NOUN bin_NOUN number_NOUN 27_NUM -_PUNCT £_SYM 12.95_NUM
Gin_VERB of_X the_X week_NOUN (_PUNCT Brockmans_PROPN )_PUNCT with_NOUN dash_NOUN -_PUNCT £_SYM 3.50_NUM
Whiskey_VERB of_X the_X week_NOUN
Rum_VERB of_X the_X week_NOUN
Diet_VERB coke_NOUN bottle_NOUN -_PUNCT £_SYM 1.50_NUM
Latte_PROPN -_PUNCT £_SYM 1.50_NUM
Mae_VERB ein_DET cynigion_NOUN diod_NOUN ar_ADP gael_VERB drwy_ADP 'r_DET dydd_NOUN ,_PUNCT bob_DET dydd_NOUN ._PUNCT
(_PUNCT Cynnig_ADJ yn_PART dod_VERB i_ADP ben_NOUN 19_NUM /_SYM 01_NUM /_SYM 2018_NUM )_PUNCT
Sylwch_VERB fod_VERB y_DET cerdyn_NOUN teyrngarwch_VERB hefyd_ADV yn_PART rhoi_VERB cyfraddau_NOUN gostyngol_ADJ ar_ADP y_DET rhan_NOUN fwyaf_ADJ o_ADP 'n_PART diodydd_VERB bar_NOUN eraill_ADJ hefyd_ADV ._PUNCT
Mwy_PRON o_ADP gynnigion_NOUN ._PUNCT ._PUNCT ._PUNCT
Mwynhewch_VERB bwyd_NOUN blasus_ADJ gyda_ADP ni_PRON ._PUNCT ._PUNCT ._PUNCT
Mae_VERB gennym_ADP amrywiaeth_NOUN o_ADP gynnigion_VERB ar_ADP fwydydd_NOUN dethol_ADJ o_ADP 'n_PART bwydlen_NOUN bwyd_NOUN cinio_NOUN a_CONJ fydd_VERB ar_ADP gael_VERB yn_ADP ystod_NOUN dyddiau_NOUN 'r_DET wythnos_NOUN yn_PART unig_ADJ ._PUNCT
Venison_VERB in_X red_VERB wine_NOUN with_NOUN mash_NOUN -_PUNCT £_SYM 6.50_NUM
Breaded_VERB plaice_NOUN ,_PUNCT chips_NOUN &_ADP peas_NOUN -_PUNCT £_SYM 3.00_NUM
Mince_VERB dumplings_VERB with_NOUN mash_NOUN -_PUNCT £_SYM 4.95_NUM
Chicken_PROPN &_ADP pasta_NOUN in_ADP basil_NOUN sauce_NOUN -_PUNCT £_SYM 5.50_NUM
Trio_VERB of_X pork_NOUN leek_ADV sausages_VERB mash_NOUN &_ADP gravy_NOUN -_PUNCT £_SYM 4.50_NUM
Small_VERB lasagna_NOUN served_NOUN with_NOUN chips_NOUN ,_PUNCT garlic_PART bread_NOUN &_ADP salad_NOUN -_PUNCT £_SYM 5.50_NUM
Small_VERB fish_NOUN ,_PUNCT triple_X cooked_NOUN chips_NOUN &_ADP mushy_NOUN peas_NOUN
-_PUNCT £_SYM 6.95_NUM
Ham_NOUN ,_PUNCT egg_INTJ ,_PUNCT chips_NOUN &_ADP peas_NOUN -_PUNCT £_SYM 3_NUM ._PUNCT 50_NUM
Mae_AUX 'r_DET ddewislen_NOUN amser_NOUN cinio_NOUN ar_ADP gael_VERB rhwng_ADP 12_NUM y_DET pnawn_NOUN -_PUNCT 6_NUM y_DET pnawn_NOUN
Llun_NOUN -_PUNCT Gwener_PROPN
(_PUNCT Cynnig_ADJ yn_PART dod_VERB i_ADP ben_NOUN 19_NUM /_SYM 01_NUM /_SYM 2018_NUM )_PUNCT
Llogi_VERB bwrdd_NOUN
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
