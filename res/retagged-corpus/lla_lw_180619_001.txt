[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
A_PART mae_VERB hi_PRON newydd_ADJ fynd_VERB i_ADP 'r_DET toiled_NOUN a_CONJ lle_NOUN wyt_AUX ti_PRON 'n_PART mynd_VERB fory_ADV ?_PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT On_PRON i_PRON 'n_PART gwitsiad_NOUN +_SYM
Sori_INTJ ._PUNCT
+_VERB ers_ADP meitin_NOUN tydi_VERB ers_ADP +_SYM
Yndy_ADV mae_VERB ._PUNCT
+_VERB mis_NOUN Medi_PROPN ._PUNCT
Yndy_INTJ ._PUNCT
Yndy_INTJ ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB i_ADP 'r_DET ysgol_NOUN yn_PART bore_NOUN ._PUNCT
Ia_INTJ ?_PUNCT
A_PART dw_AUX i_PRON 'n_PART mynd_VERB yn_ADP y_DET p'nawn_NOUN a_CONJ '_PUNCT dyn_NOUN dw_AUX i_PRON 'n_PART mynd_VERB lawr_ADV i_ADP Gaerdydd_PROPN arol_ADJ i_ADP ysgol_NOUN orffen_VERB ._PUNCT
<_SYM SB_SYM >_SYM Ww_X ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB â_ADP enwb_NOUN i_ADP Rhwydwaith_NOUN Seren_PROPN +_SYM
Ie_INTJ ._PUNCT
Ww_ADP reit_INTJ ._PUNCT
+_VERB yna_ADV ._PUNCT
Mae_VERB o_ADP rhwbeth_NOUN yn_ADP y_DET colegau_NOUN 'ma_ADV ._PUNCT
Dw_AUX i_PRON 'm_DET yn_PART deall_VERB be_ADP ._PUNCT
Peidiwch_VERB â_ADP gofyn_VERB i_ADP mi_PRON ._PUNCT
Be_INTJ wyt_AUX ti_PRON 'n_PART neud_VERB ?_PUNCT
Ôô_NOUN dw_AUX i_PRON 'n_PART mynd_VERB i_ADP warchod_VERB yr_DET hen_ADJ blant_NOUN 'ma_ADV unwaith_ADV eto_ADV ._PUNCT
Ôô_NOUN ie_INTJ ._PUNCT
Fasau_NOUN gen_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ah_ADP yr_DET unig_ADJ drwg_NOUN ydy_VERB de_NOUN mae_VERB gennyn_ADP nhw_PRON gi_NOUN ._PUNCT
Rhaid_VERB i_ADP mi_PRON warchod_VERB hwnnw_PRON hefyd_ADV ._PUNCT
Taw_CONJ ._PUNCT
Arglwydd_NOUN mawr_ADJ '_PUNCT s'im_NOUN '_PUNCT isho_NOUN 'r_DET ci_NOUN chwaith_ADV ._PUNCT
Na_VERB fi_PRON chwaith_ADV ._PUNCT
Fedra_AUX di_PRON ddim_PART mynd_VERB a_CONJ nhw_PRON allan_ADV hefo_ADP 'r_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Lle_ADV wyt_AUX ti_PRON 'n_PART mynd_VERB enwb_NOUN ?_PUNCT
Wyt_AUX ti_PRON 'n_PART mynd_VERB i_ADP Sbaen_PROPN ?_PUNCT
Yndw_INTJ ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB efo_ADP enwg_VERB fory_ADV ._PUNCT
I_ADP lle_NOUN wyt_VERB ti_PRON +_SYM
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT +_SYM
+_VERB am_ADP gor_PRON '_PUNCT mynd_VERB ?_PUNCT
+_VERB rhydio_VERB drwy_ADP weith_NOUN o_ADP de_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Lle_ADV mae_AUX o_PRON 'n_PART mynd_VERB ?_PUNCT
'_PUNCT Dy_DET o_PRON ddim_PART yn_PART sâff_ADJ iawn_ADV eto_ADV ._PUNCT
S1_NOUN ?_PUNCT Dim_PRON yn_PART gwbod_VERB ._PUNCT
I_ADP rhywle_ADV i_ADP 'r_DET Canolbarth_NOUN yn_PART rhywle_ADV ._PUNCT
Ôô_NOUN i_ADP 'r_DET Canolbarth_NOUN ?_PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gwbod_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ar_ADP draws_ADJ y_DET cwpwrdd_NOUN 'na_ADV ._PUNCT
Mae_VERB 'r_DET ddynas_NOUN yma_DET fan_NOUN hyn_DET yn_PART byw_VERB yn_PART Gaer_PROPN ._PUNCT
<_SYM SB_SYM >_SYM Ww_X ie_INTJ ?_PUNCT
Cemist_VERB ydy_VERB hi_PRON ._PUNCT
<_SYM SB_SYM >_SYM Duw_NOUN ._PUNCT
A_PART mae_AUX gŵr_NOUN hi_PRON 'n_PART gweithio_VERB i_ADP 'r_DET dyn_NOUN yna_DET ._PUNCT
Sbia_NOUN lle_NOUN Caravan_PROPN Park_PROPN hwn_PRON ._PUNCT
Cricieth_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Mae_AUX gŵr_NOUN hi_PRON 'n_PART gweithio_VERB iddo_ADP fo_PRON ._PUNCT
Wedyn_ADV mae_AUX hi_PRON yn_PART gweithio_VERB dau_NUM ddiwrnod_NOUN y_DET wythnos_NOUN yn_PART Gaer_NOUN mewn_ADP Cemist_PROPN a_CONJ '_PUNCT dyn_NOUN mae_AUX hi_PRON 'n_PART dod_VERB i_ADP lawr_ADV i_ADP fan_NOUN hyn_DET at_ADP 'i_PRON gŵr_NOUN ._PUNCT
Mae_AUX gŵr_NOUN hi_PRON wedi_PART ymddeuo_VERB o_ADP 'i_PRON waith_NOUN ._PUNCT
enwb_VERB '_PUNCT dy_DET enw_NOUN hi_PRON ._PUNCT
enwb_VERB '_PUNCT dy_DET enw_NOUN hi_PRON ._PUNCT
Mae_VERB hi_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'di_PART cael_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Sut_ADV wyt_AUX ti_PRON 'di_PART ffeindio_VERB hynna_DET allan_ADV '_PUNCT ŵan_NOUN ?_PUNCT
Siarad_VERB i_ADP nhw_PRON ._PUNCT
Lle_ADV mae_VERB enwb_NOUN ?_PUNCT
O_ADP gwmpas_NOUN Pwllheli_PROPN 'ma_ADV ._PUNCT
Dyna_ADV ti_PRON ._PUNCT
I_ADP 'r_DET farchnad_NOUN fory_ADV a_CONJ +_SYM
Ww_ADP ie_INTJ ?_PUNCT
+_VERB dydd_NOUN mercher_NOUN ._PUNCT
Ww_ADP ie_INTJ ma_VERB hi_PRON ._PUNCT
Ie_INTJ ._PUNCT
Dw_AUX i_PRON '_PUNCT isio_ADV gwario_VERB eto_ADV fory_ADV enwb_NOUN ._PUNCT
Na_VERB fi_PRON wir_ADJ dw_AUX i_ADP 'm_DET yn_PART wario_VERB fory_ADV ._PUNCT
Wyt_VERB [_PUNCT =_SYM ]_PUNCT Ges_VERB ti_PRON [_PUNCT /=_PROPN ]_PUNCT ges_VERB ti_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ti_PRON 'm_DET angen_NOUN dim_DET byd_NOUN ?_PUNCT
Na'dw_SYM ._PUNCT
Ges_VERB ti_PRON dop_NOUN bach_ADJ gwyn_ADJ del_ADJ ._PUNCT
Oedd_VERB o_PRON ddel_NOUN i_ADP chdi_NOUN ?_PUNCT
A_CONJ ti_PRON licio_VERB fo_PRON ?_PUNCT
Oedd_VERB ofnadwy_ADJ i_ADP chdi_NOUN ._PUNCT
Da_ADJ iawn_ADV ._PUNCT
enwb_VERB yn_PART dod_VERB yn_PART ôl_NOUN o_ADP 'r_DET toiled_NOUN ._PUNCT
Wel_ADV ydy_VERB ._PUNCT
Es_VERB ti_PRON 'm_DET yn_PART hir_ADJ ._PUNCT
Es_VERB ti_PRON 'r_DET lle_NOUN chwech_VERB enwb_NOUN ?_PUNCT
'_PUNCT Mond_X pî_X pî_X '_PUNCT nest_PUNCT ti_PRON enwb_NOUN ?_PUNCT
Eh_PART ?_PUNCT
Am_ADP pî_X pî_X est_VERB ti_PRON ?_PUNCT
Wel_INTJ ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Lle_ADV wyt_AUX ti_PRON 'n_PART mynd_VERB yfory_ADV enwb_NOUN ?_PUNCT
Ellai_VERB i_ADP Gaernarfon_PROPN ._PUNCT
Dim_PRON yn_PART siŵr_ADJ iawn_ADV ._PUNCT
S1_NOUN ?_PUNCT Pwy_PRON sydd_VERB fano_NOUN ?_PUNCT
Y_DET plant_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ond_CONJ at_ADP enwb_NOUN a_CONJ 'i_PRON plant_NOUN de_NOUN ?_PUNCT
Hefo_ADP 'r_DET blant_NOUN sy_AUX 'n_PART byw_VERB 'na_ADV tydi_VERB enwb_NOUN ?_PUNCT
Ie_INTJ ._PUNCT
A_PART fo_PRON enwb_VERB 'n_PART gweithio_VERB yn_PART  _SPACE Bangor_PROPN ._PUNCT
A_CONJ '_PUNCT dyn_NOUN mae_AUX 'r_DET holl_DET gêm_NOUN di-_NOUN [_PUNCT aneglur_ADJ ]_PUNCT a_CONJ rhyw_PRON ._PUNCT
Lle_ADV mae_VERB hi_PRON ?_PUNCT
Yn_PART prifysgol_NOUN yn_ADP Bangor_PROPN ?_PUNCT
Mae_VERB 'n_PART prifysgol_ADJ +_SYM
Lle_ADV mae_VERB hi_PRON yn_PART ysgol_NOUN ?_PUNCT
+_VERB lecturo_ADJ yn_ADP y_DET prifysgol_NOUN ._PUNCT
Be_PRON mae_AUX 'n_PART neud_VERB yna_ADV '_PUNCT ŵan_NOUN '_PUNCT lly_X ?_PUNCT
lecturo_ADJ ._PUNCT
Ie_INTJ ?_PUNCT
Ie_INTJ ._PUNCT
So_NOUN ar_ADP bebe_NOUN lly_X ?_PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gwybod_VERB ._PUNCT
Jyst_ADV training_NOUN felly_ADV a_CONJ +_SYM
Darlithro_VERB ?_PUNCT
+_VERB darlithro_VERB ._PUNCT
Ie_INTJ ._PUNCT
'_PUNCT Dy_DET hi_PRON bob_DET diwrnod_NOUN ?_PUNCT
Ydy_VERB ._PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gwybod_VERB +_SYM
Full_ADJ -_PUNCT time_NOUN ._PUNCT
Full_INTJ -_PUNCT time_NOUN <_SYM /_PUNCT en_PRON >_SYM ._PUNCT
+_VERB Nacdw_PROPN ?_PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gw'bod_ADJ ._PUNCT
Yndy_INTJ ._PUNCT
Mae_AUX 'n_PART lot_PRON o_ADP waith_NOUN yn_PART ol_VERB ar_ADP [_PUNCT aneglur_ADJ ]_PUNCT y_DET blant_NOUN 'ma_ADV ._PUNCT
Ôô_NOUN yndyn_NOUN ._PUNCT
Del_VERB '_PUNCT dy_DET 'r_DET [_PUNCT =_SYM ]_PUNCT ysgol_NOUN [_PUNCT /=_PROPN ]_PUNCT dillad_NOUN ysgol_NOUN Caernarfon_PROPN Navy_NOUN blue_VERB a_CONJ coch_ADJ '_PUNCT dy_DET o_ADP ie_INTJ ?_PUNCT
Ie_INTJ ._PUNCT
Ôô_NOUN ie_INTJ ._PUNCT
'_PUNCT On_PRON i_PRON 'n_PART gweld_VERB nhw_PRON yn_PART Bontnewydd_PROPN +_SYM
Ie_INTJ ._PUNCT
+_SYM mae_VERB nhw_PRON 'n_PART neis_ADJ ._PUNCT
'_PUNCT Dy_PRON ._PUNCT
<_SYM Aneglur_PROPN 2_NUM >_SYM erstalwm_NOUN de_NOUN ._PUNCT
Ie_INTJ ._PUNCT
<_SYM SS_X >_SYM Ie_INTJ ._PUNCT
Rhai_PRON coch_ADJ yn_PART delach_ADJ na_CONJ rhai_DET gwyrdd_ADJ ._PUNCT
Coch_ADJ i_ADP 'r_DET rhei_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_CONJ bebe_NOUN lly_NOUN sy_VERB 'di_PART bod_VERB hefo_ADP gwyrdd_ADJ de_NOUN ?_PUNCT
<_SYM SB_SYM >_SYM Do_VERB 'n_PART i_ADP 'm_DET yn_PART licio_ADV gwyrdd_ADJ '_PUNCT sti_NOUN ._PUNCT
Do_AUX 'n_PART i_ADP 'm_DET yn_PART licio_VERB gwyrdd_ADJ chwaith_ADV pan_CONJ o_ADP 'n_PART i_PRON 'n_PART ysgol_NOUN nagoeddet_NOUN ?_PUNCT
Oedd_VERB jest_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM delach_NOUN doedd_VERB ?_PUNCT
Ôô_NOUN dw_AUX i_PRON yn_PART meddwl_VERB mae_VERB hi_PRON 'n_PART hogan_NOUN del_ADJ mewn_ADP blasers_NOUN a_CONJ mae_VERB rheini_NOUN ._PUNCT
Ôô_NOUN dw_AUX i_PRON 'n_PART cofio_VERB chdi_NOUN 'n_PART cael_VERB blaser_NOUN coch_ADJ ._PUNCT
Ôô_NOUN ie_INTJ ._PUNCT
<_SYM SB_SYM >_SYM Ie_INTJ ._PUNCT
O_ADP '_PUNCT ti_PRON '_PUNCT bo_VERB yn_ADP y_DET caneloedd_NOUN oedd_VERB pawb_PRON hefo_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Blaser_PROPN Navy_NOUN Blue_PROPN ?_PUNCT
Rhei_VERB 'di_PART mynd_VERB i_ADP County_PROPN [_PUNCT =_SYM ]_PUNCT Blaser_PROPN [_PUNCT /=_PROPN ]_PUNCT blaser_VERB '_PUNCT wyrdd_ADJ gennyn_ADP nhw_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Aeth_VERB mam_NOUN â_ADP fi_PRON i_ADP Gaernarfon_PROPN ._PUNCT
Gaeth_VERB hi_PRON enw_NOUN well_ADJ na_CONJ neb_PRON do_VERB ?_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Mae_VERB 'n_PART ddrytach_ADJ ._PUNCT
D'on_NOUN i_PRON 'm_DET yn_PART licio_VERB fod_VERB fi_PRON heb_ADP fynd_VERB i_ADP County_PROPN ._PUNCT
A_PART cael_VERB blaser_NOUN ._PUNCT
Pram_NOUN goch_ADJ ._PUNCT
Ôô_NOUN do_PRON ._PUNCT
O_ADP 'n_PART ni_PRON yna_ADV wrid_NOUN doedd_VERB ?_PUNCT
Ysgol_NOUN Caernarfon_PROPN ._PUNCT
'_PUNCT Sa_NOUN rheini_VERB 'n_PART ddel_ADV wan_ADJ bysa_VERB ?_PUNCT
Bysa_VERB ._PUNCT
Oedd_VERB hell_ADV i_ADP 'r_DET cael_VERB ei_PRON hogia_NOUN yn_PART doedden_VERB ?_PUNCT
[_PUNCT =_SYM ]_PUNCT enwb_NOUN [_PUNCT /=_PROPN ]_PUNCT enwb_VERB +_SYM
Ââ_VERB reit_INTJ ._PUNCT
+_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT yr_DET un_NUM fath_NOUN a_CONJ fi_PRON ._PUNCT
Ia_INTJ ._PUNCT
Ia_INTJ ._PUNCT
<_SYM SB_SYM >_SYM Ôô_NOUN ia_ADJ ._PUNCT
'_PUNCT S_NUM 'na_ADV neb_PRON yn_PART cael_VERB cacan_NOUN heddiw_ADV '_PUNCT lly_X ?_PUNCT
Dw_AUX i_PRON 'di_PART cael_VERB un_NUM ._PUNCT
enwb_VERB 'di_PART cael_VERB ._PUNCT
Myffin_NOUN cêc_NOUN ._PUNCT
Dw_AUX i_PRON 'm_DET yn_PART cael_VERB bwyta_VERB cêcs_ADJ '_PUNCT wan_ADJ ._PUNCT
Ôô_NOUN na_INTJ ._PUNCT
Mae_AUX hi_PRON 'n_PART meddwl_VERB bod_VERB y_DET +_SYM
'_PUNCT c'ofn_PRON ._PUNCT
+_VERB siwgwr_NOUN yn_PART uchel_ADJ ._PUNCT
Mae_VERB o_PRON ._PUNCT
Dydy_VERB o_PRON ddim_PART siŵr_ADJ ?_PUNCT
Ydy_VERB mae_VERB o_PRON ._PUNCT
Mae_AUX nhw_PRON 'di_PART deud_VERB ._PUNCT
Ââ_VERB ie_INTJ ._PUNCT
Dw_AUX i_PRON '_PUNCT isio_ADV mynd_VERB yn_PART ôl_NOUN ŵan_NOUN '_PUNCT dy_PRON ._PUNCT
Ie_INTJ borderline_NOUN ._PUNCT
Wel_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Mae_VERB o_PRON 'n_PART uwch_ADJ nag_CONJ oedd_VERB o'blaen_NOUN eto_ADV ._PUNCT
A_PART dw_VERB i_PART methu_ADV dallt_VERB ._PUNCT
Dw_AUX i_PRON 'n_PART bwyta_VERB 'r_DET holl_DET '_PUNCT nialwch_NOUN 'ma_ADV a_CONJ mae_VERB un_NUM fi_PRON yn_PART iawn_ADJ a_CONJ mae_VERB 'r_DET colesterol_NOUN yn_PART iawn_ADJ ._PUNCT
Mae_AUX 'r_DET pressure_NOUN dim_DET de_NOUN ._PUNCT
<_SYM SB_SYM >_SYM [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Sut_ADV bod_VERB colesterol_NOUN chdi_NOUN 'n_PART iawn_ADJ dŵad_NOUN ?_PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gwbod_VERB ._PUNCT
Dw_VERB i_PRON 'n_PART holl_DET rwtsh_NOUN ._PUNCT
Ti_PRON 'n_PART bwyta_VERB bach_ADJ o_ADP caws_NOUN ?_PUNCT
Nadw_INTJ ._PUNCT
Dim_DET byd_NOUN ._PUNCT
Saf_VERB '_PUNCT dan_ADP ni_PRON 'n_PART ffonio_VERB os_CONJ oes_VERB yna_ADV rhywsut_ADV ._PUNCT
Dw_AUX i_PRON yn_PART bwyta_VERB caws_NOUN de_NOUN ond_CONJ dw_AUX i_PRON 'n_PART byta_VERB lot_PRON o_ADP fries_VERB anialwch_VERB '_PUNCT dw_VERB ._PUNCT
Mars_INTJ -_PUNCT Bars_PROPN a_CONJ pethau_NOUN 'ma_ADV ._PUNCT
Ond_CONJ ti_PRON 'n_PART ddeud_VERB ti_PRON 'm_DET '_PUNCT isio_VERB os_CONJ ti_PRON 'n_PART ofn_NOUN iawn_ADV ._PUNCT
Ond_CONJ mae_AUX wyt_AUX yndwt_ADJ enwb_NOUN ŵan_NOUN ?_PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Cadw_VERB 'n_PART da'_ADJUNCT dwt_ADJ ?_PUNCT
Ie_INTJ ._PUNCT
I_ADP feddwl_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Fel_CONJ dw_VERB i_PRON ._PUNCT
Yndw_VERB dw_AUX i_PRON 'n_PART fwyta_VERB iawn_ADV de_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Ellai_VERB bod_VERB yn_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ti_PRON erioed_ADV 'di_PART cael_VERB ?_PUNCT
Unwaith_ADV pan_CONJ oeddwn_AUX i_PRON 'n_PART gweithio_VERB yn_ADP Plas_NOUN Corn_PROPN a_CONJ oedd_VERB o_PRON 'n_PART jyst_ADV yn_PART dŵad_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Os_CONJ o_ADP '_PUNCT ti_PRON '_PUNCT isio_ADV testio_VERB dy_DET waed_NOUN a_CONJ pethau_NOUN a_CONJ ti_PRON 'n_PART mynd_VERB ati_ADP ._PUNCT
Ie_INTJ ._PUNCT
A_PART  _SPACE colesterol_NOUN yn_PART bach_ADJ ._PUNCT
Ddim_PART llawer_ADJ ._PUNCT
Dim_DET byd_NOUN i_ADP poeni_VERB am_ADP fatha_NOUN hi_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB yn_PART ôl_NOUN blynyddoedd_NOUN ŵan_NOUN '_PUNCT dw_VERB ?_PUNCT
A_PART dw_AUX i_PRON 'n_PART cofio_VERB fi_PRON Eighty_X percent_X drwg_ADJ a_CONJ [_PUNCT =_SYM ]_PUNCT twenty_NOUN [_PUNCT /=_PROPN ]_PUNCT twenty_NOUN da_ADJ ._PUNCT
'_PUNCT On_PRON i_PRON 'n_PART meddwl_VERB rhwbath_NOUN arall_ADJ oeddet_VERB ti_PRON ._PUNCT
Naci_PROPN ._PUNCT
Oedd_VERB hi_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Twenty_VERB drwg_ADJ gen_ADP i_PRON ._PUNCT
Ie_INTJ ._PUNCT
Naci_NOUN mwyaf_ADJ ._PUNCT
Yn_PART uchel_ADV ofnadwy_ADJ ._PUNCT
Ôô_NOUN taw_ADP ._PUNCT
Ôô_NOUN 'na_ADV fo_PRON ._PUNCT
Sa_CONJ fo_PRON i_ADP 'm_DET yma_DET fydda_VERB i_ADP 'm_DET yma_ADV na_PART fydda_VERB ?_PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Paid_VERB a_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ôô_NOUN dim_PRON ._PUNCT
Paid_VERB a_CONJ deud_VERB hynny_PRON wrth_ADP enwg_VERB cyfenw_NOUN ._PUNCT
Wel_INTJ ia_PRON ._PUNCT
'_PUNCT Sa_PROPN honna_VERB fethu_ADV neud_VERB hebddot_NOUN ti_PRON na_PART '_PUNCT sa_PRON ?_PUNCT
'_PUNCT Sa_NOUN fo_PRON 'n_PART well_ADJ hebddo_ADP fitha_ADJ ._PUNCT
Rho_VERB cliw_NOUN '_PUNCT ta_NOUN ._PUNCT
Rhaid_VERB i_ADP rhywun_NOUN neud_VERB o_ADP ._PUNCT
Ti_PRON '_PUNCT bo_VERB ._PUNCT
Dydy_VERB o_ADP 'm_DET yn_PART hawdd_ADJ nac_CONJ ydy_VERB ?_PUNCT
Na_INTJ ._PUNCT
Dydy_VERB o_PRON ddim_PART ._PUNCT
Oedd_AUX enwb_NOUN 'di_PART deud_VERB mae_AUX Dad_PROPN yn_PART cael_VERB gymaint_ADV o_ADP sylw_NOUN hefo_ADP 'i_PRON salwch_NOUN ._PUNCT
Mae_VERB '_PUNCT sa_AUX neb_PRON yn_PART gofyn_VERB be_ADP dach_VERB chi_PRON ._PUNCT
'_PUNCT Dach_AUX chi_PRON 'n_PART deud_VERB '_PUNCT bo_VERB chi_PRON 'n_PART iawn_ADJ medda_VERB enwb_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Bod_VERB dim_PRON +_SYM
Ia_INTJ ._PUNCT
+_AUX yn_PART gofyn_VERB i_ADP fi_PRON ._PUNCT
Fathau_VERB rhywun_NOUN sy_VERB methu_ADV neud_VERB pethau_NOUN a_CONJ ti_PRON 'n_PART teimlo_VERB 'n_PART olew_ADJ ._PUNCT
Does_VERB na_CONJ 'm_DET byd_NOUN am_ADP hynna_DET nagoes_NOUN ?_PUNCT
Ti_PRON 'm_DET yn_PART rêl_NOUN ._PUNCT
Ti_PRON 'm_DET yn_PART un_NUM berffaith_NOUN ._PUNCT
Ydw_VERB tad_NOUN ._PUNCT
Dw_VERB i_PRON 'n_PART dda_ADJ iawn_ADV ._PUNCT
Blydi_VERB hell_ADV ydw_VERB ._PUNCT
Yn_PART [_PUNCT aneglur_ADJ ]_PUNCT i_ADP dim_PRON ._PUNCT
Ôô_NOUN na_INTJ ._PUNCT
Wyt_INTJ ._PUNCT
Dau_NUM '_PUNCT dy_DET o_ADP dan_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Yndy_INTJ ._PUNCT
Ond_CONJ mi_PART wyt_VERB ti_PRON '_PUNCT dwt_ADJ ?_PUNCT
Clywais_VERB i_ADP erioed_ADV o_ADP 'r_DET gair_NOUN yma_DET o_ADP blaen_NOUN ._PUNCT
BeBe_VERB dy_DET o_ADP enwb_NOUN ?_PUNCT
"_PUNCT Defnyddir_VERB y_DET casgliad_NOUN neu_CONJ gorpws_VERB hwn_PRON i_PART greu_VERB adnodd_NOUN "_PUNCT ._PUNCT
BeBe_VERB dy_DET "_PUNCT adnodd_NOUN "_PUNCT ?_PUNCT
Ie_INTJ ._PUNCT
"_PUNCT Arlein_NOUN cyhoeddus_ADJ  _SPACE mwyn_NOUN ymchwylio_VERB ac_CONJ arstudio_VERB 'r_DET Gymraeg_PROPN fel_CONJ y_PART defnyddir_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT "_PUNCT Yn_ADP Saesneg_PROPN de_NOUN "_PUNCT This_X collection_X or_ADV corpus_ADJ will_ADJ be_ADP used_NOUN to_NOUN create_NOUN an_PRON accessible_VERB online_VERB public_NOUN resource_NOUN for_CONJ exploring_NOUN "_PUNCT ._PUNCT
So_INTJ ,_PUNCT "_PUNCT gawn_VERB ni_PART greu_VERB adnodd_NOUN arlein_NOUN "_PUNCT ._PUNCT
"_PUNCT Create_NOUN an_PRON accessible_VERB online_VERB public_NOUN resource_NOUN "_PUNCT ._PUNCT
"_PUNCT Adnodd_VERB "_PUNCT clywais_VERB i_ADP erioed_ADV o_ADP 'r_DET gair_NOUN ._PUNCT
Ôô_NOUN bechod_NOUN ._PUNCT
Sa_ADV rhei_VERB +_SYM
[_PUNCT =_SYM ]_PUNCT Na_VERB na_INTJ [_PUNCT /=_PROPN ]_PUNCT na_INTJ ._PUNCT
+_VERB 'di_PART deud_VERB ._PUNCT
D'eda_VERB mae_VERB hwn_PRON i_PART greu_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Beth_PRON am_ADP y_DET llythyr_NOUN mae_VERB ganddo_ADP enwb_NOUN ?_PUNCT
Wel_INTJ ia_PRON ._PUNCT
Seren_PROPN ._PUNCT
Ww_ADP ._PUNCT
Ia_INTJ ._PUNCT
Ti_PRON [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
Ti_PRON 'n_PART cofio_VERB ?_PUNCT
Ia_INTJ ._PUNCT
Yndw_INTJ ._PUNCT
Ti_PRON 'n_PART cofio_VERB enwb_NOUN ?_PUNCT
Llythyr_NOUN o_ADP prifysgol_NOUN de_NOUN ?_PUNCT
Ia_INTJ ?_PUNCT
A_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
To_NOUN Star_NOUN cyfenw_NOUN ._PUNCT
Seren_PROPN cyfenw_NOUN ._PUNCT
Star_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Oedd_VERB Seren_PROPN cyfenw_NOUN '_PUNCT dy_DET enw_NOUN hi_PRON +_SYM
<_SYM SS_X >_SYM Ie_INTJ ._PUNCT
+_VERB a_CONJ enw_NOUN 'r_DET ty_NOUN ydy_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Awelon_INTJ ._PUNCT
Awelon_INTJ ._PUNCT
Seabreeze_NOUN oedd_VERB o_PRON ._PUNCT
Ââ_VERB ia_PRON ._PUNCT
O_ADP '_PUNCT nhw_PRON newid_VERB o_ADP ._PUNCT
Newid_NOUN o_ADP i_ADP Saesneg_PROPN ._PUNCT
Peth_NOUN ydy_VERB o_ADP i_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Beth_PRON '_PUNCT nath_NOUN nhw_PRON deud_VERB bod_VERB rhywun_NOUN anabl_ADJ ydy_VERB ._PUNCT
Cael_VERB ei_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Neud_VERB yno_ADV fel_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Am_ADP berson_NOUN anabl_ADJ oedden_ADP nhw_PRON 'n_PART dda_ADJ iawn_ADV medru_VERB newid_NOUN o_ADP ._PUNCT
Wel_CONJ oedd_VERB clyfar_ADJ iawn_ADV ._PUNCT
Doedden_VERB ?_PUNCT
A_PART deud_VERB bod_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Gafon_VERB ni_PRON lythyr_NOUN +_SYM
Deud_VERB bod_VERB nhw_PRON 'di_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
+_VERB i_PART ddeud_VERB bod_NOUN ddrwg_ADJ ganddyn_ADP nhw_PRON rhyw_DET ffordd_NOUN felly_CONJ de_NOUN ?_PUNCT
'_PUNCT On_PRON i_PRON 'n_PART weld_VERB +_SYM
Oedd_AUX enwg_VERB yn_PART +_SYM
+_VERB o_ADP 'n_PART od_ADJ iawn_NOUN ._PUNCT
+_SYM cael_VERB gradd_NOUN +_SYM
enwb_VERB [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
+_VERB yn_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT hefyd_ADV a_CONJ ti_PRON 'm_DET yn_PART deud_VERB [_PUNCT saib_NOUN ]_PUNCT byth_ADV yn_PART darn_NOUN o_ADP prifysgol_NOUN ._PUNCT
Ia_INTJ ._PUNCT
A_PART mae_VERB o_PRON bod_VERB yna_ADV i_PART gael_VERB ei_PRON gapio_VERB ._PUNCT
Bob_DET un_NUM dim_DET yn_ADP Saesneg_PROPN yna_ADV ._PUNCT
Ôô_NOUN ._PUNCT
Sobor_NOUN hefyd_ADV ynde_ADV ?_PUNCT
Yn_PART -_PUNCT Llanel_PROPN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ond_CONJ y_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_CONJ wedyn_ADV '_PUNCT dan_ADP ni_PRON 'n_PART siarad_VERB yn_PART fama_ADV iddyn_ADP nhw_PRON glywed_VERB ni_PRON 'n_PART siarad_VERB Cymraeg_PROPN ._PUNCT
Yndan_PROPN ?_PUNCT
Ia_INTJ ._PUNCT
Yndan_PROPN ._PUNCT
I_ADP fod_VERB ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART gweld_VERB sefydliad_NOUN yn_ADP Pwllheli_PROPN 'ma_ADV wedi_PART gwneud_VERB joban_NOUN dda_ADJ ar_ADP [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
Ydan_AUX ni_PRON 'n_PART neud_VERB yn_PART iawn_ADJ ?_PUNCT
Ar_ADP y_DET gongl_NOUN yn_PART fana_ADV ._PUNCT
Mae_AUX 'r_DET lle_NOUN yn_PART edrych_VERB yn_ADP +_SYM
Ti_PRON 'n_PART glywed_VERB ni_PRON wyt_VERB ?_PUNCT
+_VERB ardderchog_ADJ i_PART dweud_VERB y_DET gwir_NOUN ._PUNCT
'_PUNCT Dydy_PROPN ?_PUNCT
Ond_CONJ does_VERB na_PART 'm_DET un_NUM gair_NOUN o_ADP Gymraeg_PROPN yna_ADV ._PUNCT
Does_VERB na_CONJ mond_CONJ gair_NOUN "_PUNCT croeso_INTJ "_PUNCT +_SYM
Ti_PRON 'di_PART gorffen_VERB '_PUNCT wan_ADJ ?_PUNCT
+_VERB yna_ADV de_NOUN ?_PUNCT
Dw_AUX i_PRON '_PUNCT isio_ADV mynd_VERB yn_PART ol_VERB i_ADP 'm_DET car_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Waitiwch_VERB ._PUNCT
Mae_VERB 'na_ADV hogan_NOUN +_SYM
'_PUNCT Sa_X jyst_X "_PUNCT croeso_INTJ "_PUNCT 'na_ADV de_NOUN ?_PUNCT
+_VERB bach_ADJ yn_PART dod_VERB i_ADP nol_NOUN teclun_NOUN hi_PRON wan_ADJ ._PUNCT
Ia_INTJ ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
459.840_VERB
