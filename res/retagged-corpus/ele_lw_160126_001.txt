Bore_NOUN da_ADJ ,_PUNCT
Gair_NOUN i_ADP bawb_PRON i_ADP hysbysebu_VERB noson_NOUN yng_ADP nghwmni_NOUN siaradwyr_NOUN gwadd_ADJ ._PUNCT
Mae_AUX 'r_DET enw_NOUN yn_PART codi_VERB arian_NOUN tuag_ADP at_ADP lyfrgell_NOUN enw_NOUN __NOUN ._PUNCT
Y_DET dau_NUM siaradwr_NOUN yw_VERB :_PUNCT enwb1_NOUN cyfenw1_ADJ __NOUN sy_VERB newydd_ADV gyhoeddu_NOUN nofel_NOUN ,_PUNCT __NOUN __NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ,_PUNCT sy_VERB 'n_PART __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN a_CONJ sy_AUX 'n_PART ennill_VERB ei_DET damaid_NOUN yno_ADV ers_ADP bron_ADV i_ADP 18_NUM mlynedd_NOUN ._PUNCT
Bydd_AUX y_DET ddau_NUM yn_PART siarad_VERB am_ADP broses_NOUN creu_VERB eu_DET gwaith_NOUN a_CONJ byddant_AUX yn_PART ateb_VERB cwestiynau_NOUN ar_ADP ddiwedd_NOUN y_DET sesiwn_NOUN ._PUNCT
Bydd_VERB lluniaeth_NOUN ysgafn_ADJ ar_ADP gael_VERB ar_ADP y_DET noson_NOUN ._PUNCT
Beth_PRON -_PUNCT Noson_VERB gyda_ADP dau_NUM siaradwr_NOUN gwadd_NOUN
Pwy_PRON -_PUNCT enwb1_NOUN cyfenw1_ADJ __NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN
Ble_ADV -_PUNCT Yng_ADP neuadd_NOUN enw_NOUN
Pryd_ADV -_PUNCT ddydd_NOUN Llun_PROPN 8_NUM fed_VERB o_ADP Chwefror_PROPN ,_PUNCT 7_NUM -_SYM 9_NUM gyda_ADP 'r_DET hwyr_NOUN
Pam_ADV -_PUNCT i_PART godi_VERB arian_NOUN tuag_ADP at_ADP y_DET llyfrgell_NOUN
Croeso_NOUN i_ADP bawb_PRON !_PUNCT
