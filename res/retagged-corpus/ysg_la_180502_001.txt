Newyddlen_VERB 4_NUM
Ionawr_NOUN 2018_NUM
Gair_NOUN gan_ADP y_DET Pennaeth_PROPN
Mae_AUX 'r_DET flwyddyn_NOUN ysgol_NOUN yn_PART hedfan_ADJ ac_CONJ mae_VERB ein_DET Blynyddoedd_NOUN 11_NUM -_PUNCT 13_NUM yn_PART dechrau_VERB gweld_VERB eu_DET arholiadau_NOUN yn_PART ymddangos_VERB ar_ADP y_DET gorwel_NOUN ._PUNCT
Maent_VERB yn_PART brysur_ADJ yn_PART gorffen_VERB eu_DET gwaith_NOUN cwrs_NOUN ,_PUNCT rhoi_VERB sglein_NOUN ar_ADP y_DET llafar_NOUN ac_CONJ yn_PART hoelio_VERB 'r_DET hoelen_NOUN olaf_ADJ i_ADP 'r_DET darnau_NOUN D_NUM &_ADP Th_X ._PUNCT
Mae_AUX Bl_PRON 11_NUM yn_PART meddwl_VERB am_ADP eu_DET cam_NOUN nesaf_ADJ :_PUNCT aros_VERB yn_ADP yr_DET ysgol_NOUN ;_PUNCT mynd_VERB i_ADP goleg_NOUN 6_NUM ed_NUM dosbarth_NOUN neu_CONJ ymgeisio_VERB am_ADP brentisiaeth_NOUN ac_CONJ mae_AUX Blwyddyn_NOUN 9_NUM yn_PART dechrau_VERB pendroni_VERB am_ADP eu_DET pynciau_NOUN opsiwn_ADJ ._PUNCT
Hyn_PRON oll_ADV gyda_ADP 'r_DET sicrwydd_NOUN bod_VERB eu_DET siawns_NOUN o_ADP ennill_NOUN swydd_NOUN dda_ADJ yn_PART uwch_ADJ gan_CONJ eu_PRON bod_VERB yn_PART ddisgyblion_NOUN dwyieithog_ADJ ._PUNCT
Mae_AUX 'r_DET ysgol_NOUN hefyd_ADV wrthi_ADP fel_ADP lladd_VERB nadredd_NOUN yn_PART paratoi_VERB ar_ADP gyfer_NOUN yr_DET Eisteddfod_NOUN ysgol_NOUN ar_ADP Fawrth_NOUN 2_NUM il_PRON ._PUNCT
Mae_AUX 'r_DET Eisteddfod_NOUN yn_PART gynnar_ADJ eleni_ADV er_ADP mwyn_NOUN i_ADP ni_PRON fedru_ADV llwyfannu_VERB ein_DET sioe_NOUN gerdd_NOUN '_PUNCT Dal_NOUN Sownd_X '_PUNCT yn_ADP yr_DET wythnos_NOUN llawn_ADJ olaf_ADJ o_ADP dymor_NOUN yr_DET haf_NOUN ._PUNCT
Rhywbeth_NOUN yr_PART ydym_VERB i_PRON gyd_NOUN yn_PART gyffrous_ADJ amdano_ADP ._PUNCT
Cyhoeddwyd_VERB Y_DET Categoreiddio_VERB Ysgolion_NOUN Cenedlaethol_ADJ yr_DET wythnos_NOUN ddiwethaf_ADJ gyda_ADP 'r_DET ysgol_NOUN yn_PART disgyn_VERB i_ADP 'r_DET coch_ADJ ._PUNCT
Fel_ADP ysgol_NOUN ,_PUNCT cawsom_VERB wybod_VERB am_ADP hyn_PRON ddiwedd_NOUN mis_NOUN Medi_PROPN gan_CONJ fod_VERB y_DET canlyniadau_NOUN yn_PART seiliedig_ADJ yn_PART bennaf_ADJ ar_ADP -_PUNCT 2016_NUM 17_NUM ._PUNCT
O_ADP ganlyniad_NOUN rydym_AUX wedi_PART bod_AUX yn_PART gweithio_VERB 'n_PART galed_ADJ gyda_ADP 'r_DET Consortiwm_PROPN Addysg_PROPN (_PUNCT GCA_PROPN )_PUNCT a_CONJ 'r_DET Awdurdod_NOUN Lleol_PROPN er_ADP mwyn_NOUN codi_VERB safonau_NOUN a_CONJ deilliannau_NOUN ar_ADP bob_DET lefel_NOUN ._PUNCT
Mae_AUX 'r_DET gwaith_NOUN yn_PART parhau_VERB ac_CONJ y_PART mae_VERB 'r_DET staff_NOUN i_ADP gyd_ADP wedi_PART ymrwymo_VERB i_ADP wella_VERB safle_NOUN 'r_DET ysgol_NOUN ._PUNCT
Gobeithio_VERB y_PART gwnewch_VERB fwynhau_VERB 'r_DET newyddlen_NOUN ddiweddaraf_ADJ yma_ADV ac_CONJ yn_PART cymryd_VERB y_DET cyfle_NOUN i_ADP ymfalchi_VERB ̈_NOUN o_ADP fel_ADP finnau_PRON yn_PART llwyddiannau_VERB ein_DET disgyblion_NOUN ar_ADP draws_ADJ y_DET meysydd_NOUN ._PUNCT
Ymweliadau_PROPN
Cafodd_VERB Bl_PRON 9_NUM sesiwn_NOUN ar_ADP opsiynnau_NOUN gan_ADP enwb2_NOUN cyfenw1_ADJ __NOUN o_ADP Brifysgol_NOUN Caerdydd_PROPN ar_ADP ba_NOUN mor_ADV bwysig_ADJ yw_ADV gwneud_VERB y_DET dewisiadau_NOUN cywir_ADJ ar_ADP gyfer_NOUN y_DET dyfodol_NOUN ._PUNCT
Cafodd_VERB Bl_PRON 9_NUM hefyd_ADV ymweliad_NOUN gan_ADP y_DET nyrs_NOUN ac_CONJ fe_PART gafodd_VERB y_DET flwyddyn_NOUN gyfan_ADJ eu_PRON brechu_VERB rhag_ADP sawl_ADJ haint_NOUN a_CONJ dolur_NOUN cas_NOUN ._PUNCT
Cafodd_VERB Bl_PRON 12_NUM gwmni_NOUN 'r_DET actorion_NOUN Ieuan_NOUN Rhys_PROPN ,_PUNCT Lisa_PROPN Jen_PROPN a_CONJ chriw_NOUN o_ADP S4C_PROPN a_CONJ fu_AUX 'n_PART trafod_VERB eu_DET gwaith_NOUN diweddara_VERB sef_CONJ y_DET gyfres_NOUN ddrama_NOUN Gwaith_NOUN Cartref_NOUN a_CONJ ddangosir_VERB bob_DET nos_NOUN Iau_ADJ ._PUNCT
Bu_AUX 'r_DET Urdd_NOUN a_CONJ 'r_DET Mentrau_NOUN Iaith_NOUN yn_ADP yr_DET ysgol_NOUN yn_PART cynnal_VERB cyfarfod_NOUN gyda_ADP Fforwm_PROPN Blwyddyn_NOUN 10_NUM i_PART drefnu_VERB 'r_DET digwyddiadau_NOUN nesaf_ADJ ._PUNCT
Pontio_PROPN
Bydd_VERB y_DET ti_PRON ̂_PART m_PART pontio_VERB yn_PART ymweld_VERB a_CONJ ̂_SYM phob_DET ysgol_NOUN gynradd_NOUN yn_ADP ei_DET thro_NOUN er_ADP mwyn_NOUN siarad_VERB a_CONJ ̂_NUM rhienin_NOUN Bl_PROPN 6_NUM am_ADP faterion_NOUN trosglwyddo_ADP
Mawrth_NOUN /_PUNCT March_PROPN 5_NUM ed_ADV -_PUNCT Bryn_PROPN Onnen_PROPN
Mawrth_NOUN /_PUNCT March_PROPN 6_NUM ed_ADV -_PUNCT Bro_NOUN Helyg_PROPN
Mawrth_NOUN /_PUNCT March_PROPN 13_NUM eg_PRON -_PUNCT Cwmbran_NOUN
Mawrth_NOUN /_PUNCT March_PROPN 19_NUM eg_PRON -_PUNCT Panteg_PROPN
Mawrth_NOUN /_PUNCT March_PROPN 20_NUM fed_NOUN -_PUNCT Y_DET Fenni_PROPN
Cynhaliwyd_VERB diwrnod_NOUN ABCh_ADJ i_ADP Fl_PROPN 11_NUM ar_ADP Chwefror_PROPN 1_NUM af_NOUN ._PUNCT
Cafwyd_VERB cyflwyniadau_NOUN gan_ADP Miss_PROPN cyfenw4_PROPN a_CONJ Miss_PROPN enwb3_VERB __NOUN __NOUN __NOUN ar_ADP opsiynnau_NOUN a_CONJ dewisiadau_NOUN ._PUNCT
Hyfryd_ADJ oedd_VERB cael_VERB croesawu_VERB yn_PART ol_VERB ein_DET cyn_ADP Brif_PROPN Swyddogion_PROPN i_ADP siarad_VERB a_CONJ ̂_SYM Bl_PRON 11_NUM sef_CONJ enwg2_SYM cyfenw6_NOUN __NOUN ,_PUNCT __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Cafywd_VERB sesiynnau_ADV gan_ADP PC_X Thomas_PROPN ,_PUNCT enwbg2_VERB cyfenw10_NOUN __NOUN o_ADP 'r_DET Tim_NOUN Gyrfaoedd_NOUN ,_PUNCT Bron_ADV Afon_NOUN yn_PART son_VERB am_ADP ddi-_DET gartrefedd_NOUN a_CONJ Cartrefi_PROPN Melin_PROPN ._PUNCT
Cafwyd_VERB cyflwyniad_NOUN am_ADP y_DET CV_NOUN gan_ADP Mr_NOUN enwg3_ADJ cyfenw9_NOUN __NOUN ._PUNCT
Honours_NOUN and_NOUN Achievements_PROPN
Rydym_AUX yn_PART ymfalchio_VERB yn_PART llwyddiant_NOUN enwg4_NOUN cyfenw11_NOUN __NOUN Bl_PRON 11_NUM ar_ADP ddod_VERB yn_PART bencampwr_NOUN beicio_VERB Cymru_PROPN ._PUNCT
Llwyddodd_VERB i_ADP ennill_VERB llond_NOUN dwrn_NOUN o_ADP fedalau_NOUN 'n_PART ddiweddar_ADJ a_CONJ bellach_ADV mae_VERB ar_ADP gyrion_NOUN carfan_NOUN Prydain_PROPN ._PUNCT
Military_PROPN Prep_PROPN
Rhannwyd_VERB nifer_NOUN o_ADP ddisgyblion_NOUN dystysgrifau_NOUN gan_ADP Staff_NOUN Sullivan_X o_ADP 'r_DET Military_PROPN Prep_PROPN am_ADP eu_DET hymroddiad_NOUN a_CONJ 'u_PRON llwyddiant_NOUN ar_ADP y_DET cwrs_NOUN heriol_ADJ a_CONJ chaled_VERB hwn_DET ._PUNCT
Trip_PROPN Daearyddiaeth_PROPN
Yn_PART ystod_NOUN yr_DET hanner_NOUN tymor_NOUN dwethaf_ADJ ,_PUNCT mi_PART fuodd_VERB criw_NOUN Daearyddwyr_NOUN blwyddyn_NOUN 12_NUM ar_ADP waith_NOUN maes_NOUN i_ADP Ogledd_NOUN Cymru_PROPN ar_ADP y_DET cyd_NOUN gyda_ADP ysgolion_NOUN Cymraeg_PROPN y_DET de_NOUN -_PUNCT ddwyrain_NOUN er_ADP mwyn_NOUN paratoi_VERB at_ADP eu_DET haroliadau_NOUN ._PUNCT
Gan_CONJ ddefnyddio_VERB gwersyll_NOUN yr_DET Urdd_PROPN ,_PUNCT Glanllyn_PROPN fel_CONJ ein_DET HQ_NOUN cwblhawyd_VERB astudiaeth_NOUN daearyddiaeth_NOUN dynol_ADJ yn_ADP y_DET Bala_PROPN i_PART weld_VERB sut_ADV mae_AUX trefi_VERB 'n_PART newid_VERB ac_CONJ yna_ADV astudiaethau_VERB daearyddiaeth_NOUN ffisegol_ADJ yn_PART twyni_VERB tywod_NOUN Harlech_PROPN ac_CONJ yn_PART nyffryn_NOUN rhewlifol_ADJ Cwm_NOUN Idwal_PROPN ._PUNCT
Cafodd_VERB disgyblion_NOUN y_DET profiad_NOUN o_ADP gasglu_VERB data_NOUN yn_ADP y_DET maes_NOUN ac_CONJ yna_ADV eu_PRON dadansoddi_VERB er_ADP mwyn_NOUN gweld_VERB patrymau_NOUN daearyddol_ADJ ._PUNCT
Roedd_VERB yn_PART waith_NOUN caled_ADJ yn_ADP y_DET tymhereddau_NOUN oer_ADJ ond_CONJ cafoddpawb_ADJ foddhad_NOUN !_PUNCT
Hanes_NOUN
Mae_AUX 'r_DET Adran_NOUN Hanes_PROPN yn_PART trefnu_VERB taith_NOUN gyda_ADP disgyblion_NOUN Hy_PRON ̂_SYM n_ADP yr_DET ysgol_NOUN i_PART wersyll_NOUN Crynhoi_VERB Auschwitz_PROPN ._PUNCT
Mae_VERB 'r_DET edrych_VERB ymlaen_ADV yn_PART fawr_ADJ ._PUNCT
Cymraeg_NOUN
Mae_AUX 'r_DET Adran_NOUN Gymraeg_PROPN yn_PART trefnu_VERB trip_NOUN i_PART weld_VERB cynhyrchiad_NOUN diweddara_VERB Theatr_NOUN Genedlaethol_ADJ Cymru_PROPN o_ADP 'r_DET Tad_NOUN sef_CONJ trosiad_NOUN newydd_ADJ gan_ADP Geraint_PROPN Løvgreen_PROPN o_ADP Le_NOUN Pe_X ̀_X re_NUM ,_PUNCT drama_NOUN Ffrangeg_PROPN o_ADP fri_NOUN ._PUNCT
Maent_AUX wedi_PART llwyddo_VERB i_PART sicrhau_VERB grant_NOUN a_CONJ fydd_AUX yn_PART caniatau_VERB 'r_DET disgyblion_NOUN i_PART weld_VERB y_DET ddrama_NOUN am_ADP bris_NOUN hynod_ADJ o_ADP resymol_ADJ ._PUNCT
Ieithoedd_NOUN Modern_PROPN
Am_ADP drydedd_NOUN flwyddyn_NOUN yn_PART olynol_ADJ mae_VERB disgyblion_NOUN bl_ADP 9_NUM ITM_PROPN yn_PART cymryd_VERB rhan_NOUN ym_ADP mhrosiect_NOUN mentora_NOUN Prifysgol_NOUN Caerdydd_PROPN ._PUNCT
Mae_AUX mentor_NOUN o_ADP 'r_DET brifysgol_NOUN (_PUNCT Glesni_PROPN )_PUNCT yn_PART dod_VERB i_ADP weithio_VERB efo_ADP grwpiau_NOUN bach_ADJ o_ADP ddisgyblion_NOUN bl_ADP 9_NUM er_ADP mwyn_NOUN cynnyddu_VERB eu_DET hymwybyddiaeth_NOUN o_ADP fanteision_VERB astudio_VERB ITM_PROPN ._PUNCT
Dyma_DET gynllun_NOUN peilot_NOUN Llywodraeth_NOUN Cymru_PROPN i_ADP gynyddu_VERB niferoedd_NOUN ITM_X CA_X 4_NUM ac_CONJ mae_VERB 'r_DET cynllun_NOUN newydd_ADJ ennill_VERB gwobr_NOUN Cwpan_NOUN Threlford_PROPN ._PUNCT
Drama_PROPN
Ar_ADP yr_DET un_NUM dyddiad_NOUN mae_AUX 'r_DET Adran_NOUN Ddrama_PROPN yn_PART digwydd_VERB bod_AUX yn_PART trefnu_VERB trip_NOUN i_PART weld_VERB y_DET ddrama_NOUN gerdd_NOUN Hairspray_PROPN ._PUNCT
Cerddoriaeth_NOUN
Mae_AUX Miss_PROPN cyfenw1_ADJ wedi_PART trefnu_VERB bod_VERB yna_ADV bobl_NOUN yn_PART galw_VERB mewn_ADP yn_PART wythnosol_ADJ i_PART gynnal_VERB sesiwn_NOUN '_PUNCT jamio_VERB '_PUNCT gyda_ADP rhai_DET disgyblion_NOUN ._PUNCT
Academaidd_ADJ
Braf_ADJ cyhoeddi_VERB bod_VERB yna_ADV ddeg_NOUN o_ADP Fl_NOUN 13_NUM wedi_PART sefyll_VERB arholiadau_ADJ ysgoloriaeth_NOUN Prifysgol_NOUN Aberystwyth_PROPN ._PUNCT
Croeswn_VERB fysedd_NOUN a_CONJ dymuno_VERB 'n_PART dda_ADJ i_ADP hwy_PRON gan_ADP eu_DET llongyfarch_NOUN ar_ADP osod_NOUN eu_DET bryd_NOUN ar_ADP barhau_VERB a_CONJ 'u_PRON hadysg_NOUN yng_ADP Nghymru_PROPN ._PUNCT
Y_DET Clwb_NOUN Llwyddo_PROPN
Mae_AUX 'r_DET ysgol_NOUN wedi_PART cychwyn_VERB clwb_NOUN gwaith_NOUN cartref_NOUN newydd_ADJ ar_ADP ol_NOUN yr_DET ysgol_NOUN er_ADP mwyn_NOUN rhoi_VERB cyfle_NOUN i_ADP ddisgyblion_NOUN i_ADP barhau_VERB a_CONJ 'u_PRON hastudiaethau_VERB gan_CONJ roi_VERB cyfle_NOUN i_ADP sawl_ADJ disgybl_NOUN fanteisio_VERB ar_ADP adnoddau_NOUN 'r_DET ysgol_NOUN a_CONJ 'r_DET llonydd_NOUN sydd_VERB i_ADP 'w_PRON gael_VERB mewn_ADP awyrgylch_NOUN addsygol_ADJ ._PUNCT
Sgi_NOUN ̈_VERB o_PRON
Cafodd_VERB enwg_VERB cyfenw2_SYM __NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN Bl_PRON 10_NUM brofiad_NOUN amhrisiadwy_ADJ wrth_ADP iddyn_ADP nhw_PRON gystadlu_VERB 'n_PART llwyddiannus_ADJ ym_ADP Mhencampwriaethau_NOUN Sgio_PROPN yn_ADP Champery_PROPN Swistir_PROPN ._PUNCT
Dyma_DET 'r_DET Welsh_NOUN Alpine_NOUN Ski_NOUN Championships_PROPN ._PUNCT
Cyastadlodd_VERB y_DET ddau_NUM yn_ADP y_DET '_PUNCT Giant_NOUN Slalom_PROPN '_PUNCT a_CONJ 'r_DET ras_NOUN Slalom_PROPN yn_PART erbyn_ADP gwrthwynebwyr_NOUN rhyngwladol_ADJ ,_PUNCT a_CONJ ddeuai_VERB cyn_ADP belled_ADJ ag_ADP Awstralia_PROPN ._PUNCT
Daeth_VERB enwg_VERB yn_PART ail_ADJ o_ADP fechgyn_NOUN Dan_PROPN 16_NUM Cymru_PROPN yn_ADP y_DET Slalom_PROPN ._PUNCT
Daeth_VERB hefyd_ADV yn_PART drydydd_ADJ o_ADP fechgyn_NOUN dan_ADP 16_NUM Cymru_PROPN yn_ADP y_DET Giant_NOUN Slalom_PROPN ._PUNCT
Chweched_VERB oedd_VERB enwb1_SYM o_ADP Ferched_PROPN dan_ADP 16_NUM Cymru_PROPN yn_ADP y_DET Slalom_PROPN yn_ADP Champery_PROPN gan_ADP orffen_VERB yn_PART bumed_ADJ o_ADP 'r_DET merched_NOUN dan_ADP 16_NUM o_ADP Gymru_PROPN yn_ADP y_DET Giant_NOUN Slalom_PROPN ._PUNCT
enwg_VERB a_CONJ enwb1_NUM __NOUN bellach_ADV yw_VERB Capteiniaid_NOUN timau_NOUN sgio_VERB bechgyn_NOUN a_CONJ merched_NOUN __NOUN __NOUN ._PUNCT
Mae_VERB tim_NOUN y_DET bechgyn_NOUN wedi_PART cymhwyso_VERB sgio_VERB ym_ADP Mhencampwriaethau_NOUN Sgio_PROPN Prydain_PROPN y_DET ddwy_NUM flynedd_NOUN ddiwethaf_ADJ gyda_ADP 'r_DET merched_NOUN dros_ADP y_DET tair_NUM blynedd_NOUN ddiwethaf_ADJ ._PUNCT
Dim_PRON ond_ADP y_DET 25_NUM ysgol_NOUN orau_ADV gaiff_VERB gystadlu_VERB yn_ADP y_DET gystadleuaeth_NOUN hon_DET !_PUNCT
Mae_AUX tim_NOUN sgio_ADV lleoliad_NOUN yn_PART edrych_VERB am_ADP sgi_NOUN ̈_VERB wyr_NOUN newydd_ADJ i_PART ymuno_VERB a_CONJ ̂_VERB nhw_PRON ,_PUNCT yn_PART arbennig_ADJ bechgyn_NOUN ._PUNCT
Os_CONJ oes_VERB gennych_ADP ddiddordeb_NOUN ,_PUNCT cysylltwch_VERB drwy_ADP ebost_NOUN :_PUNCT cyfeiriad_NOUN -_PUNCT bost_NOUN ._PUNCT
Y_PART mae_VERB croeso_NOUN i_ADP ddehreuwyr_NOUN ._PUNCT
