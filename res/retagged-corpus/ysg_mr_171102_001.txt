PAPUR_NOUN WYTHNOSOL_X YR_X ANNIBYNWYR_NOUN CYMRAEG_PROPN
Y_DET TYST_PROPN
Sefydlwyd_VERB 1867_NUM Cyfrol_ADJ 150_NUM Rhif_PROPN 44_NUM Tachwedd_NOUN 2_NUM ,_PUNCT 2017_NUM 50_NUM c_PRON ._PUNCT
Dim_DET Eglwys_NOUN Sefydliedig_ADJ
Tangnefedd_PROPN
Ar_ADP fore_NOUN Sul_PROPN 24_NUM Medi_PROPN cynhaliwyd_VERB gwasanaeth_NOUN arbennig_ADJ yng_ADP Nghapel_X Hope_X -_PUNCT Siloh_VERB i_ADP ddathlu_VERB Diwrnod_NOUN Heddwch_PROPN Rhyngwladol_ADJ ._PUNCT
Y_DET gwragedd_NOUN a_CONJ 'r_DET gwy_NOUN ̂_ADJ r_NOUN ifanc_ADJ oedd_VERB yng_ADP ngofal_NOUN y_DET gwasanaeth_NOUN a_CONJ chafwyd_VERB cyflwyniadau_NOUN o_ADP gerddi_NOUN a_CONJ chaneuon_VERB i_ADP gofio_VERB am_ADP Hedd_PROPN Wyn_PROPN ac_CONJ eraill_PRON a_PART gollodd_VERB eu_DET bywydau_NOUN yn_ADP y_DET Rhyfel_NOUN Byd_NOUN Cyntaf_PROPN ._PUNCT
Diolch_NOUN i_ADP Gwen_PROPN Jones_PROPN am_ADP greu_VERB 'r_DET cyflwyniad_NOUN ,_PUNCT i_ADP Eric_PROPN Jones_PROPN am_ADP ei_DET gyfraniad_NOUN cerddorol_ADJ ac_CONJ i_ADP 'r_DET aelodau_NOUN a_CONJ gymerodd_VERB ran_NOUN ._PUNCT
Cafwyd_VERB gwasanaeth_NOUN safonol_ADJ a_CONJ bendithiol_ADJ iawn_ADV ._PUNCT
Jennifer_VERB Clark_PROPN
Diolch_VERB yn_ADP yr_DET Hendy_PROPN
Cynhaliwyd_VERB Cwrdd_PROPN Diolchgarwch_VERB y_DET plant_NOUN a_CONJ 'r_DET ieuenctid_NOUN yn_ADP y_DET Tabernacl_PROPN ,_PUNCT Hendy_PROPN -_PUNCT gwyn_ADJ ar_ADP ddechrau_NOUN Hydref_ADJ ._PUNCT
Bwyd_PROPN oedd_VERB y_DET thema_NOUN a_CONJ chanolbwyntiwyd_VERB yn_PART arbennig_ADJ ar_ADP sut_ADV mae_VERB bara_NOUN a_CONJ bananas_NOUN yn_PART cyrraedd_VERB ein_DET cartrefi_VERB ._PUNCT
Casglwyd_VERB bocsys_NOUN ar_ADP gyfer_NOUN Operation_NOUN Christmas_PROPN Child_PROPN a_CONJ galwodd_VERB Tecwyn_PROPN y_DET mascot_NOUN heibio_ADV yn_PART ystod_NOUN y_DET te_NOUN ar_ADP o_PRON ̂_SYM l_ADP y_DET gwasanaeth_NOUN ._PUNCT
Gellir_VERB gweld_VERB fideo_NOUN o_ADP 'r_DET plant_NOUN lleiaf_ADJ yn_PART canu_VERB ar_ADP dudalen_NOUN Facebook_PROPN y_DET capel_NOUN -_PUNCT Tabernacl_X Hendy_X -_PUNCT gwyn_ADJ ._PUNCT
Canu_VERB Mawl_NOUN Gwefreiddiol_ADJ
CYMANFA_VERB GANU_NUM CYFUNDEB_NOUN LLUNDAIN_PROPN
O_ADP dan_ADP arweiniad_NOUN hwylus_ADJ a_CONJ medrus_ADJ ein_DET Harweinydd_PROPN -_PUNCT Mary_PROPN Lloyd_PROPN Davies_PROPN o_ADP Lanuwchllyn_PROPN ,_PUNCT cafwyd_VERB canu_VERB bendigedig_ADJ ,_PUNCT gyda_ADP Mary_PROPN 'n_ADP ein_DET hannog_VERB i_PART ystyried_VERB geiriau_NOUN 'r_DET emynau_NOUN gan_CONJ wneud_VERB yn_PART siw_NOUN ̂_ADJ r_PRON os_CONJ nad_ADV oedd_VERB coma_NOUN ar_ADP ddiwedd_NOUN llinell_NOUN y_PART dylem_VERB ddal_VERB ymlaen_ADV i_ADP 'r_DET llinell_NOUN nesaf_ADJ ._PUNCT
Tipyn_NOUN o_ADP gamp_NOUN i_ADP ambell_ADJ un_NUM ohonom_ADP ond_CONJ llwyddo_VERB a_PART wnaethom_VERB o_ADP dan_ADP arweiniad_NOUN y_DET baton_NOUN !_PUNCT
Eitemau_NOUN arbennig_ADJ
Eleni_ADV cawsom_VERB gyfraniadau_NOUN cerddorol_ADJ newydd_ADJ i_ADP 'r_DET Gymanfa_NOUN sef_NOUN ,_PUNCT deuawd_NOUN gan_ADP Robert_PROPN a_CONJ James_PROPN ,_PUNCT sef_CONJ ,_PUNCT dau_NUM Gymro_PROPN sy_AUX 'n_PART astudio_VERB yn_ADP Llundain_PROPN ._PUNCT
Eu_DET dewis_VERB oedd_VERB '_PUNCT Y_DET Ddau_NUM Wladgarwr_NOUN '_PUNCT -_PUNCT a_CONJ 'r_DET ddau_NUM lais_NOUN yn_PART asio_VERB 'n_PART hyfryd_ADJ ac_CONJ roedd_VERB y_DET ga_NOUN ̂_SYM n_PRON yn_PART ein_PRON hatgoffa_VERB o_ADP eiriau_NOUN T_NUM ._PUNCT H_INTJ ._PUNCT Parry_PROPN -_PUNCT Williams_PROPN ,_PUNCT '_PUNCT Duw_NOUN a_CONJ 'm_DET gwaredo_VERB ,_PUNCT ni_PART allaf_ADV ddianc_VERB rhag_ADP hon_PRON '_PUNCT ._PUNCT
Yn_PART dilyn_VERB y_DET bechgyn_NOUN cawsom_VERB eitem_NOUN gan_ADP Go_ADV ̂_ADJ r_ADP y_DET Boro_PROPN ,_PUNCT dan_ADP arweiniad_NOUN Dr_NOUN Haydn_X James_PROPN ,_PUNCT tra_CONJ roedd_VERB eu_DET harweinydd_NOUN arferol_ADJ ,_PUNCT Mike_PROPN Williams_PROPN ,_PUNCT yn_PART canu_VERB yn_ADP y_DET co_NOUN ̂_PRON r_NOUN ._PUNCT
Dyma_DET 'r_DET co_NOUN ̂_SYM r_NOUN a_PART wnaeth_VERB argraff_NOUN fawr_ADJ yn_ADP Eisteddfod_NOUN Genedlaethol_ADJ Bodedern_PROPN ,_PUNCT Mo_PART ̂_ADJ n_ADJ eleni_ADV ac_CONJ sy_VERB 'n_PART barod_ADJ iawn_ADV i_PART ddod_VERB i_ADP wasanaethau_VERB Cymry_PROPN Llundain_PROPN ._PUNCT
Roedd_VERB presenoldeb_NOUN y_DET co_NOUN ̂_SYM r_AUX yn_PART hybu_VERB 'r_DET harmoni_NOUN ̈_VERB au_ADP yn_ADP yr_DET emynau_NOUN ac_CONJ yn_PART gaffaeliad_NOUN mawr_ADJ i_ADP 'r_DET canu_NOUN a_CONJ oedd_VERB o_ADP dan_ADP arweiniad_NOUN Mary_PROPN yn_PART dod_VERB ac_CONJ ysbryd_NOUN geiriau_NOUN a_CONJ thestun_NOUN yr_DET emynau_NOUN yn_PART fyw_VERB i_ADP ni_PRON gyd_ADP ._PUNCT
Llywyddu_VERB a_CONJ llongyfarch_NOUN
Mr_NOUN Rob_PROPN Nicholls_X ,_PUNCT Eglwys_NOUN Gymraeg_PROPN Canol_ADJ Llundain_PROPN ,_PUNCT Cadeirydd_NOUN y_DET Cyfundeb_NOUN oedd_AUX yn_PART llywyddu_VERB 'r_DET noson_NOUN a_CONJ darllenwyd_VERB Salm_PROPN gan_ADP Mr_NOUN Aldwyn_PROPN Jones_PROPN ,_PUNCT Harrow_PROPN ._PUNCT
Mr_NOUN John_PROPN Jones_PROPN y_DET Boro_NOUN oedd_VERB wrth_ADP yr_DET organ_NOUN ac_CONJ efe_PRON gyfeiliodd_VERB hefyd_ADV i_ADP 'r_DET co_NOUN ̂_PRON r_NOUN ._PUNCT
Diolchodd_VERB ef_PRON i_ADP bawb_PRON a_PART gymerodd_VERB ran_NOUN mewn_ADP noson_NOUN oedd_AUX yn_PART ein_PRON hatgoffa_VERB o_ADP 'n_PART cefndir_NOUN Cymreig_ADJ a_CONJ 'n_PART magwraeth_NOUN yn_PART ein_PRON heglwysi_VERB ,_PUNCT ble_ADV yr_PART oedd_VERB ac_CONJ y_PART mae_VERB addoli_VERB drwy_ADP ganu_VERB emynau_NOUN yn_PART datgan_VERB mawredd_NOUN ein_DET Harglwydd_NOUN ._PUNCT
Cafodd_VERB Mr_NOUN Rob_PROPN Nicholls_X gyfle_NOUN hefyd_ADV i_ADP gyhoeddi_VERB a_CONJ llongyfarch_NOUN y_DET Parchg_PROPN Gwylfa_NOUN Evans_PROPN sydd_AUX yn_PART dathlu_VERB 39_NUM mlynedd_NOUN fel_ADP Gweinidog_NOUN yn_ADP Harrow_NOUN ac_CONJ er_ADP wedi_PART ymddeol_VERB yn_PART swyddogol_ADJ yn_PART dal_ADV i_ADP fod_VERB yn_PART weithgar_ADJ yn_ADP ein_DET plith_NOUN ._PUNCT
Diolchwyd_VERB hefyd_ADV i_ADP aelodau_NOUN Harrow_PROPN am_ADP y_DET lluniaeth_NOUN oedd_AUX wedi_PART ei_DET baratoi_VERB i_ADP ni_PRON ar_ADP o_PRON ̂_SYM l_ADP y_DET Gymanfa_PROPN -_PUNCT a_CONJ chyfle_NOUN unwaith_ADV eto_ADV i_ADP gymdeithasu_VERB fel_ADP Cymry_PROPN sydd_VERB ar_ADP wasgar_NOUN yn_ADP y_DET brifddinas_NOUN ._PUNCT
Noson_VERB i_ADP 'w_PRON chofio_VERB ._PUNCT
Ar_ADP nos_NOUN Sul_PROPN olaf_ADJ mis_NOUN Medi_PROPN cynhaliwyd_VERB Cymanfa_PROPN Ganu_PROPN flynyddol_ADJ Cyfundeb_NOUN Llundain_PROPN ._PUNCT
Cystal_ADV oedd_VERB y_DET wefr_NOUN a_CONJ 'r_DET ysbrydoliaeth_NOUN yn_ADP y_DET canu_VERB yng_ADP Nghapel_NOUN Harrow_NOUN nos_NOUN Sul_PROPN ,_PUNCT 24_NUM Medi_PROPN fel_CONJ y_PART byddai_VERB cymanfa_NOUN fisol_ADJ yn_PART fodd_NOUN i_ADP fyw_VERB i_ADP Gymry_PROPN Llundain_PROPN !_PUNCT
Mair_NOUN Jones_PROPN
Diffyg_NOUN Cofio_PROPN
Y_PART mae_AUX rhai_DET pobl_NOUN yn_PART ddiweddar_ADJ wedi_PART mynegi_VERB siom_NOUN ,_PUNCT at_ADP y_DET diffyg_NOUN sylw_NOUN y_PART mae_VERB llawer_NOUN o_ADP gewri_NOUN hanesyddol_ADJ ein_DET cenedl_NOUN yn_PART ei_PRON gael_VERB gan_ADP y_DET Cynulliad_NOUN ._PUNCT
Tila_ADJ iawn_ADV oedd_VERB unrhyw_DET ymdrech_NOUN swyddogol_ADJ i_PART gofio_VERB am_ADP William_PROPN Salesbury_X a_CONJ William_X Williams_PROPN ,_PUNCT Pantycelyn_PROPN ._PUNCT
Wrth_PART ymateb_VERB i_ADP 'r_DET mater_NOUN hwn_DET yn_ADP y_DET Cynulliad_NOUN ,_PUNCT (_PUNCT meddai_VERB Golwg_PROPN 360_NUM )_PUNCT ,_PUNCT fe_PART ddywedodd_VERB Ysgrifennydd_NOUN yr_DET Economi_PROPN ,_PUNCT Ken_X Skates_PROPN ,_PUNCT ei_PRON fod_VERB yn_PART siomedig_ADJ iawn_ADV nad_PART oedd_VERB yr_DET un_NUM corff_NOUN arall_ADJ wedi_PART dod_VERB at_ADP y_DET Llywodraeth_NOUN i_PART ofyn_VERB am_ADP gefnogaeth_NOUN i_ADP ddigwyddiadau_NOUN i_ADP ddathlu_VERB Pantycelyn_PROPN ._PUNCT
Roedd_AUX hynny_PRON wedi_PART digwydd_VERB yn_PART achos_NOUN yr_DET awduron_NOUN Saesneg_PROPN eu_DET hiaith_NOUN ,_PUNCT Dylan_PROPN Thomas_PROPN a_CONJ Roald_PROPN Dahl_PROPN ,_PUNCT ac_CONJ roedd_AUX yr_DET Ysgrifennydd_NOUN wedi_PART dweud_VERB ddechrau_VERB 'r_DET flwyddyn_NOUN y_PART bydden_AUX nhw_PRON 'n_PART croesawu_VERB trafodaeth_NOUN ._PUNCT
'_PUNCT Piti_NOUN na_PART fyddai_AUX 'r_DET Cynulliad_NOUN wedi_PART bod_VERB yn_PART fwy_ADV rhagweithiol_ADJ meddai_VERB Dr_PROPN E_NUM ._PUNCT Wyn_PROPN James_PROPN ,_PUNCT arbenigwr_NOUN ar_ADP emynyddiaeth_NOUN Cymru_PROPN ._PUNCT
Ychwanegodd_VERB ,_PUNCT '_PUNCT Ond_CONJ mae_AUX hyn_PRON hefyd_ADV yn_PART awgrymu_VERB nad_PART oedd_VERB yr_DET un_NUM o_ADP asiantaethu_VERB 'r_DET Llywodraeth_NOUN wedi_PART gofyn_VERB am_ADP ddim_PRON ._PUNCT
Mae_AUX yea_ADP elfen_NOUN yn_ADP y_DET Gymru_PROPN amlddiwylliannol_ADJ yma_DET sydd_VERB braidd_ADV yn_PART swil_ADJ o_ADP roi_VERB sylw_NOUN i_ADP Gristnogaeth_PROPN -_PUNCT rhai_PRON yn_PART elyniaethyus_NOUN a_CONJ rhai_PRON 'n_PART anywybodus_ADJ ._PUNCT
Rhwng_ADP hynny_PRON ,_PUNCT a_CONJ 'r_DET ffaith_NOUN fod_AUX hyn_PRON yn_PART ymwneud_VERB a_CONJ ̂_SYM llenyddiaeth_NOUN Gymraeg_PROPN ,_PUNCT mae_AUX peryg_ADJ fod_AUX Pantycelyn_PROPN wedi_PART diodde_VERB o_ADP double_VERB whammy_NOUN ._PUNCT '_PUNCT
Testun_PROPN llawenydd_NOUN felly_CONJ oedd_VERB clywed_VERB bod_AUX y_DET Llyfrgell_NOUN Genedlaethol_ADJ wedi_PART trefnu_VERB digwyddiad_NOUN yn_ADP y_DET Cynulliad_NOUN i_ADP nodi_VERB trichanmlwyddiant_NOUN geni_VERB William_PROPN Williams_PROPN ar_ADP 18_NUM Hydref_X ._PUNCT
Gwell_ADJ hwyr_ADJ na_CONJ hwyrach_ADP ._PUNCT
Bydd_VERB rhaid_VERB cofio_VERB hysbysu_VERB 'r_DET Cynulliad_NOUN pan_CONJ fydd_VERB dyddiad_NOUN neu_CONJ ddathliad_NOUN hanesyddol_ADJ arall_ADJ o_ADP bwys_NOUN ar_ADP y_DET gorwel_NOUN ._PUNCT
Undeb_NOUN yr_DET Annibynwyr_NOUN Cymraeg_PROPN
Diwrnod_NOUN Gweinidogion_PROPN y_DET Gogledd_NOUN
Festri_NOUN Capel_PROPN Coffa_NOUN ,_PUNCT Cyffordd_NOUN Llandudno_PROPN LL_NUM 31_NUM 9_NUM HD_X
Dydd_NOUN Iau_ADV ,_PUNCT 30_NUM Tachwedd_NOUN 2017_NUM
Byddwn_AUX yn_PART dechrau_VERB am_ADP 10.30_NUM am_ADP gyda_ADP phaned_NOUN a_CONJ chroeso_ADJ
'_PUNCT Diffinio_PROPN ac_CONJ Ailddiffinio_X '_PUNCT
Mrs_NOUN Delyth_PROPN Wyn_PROPN Davies_NOUN Swyddog_PROPN Dysgu_PROPN a_CONJ Datblygu_VERB Cymru_PROPN ,_PUNCT yr_DET Eglwys_NOUN Fethodistaidd_ADJ
'_PUNCT Y_DET Ffordd_NOUN y_DET Daethom_NOUN :_PUNCT Oes_VERB y_DET Saint_NOUN '_PUNCT
Y_DET Parchedig_ADJ J_NUM ._PUNCT Lloyd_PROPN Jones_PROPN Ficer_NOUN Plwyf_PROPN Beuno_PROPN Sant_NOUN Uwchgwyrfai_PROPN
Bydd_AUX y_DET cyfarfod_NOUN yn_PART gorffen_VERB am_ADP 1.00_NUM pm_PRON ._PUNCT
Os_CONJ am_ADP gael_VERB cinio_NOUN ,_PUNCT cysyllter_VERB a_CONJ ̂_PUNCT Thy_PROPN ̂_PROPN John_PROPN Penri_PROPN ar_ADP [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT neu_CONJ trwy_ADP e_PRON -_PUNCT bost_NOUN at_ADP [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT
Edrychwn_VERB ymlaen_ADV at_ADP groesawu_VERB 'r_DET Gweinidogion_NOUN i_ADP Gyffordd_NOUN Llandudno_PROPN
Undeb_NOUN yr_DET Annibynwyr_NOUN Cymraeg_PROPN
Diwrnod_NOUN i_ADP Weinidogion_PROPN y_DET De_NOUN
Festri_NOUN Capel_PROPN Bethania_PROPN [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT
Dydd_NOUN Iau_ADV ,_PUNCT 9_NUM Tachwedd_NOUN 2017_NUM
11.00_NUM am_ADP -_PUNCT 3.30_NUM pm_PRON
'_PUNCT Paratoi_NOUN cyflwyniadau_NOUN ar_ADP
gyfer_NOUN gwasanaethau_NOUN '_PUNCT
Rhodri_PROPN Darcy_PROPN Swyddog_NOUN Hyrwyddo_PROPN a_CONJ Chyfathrebu_VERB 'r_DET Undeb_NOUN '_PUNCT Pam_ADV cefnogi_VERB Madagascar_PROPN ?_PUNCT '_PUNCT
Robin_NOUN Samuel_NOUN Swyddog_NOUN Cynnal_PROPN ac_CONJ Adnoddau_NOUN De_NOUN Cymru_PROPN
Darperir_VERB cinio_NOUN am_ADP ddim_PRON
Edrychwn_VERB ymlaen_ADV at_ADP groesawu_VERB 'r_DET gweinidogion_NOUN i_ADP 'r_DET Tymbl_NOUN Uchaf_PROPN
Anfonwch_VERB e_PRON -_PUNCT bost_NOUN i_PRON [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT neu_CONJ ffoniwch_VERB Dy_DET ̂_NOUN John_PROPN Penri_PROPN [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT erbyn_ADP 3_NUM Tachwedd_NOUN i_ADP sicrhau_VERB eich_DET cinio_NOUN ._PUNCT
Barn_NOUN Annibynnol_ADJ
Y_DET CYMRO_NOUN MWYAF_PROPN ERIOED_PROPN ?_PUNCT
Braf_ADJ oedd_ADV cael_VERB ymweld_VERB a_CONJ ̂_ADJ Llansannan_VERB fis_NOUN yn_PART o_ADJ ̂_SYM l_PRON ._PUNCT
Mae_VERB mynd_VERB am_ADP dro_NOUN i_ADP fro_VERB fy_DET mebyd_NOUN wastad_ADV yn_PART braf_ADJ wrth_ADP reswm_NOUN ,_PUNCT ond_CONJ roedd_VERB hwn_PRON yn_PART ymweliad_NOUN arbennig_ADJ ._PUNCT
Dyma_DET lle_NOUN y_PART cyfarfu_VERB Cymdeithas_NOUN Hanes_PROPN yr_DET Annibynwyr_NOUN i_PART wrando_VERB ar_ADP Dr_NOUN Gwynn_NOUN Mathews_PROPN yn_PART darlithio_VERB ar_ADP William_PROPN Salesbury_PROPN ._PUNCT
Ar_ADP ein_DET ffordd_NOUN cawsom_VERB gyfle_NOUN i_PART alw_VERB ym_ADP Myd_NOUN Mari_PROPN Jones_PROPN a_CONJ mwynhau_VERB arddangosfa_NOUN yno_ADV ,_PUNCT gan_CONJ gynnwys_VERB fideos_NOUN ar_ADP Salesbury_X ,_PUNCT ac_CONJ yn_PART arbennig_ADJ cawsom_VERB weld_VERB copi_NOUN gwreiddiol_ADJ o_ADP Destament_PROPN 1567_NUM ._PUNCT
Roedd_VERB y_DET ddarlith_NOUN yn_PART ardderchog_ADJ ,_PUNCT yn_PART fanwl_ADJ ond_CONJ heb_ADP fod_VERB yn_PART drwm_ADJ ,_PUNCT yn_PART llawn_ADJ goleuni_NOUN ac_CONJ ysgolheictod_NOUN ._PUNCT
Cawsom_VERB ein_PRON hysbrydoli_VERB wrth_ADP glywed_VERB am_ADP gampau_NOUN Salesbury_PROPN ,_PUNCT a_CONJ rhyfeddu_VERB at_ADP ei_PRON orchestion_NOUN ._PUNCT
Mae_VERB 'n_PART fwriad_NOUN iddi_ADP ymddangos_VERB yn_ADP Y_DET Cofiadur_PROPN cyn_ADP bo_VERB hir_ADJ ._PUNCT
Gwnewch_VERB yn_PART siw_NOUN ̂_ADJ r_PRON o_ADP 'ch_PRON copi_NOUN ._PUNCT
Yn_ADP y_DET cyfamser_NOUN mae_VERB 'n_PART werth_ADJ darllen_VERB cofiant_VERB James_PROPN Pierce_PROPN ;_PUNCT The_X Life_X and_X Work_X of_X William_X Salesbury_X -_PUNCT A_X Rare_X Scholar_PROPN ,_PUNCT a_CONJ gyhoeddir_VERB gan_ADP Y_DET Lolfa_NOUN ._PUNCT
O_ADP waelod_NOUN buarth_NOUN Llys_NOUN Aled_ADJ ,_PUNCT lle_ADV ces_VERB fy_DET magu_VERB ,_PUNCT gallwn_VERB weld_VERB Cae_NOUN Du_ADJ ar_ADP y_DET llechwedd_NOUN ar_ADP ochr_NOUN draw_ADV Dyffryn_NOUN Aled_PROPN ;_PUNCT yno_ADV y_PART cafodd_VERB Salesbury_PROPN ei_DET eni_VERB ._PUNCT
Does_VERB ryfedd_NOUN iddo_ADP fod_VERB yn_PART arwr_NOUN i_ADP mi_PRON ers_ADP pan_CONJ gofiaf_VERB ._PUNCT
Ond_CONJ o_ADP wrando_VERB ar_ADP y_DET ddarlith_NOUN ,_PUNCT ac_CONJ wedi_ADP i_ADP mi_PRON ddarllen_VERB y_DET llyfr_NOUN ,_PUNCT tyfodd_VERB yr_DET arwr_NOUN yn_PART gawr_ADJ ._PUNCT
Fu_ADP ei_DET fywyd_NOUN personol_ADJ ddim_PART yn_PART hawdd_ADJ ,_PUNCT gydag_ADP aelodau_NOUN o_ADP 'i_PRON deulu_NOUN yn_PART cyfreitha_VERB yn_ADP ei_DET erbyn_ADP ._PUNCT
Cuddir_VERB llawer_PRON o_ADP 'i_PRON fywyd_NOUN mewn_ADP niwl_NOUN ;_PUNCT byddai_VERB ysgolheigion_VERB yn_PART falch_ADJ o_ADP wybod_VERB llawer_ADV mwy_ADJ am_ADP droeon_NOUN ei_DET yrfa_NOUN ._PUNCT
Er_ADP hynny_PRON ,_PUNCT mae_AUX digonedd_NOUN o_ADP ffeithiau_NOUN ar_ADP gael_VERB i_ADP fedru_ADV creu_VERB portread_NOUN ohono_ADP ._PUNCT
Dyma_DET 'r_DET gw_NOUN ̂_ADJ r_NOUN a_PART aeth_VERB ati_ADP gyntaf_NUM o_ADP ddifri_NOUN i_ADP gyfieithu_VERB 'r_DET ysgrythurau_NOUN i_ADP Gymraeg_PROPN ._PUNCT
Ymddangosodd_VERB ei_DET waith_NOUN ,_PUNCT Kynniver_PROPN Llith_PROPN a_CONJ Ban_PROPN ,_PUNCT mor_ADV gynnar_ADJ a_CONJ ̂_SYM 1551_NUM ._PUNCT
Roedd_VERB mewn_ADP iaith_NOUN y_PART gallai_VERB 'r_DET cynulleidfaoedd_NOUN ei_PRON deall_VERB ._PUNCT
Ganrif_NOUN ynghynt_VERB y_PART dyfeisiwyd_VERB y_DET wasg_NOUN argraffu_VERB ,_PUNCT cam_NOUN aruthrol_ADJ ymlaen_ADV mewn_ADP cyfathrebu_VERB ._PUNCT
Cymerodd_VERB Salesbury_PROPN fantais_NOUN ohoni_ADP ac_CONJ argraffu_VERB nifer_NOUN dda_ADJ o_ADP lyfrau_NOUN ._PUNCT
Defnyddiodd_VERB y_DET dechnoleg_NOUN ddiweddaraf_ADJ ,_PUNCT a_CONJ gwneud_VERB y_DET Gymraeg_PROPN yn_PART iaith_NOUN mewn_ADP print_NOUN yn_PART gynnar_ADJ ._PUNCT
Roedd_AUX yn_PART byw_VERB mewn_ADP cyfnod_NOUN hynod_ADV beryglus_ADJ i_PART fod_VERB yn_PART flaenllaw_ADJ ym_ADP myd_NOUN crefydd_NOUN ._PUNCT
Gorfu_VERB i_ADP amryw_PRON ffoi_VERB i_ADP 'r_DET cyfandir_NOUN ,_PUNCT Genefa_VERB yn_PART arbennig_ADJ ._PUNCT
Cai_VERB brwdfrydwyr_NOUN Cristnogol_ADJ ,_PUNCT yn_PART Brotestaniaid_PROPN a_CONJ Chatholigion_NOUN ,_PUNCT eu_DET herlid_VERB ,_PUNCT eu_DET crogi_VERB a_CONJ 'u_PRON llosgi_VERB wrth_ADP y_DET stanc_NOUN ._PUNCT
Bu_AUX 'n_PART rhaid_VERB i_ADP Salesbury_PROPN aros_VERB o_ADP 'r_DET golwg_NOUN yn_ADP ystod_NOUN teyrnasiad_NOUN Mari_PROPN ._PUNCT
Roedd_VERB angen_NOUN caniata_NOUN ̂_ADJ d_PRON i_PART gyhoeddi_VERB a_CONJ rhaid_VERB mynd_VERB i_ADP 'r_DET uchelfannau_NOUN ,_PUNCT at_ADP y_DET Cyfrin_NOUN Gyngor_NOUN neu_CONJ 'r_DET Brenin_NOUN ._PUNCT
Mae_VERB posibilrwydd_NOUN cryf_ADJ y_DET mentrodd_VERB Salesbury_PROPN gysylltu_VERB a_CONJ ̂_ADJ Harri_PROPN 'r_DET V_NUM 111_NUM ._PUNCT
Dim_PRON ond_ADP y_DET dewr_ADJ allai_VERB wneud_VERB hynny_PRON ,_PUNCT ac_CONJ yntau_PRON mor_ADV aml_ADJ mewn_ADP hwyliau_NOUN drwg_ADJ o_ADP achos_ADP ei_DET boenau_NOUN corfforol_ADJ a_CONJ 'i_PRON broblemau_NOUN eraill_ADJ ._PUNCT
Ond_CONJ cafodd_VERB Salesbury_PROPN ei_DET fendith_NOUN ._PUNCT
Ef_INTJ ,_PUNCT mae_VERB 'n_PART debyg_ADJ ,_PUNCT oedd_VERB yn_PART bennaf_ADJ gyfrifol_ADJ am_ADP ddeddf_NOUN 1563_NUM oedd_VERB yn_PART gorchymyn_NOUN yr_DET esgobion_NOUN i_PART ddarparu_VERB cyfieithiad_NOUN Cymraeg_PROPN o_ADP 'r_DET Beibl_NOUN a_CONJ 'i_PRON osod_VERB ym_ADP mhob_DET eglwys_NOUN erbyn_ADP Gw_PROPN ̂_SYM yl_NOUN Ddewi_PROPN 1567_NUM ._PUNCT
Bwrw_VERB ymlaen_ADV a_CONJ ̂_AUX 'r_DET gwaith_NOUN ei_DET hun_PRON a_PART wnaeth_VERB ,_PUNCT a_CONJ chael_VERB peth_DET cymorth_NOUN gan_ADP yr_DET Esgob_NOUN Richard_PROPN Davies_PROPN a_CONJ Thomas_PROPN Huet_X ._PUNCT
Erbyn_ADP gwanwyn_NOUN 1567_NUM roedd_VERB y_DET Llyfr_NOUN Gweddi_PROPN Gyffredin_PROPN wedi_PART 'i_PRON argraffu_VERB ._PUNCT
Yn_ADP y_DET cyfnod_NOUN hwnnw_PRON ,_PUNCT gellid_VERB dadlau_NOUN ,_PUNCT bod_VERB y_DET Llyfr_NOUN Gweddi_VERB 'n_PART bwysicach_ADJ na_CONJ 'r_DET Testament_PROPN ._PUNCT
Gallai_VERB 'r_DET Cymry_PROPN briodi_VERB ,_PUNCT bedyddio_VERB ,_PUNCT claddu_VERB yn_ADP Gymraeg_PROPN am_ADP y_DET tro_NOUN cyntaf_ADJ ._PUNCT
Wedyn_ADV ym_ADP mis_NOUN Hydref_PROPN roedd_VERB y_DET Testament_NOUN Newydd_ADJ Cymraeg_PROPN yn_PART eglwysi_VERB 'r_DET plwyf_NOUN ._PUNCT
Fe_PART 'i_PRON cyfrifir_VERB yn_PART waith_NOUN gorchestol_ADJ ,_PUNCT yn_PART adlewyrchu_VERB 'r_DET gwreiddiol_NOUN yn_PART agos_ADJ iawn_ADV ,_PUNCT a_CONJ 'i_PRON iaith_NOUN yn_PART urddasol_ADJ ac_CONJ yn_PART llifo_VERB ._PUNCT
Yr_DET unig_ADJ ddiffyg_NOUN yn_ADP y_DET gwaith_NOUN yw_VERB 'r_DET sillafu_NOUN ._PUNCT
Nid_PART oedd_AUX Salesbury_PROPN yn_PART disgwyl_VERB i_ADP bob_DET llythyren_NOUN gael_VERB ei_PRON hynganu_VERB ,_PUNCT mwy_ADJ nag_CONJ y_PART mae_AUX Saeson_PROPN yn_PART disgwyl_VERB clywed_VERB y_DET '_PUNCT b_PRON '_PUNCT yn_PART doubt_ADJ neu_CONJ 'r_DET '_PUNCT k_PRON '_PUNCT yn_PART know_VERB ._PUNCT
Ei_DET fwriad_NOUN oedd_VERB dangos_VERB bod_VERB llu_NOUN o_ADP eiriau_NOUN Cymraeg_PROPN yn_PART tarddu_VERB o_ADP 'r_DET Lladin_PROPN ._PUNCT
Lladin_PROPN yw_VERB iaith_NOUN glasurol_ADJ dysg_NOUN ac_CONJ mae_AUX 'r_DET Gymraeg_PROPN yn_PART perthyn_VERB iddi_ADP ._PUNCT
Nid_PART iaith_NOUN llwyn_NOUN a_CONJ pherth_ADJ mohoni_PART ._PUNCT
Mae_VERB 'n_PART iaith_NOUN Ewropeaidd_PROPN sy_AUX 'n_PART haeddu_VERB parch_NOUN ._PUNCT
Bernir_VERB mai_PART cael_VERB yr_DET ysgrythurau_NOUN yn_ADP yr_DET iaith_NOUN frodorol_ADJ erbyn_ADP tua_ADV 1600_NUM oedd_VERB un_NUM o_ADP 'r_DET ffactorau_NOUN pwysicaf_ADJ i_ADP 'w_PRON chadw_VERB 'n_PART fyw_VERB ._PUNCT
Prin_ADJ y_PART byddai_AUX hynny_PRON wedi_PART digwydd_VERB yn_ADP ein_DET hanes_NOUN ni_PRON oni_CONJ bai_VERB am_ADP William_PROPN Salesbury_PROPN ._PUNCT
Ni_PART chafodd_VERB ei_DET werthfawrogi_VERB fel_CONJ y_DET dylai_VERB ._PUNCT
Ychydig_ADJ fu_VERB 'n_PART gefn_NOUN iddo_ADP ._PUNCT
Cael_VERB ei_PRON wawdio_VERB fu_VERB ei_DET hanes_NOUN mor_ADV aml_ADJ ._PUNCT
Tybed_ADV a_PART ydy_AUX 'r_DET llanw_NOUN yn_PART troi_VERB o_ADP 'r_DET diwedd_NOUN ?_PUNCT
Mae_AUX James_PROPN Pierce_PROPN yn_PART ei_DET gofiant_NOUN ardderchog_ADJ yn_PART cyfeirio_VERB at_ADP ddynion_NOUN eraill_ADJ y_PART mae_VERB ar_ADP Gymru_PROPN ddyled_NOUN fawr_ADJ iddynt_ADP yn_ADP y_DET cyfnod_NOUN 1550_NUM -_PUNCT 1600_NUM :_PUNCT John_PROPN Prys_PROPN ,_PUNCT Gruffydd_NOUN Robert_PROPN ,_PUNCT William_PROPN Salesbury_PROPN ,_PUNCT Richard_PROPN Davies_PROPN ,_PUNCT William_PROPN Morgan_PROPN ,_PUNCT ond_CONJ ,_PUNCT '_PUNCT mae_VERB 'r_DET ddyled_NOUN fwyaf_ADJ ,_PUNCT o_ADP ddigon_ADJ ,_PUNCT i_ADP William_PROPN Salesbury_X ._PUNCT '_PUNCT Fe_NOUN o_ADP ddigon_ADJ ,_PUNCT nid_PART o_ADP ychydig_ADV ,_PUNCT neu_CONJ efallai_ADV ,_PUNCT ond_CONJ o_ADP ddigon_ADJ ._PUNCT
Ie_INTJ ,_PUNCT ac_CONJ yn_PART fwy_ADJ nag_CONJ i_ADP William_PROPN Morgan_PROPN !_PUNCT
Gallai_VERB 'r_DET iaith_NOUN fod_VERB wedi_PART hen_ADJ ddisgyn_VERB i_ADP 'r_DET tywyllwch_NOUN oni_CONJ bai_NOUN amdano_ADP ._PUNCT
Mae_VERB 'r_DET gwron_NOUN hwn_DET yn_PART haeddu_VERB pob_DET clod_NOUN a_CONJ diolch_NOUN ._PUNCT
Dewi_VERB M_NUM ._PUNCT Hughes_PROPN
Erthyglau_PROPN ,_PUNCT llythyrau_NOUN at_ADP y_DET Prif_NOUN Olygydd_X :_PUNCT
Y_DET Parchg_NOUN Ddr_NOUN Alun_PROPN Tudur_PROPN [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT
Golygyddion_PROPN
Y_DET Parchg_PROPN Iwan_PROPN Llewelyn_PROPN Jones_PROPN [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT
Alun_NOUN Lenny_PROPN [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT
Archebion_NOUN a_CONJ thaliadau_NOUN i_ADP 'r_DET Llyfrfa_PROPN :_PUNCT [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT
Dalier_VERB Sylw_PROPN !_PUNCT
Cyhoeddir_VERB y_DET Pedair_NOUN Tudalen_NOUN Gydenwadol_ADJ gan_ADP Bwyllgor_PROPN Llywio_PROPN 'r_DET Pedair_NOUN Tudalen_PROPN ac_CONJ nid_PART gan_ADP Undeb_NOUN yr_DET Annibynwyr_NOUN Cymraeg_PROPN ._PUNCT
Nid_PART oes_VERB a_PART wnelo_VERB Golygyddion_PROPN Y_DET Tyst_NOUN ddim_PART â_ADP chynnwys_VERB y_DET Pedair_NOUN Tudalen_PROPN ._PUNCT
YCHWANEGIAD_NOUN I_ADP 'R_DET TEULU_PROPN
Yn_ADP y_DET niwedd_NOUN Ionawr_NOUN eleni_ADV (_PUNCT 2017_NUM )_PUNCT ,_PUNCT ychwanegwyd_VERB at_ADP ein_DET teulu_NOUN ni_PRON pan_CONJ ddaeth_VERB Macsen_PROPN atom_ADJ ._PUNCT
Fe_PART 'i_PRON ganwyd_VERB yn_ADP Nhachwedd_NOUN 2016_NUM ym_ADP mhentref_NOUN gwledig_ADJ Llwyndyrys_PROPN yng_ADP Ngodre_PROPN 'r_DET Eifl_PROPN ,_PUNCT
ac_CONJ ymhen_ADP deufis_NOUN fe_PRON ddaeth_VERB i_ADP fyw_VERB i_PRON 'n_PART ty_NOUN ̂_ADP ni_PRON yn_PART fwndel_NOUN bywiog_ADJ ,_PUNCT blewog_ADJ ._PUNCT
Prysuraf_VERB i_PART ddweud_VERB mai_PART ci_NOUN yw_VERB Macsen_PROPN neu_CONJ Macs_PROPN ,_PUNCT ac_CONJ fel_ADP pob_DET anifail_NOUN anwes_VERB ,_PUNCT enillodd_VERB ei_DET le_NOUN 'n_PART llwyr_ADJ ,_PUNCT neu_CONJ 'n_PART hytrach_ADV ,_PUNCT hawlio_VERB 'i_PRON le_NOUN ._PUNCT
Ond_CONJ ar_ADP y_DET cyfan_NOUN ,_PUNCT y_PART mae_VERB 'n_PART gi_NOUN da_ADJ ,_PUNCT yn_PART ufudd_ADJ ,_PUNCT ac_CONJ yn_PART eithriadol_ADJ o_ADP ffyddlon_ADJ ._PUNCT
Petai_VERB ei_DET enw_NOUN 'n_PART MacseM_PROPN ,_PUNCT mi_PART fyddai_VERB yna_ADV odl_NOUN berffaith_NOUN glir_ADJ i_ADP MacseM_NOUN yn_PART SaleM_ADJ ._PUNCT ._PUNCT ._PUNCT ond_CONJ dyna_ADV fo_PRON ._PUNCT
Ar_ADP sawl_DET achlysur_NOUN (_PUNCT heblaw_ADP achlysuron_VERB cyhoeddus_ADJ )_PUNCT ,_PUNCT bu_AUX Macsen_PROPN yn_PART festri_NOUN Salem_PROPN ,_PUNCT Porthmadog_PROPN ,_PUNCT yn_PART rhedeg_VERB a_CONJ phrancio_VERB tra_CONJ roeddem_VERB ar_ADP ryw_NOUN berwyl_NOUN neu_CONJ 'i_PRON gilydd_NOUN ._PUNCT
Ond_CONJ un_NUM diwrnod_NOUN ,_PUNCT cafodd_VERB Macs_PROPN ddyrchafiad_NOUN o_ADP 'r_DET festri_NOUN i_ADP 'r_DET capel_NOUN gan_CONJ fy_PRON mod_AUX i_ADP wedi_PART mynd_VERB a_CONJ fo_VERB efo_ADP fi_PRON un_NUM pnawn_NOUN ,_PUNCT a_CONJ do_INTJ ,_PUNCT mi_PART fentrais_VERB ei_PRON ollwng_VERB yn_PART rhydd_ADJ yn_PART capel_NOUN sy_AUX 'n_PART dal_ADV saith_ADP cant_NUM o_ADP bobl_NOUN ._PUNCT
Dyna_DET lle_NOUN roedd_AUX o_PRON 'n_PART busnesu_VERB mewn_ADP corneli_NOUN ,_PUNCT yn_PART cerdded_VERB rhwng_ADP seti_NOUN ,_PUNCT ond_CONJ yn_PART sydyn_ADJ reit_ADP brasgamodd_VERB i_ADP 'r_DET cyntedd_NOUN (_PUNCT gan_CONJ fod_AUX y_DET drws_NOUN yn_PART agored_ADJ )_PUNCT a_CONJ rhedeg_VERB i_ADP fyny_ADV i_ADP 'r_DET oriel_NOUN ._PUNCT
I_ADP 'r_DET sawl_PRON a_CONJ w_PRON ̂_SYM yr_DET am_ADP yr_DET adeilad_NOUN ,_PUNCT yn_ADP yr_DET oriel_NOUN y_PART mae_VERB 'r_DET organ_NOUN bib_NOUN ._PUNCT
Bu_VERB o_ADP gwmpas_NOUN honno_PRON ._PUNCT
A_CONJ dyna_DET lle_NOUN ro_PART 'n_PART innau_VERB yn_ADP fy_DET mhryder_NOUN (_PUNCT am_ADP resymau_NOUN pur_ADV amlwg_ADJ )_PUNCT yn_PART ceisio_VERB ei_PRON gael_VERB i_ADP lawr_ADV o_ADP 'r_DET oriel_NOUN ._PUNCT
Er_CONJ i_ADP mi_PRON geisio_VERB pob_DET ffordd_NOUN drwy_ADP gerdded_VERB rhwng_ADP y_DET seti_NOUN (_PUNCT a_CONJ thros_NOUN ambell_ADJ un_NUM )_PUNCT ,_PUNCT i_ADP macnabs_ADV ,_PUNCT roedd_VERB y_DET rhyddid_NOUN yn_ADP y_DET galeri_NOUN yn_PART ge_NOUN ̂_ADJ m_CONJ rhy_ADV dda_ADJ i_ADP 'w_PRON therfynnu_VERB !_PUNCT
Yn_ADP y_DET diwedd_NOUN ,_PUNCT llwyddais_VERB i_ADP 'w_PRON ddal_VERB a_CONJ 'i_PRON gario_VERB i_ADP lawr_ADV y_DET capel_NOUN ._PUNCT
Mentro_VERB yn_ADP Salem_PROPN
Cyn_CONJ gadael_VERB y_DET capel_NOUN ,_PUNCT ro_AUX 'n_PART i_ADP eisiau_ADV mynd_VERB i_ADP no_PRON ̂_SYM l_ADP llyfr_NOUN oedd_AUX wedi_PART 'u_PRON gadael_VERB ar_ADP se_NOUN ̂_ADJ t_ADP y_DET pulpud_NOUN ._PUNCT
A_CONJ dyma_DET fentro_NOUN ._PUNCT ._PUNCT ._PUNCT Beth_PRON wnes_VERB i_PRON ?_PUNCT
Mi_PART adewais_VERB i_ADP Macs_PROPN wrth_ADP droed_NOUN grisiau_NOUN 'r_DET pulpud_NOUN ,_PUNCT ac_CONJ y_PART mae_VERB 'na_ADV wyth_NUM gris_NOUN iddo_ADP fo_PRON ,_PUNCT a_CONJ dyna_DET lle_NOUN roedd_VERB o_ADP yng_ADP ngodre_NOUN 'r_DET pulpud_NOUN yn_PART syllu_VERB i_ADP 'r_DET entrychion_NOUN megis_NOUN ,_PUNCT yn_PART anadlu_VERB 'n_PART gyflym_ADJ ,_PUNCT a_CONJ 'r_DET olwg_NOUN yn_ADP ei_DET lygaid_NOUN fel_ADP petai_VERB o_ADP 'n_PART erfyn_VERB arna_ADP i_PRON ,_PUNCT '_PUNCT Ty'd_NOUN lawr_ADV o_ADP fanna_X !_PUNCT '_PUNCT A_CONJ dyma_DET ddechrau_NOUN meddwl_VERB ,_PUNCT ac_CONJ o_ADP gopa_NOUN 'r_DET pulpud_NOUN uchel_ADJ ,_PUNCT ceisiais_VERB gael_VERB Macs_PROPN i_ADP ddringo_VERB 'r_DET grisiau_NOUN tuag_ADP ataf_ADP ._PUNCT
Dyna_DET lle_NOUN ro_ADP 'n_PART i_PRON o_ADP 'r_DET copa_NOUN yn_PART erfyn_VERB arno_ADP fo_PRON ,_PUNCT '_PUNCT Ty'd_PROPN i_ADP fyny_ADV ngwash_NOUN i_PRON ._PUNCT
Ty'd_INTJ ,_PUNCT ty'd_NOUN tyyyyyy'd_SYM ._PUNCT ._PUNCT ._PUNCT '_PUNCT Ond_CONJ wyddoch_VERB chi_PRON beth_PRON ?_PUNCT
Wedi_ADP eiliadau_NOUN go_ADV solat_ADJ o_ADP erfyn_VERB arno_ADP fo_PRON i_PART ddod_VERB ataf_ADP ,_PUNCT gwrthod_VERB yn_PART bendant_ADJ wnaeth_VERB o_PRON ,_PUNCT er_CONJ ei_PRON fod_VERB o_ADP rai_DET munudau_NOUN yng_ADP nghynt_NOUN wedi_PART dringo_VERB cryn_ADJ ddau_NUM ddwsin_NOUN o_ADP risiau_NOUN 'r_DET galeri_NOUN a_CONJ rhai_DET ychwanegol_ADJ o_ADP gwmpas_NOUN y_DET lle_NOUN hwnnw_DET a_CONJ 'r_DET organ_NOUN ._PUNCT
Ond_CONJ gwrthod_ADV dringo_VERB 'r_DET pulpud_NOUN -_PUNCT wyth_NUM -_PUNCT gris_NOUN wnaeth_VERB o_PRON ._PUNCT
Mynd_VERB i_ADP bob_DET man_NOUN ond_CONJ y_DET pulpud_NOUN ._PUNCT
Diddorol_ADJ ._PUNCT
Dydw_VERB i_PRON ddim_PART yn_PART addolwr_NOUN llefydd_NOUN fel_ADP y_DET pulpud_NOUN a_CONJ 'r_DET se_NOUN ̂_ADJ t_NOUN fawr_ADJ mewn_ADP unrhyw_DET gapel_NOUN fel_ADP ag_ADP y_DET rhydd_NOUN y_DET Pabyddion_PROPN a_CONJ 'r_DET Anglicaniaid_NOUN bwyslais_NOUN ar_ADP lefydd_NOUN '_PUNCT cysegredig_ADJ '_PUNCT o_ADP fewn_ADP eu_DET hadeiladau_NOUN ,_PUNCT ond_CONJ mi_PART ydw_VERB i_PRON 'n_PART credu_VERB mewn_ADP gostyngeiddrwydd_NOUN wrth_ADP weddi_ADP ̈_VERB o_ADP ,_PUNCT wrth_ADP agor_VERB y_DET Gair_NOUN ,_PUNCT wrth_ADP arwain_VERB oedfa_NOUN Fedydd_PROPN a_CONJ Chymun_PROPN ,_PUNCT ac_CONJ wrth_ADP weinyddu_VERB mewn_ADP priodasau_NOUN ac_CONJ angladdau_NOUN ,_PUNCT ac_CONJ mi_PART gredaf_VERB yn_PART gryf_ADJ mewn_ADP gostyngeiddrwydd_NOUN wrth_ADP wasanaethu_VERB pobl_NOUN yn_ADP eu_DET gwahanol_ADJ amgylchiadau_NOUN ._PUNCT
A_CONJ llawn_ADJ cymaint_ADV a_CONJ ̂_SYM hynny_PRON ,_PUNCT credaf_VERB mewn_ADP gostyngeiddrwydd_NOUN wrth_ADP siarad_VERB a_CONJ thrafod_VERB gyda_ADP chyd-_NOUN Gristnogion_PROPN ,_PUNCT a_CONJ phan_PRON glywaf_VERB am_ADP aelodau_NOUN eglwysig_ADJ a_CONJ gweinidogion_VERB yn_PART anghwrtais_ADJ ,_PUNCT dychrynaf_VERB am_ADP fy_DET mywyd_NOUN ,_PUNCT a_CONJ chaf_VERB f'atgoffa_PROPN o_ADP eiriau_NOUN 'r_DET Apostol_NOUN Paul_PROPN :_PUNCT '_PUNCT Peidiwch_PROPN a_CONJ ̂_ADV gwneud_VERB dim_PRON o_ADP gymhellion_NOUN hunanol_ADJ nac_CONJ o_ADP ymffrost_NOUN gwag_ADJ ,_PUNCT ond_CONJ mewn_ADP gostyngeiddrwydd_NOUN bydded_VERB i_ADP bob_DET un_NUM ohonoch_ADP gyfrif_NOUN y_DET llall_PRON yn_PART deilyngach_ADJ nag_CONJ ef_PRON ei_DET hun_NOUN ._PUNCT ._PUNCT ._PUNCT '_PUNCT (_PUNCT Colosiaid_NOUN 2_NUM :_SYM 3_NUM )_PUNCT Ac_CONJ y_PART mae_VERB gweddill_NOUN y_DET bennod_NOUN hon_PRON yn_PART adrodd_VERB nid_PART llinellau_NOUN ond_CONJ cyfrolau_NOUN am_ADP safon_NOUN ein_DET hymddygiad_NOUN os_CONJ yw_VERB 'r_DET label_NOUN '_PUNCT Cristion_NOUN '_PUNCT arnom_ADP ._PUNCT
'_PUNCT Cofia_PROPN 'r_DET enw_NOUN arnom_ADP sydd_VERB '_PUNCT ,_PUNCT meddai_VERB Eifion_PROPN Wyn_PROPN ._PUNCT
Mae_AUX rhai_DET pobl_NOUN yn_PART medru_VERB bod_VERB yn_PART fwy_ADV CI_NOUN aidd_ADJ eu_DET hymddygiad_NOUN na_CONJ chw_NOUN ̂_SYM n_NUM !_PUNCT
Gostyngeiddrwydd_NOUN piau_VERB hi_PRON ._PUNCT ._PUNCT ._PUNCT bob_DET amser_NOUN ._PUNCT
Iwan_PROPN Llewelyn_PROPN
Salem_INTJ ,_PUNCT Y_DET Ffo_NOUN ̂_PRON r_NOUN
Cynhelir_VERB Oedfa_X Ddatgorffori_PROPN 'r_DET eglwys_NOUN uchod_ADJ
Nos_NOUN Fercher_NOUN
15_NUM Tachwedd_NOUN ,_PUNCT 7.00_SYM p_NOUN ._PUNCT m_ADJ ._PUNCT
Capel_NOUN Ebeneser_PROPN ,_PUNCT y_DET Ffo_NOUN ̂_ADJ r_NOUN ._PUNCT
Oedfa_NOUN o_ADP dan_ADP arweiniad_NOUN
y_DET Parchg_PROPN Euros_X Wyn_PROPN Jones_PROPN
Cystadlaethau_VERB Cwpan_NOUN Denman_PROPN a_CONJ Thlysau_NOUN Cyfundeb_NOUN Lerpwl_PROPN ,_PUNCT 2018_NUM
Cywaith_NOUN gweledol_ADJ gan_ADP Ysgol_NOUN Sul_PROPN (_PUNCT poster_NOUN ,_PUNCT ffotograff_NOUN ,_PUNCT llun_NOUN neu_CONJ waith_NOUN celf_NOUN arall_ADJ ,_PUNCT yn_PART mesur_NOUN dim_ADV mwy_ADJ na_CONJ 4_NUM x_SYM 3_NUM troedfedd_NOUN )_PUNCT ar_ADP y_DET thema_NOUN :_PUNCT
'_PUNCT Cymru_PROPN a_CONJ Madagascar_X '_PUNCT
Cystadleuaeth_NOUN Tlysau_NOUN Cyfundeb_NOUN Lerpwl_PROPN Cystadlaethau_PROPN siarad_VERB cyhoeddus_ADJ ._PUNCT
Mae_VERB dau_NUM ddosbarth_NOUN ._PUNCT
Blynyddoedd_NOUN ysgol_NOUN 7_NUM -_SYM 9_NUM a_CONJ 10_NUM -_SYM 13_NUM ._PUNCT
Testun_PROPN :_PUNCT
'_PUNCT Dwylo_NOUN dros_ADP y_DET Mo_NOUN ̂_ADJ r_NOUN '_PUNCT
Y_DET cywaith_NOUN ac_CONJ /_PUNCT neu_CONJ enwau_NOUN 'r_DET cystadleuwyr_NOUN ar_ADP gyfer_NOUN y_DET siarad_NOUN cyhoeddus_ADJ i_PART gyrraedd_VERB Ty_PROPN ̂_PROPN John_PROPN Penri_PROPN ,_PUNCT 5_NUM Axis_X Court_PROPN ,_PUNCT Parc_NOUN Busnes_PROPN Glanyrafon_PROPN ,_PUNCT Bro_PROPN Abertawe_PROPN ,_PUNCT Abertawe_PROPN SA_NOUN 7_NUM 0_NUM AJ_X ,_PUNCT erbyn_ADP dydd_NOUN Gwener_PROPN ,_PUNCT 27_NUM Ebrill_PROPN 2018_NUM ,_PUNCT os_CONJ gwelwch_VERB yn_PART dda_ADJ ._PUNCT
Geraint_NOUN Tudur_NOUN Ysgrifennydd_NOUN Cyffredinol_ADJ
Cyhoeddwyd_VERB gan_ADP Undeb_NOUN yr_DET Annibynwyr_NOUN Cymraeg_PROPN ac_CONJ argraffwyd_VERB gan_ADP Wasg_X Morgannwg_PROPN ,_PUNCT [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT Cofrestrwyd_VERB yn_ADP y_DET Swyddfa_NOUN Bost_PROPN ._PUNCT
