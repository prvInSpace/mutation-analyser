Pa_PART liw_NOUN darw_NOUN sydd_VERB gen_ADP ti_PRON acw_ADV ?_PUNCT
Coch_ADJ ._PUNCT
Ond_CONJ ma_VERB fe_PRON 'n_PART coch_ADJ '_PUNCT itha_VERB gole_NOUN '_PUNCT ._PUNCT
Mae_VERB fe_PRON [_PUNCT saib_NOUN ]_PUNCT bach_ADJ yn_PART oleuach_ADJ na_CONJ enwb_VERB fi_PRON 'n_PART credu_VERB +_SYM
O_ADP yndi_VERB ?_PUNCT
Ydy_VERB +_SYM
Ydy_VERB o_PRON 'n_PART [_PUNCT aneglur_ADJ ]_PUNCT ond_CONJ coch_ADJ ?_PUNCT
Na_INTJ ._PUNCT
Na_INTJ ._PUNCT
Ydy_VERB +_SYM
Nes_CONJ di_PRON ddangos_VERB llunie_ADP imi_ADP ._PUNCT
Na_INTJ mae_VERB 'r_DET un_NUM gwyn_ADJ ges_VERB i_ADP ddim_PART mas_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT mas_NOUN o_ADP un_NUM arall_ADJ +_SYM
O_ADP reit_NOUN ._PUNCT
A_CONJ mama_NOUN un_NUM llwyd_ADJ mas_NOUN o_ADP [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Allan_ADV o_ADP hwnna_PRON ?_PUNCT
Ie_INTJ ._PUNCT
O_ADP reit_NOUN ._PUNCT
Mam_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
Mam_NOUN [_PUNCT -_PUNCT ]_PUNCT Mam_NOUN gwyn_ADJ a_CONJ tarw_NOUN choch_ADJ +_SYM
Na_INTJ mam_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
O_ADP reit_NOUN dyna_PRON pam_ADV mae_VERB 'r_DET llo_NOUN yn_PART [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ie_INTJ ._PUNCT
O_ADP [_PUNCT =_SYM ]_PUNCT ie_INTJ ie_X ie_X ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Wyt_AUX ti_PRON 'n_PART poeni_VERB am_ADP y_DET lliw_NOUN ?_PUNCT
Na_VERB dim_PRON really_NOUN +_SYM
So_PART ti_PRON ddim_PART yn_PART trio_ADV bridio_VERB gwyn_ADJ ?_PUNCT
Na_INTJ ._PUNCT
O_ADP ni_PRON 'n_PART gweld_VERB yr_DET heiffar_NOUN wen_NOUN gen_ADP ti_PRON ._PUNCT
odi_VERB mama_NOUN ddi_DET mama_NOUN honna'_VERBUNCT da_ADJ fi_PRON ond_CONJ dim_PRON wel_NOUN dim_PART fi_PRON oedd_AUX yn_PART moyn_ADV honna_VERB fe_PRON oedd_AUX yn_PART moyn_ADV honna_VERB +_SYM
O_ADP fo_PRON sydd_VERB ishio_VERB fo_PRON ?_PUNCT
Ie_INTJ ._PUNCT
Un_NUM du_ADJ fi_PRON moyn_NOUN nawr_ADV gan_CONJ fod_VERB pob_DET lliw_NOUN arall_ADJ '_PUNCT da_ADJ fi_PRON fi_PRON moyn_NOUN un_NUM du_ADJ ._PUNCT
O_ADP bydd_VERB rhaid_VERB i_ADP ti_PRON roid_VERB y_DET tarw_NOUN i_PRON [_PUNCT aneglur_ADJ ]_PUNCT '_PUNCT lle_NOUN ._PUNCT
Tarw_VERB du_ADJ ._PUNCT
Falle_PROPN ._PUNCT
Ond_CONJ wedyn_ADV yn_PART '_PUNCT nabod_VERB fi_PRON gaga_VERB fi_PRON un_NUM coch_ADJ mas_ADJ wedyn_ADV '_PUNCT ny_PRON [_PUNCT chwerthin_VERB ]_PUNCT +_SYM
O_ADP bydde_VERB fe_PRON 'n_PART neis_ADJ sa_AUX ti_PRON 'n_PART roi_VERB y_DET tarw_NOUN coch_ADJ ar_ADP y_DET fuwch_NOUN [_PUNCT aneglur_ADJ ]_PUNCT +_SYM
Ie_INTJ ._PUNCT
Mae_VERB 'n_PART bosib_ADJ iti_ADV gael_VERB du_ADJ ._PUNCT
O_ADP galle_VERB '_PUNCT ._PUNCT
Wel_PART coch_ADV odd_VERB y_DET tarw_NOUN g'ath_NOUN hi_PRON llynedd_ADV a_CONJ du_ADJ daeth_VERB y_DET llo_NOUN ._PUNCT
O_ADP '_PUNCT ny_PRON ti_PRON wedi_PART cael_VERB du_ADJ ?_PUNCT
Do_INTJ ond_ADP un_NUM [_PUNCT aneglur_ADJ ]_PUNCT oedd_VERB e_PRON ._PUNCT
ôô_NOUN
Gwerth'es_ADJ i_ADP fe_PRON mis_NOUN  _SPACE [_PUNCT -_PUNCT ]_PUNCT wythnos_NOUN diwetha'_ADJUNCT
So_NOUN chwil_NOUN [_PUNCT -_PUNCT ]_PUNCT chwilio_VERB am_ADP y_DET [_NOUN aneglur_ADJ ]_PUNCT wyt_VERB ti_PRON felly_ADV ?_PUNCT
Ie_INTJ licien_NOUN i_ADP ga'l_VERB un_NUM [_PUNCT Swn_PROPN yn_ADP y_DET cefndir_NOUN yn_PART tarfu_VERB ]_PUNCT
Ie_INTJ 'na_ADV fe_PRON ._PUNCT
Gewn_ADP ni_PRON weld_VERB ._PUNCT
Ie_INTJ '_PUNCT cos_NOUN o'dd_NOUN y_DET ferch_NOUN yn_PART gobeithio_VERB a_CONJ 'i_PRON di_PRON roi_VERB y_DET tarw_NOUN du_ADJ ar_ADP yr_DET heiffar_NOUN felyn_ADJ ac_CONJ roedd_AUX hi_PRON 'n_PART gobeithio_VERB ca'l_NOUN gwyn_ADJ sa_ADP 'r_DET gorau_ADJ +_SYM
Ie_INTJ ._PUNCT
Mi_PART ga_NOUN 'th_NOUN hi_PRON lo_NOUN da_ADJ ond_CONJ gwrw_NOUN ._PUNCT
O_ADP ie_INTJ ._PUNCT
So_NOUN da_ADJ 'n_PART ni_PRON 'n_PART gorfod_ADV treio_VERB to_NOUN yndan_ADJ ._PUNCT
Bydde_VERB wedyn_ADV ._PUNCT
I_ADP drio_ADV cael_VERB <_SYM aneglur_ADJ 2_NUM >_SYM So_ADP ni_PRON 'di_PART ca'l_VERB <_SYM aneglur_ADJ 2_NUM >_SYM o_ADP '_PUNCT sa_PRON i_PRON 'n_PART gwbod_VERB +_SYM
Bydde_VERB honna_VERB 'n_PART neis_ADJ ._PUNCT
O_ADP o'dd_NOUN yn_PART lyfli_NOUN ._PUNCT
Odd_VERB e_PRON 'n_PART lyfli_NOUN o_ADP lo_NOUN a_CONJ mama_NOUN e_PRON 'n_PART rel_NOUN dof_NOUN ._PUNCT
T'mod_VERB fel_ADP y_DET boi_NOUN ._PUNCT
Fi_PRON licio_VERB 'r_DET un_NUM sydd_VERB mewn_ADP fan_NOUN 'na_ADV ._PUNCT
Eh_PART ?_PUNCT
Mae_VERB 'r_DET un_NUM '_PUNCT dy_DET nhw_PRON '_PUNCT myn_VERB 'na_ADV ._PUNCT
[_PUNCT =_SYM ]_PUNCT Oes_VERB oes_NOUN [_PUNCT /=_PROPN ]_PUNCT
Ma'_VERBUNCT fe_PRON 'n_PART un_NUM pert_ADJ ._PUNCT
A_CONJ [_PUNCT -_PUNCT ]_PUNCT ac_CONJ o'dd_NOUN o_PRON 'n_PART dweud_VERB mama_NOUN hwnnw_DET reit_ADJ wel_NOUN yn_PART fwy_ADV dof_VERB na_CONJ 'r_DET llall_PRON ac_CONJ os_CONJ o'dd_NOUN genno_VERB fo_PRON ddewis_ADV mynd_VERB a_CONJ nhw_PRON allan_ADV bydde_AUX fe_PRON 'n_PART mynd_VERB a_CONJ 'r_DET un_NUM frown_ADJ ._PUNCT
O_ADP reit_NOUN ._PUNCT
T'mod_VERB o'dd_NOUN y_DET llall_PRON wedyn_ADV mwy_ADV [_PUNCT saib_NOUN ]_PUNCT +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT
Ia_INTJ [_PUNCT aneglur_ADJ ]_PUNCT [_PUNCT chwerthin_VERB ]_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT do_PART [_PUNCT chwerthin_VERB ]_PUNCT
So_INTJ [_PUNCT -_PUNCT ]_PUNCT na_CONJ so_VERB un_NUM du_ADJ licien_NOUN i_ADP ga'l_VERB ryw_DET bryd_NOUN pan_CONJ deuth_NOUN beth_PRON bynnag_NOUN ddeuth_VERB ._PUNCT
'_PUNCT Wy_PROPN 'n_PART ffaelu_VERB cymryd_VERB +_SYM
So_PART ti_PRON 'n_PART gobeithio_VERB deuth_NOUN y_DET fuwch_NOUN ag_ADP un_NUM arall_ADJ yn_PART ddu_ADJ ond_ADP heiffar_NOUN tro_NOUN nesa_ADJ ?_PUNCT
O_ADP ie_INTJ ._PUNCT
Fel_CONJ wedodd_VERB e_PRON mama_NOUN fe_PRON yn_PART well_ADJ '_PUNCT da_ADJ ni_PRON as_VERB long_NOUN as_ADJ bod_VERB nhw_PRON 'n_PART iach_ADJ +_SYM
Os_CONJ di_PRON e_PRON 'n_PART fyw_VERB dyna_DET 'r_DET peth_NOUN mwya_ADJ de_NOUN +_SYM
Ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Byw_VERB a_CONJ iach_ADJ a_CONJ wedyn_ADV gewn_ADP ni_PRON weld_VERB beth_PRON 'i_PRON nhw_PRON ._PUNCT
Ie_INTJ gan_ADP gobeithio_VERB wedyn_ADV bod_AUX o_PRON 'n_PART byw_VERB fel_ADP [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Wel_INTJ collon_VERB ni_PRON un_NUM llynedd_ADV [_PUNCT saib_NOUN ]_PUNCT un_NUM gwrw_NOUN +_SYM
Be_PRON ar_ADP ei_DET [_PUNCT aneglur_ADJ ]_PUNCT ?_PUNCT
Na_VERB wel_ADV na_PART buodd_VERB e_PRON dau_NUM ddiwrnod_NOUN [_PUNCT saib_NOUN ]_PUNCT o'dd_NOUN e_PRON 'n_PART iawn_ADJ ._PUNCT
O'dd_NOUN e_PRON 'n_PART rhedeg_VERB 'r_DET hyd_NOUN y_DET lle_NOUN yn_PART bownsio_VERB yn_PART iawn_ADJ ._PUNCT
A_CONJ '_PUNCT sa_PRON i_ADP 'n_PART gwbobo_VERB beth_PRON [_PUNCT -_PUNCT ]_PUNCT so_AUX ni_PRON 'n_PART gwbod_VERB +_SYM
O_ADP reit_NOUN ._PUNCT
So_PART mama_NOUN hi_PRON wedi_PART dod_VERB a_CONJ gwrw_NOUN '_PUNCT leni_VERB to_NOUN +_SYM
Reit_INTJ ._PUNCT
Ma_VERB 'r_DET rest_NOUN o_ADP nhw_PRON wedi_PART [_PUNCT aneglur_ADJ ]_PUNCT dod_VERB a_CONJ [_PUNCT aneglur_ADJ ]_PUNCT eleni_ADV so_AUX ni_PRON 'n_PART credu_VERB mama_NOUN rhaid_VERB i_ADP nhw_PRON gyd_ADP magu_VERB un_NUM gwr_NOUN 'w_PRON yn_PART gynta'_ADJUNCT cyn_CONJ bod_VERB nhw_PRON +_SYM
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT
'_PUNCT Na_INTJ beth_PRON fi_PRON 'di_PART meddwl_VERB ._PUNCT
Wel_INTJ '_PUNCT ti_PRON syniad_NOUN da_ADJ ._PUNCT
Ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
So_PART falle_VERB blwyddyn_NOUN nesa_ADJ bydd_VERB gen_ADP i_PRON yn_PART  _SPACE benw_NOUN +_SYM
So_NOUN os_CONJ gen_ADP ti_PRON hints_NOUN a_CONJ tips_NOUN sut_ADV i_ADP ga'l_VERB [_PUNCT aneglur_ADJ ]_PUNCT felly_ADV '_PUNCT cos_NOUN ma_VERB 'r_DET ddwy_NUM fan_NOUN acw_ADV wedi_PART cael_VERB gwr_NOUN 'w_PRON ._PUNCT
O_ADP iawn_ADV ?_PUNCT
A_CONJ [_PUNCT aneglur_ADJ ]_PUNCT '_PUNCT da_ADJ nhw_PRON ni_PRON 'n_PART gobeithio_VERB
Falle_VERB blwyddyn_NOUN nesa_ADJ de_NOUN [_PUNCT chwerthin_VERB ]_PUNCT
Dibynnu_VERB be_ADP dan_ADP nhw_PRON 'n_PART i_PRON '_PUNCT fyta_NOUN wy_NOUN 'n_PART cymryd_VERB ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
Neu_CONJ os_CONJ '_PUNCT dy_DET 'r_DET haul_NOUN allan_ADV ._PUNCT
Ie_INTJ falle_NOUN bod_VERB e_PRON '_PUNCT sa_PRON i_PRON 'n_PART gwbod_VERB
[_PUNCT chwerthin_VERB ]_PUNCT
'_PUNCT Sa_PRON i_PRON 'n_PART gw'bod_VERB [_PUNCT saib_NOUN ]_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT Dal_VERB i_PRON [_PUNCT aneglur_ADJ ]_PUNCT +_SYM
Wel_CONJ ma_VERB 'r_DET tarw_NOUN mewn_ADP '_PUNCT da_ADJ dwy_NUM tair_NUM blwydd_NOUN nawr_ADV ._PUNCT
Yr_DET un_NUM tarw_NOUN ?_PUNCT
Ie_INTJ +_SYM
So_PART fydd_VERB rhai_PRON i_ADP ti_PRON newid_VERB o_ADP tro_NOUN nesa_ADJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT Na_VERB na_INTJ [_PUNCT /=_PROPN ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT lloi_NOUN cynta_ADJ fydd_VERB rhain_PRON iddyn_ADP nhw_PRON ._PUNCT
O_ADP reit_ADV okay_NOUN ._PUNCT
ym_ADP ma_VERB nhw_PRON 'di_PART ca'l_ADV mynd_VERB mewn_ADP nawr_ADV yn_PART barod_ADJ am_ADP blwyddyn_NOUN nesa_ADJ ._PUNCT
Reit_INTJ ._PUNCT
A_CONJ mama_NOUN fe_PRON 'di_PART ca'l_ADV mynd_VERB mewn_ADP at_ADP y_DET rest_NOUN o_ADP nhw_PRON ddo_VERB '_PUNCT [_PUNCT -_PUNCT ]_PUNCT na_CONJ Dydd_NOUN Gwener_PROPN ._PUNCT
So_PART bydd_VERB rheini_VERB 'n_PART [_PUNCT aneglur_ADJ ]_PUNCT ben_NOUN mis_NOUN Mawrth_PROPN ?_PUNCT
Ie_INTJ fi_PRON 'n_PART gobeithio_VERB ._PUNCT
Bach_ADJ yn_PART gynt_ADV gobeithio_VERB eleni_ADV ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT
So_PART o_ADP ni_PRON [_PUNCT -_PUNCT ]_PUNCT o_ADP ni_PRON ffaelu_ADV mynd_VERB mewn_ADP i_ADP dim_DET byd_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT bydden_AUX ni_PRON wedi_PART entro_VERB fan_NOUN hyn_DET +_SYM
O_ADP dyna_DET ia_ADP ti_PRON ishio_VERB neud_VERB yn_PART gynt_VERB i_ADP +_SYM
Ie_INTJ achos_NOUN o_ADP ni_PRON ddim_PART even_AUX yn_PART gwbobo_NOUN bod_VERB ddim_PART llo_NOUN gydan_VERB ni_PRON ._PUNCT
A_CONJ reit_INTJ ._PUNCT
Pan_CONJ o'dd_NOUN entries_VERB yn_PART cau_VERB ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_X ie_INTJ [_PUNCT /=_PROPN ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT A_CONJ ond_CONJ ma_VERB 'n_PART well_ADJ ca'l_VERB nhw_PRON ar_ADP ôl_NOUN Dolig_ADJ +_SYM
Ydy_VERB ._PUNCT
Mis_NOUN Ionawr_PROPN i_ADP ti_PRON ga'l_VERB rhoid_VERB nhw_PRON yn_ADP y_DET flwyddyn_NOUN nesa'_ADJUNCT yn_ADP y_DET llyfrau_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Ydyn_AUX ni_PRON 'n_PART gobeithio_VERB ca'l_VERB nhw_PRON [_PUNCT -_PUNCT ]_PUNCT Wel_INTJ odyn_NOUN achos_CONJ bod_VERB ni_PRON [_PUNCT -_PUNCT ]_PUNCT bod_AUX nhw_PRON 'n_PART mynd_VERB i_ADP ga'l_VERB nhw_PRON bach_ADJ yn_PART gynt_VERB gyda_ADP 'r_DET [_NOUN aneglur_ADJ ?_PUNCT ]_PUNCT neud_VERB yn_PART siŵr_ADJ bod_VERB nhw_PRON ddim_PART rhy_ADV gynnar_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_X ie_X ie_INTJ [_PUNCT /=_PROPN ]_PUNCT jyst_X rhag_ADP ofn_NOUN bod_AUX nhw_PRON 'n_PART dod_VERB yn_PART gynt_VERB +_SYM
Ie_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT y_DET flwyddyn_NOUN anghywir_ADJ ._PUNCT
Bydde_PROPN ._PUNCT
Ie_INTJ ._PUNCT
Bydden_VERB nhw_PRON bach_ADJ yn_PART fach_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Byse_INTJ ._PUNCT
Rhy_ADV fach_ADJ ._PUNCT
Ia_INTJ '_PUNCT cos_NOUN wyt_AUX ti_PRON 'n_PART gwerthu_VERB nhw_PRON ymlaen_ADV dwyt_ADJ ?_PUNCT
Wel_INTJ yn_PART dibynnu_VERB ar_ADP beth_PRON ni_PRON 'n_PART ca'l_VERB ._PUNCT
Wel_CONJ ia_PRON yn_PART do_PRON o'dd_NOUN o_PRON 'n_PART dweud_VERB sa_PRON 'n_PART well_ADJ gan_ADP o_ADP drio_ADV gwerthu_VERB pob_DET dim_PRON i_ADP fridio_VERB +_SYM
Ie_INTJ ._PUNCT
Na_PART neud_VERB nhw_PRON 'n_PART gig_ADJ ._PUNCT
Dyna_ADV ydy_VERB 'r_DET gobaith_NOUN medde_VERB fo_PRON ._PUNCT
O_ADP na_PART beth_PRON ddwedodd_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT  _SPACE sai_VERB o_PRON 'n_PART licio_VERB o_ADP bosib_NOUN ca'l_VERB cig_NOUN +_SYM
Licien_NOUN i_ADP ga'l_VERB cig_NOUN rywbryd_ADV on_VERB [_PUNCT -_PUNCT ]_PUNCT ond_CONJ '_PUNCT sa_PRON i_ADP 'n_PART gwbo_NOUN mama_NOUN nhw_PRON i_ADP gyd_NOUN yn_PART dod_VERB mas_NOUN mor_ADV bert_ADJ mor_ADV ciwt_ADJ a_CONJ o_ADP sa_PART 'i_PRON moyn_NOUN gwerthu_VERB nhw_PRON ._PUNCT
Pwy_PRON '_PUNCT dy_DET 'r_DET un_NUM soft_NOUN ?_PUNCT
Tithe_PROPN neu_CONJ fo_VERB +_SYM
[_PUNCT =_SYM ]_PUNCT Fe_DET fe_PRON [_PUNCT /=_PROPN ]_PUNCT
Ti_PRON 'n_PART si_ADV [_PUNCT -_PUNCT ]_PUNCT ie_INTJ ma_VERB hynny_PRON 'n_PART reit_ADJ anodd_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Na_INTJ [_PUNCT =_SYM ]_PUNCT fe_PART fe_PRON [_PUNCT /=_PROPN ]_PUNCT sydd_AUX yn_PART pallu_VERB b'yta_NOUN yr_DET [_PUNCT -_PUNCT ]_PUNCT t'mod_VERB y_DET [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT gynta'_ADJUNCT gelon_VERB ni_PRON wedodd_VERB e_PRON na_CONJ '_PUNCT sa_PRON i_ADP 'n_PART b'yta_VERB fe_PRON ._PUNCT
Gwerthai_VERB e_PRON i_ADP rywun_NOUN arall_ADJ i_PART neud_VERB [_PUNCT -_PUNCT ]_PUNCT i_PART dorri_VERB 'n_PART gig_ADJ +_SYM
Neuth_NOUN o_ADP +_SYM
Sa_CONJ i_PRON yn_PART b'yta_NOUN fe_ADP +_SYM
Fo_PRON 'n_PART gallu_ADV neud_VERB o_ADP [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
[_PUNCT wfftio_VERB ]_PUNCT
Ti_PRON 'm_DET yn_PART meddwl_VERB ?_PUNCT
Na_INTJ '_PUNCT sa_PRON i_PRON 'n_PART gwbobo_VERB ._PUNCT
O_ADP diar_NOUN ._PUNCT
Bydd_VERB gen_ADP ti_PRON lot_PRON o_ADP wartheg_NOUN acw_ADV fydd_VERB ._PUNCT
Fi_PRON 'n_PART credu_VERB bydd_VERB e_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT
Fi_PRON 'n_PART credu_VERB bydd_VERB e_PRON ._PUNCT
Na_PART wedodd_VERB e_PRON dim_DET gobaith_NOUN mewn_ADP b'yta_NOUN enwg_VERB ._PUNCT
O_ADP wel_NOUN ._PUNCT
Y_DET fe_PART enwodd_VERB e_PRON ._PUNCT
Ie_INTJ wel_NOUN 'na_ADV fo_PRON ._PUNCT
Camgymeriad_NOUN mawr_ADJ ._PUNCT
Camgymeriad_NOUN ._PUNCT
Ond_CONJ dwi_AUX 'n_PART cofio_VERB yr_DET  _SPACE fuwch_NOUN gynta_ADJ gathon_VERB ni_PRON a_CONJ wedyn_ADV llo_NOUN gwrw_NOUN a_PART wnes_VERB i_PRON gyrru_VERB tecst_VERB i_ADP 'm_DET chwaer_NOUN i_PART ddeud_VERB ma_PRON hi_PRON 'di_PART ca'l_VERB llo_NOUN gwrw_NOUN a_CONJ dwi_VERB ishio_VERB 'r_DET bocs_NOUN gynta_ADJ a_CONJ o'dd_NOUN yr_PART ymateb_NOUN ges_VERB i_ADP +_SYM
[_PUNCT chwerthin_VERB ]_PUNCT O_ADP neis_NOUN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT Oedden_ADP ni_PRON 'di_PART deud_VERB llo_X gwrw_NOUN yn_ADP y_DET bocs_NOUN i_ADP dalu_VERB am_ADP y_DET lleill_NOUN ._PUNCT
Iawn_INTJ ._PUNCT
Neud_VERB sense_NOUN ._PUNCT
Ond_CONJ 'na_ADV ti_PRON 'r_DET unig_ADJ ffordd_NOUN o_ADP neud_VERB o_ADP ynde_ADP +_SYM
Odi_ADP ._PUNCT
Os_CONJ nad_PART wyt_AUX ti_PRON 'n_PART gwerthu_VERB nhw_PRON ymlaen_ADV [_PUNCT aneglur_ADJ ]_PUNCT tarw_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Wel_INTJ 'na_ADV beth_PRON [_PUNCT -_PUNCT ]_PUNCT gelon_VERB ni_PRON bris_NOUN '_PUNCT itha_VERB da_ADJ amdano_ADP fe_PRON wythnos_NOUN diwetha_ADJ +_SYM
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT '_PUNCT cos_NOUN o'dd_NOUN o_PRON 'n_PART ddigon_ADV ifanc_ADJ doedd_VERB a_CONJ oedd_VERB hwnna_PRON 'n_PART iawn_ADJ i_ADP +_SYM
[_PUNCT =_SYM ]_PUNCT O'dd_NOUN o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Ond_CONJ '_PUNCT da_ADJ hwn_DET o'ddan_AUX ni_PRON wedi_PART cadw_VERB fo_PRON tair_ADV so_VERB o'dd_NOUN o_PRON 'di_PART ca'l_VERB bywyd_NOUN reit_NOUN da_ADJ ._PUNCT
ôô_NOUN ._PUNCT
A_CONJ wedyn_ADV i_ADP 'r_DET bocs_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Achos_CONJ o_ADP ni_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT wel_NOUN o_ADP ni_PRON 'n_PART siarad_VERB '_PUNCT da_ADJ menyw_NOUN lan_ADV yn_PART [_PUNCT aneglur_ADJ ]_PUNCT o'dd_NOUN hi_PRON o_ADP lleoliad_NOUN a_CONJ o'dd_AUX hi_PRON 'n_PART gofyn_VERB na_CONJ beth_PRON [_PUNCT -_PUNCT ]_PUNCT os_CONJ na_PART '_PUNCT gon_AUX ni_PRON 'n_PART mynd_VERB i_PART werthu_VERB fe_PRON beth_PRON o_ADP ni_PRON 'n_PART mynd_VERB i_PART neud_VERB iddo_ADP fe_PRON ?_PUNCT
A_PART wedes_VERB i_PRON o_PRON [_PUNCT saib_NOUN ]_PUNCT cig_NOUN bydde_VERB fe_PART wedyn_ADV ._PUNCT
Ie_INTJ ._PUNCT
A_PART wedodd_VERB hi_PRON o_ADP a_PART bydden_AUX ni_PRON 'n_PART neud_VERB e_PRON cyn_CONJ bod_VERB e_PRON 'n_PART tri_NUM deg_NUM mis_NOUN neu_CONJ wedyn_ADV '_PUNCT ny_PRON '_PUNCT cos_NOUN o_ADP ni_PRON ddim_PART yn_PART gw'bod_ADJ bod_VERB gwahaniaeth_NOUN ._PUNCT
A_CONJ reit_ADV okay_VERB ._PUNCT
A_PART wedes_VERB i_ADP dim_PRON idea_VERB wedes_NOUN i_ADP achos_NOUN o_ADP ni_PRON ddim_PART yn_PART gw'bod_ADJ bod_VERB gwahaniaeth_NOUN ife_ADV a_CONJ wedodd_VERB hi_PRON o_ADP odd_NOUN [_PUNCT -_PUNCT ]_PUNCT mama_NOUN hi_PRON 'n_PART neud_VERB nhw_PRON ar_ADP ôl_NOUN tri_NUM deg_NUM mis_NOUN o'dd_PUNCT hi_PRON 'n_PART tynnu_VERB 'r_DET <_SYM aneglur_ADJ 2_NUM >_SYM mas_NOUN +_SYM
O_ADP ie_INTJ [_PUNCT =_SYM ]_PUNCT ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
A_PART wedodd_VERB hi_PRON  _SPACE bod_VERB nhw_PRON 'n_PART coll_ADJ [_PUNCT -_PUNCT ]_PUNCT yn_PART colli_VERB fel_CONJ cwpwl_ADP o_ADP joints_NOUN a_CONJ pethe_VERB mama_NOUN mwy_PRON o_ADP gig_NOUN '_PUNCT da_ADJ nhw_PRON a_CONJ mwy_PRON o_ADP flas_NOUN '_PUNCT da_ADJ nhw_PRON ._PUNCT
Wel_INTJ ie_INTJ ._PUNCT
Am_ADP bod_VERB nhw_PRON 'n_PART h_VERB ?_PUNCT n_NUM de_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Ma'_VERBUNCT nhw_PRON braidd_ADV yn_PART ifanc_ADJ o_ADP dan_ADP hynny_PRON '_PUNCT cos_NOUN dyw_VERB +_SYM
So_ADP nhw_PRON 'n_PART trafeili_NOUN ._PUNCT
Hwyr_ADJ mama_AUX nhw_PRON 'n_PART tyfu_VERB ynde_ADP ._PUNCT
Ie_INTJ ._PUNCT
A_CONJ wedyn_ADV '_PUNCT wyt_VERB ti_PRON angen_NOUN [_PUNCT saib_NOUN ]_PUNCT yr_DET cig_NOUN +_SYM
I_ADP gyd_CONJ oes_VERB ._PUNCT
Ie_INTJ '_PUNCT cos_NOUN ma_VERB [_PUNCT -_PUNCT ]_PUNCT ma_AUX nhw_PRON fod_VERB yn_PART h_ADJ ?_PUNCT n_VERB i_PRON gael_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ydyn_VERB so_ADJ +_SYM
Ie_INTJ byddai_AUX 'n_PART neud_VERB nhw_PRON ar_ADP ôl_NOUN tri_NUM deg_NUM chwech_NUM +_SYM
O_ADP reit_NOUN ._PUNCT
'_PUNCT Cos_NOUN dyw_VERB e_PRON 'm_DET werth_ADJ [_PUNCT saib_NOUN ]_PUNCT +_SYM
Na_INTJ ._PUNCT
Pris_NOUN lladd_VERB ._PUNCT
Iawn_ADV os_CONJ oes_VERB gen_ADP ti_PRON tir_NOUN gwell_ADJ ond_CONJ dyw_VERB ein_DET tir_NOUN ni_PRON ddim_PART digon_PRON o_ADP seis_NOUN tydy_ADJ ._PUNCT
Wel_CONJ s'dim_DET pwynt_NOUN mynd_VERB a_CONJ fe_PRON i_ADP tir_NOUN rhy_ADV da_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT +_SYM
Wel_CONJ na_PART ti_PRON '_PUNCT im_PRON yn_PART gallu_ADV fforsio_VERB fo_PRON achos_CONJ ma_AUX fo_PRON 'n_PART mynd_VERB yn_PART ddew_ADJ ._PUNCT
Ydy_VERB ._PUNCT
Ond_CONJ os_CONJ wyt_VERB ti_PRON ishio_VERB cael_VERB y_DET cig_NOUN mama_NOUN rhaid_VERB c'al_NOUN fraster_NOUN a_CONJ ti_PRON 'n_PART gorfod_ADV cael_VERB y_DET seis_NOUN cyn_CONJ cael_VERB y_DET braster_NOUN yn_PART dwyt_ADJ ._PUNCT
Wyt_INTJ ._PUNCT
Odi_ADP ._PUNCT
A_CONJ wedyn_ADV dyw_VERB ein_DET tir_NOUN ni_PRON ara_ADV deg_NUM y_PART mae_AUX 'n_PART tyfu_VERB ac'os_PUNCT dio_NOUN 'm_DET yn_PART dir_NOUN da_ADJ ._PUNCT
Na_INTJ ._PUNCT
A_CONJ wedyn_ADV mama_AUX nhw_PRON 'n_PART ca'l_VERB 'i_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT [_PUNCT =_SYM ]_PUNCT Na_VERB na_INTJ [_PUNCT /=_PROPN ]_PUNCT rhai_DET ni_PRON '_PUNCT fyd_NOUN ._PUNCT
Digon_ADV de_NOUN [_PUNCT chwerthin_VERB ]_PUNCT +_SYM
A_CONJ wedyn_ADV mama_VERB nhw_PRON gorfod_ADV ca'l_NOUN bach_ADJ yfe_PART ca'l_NOUN maldodi_VERB nhw_PRON a_CONJ ca'l_VERB nhw_PRON i_ADP ddod_VERB a_CONJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Sa'_NOUNUNCT fyddi_VERB 'di_PART dipyn_NOUN bach_ADJ o_ADP amser_NOUN cyn_ADP i_ADP ti_PRON gael_VERB stecen_NOUN ._PUNCT
Os_CONJ di_PRON o_ADP +_SYM
Byddaf_AUX fi_PRON 'n_PART credu_VERB ._PUNCT
Ond_CONJ man_NOUN y_DET man_NOUN i_ADP ni_PRON jyst_ADV mynd_VERB yn_PART vegeterian_VERB i_ADP gyd_ADP ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT Sa_X '_PUNCT fo_PRON bydd_VERB a_CONJ 'r_DET bai_NOUN ._PUNCT
Ie_INTJ +_SYM
Gorfod_ADV prynu_VERB mwy_PRON o_ADP ddir_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT
[_PUNCT =_SYM ]_PUNCT Nes_CONJ nes_CONJ [_PUNCT /=_PROPN ]_PUNCT i_PART ofyn_VERB iddo_ADP fo_PRON bu_VERB rhaid_VERB i_ADP ti_PRON brynu_VERB cae_NOUN arall_ADJ ._PUNCT
Na_PART medda_VERB fo_PRON ._PUNCT
O_ADP 'n_PART ni_PRON 'n_PART meddwl_VERB ie_INTJ ?_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Os_CONJ '_PUNCT da_ADJ ti_PRON am_ADP fynd_VERB a_CONJ nhw_PRON ._PUNCT
Na_AUX fi_PRON 'n_PART meddwl_VERB gwerthu_VERB fydd_AUX e_PRON 'n_PART trio_ADV neud_VERB siŵr_ADV y_PART fod_VERB ._PUNCT
Trial_PROPN gwerthu_VERB dwi_AUX 'n_PART meddwl_VERB ._PUNCT
Ond_CONJ na_PART mama_NOUN fe_PRON jyst_X yn_PART moyn_ADV rhoi_VERB ben_NOUN i_ADP nhw_PRON i_ADP gyd_ADP ._PUNCT
Ma_VERB fe_PRON jyst_X moyn_VERB nhw_PRON i_ADP gyd_NOUN ar_ADP ben_NOUN nawr_ADV ._PUNCT
Beth_PRON i_ADP ti_PRON 'n_PART neud_VERB a_CONJ nhw_PRON i_ADP gyd_ADP de_NOUN ?_PUNCT
Ti_PRON ffaelu_VERB cadw_VERB nhw_PRON i_ADP gyd_ADP ._PUNCT
Bydd_AUX e_PRON 'di_PART cael_VERB gwared_NOUN y_DET tarw_NOUN ?_PUNCT
Wel_CONJ bydd_VERB +_SYM
A_CONJ geith_NOUN o_ADP gadw_VERB nhw_PRON 'n_PART hen_ADJ wedyn_NOUN ._PUNCT
Ma'_VERBUNCT nhw_PRON 'n_PART byw_VERB drost_NOUN i_ADP '_PUNCT hugain_NOUN '_PUNCT sti_NOUN dwi_AUX 'n_PART dallt_VERB hynny_PRON ._PUNCT
Ie_INTJ [_PUNCT saib_NOUN ]_PUNCT
Bydd_VERB gen_ADP ti_PRON llond_NOUN cae_NOUN o_ADP hen_ADJ bethau_NOUN ._PUNCT
Bydd_VERB a_CONJ [_PUNCT -_PUNCT ]_PUNCT ond_CONJ na_PART wedodd_VERB e_PRON bydd_VERB rhaid_VERB i_ADP ni_PRON ddech_VERB [_PUNCT -_PUNCT ]_PUNCT fi_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT wedodd_VERB e_PRON fi_PRON 'n_PART gwbobo_VERB pryd_ADV bydden_AUX ni_PRON 'n_PART gwerthu_VERB gynta'_ADJUNCT yn_PART lle_NOUN bod_AUX ni_PRON 'n_PART cadw_VERB nhw_PRON mynd_VERB yn_PART rhy_ADV hen_ADJ a_CONJ wedyn_ADV na_PART ti_PRON ffaelu_VERB cael_VERB gwared_NOUN ar_ADP hi_PRON mama_NOUN hi_PRON 'n_PART neis_ADJ ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Gewn_ADP ni_PRON weld_VERB [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
'_PUNCT na_ADV fe_PRON ._PUNCT
512.836_NOUN
