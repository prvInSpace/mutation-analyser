Thema_NOUN -_PUNCT thema_NOUN Amlieithog_PROPN WordPress_X ar_ADP gyfer_NOUN Cymru_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Thema_NOUN Amlieithog_ADJ ar_ADP gyfer_NOUN WordPress_X
Ar_ADP Gael_VERB Nawr_ADV -_PUNCT Lawrlwythwch_PROPN Yma_ADV !_PUNCT
Llwytho_VERB ._PUNCT
._PUNCT ._PUNCT
Gweithredu_NOUN ._PUNCT
._PUNCT ._PUNCT
Llwyddo_VERB ._PUNCT
Thema_NOUN Gymraeg_PROPN sydd_AUX yn_PART galluogi_VERB aml_ADJ -_PUNCT ieithrwydd_NOUN yn_PART hawdd_ADJ yw_VERB Thema_PROPN ._PUNCT
Mae_VERB yn_PART hawdd_ADJ i_ADP 'w_PRON lwytho_VERB ar_ADP unrhyw_DET wefan_NOUN WordPress_X ag_CONJ yn_PART cynnwys_VERB llwyth_PRON o_ADP newidiadau_NOUN sydd_VERB yn_PART eich_PRON galluogi_VERB i_ADP reoli_VERB a_CONJ rhedeg_VERB eich_DET gwefan_NOUN amlieithog_ADJ yn_PART hawdd_ADJ ._PUNCT
Nodweddion_PROPN
Amlieithog_ADJ allan_ADV o_ADP 'r_DET bocs_NOUN -_PUNCT Gweithredwch_PROPN a_CONJ dyna_ADV ni_PRON !_PUNCT
Personoli_VERB -_PUNCT ychwanegwch_VERB eich_DET logo_VERB a_CONJ newidiwch_VERB y_DET lliwiau_NOUN
Panel_NOUN oruwchwylio_VERB -_PUNCT gweld_VERB ystadegau_NOUN am_ADP eich_DET gwefan_NOUN o_ADP 'r_DET bwrdd_NOUN cyfenw_NOUN
Llawer_ADV mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
Mae_VERB yn_PART gôd_ADV agored_ADJ sydd_AUX yn_PART golygu_VERB gall_VERB unrhyw_DET un_NUM ei_DET gymeryd_NOUN a_CONJ 'i_PRON addasu_VERB i_ADP 'w_PRON anghenion_NOUN ._PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Cyhoeddwyd_VERB ar_ADP dyddiad_NOUN dyddiad_NOUN gan_ADP Thema_PROPN
Wel_INTJ -_PUNCT dyma_DET gychwyn_NOUN ar_ADP brosiect_NOUN hynod_ADJ o_ADP ddiddorol_ADJ arall_ADJ !_PUNCT
Yn_PART ddiweddar_ADJ rydw_AUX i_ADP wedi_PART bod_VERB yn_PART ddigon_ADV ffodus_ADJ i_PART ennill_VERB grant_NOUN gan_ADP Llywodraeth_NOUN Cymru_PROPN i_ADP ddatblygu_VERB fframwaith_NOUN thema_NOUN ar_ADP gyfer_NOUN WordPress_X sydd_AUX yn_PART galluogi_VERB defnyddwyr_NOUN i_ADP gyhoeddi_VERB cynnwys_NOUN yn_PART ddwyieithog_ADJ yn_PART hawdd_ADJ ,_PUNCT a_CONJ sydd_VERB yn_PART hygyrch_ADJ ._PUNCT
Bydd_AUX tri_NUM thema_NOUN yn_PART cael_VERB eu_DET creu_VERB yn_PART gyntaf_ADJ ,_PUNCT ar_ADP gyfer_NOUN :_PUNCT
Busnesau_NOUN /_PUNCT Elusennau_PROPN
Blogiau_NOUN personol_ADJ
Gwefannau_VERB e_PRON -_PUNCT werthiant_NOUN
Y_DET gobaith_NOUN (_PUNCT or-_CONJ obeithiol_ADJ ,_PUNCT efallai_ADV ?_PUNCT )_PUNCT yw_VERB i_PART ddogfennu_VERB sut_ADV y_PART mae_AUX 'r_DET prosiect_NOUN yn_PART dod_VERB yn_ADP ei_DET flaen_NOUN drwy_ADP gyfres_NOUN o_ADP flogiau_NOUN byr_ADJ fel_ADP hyn_PRON ._PUNCT
Y_DET Pwrpas_PROPN
Pwrpas_VERB y_DET prosiect_NOUN o_ADP fy_DET ochor_NOUN i_PRON yw_VERB i_PART allu_VERB rhoi_VERB y_DET gallu_VERB i_ADP gyhoeddi_VERB yn_PART aml_ADJ -_PUNCT ieithol_ADJ i_ADP unrhyw_DET un_NUM ,_PUNCT a_CONJ gwneud_VERB yn_PART siwr_ADJ fod_AUX y_DET cynnwys_NOUN hynny_PRON yn_PART hygyrch_ADJ (_PUNCT gellir_VERB ei_PRON defnyddio_VERB yn_PART rhwydd_NOUN er_ADP unrhyw_DET anabledd_NOUN )_PUNCT ._PUNCT
Mae_AUX 'n_PART gyfle_NOUN felly_ADV i_ADP fusnes_NOUN ,_PUNCT elusen_NOUN ,_PUNCT siop_NOUN ar_ADP y_DET we_NOUN ,_PUNCT unigolion_NOUN a_CONJ blogiau_ADV allu_ADP cael_VERB dyluniad_NOUN gwefan_NOUN fodern_ADJ sydd_AUX yn_PART rhoi_VERB y_PART gallu_VERB o_ADP gyhoeddi_VERB yn_PART ddwyieithog_ADJ yn_PART greiddiol_ADJ yn_ADP eu_DET gwefan_NOUN ._PUNCT
Rydw_AUX i_PRON yn_PART rhedeg_VERB busnes_NOUN dylunio_VERB a_CONJ datblygu_VERB gwe_NOUN o_ADP 'r_DET enw_NOUN Gwe_NOUN Cambrian_NOUN Web_PROPN -_PUNCT ag_ADP ein_DET amcan_NOUN busnes_NOUN yw_VERB "_PUNCT Cael_VERB Cymru_PROPN a_CONJ 'r_DET Gymraeg_PROPN arlein_NOUN "_PUNCT -_PUNCT rydw_VERB i_ADP yn_PART frwdfrydig_ADJ iawn_ADV dros_ADP allu_NOUN cynyddu_VERB faint_NOUN o_ADP gynnwys_VERB Cymraeg_PROPN sydd_VERB arlein_PROPN -_PUNCT yn_PART enwedig_ADJ y_DET cynnwys_NOUN a_CONJ ellir_VERB ei_PRON chwilio_VERB drwy_ADP beiriannau_NOUN gwe_NOUN megis_ADP Google_PROPN ._PUNCT
O_ADP ran_NOUN y_DET Llywodraeth_NOUN ,_PUNCT eu_DET hamcanion_NOUN ar_ADP gyfer_NOUN y_DET prosiect_NOUN yw_VERB i_PART gael_VERB mwy_PRON o_ADP Gymraeg_PROPN arlein_NOUN ,_PUNCT amcan_NOUN y_PART rydan_VERB ni_PRON yn_PART frwdfrydig_ADJ amdano_ADP hefyd_ADV ._PUNCT
Ffynhonnell_PROPN Agored_PROPN
Rydw_AUX i_ADP wedi_PART penderfynu_VERB ei_DET agor_VERB i_ADP 'r_DET gymuned_NOUN fel_ADP prosiect_NOUN agored_ADJ /_PUNCT open_NOUN -_PUNCT source_NOUN ._PUNCT
Golyga_VERB hyn_PRON wrth_ADP gwrs_NOUN y_PART bydd_VERB ar_ADP gael_VERB am_ADP ddim_PRON i_ADP unrhyw_DET un_NUM ei_PRON ddefnyddio_VERB unwaith_ADV y_PART bydd_VERB gofynnion_NOUN y_DET grant_NOUN wedi_PART eu_DET cyflawni_NOUN ,_PUNCT ond_CONJ dwi_VERB yn_PART wir_ADJ gobeithio_VERB gallu_VERB creu_VERB cymuned_NOUN o_ADP ddatblygwyr_NOUN gwe_NOUN WordPress_X Cymraeg_PROPN sydd_VERB yn_PART frwdfrydig_ADJ dros_ADP ddefnyddio_VERB 'r_DET dechnoleg_NOUN yma_DET yng_ADP Nghymru_PROPN ar_ADP gyfer_NOUN hybu_VERB Cymru_PROPN a_CONJ 'r_DET Gymraeg_PROPN arlein_NOUN ._PUNCT
Gadael_VERB Ymateb_NOUN Diddymu_PROPN ymateb_VERB
Ni_PART fydd_AUX eich_DET cyfeiriad_NOUN e_PRON -_PUNCT bost_NOUN yn_PART cael_VERB ei_DET gyhoeddi_VERB ._PUNCT
Mae_AUX 'r_DET meysydd_NOUN gofynnol_ADJ yn_PART cael_VERB eu_DET marcio_VERB *_SYM
Sylw_NOUN
Enw_NOUN *_SYM
E_NUM -_PUNCT bost_NOUN *_SYM
Gwefan_PROPN
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Adnoddau_NOUN Cymraeg_PROPN ar_ADP gyfer_NOUN defnyddio_VERB 'r_DET We_PROPN yn_ADP Gymraeg_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Adnoddau_NOUN Cymraeg_PROPN
Mae_VERB llwyth_PRON o_ADP adnoddau_NOUN Cymraeg_PROPN ar_ADP gael_VERB y_DET dyddiau_NOUN hyn_PRON -_PUNCT dyma_DET rai_PRON o_ADP 'n_PART ffefrynnau_NOUN ._PUNCT
Mae_AUX ystod_NOUN ehangach_ADJ o_ADP feddalwedd_NOUN Cymraeg_PROPN ar_ADP gael_VERB ar_ADP wefan_NOUN Meddal_PROPN ._PUNCT Cymru_PROPN ._PUNCT
Creu_VERB Gwefannau_PROPN
Mae_VERB Thema_PROPN ._PUNCT Cymru_PROPN yn_PART falch_ADJ iawn_ADV o_ADP fod_VERB wedi_PART ei_PRON seilio_VERB ar_ADP sail_NOUN gadarn_NOUN ,_PUNCT ddiogel_ADJ a_CONJ chôd_VERB -_PUNCT agored_VERB WordPress_X ._PUNCT
Mae_AUX yn_PART pweru_VERB bron_ADV i_ADP 25_NUM %_NOUN o_ADP wefannau_NOUN 'r_DET we_NOUN -_PUNCT a_CONJ mae_VERB ar_ADP gael_VERB yn_ADP y_DET Gymraeg_PROPN ._PUNCT
Diolch_INTJ o_ADP galon_NOUN i_ADP enwb_VERB cyfenw_NOUN am_ADP ei_DET gyfieithu_VERB ._PUNCT
WordPress_VERB yn_ADP y_DET Gymraeg_PROPN ._PUNCT
Pori_VERB 'r_DET We_PROPN
Mae_VERB popeth_PRON arlein_NOUN y_DET dyddiau_NOUN yma_ADV -_PUNCT a_CONJ mae_VERB angen_NOUN porth_NOUN Gymraeg_PROPN iddo_ADP ._PUNCT
Dyma_DET opsiynau_NOUN Gymraeg_PROPN ar_ADP gyfer_NOUN porwyr_NOUN gwe_NOUN :_PUNCT
Google_PROPN Chrome_PROPN -_PUNCT Sut_ADV i_ADP Newid_VERB Iaith_NOUN Google_PROPN Chrome_PROPN
Mozilla_VERB Firefox_NOUN Cymraeg_PROPN
Ebyst_PROPN
I_ADP ddelio_VERB a_CONJ 'r_DET gybolfa_NOUN o_ADP ebyst_NOUN a_CONJ ddaw_VERB i_ADP 'n_PART blychau_VERB pob_DET diwrnod_NOUN ,_PUNCT dyma_DET raglenni_NOUN sydd_VERB ar_ADP gael_VERB yn_ADP y_DET Gymraeg_PROPN :_PUNCT
Microsoft_PROPN Outlook_PROPN -_PUNCT gweler_VERB pecynnau_NOUN ar_ADP gyfer_NOUN Office_PROPN islaw_ADP ._PUNCT
Mozilla_VERB Thunderbird_X -_PUNCT Meddalwedd_PROPN am_ADP ddim_PRON ,_PUNCT côd_VERB agored_VERB ar_ADP gyfer_NOUN ebyst_NOUN ._PUNCT
Prosesu_VERB Geiriau_PROPN ,_PUNCT Taenlenni_PROPN
Pecyn_NOUN LibreOffice_PROPN Cymraeg_PROPN
Office_PROPN 2003_NUM -_PUNCT Pecyn_PROPN Rhyngweithio_PROPN Cymraeg_PROPN
Office_VERB 2007_NUM -_PUNCT Pecyn_PROPN Rhyngweithio_PROPN Cymraeg_PROPN
Office_VERB 2010_NUM -_PUNCT Pecyn_PROPN Rhyngweithio_PROPN Cymraeg_PROPN
Office_PROPN 2013_NUM -_PUNCT Pecyn_PROPN Rhyngweithio_PROPN Cymraeg_PROPN
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Ein_DET blog_ADJ a_CONJ newyddion_NOUN diweddaraf_ADJ
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Cyhoeddwyd_VERB ar_ADP dyddiad_NOUN dyddiad_NOUN gan_ADP Thema_PROPN
Lawnsio_VERB WordPress_X Cymru_PROPN !_PUNCT
Yn_PART ddiweddar_ADJ mae_AUX Cymru_PROPN wedi_PART profi_VERB rhyw_PRON chwildro_NOUN dechnegol_ADJ reit_NOUN sylweddol_ADJ -_PUNCT ond_CONJ wrth_ADP gwrs_NOUN mae_VERB tipyn_NOUN o_ADP ffordd_NOUN i_ADP fynd_VERB eto_ADV cyn_CONJ fod_AUX Cymru_PROPN yn_PART cael_VERB ei_PRON weld_VERB i_ADP fod_VERB yn_PART leoliad_NOUN gwych_ADJ ar_ADP gyfer_NOUN busnesau_NOUN digidol_ADJ ._PUNCT
Er_ADP hyn_PRON mae_VERB llawer_NOUN o_ADP gymdeithasau_NOUN a_CONJ grwpiau_NOUN technegol_ADJ wedi_PART codi_VERB a_CONJ datblygu_VERB (_PUNCT e_PRON ._PUNCT e_PRON ._PUNCT Haciaith_PROPN ,_PUNCT Hedyn_PROPN a._PRON y._ADP y._CONJ b_PRON )_PUNCT sydd_AUX yn_PART clodfori_VERB y_DET Cymru_PROPN digidol_ADJ a_CONJ hybu_ADV cyfathrebu_VERB ac_CONJ ymwybyddiaeth_VERB rhwng_ADP blogwyr_NOUN Cymru_PROPN ._PUNCT
Blogiau_NOUN Cymraeg_PROPN
Ar_ADP restr_NOUN Hedyn_PROPN o_ADP flogiau_NOUN Cymru_PROPN -_PUNCT mae_AUX 'r_DET rhan_NOUN fwyaf_ADJ ohonyn_ADP nhw_PRON yn_PART defnyddio_VERB WordPress_X fel_ADP eu_DET system_NOUN rheoli_VERB cynnwys_NOUN -_PUNCT yn_PART debygol_ADJ oherwydd_ADP yr_DET hawster_NOUN a_CONJ chyflymder_NOUN cychwyn_VERB arnynt_ADP ._PUNCT
Mae_AUX 'r_DET rhan_NOUN fwyaf_ADJ wedi_PART mynd_VERB lawr_ADV y_DET llwybr_NOUN o_ADP wasanaeth_NOUN o_ADP WordPress_X wedi_PART ei_DET letya_VERB (_PUNCT ar_ADP WordPress_X ._PUNCT com_NOUN )_PUNCT yn_PART hytrach_ADV na_CONJ 'r_DET opsiwn_NOUN hunan-_ADP letya_VERB (_PUNCT WordPress_X ._PUNCT org_NOUN )_PUNCT ._PUNCT
Yn_PART ogystal_ADJ a_CONJ hyn_PRON ,_PUNCT mae_VERB llawer_NOUN o_ADP asiantaethau_VERB a_CONJ busnesau_NOUN dylunio_VERB a_CONJ datblygu_VERB gwe_NOUN (_PUNCT e_PRON ._PUNCT e_PRON ._PUNCT Gwe_NOUN Cambrian_NOUN Web_PROPN !_PUNCT )_PUNCT sydd_AUX yn_PART arbenigo_VERB mewn_ADP WordPress_X ar_ADP gyfer_NOUN eu_DET cleientiaid_NOUN ._PUNCT
Golyga_VERB hyn_PRON bod_VERB ystod_NOUN eang_ADJ o_ADP ddefnyddwyr_NOUN ,_PUNCT dylunwyr_NOUN a_CONJ datblygwyr_NOUN WordPress_X yng_ADP Nghymru_PROPN -_PUNCT ond_CONJ dim_DET modd_NOUN iddynt_ADP gyfathrebu_VERB a_CONJ rhannu_VERB newyddion_NOUN am_ADP y_DET meddalwedd_NOUN ._PUNCT
Felly_CONJ dyma_DET greu_VERB un_NUM !_PUNCT
Fel_ADP rhan_NOUN o_ADP Thema_PROPN ._PUNCT Cymru_PROPN -_PUNCT dyma_DET gymuned_NOUN WordPress_X Cymru_PROPN ar_ADP Facebook_PROPN -_PUNCT ymunwch_VERB nawr_ADV a_CONJ chyfrannwch_VERB i_PRON roi_VERB Cymru_PROPN ar_ADP fap_NOUN WordPress_X !_PUNCT
enw_NOUN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Cyhoeddwyd_VERB ar_ADP dyddiad_NOUN dyddiad_NOUN gan_ADP Thema_PROPN
Mae_AUX Thema_PROPN yn_PART cael_VERB ei_DET greu_VERB er_ADP mwyn_NOUN galluogi_VERB unigolion_NOUN i_PART greu_VERB gwefannau_NOUN amlieithog_ADJ eu_DET hunan_VERB ,_PUNCT gyda_ADP 'r_DET gobaith_NOUN o_ADP symleiddio_VERB cyhoeddi_VERB cynnwys_NOUN digidol_ADJ yn_ADP Gymraeg_PROPN ._PUNCT
Gall_VERB hefyd_ADV ,_PUNCT drwy_ADP gael_VERB ei_DET gyfieithu_VERB ,_PUNCT alluogi_VERB amlieithrwydd_ADJ tu_NOUN hwnt_ADV i_ADP 'r_DET deuawd_NOUN arferol_ADJ o_ADP Gymraeg_PROPN /_PUNCT Saesneg_PROPN -_PUNCT a_PART chael_VERB gwefannau_ADP Cymraeg_PROPN /_PUNCT Basgeg_PROPN ,_PUNCT Cymraeg_PROPN /_PUNCT Catalaneg_PROPN ,_PUNCT ag_CONJ yn_ADP y_DET blaen_NOUN ._PUNCT
Erbyn_ADP hyn_PRON rydw_AUX i_ADP wedi_PART rhoi_VERB 'r_DET elfen_NOUN sylfaenol_ADJ o_ADP ddwyieithrwydd_NOUN yn_PART rhan_NOUN o_ADP 'r_DET thema_NOUN ._PUNCT
Mae_AUX 'n_PART gweithio_VERB mewn_ADP ffordd_NOUN digon_ADV syml_ADJ -_PUNCT drwy_ADP orfodi_VERB llwytho_VERB yr_DET ategyn_NOUN Polylang_ADJ gan_ADP Chouby_PROPN -_PUNCT sydd_AUX yn_PART gwneud_VERB yr_DET holl_DET bethau_NOUN technegol_ADJ sydd_AUX yn_PART galluogi_VERB amlieithrwydd_ADV ._PUNCT
Bydd_AUX Thema_PROPN yn_PART gorfodi_VERB hyn_PRON wedi_PART iddo_ADP gael_VERB ei_PRON weithredu_VERB o_ADP fewn_ADP WordPress_X ._PUNCT
Yn_ADP Polylang_PROPN ,_PUNCT caiff_VERB y_DET tudalenau_NOUN /_PUNCT cofnodion_NOUN eu_DET "_PUNCT marcio_VERB "_PUNCT yn_ADP y_DET bas_NOUN data_NOUN gyda_ADP 'r_DET iaith_NOUN ,_PUNCT a_CONJ bydd_AUX y_DET defnyddwyr_NOUN yn_PART dynodi_VERB pa_PART dudalennau_NOUN eraill_ADJ sydd_AUX yn_PART cyfateb_VERB i_ADP rhain_PRON yn_ADP yr_DET ieithoedd_NOUN eraill_ADJ ._PUNCT
Dwi_AUX 'n_PART gobeithio_VERB i_ADP Thema_PROPN ychwanegu_VERB haen_NOUN o_ADP ddefnyddiadwyedd_NOUN wedi_PART ei_PRON seilio_VERB ar_ADP Polylang_PROPN fel_CONJ y_PART bydd_AUX yn_PART ei_PRON gwneud_VERB yn_PART llawer_ADV haws_ADJ i_PART reoli_VERB y_DET wefan_NOUN amlieithog_ADJ ._PUNCT
Ymysg_ADP y_DET defnyddiadwyedd_NOUN sydd_VERB yn_ADP fy_DET nghynllun_NOUN ,_PUNCT dyma_DET rai_PRON :_PUNCT
Gweld_VERB faint_NOUN o_ADP dudalenau_NOUN /_PUNCT cofnodion_NOUN sydd_VERB heb_ADP gael_VERB iaith_NOUN wedi_PART ei_DET osod_VERB
Gweld_VERB faint_NOUN o_ADP dudalennau_NOUN /_PUNCT cofnodion_NOUN sydd_VERB heb_ADP fersiwn_NOUN gyfarpar_ADJ yn_ADP yr_DET ieithoedd_NOUN arall_ADJ
Adio_VERB iaith_NOUN newydd_ADJ yn_PART hawdd_ADJ
A_PART oes_VERB unrhyw_DET ddefnyddiadwyedd_NOUN hoffech_VERB chi_PRON ei_PRON weld_VERB ?_PUNCT
Gadewch_VERB i_ADP mi_PRON wybod_VERB yn_ADP y_DET sylwadau_NOUN 🙂_SYM
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Cyhoeddwyd_VERB ar_ADP dyddiad_NOUN dyddiad_NOUN gan_ADP Thema_PROPN
Wel_INTJ -_PUNCT dyma_DET gychwyn_NOUN ar_ADP brosiect_NOUN hynod_ADJ o_ADP ddiddorol_ADJ arall_ADJ !_PUNCT
Yn_PART ddiweddar_ADJ rydw_AUX i_ADP wedi_PART bod_VERB yn_PART ddigon_ADV ffodus_ADJ i_PART ennill_VERB grant_NOUN gan_ADP Llywodraeth_NOUN Cymru_PROPN i_ADP ddatblygu_VERB fframwaith_NOUN thema_NOUN ar_ADP gyfer_NOUN WordPress_X sydd_AUX yn_PART galluogi_VERB defnyddwyr_NOUN i_ADP gyhoeddi_VERB cynnwys_NOUN yn_PART ddwyieithog_ADJ yn_PART hawdd_ADJ ,_PUNCT a_CONJ sydd_VERB yn_PART hygyrch_ADJ ._PUNCT
Bydd_AUX tri_NUM thema_NOUN yn_PART cael_VERB eu_DET creu_VERB yn_PART gyntaf_ADJ ,_PUNCT ar_ADP gyfer_NOUN :_PUNCT
Busnesau_NOUN /_PUNCT Elusennau_PROPN
Blogiau_NOUN personol_ADJ
Gwefannau_VERB e_PRON -_PUNCT werthiant_NOUN
Y_DET gobaith_NOUN (_PUNCT or-_CONJ obeithiol_ADJ ,_PUNCT efallai_ADV ?_PUNCT )_PUNCT yw_VERB i_PART ddogfennu_VERB sut_ADV y_PART mae_AUX 'r_DET prosiect_NOUN yn_PART dod_VERB yn_ADP ei_DET flaen_NOUN drwy_ADP gyfres_NOUN o_ADP flogiau_NOUN byr_ADJ fel_ADP hyn_PRON ._PUNCT
Y_DET Pwrpas_PROPN
Pwrpas_VERB y_DET prosiect_NOUN o_ADP fy_DET ochor_NOUN i_PRON yw_VERB i_PART allu_VERB rhoi_VERB y_DET gallu_VERB i_ADP gyhoeddi_VERB yn_PART aml_ADJ -_PUNCT ieithol_ADJ i_ADP unrhyw_DET un_NUM ,_PUNCT a_CONJ gwneud_VERB yn_PART siwr_ADJ fod_AUX y_DET cynnwys_NOUN hynny_PRON yn_PART hygyrch_ADJ (_PUNCT gellir_VERB ei_PRON defnyddio_VERB yn_PART rhwydd_NOUN er_ADP unrhyw_DET anabledd_NOUN )_PUNCT ._PUNCT
Mae_AUX 'n_PART gyfle_NOUN felly_ADV i_ADP fusnes_NOUN ,_PUNCT elusen_NOUN ,_PUNCT siop_NOUN ar_ADP y_DET we_NOUN ,_PUNCT unigolion_NOUN a_CONJ blogiau_ADV allu_ADP cael_VERB dyluniad_NOUN gwefan_NOUN fodern_ADJ sydd_AUX yn_PART rhoi_VERB y_PART gallu_VERB o_ADP gyhoeddi_VERB yn_PART ddwyieithog_ADJ yn_PART greiddiol_ADJ yn_ADP eu_DET gwefan_NOUN ._PUNCT
Rydw_AUX i_PRON yn_PART rhedeg_VERB busnes_NOUN dylunio_VERB a_CONJ datblygu_VERB gwe_NOUN o_ADP 'r_DET enw_NOUN Gwe_NOUN Cambrian_NOUN Web_PROPN -_PUNCT ag_ADP ein_DET amcan_NOUN busnes_NOUN yw_VERB "_PUNCT Cael_VERB Cymru_PROPN a_CONJ 'r_DET Gymraeg_PROPN arlein_NOUN "_PUNCT -_PUNCT rydw_VERB i_ADP yn_PART frwdfrydig_ADJ iawn_ADV dros_ADP allu_NOUN cynyddu_VERB faint_NOUN o_ADP gynnwys_VERB Cymraeg_PROPN sydd_VERB arlein_PROPN -_PUNCT yn_PART enwedig_ADJ y_DET cynnwys_NOUN a_CONJ ellir_VERB ei_PRON chwilio_VERB drwy_ADP beiriannau_NOUN gwe_NOUN megis_ADP Google_PROPN ._PUNCT
O_ADP ran_NOUN y_DET Llywodraeth_NOUN ,_PUNCT eu_DET hamcanion_NOUN ar_ADP gyfer_NOUN y_DET prosiect_NOUN yw_VERB i_PART gael_VERB mwy_PRON o_ADP Gymraeg_PROPN arlein_NOUN ,_PUNCT amcan_NOUN y_PART rydan_VERB ni_PRON yn_PART frwdfrydig_ADJ amdano_ADP hefyd_ADV ._PUNCT
Ffynhonnell_PROPN Agored_PROPN
Rydw_AUX i_ADP wedi_PART penderfynu_VERB ei_DET agor_VERB i_ADP 'r_DET gymuned_NOUN fel_ADP prosiect_NOUN agored_ADJ /_PUNCT open_NOUN -_PUNCT source_NOUN ._PUNCT
Golyga_VERB hyn_PRON wrth_ADP gwrs_NOUN y_PART bydd_VERB ar_ADP gael_VERB am_ADP ddim_PRON i_ADP unrhyw_DET un_NUM ei_PRON ddefnyddio_VERB unwaith_ADV y_PART bydd_VERB gofynnion_NOUN y_DET grant_NOUN wedi_PART eu_DET cyflawni_NOUN ,_PUNCT ond_CONJ dwi_VERB yn_PART wir_ADJ gobeithio_VERB gallu_VERB creu_VERB cymuned_NOUN o_ADP ddatblygwyr_NOUN gwe_NOUN WordPress_X Cymraeg_PROPN sydd_VERB yn_PART frwdfrydig_ADJ dros_ADP ddefnyddio_VERB 'r_DET dechnoleg_NOUN yma_DET yng_ADP Nghymru_PROPN ar_ADP gyfer_NOUN hybu_VERB Cymru_PROPN a_CONJ 'r_DET Gymraeg_PROPN arlein_NOUN ._PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Cychwyn_NOUN Cymdeithas_NOUN WordPress_X
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Cyhoeddwyd_VERB ar_ADP dyddiad_NOUN dyddiad_NOUN gan_ADP Thema_PROPN
Lawnsio_VERB WordPress_X Cymru_PROPN !_PUNCT
Yn_PART ddiweddar_ADJ mae_AUX Cymru_PROPN wedi_PART profi_VERB rhyw_PRON chwildro_NOUN dechnegol_ADJ reit_NOUN sylweddol_ADJ -_PUNCT ond_CONJ wrth_ADP gwrs_NOUN mae_VERB tipyn_NOUN o_ADP ffordd_NOUN i_ADP fynd_VERB eto_ADV cyn_CONJ fod_AUX Cymru_PROPN yn_PART cael_VERB ei_PRON weld_VERB i_ADP fod_VERB yn_PART leoliad_NOUN gwych_ADJ ar_ADP gyfer_NOUN busnesau_NOUN digidol_ADJ ._PUNCT
Er_ADP hyn_PRON mae_VERB llawer_NOUN o_ADP gymdeithasau_NOUN a_CONJ grwpiau_NOUN technegol_ADJ wedi_PART codi_VERB a_CONJ datblygu_VERB (_PUNCT e_PRON ._PUNCT e_PRON ._PUNCT Haciaith_PROPN ,_PUNCT Hedyn_PROPN a._PRON y._ADP y._CONJ b_PRON )_PUNCT sydd_AUX yn_PART clodfori_VERB y_DET Cymru_PROPN digidol_ADJ a_CONJ hybu_ADV cyfathrebu_VERB ac_CONJ ymwybyddiaeth_VERB rhwng_ADP blogwyr_NOUN Cymru_PROPN ._PUNCT
Blogiau_NOUN Cymraeg_PROPN
Ar_ADP restr_NOUN Hedyn_PROPN o_ADP flogiau_NOUN Cymru_PROPN -_PUNCT mae_AUX 'r_DET rhan_NOUN fwyaf_ADJ ohonyn_ADP nhw_PRON yn_PART defnyddio_VERB WordPress_X fel_ADP eu_DET system_NOUN rheoli_VERB cynnwys_NOUN -_PUNCT yn_PART debygol_ADJ oherwydd_ADP yr_DET hawster_NOUN a_CONJ chyflymder_NOUN cychwyn_VERB arnynt_ADP ._PUNCT
Mae_AUX 'r_DET rhan_NOUN fwyaf_ADJ wedi_PART mynd_VERB lawr_ADV y_DET llwybr_NOUN o_ADP wasanaeth_NOUN o_ADP WordPress_X wedi_PART ei_DET letya_VERB (_PUNCT ar_ADP WordPress_X ._PUNCT com_NOUN )_PUNCT yn_PART hytrach_ADV na_CONJ 'r_DET opsiwn_NOUN hunan-_ADP letya_VERB (_PUNCT WordPress_X ._PUNCT org_NOUN )_PUNCT ._PUNCT
Yn_PART ogystal_ADJ a_CONJ hyn_PRON ,_PUNCT mae_VERB llawer_NOUN o_ADP asiantaethau_VERB a_CONJ busnesau_NOUN dylunio_VERB a_CONJ datblygu_VERB gwe_NOUN (_PUNCT e_PRON ._PUNCT e_PRON ._PUNCT Gwe_NOUN Cambrian_NOUN Web_PROPN !_PUNCT )_PUNCT sydd_AUX yn_PART arbenigo_VERB mewn_ADP WordPress_X ar_ADP gyfer_NOUN eu_DET cleientiaid_NOUN ._PUNCT
Golyga_VERB hyn_PRON bod_VERB ystod_NOUN eang_ADJ o_ADP ddefnyddwyr_NOUN ,_PUNCT dylunwyr_NOUN a_CONJ datblygwyr_NOUN WordPress_X yng_ADP Nghymru_PROPN -_PUNCT ond_CONJ dim_DET modd_NOUN iddynt_ADP gyfathrebu_VERB a_CONJ rhannu_VERB newyddion_NOUN am_ADP y_DET meddalwedd_NOUN ._PUNCT
Felly_CONJ dyma_DET greu_VERB un_NUM !_PUNCT
Fel_ADP rhan_NOUN o_ADP Thema_PROPN ._PUNCT Cymru_PROPN -_PUNCT dyma_DET gymuned_NOUN WordPress_X Cymru_PROPN ar_ADP Facebook_PROPN -_PUNCT ymunwch_VERB nawr_ADV a_CONJ chyfrannwch_VERB i_PRON roi_VERB Cymru_PROPN ar_ADP fap_NOUN WordPress_X !_PUNCT
enw_NOUN
Gadael_VERB Ymateb_NOUN Diddymu_PROPN ymateb_VERB
Ni_PART fydd_AUX eich_DET cyfeiriad_NOUN e_PRON -_PUNCT bost_NOUN yn_PART cael_VERB ei_DET gyhoeddi_VERB ._PUNCT
Mae_AUX 'r_DET meysydd_NOUN gofynnol_ADJ yn_PART cael_VERB eu_DET marcio_VERB *_SYM
Sylw_NOUN
Enw_NOUN *_SYM
E_NUM -_PUNCT bost_NOUN *_SYM
Gwefan_PROPN
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Cysylltu_VERB a_CONJ 'r_DET tim_NOUN tu_NOUN ôl_NOUN i_ADP Thema_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Cysylltwch_PROPN
Rhywbeth_NOUN i_ADP ddweud_VERB /_PUNCT ofyn_VERB ?_PUNCT
Gyrrwch_VERB neges_NOUN :_PUNCT
Eich_DET Enw_PROPN (_PUNCT angen_NOUN )_PUNCT
Eich_DET Ebost_PROPN (_PUNCT angen_NOUN )_PUNCT
Eich_DET Neges_NOUN (_PUNCT angen_NOUN )_PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Datblygwyr_NOUN WordPress_X yng_ADP Nghymru_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Datblygwyr_NOUN
Rydym_AUX yn_PART chwilio_VERB am_ADP datblygwyr_NOUN WordPress_X yng_ADP Nghymru_PROPN ._PUNCT
Ymunwch_VERB yn_ADP y_DET grwp_NOUN Facebook_PROPN yma_ADV
Ymunwch_VERB yn_ADP y_DET grwp_NOUN LinkedIn_NOUN yma_ADV
Mae_VERB Thema_PROPN yn_PART fframwaith_NOUN agored_ADJ gyda_ADP thrywdded_VERB GPL_PROPN v_ADP ._PUNCT 2_NUM sydd_AUX yn_PART golygu_VERB y_DET gallech_VERB gymeryd_NOUN y_DET cod_NOUN gwereiddiol_ADJ a_CONJ 'i_PRON newid_VERB ar_ADP gyfer_NOUN eich_DET anghenion_NOUN ._PUNCT
Fodd_NOUN bynnag_PRON ,_PUNCT fy_DET amcan_NOUN yw_VERB i_PRON gael_VERB dylunwyr_NOUN a_CONJ datblygwyr_NOUN i_ADP ddatblygu_VERB themau_NOUN drwy_ADP css_NOUN a_CONJ fydd_AUX yn_PART golygu_VERB bydd_VERB llawer_ADV "_PUNCT blas_NOUN "_PUNCT ar_ADP Thema_PROPN ar_ADP gyfer_NOUN gwahanol_ADJ amgylchiadau_NOUN (_PUNCT e_PRON ._PUNCT e_DET busnesau_NOUN ,_PUNCT blogio_VERB ,_PUNCT e_PRON -_PUNCT werthiant_NOUN a._PRON y._PART y._CONJ b_PRON )_PUNCT ._PUNCT
Os_CONJ oes_VERB diddordeb_NOUN gennych_ADP mewn_ADP gwybod_VERB mwy_ADV am_ADP ddatblygu_VERB gyda_ADP thema_NOUN ,_PUNCT cysylltwch_VERB a_CONJ ni_PRON !_PUNCT
Y_DET peth_NOUN olaf_ADJ hoffwn_VERB weld_VERB byddai_VERB yr_DET un_NUM thema_NOUN yn_PART cael_VERB ei_PRON defnyddio_VERB ymhob_ADP gwefan_NOUN ddwyieithog_ADJ yng_ADP Nghymru_PROPN ,_PUNCT felly_CONJ y_DET mwyaf_NOUN o_ADP ddyluniadau_NOUN a_CONJ gellir_VERB ei_PRON gwneud_VERB y_DET gorau_ADJ !_PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Adnoddau_NOUN ar_ADP gyfer_NOUN dylunwyr_NOUN WordPress_X Cymru_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Dylunwyr_NOUN WordPress_X yng_ADP Nghymru_PROPN
A_PART ydych_VERB chi_PRON yn_PART ddyluniwr_NOUN gwe_NOUN yng_ADP Nghymru_PROPN sydd_VERB a_CONJ diddordeb_NOUN mewn_ADP dylunio_VERB templedi_NOUN WordPress_X ar_ADP gyfer_NOUN Thema_PROPN ?_PUNCT
Rydym_AUX yn_PART gobeithio_ADV gallu_VERB cymeryd_VERB cynnigion_NOUN ar_ADP gyfer_NOUN dyluniadau_NOUN ar_ADP ffurf_NOUN Photoshop_PROPN a_CONJ cael_VERB y_DET gymuned_NOUN o_ADP ddatblygwyr_NOUN i_ADP 'w_PRON godio_VERB i_ADP greu_VERB thema_NOUN WordPress_X ._PUNCT
Os_CONJ oes_VERB diddordeb_NOUN gennych_ADP mewn_ADP hyn_PRON ,_PUNCT cysylltwch_VERB a_CONJ ni_PRON !_PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_PROPN
Multilingual_PROPN ._PUNCT
Responsive_INTJ ._PUNCT
Accessible_INTJ ._PUNCT
Simple_PROPN ._PUNCT
Cymraeg_NOUN
Load_NOUN ._PUNCT
._PUNCT ._PUNCT
Apply_PROPN ._PUNCT
._PUNCT ._PUNCT
Succeed_INTJ ._PUNCT
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Lawrlwytho_VERB -_PUNCT Thema_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Lawrlwytho_PROPN
Lawrlwythwch_VERB Thema_PROPN Nawr_ADV !_PUNCT
Eich_DET Enw_PROPN (_PUNCT angen_NOUN )_PUNCT
Eich_DET Ebost_PROPN (_PUNCT angen_NOUN )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN
Lawrlwythwch_VERB y_DET thema_NOUN blentyn_NOUN ar_ADP gyfer_NOUN gwefannau_NOUN blog_ADJ ._PUNCT
Thema_VERB :_PUNCT Blog_PROPN
Thema_VERB :_PUNCT Siop_PROPN
Lawrlwythwch_VERB y_DET thema_NOUN blentyn_NOUN ar_ADP gyfer_NOUN gwefannau_VERB e_PRON -_PUNCT werthiant_NOUN ._PUNCT
Thema_VERB :_PUNCT Siop_PROPN
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_ADP Greiddiol_ADJ
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Cyhoeddwyd_VERB ar_ADP dyddiad_NOUN dyddiad_NOUN gan_ADP Thema_PROPN
Mae_AUX Thema_PROPN yn_PART cael_VERB ei_DET greu_VERB er_ADP mwyn_NOUN galluogi_VERB unigolion_NOUN i_PART greu_VERB gwefannau_NOUN amlieithog_ADJ eu_DET hunan_VERB ,_PUNCT gyda_ADP 'r_DET gobaith_NOUN o_ADP symleiddio_VERB cyhoeddi_VERB cynnwys_NOUN digidol_ADJ yn_ADP Gymraeg_PROPN ._PUNCT
Gall_VERB hefyd_ADV ,_PUNCT drwy_ADP gael_VERB ei_DET gyfieithu_VERB ,_PUNCT alluogi_VERB amlieithrwydd_ADJ tu_NOUN hwnt_ADV i_ADP 'r_DET deuawd_NOUN arferol_ADJ o_ADP Gymraeg_PROPN /_PUNCT Saesneg_PROPN -_PUNCT a_PART chael_VERB gwefannau_ADP Cymraeg_PROPN /_PUNCT Basgeg_PROPN ,_PUNCT Cymraeg_PROPN /_PUNCT Catalaneg_PROPN ,_PUNCT ag_CONJ yn_ADP y_DET blaen_NOUN ._PUNCT
Erbyn_ADP hyn_PRON rydw_AUX i_ADP wedi_PART rhoi_VERB 'r_DET elfen_NOUN sylfaenol_ADJ o_ADP ddwyieithrwydd_NOUN yn_PART rhan_NOUN o_ADP 'r_DET thema_NOUN ._PUNCT
Mae_AUX 'n_PART gweithio_VERB mewn_ADP ffordd_NOUN digon_ADV syml_ADJ -_PUNCT drwy_ADP orfodi_VERB llwytho_VERB yr_DET ategyn_NOUN Polylang_ADJ gan_ADP Chouby_PROPN -_PUNCT sydd_AUX yn_PART gwneud_VERB yr_DET holl_DET bethau_NOUN technegol_ADJ sydd_AUX yn_PART galluogi_VERB amlieithrwydd_ADV ._PUNCT
Bydd_AUX Thema_PROPN yn_PART gorfodi_VERB hyn_PRON wedi_PART iddo_ADP gael_VERB ei_PRON weithredu_VERB o_ADP fewn_ADP WordPress_X ._PUNCT
Yn_ADP Polylang_PROPN ,_PUNCT caiff_VERB y_DET tudalenau_NOUN /_PUNCT cofnodion_NOUN eu_DET "_PUNCT marcio_VERB "_PUNCT yn_ADP y_DET bas_NOUN data_NOUN gyda_ADP 'r_DET iaith_NOUN ,_PUNCT a_CONJ bydd_AUX y_DET defnyddwyr_NOUN yn_PART dynodi_VERB pa_PART dudalennau_NOUN eraill_ADJ sydd_AUX yn_PART cyfateb_VERB i_ADP rhain_PRON yn_ADP yr_DET ieithoedd_NOUN eraill_ADJ ._PUNCT
Dwi_AUX 'n_PART gobeithio_VERB i_ADP Thema_PROPN ychwanegu_VERB haen_NOUN o_ADP ddefnyddiadwyedd_NOUN wedi_PART ei_PRON seilio_VERB ar_ADP Polylang_PROPN fel_CONJ y_PART bydd_AUX yn_PART ei_PRON gwneud_VERB yn_PART llawer_ADV haws_ADJ i_PART reoli_VERB y_DET wefan_NOUN amlieithog_ADJ ._PUNCT
Ymysg_ADP y_DET defnyddiadwyedd_NOUN sydd_VERB yn_ADP fy_DET nghynllun_NOUN ,_PUNCT dyma_DET rai_PRON :_PUNCT
Gweld_VERB faint_NOUN o_ADP dudalenau_NOUN /_PUNCT cofnodion_NOUN sydd_VERB heb_ADP gael_VERB iaith_NOUN wedi_PART ei_DET osod_VERB
Gweld_VERB faint_NOUN o_ADP dudalennau_NOUN /_PUNCT cofnodion_NOUN sydd_VERB heb_ADP fersiwn_NOUN gyfarpar_ADJ yn_ADP yr_DET ieithoedd_NOUN arall_ADJ
Adio_VERB iaith_NOUN newydd_ADJ yn_PART hawdd_ADJ
A_PART oes_VERB unrhyw_DET ddefnyddiadwyedd_NOUN hoffech_VERB chi_PRON ei_PRON weld_VERB ?_PUNCT
Gadewch_VERB i_ADP mi_PRON wybod_VERB yn_ADP y_DET sylwadau_NOUN 🙂_SYM
Gadael_VERB Ymateb_NOUN Diddymu_PROPN ymateb_VERB
Ni_PART fydd_AUX eich_DET cyfeiriad_NOUN e_PRON -_PUNCT bost_NOUN yn_PART cael_VERB ei_DET gyhoeddi_VERB ._PUNCT
Mae_AUX 'r_DET meysydd_NOUN gofynnol_ADJ yn_PART cael_VERB eu_DET marcio_VERB *_SYM
Sylw_NOUN
Enw_NOUN *_SYM
E_NUM -_PUNCT bost_NOUN *_SYM
Gwefan_PROPN
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_NOUN -_PUNCT thema_NOUN Amlieithog_PROPN WordPress_X ar_ADP gyfer_NOUN Cymru_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Thema_NOUN Amlieithog_ADJ ar_ADP gyfer_NOUN WordPress_X
Ar_ADP Gael_VERB Nawr_ADV -_PUNCT Lawrlwythwch_PROPN Yma_ADV !_PUNCT
Llwytho_VERB ._PUNCT
._PUNCT ._PUNCT
Gweithredu_NOUN ._PUNCT
._PUNCT ._PUNCT
Llwyddo_VERB ._PUNCT
Thema_NOUN Gymraeg_PROPN sydd_AUX yn_PART galluogi_VERB aml_ADJ -_PUNCT ieithrwydd_NOUN yn_PART hawdd_ADJ yw_VERB Thema_PROPN ._PUNCT
Mae_VERB yn_PART hawdd_ADJ i_ADP 'w_PRON lwytho_VERB ar_ADP unrhyw_DET wefan_NOUN WordPress_X ag_CONJ yn_PART cynnwys_VERB llwyth_PRON o_ADP newidiadau_NOUN sydd_VERB yn_PART eich_PRON galluogi_VERB i_ADP reoli_VERB a_CONJ rhedeg_VERB eich_DET gwefan_NOUN amlieithog_ADJ yn_PART hawdd_ADJ ._PUNCT
Nodweddion_PROPN
Amlieithog_ADJ allan_ADV o_ADP 'r_DET bocs_NOUN -_PUNCT Gweithredwch_PROPN a_CONJ dyna_ADV ni_PRON !_PUNCT
Personoli_VERB -_PUNCT ychwanegwch_VERB eich_DET logo_VERB a_CONJ newidiwch_VERB y_DET lliwiau_NOUN
Panel_NOUN oruwchwylio_VERB -_PUNCT gweld_VERB ystadegau_NOUN am_ADP eich_DET gwefan_NOUN o_ADP 'r_DET bwrdd_NOUN cyfenw_NOUN
Llawer_ADV mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
Mae_VERB yn_PART gôd_ADV agored_ADJ sydd_AUX yn_PART golygu_VERB gall_VERB unrhyw_DET un_NUM ei_DET gymeryd_NOUN a_CONJ 'i_PRON addasu_VERB i_ADP 'w_PRON anghenion_NOUN ._PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_NOUN -_PUNCT thema_NOUN Amlieithog_PROPN WordPress_X ar_ADP gyfer_NOUN Cymru_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Thema_NOUN Amlieithog_ADJ ar_ADP gyfer_NOUN WordPress_X
Ar_ADP Gael_VERB Nawr_ADV -_PUNCT Lawrlwythwch_PROPN Yma_ADV !_PUNCT
Llwytho_VERB ._PUNCT
._PUNCT ._PUNCT
Gweithredu_NOUN ._PUNCT
._PUNCT ._PUNCT
Llwyddo_VERB ._PUNCT
Thema_NOUN Gymraeg_PROPN sydd_AUX yn_PART galluogi_VERB aml_ADJ -_PUNCT ieithrwydd_NOUN yn_PART hawdd_ADJ yw_VERB Thema_PROPN ._PUNCT
Mae_VERB yn_PART hawdd_ADJ i_ADP 'w_PRON lwytho_VERB ar_ADP unrhyw_DET wefan_NOUN WordPress_X ag_CONJ yn_PART cynnwys_VERB llwyth_PRON o_ADP newidiadau_NOUN sydd_VERB yn_PART eich_PRON galluogi_VERB i_ADP reoli_VERB a_CONJ rhedeg_VERB eich_DET gwefan_NOUN amlieithog_ADJ yn_PART hawdd_ADJ ._PUNCT
Nodweddion_PROPN
Amlieithog_ADJ allan_ADV o_ADP 'r_DET bocs_NOUN -_PUNCT Gweithredwch_PROPN a_CONJ dyna_ADV ni_PRON !_PUNCT
Personoli_VERB -_PUNCT ychwanegwch_VERB eich_DET logo_VERB a_CONJ newidiwch_VERB y_DET lliwiau_NOUN
Panel_NOUN oruwchwylio_VERB -_PUNCT gweld_VERB ystadegau_NOUN am_ADP eich_DET gwefan_NOUN o_ADP 'r_DET bwrdd_NOUN cyfenw_NOUN
Llawer_ADV mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
Mae_VERB yn_PART gôd_ADV agored_ADJ sydd_AUX yn_PART golygu_VERB gall_VERB unrhyw_DET un_NUM ei_DET gymeryd_NOUN a_CONJ 'i_PRON addasu_VERB i_ADP 'w_PRON anghenion_NOUN ._PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_NOUN -_PUNCT thema_NOUN Amlieithog_PROPN WordPress_X ar_ADP gyfer_NOUN Cymru_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Thema_NOUN Amlieithog_ADJ ar_ADP gyfer_NOUN WordPress_X
Ar_ADP Gael_VERB Nawr_ADV -_PUNCT Lawrlwythwch_PROPN Yma_ADV !_PUNCT
Llwytho_VERB ._PUNCT
._PUNCT ._PUNCT
Gweithredu_NOUN ._PUNCT
._PUNCT ._PUNCT
Llwyddo_VERB ._PUNCT
Thema_NOUN Gymraeg_PROPN sydd_AUX yn_PART galluogi_VERB aml_ADJ -_PUNCT ieithrwydd_NOUN yn_PART hawdd_ADJ yw_VERB Thema_PROPN ._PUNCT
Mae_VERB yn_PART hawdd_ADJ i_ADP 'w_PRON lwytho_VERB ar_ADP unrhyw_DET wefan_NOUN WordPress_X ag_CONJ yn_PART cynnwys_VERB llwyth_PRON o_ADP newidiadau_NOUN sydd_VERB yn_PART eich_PRON galluogi_VERB i_ADP reoli_VERB a_CONJ rhedeg_VERB eich_DET gwefan_NOUN amlieithog_ADJ yn_PART hawdd_ADJ ._PUNCT
Nodweddion_PROPN
Amlieithog_ADJ allan_ADV o_ADP 'r_DET bocs_NOUN -_PUNCT Gweithredwch_PROPN a_CONJ dyna_ADV ni_PRON !_PUNCT
Personoli_VERB -_PUNCT ychwanegwch_VERB eich_DET logo_VERB a_CONJ newidiwch_VERB y_DET lliwiau_NOUN
Panel_NOUN oruwchwylio_VERB -_PUNCT gweld_VERB ystadegau_NOUN am_ADP eich_DET gwefan_NOUN o_ADP 'r_DET bwrdd_NOUN cyfenw_NOUN
Llawer_ADV mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
Mae_VERB yn_PART gôd_ADV agored_ADJ sydd_AUX yn_PART golygu_VERB gall_VERB unrhyw_DET un_NUM ei_DET gymeryd_NOUN a_CONJ 'i_PRON addasu_VERB i_ADP 'w_PRON anghenion_NOUN ._PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_NOUN -_PUNCT thema_NOUN Amlieithog_PROPN WordPress_X ar_ADP gyfer_NOUN Cymru_PROPN
Neidio_VERB i_ADP 'r_DET cynnwys_NOUN
Thema_PROPN
Amlieithog_ADJ ._PUNCT
Ymatebol_PROPN ._PUNCT
Hygyrch_PROPN ._PUNCT
Hawdd_PROPN
Cymraeg_NOUN
Thema_NOUN Amlieithog_ADJ ar_ADP gyfer_NOUN WordPress_X
Ar_ADP Gael_VERB Nawr_ADV -_PUNCT Lawrlwythwch_PROPN Yma_ADV !_PUNCT
Llwytho_VERB ._PUNCT
._PUNCT ._PUNCT
Gweithredu_NOUN ._PUNCT
._PUNCT ._PUNCT
Llwyddo_VERB ._PUNCT
Thema_NOUN Gymraeg_PROPN sydd_AUX yn_PART galluogi_VERB aml_ADJ -_PUNCT ieithrwydd_NOUN yn_PART hawdd_ADJ yw_VERB Thema_PROPN ._PUNCT
Mae_VERB yn_PART hawdd_ADJ i_ADP 'w_PRON lwytho_VERB ar_ADP unrhyw_DET wefan_NOUN WordPress_X ag_CONJ yn_PART cynnwys_VERB llwyth_PRON o_ADP newidiadau_NOUN sydd_VERB yn_PART eich_PRON galluogi_VERB i_ADP reoli_VERB a_CONJ rhedeg_VERB eich_DET gwefan_NOUN amlieithog_ADJ yn_PART hawdd_ADJ ._PUNCT
Nodweddion_PROPN
Amlieithog_ADJ allan_ADV o_ADP 'r_DET bocs_NOUN -_PUNCT Gweithredwch_PROPN a_CONJ dyna_ADV ni_PRON !_PUNCT
Personoli_VERB -_PUNCT ychwanegwch_VERB eich_DET logo_VERB a_CONJ newidiwch_VERB y_DET lliwiau_NOUN
Panel_NOUN oruwchwylio_VERB -_PUNCT gweld_VERB ystadegau_NOUN am_ADP eich_DET gwefan_NOUN o_ADP 'r_DET bwrdd_NOUN cyfenw_NOUN
Llawer_ADV mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
Mae_VERB yn_PART gôd_ADV agored_ADJ sydd_AUX yn_PART golygu_VERB gall_VERB unrhyw_DET un_NUM ei_DET gymeryd_NOUN a_CONJ 'i_PRON addasu_VERB i_ADP 'w_PRON anghenion_NOUN ._PUNCT
Cofnodion_NOUN Diweddar_PROPN
Cychwyn_VERB Cymdeithas_NOUN WordPress_X Cymru_PROPN
Rhoi_VERB 'r_DET amlieithrwydd_NOUN yn_PART greiddiol_ADJ …_PUNCT
Prosiect_NOUN Thema_NOUN Amlieithog_ADJ i_ADP Gymru_PROPN
Newidiwr_NOUN Themâu_PROPN
Thema_NOUN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Blog_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
Thema_VERB :_PUNCT Siop_PROPN (_PUNCT 1.0_NUM ._PUNCT 0_NUM )_PUNCT
