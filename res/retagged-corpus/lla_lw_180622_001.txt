Ma_INTJ '_PUNCT hwn_PRON fel_ADP yr_DET ym_ADP ._PUNCT
Fatha_NOUN heno_ADV oedd_VERB ?_PUNCT
Fatha_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Wyt_AUX ti_PRON 'n_PART mynd_VERB i_ADP 'r_DET parc_NOUN heno_ADV wyt_VERB [_PUNCT -_PUNCT ]_PUNCT i_ADP 'r_DET  _SPACE peth_NOUN miwsig_NOUN 'ma_ADV yn_ADP y_DET parc_NOUN ?_PUNCT
Yndw_VERB fel_ADP fi_PRON yn_PART [_PUNCT -_PUNCT ]_PUNCT fel_ADP fi_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT BeBe_NOUN [_PUNCT /=_PROPN ]_PUNCT bebe_NOUN sy_VERB 'n_PART mlaen_NOUN wyt_AUX ti_PRON 'n_PART gwybod_VERB pa_PART grŵpiau_NOUN sy_VERB mlaen_NOUN ?_PUNCT
na'dw_NOUN dwi_VERB 'm_DET yn_PART siŵr_ADJ actually_ADV [_PUNCT saib_NOUN ]_PUNCT  _SPACE [_PUNCT saib_NOUN ]_PUNCT mae_VERB 'n_PART siŵr_ADJ fel_ADP rhyw_DET bandiau_NOUN lleol_ADJ ._PUNCT
Ie_INTJ ?_PUNCT
Ie_INTJ ond_CONJ dwi_VERB 'm_DET yn_PART siŵr_ADJ bebe_NOUN '_PUNCT dy_DET 'r_DET line_NOUN up_VERB yn_PART union_ADJ ._PUNCT
Ô_NUM reit_INTJ ._PUNCT
Cwbwl_PROPN dwi_AUX 'n_PART gwybod_VERB ydy_VERB bod_VERB o_PRON 'n_PART [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ô_NUM faint_NOUN mama_NOUN [_PUNCT -_PUNCT ]_PUNCT faint_ADV o_ADP 'r_DET gloch_NOUN mae_AUX 'n_PART dechrau_VERB ?_PUNCT
saith_VERB o_ADP 'r_DET glôch_NOUN tan_ADP naw_NUM [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ô_NUM reit_INTJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ydy_VERB [_PUNCT /=_PROPN ]_PUNCT ydy_VERB dy_DET ffrindiau_NOUN di_PRON 'n_PART mynd_VERB ?_PUNCT
Yn_PART [_PUNCT -_PUNCT ]_PUNCT yndyn_NOUN '_PUNCT dan_ADP ni_PRON 'n_PART mynd_VERB [_PUNCT -_PUNCT ]_PUNCT mama_NOUN rhyw_DET grŵp_NOUN o_ADP [_PUNCT -_PUNCT ]_PUNCT criw_NOUN ohonon_ADP ni_PRON 'n_PART mynd_VERB  _SPACE fi_PRON enwb1_SYM enwb2_NOUN a_CONJ wedyn_ADV  _SPACE grŵp_NOUN o_ADP ffrindiau_NOUN __NOUN hefyd_ADV ._PUNCT
Ô_NUM reit_NUM reit_NOUN neis_ADJ ti_PRON 'n_PART cofio_VERB ni_PRON 'n_PART mynd_VERB i_PART weld_VERB y_PART enwg1_VERB yna_ADV ?_PUNCT
Yn_PART ysgol_NOUN ?_PUNCT
Pan_CONJ o_ADP '_PUNCT chi_PRON 'n_PART fach_ADJ ._PUNCT
Yndw_INTJ ?_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Wel_CONJ mae_VERB gennai_VERB rhyw_DET go_NOUN '_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Oedd_VERB hynna_PRON wastad_ADV yn_PART boblogaidd_ADJ yn_PART doedd_VERB ?_PUNCT
Oedd_VERB oedden_ADP nhw_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT '_PUNCT dy_DET nhw_PRON dal_ADV yn_PART neud_VERB hynna_PRON ?_PUNCT
Na_INTJ dwi_AUX 'm_DET yn_PART meddwl_VERB dwi_AUX 'm_DET yn_PART meddwl_VERB bobo_VERB fi_PRON 'di_PART gweld_VERB o_ADP yn_PART dod_VERB yn_PART diweddar_ADJ  _SPACE dwi_AUX 'n_PART meddwl_VERB bobo_VERB nhw_PRON 'di_PART bod_VERB yn_PART lleoliad_NOUN +_SYM
Ô_NUM fel_ADP  _SPACE jam_NOUN [_PUNCT -_PUNCT ]_PUNCT
+_VERB Yn_PART diweddar_ADJ ie_INTJ fel_ADP jambori_NOUN ond_CONJ na_ADV ddim_PART yn_ADP y_DET parc_NOUN o'dd_NOUN hynna_PRON 'n_PART reit_NOUN da_ADJ i_PART deud_VERB y_DET gwir_NOUN pan_CONJ o'dd_NOUN yn_PART canu_VERB yn_ADP y_DET parc_NOUN o_ADP 'n_PART i_PRON 'n_PART licio_VERB hwnna_PRON ._PUNCT
Oedd_VERB ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Oedd_VERB 'na_ADV dipyn_PRON o_ADP bobol_NOUN yn_PART mynd_VERB ?_PUNCT
Oedd_VERB o'dd_NOUN o_PRON 'n_PART boblogaidd_ADJ iawn_ADV m_PROPN -_PUNCT hm_NOUN ._PUNCT
Ô_NUM +_SYM
Oedd_VERB ._PUNCT
+_VERB Ie_INTJ o_PRON 'n_PART i_PRON 'n_PART fach_ADJ ._PUNCT
Ie_INTJ ._PUNCT
Iawn_INTJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT Oedd_VERB [_PUNCT /=_PROPN ]_PUNCT oedd_VERB o_PRON '_PUNCT ti_PRON yn_PART fach_ADJ oedd_VERB [_PUNCT saib_NOUN ]_PUNCT oedd_VERB ._PUNCT
So_ADP sut_ADV ddiwrnod_NOUN wyt_AUX ti_PRON wedi_PART gael_VERB wel_ADP bore_NOUN 'ma_ADV ?_PUNCT
wel_ADV mae_VERB 'di_PART bod_VERB yn_PART eitha_ADJ tawel_ADJ '_PUNCT naeth_NOUN y_DET ym_ADP [_PUNCT -_PUNCT ]_PUNCT naeth_NOUN y_DET ddynes_NOUN ddod_VERB mewn_ADP a_CONJ gweld_VERB y_DET llun_NOUN o_ADP Sir_NOUN dim_DET Sir_NOUN lleoliad_NOUN ym_ADP ._PUNCT
lleoliad_NOUN ._PUNCT
Ynys_VERB lleoliad_NOUN ._PUNCT
Ô_INTJ ._PUNCT
Ag_ADP  _SPACE deud_VERB bod_VERB [_PUNCT -_PUNCT ]_PUNCT yw_VERB merch_NOUN hi_PRON neu_CONJ ym_ADP merch_NOUN -_PUNCT yng_ADP -_PUNCT nghyfraith_NOUN efo_ADP carafan_NOUN sy_AUX yn_PART edrych_VERB +_SYM
Ô_NUM dros_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
+_VERB dros_ADP yr_DET ynys_NOUN ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Ond_CONJ ddim_PART yn_PART bell_ADJ o_ADP lle_NOUN mama_NOUN dad_NOUN 'di_PART tynnu_VERB 'r_DET llun_NOUN ._PUNCT
Ô_NUM dwi_AUX 'n_PART gweld_VERB lleoliad_NOUN ?_PUNCT
Ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Rhywbeth_NOUN felly_ADV a_CONJ mama_NOUN hi_PRON [_PUNCT -_PUNCT ]_PUNCT dwi_AUX 'n_PART meddwl_VERB mae_VERB 'r_DET un_NUM un_NUM ddynes_NOUN o'dd_NOUN o_ADP ym_ADP mae_VERB 'di_PART tynnu_VERB llun_NOUN o_ADP  _SPACE bont_ADJ o_ADP 'r_DET bont_NOUN yn_PART lleoliad_NOUN ._PUNCT
Ô_NUM ie_INTJ ._PUNCT
A_PART mae_AUX 'di_PART cael_VERB dros_ADP wel_NOUN i_ADP [_PUNCT -_PUNCT ]_PUNCT o_ADP gwmpas_NOUN miliwn_NUM o_ADP hits_NOUN ar_ADP Instagram_PROPN neu_CONJ rhywbeth_NOUN ._PUNCT
Ô_NUM rargen_NOUN ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
BeBe_ADV oedd_VERB yn_PART arbennig_ADJ am_ADP yr_DET llun_NOUN ?_PUNCT
Dwi_VERB 'm_PRON yn_PART siŵr_ADJ iawn_ADV  _SPACE fi_PRON 'n_PART siŵr_ADJ bod_VERB hi_PRON 'n_PART ddiwrnod_NOUN neis_ADJ a_CONJ mae_VERB yn_PART [_PUNCT saib_NOUN ]_PUNCT olygfa_NOUN eitha_ADJ neis_ADJ 'na_ADV yntydy_ADV ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Ydy_VERB [_PUNCT /=_PROPN ]_PUNCT ydy_VERB ._PUNCT
Ond_CONJ ym_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Syniad_NOUN da_ADJ ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
dwi_VERB 'n_PART medd_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
So_INTJ [_PUNCT =_SYM ]_PUNCT o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT o'dd_AUX hi_PRON 'n_PART mynd_VERB i_ADP  _SPACE weld_VERB os_CONJ o'dd_NOUN gen_ADP ei_DET merch_NOUN hi_PRON llun_NOUN o_ADP yr_DET ynys_NOUN +_SYM
Ie_INTJ ._PUNCT
A_CONJ hwyrach_ADV '_PUNCT neuth_NOUN hi_PRON dod_VERB nôl_ADV i_ADP brynu_VERB o_ADP iddi_ADP hi_PRON ._PUNCT
Ô_INTJ ._PUNCT
Ond_CONJ  _SPACE o'dd_NOUN hi_PRON 'n_PART deud_VERB bod_VERB y_DET garafan_NOUN [_PUNCT saib_NOUN ]_PUNCT ddim_PART yn_PART bell_ADJ o_ADP 'r_DET lle_NOUN +_SYM
Ie_INTJ ._PUNCT
+_VERB mama_NOUN dad_NOUN 'di_PART tynnu_VERB 'r_DET llun_NOUN ._PUNCT
Ô_NUM reit_INTJ ie_INTJ mae_VERB 'n_PART siŵr_ADJ ie_ADP lleoliad_NOUN [_PUNCT =_SYM ]_PUNCT ydy_VERB [_PUNCT /=_PROPN ]_PUNCT ydy_VERB hwnna_PRON dwi_AUX 'n_PART meddwl_VERB  _SPACE ym_ADP lleoliad_NOUN ._PUNCT
Ô_NUM reit_INTJ ._PUNCT
Da_ADJ iawn_ADV ti_PRON 'di_PART gweld_VERB dipyn_PRON o_ADP bel_NOUN -_PUNCT droed_NOUN heddiw_ADV neu_CONJ ?_PUNCT
Wel_INTJ na_INTJ dyw'im_PUNCT 'di_PART cychwyn_VERB eto_ADV naddo_VERB ?_PUNCT
Ô_NUM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Un_NUM o_ADP 'r_DET glôch_NOUN +_SYM
Ô_NUM ie_INTJ ._PUNCT
+_VERB Mae_VERB 'r_DET gêm_NOUN cyntaf_ADJ ._PUNCT
Ô_NUM reit_INTJ ._PUNCT
'_PUNCT Naeth_PROPN rhywun_NOUN  _SPACE o_ADP [_PUNCT =_SYM ]_PUNCT y_DET [_PUNCT /=_PROPN ]_PUNCT y_DET tîm_NOUN [_PUNCT saib_NOUN ]_PUNCT promo_NOUN [_PUNCT -_PUNCT ]_PUNCT  _SPACE promotion_NOUN +_SYM
M_NUM -_PUNCT hm_NOUN ._PUNCT
+_VERB Tîm_PROPN pawb_PRON dod_VERB a_CONJ  _SPACE jyst_ADV isho_NOUN adborth_NOUN a_CONJ gweld_VERB sut_ADV mae_VERB o_PRON 'n_PART  _SPACE siarad_VERB i_ADP pawb_PRON yn_ADP y_DET lle_NOUN ._PUNCT
Ô_NUM reit_INTJ ._PUNCT
Mae_VERB 'r_DET marchnad_NOUN marchnatwyr_ADJ ._PUNCT
Ie_INTJ ._PUNCT
A_PART gweld_VERB beth_PRON '_PUNCT alleth_NOUN e_PRON neud_VERB ._PUNCT
Ô_NUM reit_INTJ ._PUNCT
Ti_PRON 'm_DET 'di_PART gweld_VERB  _SPACE enwb_NOUN eto_ADV [_PUNCT saib_NOUN ]_PUNCT naddo_VERB ?_PUNCT
Na_INTJ ._PUNCT
Na_INTJ ._PUNCT
Na_INTJ ._PUNCT
Dyw_VERB e_PRON ddim_PART 'di_PART dechrau_VERB eto_ADV dwi_AUX 'm_DET yn_PART meddwl_VERB ._PUNCT
Naddo_VERB ?_PUNCT
Na_INTJ ._PUNCT
Ie_INTJ o_ADP 'n_PART i_PRON 'n_PART deud_VERB byse_NOUN fo_PRON yn_PART neis_ADJ cael_VERB  _SPACE theatr_NOUN cynyrchiadau_NOUN theatr_NOUN yn_ADP y_DET theatr_NOUN fel_ADP neu_CONJ hyd_NOUN yn_PART oed_NOUN rhoi_VERB cynnig_NOUN i_ADP  _SPACE [_PUNCT saib_NOUN ]_PUNCT wel_ADP perfformwyr_NOUN lleol_ADJ ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Defnyddio_VERB fo_PRON +_SYM
Ô_NUM ie_INTJ ._PUNCT
+_VERB Ar_ADP gyfer_NOUN neud_VERB dwi_AUX 'm_DET yn_PART gwybod_VERB monalogues_VERB neu_CONJ os_CONJ oes_VERB 'na_ADV rhywun_NOUN 'di_PART sgweni_VERB script_NOUN neu_CONJ rhywbeth_NOUN ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Dwi_AUX 'm_PRON yn_PART gwybod_VERB ._PUNCT
Ie_INTJ ô_ADP reit_INTJ [_PUNCT saib_NOUN ]_PUNCT ie_INTJ ._PUNCT
Gewn_ADP ni_PRON weld_VERB ._PUNCT
Ô_NUM da_ADJ iawn_ADV falle_ADV byse_CONJ '_PUNCT rhei_VERB o_ADP 'r_DET ysgolion_NOUN lleol_ADJ yn_PART fedru_ADV neud_VERB rhywbeth_NOUN neu_CONJ ym_ADP +_SYM
Ie_INTJ ._PUNCT
+_VERB Theatr_NOUN yr_DET ifanc_ADJ neu_CONJ [_PUNCT saib_NOUN ]_PUNCT +_SYM
Ie_INTJ ._PUNCT
+_VERB Neu_PART coleg_NOUN sefydliad_NOUN wel_NOUN coleg_NOUN sefydliad_NOUN __NOUN ._PUNCT
Ie_INTJ [_PUNCT saib_NOUN ]_PUNCT ag_ADP  _SPACE hwyrach_ADP '_PUNCT neuth_NOUN 'na_ADV fel_ADP  _SPACE cynyrchiadau_NOUN theatr_NOUN Cymraeg_PROPN [_PUNCT saib_NOUN ]_PUNCT do_PART dwi_AUX 'n_PART meddwl_VERB fel_ADP mama_VERB nhw_PRON 'n_PART neud_VERB i_PART mynd_VERB i_ADP 'r_DET lleoliad_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Hwyrach_ADV ._PUNCT
Ô_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT '_PUNCT neuth_NOUN e_PRON cymryd_VERB amser_NOUN i_ADP hynna_PRON ddigwydd_VERB ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ie_INTJ m_PRON -_PUNCT hm_NOUN ._PUNCT
Ie_INTJ ._PUNCT
So_ADP  _SPACE bebe_NOUN '_PUNCT dy_DET 'r_DET plan_NOUN fory_ADV ?_PUNCT
'_PUNCT da_ADJ 'n_PART ni_PRON 'n_PART mynd_VERB i_ADP lleoliad_NOUN yn_ADP y_DET bore_NOUN a_CONJ enwg3_NOUN wedyn_ADV gei_PRON di_PRON  _SPACE brynu_VERB dy_DET defnyddie_NOUN '_PUNCT neu_CONJ bebe_NOUN bynag_NOUN ti_PRON isho_NOUN ag_ADP  _SPACE a_CONJ wedyn_ADV '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB i_PART gael_VERB y_DET te_NOUN prynhawn_NOUN 'ma_ADV [_PUNCT saib_NOUN ]_PUNCT yn_PART __NOUN ._PUNCT
Yn_PART lleoliad_NOUN ?_PUNCT
lleoliad_NOUN wrth_ADP ymyl_NOUN lleoliad_NOUN ._PUNCT
Ô_NUM o_ADP 'n_PART i_PRON 'n_PART meddwl_VERB mae_VERB yn_PART lleoliad_NOUN ni_PRON 'n_PART mynd_VERB ._PUNCT
[_PUNCT =_SYM ]_PUNCT Na_INTJ [_PUNCT /=_PROPN ]_PUNCT na_INTJ ._PUNCT
Oh_INTJ ._PUNCT
Ie_INTJ so_VERB diwrnod_NOUN i_ADP 'r_DET merched_NOUN fory_ADV ._PUNCT
Pwy_PRON sy_AUX 'n_PART dod_VERB ?_PUNCT
Wel_CONJ timod_VERB merched_NOUN y_DET teulu_NOUN [_PUNCT saib_NOUN ]_PUNCT m_ADJ -_PUNCT hm_NOUN ._PUNCT
So_INTJ '_PUNCT dy_DET  _SPACE enwb3_VERB a_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Dwi_AUX 'n_PART meddwl_VERB yndyn_NOUN ._PUNCT
Pawb_PRON yn_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
So_PART bydd_AUX ddim_PART rhaid_VERB i_ADP ni_PRON fwyta_VERB lot_PRON o_ADP ginio_NOUN [_PUNCT -_PUNCT ]_PUNCT well_ADJ i_ADP ni_PRON beidio_VERB bwyta_VERB cinio_NOUN fory_ADV pan_CONJ ti_PRON 'n_PART cael_VERB lot_PRON o_ADP fwyd_NOUN ._PUNCT
Cael_VERB brecwast_NOUN [_PUNCT saib_NOUN ]_PUNCT mawr_ADJ a_CONJ wedyn_ADV +_SYM
[_PUNCT =_SYM ]_PUNCT Ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ie_INTJ ._PUNCT
Snack_NOUN fach_ADJ ._PUNCT
Ô_NUM ie_X m_NOUN -_PUNCT hm_NOUN ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ie_INTJ  _SPACE naeth_NOUN enwg_VERB o_ADP  _SPACE capel_NOUN ddod_VERB mewn_ADP ._PUNCT
Ô_NUM do_PRON ?_PUNCT
Ond_CONJ o_ADP 'n_PART i_PRON 'n_PART siarad_VERB so_ADV [_PUNCT -_PUNCT ]_PUNCT wnai_VERB weld_VERB o_ADP dydd_NOUN Sul_PROPN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
O'dd_NOUN en_PRON jyst_ADV yn_PART crwydro_VERB o_ADP gwmpas_NOUN ._PUNCT
Ô_NUM reit_INTJ ie_INTJ [_PUNCT saib_NOUN ]_PUNCT ô_INTJ ._PUNCT
Oedd_VERB ._PUNCT
So_PART bebe_NOUN ti_PRON angen_NOUN neud_VERB yn_PART capel_NOUN ?_PUNCT
Ô_NUM fynd_VERB i_ADP nôl_NOUN rhywbeth_NOUN i_ADP mam_NOUN rhywbeth_NOUN fel_ADP masnach_NOUN deg_NUM rhoi_VERB o_PRON yn_ADP y_DET car_NOUN a_CONJ wedyn_ADV meddwl_ADV dod_VERB nôl_ADV yma_ADV a_CONJ byta_VERB 'n_PART nghinio_NOUN wedyn_ADV ._PUNCT
Iawn_INTJ ._PUNCT
Oce_VERB [_PUNCT saib_NOUN ]_PUNCT '_PUNCT dy_DET hynna_NOUN 'n_PART iawn_ADJ ?_PUNCT
472.021_NOUN
