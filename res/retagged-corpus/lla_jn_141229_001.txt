0.0_NOUN
Pan_CONJ mama_NOUN enw_NOUN isie_ADJ '_PUNCT bwyd'd_NOUN oes_VERB dim_DET dal_VERB 'n_PART ôl_NOUN ._PUNCT
Ma_AUX 'n_PART rhaid_VERB iddo_ADP fe_PART lenwi_VERB 'i_PRON fol_NOUN ._PUNCT
Y_DET moch_NOUN yn_PART gwichian_VERB ?_PUNCT
Dyna_DET beth_PRON od_DET ._PUNCT
BeBe_PRON sy_AUX 'n_PART bod_VERB arnyn_ADP nhw_PRON '_PUNCT nawr_ADV ?_PUNCT
Hei_INTJ ._PUNCT
Pam_ADV y_PART 'ch_AUX chi_PRON 'n_PART cadw_VERB sŵn_NOUN ?_PUNCT
Y_DET 'ch_PRON chi_PRON wedi_PART ca'l_VERB eich_DET brecwast_NOUN ._PUNCT
Wel_INTJ ?_PUNCT
Mae_AUX e_PRON '_PUNCT enw_NOUN wedi_PART b_VERB '_PUNCT yta_NOUN 'r_DET '_PUNCT fale_NOUN '_PUNCT i_PRON gyd_ADP ._PUNCT
A_CONJ '_PUNCT nawr_ADV mae_AUX e_PRON '_PUNCT wedi_PART mynd_VERB i_PART chwilio_VERB am_ADP fwy_PRON ._PUNCT
Ond_CONJ '_PUNCT wede_VERB 's_X i_ADP fyddwn_VERB i_PRON 'n_PART nôl_NOUN rhagor_PRON i_ADP chi_PRON ._PUNCT
Ble_ADV mama_NOUN fe_PRON 'di_PART mynd_VERB ?_PUNCT
I_ADP gyfeiriad_NOUN y_DET cwt_NOUN ieir_NOUN '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB ._PUNCT
Ah_INTJ ._PUNCT
Be_DET mama_NOUN fe_PRON 'n_PART neud_VERB fan_NOUN 'na_ADV ?_PUNCT
Well_ADJ i_ADP fi_PRON fynd_VERB i_PART chwilio_VERB amdano_ADP fe_PRON ._PUNCT
Pob_DET lwc_NOUN i_ADP ti_PRON ._PUNCT
A_CONJ enw_NOUN os_CONJ gei_VERB di_PRON gyfle_NOUN ti_PRON 'n_PART fodlon_ADJ dod_VERB â_ADP mwy_PRON o_ADP '_PUNCT fale_NOUN '_PUNCT i_ADP ni_PRON ?_PUNCT
Iawn_INTJ ._PUNCT
Fel_CONJ mae_AUX 'n_PART digwydd_VERB ma_VERB 'r_DET ieir_NOUN yn_PART dwli_VERB ar_ADP afalau_NOUN ._PUNCT
A_CONJ mama_NOUN digon_ADJ ar_ADP ôl_NOUN gan_ADP enw_NOUN iddyn_NOUN '_PUNCT nhw_PRON hefyd_ADV ._PUNCT
Mmm_VERB blasus_ADJ ._PUNCT
Ond_CONJ gobeithio_VERB gawn_VERB ni_PRON ŷd_NOUN nes_CONJ ymla_VERB 'n_PART '_PUNCT fyd_NOUN ._PUNCT
Byta_VERB rhain_PRON gynta'_ADJUNCT enw_NOUN cyn_CONJ gofyn_VERB am_ADP fwy_PRON ._PUNCT
O_ADP ti_PRON mor_ADV farus_ADJ ._PUNCT
Barus_ADJ ?_PUNCT
Na_INTJ ._PUNCT
Ma'_VERBUNCT pob_DET ceiliog_NOUN yn_PART gor'od_PUNCT bwyta_VERB 'n_PART dda_ADJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_INTJ [_PUNCT /=_PROPN ]_PUNCT ia_INTJ ._PUNCT
Os_CONJ ti_PRON 'n_PART d'eud_VERB ._PUNCT
Ydw_INTJ ._PUNCT
A_PART mama_NOUN enw_NOUN 'n_PART deall_VERB yn_PART iawn_ADJ hefyd_ADV ._PUNCT
Ta_CONJ '_PUNCT beth_PRON ._PUNCT
'_PUNCT D_NUM ydw_VERB i_PRON ddim_PART yn_PART hoffi_ADV rhannu_VERB fy_DET mwyd_NOUN gyda_ADP phawb_NOUN ._PUNCT
Ma'_VERBUNCT digon_ADV fa'_NOUNUNCT 'ma_ADV i_ADP ni_PRON gyd_ADP ._PUNCT
Be_PRON sy_AUX 'n_PART bod_VERB arna_ADP '_PUNCT chdi_NOUN ?_PUNCT
Hmm_INTJ ._PUNCT
Pam_ADV yn_ADP y_DET byd_NOUN mama_NOUN enw_NOUN yn_PART dod_VERB i_ADP 'r_DET cwt_NOUN ieir_NOUN ?_PUNCT
Iwhŵ_INTJ ._PUNCT
Chi_PRON 'n_PART iawn_ADJ ?_PUNCT
BeBe_ADV wyt_VERB ti_PRON isio_VERB 'n_PART fa'_VERBUNCT 'ma_ADV ?_PUNCT
Wel_INTJ 'na_ADV chi_PRON groeso_ADJ ._PUNCT
Chwilio_VERB am_ADP fwyd_NOUN ydw_VERB i_PRON ._PUNCT
O_ADP 's_DET peth_NOUN sbâr_ADJ 'ma_ADV ?_PUNCT
Nag_PART oes_VERB wir_ADV ._PUNCT
Ein_DET '_PUNCT fale_NOUN '_PUNCT ni_PRON yw_VERB rhain_PRON '_PUNCT a_CONJ ._PUNCT
Ond_CONJ mae_VERB digon_PRON i_ADP bawb_PRON fan_NOUN hyn_DET ._PUNCT
Mmm_VERB ._PUNCT
Mae_VERB nhw_PRON mor_ADV flasus_ADJ ._PUNCT
A_CONJ rhain_PRON [_PUNCT -_PUNCT ]_PUNCT mmm_PART mama_NOUN rhain_PRON yn_PART flasus_ADJ hefyd_ADV ._PUNCT
Ond_CONJ ge_NOUN 's_CONJ di_PRON ddim_PART brecwast_NOUN bore_NOUN '_PUNCT mama_NOUN tê_PRON ?_PUNCT
Do_INTJ ._PUNCT
Ond_CONJ dim_PART digon_ADJ ._PUNCT
O_ADP wel_NOUN enw_NOUN ._PUNCT
Sbia_INTJ ._PUNCT
Rhywun_NOUN sy_VERB 'n_PART fwy_ADV barus_ADJ na_CONJ chdi_NOUN ._PUNCT
Mmm_VERB ._PUNCT
Hyfryd_ADJ ._PUNCT
O_ADP 's_X mwy_ADJ '_PUNCT dach_VERB chi_PRON ?_PUNCT
Dydw_VERB i_PRON dal_ADV ddim_PART yn_PART llawn_ADJ ._PUNCT
'_PUNCT D_NUM yw_VERB dy_DET fol_NOUN di_PRON byth_ADV yn_PART llawn_ADJ '_PUNCT wedwn_VERB ni_PRON ._PUNCT
Os_CONJ nad_PART o_ADP 's_DET rhagor_DET fan_NOUN hyn_DET bydd_VERB rhaid_VERB i_ADP mi_PRON fynd_VERB i_PART chwilio_VERB yn_PART rh'wle_NOUN arall_ADJ ._PUNCT
Hwyl_INTJ a_CONJ diolch_INTJ ._PUNCT
A_CONJ hwyl_NOUN fawr_ADJ i_ADP ti_PRON ._PUNCT
Diolch_NOUN wir_ADJ ._PUNCT
379.822_ADJ
Mmm_VERB ._PUNCT
'_PUNCT Fale_NOUN '_PUNCT ._PUNCT
Mmm_VERB ._PUNCT
Iwhŵ_INTJ ._PUNCT
Helo_INTJ ._PUNCT
O_ADP 's_X '_PUNCT pyn_NOUN bach_ADJ o_ADP fwyd_NOUN sbâr_ADJ 'ma_ADV ?_PUNCT
Na_INTJ ._PUNCT
Dim_PRON ond_ADP y_DET '_PUNCT fale_NOUN '_PUNCT 'ma_ADV ._PUNCT
A_CONJ ni_PRON isio_VERB rhain_PRON ._PUNCT
'_PUNCT Fale_NOUN '_PUNCT ._PUNCT
Hwre_INTJ ._PUNCT
Ww_ADP mama_NOUN digon_ADJ 'ma_ADV i_ADP ni_PRON gyd_ADP ._PUNCT
O_ADP cer_VERB o_ADP fan_NOUN hyn_DET enw_NOUN ._PUNCT
Gad_VERB fi_PRON fod_VERB ._PUNCT
'_PUNCT S_NUM na_CONJ i_PRON 'n_PART g'neud_VERB dim_PRON ._PUNCT
Dim_PRON ond_ADP b'yta'_NOUNUNCT fale_NOUN '_PUNCT blasus_ADJ diolch_INTJ ._PUNCT
Ni_PART sy_VERB bia_NUM rhain_PRON '_PUNCT a_CONJ ._PUNCT
Y_DET 'n_PART ni_PRON mynd_VERB i_ADP '_PUNCT weud_VERB wrth_ADP enw_NOUN ._PUNCT
enw_NOUN paff_NOUN ._PUNCT
'_PUNCT S_NUM dim_DET ots_NOUN '_PUNCT da_ADJ fi_PRON ._PUNCT
Ah_ADP ti_PRON 'n_PART fochyn_NOUN barus_ADJ ._PUNCT
Aw_ADP ._PUNCT
Peidiwch_VERB â_CONJ phoeni_VERB ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART mynd_VERB ._PUNCT
Ond_CONJ gaga_VERB i_ADP un_NUM bach_ADJ arall_ADJ ._PUNCT
Mae_VERB nhw_PRON mor_ADV flasus_ADJ ._PUNCT
Wel_CONJ a_CONJ un_NUM bach_ADJ arall_ADJ '_PUNCT dê_NOUN ._PUNCT
Diolch_INTJ ._PUNCT
A_CONJ hwyl_NOUN ._PUNCT
Hwyl_NOUN fawr_ADJ ._PUNCT
A_CONJ phaid_NOUN â_ADP brysio_VERB 'n_PART ôl_NOUN ._PUNCT
Ond_CONJ rhain_PRON yw_VERB 'r_DET '_PUNCT fale_NOUN '_PUNCT mwya'_ADJUNCT blasus_NOUN '_PUNCT dw_AUX i_ADP wedi_PART ca'l_VERB heddi_NOUN '_PUNCT ._PUNCT
enw_NOUN enw_NOUN ._PUNCT
Pam_ADV mae_AUX pawb_PRON yn_PART pigo_VERB arna_ADP '_PUNCT i_PRON heddi_NOUN '_PUNCT ?_PUNCT
'_PUNCT D_NUM y_DET '_PUNCT i_PRON wedi_PART g'neud_VERB dim_DET byd_NOUN i_ADP neb_PRON ._PUNCT
Mwy_PRON o_ADP '_PUNCT fale_NOUN '_PUNCT ._PUNCT
Hwre_INTJ ._PUNCT
Ww_ADP mama_NOUN rhain_PRON yn_PART flasus_ADJ hefyd_ADV ._PUNCT
BeBe_VERB yn_ADP y_DET byd_NOUN wyt_AUX ti_PRON 'n_PART g'neud_VERB fan_NOUN hyn_DET enw_NOUN ?_PUNCT
Chwilio_VERB am_ADP fwyd_NOUN ._PUNCT
'_PUNCT D_NUM ydw_VERB i_PRON ddim_PART wedi_PART cael_VERB digon_PRON o_ADP fwyd_NOUN heddi_ADP '_PUNCT ._PUNCT
Meddwl_VERB '_PUNCT falle_NOUN bod_VERB '_PUNCT pyn_NOUN bach_ADJ o_ADP fwyd_NOUN sbâr_ADJ fan_NOUN hyn_DET ?_PUNCT
Na_INTJ ._PUNCT
'_PUNCT S_NUM dim_DET byd_NOUN sbâr_ADJ fan_NOUN hyn_DET ._PUNCT
Y_DET 'n_PART ni_PRON isio_VERB 'r_DET '_PUNCT fale_NOUN '_PUNCT 'na_ADV ._PUNCT
O_ADP brensiach_NOUN y_DET brain_NOUN ._PUNCT
Mae_VERB enw_NOUN yn_PART anodd_ADJ i_ADP 'w_PRON ddal_VERB ._PUNCT
Ond_CONJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB mod_AUX i_PRON 'n_PART gw'bod_VERB lle_ADV mae_VERB e_PRON '_PUNCT nawr_ADV ?_PUNCT
Mmm_VERB ._PUNCT
Mmm_VERB ._PUNCT
enw_NOUN ._PUNCT
Gad_AUX i_PRON 'n_PART bwyd_VERB ni_PRON fod_VERB ._PUNCT
Ti_PRON mor_ADV farus_ADJ ._PUNCT
Be_PRON sy_AUX 'n_PART bod_VERB ar_ADP bawb_PRON yn_PART meddwl_VERB bod_VERB i_PRON 'n_PART farus_ADJ ?_PUNCT
'_PUNCT Wi_PROPN 'n_PART mwynhau_VERB fy_DET mwyd_NOUN ._PUNCT
Dyna_ADV i_ADP gyd_ADP ._PUNCT
Ond_CONJ ti_PRON 'n_PART mwynhau_VERB ein_DET bwyd_NOUN ni_PRON ._PUNCT
[_PUNCT Ci_NOUN yn_PART cyfarth_VERB ]_PUNCT ._PUNCT
Wps_INTJ ._PUNCT
Well_ADJ i_ADP fi_PRON fynd_VERB ._PUNCT
Helo_INTJ chi_PRON ._PUNCT
Ydi_NOUN enw_NOUN yma_ADV ?_PUNCT
Da_ADJ iawn_ADV ._PUNCT
'_PUNCT D_NUM yw_AUX enw_NOUN ddim_PART wedi_PART ngweld_VERB i_ADP ._PUNCT
Well_ADJ i_ADP fi_PRON fynd_VERB adre_ADV '_PUNCT ._PUNCT
Y_DET 'ch_PRON chi_PRON 'n_PART gw'bod_VERB i_ADP ble_ADV a_CONJ 'th_VERB e_PRON ?_PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART meddwl_VERB ei_PRON fod_AUX wedi_PART mynd_VERB adre_ADV '_PUNCT ._PUNCT
Hen_ADJ bryd_NOUN '_PUNCT fyd_NOUN ._PUNCT
675.539_NOUN
812.949_VERB
