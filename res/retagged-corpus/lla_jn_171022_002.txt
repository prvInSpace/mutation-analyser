0.0_NOUN
Un_NUM o_ADP enwogion_NOUN <_SYM lleoliad_NOUN 1_NUM >_SYM yw_VERB 'r_DET actor_NOUN enwg_VERB cyfenw_NOUN ._PUNCT
Mae_AUX enwg_VERB wedi_PART treulio_VERB rhan_NOUN fwya'_ADJUNCT o_ADP 'i_PRON yrfa_NOUN broffesiynol_ADJ yng_ADP lleoliad_NOUN y_DET cwm_NOUN dychmygol_ADJ hwnnw_PRON sydd_AUX wedi_PART ei_DET seilio_VERB ar_ADP yr_DET ardal_NOUN hon_DET [_PUNCT saib_NOUN ]_PUNCT [_PUNCT cerddoriaeth_NOUN ]_PUNCT ._PUNCT
Ar_ADP ben_NOUN y_DET cwbwl_NOUN mama'_NOUNUNCT isio_VERB ordro_ADP dwy_NUM dunnell_NOUN o_ADP fwyd_NOUN i_PART gadw_VERB nhw_PRON fynd_VERB nes_CONJ gawn_VERB ni_PRON 'r_DET canlyniad_NOUN prawf_NOUN [_PUNCT saib_NOUN ]_PUNCT bron_ADV i_ADP fil_NUM o_ADP bunne_NOUN arall_ADJ 'di_PART 'r_DET arian_NOUN jest_ADV ddim_PART gydan_VERB ni_PRON [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Allen_VERB ni_PRON ddim_PART starfo_VERB nhw_PRON [_PUNCT saib_NOUN ]_PUNCT [_PUNCT swn_VERB defaid_NOUN ]_PUNCT ._PUNCT
<_SYM aneglur_ADJ 1_NUM >_SYM meddwl_VERB am_ADP ffordd_NOUN arall_ADJ i_PART arbed_VERB arian_NOUN [_PUNCT ochneidio_VERB ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Dere_VERB mlaen_NOUN te_NOUN gwed_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Stopo_VERB 'ch_PRON cyflog_ADJ chi_PRON 'ch_PRON dou_NOUN [_PUNCT ochneidio_VERB ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Pa_PART mor_ADV agos_ADJ ydi_VERB yr_DET iaith_NOUN dani_VERB 'n_PART clwad_NOUN ar_ADP bobl_NOUN y_DET cwm_NOUN i_ADP iaith_NOUN y_DET cwm_NOUN go_ADV iawn_ADJ ?_PUNCT
Wel_INTJ [_PUNCT ochneidio_VERB ]_PUNCT bydden_VERB ni_PRON 'n_PART anhapus_ADJ '_PUNCT da_ADJ rhai_DET petha_VERB oherwydd_ADP o_ADP 'n_PART i_PRON 'n_PART ymwybodol_ADJ iawn_ADV bobo_VERB nhw_PRON ddim_PART yn_PART perthyn_VERB i_ADP lleoliad_NOUN ia_ADJ +_SYM
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ia_PRON ._PUNCT
+_AUX Fi_PRON 'n_PART cofio_VERB benodol_ADJ o_ADP nhw_PRON 'n_PART sôn_VERB am_ADP anner_NOUN yn_ADP y_DET sgript_NOUN sef_CONJ heffer_NOUN yn_ADP y_DET gogledd_NOUN wel_PRON dy_DET 'n_PART ni_PRON ddim_PART yn_PART gweud_VERB y_DET gair_NOUN yna_DET yn_PART lleoliad3_ADJ mama'_NOUNUNCT wnna_NOUN a_CONJ '_PUNCT wedes_NOUN i_ADP treisiad_NOUN bydd_VERB gydan_VERB ni_PRON ac_CONJ oddan_VERB nhw_PRON ddim_PART yn_PART lico_NOUN '_PUNCT wnna_NOUN o_ADP gwbwl_NOUN oedd_VERB e_PRON 'n_PART rhy_ADV debyg_ADJ i_PART treisio_VERB wel_ADV 'na_ADV fe_PRON wedes_VERB i_ADP treisiad_NOUN yw_VERB un_NUM a_PART tresiedi_VERB yw_VERB mwy_ADJ na_CONJ un_NUM a_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Neu_CONJ <_SYM aneglur_ADJ 1_NUM >_SYM +_SYM
[_PUNCT chwerthin_VERB ]_PUNCT Wel_CONJ ia_PRON ._PUNCT
+_VERB [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Hefyd_ADV fi_PRON 'n_PART cofio_VERB dadl_NOUN gyda_ADP 'n_PART hen_ADJ ffrind_NOUN enwg_VERB cyfenw_NOUN ynglyn_VERB â_ADP 'r_DET gair_NOUN stilo_VERB bydd_AUX '_PUNCT e_PRON 'n_PART cofio_VERB 'n_PART iawn_ADJ ._PUNCT
'_PUNCT Oedd_AUX '_PUNCT e_PRON 'n_PART mynnu_VERB bobo_VERB ni_PRON 'n_PART deud_VERB stilo_VERB yng_ADP lleoliad_NOUN ac_CONJ es_VERB i_ADP adra_ADV a_CONJ gofyn_VERB i_ADP '_PUNCT y_DET mam_NOUN yng_ADP nghyfraith_NOUN a_CONJ '_PUNCT y_DET ngwraig_NOUN o_ADP nhw_PRON 'm_DET hyd_NOUN yn_PART oed_NOUN yn_PART gw'bod_VERB beth_PRON oedd_VERB stilo_VERB [_PUNCT saib_NOUN ]_PUNCT smwddo_VERB o_ADP 'n_PART i_PRON 'n_PART gweud_VERB a_CONJ fi_PRON 'n_PART credu_VERB bod_VERB stilo_VERB falle_NOUN 'di_PART dod_VERB lan_ADV gyda_ADP enwg_VERB __NOUN '_PUNCT lly_NOUN o_ADP ardal_NOUN __NOUN __NOUN __NOUN ._PUNCT
'_PUNCT lly_X [_PUNCT =_SYM ]_PUNCT ma_VERB ma_PRON [_PUNCT /=_PROPN ]_PUNCT ma_VERB 'r_DET peth_NOUN yn_PART digwydd_VERB os_CONJ ydi_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT lleoli_VERB cyfres_VERB mewn_ADP man_NOUN penodol_ADJ [_PUNCT =_SYM ]_PUNCT ma_VERB [_PUNCT /=_PROPN ]_PUNCT ma_PRON isio_ADV bod_VERB yn_PART ofalus_ADJ '_PUNCT da_ADJ petha_ADJ fel_ADP '_PUNCT a_CONJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ond_CONJ sut_ADV ma_VERB petha_NOUN yn_ADP y_DET cwm_NOUN go_ADV iawn_ADJ ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT Mae_VERB 'r_DET newid_NOUN mewn_ADP patryma_ADJ gwaith_NOUN yn_ADP y_DET lleoliad_NOUN yn_PART rh'wbath_NOUN sy_VERB 'n_PART destun_NOUN pryder_NOUN i_PART enwg_VERB ._PUNCT
Ma_AUX lot_PRON o_ADP bobol_NOUN nawr_ADV yn_PART mynd_VERB mas_NOUN o_ADP 'r_DET cwm_NOUN i_ADP weitho_VERB ._PUNCT
Ia_INTJ ._PUNCT
Ag_CONJ yn_PART aml_ADJ wrth_ADP gwrs_NOUN os_CONJ yn_PART mynd_VERB mas_NOUN o_ADP 'r_DET cwm_NOUN tase_AUX nhw_PRON 'n_PART gorffo_VERB '_PUNCT mynd_VERB i_ADP lleoliad_NOUN ne_NOUN rwle_NOUN dy_DET '_PUNCT nhw_PRON 'm_DET yn_PART myn_VERB '_PUNCT i_PRON gael_VERB Cymraeg_PROPN yn_ADP y_DET gweithle_NOUN yr_DET un_NUM peth_NOUN +_SYM
Mm_INTJ ._PUNCT
+_VERB Felly_CONJ '_PUNCT falle_ADV wedyn_ADV bobo_VERB 'na_ADV fwy_PRON o_ADP gyfrifoldeb_NOUN ar_ADP gyrff_NOUN fel_ADP y_DET Ffermwyr_PROPN Ifenc_PROPN a_CONJ 'r_DET [_NOUN aneglur_ADJ ?_PUNCT ]_PUNCT a_CONJ 'r_DET capeli_NOUN ond_CONJ hefyd_ADV ma_VERB gyda_ADP ti_PRON wedyn_ADV p_PART -_PUNCT pethe_VERB fel_ADP y_DET fenter_NOUN wrth_ADP gwrs_NOUN wedi_PART dod_VERB mewn_ADP '_PUNCT efo_ADP rhyw_DET fath_NOUN o_ADP  _SPACE grutch_VERB i_ADP bwyso_VERB arno_ADP fe_PRON ondofe_NOUN ._PUNCT
Mae_VERB gwaith_NOUN y_DET fenter_NOUN wedi_PART bod_VERB yn_PART amhrisiadwy_ADJ a_CONJ dweud_VERB y_DET gwir_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT nhw_PRON 'n_PART cynnal_VERB cymreictod_NOUN ._PUNCT
[_PUNCT cerddoriaeth_NOUN ]_PUNCT ._PUNCT
Drws_NOUN nesa_ADJ i_ADP swyddfeydd_NOUN menter_NOUN lleoliad_NOUN mae_VERB sefydliad_NOUN felly_CONJ baches_VERB i_ADP ar_ADP y_DET cyfle_NOUN am_ADP banad_NOUN sydyn_ADJ ac_CONJ i_PART holi_VERB mwy_PRON am_ADP waith_NOUN y_DET fenter_NOUN ._PUNCT
Helo_INTJ ._PUNCT
Dw_AUX i_PRON 'm_DET yn_PART rhy_ADV hwyr_ADJ ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Na_VERB na_PART dim_DET to_NOUN ._PUNCT
Allai_VERB gal_NOUN panad_NOUN o_ADP dê_NOUN plis_INTJ a_CONJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT disgled_NOUN o_ADP dê_NOUN ynde_ADP ?_PUNCT
Disgled_ADV o_ADP dê_NOUN [_PUNCT saib_NOUN ]_PUNCT Siwger_X ?_PUNCT
dim_PRON diolch_NOUN i_ADP chi_PRON ._PUNCT
[_PUNCT swn_NOUN llwy_NOUN ar_ADP y_DET gwpan_NOUN ]_PUNCT
'_PUNCT Na_VERB ni_PRON [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
"_PUNCT Dechreuwch_VERB pob_DET sgwrs_NOUN yn_ADP Gymraeg_PROPN "_PUNCT +_SYM
Ia_INTJ ?_PUNCT
+_VERB ydi_VERB 'r_DET rhan_NOUN fwya_ADJ o_ADP bobl_NOUN sy_AUX 'n_PART dod_VERB yma_ADV yn_PART +_SYM
Odyn_NOUN +_VERB S1_NOUN +_VERB siarad_VERB Cymraeg_PROPN ?_PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT siarad_VERB cymraeg_PROPN ma_PRON peth_NOUN o_ADP nhw_PRON 'n_PART siarad_VERB saesneg_NOUN on_X ma_AUX nhw_PRON 'n_PART gw'bod_VERB bobo_VERB ni_PRON gyd_ADV yn_PART siarad_VERB Cymraeg_PROPN [_PUNCT -_PUNCT ]_PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ia_DET dw_VERB i_PRON 'n_PART iawn_ADJ i_ADP feddwl_VERB [_PUNCT =_SYM ]_PUNCT ma_VERB [_PUNCT /=_PROPN ]_PUNCT ma_VERB 'r_DET fenter_NOUN yn_PART cyflogi_VERB nifer_NOUN yn_ADP y_DET cwm_NOUN yndi_ADJ ?_PUNCT
Odi_ADP mama_NOUN nhw_PRON yn_PART pedwar_NOUN sy_AUX 'n_PART gweithio_VERB yn_ADP y_DET caffi_VERB ti_PRON '_PUNCT bo_VERB ma_PRON dou_NOUN mewn_ADP fi_PRON a_CONJ enwb1_NUM mewn_ADP heddi_NOUN a_CONJ wedyn_ADV ma_VERB enwb2_NOUN a_CONJ __NOUN yn_PART dod_VERB fewn_ADP <_SYM aneglur_ADJ 1_NUM >_SYM ni_PRON '_PUNCT efyd_NOUN ._PUNCT
237.148_NOUN
Yn_ADP y_DET naw_NUM dege_NOUN cynnar_ADJ ysgrifennodd_VERB enwg_VERB cyfenw_NOUN cyfarwyddwr_NOUN y_DET fenter_NOUN ar_ADP y_DET pryd_NOUN fod_VERB unrhyw_DET sôn_NOUN am_ADP achub_VERB iaith_NOUN yn_PART ymarfer_VERB cwbl_NOUN ofer_ADJ "_PUNCT oni_CONJ chyplysir_VERB yr_DET iaith_NOUN ar_ADP gymdeithas_NOUN sy_AUX 'n_PART ei_DET chynnal_VERB "_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Nododd_VERB hefyd_ADV fod_VERB y_DET berthynas_NOUN rhwng_ADP iaith_NOUN a_CONJ gwaith_NOUN yn_ADP holl_DET bwysig_ADJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Es_VERB inna_X '_PUNCT drws_NOUN nesa'_ADJUNCT felly_ADV i_PART holi_VERB mwy_ADV gyn_NOUN gyfarwyddwraig_NOUN bresennol_ADJ y_DET fenter_NOUN sef_CONJ enwb_NOUN cyfenw_NOUN sydd_AUX wedi_PART bod_AUX yn_PART gweithio_VERB hefo_ADP 'r_DET fenter_NOUN ers_ADP y_DET cychwyn_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
BeBe_AUX oeddech_VERB chi_PRON 'n_PART neud_VERB ar_ADP y_DET dechra'_VERBUNCT ta_NOUN ?_PUNCT
Ar_ADP y_DET cychwyn_NOUN cynta'_ADJUNCT [_PUNCT saib_NOUN ]_PUNCT adnabod_VERB y_DET gymuned_NOUN leol_ADJ oedd_VERB yn_PART bwysig_ADJ [_PUNCT saib_NOUN ]_PUNCT gan_CONJ gofio_VERB bod_VERB y_DET pwll_NOUN glo_NOUN ola_X '_PUNCT wedi_PART cau_VERB yn_PART lleoliad_NOUN yn_PART un_NUM naw_NUM naw_NUM un_NUM yr_DET union_ADJ yr_DET un_NUM flwyddyn_NOUN â_ADP sefydlwyd_VERB y_DET fenter_NOUN [_PUNCT saib_NOUN ]_PUNCT ac_CONJ felly_ADV '_PUNCT oedd_VERB  _SPACE llenwi_VERB gwagle_NOUN a_CONJ mynd_VERB a_CONJ 'r_DET iaith_NOUN at_ADP lle_NOUN ma_PRON bobol_NOUN yn_PART cymdeithasu_VERB lle_ADV ma_PRON bobol_NOUN yn_PART cyfarfod_VERB ac_CONJ yn_PART fey_ADP na_CONJ hynny_PRON atal_ADV y_DET ffin_NOUN ieithyddol_ADJ rhag_CONJ dod_VERB yn_PART nes_ADJ gwir_NOUN oedd_VERB y_DET weledigeth_NOUN ._PUNCT
A_CONJ ma_VERB 'r_DET dafodieth_NOUN wrth_ADP gwrs_NOUN wedi_PART bod_AUX yn_PART glynnu_VERB yn_ADP yr_DET ardaloedd_NOUN yma_ADV oddi_ADP ar_ADP dyddie_NOUN 'r_DET pylle_NOUN glo_NOUN hynny_PRON yw_VERB ar_ADP ddiwedd_NOUN dydd_NOUN roedd_AUX bobol_ADJ yn_PART roi_VERB tools_NOUN ar_ADP y_DET bar_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Reit_INTJ ._PUNCT
<_SYM aneglur_ADJ 3_NUM >_SYM Roi_VERB terfyn_NOUN pan_CONJ o_ADP nhw_PRON 'n_PART dod_VERB i_ADP ben_NOUN ar_ADP rywbeth_NOUN a_CONJ fel_ADP bydde_VERB rywun_NOUN yn_PART gweud_VERB peidiwch_VERB a_CONJ <_SYM aneglur_ADJ 1_NUM >_SYM neud_VERB y_DET job_NOUN <_SYM /_SYM en_PRON '_PUNCT newch_NOUN y_DET [_PUNCT *_SYM en_PRON >_NUM job_NOUN <_SYM /_SYM en_PRON *_SYM ]_PUNCT yn_PART iawn_ADJ peidwich_NOUN ai_PART adael_VERB '_PUNCT e_PRON ar_ADP i_PRON '_PUNCT anner_NOUN gwnewch_VERB '_PUNCT e_PRON 'n_PART gyflawn_ADJ ._PUNCT
I_ADP chi_PRON yn_PART colli_VERB hynny_PRON nawr_ADV wrth_ADP gwrs_NOUN achos_CONJ fel_ADP i_ADP ni_PRON sôn_VERB oedd_VERB 'na_ADV [_PUNCT saib_NOUN ]_PUNCT beuoedd_NOUN cryf_ADJ oedd_VERB 'na_ADV dyrfa_NOUN o_ADP bobol_NOUN yn_PART siarad_VERB y_DET Gymraeg_PROPN gyda_ADP 'u_PRON gilydd_NOUN bob_DET dydd_NOUN mewn_ADP bob_DET amgylchiade_NOUN '_PUNCT
[_PUNCT cerddoriaeth_NOUN ]_PUNCT
"_PUNCT Un_NUM teulu_NOUN agos_ADJ oeddem_VERB ni_PRON gyd_ADV [_PUNCT saib_NOUN ]_PUNCT yn_PART '_PUNCT werthin_NOUN ne_PRON '_PUNCT 'n_PART llefen_VERB yr_DET un_NUM pryd_NOUN [_PUNCT saib_NOUN ]_PUNCT a_CONJ thra_NOUN bod_VERB talcen_NOUN glo_NOUN dan_ADP ein_PRON traed_VERB [_PUNCT saib_NOUN ]_PUNCT bydd_VERB sgrech_NOUN hwteri_NOUN 'n_PART gryndod_NOUN yn_ADP ein_DET gwaed_NOUN [_PUNCT saib_NOUN ]_PUNCT
[_PUNCT cerddoriaeth_NOUN yn_PART parhau_VERB ]_PUNCT ._PUNCT
Bu_VERB farw_VERB dau_NUM ddeg_NOUN chwech_NUM o_ADP lowyr_NOUN yn_ADP y_DET drychineb_NOUN hon_DET ac_CONJ roedd_VERB y_DET fenga_VERB dim_PRON ond_ADP yn_PART ddeng_NUM mlwydd_NOUN oed_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ma_VERB 'r_DET diwydiant_NOUN wedi_PART mynd_VERB on_X 'ma_ADV 'r_DET arferiad_NOUN yma_DET o_ADP hyd_NOUN ac_CONJ un_NUM o_ADP 'r_DET pethe_NOUN pwysig_ADJ sydd_VERB y_DET balchder_NOUN cymunedol_ADJ ac_CONJ yn_PART hynny_PRON wrth_ADP gwrs_NOUN y_DET balchder_NOUN ieithyddol_ADJ yn_PART ogystal_ADJ sy_AUX 'n_PART cynnal_VERB y_DET cymunede_VERB ag_CONJ yn_PART roi_VERB hyder_NOUN i_ADP fobol_NOUN a_CONJ i_PRON ddefnyddio_VERB 'u_PRON cymrag_NOUN ._PUNCT
[_PUNCT cerddoriaeth_NOUN <_SYM /_SYM N_NUM
Ganllath_NOUN i_ADP fyny_ADV 'r_DET stryd_NOUN heibio_ADV i_ADP neuadd_NOUN lleoliad_NOUN mae_VERB siop_NOUN lyfra_VERB sefydliad_NOUN cangen_NOUN arall_ADJ o_ADP 'r_DET fenter_NOUN o_ADP fewn_ADP y_DET pentra'_NOUNUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Tra_ADV 'n_PART prynu_VERB llyfr_NOUN am_ADP hanes_NOUN y_DET cwm_NOUN nes_CONJ i_PART ddechra_VERB sgwrsio_VERB am_ADP gyfleoedd_NOUN gwaith_NOUN ac_CONJ addysg_NOUN o_ADP fewn_ADP y_DET cwm_NOUN heddiw_ADV [_PUNCT saib_NOUN ]_PUNCT so_VERB lle_NOUN oddat_AUX ti_PRON 'n_PART mynd_VERB i_ADP 'r_DET ysgol_NOUN ta_NOUN ?_PUNCT
<_SYM aneglur_ADJ 1_NUM >_SYM i_ADP i_ADP 'r_DET lleoliad_NOUN ._PUNCT
Oedd_VERB hwnna_PRON cyn_CONJ bod_VERB  _SPACE bebe_NOUN 'di_PART enw_NOUN fo_PRON ?_PUNCT
sefydliad_NOUN i_ADP gael_VERB ne_NOUN ?_PUNCT
Na_PART oedd_VERB sefydliad_NOUN ar_ADP gael_VERB '_PUNCT fyd_NOUN +_SYM
Reit_INTJ +_SYM
+_VERB On_ADP '_PUNCT o_PRON 'n_PART i_PRON ddim_PART yn_PART lico_NOUN cymraeg_NOUN pan_CONJ o_ADP 'n_PART i_PRON 'n_PART ifanc_ADJ +_SYM
+_VERB Reit_PROPN ._PUNCT
O_ADP 'n_PART i_PRON fili_ADV gwneud_VERB gwersi_NOUN fi_PRON 'n_PART gymraeg_NOUN so_ADJ nes_CONJ i_PART fynd_VERB lleoliad5_NOUN ._PUNCT
Ar_ADP ôl_NOUN gorffen_VERB ysgol_NOUN es_VERB i_ADP mewn_ADP i_ADP coleg_NOUN a_CONJ nes_CONJ i_ADP pedwar_NUM blwyddyn_NOUN yn_PART coleg_NOUN [_PUNCT saib_NOUN ]_PUNCT a_CONJ o_PRON 'n_PART i_PRON 'n_PART ffili_ADV cael_VERB swydd_NOUN 'ma_ADV so_VERB beth_PRON nes_CONJ i_ADP +_SYM
Ia_INTJ ._PUNCT
+_VERB so_VERB es_VERB i_ADP lawr_ADV i_ADP <_SYM aneglur_ADJ 2_NUM >_SYM a_CONJ ffindis_VERB i_ADP swydd_NOUN wedyn_ADV a_CONJ fi_PRON 'di_PART bod_VERB yma_ADV ers_ADP '_PUNCT nny_NOUN so_VERB ia_PRON ._PUNCT
'_PUNCT llu_NOUN ma_PRON 'n_PART eitha'_ADJUNCT eironig_ADJ mewn_ADP ffordd_NOUN ma_PRON [_PUNCT saib_NOUN ]_PUNCT ma_VERB hefo_ADP r'wbath_NOUN cymraeg_NOUN ti_PRON 'di_PART cael_VERB gwaith_NOUN ?_PUNCT
Ie_INTJ on_X '_PUNCT o_PRON 'n_PART i_PRON wastad_ADV yn_PART lico_NOUN cymrag_NOUN ond_CONJ jest_ADV  _SPACE dysgu_VERB fe_PRON o_ADP 'n_PART i_PRON 'n_PART ffindo_VERB fe_PRON 'n_PART anodd_ADJ i_PART neud_VERB pob_DET gwers_NOUN yn_ADP cymrag_NOUN ._PUNCT
424.495_NOUN
Ma_AUX enwb_NOUN wedi_PART bod_VERB yn_PART ffodus_ADJ i_PART gael_VERB gwaith_NOUN o_ADP fewn_ADP y_DET cwm_NOUN gan_CONJ ma_ADV teithio_VERB allan_ADV o_ADP 'r_DET lleoliad_NOUN ydi_VERB hanes_NOUN y_DET rhan_NOUN fwya'_ADJUNCT heddiw_ADV wrth_PART geisio_VERB swyddi_VERB [_PUNCT saib_NOUN ]_PUNCT Ond_CONJ roedd_VERB hi_PRON 'n_PART wahanol_ADJ iawn_ADV ers_ADP dalwm_NOUN pan_CONJ oedd_VERB y_DET pylla_VERB 'n_ADP 'i_PRON hanterth_NOUN fel_CONJ y_PART deallis_VERB i_ADP pan_CONJ es_VERB i_ADP draw_ADV i_ADP glwb_NOUN rygbi_NOUN 'r_DET __NOUN i_ADP gyfarfod_VERB â_ADP __NOUN __NOUN __NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN dau_NUM o_ADP ffrindia_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Ia_NOUN ar_ADP yr_DET amser_NOUN 'na_ADV 's_DET dim_DET lot_PRON o_ADP nhw_PRON 'n_PART gallu_VERB trafeili_NOUN 's_PROPN [_PUNCT =_SYM ]_PUNCT dim_DET [_PUNCT /=_PROPN ]_PUNCT dim_DET ceir_NOUN '_PUNCT da_ADJ nhw_PRON [_PUNCT -_PUNCT ]_PUNCT
O_ADP na_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT wel_ADV pr_PRON '_PUNCT '_PUNCT nny_NOUN o_ADP ti_PRON 'n_PART <_SYM en_PART gair_NOUN =_SYM "_PUNCT "_PUNCT depend_NOUN "_PUNCT "_PUNCT >_SYM dependo_VERB ar_ADP y_DET bysys_NOUN +_SYM
Ia_INTJ ._PUNCT
+_VERB i_ADP fynd_VERB i_ADP lleoliad9_NOUN lleoliad6_NOUN ne_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Wel_CONJ 'r_DET un_NUM peth_NOUN o_ADP nhw_PRON 'n_PART ffindo_VERB 'i_PRON 'n_PART anodd_ADJ dod_VERB aton_VERB ni_PRON so_VERB dylanwad_NOUN +_SYM
Oedd_VERB ia_PRON +_SYM
+_VERB saesneg_NOUN ardaloedd_ADJ fel_ADP lleoliad6_NOUN ddim_PART yn_PART dod_VERB aton_VERB ni_PRON wedyn_ADV ._PUNCT
+_VERB [_PUNCT =_SYM ]_PUNCT na_INTJ [_PUNCT /=_PROPN ]_PUNCT na_CONJ wrth_ADP gwrs_NOUN ._PUNCT
Heddi_VERB wrth_ADP gwrs_NOUN [_PUNCT =_SYM ]_PUNCT ma_VERB nhw_PRON [_PUNCT /=_PROPN ]_PUNCT ma_AUX nhw_PRON 'n_PART gallu_ADV dod_VERB aton_VERB ni_PRON 'n_PART haws_ADJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT ia_PRON [_PUNCT /=_PROPN ]_PUNCT ia_INTJ ._PUNCT
Yn_PART fwy_ADV symudol_ADJ ?_PUNCT
Odyn_NOUN ._PUNCT
Cofio_VERB nôl_ADV i_ADP  _SPACE dechra'_VERBUNCT wech_CONJ dege_NOUN wel_ADV daeth_VERB 'na_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT gwaith_NOUN glo_NOUN y_DET lleoliad_NOUN ond_CONJ oess_X isie_NOUN gweithwyr_NOUN oedd_VERB yn_PART gwaith_NOUN lleoliad_NOUN i_PRON fyn_VERB '_PUNCT lawr_ADV i_ADP 'r_DET gwaith_NOUN glo_NOUN newydd_ADJ oedd_VERB lawr_ADV yn_PART __NOUN [_PUNCT saib_NOUN ]_PUNCT a_CONJ [_PUNCT aneglur_ADJ ]_PUNCT daeth_VERB lot_PRON o_ADP [_PUNCT aneglur_ADJ ]_PUNCT lawr_ADV '_PUNCT da_ADJ ni_PRON pry_VERB '_PUNCT '_PUNCT nny_NOUN achos_NOUN <_SYM aneglur_ADJ 2_NUM >_SYM oedd_VERB le_NOUN oedd_VERB hen_ADJ gartrefi_VERB __NOUN __NOUN __NOUN <_SYM en_PART gair_NOUN =_SYM "_PUNCT "_PUNCT build_X "_PUNCT "_PUNCT >_SYM buildo_VERB nhw_PRON  _SPACE stad_NOUN o_ADP dai_NOUN fan_NOUN 'na_ADV ._PUNCT
504.813_NOUN
1424.96_PROPN
