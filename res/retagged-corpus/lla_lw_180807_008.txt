Bydd_VERB by_VERB +_SYM
O_ADP 'n_PART ni_PRON 'n_PART gwrando_VERB ar_ADP y_DET radio_NOUN ._PUNCT
+_VERB By_PRON '_PUNCT ni_PRON ddim_PART by_VERB nhw_PRON 'na_ADV t'wel_NOUN ._PUNCT
O_ADP 'n_PART ni_PRON 'n_PART gwrando_VERB ar_ADP y_DET radio_NOUN ar_ADP y_DET ffor_NOUN draw_ADV a_CONJ o_AUX 'n_PART hw_PRON 'n_PART sôn_VERB am_ADP wahanol_ADJ enwe_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O'dd_NOUN ._PUNCT
Ar_ADP radio_NOUN dw_AUX 'm_DET 'di_PART clywed_VERB enw_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM ._PUNCT
Oedd_AUX o_PRON 'n_PART sôn_VERB am_ADP oefad_NOUN a_CONJ ryw_DET bethe_NOUN fel'na_PUNCT ._PUNCT
Beth_PRON i_ADP '_PUNCT chi_PRON 'n_PART galw_VERB crempog_NOUN mae_VERB ryw_DET air_NOUN [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
Ffroes_PROPN ._PUNCT
Ffroes_PROPN ._PUNCT
Ffroesen_PROPN am_ADP un_NUM ._PUNCT
Ffroes_NOUN ?_PUNCT
Sai_VERB 'di_PART clywed_VERB e_PRON o_ADP 'r_DET blaen_NOUN ._PUNCT
Na_INTJ ._PUNCT
Nag_CONJ i'ti_VERB 'di_PART clywed_VERB hwnna_PRON o_ADP 'r_DET blaen_NOUN ._PUNCT
Naddo_VERB ._PUNCT
Gog_CONJ gog_NOUN 'i_PRON fi_PRON ._PUNCT
Sai_VERB erioed_ADV 'di_PART clywed_VERB ffroes_NOUN ._PUNCT
Na_INTJ ._PUNCT
Ffroes_PROPN wyt_AUX ti_PRON 'n_PART ddeud_VERB ife_ADV ie_INTJ fi_PRON 'n_PART gwybod_VERB ._PUNCT
Pancos_NOUN fydden_AUX ni_PRON 'n_PART gweud_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_CONJ beth_PRON wyt_AUX ti_PRON 'n_PART dweud_VERB amser_NOUN ti_PRON 'n_PART dodi_VERB ti'mod_AUX  _SPACE sudds_VERB amser_NOUN ti_PRON 'n_PART +_SYM
Ar_ADP dy_DET wallt_NOUN ._PUNCT
+_VERB Ar_ADP dy_DET wallt_NOUN neu_CONJ yn_ADP y_DET bath_NOUN ._PUNCT
Be_DET shampw_NOUN ?_PUNCT
Ie_INTJ ._PUNCT
Beth_PRON ti_PRON 'n_PART gweud_VERB ti_PRON 'n_PART ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
A_CONJ fi_PRON ._PUNCT
Ie_INTJ 'na_ADV ti_PRON ._PUNCT
Na_VERB fe_PRON am_ADP woblyn_NOUN 'na_ADV fe_PRON ie_INTJ ie_X ie_INTJ ._PUNCT
Ti_PRON 'n_PART clirio_VERB woblyn_NOUN oddi_ADP wrthai_VERB ._PUNCT
Mae_VERB mwy_PRON o_ADP wallt_NOUN '_PUNCT da_ADJ chi_PRON 'na_ADV fi_PRON siŵr_ADJ o_ADP fod_VERB ._PUNCT
A_PART mae_VERB enwg_VERB t'wel_NOUN s'dim_DET gair_NOUN gyda_ADP fe_PRON oes_VERB e_PRON ._PUNCT
Am_ADP be_PRON ?_PUNCT
Am_ADP am_ADP woblyn_NOUN ._PUNCT
Nagoes_PROPN dwi_AUX 'n_PART gobeithio_VERB '_PUNCT ny_PRON sudds_VERB ._PUNCT
[_PUNCT giglan_VERB ]_PUNCT sudds_VERB ._PUNCT
Bach_ADJ yn_PART <_SYM aneglur_ADJ 1_NUM >_SYM ._PUNCT
Os_CONJ yw_VERB 'r_DET dŵr_NOUN yn_PART galed_ADJ a_CONJ rywbeth_NOUN a_CONJ ti_PRON 'n_PART aros_VERB 'na_ADV a_CONJ ti_PRON 'n_PART mynd_VERB i_ADP olchi_VERB dy_DET dd'ylo_NOUN a_CONJ mae_VERB 'r_DET dŵr_NOUN hyn_DET reit_ADV sy_VERB 'n_PART woblyn_ADJ ._PUNCT
So_PART chi_PRON '_PUNCT lly_ADV cael_VERB woblyn_NOUN ._PUNCT
Fi_PRON 'n_PART ffeindio_VERB 'na_ADV yn_PART lleoliad_NOUN ._PUNCT
Wyt_VERB ti_PRON ?_PUNCT
Y_DET dŵr_NOUN ydw_VERB dw_AUX i_PRON 'n_PART golchi_VERB llestri_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'Di_PART dŵr_NOUN lleoliad_NOUN '_PUNCT im_NOUN yn_PART galed_ADJ ._PUNCT
Nagyw_NOUN e_PRON fod_VERB yn_PART ry_VERB -_PUNCT rwyddach_NOUN ar_ADP bwys_NOUN y_DET môr_NOUN ?_PUNCT
Na_INTJ na_INTJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
O_ADP '_PUNCT dd_VERB 'i_PRON a_CONJ 'r_DET meic_NOUN 'na_ADV ._PUNCT
Dyna_DET pam_ADV ma_VERB dadcu_NOUN enwb_NOUN efo_ADP 'r_DET thing_NOUN 'na_ADV yn_ADP y_DET yn_ADP y_DET shower_NOUN ._PUNCT
Mae_AUX teulu_NOUN enwg_VERB i_ADP gyd_ADP gyda_ADP fel_ADP ch'mod_VERB y_DET things_NOUN chi_PRON 'n_PART defnyddio_VERB i_ADP +_SYM
Gl'nau_PART ffenestri_NOUN ._PUNCT
+_VERB Glanhau_PROPN ffenestri_NOUN mae_VERB '_PUNCT da_ADJ nhw_PRON nhw_PRON yn_ADP y_DET showers_NOUN ._PUNCT
Wel_ADV ma_VERB 'n_PART gwd_NOUN i_ADP gael_VERB t'wel_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Ie_INTJ can't_NOUN fault_VERB it_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Mae_AUX 'n_PART neud_VERB e_PRON bob_DET -_PUNCT o_ADP dyna_PRON pam_ADV mae_VERB 'r_DET dŵr_NOUN yn_PART galed_ADJ ._PUNCT
Ond_CONJ mae_VERB mae_VERB '_PUNCT da_ADJ fe_PART un_NUM electric_NOUN i_ADP ca_NOUN -_PUNCT casglu_VERB ._PUNCT
O_ADP un_NUM electric_NOUN ._PUNCT
Oh_X duw_NOUN duw_NOUN ._PUNCT
Ie_INTJ nagyw_NOUN e_PRON 'n_PART cael_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mae_AUX fe_PRON 'n_ADP y_DET gor-_NOUN [_PUNCT =_SYM ]_PUNCT na_CONJ na_ADV na_INTJ [_PUNCT /=_PROPN ]_PUNCT mae_VERB fe_PRON 'n_PART gornel_NOUN ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_X ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Mae_VERB ddim_ADP yr_DET un_NUM peth_NOUN ._PUNCT
Os_CONJ digwyddodd_VERB e_PRON bydde_AUX fe_PRON 'n_PART gret_VERB ._PUNCT
Mae_VERB ice_NOUN scraper_NOUN '_PUNCT da_ADJ fi_PRON yn_ADP y_DET car_NOUN a_CONJ fi_PRON 'n_PART ._PUNCT
<_SYM SS_NOUN >_SYM O_ADP ie_X ie_X ie_INTJ [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
O't_AUX ti_PRON 'n_PART gweud_VERB am_ADP oefad_NOUN ._PUNCT
Ie_INTJ ._PUNCT
T'wel_VERB o_ADP 'n_PART ni_PRON byth_ADV yn_PART mynd_VERB i_ADP 'r_DET pwll_NOUN nofio_VERB yn_PART lleoliad_NOUN ._PUNCT
Na_INTJ ?_PUNCT
Y_DET babs_NOUN o_ADP ni_PRON mewn_ADP ._PUNCT
O_ADP i_ADP babs_NOUN ._PUNCT
Ie_INTJ 'na_ADV ryfedd_NOUN yndyfe_VERB ?_PUNCT
Baz_PROPN <_SYM aneglur_ADJ 1_NUM >_SYM ._PUNCT
Mae_VERB nhw_PRON isho_NOUN ailagor_NOUN o_ADP ydyn_VERB nhw_DET ?_PUNCT
Mae_VERB hwnna_PRON mas_NOUN tu_NOUN fas_ADJ nagyw_NOUN e_PRON ?_PUNCT
Odi_ADV odi_NOUN ._PUNCT
Mae_VERB os_CONJ rywbeth_NOUN oes_VERB 'na_ADV ryw_NOUN ymgyrch_NOUN neu_CONJ na_ADV ?_PUNCT
Mae_VERB grwp_NOUN o_ADP bobl_NOUN wrthi_ADP 'n_PART trial_ADJ ._PUNCT
Fi_PRON 'n_PART credu_VERB bod_VERB nhw_PRON 'n_PART trial_ADJ ail_ADJ neud_VERB e_PRON i_PART ddechre_VERB fo_VERB to_NOUN ti'mod_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Nethon_VERB nhw_PRON ymgyrch_NOUN mowr_NOUN actually_CONJ lleoliad_NOUN ._PUNCT
Mae_VERB 'di_PART mynd_VERB rhy_ADV bell_ADJ odyw_ADP e_PRON ?_PUNCT
Do_VERB fe_PRON ?_PUNCT
Yn_PART lleoliad_NOUN ._PUNCT
O_ADP ie_INTJ ._PUNCT
Nethon_AUX nhw_PRON ymgyrch_NOUN yn_PART mynd_VERB i_ADP ail_ADJ agor_VERB mae_VERB e_PRON i_ADP gyd_NOUN yn_PART art_NOUN deco_NOUN ._PUNCT
Ww_ADP neis_ADJ ife_ADV ie_INTJ ._PUNCT
Fel'a_PART dysgodd_VERB enwg_VERB i_ADP oifad_NOUN ._PUNCT
Ie_INTJ ?_PUNCT
A_CONJ be_INTJ ?_PUNCT
Mae_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT oafad_NOUN yn_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Sa_PROPN i_PRON 'n_PART gallu_VERB oafad_NOUN ._PUNCT
Na_VERB fi_PRON ._PUNCT
Ti_PRON 'n_PART gallu_VERB ond_CONJ ti_PRON 'm_DET yn_PART lico_NOUN rhoi_VERB dy_DET ben_NOUN ._PUNCT
Na_INTJ 'na_ADV beth_PRON wedodd_VERB wrthai_VERB ._PUNCT
Chi_PRON 'n_PART neud_VERB y_DET mwfs_NOUN i_ADP gyd_NOUN yn_PART iawn_ADJ ond_CONJ so_AUX chi_PRON 'n_PART rhoi_VERB 'ch_PRON pen_NOUN yn_ADP y_DET dŵr_NOUN so_VERB newch_NOUN chi_PRON byth_ADV oafad_NOUN yn_PART iawn_ADJ wedodd_VERB e_PRON wrthai_VERB ._PUNCT
S'dim_DET hawl_NOUN '_PUNCT da_ADJ ti_PRON deifio_VERB off_ADP y_DET bwrdd_NOUN ._PUNCT
"_PUNCT Pam_ADV 'na_ADV "_PUNCT wedes_NOUN i_ADP ti_PRON ffili_NOUN oafad_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM wedes_NOUN i_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Nagw_PROPN i_PRON 'n_PART trio_VERB ._PUNCT
Dere_VERB lawr_ADV m'an_ADP hyn_PRON bore_NOUN fory_ADV medde_VERB sesiwn_NOUN gynta_ADJ gei_X 'di_PART ddod_VERB mewn_ADP am_ADP ddim_PRON ._PUNCT
Fi_PRON moyn_VERB ti_PRON ddangos_VERB bo_AUX ti_PRON 'n_PART gallu_VERB oafad_NOUN ._PUNCT
O_ADP ie_INTJ ie_INTJ ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
real_VERB hero_ADV fi_PRON 'n_PART gallu_VERB oafad_NOUN dim_DET problem_NOUN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Reit_VERB de_NOUN cere_VERB mewn_ADP i_ADP 'r_DET deep_NOUN end_NOUN gynta_ADJ i_PART neud_VERB yn_PART siŵr_ADJ bo_AUX ti_PRON 'n_PART reit_ADJ +_SYM
Ie_INTJ ._PUNCT
+_VERB Fi_PRON moyn_VERB ti_PRON oafad_NOUN nes_CONJ bod_AUX faint_ADV ti_PRON 'n_PART gallu_VERB ._PUNCT
Dynnodd_VERB e_PRON fi_PRON mas_NOUN ar_ADP ôl_NOUN dwy_NUM awr_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT o'dd_NOUN e_PRON 'di_PART mynd_VERB gytre_ADV ._PUNCT
O_ADP na_PART o't_VERB ti_PRON 'di_PART ._PUNCT
Neud_VERB non_ADP shop_NOUN ._PUNCT
O_ADP ti_PRON wedi_PART oet_VERB ti_PRON ?_PUNCT
O_ADP 'na_ADV dda_ADJ wel_ADV ie_ADP ti'mod_ADV mae_VERB hwnna_PRON 'n_PART ._PUNCT
Ti_PRON 'n_PART olreit_ADJ wedyn_ADV ._PUNCT
Gyfansoddiad_NOUN 'di_PART na_CONJ na_PRON ._PUNCT
Na_INTJ na_INTJ na_PRON ._PUNCT
Mae_VERB hwnna_PRON 'n_PART crazy_NOUN nagyw_NOUN e_PRON ._PUNCT
Odi_ADP ._PUNCT
Ond_CONJ oedd_VERB dy_DET wncwl_NOUN 'di_PART 'n_PART mynd_VERB i_PART nofio_VERB yn_PART  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Wncwl_NOUN viv_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT o'dd_NOUN e_PRON o'dd_NOUN e_PRON yn_ADP ei_DET wythdege_NOUN ._PUNCT
O'dd_NOUN o'dd_NOUN ._PUNCT
O'dd_NOUN e_PRON 'n_PART mynd_VERB bob_DET bore_NOUN ti'mod_ADJ neu_CONJ dwyweth_NOUN yr_DET wythnos_NOUN ._PUNCT
Na_INTJ oedd_VERB e_PRON ddi-_NOUN o_ADP nhw_PRON 'di_PART newid_VERB e_DET nagon_VERB nhw_PRON unwaith_ADV oedd_VERB yr_DET eli_NOUN 'na_ADV di_PRON enill_ADJ newidon_VERB nhw_PRON fe_PRON o'dd_NOUN e_PRON fel_ADP "_PUNCT o_ADP so_VERB fe_NOUN mor_ADV dda_ADJ a_CONJ 'na_ADV "_PUNCT ._PUNCT
Ti'mo_VERB Ellie_PROPN Simmons_PROPN ife_ADV ._PUNCT
<_SYM SS_NOUN >_SYM O_ADP ie_X ie_INTJ ._PUNCT
Oedd_VERB hi_PRON my_NOUN -_PUNCT o_ADP 'n_PART nhw_PRON 'di_PART newid_VERB a_CONJ o_PRON 'n_PART nhw_PRON 'n_PART rhoi_VERB o_ADP 'n_PART nhw_PRON 'di_PART rhoi_VERB lanes_NOUN i_ADP ti_PRON wedyn_ADV o'dd_NOUN e_PRON 'm_DET yn_PART lico_NOUN '_PUNCT ny_PRON ._PUNCT
O_ADP na_PART ._PUNCT
Oedd_VERB rhaid_VERB aros_VERB yn_ADP 'i_PRON lane_NOUN ti'mo_ADJ pryd_ADV oedd_AUX o_PRON 'n_PART mynd_VERB yn_PART gynnar_ADJ yn_PART bora_VERB bobl_NOUN yn_PART o'dd_NOUN o_ADP 'm_DET yn_PART licio_VERB '_PUNCT ny_PRON o_ADP gwbl_NOUN ._PUNCT
O'dd_NOUN e_PRON 'm_DET yn_PART lico_NOUN 'na_ADV nagoedd_NOUN e_PRON ?_PUNCT
Na_INTJ ._PUNCT
Joio_NOUN o_PRON 'n_PART ni_PRON 'n_PART neud_VERB ._PUNCT
O_ADP na_PART ti'mo_VERB os_CONJ oet_AUX ti_PRON 'n_PART gallu_ADV mynd_VERB fel_ADP o't_ADP ti_PRON moyn_NOUN ti'mod_VERB ife_ADV ._PUNCT
Mae_VERB 'r_DET llun_NOUN mae_VERB 'r_DET llun_NOUN fi_PRON 'n_PART '_PUNCT whilo_VERB amdano_ADP fe_PRON fan_NOUN hyn_DET nawr_ADV mae_AUX fe_PRON wedi_PART bod_VERB ar_ADP y_DET sefydliad_NOUN y_DET south_NOUN wales_VERB sefydliad_NOUN ife_ADV droaon_NOUN ._PUNCT
Fi_PRON 'n_PART trio_ADV ffeindio_VERB fe_NOUN nawr_ADV ._PUNCT
O_ADP y_DET gang_NOUN 'na_ADV 'n_PART nofio_VERB ?_PUNCT
Ie_INTJ ._PUNCT
gang_NOUN yn_PART jwmpo_NOUN mewn_ADP i_PRON pwl_ADV ._PUNCT
O_ADP fi_PRON 'di_PART weld_VERB e_DET do_INTJ ._PUNCT
Fi_PRON 'n_PART trio_ADV ffeindio_VERB enwg_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Wyt_VERB ti_PRON 'n_ADP y_DET llun_NOUN 'na_ADV ?_PUNCT
Wyt_AUX ti_PRON yn_PART hwnna_PRON ?_PUNCT
Be_INTJ ?_PUNCT
gang_NOUN dw_AUX 'm_DET yn_PART gwybo_VERB dim_DET byd_NOUN am_ADP hwn_PRON ._PUNCT
Beth_PRON yw_VERB e_PRON ?_PUNCT
O_ADP mae_VERB mae_VERB llwyth_PRON o_ADP foi_NOUN -_PUNCT bois_VERB i'nhw_PRON gyd_ADP ife_ADV ?_PUNCT
Ie_INTJ bois_VERB i'nhw_PRON ._PUNCT
Bois_VERB i'nhw_PRON gyd_ADP yndyfe_VERB ._PUNCT
Ie_INTJ fi_PRON moyn_VERB fi_PRON moyn_ADV cael_ADV gwybod_VERB pam_ADV oedd_VERB y_DET merched_NOUN ddim_PART ynddo_ADP fe_PRON ._PUNCT
D'yfe_VERB bechgyn_NOUN i'nhw_PRON gyd_ADP ife_ADV ._PUNCT
Ie_INTJ ._PUNCT
O_ADP ella_ADV bo_VERB nhw_PRON 'm_DET yn_PART lico_NOUN ._PUNCT
O_ADP mae_VERB nhw_PRON ar_ADP y_DET bwr_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Fi_PRON moyn_ADV neud_VERB e_PRON ar_ADP ran_NOUN ._PUNCT
Do_AUX fi_PRON 'di_PART weld_VERB e_PRON ._PUNCT
walrus_ADJ walrus_ADJ dip_NOUN ._PUNCT
Beth_PRON yw_VERB hwnnw_DET te_NOUN ?_PUNCT
Yn_PART [_PUNCT saib_NOUN ]_PUNCT lleoliad_NOUN ._PUNCT
Anifail_PROPN a_CONJ mwstash_NOUN ._PUNCT
Ie_INTJ ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Na_VERB yn_PART lleoliad_NOUN ar_ADP boxing_NOUN day_AUX chi_PRON 'n_PART mynd_VERB mewn_ADP fancy_NOUN dress_VERB mewn_ADP i_ADP 'r_DET môr_NOUN ._PUNCT
O_ADP ie_INTJ ._PUNCT
O_ADP mae_AUX nhw_PRON 'n_PART neud_VERB 'na_ADV yn_PART lleoliad_NOUN hefyd_ADV neu_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP falle_NOUN lleoliad_NOUN yw_VERB e_PRON de_NOUN ._PUNCT
lleoliad_NOUN falle_ADJ mae_AUX nhw_PRON 'n_PART neud_VERB e_PRON ._PUNCT
Ie_INTJ ._PUNCT
Mae_AUX nhw_PRON 'n_PART neud_VERB e_PRON 'n_PART lleoliad_NOUN wnna_NOUN wnna_NOUN yw_VERB 'r_DET llun_NOUN ti_PRON 'n_PART meddwl_VERB ?_PUNCT
Ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Hwnna_PRON 'n_PART wag_ADJ ._PUNCT
Ie_INTJ <_SYM aneglur_ADJ 1_NUM >_SYM a_CONJ 'th_VERB e_DET mas_NOUN ar_ADP y_DET sefydliad_NOUN tro_NOUN cynta_ADJ o'dd_PART enwg_VERB 'di_PART bo_VERB 'n_PART '_PUNCT whare_VERB golff_NOUN +_SYM
O_ADP ie_INTJ ._PUNCT
+_VERB A_CONJ dath_NOUN e_PRON tua_ADV thre_NOUN a_CONJ wedes_VERB i_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ ._PUNCT
Fi_PRON moyn_VERB ti_PRON ddishgwl_NOUN ar_ADP y_DET llun_NOUN hyn_PRON yn_PART sefydliad_NOUN <_SYM ebychiad_NOUN >_SYM fi_PRON 'n_PART gwybod_VERB fi_PRON yn_ADP y_DET llun_NOUN 'na_ADV medde_VERB fe_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ti_PRON yn_PART hwnna_PRON wyt_VERB ti_PRON ?_PUNCT
Fi_PRON 'n_PART cofio_VERB nhw_PRON 'n_PART tynnu_VERB fe_PRON medde_VERB fe_PRON ._PUNCT
O_ADP ti'mo_NOUN be_PRON sy_AUX 'n_PART neud_VERB fi_PRON '_PUNCT werthin_NOUN amser_NOUN mama_NOUN enwg_VERB cyfenw_NOUN yn_PART tynnu_VERB llun_NOUN tr'amser_ADJ ma_VERB 'n_PART joio_ADJ odyw_VERB e_PRON ._PUNCT
O_ADP mae_VERB wrth_ADP 'i_PRON fodd_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Hwnna_VERB ti_PRON yn_PART hwnna_PRON ._PUNCT
Achos_CONJ wedon_VERB nhw_PRON bo_VERB nhw_PRON 'di_PART mynd_VERB i_ADP rywle_ADV a_PART wedodd_VERB hi_PRON o_ADP ma_PRON enwg_VERB ni_PRON 'n_PART gadel_VERB enwg_VERB yn_ADP y_DET pwll_NOUN nofio_VERB trwy_ADP 'r_DET nos_NOUN wedon_VERB nhw_PRON ._PUNCT
Yn_PART lleoliad_NOUN ?_PUNCT
A_CONJ wedyn_ADV i_ADP '_PUNCT ni_PRON 'n_PART mwynhau_ADV wedodd_VERB hi_PRON ._PUNCT
Wel_INTJ 'na_ADV gymeriad_NOUN ._PUNCT
Ti_PRON yw_VERB hwnna_PRON ?_PUNCT
Dda_ADJ cofia_VERB i_ADP 'w_PRON oedran_NOUN e_PRON ._PUNCT
Mae_VERB hwnna_PRON 'n_PART uffarn_ADJ o_ADP lun_NOUN da_ADJ achos_ADP pryd_ADV gath_NOUN hwnna_PRON 'i_PRON dynnu_VERB ?_PUNCT
Mae_AUX hwnna_PRON 'n_PART briliant_VERB ._PUNCT
Dechre_VERB 'r_DET chwedege_NOUN ni_PRON 'di_PART bod_AUX yn_PART gweithio_VERB mas_NOUN fi_PRON 'n_PART credu_VERB ife_ADV ?_PUNCT
Ie_INTJ ie_INTJ ._PUNCT
Achos_CONJ ni_PRON '_PUNCT llu_NOUN enwi_VERB lot_PRON o_ADP 'r_DET bois_NOUN yn_ADP y_DET llun_NOUN 'na_ADV ._PUNCT
Ie_INTJ ond_CONJ gallet_VERB ti_PRON 'm_PART neud_VERB hynna_PRON nawr_ADV achos_CONJ health_NOUN and_NOUN safety_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP bydden_AUX nhw_PRON 'm_DET yn_PART gadel_VERB ti_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Se_DET thirty_NOUN ohonon_ADP ni_PRON 'n_PART jwmpo_NOUN off_ADP y_DET bwrdd_NOUN ._PUNCT
Dim_PRON ond_ADP dim_PRON ond_ADP un_NUM ar_ADP y_DET tro_NOUN ti_PRON 'n_PART cael_VERB nawr_ADV ._PUNCT
Faint_ADV sy_VERB ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT Pa_PART un_NUM wyt_VERB ti_PRON de_NOUN yr_DET un_NUM ar_ADP lly_X -_PUNCT breichie_VERB 'na_ADV ar_ADP agor_VERB ife_ADV ?_PUNCT
Ie_INTJ ie_INTJ ._PUNCT
Na_VERB r'un_NOUN deifio_VERB [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Ôôô_INTJ ._PUNCT
Ond_CONJ bechgyn_NOUN i'nhw_PRON i_ADP gyd_NOUN ?_PUNCT
Na_INTJ pam_ADV o_ADP 'n_PART ni_PRON 'n_PART deifio_VERB achos_NOUN o_ADP 'n_PART ni_PRON 'n_PART deifio_ADV mynd_VERB wedyn_ADV '_PUNCT ny_PRON oedd_VERB pawb_PRON yn_PART jwmpo_NOUN mewn_ADP a_CONJ ._PUNCT
Ie_INTJ o't_ADP ti_PRON 'n_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ar_ADP dy_DET ben_NOUN 'di_PART ._PUNCT
O_ADP ma_VERB hwnna_PRON 'n_PART lun_NOUN da_ADJ ondyw_VERB e_PRON ?_PUNCT
Mae_AUX hwnna_PRON 'n_PART briliant_VERB ._PUNCT
Fi_PRON 'n_PART ffili_ADV ffeindio_VERB fe_PRON 'n_PART fan_NOUN hyn_DET fi_PRON 'n_PART gwybod_VERB bod_VERB e_DET fan_NOUN hyn_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Hwnna_PRON yn_ADP y_DET calendr_NOUN glomaner_ADJ ._PUNCT
Mae_AUX wedi_PART bod_VERB ._PUNCT
O_ADP mae_VERB e_PRON wedi_PART ._PUNCT
Mae_VERB 'di_PART bod_VERB ._PUNCT
Weloch_VERB chi_PRON 'r_DET llun_NOUN ddo_CONJ o'dd_NOUN yrr_ADV enwg_VERB 'na_ADV o_ADP lleoliad_NOUN yr_DET ffotograffydd_NOUN ._PUNCT
O_ADP ti_PRON 'di_PART gweld_VERB llun_NOUN enwg_VERB cyfenw_NOUN o_ADP yn_PART __NOUN y_DET llun_NOUN  _SPACE ._PUNCT
O'dd_NOUN hi_PRON 'n_PART bwrw_VERB glaw_NOUN yn_PART lleoliad_NOUN bore_NOUN ma_ADP ma_PRON enwb_NOUN 'di_PART ffono_NOUN fi_PRON ._PUNCT
O_ADP odd_VERB hi_PRON 'n_PART bwrw_VERB glaw_NOUN yn_ADP bob_DET man_NOUN bore_NOUN ma_AUX fi_PRON 'n_PART credu_VERB o't_ADP ti_PRON 'n_PART gweud_VERB bod_VERB hi_PRON 'n_PART wael_ADJ ._PUNCT
Ffones_VERB i_ADP ie_X jyst_X ar_ADP ol_NOUN i_ADP fi_PRON gadel_VERB ._PUNCT
Amser_NOUN iawn_ADV [_PUNCT saib_NOUN ]_PUNCT enwg_VERB cyfenw_NOUN photography_VERB [_PUNCT saib_NOUN ]_PUNCT o_ADP 'ch_PRON chi_PRON 'di_PART cael_VERB y_DET boi_NOUN rong_ADJ un_NUM __NOUN fi_PRON moyn_NOUN +_SYM
Wel_ADV mae_VERB 'n_PART neis_ADJ fan'yn_PROPN ._PUNCT
+_VERB Ife_PROPN ?_PUNCT
Mae_AUX ddim_PART yn_PART ry_VERB dwym_NOUN odyw_VERB e_PRON mae_VERB 'n_PART lyfli_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Na_INTJ 'di_PART mae_VERB jyst_ADV yn_PART hyfryd_ADJ 'ma_ADV ._PUNCT
Oh_INTJ o'dd_NOUN hi_PRON 'n_PART dwym_ADJ yn_PART lleoliad_NOUN wythnos_NOUN d'wetha_ADJ ._PUNCT
Hwnna_PRON hwnna_PRON ._PUNCT
O_ADP wel_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Weden_VERB ni_PRON bod_AUX e_PRON 'n_PART tynnu_VERB llun_NOUN y_DET ddau_NUM yn_ADP y_DET canw_NOUN a_CONJ digwydd_ADV bod_VERB tu_NOUN ôl_NOUN bod_VERB y_DET dolffin_NOUN mas_NOUN o_ADP 'r_DET dŵr_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Achos_CONJ gallet_VERB ti_PRON fynd_VERB nawr_ADV ti'mo_ADJ ti'mod_VERB +_SYM
Mae_AUX hwnna_PRON 'n_PART briliant_VERB ._PUNCT
+_VERB  _SPACE enwb2_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP 'n_PART deud_VERB jyst_ADV digwydd_VERB bod_AUX bod_AUX e_PRON 'n_PART tynnu_VERB llun_NOUN y_DET ddau_NUM yn_ADP y_DET canw_NOUN ._PUNCT
Mae_AUX 'n_PART dysgu_VERB yn_PART ysgol_NOUN yw_VERB hi_PRON nawr_ADV falle_ADJ ?_PUNCT
Oh_INTJ na_INTJ '_PUNCT sa_PRON i_PRON 'n_PART nabod_VERB hi_PRON ._PUNCT
Gwraig_NOUN enwg_VERB cyfenw_NOUN mae_AUX nhw_PRON 'n_PART byw_VERB yn_PART __NOUN ._PUNCT
O_ADP reit_NOUN ._PUNCT
Fi_PRON 'n_PART cofio_VERB llynedd_ADV odd_VERB enwg_VERB 'di_PART bod_VERB a_CONJ 'r_DET merched_NOUN lawr_ADV o_ADP 'n_PART nhw_PRON 'di_PART bod_VERB lawr_ADV i_ADP 'r_DET Cei_PROPN a_CONJ o_ADP 'n_PART nhw_PRON 'di_PART mynd_VERB mas_NOUN ar_ADP y_DET trip_NOUN ar_ADP y_DET bad_NOUN ._PUNCT
Ie_INTJ fel'a_SYM mae_AUX 'n_PART digwydd_VERB t'wel_NOUN ife_ADV ._PUNCT
Ti_PRON 'n_PART gallu_ADV mynd_VERB nawr_ADV talu_VERB fynd_VERB a_CONJ 'r_DET merched_NOUN ._PUNCT
Ie_INTJ ie_X ie_X a_CONJ ddim_ADV gweld_VERB un_NUM ooo_NOUN mae_VERB hwnna_PRON ._PUNCT
Ond_CONJ  _SPACE ._PUNCT
Ti_PRON 'n_PART gweld_VERB nhw_PRON reit_NOUN amal_ADJ yn_PART lleoliad_NOUN ._PUNCT
Nagych_VERB chi_PRON ?_PUNCT
Dyle_VERB fe_PRON dod_VERB lawr_ADV fwy_ADJ ?_PUNCT
Dim_PRON yn_PART lleoliad_NOUN na_INTJ ._PUNCT
Lawr_ADV at_ADP lleoliad_NOUN mae_VERB nhw_PRON ife_ADV ?_PUNCT
Odyn_NOUN mae_VERB nhw_PRON lawr_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Fel_CONJ i_PRON '_PUNCT fi_PRON 'n_PART cerdded_VERB o_ADP gwmpas_NOUN y_DET môr_NOUN '_PUNCT im_ADJ an_DET arall_ADJ i_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Cerdded_VERB i_ADP fewn_ADP wyt_VERB ti_PRON i_ADP 'r_DET sefydliad_NOUN ti_PRON 'm_DET yn_PART agos_ADJ i_ADP 'r_DET môr_NOUN i_ADP 'r_DET sefydliad_NOUN ._PUNCT
So_PART chi_PRON 'n_PART mynd_VERB lawr_ADV wythnos_NOUN nesa_ADJ nawr_ADV de_NOUN ._PUNCT
Fyddwn_VERB ni_PRON 'na_ADV nos_NOUN fawrth_ADJ a_CONJ dydd_NOUN mercher_NOUN wythnos_NOUN nesa'_ADJUNCT sbo_PRON ._PUNCT
Wel_INTJ 'na_ADV neis_ADJ ._PUNCT
Mae_VERB 'i_PRON brawd_NOUN +_SYM
Mae_AUX enwg6_PRON yn_PART gweithio_VERB ._PUNCT
+_SYM I_ADP 'n_PART meddwl_ADV dod_VERB lan_ADV wythnos_NOUN hyn_PRON ._PUNCT
Brawd_NOUN enwg3_NOUN a_CONJ 'i_PRON wraig_NOUN a_CONJ 'r_DET wyron_NOUN 'di_PART bod_VERB lan_ADV 'na_ADV ._PUNCT
O_ADP do_PRON fe_NOUN neis_ADJ ._PUNCT
A_PART ma_AUX nhw_PRON 'di_PART cael_VERB cwpl_NOUN o_ADP ddiwrnode_ADV hyfryd_ADJ yndyn_ADP nhw_PRON ?_PUNCT
Beth_PRON yw_VERB 'r_DET amser_NOUN ma_VERB lods_NOUN o_ADP bobl_NOUN gyda_ADP bwyd_NOUN ?_PUNCT
Pryd_ADV i_ADP '_PUNCT ti_PRON 'n_PART gweithio_VERB wythnos_NOUN nesa_ADJ ti_PRON bant_VERB dydd_NOUN mercher_NOUN ?_PUNCT
Dydd_NOUN mercher_NOUN bant_ADV dydi_VERB siŵr_ADV o_ADP fod_VERB ._PUNCT
Ôô_NOUN falle_VERB posiblrwydd_ADJ os_CONJ chi_PRON lawr_ADV ar_ADP y_DET dydd_NOUN mercher_NOUN ._PUNCT
Ni_PART 'n_PART gobeithio_ADV mynd_VERB lan_ADV nos_NOUN fawrth_ADJ i'ni_NOUN enwg_VERB ._PUNCT
P'nawn_VERB dy_DET '_PUNCT mowrth_NOUN falle_ADJ ?_PUNCT
A_CONJ 'r_DET tywydd_NOUN ni_PRON 'n_PART gael_VERB 'na_ADV t'wel_NOUN ._PUNCT
O_ADP ma_VERB hwnna_PRON 'n_PART lyfli_NOUN ._PUNCT
Ôôô_INTJ ._PUNCT
Beth_PRON yw_VERB 'r_DET llun_NOUN 'na_ADV nawr_ADV fi_PRON ffili_NOUN gweld_VERB ._PUNCT
Ti_PRON ti_PRON '_PUNCT y_DET pool_NOUN ._PUNCT
Ti_PRON 'n_ADP yr_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Blwyddyn_NOUN dwetha_VERB yw_VERB hwnna_PRON ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Blwyddyn_NOUN dwetha_VERB oedd_VERB e_PRON ie_INTJ ._PUNCT
Ti'o_VERB oh_ADJ mae_VERB 'n_PART stico_NOUN llunie_ADJ ar_ADP hwn_PRON weithie_NOUN a_CONJ mae_VERB rywun_NOUN hela_VERB comment_VERB i_ADP fi_PRON ._PUNCT
Nagw_ADV i_ADP 'di_PART gweld_VERB nhw_PRON ._PUNCT
Ma_AUX enwg_VERB yn_PART tynnu_VERB llunie_ADJ '_PUNCT ta_CONJ le_AUX ni_PRON 'n_PART mynd_VERB i_PART gael_VERB brecwast_NOUN peth_NOUN 'na_ADV mae_AUX enwg_VERB yn_PART tynnu_VERB llun_NOUN a_CONJ wedyn_ADV ti'mo_ADJ be_PRON mae_AUX nhw_PRON 'n_PART gweud_VERB cyn_ADP bo_VERB hir_ADJ '_PUNCT ta_CONJ nai_PART gyd_ADP ni_PRON 'n_PART neud_VERB yw_VERB bwyta_NOUN ._PUNCT
Wel_INTJ 'na_ADV beth_PRON wedes_NOUN i_ADP nawr_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Nai_PART gyd_ADP chi_PRON 'n_PART neud_VERB yw_VERB bwyta_NOUN ._PUNCT
Ie_INTJ ond_CONJ dyw_VERB 'na_ADV 'm_DET yn_PART wir_ADJ oedd_VERB digwydd_VERB bod_VERB gwylie_VERB '_PUNCT da_ADJ enwg_VERB buon_ADP ni_PRON bant_VERB am_ADP wythnos_NOUN do_PRON ond_ADP wedyn_ADV jyst_ADV fan_NOUN hyn_DET a_CONJ f'nco_NOUN ._PUNCT
Mae_VERB rhaid_VERB fi_PRON gweud_VERB ffefryn_NOUN peth_NOUN fi_PRON yw_VERB pan_CONJ fi_PRON 'n_PART gwitho_VERB a_CONJ chi_PRON 'n_PART anfon_VERB llunie_ADP "_PUNCT dyma_DET ble_ADV i'ni_NOUN heddi_NOUN ._PUNCT
Dyma_ADV be_ADP fi_PRON 'n_PART byta_VERB "_PUNCT fi_PRON fel_ADP diolch_INTJ dadi_AUX fi_PRON 'n_PART gwitho_VERB heddi_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Fi_PRON 'n_PART cael_VERB nhw_PRON 'n_PART amal_ADJ fel_ADP hyn_PRON so_ADV and_ADJ so_ADV so_ADV and_ADJ so_ADJ ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Byddai_AUX literally_NOUN yn_PART tecstio_VERB fe_PRON fel_ADP diolch_NOUN dad_ADP fi_PRON jyst_ADV yn_PART cael_VERB ham_NOUN sandwich_ADJ mae_VERB fel_ADP "_PUNCT o_ADP fi_PRON newyd_ADV cael_VERB tsips_NOUN a_CONJ stêc_NOUN "_PUNCT ti_PRON fel_ADP oce_NOUN dadi_ADJ ._PUNCT
Wel_CONJ diawl_PRON wyt_VERB ti_PRON 'n_PART gin_NOUN house_NOUN fory_ADV llun_NOUN -_PUNCT lli_NOUN lleoliad_NOUN ._PUNCT
Mae_VERB 'r_DET gin_NOUN house_ADP fi_PRON 'di_PART gweld_VERB ._PUNCT
O_ADP ma_VERB 'r_DET gin_NOUN house_NOUN lyfli_NOUN ._PUNCT
Oes_VERB e_PRON ?_PUNCT
Mae_VERB jin_NOUN bar_NOUN fan_NOUN hyn_DET ._PUNCT
O_ADP jin_NOUN o_ADP be_PRON ?_PUNCT
Yn_ADP y_DET be_NOUN yn_ADP y_DET steddfod_NOUN ?_PUNCT
A_PART mae_VERB mam_NOUN yn_PART hapus_ADJ ._PUNCT
Na_VERB fi_PRON 'n_PART lico_NOUN bach_ADJ o_ADP jin_NOUN ._PUNCT
Dyw_VERB e_PRON jyst_ADV lawr_ADV o_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Be_ADP y_DET stondine_NOUN ?_PUNCT
Oes_VERB +_SYM
Neu_CONJ ?_PUNCT
+_VERB Mae_VERB stondin_NOUN ._PUNCT
Mae_VERB bar_NOUN ar_ADP ochr_NOUN ._PUNCT
Bar_NOUN lan_ADV ie_INTJ ie_INTJ ._PUNCT
Dyw_VERB e_PRON ddim_PART yn_PART amlwg_ADJ mae_VERB fe_PART reit_VERB draw_ADV ._PUNCT
Bar_NOUN jin_PRON mae_AUX 'n_PART gweud_VERB ife_ADV ?_PUNCT
O_ADP ie_X bar_NOUN jin_X ._PUNCT
Mae_AUX nhw_PRON 'n_PART neud_VERB ble_ADV mae_VERB 'r_DET jin_NOUN nawr_ADV mae_AUX nhw_PRON 'n_PART neud_VERB hyn_PRON ._PUNCT
O_ADP mae_VERB sawl_ADJ un_NUM +_SYM
Lan_ADV yn_PART sefydliad3_ADJ sefydliad3_ADJ ._PUNCT
sefydliad3_NOUN 'di_PART 'r_DET un_NUM nesa_ADJ ._PUNCT
sefydliad_NOUN jin_NOUN ._PUNCT
O_ADP jin_X tasto_VERB 'n_PART ni_PRON hwnna_PRON wythnos_NOUN dwetha_ADJ ._PUNCT
O'dd_NOUN e_PRON 'n_PART neis_ADJ ._PUNCT
Yfes_VERB 'di_PART fe_PRON ddim_PART fi_PRON ._PUNCT
O_ADP na_PART oedd_VERB e_PRON 'n_PART neis_ADJ ?_PUNCT
Na_INTJ myfeni_VERB ._PUNCT
'_PUNCT Sa_NOUN i_PRON 'n_PART lico_NOUN jin_X ._PUNCT
Na_INTJ na_INTJ ._PUNCT
Na_INTJ na_PART fi_PRON ma_VERB fel_ADP sebon_NOUN scent_ADJ ._PUNCT
A_CONJ ti'mo_VERB be_ADP fi_PRON 'n_PART fi_PRON 'n_PART yfed_VERB e_PRON reit_NOUN a_CONJ mae_VERB 'r_DET botel_NOUN f'yna'_ADJUNCT da_ADJ fi_PRON a_CONJ dim_PRON ond_ADP dropyn_NOUN bach_ADJ fi_PRON 'di_PART ._PUNCT
Reit_VERB gai_VERB fan_NOUN hyn_DET dim_DET blydi_VERB mae_VERB 'n_PART cal_ADJ jin_X a_CONJ tonic_NOUN +_SYM
Na_INTJ ._PUNCT
+_VERB Ti_PRON wastad_ADV ar_ADP y_DET jin_NOUN a_CONJ tonic_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_CONJ '_PUNCT sa_PRON i_ADP 'n_PART berson_NOUN alcohol_ADJ o_ADP gwbl_NOUN a_CONJ gweud_VERB y_DET gwir_NOUN '_PUNCT thot_ADP ti_PRON '_PUNCT sa_PRON i_PRON 'n_PART yfed_VERB lot_PRON '_PUNCT sa_PRON i_ADP 'n_PART lico_NOUN fe_PRON a_CONJ fi_PRON 'n_PART dwli_VERB ar_ADP hwnna_PRON ._PUNCT
Ond_CONJ os_CONJ ti_PRON 'n_PART rhoi_VERB lot_PRON o_ADP tonic_NOUN mae_VERB 'di_PART yfed_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_CONJ 'n_PART ffrind_NOUN i_ADP wedodd_VERB so_PART ti_PRON 'di_PART dodi_VERB dim_DET tonic_NOUN yn_PART hwnna_PRON ._PUNCT
Wel_INTJ dw_AUX i_ADP fod_VERB i_ADP te_NOUN wedes_NOUN i_PRON ?_PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
enwb_AUX ti_PRON 'n_PART swnio_VERB fel_ADP reit_NOUN alcoholic_NOUN ._PUNCT
Mae_AUX 'n_PART ffein_NOUN wedes_NOUN i_PRON ._PUNCT
Ti_PRON 'di_PART blasu_VERB rhai_PRON o_ADP 'r_DET blase_NOUN newydd_ADJ though_ADP fi_PRON 'di_PART tasto_VERB reina_NOUN rhubarb_NOUN and_ADJ ginger_ADP [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Odi_ADV mae_VERB hwnna_PRON 'n_PART neis_ADJ ._PUNCT
Fi_PRON 'n_PART lico_NOUN hwnnw_DET ._PUNCT
Mae_VERB hwnnw_PRON 'n_PART neis_ADJ odi_NOUN ._PUNCT
A_CONJ fi_PRON 'di_PART cael_VERB tast_NOUN o_ADP 'r_DET un_NUM elderflower_NOUN a_CONJ rywbeth_NOUN ond_CONJ mae_VERB rai_DET jin_NOUN sefydliad_NOUN ethon_VERB ni_PRON +_SYM
O_ADP sai_VERB 'di_PART trio_VERB rheina_NOUN ._PUNCT
+_VERB I_ADP 'r_DET gwyl_NOUN fwyd_NOUN yn_PART lleoliad12_ADJ ryw_DET bythefnos_NOUN yn_PART ôl_NOUN ._PUNCT
O_ADP ie_INTJ ie_INTJ ._PUNCT
A_PART ethon_VERB ni_PRON siarad_VERB '_PUNCT da_ADJ 'r_DET boi_NOUN achos_NOUN o_ADP 'n_PART ni_PRON 'n_PART gweu'tho_VERB fe_PRON yr_DET unig_ADJ bryd_NOUN o_ADP 'n_PART ni_PRON 'di_PART clywed_VERB am_ADP sefydliad_NOUN ie_INTJ +_SYM
Ie_INTJ dawnsio_VERB ._PUNCT
+_VERB Dawnsiwyr_NOUN sefydliad3_ADJ a_CONJ o_PRON 'n_PART ni_PRON 'di_PART bod_VERB lawr_ADV yn_ADP y_DET neuadd_NOUN a_CONJ buodd_VERB e_PRON f'yni_VERB a_CONJ wel_ADV sôn_VERB am_ADP perfeddion_NOUN gwlad_NOUN ._PUNCT
Ie_INTJ odi_VERB mae_AUX e_PRON 'n_PART dyw_VERB e_PRON ydi_VERB ._PUNCT
A_CONJ y_DET ddou_NOUN hyn_PRON un_NUM o_ADP nhw_PRON 'di_PART dysgu_VERB siarad_VERB Cymraeg_PROPN ._PUNCT
Ie_INTJ ._PUNCT
A_PART mae_VERB 'r_DET ddou_NOUN o_ADP nhw_PRON dim_DET dim_PRON ond_ADP jin_ADV pur_ADV sy_VERB '_PUNCT da_ADJ chi_PRON nawr_ADV ar_ADP hyn_PRON o_ADP bryd_NOUN neu_CONJ os_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP ie_INTJ ._PUNCT
Dim_PRON ond_ADP un_NUM sy_VERB gydan_ADP ni_PRON nawr_ADV medde_VERB fe_PRON ni_PRON 'n_PART dechre_ADV off_ADP a_CONJ dim_PRON ond_ADP cwpl_NOUN bach_ADJ o_ADP lefydd_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Newydd_ADJ ddechre_ADP ie_INTJ ie_INTJ ._PUNCT
Sy_VERB gydan_VERB nhw_PRON ma_VERB dim_PRON ond_ADP 'di_PART dechre_ADV mae_VERB nhw_PRON +_SYM
Dim_PRON ond_ADP 'di_PART dechre_ADV ma_VERB nhw_PRON ?_PUNCT
+_VERB Ers_ADP cwpl_NOUN o_ADP fisoedd_NOUN ._PUNCT
A_PART mae_VERB fe_PRON fan_NOUN hyn_DET odyw_VERB e_PRON ?_PUNCT
Na_INTJ ._PUNCT
Fan_NOUN hyn_DET ti_PRON 'di_PART cael_VERB e_DET wedes_NOUN ti_PRON de_NOUN ?_PUNCT
Na_INTJ yn_PART lleoliad_NOUN wedes_NOUN i_PRON ._PUNCT
O_ADP reit_INTJ ie_INTJ ie_X ie_INTJ ._PUNCT
Mae_VERB gal_NOUN yn_ADP y_DET jin_NOUN house_ADV ._PUNCT
Odi_ADP ._PUNCT
A_PART mae_VERB signut_NOUN gin_NOUN mae_VERB hwnnw_PRON rywle_ADV lleoliad_NOUN rywle_ADV ma_VERB hwnnw_PRON ._PUNCT
Chi_PRON 'n_PART myn_VERB lawr_ADV i_ADP 'r_DET lleoliad_NOUN lot_PRON hefyd_ADV ._PUNCT
<_SYM SS_X >_SYM Odyn_X ._PUNCT
Odyn_NOUN ni_PRON 'n_PART mynd_VERB lawr_ADV y_DET lleoliad_NOUN odyn_NOUN odyn_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mae_VERB 'n_PART rwydd_NOUN o_ADP le_NOUN i'ni_NOUN t'wel_NOUN le_AUX ni_PRON 'n_PART byw_VERB mae_VERB 'n_PART rwydd_NOUN ._PUNCT
Ond_CONJ mae_AUX ti'mo_ADJ amser_NOUN yn_PART ôl_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Na_VERB beth_PRON od_DET ti_PRON wastad_ADV da_ADJ 'i_PRON ._PUNCT
O_ADP ie_INTJ wel_NOUN ._PUNCT
Diolch_INTJ diolch_INTJ ._PUNCT
Mae_VERB rhaid_VERB i_ADP rywun_NOUN tynnu_VERB 'r_DET llunie_VERB ._PUNCT
Na_VERB beth_PRON o_ADP 'n_PART ni_PRON 'n_PART mynd_VERB i_PART weud_VERB fe_PRON sy_AUX 'n_PART tynnu_VERB 'r_DET llun_NOUN ._PUNCT
Ie_INTJ gwed_NOUN y_DET gwir_NOUN bydden_AUX ni_PRON ddim_PART yn_PART botheran_NOUN dynnu_VERB unrhyw_DET lun_NOUN ._PUNCT
Na_INTJ duw_NOUN falle_ADV bod_VERB fi_PRON fyd_NOUN ar_ADP ôl_NOUN meddwl_VERB ._PUNCT
A_CONJ bues_VERB i_ADP jyst_ADV a_CONJ rhoi_VERB dodi_ADV comment_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ie_INTJ na_INTJ ._PUNCT
Rywbeth_NOUN kiss_NOUN me_ADP quick_VERB ._PUNCT
Ie_INTJ fi_PRON 'n_PART gwybod_VERB ie_INTJ fi_PRON 'n_PART gwybod_VERB ie_INTJ [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ond_CONJ fi_PRON 'n_PART mynnu_VERB bod_AUX e_PRON 'n_PART gwisgo_VERB hat_ADP achos_NOUN mae_AUX goryn_NOUN wedi_PART mynd_VERB mor_ADV goch_ADJ ie_INTJ 'na_ADV ti_PRON ._PUNCT
Achos_CONJ fuodd_VERB e_PRON 'n_PART rywle_ADV da_ADJ 'th_NOUN e_DET nol_NOUN a_CONJ ti_PRON 'di_PART gweld_VERB top_NOUN dy_DET ben_NOUN 'di_PART wedes_NOUN i_PRON ._PUNCT
O'dd_NOUN i_ADP goryn_NOUN e_PRON o'dd_NOUN e_PRON 'n_PART goch_ADJ fel_ADP '_PUNCT se_AUX fe_PRON 'n_PART dost_VERB '_PUNCT da_ADJ fe_PRON ti'mo_ADJ ._PUNCT
Ti'mo_NUM amser_NOUN yn_ADP yr_DET ha_PRON '_PUNCT +_SYM
Ti_PRON 'n_PART gwisgo_VERB hat_ADP ._PUNCT
+_VERB Haf_NOUN yn_PART r'ysgol_ADJ ._PUNCT
Ie_INTJ ._PUNCT
Oedd_VERB county_NOUN sports_NOUN ife_ADV ar_ADP y_DET athlete_NOUN lawr_ADV yn_PART lleoliad_NOUN wastad_ADV ._PUNCT
Ie_INTJ ._PUNCT
So_PART olreit_ADV fydde_ADV enwg_VERB mas_NOUN am_ADP y_DET diwrnod_NOUN ._PUNCT
Ie_INTJ ._PUNCT
A_PART fydde_AUX fe_PRON 'n_PART gweud_VERB o'dd_NOUN e_PRON wastad_ADV jyst_ADV yn_PART dod_VERB lan_ADV ar_ADP yr_DET high_NOUN jump_ADJ sy_AUX 'n_PART mynd_VERB mlaen_NOUN yn_PART ddiddiwed_VERB yndyfe_VERB ._PUNCT
O_ADP ie_INTJ ie_INTJ ._PUNCT
A_CONJ wedyn_ADV tri_NUM go_ADV ._PUNCT
A_CONJ ti_PRON mas_NOUN trwy_ADP dydd_NOUN wedyn_ADV ._PUNCT
Trwy_ADP dydd_NOUN os_CONJ oedd_VERB hi_PRON 'n_PART wyntog_ADJ trwy_ADP dydd_NOUN neu_CONJ os_CONJ oedd_VERB hi_PRON 'n_PART heulog_ADJ trwy_ADP dydd_NOUN ._PUNCT
Oedd_VERB pawb_PRON yn_PART ysgol_NOUN yn_PART gwybod_VERB diwrnod_NOUN wedyn_ADV ble_ADV oedd_AUX enwg_VERB wedi_PART bod_VERB ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Fel_ADP [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT myn_VERB ._PUNCT
O_ADP o't_PART ti_PRON 'n_PART goch_ADJ ie_INTJ ie_INTJ ._PUNCT
Bydde_VERB enwb_NOUN wedyn_ADV '_PUNCT ny_PRON ar_ADP y_DET adran_NOUN merched_NOUN nawr_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Fi_PRON jyst_ADV yn_PART mynd_VERB yn_PART goch_ADJ dw_AUX i_ADP 'm_DET yn_PART mynd_VERB ._PUNCT
Na_VERB fi_PRON jyst_ADV yn_PART mynd_VERB yn_PART goch_ADJ ._PUNCT
Myn_VERB '_PUNCT n'frown_X f'yna_VERB though_NOUN ._PUNCT
Achos_CONJ falle_ADV bydde_VERB hi_PRON ._PUNCT
Pam_ADV ma_VERB fe_NOUN 'r_DET dy_DET ben_NOUN di_PRON ._PUNCT
lleoliad_NOUN yw_VERB hwn_DET t'wel_NOUN lleoliad_NOUN ._PUNCT
Ie_INTJ o'dd_NOUN hi_PRON 'n_PART gallu_VERB [_PUNCT =_SYM ]_PUNCT o'dd_NOUN o'dd_NOUN o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Mae_VERB 'n_PART od_ADJ bod_VERB y_DET ie_NOUN ti'mo_VERB beth_PRON yw_VERB e_PRON so_AUX hwnna_PRON 'n_PART cael_VERB gymaint_ADV o_ADP haul_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ie_INTJ siŵr_ADJ o_ADP fod_VERB ._PUNCT
A_PART wedon_VERB ni_PRON bod_AUX ni_PRON 'n_PART mynd_VERB i_ADP fynd_VERB i_PART watsian_VERB  _SPACE [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Reit_AUX ni_PRON 'n_PART mynd_VERB am_ADP wac_NOUN fyd_NOUN ._PUNCT
Llwyfan_NOUN y_DET Maes_NOUN ._PUNCT
Odyn_NOUN ._PUNCT
Ni_PART 'n_PART mynd_VERB ._PUNCT
Na_VERB chi_PRON ._PUNCT
O_ADP mae_VERB mae_VERB jyst_ADV tu_NOUN ôl_NOUN ti_PRON dad_NOUN ._PUNCT
Ble_ADV mae_VERB o_PRON mae_VERB un_NUM arall_ADJ yn_PART fan'na_ADJ ._PUNCT
Fi_PRON 'n_PART mynd_VERB i_ADP bysgodi_VERB ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ i_ADP ti_PRON ._PUNCT
O_ADP so_PRON ._PUNCT
Ni_PART 'n_PART mynd_VERB nawr_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Diolch_INTJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ni_PART 'n_PART mynd_VERB ni_PRON 'n_PART mynd_VERB [_PUNCT /=_PROPN ]_PUNCT diolch_INTJ ._PUNCT
Oes_VERB ots_NOUN '_PUNCT da_ADJ chi_PRON seino_VERB rywbeth_NOUN cyn_ADP chi_PRON fynd_VERB ._PUNCT
Ie_INTJ mae_VERB hwnna_PRON 'n_PART iawn_ADJ ._PUNCT
Beth_PRON yw_VERB hwn_DET official_NUM secret_VERB act_ADP ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ie_INTJ so_PRON mae_AUX rhaid_VERB i_ADP pawb_PRON seinio_VERB ._PUNCT
official_VERB secrets_NOUN act_ADP nawr_ADV ._PUNCT
official_VERB secret_VERB act_ADP ._PUNCT
Pawb_PRON sy_VERB '_PUNCT n_PRON '_PUNCT siarad_VERB ?_PUNCT
Ie_INTJ ._PUNCT
O_ADP mae_VERB ti'mod_NOUN ._PUNCT
Ti_PRON wedi_PART siarad_VERB ._PUNCT
Na_INTJ nagyw_NOUN i_ADP wedi_PART ._PUNCT
Reit_VERB be_ADP ti_PRON moyn_NOUN '_PUNCT te_NOUN bach_ADJ ?_PUNCT
Popeth_NOUN ie_INTJ ?_PUNCT
Fel_ADP ti_PRON 'n_PART sillafu_VERB micky_ADJ beth_PRON yw_VERB e_PRON ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
M_NUM O_ADP S_NUM U_NUM C_NUM ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ ._PUNCT
753.488_ADJ
