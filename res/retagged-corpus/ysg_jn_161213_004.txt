Newyddion_PROPN
Pobl_NOUN hŷn_ADJ yn_PART chwarae_VERB plant_NOUN -_PUNCT rhaglen_NOUN i_ADP doddi_VERB 'ch_PRON calon_NOUN
Beth_PRON ddigwyddodd_VERB pan_CONJ dreuliodd_VERB grŵp_NOUN o_ADP blant_NOUN ysgol_NOUN feithrin_VERB dridiau_NOUN mewn_ADP canolfan_NOUN i_ADP bobl_NOUN hŷn_ADJ yng_ADP Nghaernarfon_PROPN ?_PUNCT
Mae_VERB 'r_DET canlyniad_NOUN yn_PART ddigon_ADJ i_ADP synnu_VERB ,_PUNCT lonni_VERB a_CONJ thoddi_VERB eich_DET calon_NOUN ._PUNCT
Yn_ADP y_DET rhaglen_NOUN newydd_ADJ ar_ADP S4C_PROPN ,_PUNCT Hen_PROPN Blant_NOUN Bach_ADJ ,_PUNCT nos_NOUN Fercher_NOUN 28_NUM Rhagfyr_NOUN ,_PUNCT bydd_VERB dau_NUM begwn_NOUN o_ADP genedlaethau_NOUN yn_PART dod_VERB at_ADP ei_DET gilydd_NOUN i_ADP rannu_VERB eu_DET gofal_NOUN dydd_NOUN ._PUNCT
Mewn_ADP arbrawf_NOUN cymdeithasol_ADJ unigryw_ADJ ,_PUNCT mae_VERB lleoliad_NOUN dros_ADP dro_NOUN yn_PART cael_VERB ei_DET greu_VERB ar_ADP gyfer_NOUN plant_NOUN meithrin_ADJ ac_CONJ oedolion_NOUN hŷn_ADJ i_ADP rannu_VERB amser_NOUN ,_PUNCT lle_NOUN a_CONJ gweithgareddau_NOUN ._PUNCT
Yn_PART ystod_NOUN y_DET dydd_NOUN ,_PUNCT maen_AUX nhw_PRON 'n_PART chwarae_VERB gemau_NOUN ac_CONJ yn_PART rhannu_VERB cinio_NOUN gyda_ADP 'i_PRON gilydd_NOUN ._PUNCT
O_ADP dan_ADP lygaid_NOUN barcud_NOUN tîm_NOUN o_ADP seicolegwyr_NOUN o_ADP Brifysgol_NOUN Bangor_PROPN ,_PUNCT rydym_AUX yn_PART gweld_VERB beth_PRON sy_AUX 'n_PART digwydd_VERB pan_CONJ mae_VERB 'r_DET bwlch_NOUN oedran_NOUN yn_PART cael_VERB ei_PRON bontio_VERB -_PUNCT a_CONJ 'r_DET effeithiau_NOUN trawsnewidiol_ADJ sy_VERB 'n_PART bosib_ADJ ._PUNCT
Gyda_ADP chamerâu_VERB wedi_PART eu_PRON gosod_VERB mewn_ADP mannau_NOUN arbennig_ADJ byddwn_AUX yn_PART gwrando_VERB ar_ADP bob_DET sibrwd_NOUN ac_CONJ yn_PART gwylio_VERB pob_DET symudiad_NOUN ._PUNCT
Trwy_ADP 'r_DET dagrau_NOUN a_CONJ 'r_DET chwerthin_VERB ,_PUNCT byddwn_AUX yn_PART gweld_VERB bod_VERB gan_ADP y_DET ddau_NUM grŵp_NOUN oedran_NOUN yma_ADV fwy_ADJ yn_PART gyffredin_ADJ nag_CONJ y_PART byddai_AUX rhywun_NOUN yn_PART tybio_VERB ._PUNCT
Yn_ADP 84_NUM oed_NOUN ,_PUNCT mae_AUX Glyn_NOUN Hughes_PROPN o_ADP Gaernarfon_PROPN yn_PART ymwelydd_NOUN cyson_NOUN â_ADP chanolfan_NOUN ddydd_NOUN Maesincla_PROPN ac_CONJ fe_PART gafodd_VERB brofiad_NOUN dirdynnol_ADJ yng_ADP nghwmni_NOUN 'r_DET plant_NOUN ._PUNCT
O_ADP ganlyniad_NOUN i_ADP strôc_NOUN bymtheg_ADJ mlynedd_NOUN yn_PART ôl_NOUN ,_PUNCT tydi_VERB lleferydd_NOUN Glyn_PROPN ddim_PART yn_PART dda_ADJ iawn_ADV ._PUNCT
Oherwydd_CONJ hynny_PRON mae_AUX 'n_PART mynd_VERB yn_PART rhwystredig_ADJ ac_CONJ yn_PART gwylltio_VERB gyda_ADP 'i_PRON hun_NOUN yn_PART aml_ADJ ._PUNCT
Er_CONJ bod_AUX staff_NOUN Maesincla_PROPN yn_PART poeni_VERB sut_ADV y_PART byddai_AUX 'r_DET plant_NOUN yn_PART ymateb_VERB i_ADP Glyn_PROPN ,_PUNCT cafodd_VERB pawb_PRON eu_PRON siomi_VERB ar_ADP yr_DET ochr_NOUN orau_ADJ ,_PUNCT fel_CONJ yr_DET eglura_NOUN un_NUM o_ADP seicolegwyr_NOUN y_DET gyfres_NOUN ,_PUNCT Nia_NOUN Williams_PROPN ._PUNCT
"_PUNCT Tydi_PROPN Glyn_PROPN ddim_PART yn_PART gallu_ADV dibynnu_VERB ar_ADP ei_DET leferydd_NOUN ac_CONJ felly_ADV mae_AUX 'n_PART defnyddio_VERB ystumiau_NOUN a_CONJ 'i_PRON ddwylo_NOUN i_PART gyfathrebu_VERB gyda_ADP 'r_DET plant_NOUN -_PUNCT ac_CONJ mae_AUX 'r_DET plant_NOUN yn_PART ymateb_VERB i_ADP hyn_PRON yn_PART syth_ADJ ._PUNCT
Fel_ADP Glyn_PROPN ,_PUNCT does_VERB gan_ADP y_DET plant_NOUN tair_NUM oed_NOUN yma_DET ddim_PART llawer_PRON o_ADP eirfa_NOUN chwaith_ADV ,_PUNCT felly_CONJ mae_VERB cyfathrebu_VERB drwy_ADP ystumiau_NOUN ac_CONJ arwyddion_NOUN yn_PART ffordd_NOUN effeithiol_ADJ iawn_ADV i_ADP greu_VERB a_CONJ magu_VERB perthynas_NOUN ._PUNCT
O_ADP ganlyniad_NOUN i_ADP 'r_DET berthynas_NOUN yma_ADV ,_PUNCT erbyn_ADP diwedd_NOUN yr_DET wythnos_NOUN roedd_AUX Glyn_PROPN yn_PART mentro_ADV defnyddio_VERB mwy_PRON o_ADP iaith_NOUN nag_CONJ arfer_NOUN ,_PUNCT ac_CONJ roedd_VERB i_ADP 'w_PRON glywed_VERB yn_PART brawddegu_VERB 'n_PART glir_ADJ gyda_ADP 'r_DET plant_NOUN ._PUNCT
Canlyniad_NOUN gwbl_ADV ryfeddol_ADJ ,_PUNCT ond_CONJ yn_PART dangos_VERB y_DET budd_NOUN seicolegol_ADJ o_ADP ddod_VERB â_ADP phlant_NOUN a_CONJ phobl_NOUN hŷn_ADJ at_ADP ei_DET gilydd_NOUN ._PUNCT "_PUNCT
Hen_ADJ Blant_NOUN Bach_PROPN
Nos_NOUN Fercher_NOUN 28_NUM Rhagfyr_NOUN 8.25_VERB ,_PUNCT S4C_PROPN
Hefyd_ADV ,_PUNCT dydd_NOUN Iau_ADJ 29_NUM Rhagfyr_NOUN 3.00_NUM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Saesneg_PROPN ar_ADP gael_VERB
Ar_ADP alw_VERB :_PUNCT s4c.cymru_NOUN ;_PUNCT BBC_X iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cynhyrchiad_NOUN Darlun_PROPN ar_ADP gyfer_NOUN S4C_PROPN mewn_ADP cydweithrediad_NOUN â_ADP Sony_NOUN Pictures_NOUN Television_PROPN
