0.0_NOUN
O_ADP helo_VERB shwmae_VERB [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Da_ADJ iawn_ADV wel_CONJ dw_AUX i_PRON 'n_PART gwneud_VERB powlen_NOUN fach_ADJ allan_ADV o_ADP glai_NOUN ._PUNCT
Yn_PART gynta_ADJ dw_AUX i_PRON 'n_PART rholio_VERB 'r_DET clai_NOUN fel_ADP hyn_PRON [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
W_PART drychwch_VERB mae_AUX 'n_PART edrych_VERB fel_ADP neidr_NOUN hir_ADJ [_PUNCT chwerthin_VERB ]_PUNCT ac_CONJ i_PART wneud_VERB y_DET bowlen_NOUN fach_ADJ dw_AUX i_PRON 'n_PART troi_VERB y_DET clai_NOUN o_ADP gwmpas_NOUN [_PUNCT saib_NOUN ]_PUNCT
Fel_ADP hyn_PRON [_PUNCT saib_NOUN ]_PUNCT
A_CONJ dyna_ADV ni_PRON ._PUNCT
Powlen_NOUN fach_ADJ berffeth_NOUN [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ond_CONJ dw_AUX i_PRON 'n_PART meddwl_VERB bydd_AUX enwg_VERB yn_PART hoffi_ADV neud_VERB un_NUM o_ADP 'r_DET rhain_PRON ._PUNCT
Beth_PRON am_ADP i_ADP ni_PRON fynd_VERB i_PART chwilio_VERB amdano_ADP fe_PRON [_PUNCT sŵn_NOUN cerddoriaeth_NOUN a_CONJ swigod_NOUN ]_PUNCT
O_ADP dyma_ADV enwg_VERB ._PUNCT
Helo_INTJ enwg_VERB [_PUNCT gwahanol_ADJ synau_NOUN ]_PUNCT
[_PUNCT sŵn_NOUN ratlo_NOUN ]_PUNCT ._PUNCT
W_AUX ti_PRON 'n_PART edrych_VERB yn_PART oer_ADJ iawn_ADV [_PUNCT saib_NOUN ]_PUNCT Pam_ADV na_PART wnei_VERB di_PRON wisgo_VERB het_NOUN a_CONJ sgarff_NOUN ?_PUNCT
<_SYM pwffian_VERB chwerthin_VERB >_DET Het_NOUN wlân_ADJ ._PUNCT
166.981_ADJ
Iawn_ADV felly_CONJ mae_VERB gydan_VERB ni_PRON 'r_DET ddwy_NUM hosan_NOUN enwg_VERB iawn_ADV ?_PUNCT
Geith_NOUN hon_PRON fod_VERB y_DET pen_NOUN a_CONJ gall_VERB honna_VERB fod_VERB y_DET cynffon_NOUN iawn_ADV ?_PUNCT
[_PUNCT cerddoriaeth_NOUN ]_PUNCT
Rho_VERB di_PRON dy_DET law_NOUN mewn_ADP fanna_NOUN i_ADP fi_PRON ._PUNCT
Ti_PRON fo'lon_PART tynnu_VERB 'r_DET hosan_NOUN i_ADP fi_PRON ?_PUNCT
'_PUNCT Na_VERB ti_PRON ._PUNCT
A_PART ni_PRON 'n_PART myn_VERB '_PUNCT i_PRON roi_VERB 'r_DET llygad_NOUN '_PUNCT mlaen_NOUN yn_PART gynta'_ADJUNCT bob_DET pen_NOUN iawn_ADV ?_PUNCT
Felly_ADV dal_VERB di_PRON fe_PRON fanna_X ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB i_PART wasgu_VERB llygad_NOUN mewn_ADP fan_NOUN hyn_DET [_PUNCT cerddoriaeth_NOUN ]_PUNCT
Reit_VERB drwodd_VERB ._PUNCT
'_PUNCT Dy_DET o_ADP drwodd_NOUN ?_PUNCT
[_PUNCT cerddoriaeth_NOUN ]_PUNCT
Ody_NOUN a_CONJ dw_AUX i_PRON 'n_PART mynd_VERB i_PART glipio_VERB fe_PRON gyda_ADP hwn_PRON [_PUNCT saib_NOUN ]_PUNCT fel_CONJ bod_AUX e_PRON 'n_PART yn_PART ei_DET le_NOUN fanna_X ._PUNCT
Ody_NOUN honna_VERB 'na_ADV ?_PUNCT
Ody_NOUN ._PUNCT
A_CONJ 'r_DET nesa_NOUN yr_DET ochr_NOUN arall_ADJ <_SYM pwffian_VERB chwerthin_VERB >_SYM [_PUNCT cerddoriaeth_NOUN ]_PUNCT
Nawr_ADV te_NOUN mama_NOUN angen_NOUN i_ADP fi_PRON dorri_VERB cardfwrdd_NOUN i_ADP gadw_VERB siâp_NOUN y_DET pen_NOUN iawn_ADV ?_PUNCT
Felly_CONJ dw_AUX i_PRON 'n_PART mynd_VERB i_ADP dorri_VERB am_ADP lawr_ADV ac_CONJ yna_ADV 'n_PART grwn_VERB <_SYM cerddoriaeth_NOUN >_SYM Os_CONJ wnei_VERB di_PRON agor_VERB e_PRON ?_PUNCT
<_SYM cerddoriaeth_NOUN >_SYM
Ydy_VERB e_PRON 'n_PART gadarn_ADJ fanna_X ?_PUNCT
Ody_NOUN da_ADJ iawn_ADV ._PUNCT
Nawr_ADV te_NOUN nawr_ADV mae_VERB 'r_DET hwyl_NOUN yn_PART dechre_ADV enwg_VERB [_PUNCT chwerthin_VERB ]_PUNCT
'_PUNCT Y_DET 'n_PART ni_PRON yn_PART myn_VERB '_PUNCT i_PRON ddefnyddio_VERB stwffin_NOUN y_DET clustog_NOUN i_ADP lenwi_VERB y_DET pen_NOUN <_SYM cerddoriaeth_NOUN >_SYM
Dyna_ADV ti_PRON ._PUNCT
A_CONJ nawr_ADV ni_PRON 'n_PART mynd_VERB i_PART lenwi_VERB 'r_DET gynffon_NOUN i_ADP gyd_ADP felly_CONJ bant_ADV â_ADP ni_PRON ._PUNCT
Digon_ADV i_PART wneud_VERB <_SYM cerddoriaeth_NOUN >_SYM
<_SYM anelwig_VERB >_SYM ni_PRON ._PUNCT
Ydy_VERB honna_VERB 'n_PART llawn_ADJ ?_PUNCT
Ydy_VERB ?_PUNCT
Nawr_ADV te_NOUN ni_PRON 'n_PART mynd_VERB i_PART rhoi_VERB 'r_DET ddau_NUM at_ADP ei_DET gilydd_NOUN nawr_ADV <_SYM cerddoriaeth_NOUN >_SYM
Beth_PRON am_ADP addurno_VERB 'r_DET neidr_NOUN 'ma_ADV ?_PUNCT
Ie_INTJ ?_PUNCT
Beth_PRON os_PART addurnwn_VERB ni_PRON hi_PRON gyda_ADP glanhawyr_NOUN pib_NOUN a_CONJ defnydd_NOUN ffelt_NOUN ._PUNCT
Ti_PRON 'n_PART fo'lon_ADJ mynd_VERB i_PART chwilio_VERB am_ADP rheini_VERB yn_ADP y_DET cwpwrdd_NOUN celf_NOUN i_ADP fi_PRON ?_PUNCT
C'mon_NOUN [_PUNCT chwerthin_VERB ]_PUNCT
Da_ADJ iawn_ADV ._PUNCT
'_PUNCT Nawn_NOUN ni_PRON gychwyn_VERB gyda_ADP y_DET glanhawyr_NOUN pib_NOUN iawn_ADV ?_PUNCT
Felly_ADV wna_VERB i_PRON roi_VERB hwnna_PRON i_ADP 'r_DET ochr_NOUN am_ADP funud_NOUN ._PUNCT
Nawr_ADV ti_PRON 'n_PART mynd_VERB â_ADP un_NUM o_ADP 'r_DET rhain_PRON a_CONJ ti_PRON 'n_PART mynd_VERB i_PART wneud_VERB siâp_NOUN igam_ADJ -_PUNCT ogam_ADJ <_SYM cerddoriaeth_NOUN >_SYM ._PUNCT
Nawr_ADV ni_PRON 'n_PART mynd_VERB i_PART rhoi_VERB rhein_NOUN ar_ADP gefn_NOUN y_DET neidr_NOUN ac_CONJ i_PART neud_VERB hyn_PRON mama_NOUN angen_NOUN tâp_NOUN dwy_NUM ochrog_ADJ arnon_ADP ni_PRON [_PUNCT saib_NOUN ]_PUNCT Ar_ADP hyd_NOUN y_DET cefn_NOUN ._PUNCT
487.191_NOUN
Da_ADJ iawn_ADV ._PUNCT
W_INTJ [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
-_PUNCT Rho_VERB be_PRON ?_PUNCT
Ti_PRON 'n_PART neud_VERB llanast_NOUN ?_PUNCT
Nawr_ADV o_ADP [_PUNCT anadlu_VERB mewn_ADP ]_PUNCT ._PUNCT
Ni_PART 'n_PART mynd_VERB i_PART rhoi_VERB rhain_PRON mewn_ADP patrwm_NOUN bach_ADJ nawr_ADV iawn_ADV ar_ADP y_DET cefn_NOUN <_SYM cerddoriaeth_NOUN >_SYM
A_PART be_AUX ni_PRON 'n_PART mynd_VERB i_PART neud_VERB nawr_ADV yw_VERB torri_VERB triongle_NOUN bach_ADJ union_ADJ yr_DET un_NUM faint_NOUN allan_ADV o_ADP 'r_DET defnydd_NOUN ffelt_NOUN 'ma_ADV iawn_ADV <_SYM pwffian_VERB chwerthin_VERB >_SYM Ie_INTJ da_ADJ iawn_ADV ti_PRON <_SYM cerddoriaeth_NOUN >_SYM
A_PART ni_PRON 'n_PART myn_VERB '_PUNCT i_PRON roi_VERB 'r_DET triongle_NOUN [_PUNCT saib_NOUN ]_PUNCT bob_DET ochr_NOUN [_PUNCT saib_NOUN ]_PUNCT i_ADP 'r_DET glanhawyr_NOUN pib_NOUN fel'na_NOUN <_SYM cerddoriaeth_NOUN >_SYM
A_CONJ dyna_ADV ni_NOUN da_ADJ iawn_ADV enwg_VERB ._PUNCT
Nawr_ADV mama_NOUN 'na_ADV un_NUM peth_NOUN bach_ADJ ar_ADP goll_ADJ dw_AUX i_PRON 'n_PART meddwl_VERB [_PUNCT synau_NOUN gwahanol_ADJ ]_PUNCT
BeBe_NOUN ?_PUNCT
[_PUNCT pwffian_VERB chwerthin_VERB ]_PUNCT Tafod_PROPN ._PUNCT
Ie_INTJ felly_ADV dw_AUX i_PRON 'n_PART mynd_VERB i_ADP dorri_VERB siâp_NOUN y_DET tafod_NOUN allan_ADV o_ADP 'r_DET ffelt_NOUN yma_ADV nawr_ADV iawn_ADV ?_PUNCT
<_SYM cerddoriaeth_NOUN >_SYM
Dyna_ADV ni_PRON ._PUNCT
Ac_CONJ os_CONJ '_PUNCT nei_CONJ di_PRON droi_VERB 'r_DET neidr_NOUN ben_NOUN i_ADP waered_NOUN i_ADP fi_PRON ?_PUNCT
<_SYM cerddoriaeth_NOUN >_SYM
Dere_VERB i_ADP ni_PRON gael_VERB gweld_VERB <_SYM cerddoriaeth_NOUN >_SYM Dyna_ADV ti_PRON [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ti_PRON 'n_PART hapus_ADJ gyda_ADP 'i_PRON ?_PUNCT
596.953_VERB
[_PUNCT chwerthin_VERB ]_PUNCT O_ADP bebe_NOUN di-_NOUN dim_DET neidr_NOUN go_ADV iawn_ADV yw_VERB hi_PRON oh_ADJ [_PUNCT chwerthin_VERB ]_PUNCT
A_PART wela_VERB i_PRON ._PUNCT
Dim_PRON ond_ADP acto_VERB bod_VERB ofan_NOUN arnot_ADP ti_PRON o't_ADP ti_PRON ife_ADV ?_PUNCT <_SYM pwffian_VERB chwerthin_VERB >_SYM
881.045_NOUN
886.285_NOUN
1079.957_NOUN
