"_PUNCT *_SYM *_SYM *_SYM Late_PROPN Change_PROPN S4C_PROPN -_PUNCT Newid_NOUN Hwyr_ADJ S4C_PROPN *_SYM *_SYM *_SYM -_PUNCT Wythnos_PROPN 24_NUM &_ADP Wythnos_PROPN 25_NUM "_PUNCT ,_PUNCT "_PUNCT *_SYM *_SYM Late_PROPN Change_PROPN S4C_PROPN -_PUNCT Newid_NOUN Hwyr_ADJ S4C_PROPN *_SYM *_SYM
Wythnos_VERB 24_NUM
11_NUM /_SYM 06_NUM /_SYM 2020_NUM Dydd_PROPN Iau_PROPN
22_NUM :_PUNCT 00_NUM Dileu_PROPN Caru_VERB Siopa_PROPN
22_NUM :_PUNCT 00_NUM Rhaglen_NOUN Newydd_ADJ Dylan_PROPN ar_ADP Daith_PROPN :_PUNCT Thomas_PROPN Picton_PROPN I_ADP 26598_NUM /_SYM 001_NUM Y_DET 96158_NUM
Ail_ADJ -_PUNCT ddangosiad_NOUN hanes_NOUN Thomas_PROPN Picton_PROPN i_ADP gyd-_PROPN fynd_VERB â_ADP digwyddiadau_NOUN cyfredol_ADJ symudiad_NOUN Black_X Lives_PROPN Matter_PROPN ._PUNCT
23_NUM :_PUNCT 00_NUM Amser_NOUN Newydd_ADJ Hansh_PROPN
23_NUM :_PUNCT 30_NUM Amser_NOUN Newydd_ADJ Bwyd_PROPN Epic_NOUN Chris_PROPN
00_NUM :_PUNCT 05_NUM Amser_NOUN Newydd_ADJ Diwedd_PROPN
22_NUM :_PUNCT 00_NUM
DYLAN_NOUN AR_NOUN DAITH_X :_PUNCT THOMAS_PROPN PICTON_PROPN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT SC_X )_PUNCT (_PUNCT HD_X )_PUNCT
I_ADP gyd-_X fynd_VERB â_ADP digwyddiadau_NOUN cyfredol_ADJ symudiad_NOUN Black_X Lives_PROPN Matter_PROPN ,_PUNCT dyma_ADV ail-_NOUN ddangos_VERB stori_NOUN Thomas_PROPN Picton_PROPN -_PUNCT y_DET milwr_NOUN o_ADP Sir_NOUN Benfro_PROPN a_CONJ ddaeth_VERB yn_ADP Llywodraethwr_PROPN cyntaf_ADJ ynys_NOUN Trinidad_PROPN ._PUNCT
Mae_AUX 'r_DET stori_NOUN yn_PART dechrau_VERB yn_PART Gibraltar_NOUN ac_CONJ yntau_PRON yn_PART gyw_NOUN o_ADP filwr_NOUN ac_CONJ yn_PART gorffen_VERB yn_ADP Waterloo_X pan_CONJ ddaeth_VERB yn_PART uchel_ADV swyddog_NOUN enwog_ADJ ._PUNCT
Ond_CONJ rhwng_ADP y_DET ddau_NUM roedd_VERB yna_ADV gyfnod_NOUN tywyll_ADJ iawn_ADV yn_PART stori_NOUN Thomas_PROPN Picton_PROPN ._PUNCT
Cyfnod_NOUN sydd_AUX yn_PART codi_VERB cwestiynau_NOUN anodd_ADJ am_ADP ei_DET rôl_NOUN e_PRON a_CONJ 'r_DET Ymerodraeth_NOUN Brydeinig_ADJ mewn_ADP hanes_NOUN sydd_VERB erbyn_ADP hyn_PRON yn_PART codi_VERB cywilydd_NOUN ;_PUNCT caethwasiaeth_NOUN ._PUNCT
23_NUM :_PUNCT 00_NUM
HANSH_ADP (_PUNCT S_NUM )_PUNCT (_PUNCT HD_X )_PUNCT
Tiwns_INTJ ,_PUNCT comedi_NOUN a_CONJ lleisiau_NOUN ffres_ADJ ._PUNCT
Blas_NOUN o_ADP gynnwys_VERB arlein_NOUN @hanshs4c_NOUN ._PUNCT
23_NUM :_PUNCT 30_NUM
CWPWRDD_NOUN EPIC_NOUN CHRIS_X (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT SC_X )_PUNCT (_PUNCT HD_X )_PUNCT
Tro_VERB 'ma_ADV bydd_AUX Chris_PROPN yn_PART coginio_VERB un_NUM o_ADP 'i_PRON hoff_ADJ brydau_NOUN o_ADP 'r_DET tecawê_AUX yn_PART defnyddio_VERB shrimp_NOUN lleol_ADJ a_CONJ saws_NOUN XO_PROPN ,_PUNCT porc_NOUN efo_ADP stwffin_NOUN a_CONJ chroen_NOUN crispi_VERB ,_PUNCT pizza_NOUN cartre_VERB '_PUNCT a_CONJ chawl_NOUN rhad_ADJ a_CONJ blasus_ADJ yn_PART defnyddio_VERB chorizo_NOUN ,_PUNCT Barca_PROPN style_NOUN !_PUNCT
00_NUM :_PUNCT 05_NUM
DIWEDD_PROPN
Wythnos_VERB 25_NUM
18_NUM /_SYM 06_NUM /_SYM 2020_NUM Dydd_PROPN Iau_PROPN
22_NUM :_PUNCT 00_NUM Dileu_PROPN Caru_VERB Siopa_PROPN
22_NUM :_PUNCT 00_NUM Rhaglen_NOUN Newydd_ADJ America_PROPN Gaeth_PROPN a_CONJ 'r_DET Cymry_PROPN I_ADP 22653_NUM /_SYM 001_NUM Z_NUM 59411_NUM
23_NUM :_PUNCT 00_NUM Amser_NOUN Newydd_ADJ Hansh_PROPN
23_NUM :_PUNCT 30_NUM Amser_NOUN Newydd_ADJ Bwyd_PROPN Epic_NOUN Chris_PROPN
00_NUM :_PUNCT 05_NUM Amser_NOUN Newydd_ADJ Diwedd_PROPN
22_NUM :_PUNCT 00_NUM
AMERICA_NOUN GAETH_PROPN A_CONJ 'R_DET CYMRY_PROPN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT SC_X )_PUNCT (_PUNCT SD_X )_PUNCT
I_ADP gyd-_X fynd_VERB â_ADP digwyddiadau_NOUN cyfredol_ADJ symudiad_NOUN Black_X Lives_PROPN Matter_PROPN ,_PUNCT dyma_ADV ail-_NOUN ddangos_VERB y_DET rhaglen_NOUN hon_DET ._PUNCT
Colin_PROPN Powell_PROPN ._PUNCT ._PUNCT ._PUNCT Serena_X Williams_PROPN ._PUNCT ._PUNCT ._PUNCT Jesse_PROPN Owens_X ._PUNCT ._PUNCT ._PUNCT ._PUNCT Pam_ADV mae_AUX gan_ADP gynifer_NOUN o_ADP Americanwyr_NOUN du_ADJ ,_PUNCT gyfenwau_NOUN Cymraeg_PROPN ?_PUNCT
Bydd_VERB y_DET gyfres_NOUN yma_ADV ,_PUNCT a_CONJ gyflwynir_VERB gan_ADP Dr_PROPN ._PUNCT
Jerry_PROPN Hunter_PROPN ,_PUNCT yn_PART olrhain_ADJ hanes_NOUN cysylltiadau_NOUN 'r_DET Cymry_PROPN â_ADP chaethwasiaeth_NOUN yn_ADP yr_DET UDA_NOUN rhwng_ADP 1619_NUM i_ADP 1865_NUM ._PUNCT
23_NUM :_PUNCT 00_NUM
HANSH_ADP (_PUNCT S_NUM )_PUNCT (_PUNCT HD_X )_PUNCT
Tiwns_INTJ ,_PUNCT comedi_NOUN a_CONJ lleisiau_NOUN ffres_ADJ ._PUNCT
Blas_NOUN o_ADP gynnwys_VERB arlein_NOUN @hanshs4c_NOUN ._PUNCT
23_NUM :_PUNCT 30_NUM
CWPWRDD_NOUN EPIC_NOUN CHRIS_X (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT SC_X )_PUNCT (_PUNCT HD_X )_PUNCT
Mae_VERB Chris_PROPN yn_PART ôl_NOUN yn_ADP y_DET gegin_NOUN yn_PART coginio_VERB ribs_X sdici_X a_CONJ secsi_NOUN Tseiniaidd_ADJ ,_PUNCT pad_PART thai_VERB sydyn_ADJ ,_PUNCT cyw_ADV iâr_NOUN cyfan_ADJ efo_ADP sôs_NOUN piri_VERB piri_VERB a_CONJ 'i_PRON hoff_ADJ bwdin_NOUN yn_ADP y_DET byd_NOUN ,_PUNCT Tarte_X tatin_NOUN Ffrengig_ADJ ._PUNCT
00_NUM :_PUNCT 05_NUM
DIWEDD_PROPN
