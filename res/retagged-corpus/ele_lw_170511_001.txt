Gwirfoddoli_VERB yn_ADP Tafwyl_PROPN 2017_NUM
MAE_NOUN MENTER_PROPN CAERDYDD_PROPN YN_PROPN EDRYCH_PROPN I_NUM RECRIWTIO_NUM UNIGOLION_NUM CYFEILLGAR_NOUN A_CONJ BRWDFRYDIG_NOUN DROS_NUM 18_NUM OED_X I_ADP WIRFODDOLI_PROPN YN_PROPN TAFWYL_PROPN ELENI_PROPN ,_PUNCT AR_VERB Y_DET 1_NUM AF_X AC_X 2_NUM IL_NUM O_ADP ORFFENNAF_X 2017_NUM ._PUNCT
Fel_ADP Diolch_INTJ ,_PUNCT cewch_VERB :_PUNCT
-_PUNCT Tocyn_PROPN bwyd_NOUN £_SYM 10_NUM a_CONJ diod_NOUN i_ADP 'w_PRON gwario_VERB yn_PART un_NUM o_ADP stondinau_NOUN bwyd_NOUN yr_DET ŵyl_NOUN
-_PUNCT Cyfle_PROPN i_ADP wirfoddoli_VERB yn_PART un_NUM o_ADP wyliau_NOUN mwyaf_ADJ Cymru_PROPN
-_PUNCT Crys_PROPN -_PUNCT T_NUM Tafwyl_X arbennig_ADJ
Er_ADP mwyn_NOUN corfrestru_VERB dilynwch_VERB y_DET linc_NOUN isod_ADV :_PUNCT
cyfeiriad_NOUN
neu_CONJ am_ADP fwy_PRON o_ADP wybodaeth_NOUN ebostiwch_VERB :_PUNCT
cyfeiriad_NOUN -_PUNCT bost_NOUN
Dewch_VERB i_ADP fwynhau_VERB gŵyl_NOUN orau_ADJ Cymru_PROPN !_PUNCT
