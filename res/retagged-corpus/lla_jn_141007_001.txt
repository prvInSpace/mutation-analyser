[_PUNCT Cerddoriaeth_NOUN gefndirol_ADJ dros_ADP y_DET sgript_NOUN ]_PUNCT ._PUNCT
Tu_NOUN hwnt_ADV i_ADP bob_DET bro_NOUN a_CONJ bryn_VERB coedwig_NOUN cwm_NOUN a_CONJ llyn_NOUN saif_VERB castell_NOUN gudd_ADJ sy_AUX 'n_PART cadw_VERB 'i_PRON lygaid_NOUN ar_ADP holl_DET gampau_NOUN 'r_DET creaduriaid_NOUN ._PUNCT
Dewch_VERB yn_PART nes_ADJ fe_PART gewch_VERB eich_DET synnu_VERB gan_ADP fyd_NOUN mawr_ADJ enw_NOUN digri_ADJ [_PUNCT Cerddoriaeth_NOUN yn_PART stopio_VERB ]_PUNCT ._PUNCT
Mae_VERB hi_PRON 'n_PART ddiwrnod_NOUN braf_ADJ o_ADP haf_NOUN ac_CONJ mae_AUX 'r_DET llyn_NOUN yn_PART dawel_ADJ [_PUNCT Saib_PROPN ]_PUNCT [_PUNCT Cerddoriaeth_NOUN gefndirol_ADJ yn_PART ail_ADJ ddechrau_VERB ]_PUNCT ._PUNCT
Mae_VERB 'r_DET crucaid_NOUN a_CONJ 'r_DET sglefrwyr_NOUN dŵr_NOUN wrth_ADP eu_DET bodd_NOUN yn_PART sglefrio_VERB ar_ADP wyneb_NOUN y_DET llyn_NOUN ._PUNCT
Mae_VERB coese_NOUN blewog_ADJ yn_PART eu_PRON cadw_VERB 'n_PART sych_ADJ a_CONJ 'u_PRON helpu_VERB i_ADP symud_VERB yn_PART slic_ADJ ._PUNCT
<_SYM gwichia_VERB >_SYM Wiii_X bwmp_NOUN mas_ADJ o_ADP ffor_NOUN ._PUNCT
O_ADP mama_NOUN mhen_NOUN i_ADP yn_PART troi_VERB fel_ADP top_NOUN ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
O_ADP mama_NOUN nghoes_NOUN i_ADP 'n_PART cosi_VERB ._PUNCT
W_VERB yn_PART fan_NOUN hyn_DET ._PUNCT
O_ADP o_ADP o_ADP y_DET piwiad_NOUN piws_ADJ ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT Ga_NOUN i_ADP ddawnsio_VERB efo_ADP chdi_NOUN ?_PUNCT
Ma_VERB enwg_VERB y_DET ceffyl_NOUN dŵr_NOUN bolwyn_ADJ yn_PART dangos_VERB 'i_PRON hun_DET wrth_ADP nofio_VERB ar_ADP ei_DET gefn_NOUN ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT O_ADP go_ADV dda_ADJ rwan_ADV de_NOUN ._PUNCT
Dyma_DET 'r_DET bywyd_NOUN i_ADP chdi_NOUN ._PUNCT
Torheulio_VERB ar_ADP y_DET llyn_NOUN ._PUNCT
Ei_INTJ ._PUNCT
Gwylia_VERB dy_DET hun_NOUN y_DET pen_NOUN dafad_NOUN ._PUNCT
Ti_PRON 'n_PART cuddio_VERB 'r_DET haul_NOUN ._PUNCT
O_ADP y_DET ddrwg_NOUN da_ADJ fi_PRON enwg_VERB [_PUNCT =_SYM ]_PUNCT y_DET [_PUNCT /=_PROPN ]_PUNCT ffilish_AUX i_PART weld_VERB ti_PRON nawr_ADV fana_ADV ._PUNCT
Bydd_AUX y_DET sglefrwyr_NOUN dŵr_NOUN yn_PART treulio_VERB orie_NOUN yn_PART sglefrio_VERB 'n_PART hapus_ADJ fel_ADP hyn_PRON [_PUNCT Saib_PROPN gyda_ADP cherddoriaeth_NOUN gefndirol_ADJ ]_PUNCT ._PUNCT
O_ADP dan_ADP y_DET dŵr_NOUN ma_VERB enwb_VERB 'r_DET falwoden_NOUN a_CONJ 'i_PRON ffrind_NOUN y_DET penbwl_NOUN yn_PART mwynhau_VERB 'r_DET sioe_NOUN ._PUNCT
Y_DET pwy_PRON wyt_AUX ti_PRON 'n_PART hoffi_VERB ora_VERB ?_PUNCT
Y_DET [_PUNCT =_SYM ]_PUNCT wi_NOUN 'n_PART [_PUNCT /=_PROPN ]_PUNCT wi_NOUN 'n_PART lico_NOUN 'r_DET ddau_NUM sy_AUX 'n_PART dawnsio_VERB ar_ADP bwyll_NOUN enwg_NOUN ._PUNCT
Ma_AUX un_NUM yn_PART pwyntio_VERB lan_ADV a_CONJ 'r_DET llall_PRON yn_PART pwyntio_VERB lawr_ADV [_PUNCT Saib_PROPN yn_ADP y_DET sgript_NOUN a_CONJ cherddoriaeth_NOUN gefndirol_ADJ yn_PART parhau_VERB ]_PUNCT ._PUNCT
Hen_ADJ bryfaid_NOUN bach_ADJ y_DET Bala_PROPN ._PUNCT
Does_VERB na_CONJ 'm_DET llonydd_NOUN i_PART gael_VERB i_ADP dorheulo_VERB wir_ADV i_ADP chi_PRON ._PUNCT
116.367_PROPN
119.881_PRON
126.966_NOUN
133.425_NOUN
136.982_NOUN
143.128_NOUN
299.989_VERB
