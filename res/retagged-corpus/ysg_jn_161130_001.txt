Crynodeb_NOUN Pobol_ADJ y_DET Cwm_NOUN W_ADP 50_NUM
Mae_AUX Ed_X yn_PART derbyn_VERB canmoliaeth_NOUN am_ADP ei_DET sgiliau_NOUN actio_VERB yn_ADP y_DET panto_VERB Nadolig_PROPN ond_CONJ mae_AUX hyn_PRON yn_PART mynd_VERB dan_ADP groen_NOUN Sioned_PROPN ._PUNCT
Pan_CONJ gaiff_VERB hi_PRON ei_DET diarddel_NOUN o_ADP 'r_DET panto_VERB oherwydd_CONJ ei_DET hagwedd_NOUN ,_PUNCT mae_AUX 'n_PART disgwyl_VERB i_ADP Ed_ADV roi_VERB 'r_DET gorau_ADJ iddi_ADP hefyd_ADV ._PUNCT
Pan_CONJ mae_AUX 'n_PART gwrthod_VERB ,_PUNCT mae_VERB 'r_DET canlyniadau_NOUN 'n_PART frawychus_ADJ ._PUNCT
Mae_AUX Kevin_PROPN yn_PART ceisio_VERB ail_ADJ sefydlu_VERB ei_DET hun_NOUN yn_ADP y_DET gymuned_NOUN ac_CONJ aiff_VERB e_PRON a_CONJ Jason_PROPN i_ADP 'r_DET caffi_NOUN am_ADP baned_VERB pan_CONJ mae_AUX Debbie_PROPN allan_ADV yn_PART siopa_NOUN ._PUNCT
Caiff_VERB Debbie_PROPN sioc_NOUN i_PART weld_VERB Kevin_PROPN y_DET tu_NOUN ôl_NOUN i_ADP gownter_NOUN y_DET caffi_VERB ac_CONJ mae_AUX 'n_PART poeni_VERB ei_PRON fod_AUX yn_PART ceisio_VERB ei_PRON bychanu_VERB ._PUNCT
Mae_VERB hi_PRON 'n_PART noson_NOUN parti_NOUN plu_NOUN Cadno_PROPN ac_CONJ mae_AUX Kelly_X yn_PART mynnu_VERB bod_AUX Eifion_PROPN yn_PART ymuno_VERB yn_ADP yr_DET hwyl_NOUN ._PUNCT
Caiff_VERB Cadno_PROPN ormod_PRON i_ADP yfed_VERB felly_CONJ mae_AUX Eifion_PROPN yn_PART cerdded_VERB gyda_ADP hi_PRON '_PUNCT nôl_NOUN i_ADP Benrhewl_PROPN ._PUNCT
Ond_CONJ a_PART fydd_AUX ddau_NUM yn_PART llwyddo_VERB i_PART frwydro_VERB yn_PART erbyn_ADP eu_DET teimladau_NOUN ?_PUNCT
Mae_AUX anrheg_NOUN Tyler_X i_ADP Iolo_PROPN yn_PART cael_VERB ei_DET difethaf_VERB pan_CONJ mae_AUX Iolo_PROPN 'n_PART darganfod_VERB mai_PART Gwyneth_PROPN sydd_AUX wedi_PART trefnu_VERB 'r_DET cyfan_NOUN ,_PUNCT a_CONJ 'i_PRON bod_VERB hi_PRON a_CONJ Siôn_PROPN am_ADP fod_VERB yn_ADP yr_DET un_NUM gwesty_NOUN ._PUNCT
Yn_PART ystod_NOUN y_DET pryd_NOUN bwyd_NOUN mae_VERB gan_ADP Gwyneth_NOUN gwestiwn_NOUN pwysig_ADJ iawn_ADV i_ADP 'w_PRON ofyn_VERB i_ADP Siôn_PROPN ,_PUNCT a_CONJ sylweddola_VERB Iolo_PROPN nad_PART oes_VERB modd_NOUN iddo_ADP ddylanwadu_VERB ar_ADP benderfyniad_NOUN ei_DET dad_NOUN ._PUNCT
Pobol_ADJ y_DET Cwm_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 8.00_NUM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 6.00_NUM ,_PUNCT S4C_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Ar_ADP alw_VERB :_PUNCT s4c.cymru_NOUN ;_PUNCT BBC_X iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
