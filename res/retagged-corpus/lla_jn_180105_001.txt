0.0_NOUN
all_VERB sorts_NOUN mixed_NOUN bag_NOUN 'u_PRON ni_PRON ._PUNCT
Ma_VERB 'n_PART le_NOUN bach_ADJ neis_ADJ i_PART ddod_VERB [_PUNCT =_SYM ]_PUNCT i_PRON [_PUNCT /=_PROPN ]_PUNCT i_PART gael_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT bore_NOUN bach_ADJ ._PUNCT
Yr_PART all_VERB sorts_NOUN hanner_NOUN cant_NUM o_ADP fenywod_NOUN canol_ADJ oed_NOUN ac_CONJ yn_PART hŷn_ADJ a_CONJ nifer_NOUN ohonnyn_ADP nhw_PRON 'n_PART dod_VERB o_ADP bentre_NOUN lleoliad_NOUN ._PUNCT
Bob_DET bore_NOUN mercher_NOUN mama_AUX nhw_PRON 'n_PART dod_VERB at_ADP 'u_PRON gilydd_NOUN i_ADP 'r_DET neuadd_NOUN leol_ADJ i_PART gael_VERB clonc_NOUN ac_CONJ i_PART drafod_ADV colli_VERB pwyse_NOUN '_PUNCT ._PUNCT
Ni_PART 'n_PART joio_NOUN 'n_PART bwyd_VERB ar_ADP y_DET ford_NOUN '_PUNCT un_NUM sa_VERB ni_PRON 'n_PART watchad_ADJ beth_DET ni_PRON 'n_PART fyta_ADJ ._PUNCT
Fi_PRON 'n_PART eitha_ADV flin_ADJ ond_CONJ fi_PRON 'di_PART anghofio_VERB 'r_DET scale_NOUN ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT [_PUNCT cerddoriaeth_NOUN yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
Ond_CONJ dewch_VERB i_ADP ni_PRON fod_VERB yn_PART onest_ADJ dyw_VERB colli_VERB pwyse_NOUN byth_ADV yn_PART rhwydd_ADJ ._PUNCT
Fe_PART ddilynwn_VERB ni_PRON hynt_NOUN a_CONJ helynt_NOUN y_DET criw_NOUN yn_ADP ystod_NOUN y_DET flwyddyn_NOUN aeth_VERB h'ibo_VERB ._PUNCT
A_PART chofiwch_VERB mae_AUX mwy_PRON i_ADP golli_VERB pwyse_NOUN na_CONJ 'r_DET gloriana_NOUN thorri_VERB 'n_PART nôl_NOUN fel_CONJ mae_VERB enwb_NOUN cyfenw_NOUN trefnydd_NOUN y_PART grwp_NOUN yn_PART 'i_PRON esbonio_VERB ._PUNCT
Dydd_NOUN gwyl_NOUN Dewi_PROPN yw_VERB 'i_PRON bore_NOUN 'ma_ADV a_CONJ mae_VERB digon_ADV yn_PART myn_VERB '_PUNCT mlaen_NOUN 'ma_ADV ._PUNCT
Mae_VERB un_NUM o_ADP 'r_DET  _SPACE aelode_ADJ enwb_NOUN mae_AUX hi_PRON 'di_PART  _SPACE neud_VERB welshcakes_ADV i_ADP ni_PRON heddi_VERB a_CONJ ma_AUX pawb_PRON wedi_PART mwynhau_VERB a_CONJ disgled_VERB o_ADP goffi_NOUN a_CONJ welshcake_VERB a_CONJ cael_VERB sgwrs_NOUN fach_ADJ [_PUNCT swn_VERB cloch_VERB yn_PART canu_VERB ]_PUNCT
Bore_NOUN da_ADJ i_ADP chi_PRON gyd_ADP ._PUNCT
<_SYM SS_X >_DET Bore_NOUN da_ADJ ._PUNCT
fi_PRON eitha'_ADJUNCT flin_ADJ on_X '_PUNCT fi_PRON 'di_PART anghofio_VERB 'r_DET scale_NOUN ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT [_PUNCT gweiddi_VERB yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
Ond_CONJ [_PUNCT saib_NOUN ]_PUNCT chi_PRON gyd_ADP wedi_PART bod_AUX yn_PART b'yta_NOUN welshcakes_NOUN +_SYM
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT +_SYM
A_PART diolch_NOUN yn_PART fawr_ADJ enwb_VERB +_SYM
<_SYM SS_X >_SYM Ie_INTJ <_SYM clapio_VERB >_SYM ._PUNCT
O_ADP [_PUNCT =_SYM ]_PUNCT dia_NUM '_PUNCT dia_ADV '_PUNCT [_PUNCT /=_PROPN ]_PUNCT dia_ADV '_PUNCT anghofies_VERB i_ADP 'r_DET scale_NOUN ._PUNCT
Mae_AUX 'r_DET scale_NOUN yn_PART dod_VERB '_PUNCT da_ADJ fi_PRON bob_DET tro_NOUN ond_ADP  _SPACE [_PUNCT =_SYM ]_PUNCT ni_PRON [_PUNCT /=_PROPN ]_PUNCT ni_PRON wedi_PART cael_VERB wythnos_NOUN bach_ADJ gymleth_ADJ  _SPACE [_PUNCT saib_NOUN ]_PUNCT oni_CONJ 'n_PART tynnu_VERB fewn_ADP 'ma_ADV bore_NOUN 'ma_ADV a_CONJ oedd_VERB ddim_ADP y_DET scale_NOUN '_PUNCT da_ADJ fi_PRON ond_CONJ fi_PRON 'n_PART credu_VERB fod_VERB ran_NOUN fwya'_ADJUNCT ohonnen_NOUN nhw_PRON 'n_PART falch_ADJ fod_VERB y_DET scale_NOUN ddim_PART '_PUNCT da_ADJ fi_PRON y_DET bore_NOUN 'ma_ADV achos_NOUN mama_VERB nhw_PRON 'di_PART bod_AUX yn_PART b'yta_NOUN welshcakes_NOUN ._PUNCT
Mae_VERB llawer_PRON o_ADP 'r_DET menywod_NOUN su_ADJ '_PUNCT me_ADP mama_AUX nhw_PRON 'n_PART byw_VERB wrth_ADP 'i_PRON hunen_NOUN a_CONJ chi_PRON '_PUNCT mbo_AUX mama_NOUN nhw_PRON 'n_PART disgwl_ADJ ml'an_NOUN i_ADP bore_NOUN dydd_NOUN mercher_NOUN i_PART ddod_VERB i_ADP gael_VERB sgwrs_NOUN a_CONJ disgled_NOUN o_ADP goffi_NOUN a_CONJ cadw_VERB lan_ADV ._PUNCT
enwb_VERB i_ADP 'n_PRON enw_NOUN i_ADP 'w_PRON i_PRON 'n_PART dod_VERB 'ma_ADV  _SPACE '_PUNCT dwy_NUM fil_NUM ag_ADP un_NUM wel_NOUN dechre_ADV off_ADP i_PART golli_VERB pwyse_NOUN ond_CONJ fi_PRON 'di_PART anghofio_VERB hwnne_NOUN nawr_ADV [_PUNCT chwerthin_VERB ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Fi_PRON 'n_PART dod_VERB 'ma_ADV i_PART gwrdda_VERB lan_ADV '_PUNCT fo_PRON 'n_PART ffrindie_NOUN bob_DET wythnos_NOUN a_CONJ 'r_DET ford_NOUN '_PUNCT yn_PART yw_VERB 'r_DET ford_NOUN ddrwg_ADJ [_PUNCT saib_NOUN ]_PUNCT ni_PRON 'n_PART byte_VERB popeth_PRON ni_PRON 'n_PART mynd_VERB i_ADP 'r_DET caffi_NOUN '_PUNCT ne_NOUN straight_NOUN ar_ADP ôl_NOUN '_PUNCT yn_ADP i_ADP gal_NOUN full_NOUN english_VERB ni_PRON 'n_PART joio_NOUN 'n_PART bwyd_NOUN yn_ADP y_DET ford_NOUN '_PUNCT yn_PART [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ti_PRON 'di_PART cal_NOUN welshcake_NOUN bach_ADJ ?_PUNCT
Fi_PRON 'di_PART cal_NOUN tair_NUM yn_PART barod_ADJ ._PUNCT
Reit_VERB beth_PRON ti_PRON 'n_PART meddwl_VERB am_ADP hwnna_DET then_NOUN +_SYM
Lyfli_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM lyfli_NOUN +_SYM
[_PUNCT saib_NOUN ]_PUNCT os_CONJ tisie_ADV siwger_NOUN ne_NOUN '_PUNCT ti_PRON 'n_PART +_SYM
Na_INTJ +_SYM
Melys_ADJ digon_ADV melys_ADJ +_SYM
Digon_ADV melys_ADJ i_ADP mi_PRON ._PUNCT
enwb_VERB cyfenw_NOUN 'i_PRON fi_PRON ._PUNCT
Fi_PRON di_PRON bod_VERB yn_PART aelod_NOUN o_ADP  _SPACE <_SYM aneglur_ADJ 1_NUM >_SYM all_VERB sorts_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT bod_VERB o_PRON 'di_PART dechre_ADV a_CONJ mae_VERB 'n_PART le_NOUN da_ADJ i_ADP gymdeithasu_VERB a_CONJ cwrdd_NOUN â_ADP ffrindie_NOUN chat_NOUN fach_ADJ '_PUNCT da_ADJ pawb_PART so_ADV ma_PRON '_PUNCT 'n_PART le_NOUN bach_ADJ neis_ADJ i_PART ddod_VERB i_PART [_PUNCT saib_NOUN ]_PUNCT i_PART gael_VERB hala_VERB bore_NOUN bach_ADJ ._PUNCT
enwb_VERB yw_VERB 'n_PART enw_NOUN i_ADP a_CONJ fi_PRON 'di_PART bod_VERB yn_ADP y_DET class_NOUN 'ma_ADV ers_ADP yr_DET un_NUM cynta'_ADJUNCT <_SYM aneglur_ADJ 2_NUM ?_PUNCT >_SYM pump_NUM blynedd_NOUN yn_PART ôl_NOUN ._PUNCT
Ma'_VERBUNCT 'n_PART neis_ADJ i_PART gymysgu_VERB '_PUNCT da_ADJ 'r_DET merched_NOUN ni_PRON 'di_PART '_PUNCT nabod_VERB nhw_PRON ers_ADP blynydde_VERB a_CONJ chi_PRON 'n_PART cal_NOUN tipyn_NOUN bach_ADJ o_ADP sgwrs_NOUN a_CONJ hefyd_ADV ni_PRON 'n_PART cal_NOUN welschcakes_ADJ a_CONJ panad_NOUN o_ADP de_NOUN ._PUNCT
Bythefnos_PROPN yn_PART ôl_NOUN '_PUNCT on_X i_ADP wedi_PART neud_VERB bwyd_NOUN a_CONJ oedd_VERB ._PUNCT
187.377_NOUN
[_PUNCT cerddoriaeth_NOUN ]_PUNCT Mae_VERB un_NUM o_ADP uchafbwyntie_VERB 'r_DET flwyddyn_NOUN wedi_PART cyrraedd_VERB y_DET trip_NOUN blynyddol_ADJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Mae_VERB 'r_DET aelode_NOUN i_ADP gyd_NOUN yn_PART disgwl_ADJ mla_NOUN 'n_PART at_ADP benw'thnos_NOUN llawn_ADJ  _SPACE lleoliad_NOUN ._PUNCT
Mae_VERB 'na_ADV lot_PRON o_ADP gynllunio_VERB wedi_PART bod_VERB a_CONJ bydd_AUX neb_PRON yn_PART edrych_VERB ar_ADP y_DET scales_NOUN o_ADP gwbl_ADV [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Odi_ADP enwb_NOUN a_CONJ enwb_VERB wedi_PART pac'o_NOUN popeth_PRON tra_ADV 'n_PART cael_VERB clonc_NOUN fach_ADJ yn_ADP y_DET salon_NOUN a_CONJ beth_PRON mae_AUX __NOUN yn_PART pacio_VERB adre_ADV tybed_ADV ?_PUNCT
[_PUNCT swn_VERB sychwr_NOUN gwallt_NOUN ]_PUNCT ._PUNCT
Pedwar_VERB dydd_NOUN felly_CONJ fory_ADV dydd_NOUN sadwrn_NOUN dydd_NOUN sul_NOUN a_CONJ dydd_NOUN llun_NOUN +_SYM
Ie_INTJ ie_INTJ a_CONJ tri_NUM yn_ADP y_DET nos_NOUN ._PUNCT
Tri_NOUN nos_NOUN wedyn_ADV nos_NOUN wener_VERB nos_NOUN sadwrn_NOUN a_CONJ nos_NOUN sul_ADV [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
O_ADP ie_INTJ a_CONJ pacad_VERB cyfan_PRON o_ADP tenaladies_ADP ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Bocs_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
Sa_AUX ti_PRON 'n_PART gwbobo_VERB bebe_NOUN 'di_PART heina_VERB [_PUNCT chwerthin_VERB ]_PUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Oes_VERB ._PUNCT
Wel_AUX fi_PRON 'n_PART gobeithio_VERB gawn_VERB ni_PRON weld_VERB bach_ADJ o_ADP 'r_DET gogledd_NOUN achos_CONJ 'w_PRON i_PRON yn_PART joio_ADV myn_VERB '_PUNCT lan_ADV i_ADP north_NOUN wales_VERB [_PUNCT saib_NOUN ]_PUNCT fi_PRON wastad_ADV yn_PART [_PUNCT cerddoriaeth_NOUN ]_PUNCT ._PUNCT
tights_NOUN [_PUNCT saib_NOUN ]_PUNCT digon_PRON o_ADP trywseri_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT gwisgo_VERB hwnna_PRON i_PART fynd_VERB [_PUNCT llais_NOUN aneglur_ADJ yn_ADP y_DET cefndir_NOUN ]_PUNCT <_SYM aneglu_VERB ?_PUNCT >_SYM hwn_PRON yn_PART ffitio_VERB fi_PRON dw_AUX i_ADP di_PRON rhoi_VERB pwyse_NOUN arno_ADP [_PUNCT saib_NOUN ]_PUNCT roid_VERB o_ADP mewn_ADP '_PUNCT da_ADJ 'r_DET [_NOUN aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Mae_VERB 'r_DET dydd_NOUN yn_PART iawn_ADJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Trywser_VERB yn_PART nos_NOUN un_NUM ,_PUNCT dou_NOUN [_PUNCT saib_NOUN ]_PUNCT a_CONJ sa_AUX i_PRON 'n_PART gwbobo_VERB p'run_NOUN sy_AUX 'n_PART myn_VERB '_PUNCT i_PART ffitio_VERB fi_PRON [_PUNCT saib_NOUN ]_PUNCT achos_CONJ mama_NOUN lot_PRON o'nyn_ADP nhw_PRON rhy_ADV fach_ADJ [_PUNCT chwerthin_VERB ]_PUNCT [_PUNCT cerddoriaeth_NOUN ]_PUNCT ._PUNCT
Reit_VERB ma_VERB 'r_DET paco_NOUN 'di_PART neud_VERB then_NOUN disgwl_ADJ mla_NOUN 'n_PART i_ADP fynd_VERB nawr_ADV 'di_PART cal_NOUN brêc_NOUN fach_ADJ ma'_VERBUNCT 'n_PART neis_ADJ ._PUNCT
Mae_VERB 'r_DET all_NOUN sorts_NOUN 'di_PART helpu_VERB fi_PRON lot_PRON dros_ADP y_DET blynydde_VERB pan_CONJ oedd_VERB enwg_VERB y_DET gwr_NOUN yn_PART cael_VERB transplant_VERB afu_NOUN '_PUNCT oeddan_VERB nhw_DET gefen_NOUN mowr_NOUN pr_PRON '_PUNCT '_PUNCT nny_NOUN pan_CONJ oedd_AUX '_PUNCT e_PRON 'n_PART dost_VERB cyn_CONJ cael_VERB y_DET transplant_NOUN a_CONJ o_AUX 'n_PART i_PRON 'n_PART myn_VERB '_PUNCT bob_DET bore_NOUN dydd_NOUN mercher_NOUN achos_CONJ o_ADP 'n_PART i_PRON 'n_PART gal_NOUN brêc_ADP achos_CONJ oedd_VERB enwg_VERB ddim_PART yn_PART hwylus_ADJ a_CONJ [_PUNCT ochneidio_VERB ]_PUNCT real_ADJ help_NOUN pry_NOUN '_PUNCT '_PUNCT nny_NOUN ._PUNCT
Ti_PRON 'n_PART myn_VERB '_PUNCT mas_NOUN i_PART newid_VERB meddwl_VERB a_CONJ ti_PRON yn_PART newid_VERB meddwl_VERB achos_ADP ti_PRON 'n_PART g'bobo_VERB ti_PRON gyda_ADP rhywun_NOUN tost_NOUN fel_ADP a_CONJ mama_NOUN enwg_VERB yn_PART well_ADJ nawr_ADV a_CONJ mama_NOUN popeth_PRON yn_PART good_VERB ._PUNCT
Pan_CONJ oedd_VERB enwg_VERB yn_ADP yr_DET hospital_NOUN yn_PART lleoliad_NOUN yn_PART cael_VERB y_DET transplant_NOUN o_ADP 'n_PART i_PRON 'n_PART pawb_PRON yn_PART ffono_NOUN ar_ADP y_DET mobile_NOUN i_PART weld_VERB sud_PART odd_ADP e_PRON '_PUNCT trw_NOUN 'r_DET wythnos_NOUN a_CONJ o_ADP 'n_PART nhw_PRON 'n_PART gyd_NOUN yn_PART ti_PRON '_PUNCT g'bobo_NOUN bobo_AUX nhw_PRON 'n_PART meddwl_VERB amdano_ADP fe_PRON yn_PART help_NOUN mawr_ADJ ._PUNCT
Ganol_NOUN nos_NOUN odd_VERB 'i_PRON ond_CONJ o_ADP 'n_PART i_PRON 'n_PART tecsto_NOUN i_PRON '_PUNCT weud_VERB bod_VERB enwg_VERB yn_ADP yr_DET theatre_NOUN a_CONJ oedd_VERB pawb_PRON yn_PART tecsto_NOUN nôl_ADV i_PART weld_VERB sud_ADV oedd_VERB '_PUNCT e_DET bore_NOUN 'r_DET ôl_NOUN '_PUNCT ny_PRON ._PUNCT
Ni_PRON gyd_NOUN yn_PART agos_ADJ ni_PRON 'n_PART gefen_NOUN i_ADP 'n_PART gilydd_NOUN os_CONJ oes_VERB problem_NOUN '_PUNCT da_ADJ un_NUM ni_PRON gyd_ADV yn_PART becso_VERB ne_PRON '_PUNCT palu_VERB mewn_ADP i_ADP sorto_NOUN pethe_NOUN mas_NOUN a_CONJ pethe_NOUN fel'a_SYM ._PUNCT
Sa_AUX fi_PRON 'n_PART trio_ADV rhoi_VERB nôl_ADV helpu_VERB mas_NOUN popeth_PRON sy_VERB 'n_PART mla_NOUN 'n_PART fi_PRON 'n_PART joinio_VERB mewn_ADP a_CONJ os_CONJ oes_VERB isio_ADV neud_VERB rywbeth_NOUN os_CONJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT moyn_NOUN d'wrnod_NOUN bant_VERB fi_PRON 'n_PART stepo_NOUN mewn_ADP a_CONJ fi_PRON 'n_PART redeg_VERB y_DET bonus_NOUN ball_ADJ you_NOUN know_VERB a_CONJ fi_PRON 'n_PART codi_VERB lot_PRON o_ADP arian_NOUN ffor_CONJ '_PUNCT 'na_ADV ._PUNCT
Ma'_VERBUNCT saith_NUM o_ADP '_PUNCT ni_PRON 'n_PART myn_VERB '_PUNCT o_ADP 'r_DET street_NOUN fach_ADJ hyn_PRON ni_PRON 'n_PART disgwl_ADJ mla_NOUN 'n_PART i_PRON fyn_VERB '_PUNCT i_ADP lleoliad_NOUN mama'_NOUNUNCT wnna_NOUN 'n_PART lyfli_VERB weekend_NOUN 'na_ADV grêt_NOUN so_PART bydd_AUX dim_DET lot_NOUN o_ADP menywod_NOUN ar_ADP ôl_NOUN ar_ADP y_DET street_NOUN am_ADP y_DET penwythnos_NOUN hyn_DET ._PUNCT
414.877_ADJ
Mae_AUX 'r_DET bws_NOUN wedi_PART cyrraedd_VERB lleoliad_NOUN ag_CONJ odi_VERB 'r_DET lle_NOUN yn_PART plesio_VERB 'r_DET aelode_NOUN ._PUNCT
[_PUNCT ochneidio_VERB ]_PUNCT 'n_PART stafell_NOUN ni_PRON ma'_VERBUNCT 'n_PART beautiful_VERB [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
'_PUNCT Da_ADJ ni_PRON fel_ADP apartments_NOUN yn_PART trump_NOUN tower_ADV [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Da_ADJ ni_PRON stafell_NOUN wely_NOUN gydan_VERB ni_PART bathroom_NOUN coridor_NOUN a_CONJ wedyn_ADV mama_NOUN lounge_NOUN '_PUNCT da_ADJ ni_PRON ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM '_PUNCT Da_ADJ ni_PRON '_PUNCT im_PRON yn_PART moyn_ADV byw_VERB yma_ADV o_ADP reit_NOUN i_ADP ddod_VERB am_ADP holiday_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT lleoliad_NOUN ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP na_PRON ._PUNCT
Ni_PART 'n_PART enjoio_ADJ ma'_VERBUNCT 'r_DET room_NOUN 'ma_ADV yn_PART good_VERB a_CONJ ni_PRON gyd_NOUN ar_ADP bwys_NOUN ein_DET gilydd_NOUN ydyn_AUX ni_PRON yn_ADP y_DET part_NOUN bach_ADJ yne_NOUN so_ADJ mama_NOUN hwnna_PRON 'n_PART neis_ADJ ._PUNCT
Ma'_VERBUNCT 'r_DET bwyd_NOUN yn_PART dda_ADJ fan_NOUN '_PUNCT yn_PART on_X dyw_VERB e_PRON '_PUNCT ._PUNCT
Chi_PRON 'n_PART '_PUNCT bo_VERB chance_VERB i_ADP bawb_PRON i_PART gwrdda_VERB oedd_VERB e_PRON '_PUNCT a_CONJ wedyn_ADV ffiles_VERB i_ADP fynd_VERB am_ADP cwpl_NOUN o_ADP flynydde_VERB '_PUNCT chos_NOUN o_ADP 'n_PART i_PRON 'n_PART gofalu_VERB ar_ADP ôl_NOUN  _SPACE plant_NOUN y_DET ferch_NOUN so_ADJ wedyn_ADV  _SPACE chi_PRON 'n_PART '_PUNCT bo_VERB oedd_VERB o_PRON 'n_PART neis_ADJ i_PART droi_VERB <_SYM aneglur_ADJ 2_NUM >_SYM o_ADP 'n_PART i_PRON 'n_PART gweld_VERB isie_NOUN fe_NOUN amser_NOUN o_ADP 'n_PART i_PRON 'n_PART ffili_ADV mynd_VERB a_CONJ mae_AUX pethach_NOUN wedi_PART altro_VERB a_CONJ ni_PRON ffili_ADV ffindio_VERB 'r_DET scale_NOUN ran_NOUN fwya'_ADJUNCT o_ADP 'r_DET diwrnode_NOUN [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Mae_VERB 'r_DET scale_NOUN mewn_ADP ryw_DET gornel_NOUN bach_ADJ a_CONJ nid_PART 'i_PRON ni_PRON 'n_PART gallu_ADV ffindio_VERB fe_PRON ond_ADP  _SPACE ni_PRON 'n_PART trio_VERB 'n_PART gore_VERB ni_PRON 'n_PART trio_VERB ein_DET gore_NOUN i_ADP fod_VERB yn_PART iach_ADJ a_CONJ ffit_ADJ ._PUNCT
Ni_PART wastad_ADV yn_PART mynd_VERB i_PART wneud_VERB digon_PRON o_ADP siope_NOUN ni_PRON 'n_PART disgwl_ADJ mla_NOUN 'n_PART i_PRON fyn_VERB '_PUNCT i_ADP lleoliad_NOUN fory_ADV bydd_VERB '_PUNCT e_PRON 'n_PART  _SPACE bydd_VERB '_PUNCT e_PRON 'n_PART neis_ADJ a_CONJ  _SPACE jest_ADV ymlacio_VERB a_CONJ gobeithio_VERB bydd_VERB y_DET tywydd_NOUN yn_PART weddol_ADJ ond_CONJ os_CONJ na_PART bydd_VERB '_PUNCT e_PRON byddwn_AUX ni_PRON still_VERB yn_PART mwynhau_VERB '_PUNCT e_PRON 'r_DET un_NUM peth_NOUN ._PUNCT
Bydd_VERB sawl_ADJ person_NOUN 'ma_ADV heddi_NOUN '_PUNCT nad_PART unig_ADJ gwylie_VERB gewn_ADP nhw_PRON trw_VERB 'r_DET flwyddyn_NOUN a_CONJ mama_VERB nhw_PRON 'n_PART disgwl_ADJ mla_NOUN 'n_PART i_PART ddod_VERB bant_ADV gydan_VERB ni_PRON bob_DET blwyddyn_NOUN ._PUNCT
Fel_ADP grwp_NOUN [_PUNCT =_SYM ]_PUNCT ni_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT ni_PRON 'n_PART helpu_VERB 'n_PART gilydd_NOUN ._PUNCT
Fi_PRON wastad_ADV yn_PART gweud_VERB mama_NOUN pawb_PRON a_CONJ rhyw_DET fath_NOUN o_ADP brobleme_NOUN ond_CONJ 's_X '_PUNCT im_ADP problem_NOUN pawb_PRON yr_DET un_NUM peth_NOUN ._PUNCT
Mae_VERB sawl_ADJ gwiddw_ADJ '_PUNCT da_ADJ ni_PRON a_CONJ rai_PRON yn_PART weddol_ADV ifanc_ADJ [_PUNCT =_SYM ]_PUNCT ma_VERB [_PUNCT /=_PROPN ]_PUNCT mama_AUX nhw_PRON 'n_PART gweud_VERB mama_NOUN nhw_PRON 'n_PART disgwl_ADJ mla_NOUN 'n_ADP i_ADP bore_NOUN dydd_NOUN mercher_NOUN <_SYM cerddoriaeth_NOUN >_SYM ._PUNCT
712.723_NOUN
1622.720_NOUN
