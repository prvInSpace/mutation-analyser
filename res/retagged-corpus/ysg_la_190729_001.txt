Annwyl_ADJ Riant_PROPN
SWYDDI_NOUN RHIANT_X LYWODRAETHWYR_X -_PUNCT Corff_NOUN Llywodraethu_PROPN Ffederal_PROPN newydd_ADJ Ysgol_PROPN
enw_NOUN ac_CONJ Ysgol_ADP enw_NOUN
Bydd_AUX ffedereiddio_VERB Ysgol_NOUN enw_NOUN ac_CONJ Ysgol_NOUN enw_NOUN yn_PART dechrau_VERB fis_NOUN medi_VERB 2019_NUM ._PUNCT
Mae_VERB un_NUM corff_NOUN llywodraethu_VERB a_CONJ fydd_VERB yn_PART gyfrifol_ADJ am_ADP drosolwg_NOUN strategol_ADJ y_DET ddwy_NUM ysgol_NOUN yn_PART cael_VERB ei_DET ffurfio_VERB ar_ADP hyn_PRON o_ADP bryd_NOUN a_CONJ bydd_VERB yn_PART weithredol_ADJ ar_ADP 3_NUM Medi_PROPN 2019_NUM ._PUNCT
Bydd_VERB pedair_NUM swydd_NOUN rhiant_NOUN lywodraethwr_NOUN ar_ADP y_DET corff_NOUN llywodraethu_VERB newydd_ADJ :_PUNCT 2_NUM ar_ADP gyfer_NOUN Ysgol_NOUN enw_NOUN a_CONJ 2_NUM ar_ADP gyfer_NOUN Ysgol_NOUN enw_NOUN a_CONJ gwahoddir_VERB enwebiadau_NOUN nawr_ADV ar_ADP gyfer_NOUN y_DET swyddi_NOUN hyn_PRON ._PUNCT
Mae_VERB hwn_PRON yn_PART gyfle_NOUN cyffrous_ADJ ac_CONJ unigryw_ADJ i_ADP chi_PRON chwarae_VERB rhan_NOUN yn_PART natblygiad_NOUN y_DET ffederasiwn_NOUN newydd_ADJ ._PUNCT
Cymerwch_VERB eich_DET amser_NOUN i_PART ddarllen_VERB y_DET wybodaeth_NOUN isod_ADV ac_CONJ i_PART ystyried_VERB cynnig_NOUN eich_DET enw_NOUN ar_ADP gyfer_NOUN un_NUM o_ADP 'r_DET swyddi_NOUN fel_ADP rhiant_NOUN lywodraethwr_NOUN ._PUNCT
Pwy_PRON yw_VERB Rhiant_NOUN Lywodraethwyr_PROPN
Caiff_VERB rhiant_NOUN lywodraethwyr_NOUN eu_PRON hethol_VERB i_PART gynrychioli_VERB diddordebau_NOUN rhieni_NOUN 'r_DET disgyblion_NOUN sydd_VERB yn_ADP yr_DET ysgol_NOUN ar_ADP hyn_PRON o_ADP bryd_NOUN ._PUNCT
Mae_VERB 'r_DET un_NUM grymoedd_NOUN ,_PUNCT dyletswyddau_VERB a_CONJ chyfrifoldebau_NOUN ganddynt_ADP â_ADP phob_DET llywodraethwr_NOUN arall_ADJ ._PUNCT
Mae_VERB rhiant_NOUN lywodraethwyr_ADJ yn_PART gategori_NOUN arbennig_ADJ o_ADP lywodraethwr_NOUN ac_CONJ ,_PUNCT fel_ADP cynrychiolwyr_NOUN etholedig_ADJ ,_PUNCT maent_VERB mewn_ADP lle_NOUN da_ADJ i_PART ddeall_VERB anghenion_NOUN a_CONJ safbwyntiau_NOUN 'r_DET rhieni_NOUN ._PUNCT
Caiff_VERB rhiant_NOUN lywodraethwyr_NOUN eu_PRON hethol_VERB i_PART wasanaethu_VERB am_ADP dymor_NOUN o_ADP 4_NUM blynedd_NOUN ._PUNCT
Dyletswyddau_NOUN craidd_ADJ y_DET Corff_NOUN Llywodraethu_PROPN :_PUNCT
•_NOUN Safonau_PROPN -_PUNCT sicrhau_VERB ymagwedd_NOUN strategol_ADJ a_CONJ systematig_ADJ i_PART hyrwyddo_VERB safonau_NOUN cyrhaeddiad_NOUN addysgol_ADJ uchel_ADV ,_PUNCT presenoldeb_NOUN ac_CONJ ymddygiad_NOUN ar_ADP draws_ADJ y_DET ddwy_NUM ysgol_NOUN ,_PUNCT gan_CONJ gynnwys_VERB craffu_VERB manwl_ADJ ar_ADP ddata_NOUN perfformiad_NOUN a_CONJ gyhoeddir_VERB ;_PUNCT
•_NOUN Targedau_PROPN -_PUNCT gweithredu_VERB fel_ADP '_PUNCT cyfaill_NOUN beirniadol_ADJ '_PUNCT gan_ADP osod_NOUN targedau_NOUN lle_ADV gellir_VERB mesur_NOUN cynnydd_NOUN ar_ADP gyfer_NOUN cyrhaeddiad_NOUN a_CONJ deilliannau_ADJ disgyblion_NOUN ;_PUNCT
•_NUM Cwricwlwm_PROPN -_PUNCT sicrhau_ADV bod_VERB pob_DET dysgwr_NOUN yn_ADP y_DET ddwy_NUM ysgol_NOUN â_ADP mynediad_NOUN at_ADP gwricwlwm_NOUN cytbwys_NUM a_CONJ bod_VERB pob_DET gofyn_NOUN statudol_ADJ yn_PART cael_VERB ei_PRON ddiwallu_VERB ;_PUNCT
•_NOUN Pennu_VERB nodau_NOUN ,_PUNCT polisïau_NOUN a_CONJ blaenoriaethau_VERB 'r_DET ysgol_NOUN -_PUNCT ymwneud_VERB â_ADP Chynllun_NOUN Gwella_NOUN 'r_DET Ysgol_NOUN ,_PUNCT Hunan_PROPN Werthusiad_NOUN yr_DET Ysgol_NOUN ac_CONJ adnewyddu_VERB a_CONJ chymeradwyo_VERB polisïau_NOUN a_CONJ dogfennau_NOUN statudol_ADJ ;_PUNCT
•_NOUN Cyllid_PROPN -_PUNCT pennu_VERB a_CONJ monitro_VERB cyllidebau_NOUN 'r_DET ysgol_NOUN ;_PUNCT
•_NOUN Staffio_PROPN -_PUNCT pennu_VERB nifer_NOUN y_DET staff_NOUN ac_CONJ ymwneud_VERB â_ADP 'r_DET holl_DET brosesau_NOUN staffio_VERB gan_ADP gynnwys_VERB tâl_NOUN ,_PUNCT penodi_VERB staff_NOUN ,_PUNCT rheoli_VERB perfformiad_NOUN ,_PUNCT atal_ADV dros_ADP dro_NOUN ,_PUNCT materion_NOUN disgyblu_VERB a_CONJ diswyddo_VERB ,_PUNCT cwynion_NOUN ;_PUNCT
•_NOUN Darparu_VERB gwybodaeth_NOUN i_ADP rieni_NOUN -_PUNCT adroddiad_NOUN blynyddol_ADJ i_ADP Rieni_PROPN ,_PUNCT Prosbectws_PROPN yr_DET Ysgol_NOUN a_CONJ chyfarfodydd_NOUN y_PART bydd_VERB rhieni_NOUN yn_PART gofyn_VERB amdanynt_ADP ;_PUNCT
•_NOUN Paratoadau_NOUN arolwg_NOUN a_CONJ dilyniant_NOUN -_PUNCT gan_CONJ gynnwys_VERB cynhyrchu_VERB cynllun_NOUN gweithredu_NOUN a_CONJ monitro_AUX cynnydd_NOUN yn_PART dilyn_VERB Archwiliad_PROPN Estyn_PROPN ;_PUNCT
•_NOUN Llesiant_NOUN a_CONJ diogelu_VERB dysgwyr_NOUN -_PUNCT gan_CONJ gynnwys_VERB Prevent_PROPN a_CONJ hyrwyddo_VERB Llesiant_PROPN a_CONJ Bwyta_PROPN Iach_PROPN ;_PUNCT
Pa_PART gymwysterau_NOUN sydd_VERB eu_DET hangen_NOUN ar_ADP Lywodraethwyr_PROPN Ysgol_ADJ ?_PUNCT
Does_VERB dim_DET angen_NOUN cymwysterau_NOUN arbennig_ADJ i_ADP fod_VERB yn_PART llywodraethwr_NOUN ._PUNCT
Mae_AUX 'r_DET Awdurdod_NOUN Lleol_PROPN yn_PART rhoi_VERB gwybodaeth_NOUN a_CONJ hyfforddiant_NOUN lawn_ADJ i_ADP lywodraethwyr_NOUN ynghylch_ADP eu_DET swydd_NOUN yn_PART ogystal_ADJ â_ADP 'r_DET cyfle_NOUN i_ADP fynychu_VERB cyrsiau_NOUN hyfforddi_NOUN ._PUNCT
Mae_AUX 'n_PART rhaid_VERB i_ADP bob_DET llywodraethwr_NOUN newydd_ADJ gwblhau_VERB rhaglen_NOUN sefydlu_VERB gorfodol_ADJ ._PUNCT
Mae_AUX 'n_PART rhaid_VERB i_ADP lywodraethwyr_NOUN a_CONJ 'r_DET rhai_PRON sy_AUX 'n_PART ymgymryd_VERB ag_CONJ ail_ADJ dymor_NOUN hefyd_ADV gwblhau_VERB hyfforddiant_NOUN gorfodol_ADJ ar_ADP ddata_NOUN ._PUNCT
Bydd_AUX yr_DET Awdurdod_NOUN Lleol_PROPN yn_PART rhoi_VERB manylion_NOUN ynghylch_ADP y_DET rhain_PRON ._PUNCT
Beth_PRON wnaf_VERB i_ADP nesaf_ADJ ?_PUNCT
Os_CONJ carech_VERB fod_VERB yn_PART rhiant_NOUN lywodraethwr_NOUN ,_PUNCT cwblhewch_VERB y_DET ffurflen_NOUN enwebu_VERB sydd_VERB ynghlwm_ADJ â_ADP 'r_DET llythyr_NOUN hwn_DET a_CONJ 'i_PRON ddychwelyd_VERB i_ADP 'r_DET ysgol_NOUN erbyn_ADP 3_NUM Mehefin_NOUN 2019_NUM ._PUNCT
Ni_PART dderbynnir_AUX unrhyw_DET enwebiadau_NOUN wedi_ADP 'r_DET dyddiad_NOUN hwn_DET ._PUNCT
Os_CONJ derbynnir_VERB mwy_PRON o_ADP enwebiadau_NOUN nag_CONJ sydd_VERB o_ADP swyddi_NOUN gweigion_NOUN ,_PUNCT caiff_VERB etholiad_NOUN ei_DET gynnal_VERB a_CONJ gaiff_VERB ei_PRON benderfynu_VERB ar_ADP sail_NOUN mwyafrif_NOUN syml_ADJ ._PUNCT
Byddai_VERB amserlen_NOUN unrhyw_DET etholiad_NOUN fel_ADP a_NOUN -_PUNCT ganlyn_NOUN :_PUNCT
Dyddiad_NOUN cau_VERB enwebiadau_NOUN :_PUNCT 3_NUM Mehefin_X 2019_NUM
Cyhoeddi_VERB papurau_NOUN pleidleisio_VERB (_PUNCT os_CONJ oes_VERB angen_NOUN etholiad_NOUN )_PUNCT :_PUNCT 7_NUM Mehefin_X 2019_NUM
Dychwelyd_VERB papurau_NOUN pleidleisio_VERB :_PUNCT 21_NUM Mehefin_NOUN 2019_NUM
Rwy_AUX 'n_PART gobeithio_VERB y_PART gwnewch_VERB chi_PRON ystyried_VERB bod_VERB yn_PART rhiant_NOUN lywodraethwr_NOUN ._PUNCT
Bydd_VERB y_DET corff_NOUN llywodraethu_VERB newydd_ADV yn_PART croesawu_VERB eich_PRON ymwneud_VERB â_ADP 'i_PRON waith_NOUN hanfodol_ADJ i_ADP hyrwyddo_VERB cyrhaeddiad_NOUN ,_PUNCT llesiant_VERB a_CONJ datblygiad_NOUN yr_DET holl_DET ddisgyblion_NOUN yn_ADP Ysgol_NOUN enw_NOUN ac_CONJ Ysgol_ADP enw_NOUN ._PUNCT
Meddyliwch_VERB am_ADP y_DET peth_NOUN ._PUNCT
Yn_PART gywir_ADJ
enw_NOUN cyfenwenw_NOUN
Pennaeth_NOUN Gweithredol_ADJ
