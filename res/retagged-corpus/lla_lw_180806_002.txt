O_ADP wna'_VERBUNCT i_PRON ddweud_VERB '_PUNCT y_DET 'n_PART hanes_NOUN o_ADP bebe_NOUN wnes_ADP i_PRON [_PUNCT -_PUNCT ]_PUNCT
Ia_ADJ na_CONJ well_ADJ '_PUNCT nni_NOUN beidio_VERB dw_VERB i_ADP meddwl_VERB
Bo_VERB chdi_NOUN newydd_ADJ gael_VERB dy_DET ddal_VERB yn_PART siarad_VERB i_ADP mewn_ADP i_ADP hwn_PRON ._PUNCT
So_PART mae_AUX pawb_PRON 'di_PART clywed_VERB chdi_NOUN yn_PART trafod_VERB y_DET cor_NOUN yn_ADP y_DET genedlaethol_ADJ rwan_ADV ._PUNCT
[_PUNCT cloch_VERB yn_PART canu_VERB dwy_NUM waith_NOUN ]_PUNCT
[_PUNCT =_SYM ]_PUNCT Do_VERB do_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Do_INTJ a_CONJ wed_VERB [_PUNCT -_PUNCT ]_PUNCT wnes_VERB i_PRON ddim_PART dweud_VERB dim_DET byd_NOUN annifyr_ADJ â_CONJ dweud_VERB y_DET gwir_NOUN
Na_VERB nid_PART gymaint_ADV â_ADP hynny_PRON ._PUNCT
Nid_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Na_VERB nid_PART gymaint_ADV â_ADP hynny_PRON
Ond_CONJ am_ADP neithiwr_ADV ._PUNCT
Ia_INTJ ._PUNCT
Ia_INTJ ._PUNCT
So_NOUN ia_PRON ia_SYM
Ar_ADP y_DET diwedd_NOUN wnes_VERB i_PRON jest_ADV dweud_VERB am_ADP neithiwr_ADV bobo_VERB fi_PRON yn_PART siomedig_ADJ iawn_ADV iawn_ADV yn'yn_VERB nhw_PRON a_PART felly_ADV ymlaen_ADV a_CONJ wrth_ADP gwrs_NOUN to_NOUN '_PUNCT ni_PRON 'n_PART gw'bod_VERB dim_DET byd_NOUN a_CONJ dw_AUX i_PRON 'n_PART wneud_VERB o_ADP eto_ADV rwan_ADV ._PUNCT
Na_INTJ ia_PRON
Haha_PROPN
Dio_NOUN 'm_PRON otch_VERB dio_VERB 'm_DET o_ADP bwys_NOUN '_PUNCT chos_NOUN mae_VERB honno_PRON 'n_PART ffaith_NOUN bod_VERB na_CONJ ddim_PART digon_PRON o_ADP Gymraeg_PROPN o_ADP gwmpas_NOUN y_DET lle_NOUN fel_ADP wnaethon_VERB ni_PRON ffendio_VERB lawr_ADV fyn_VERB '_PUNCT cw_NOUN rwan_PRON ._PUNCT
<_SYM pesychu_VERB >_SYM
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
Do_INTJ ?_PUNCT
Yn_ADP y_DET lle_NOUN coffi_NOUN ?_PUNCT
Naci_PROPN ._PUNCT
Pan_CONJ oni_NOUN 'n_PART cerddad_NOUN yn_PART fan_NOUN 'na_ADV dw_AUX i_PRON 'di_PART cael_VERB '_PUNCT yn_PART stopio_VERB ddwy_NUM waith_NOUN rwan_DET gin_NOUN rhywun_NOUN isio_ADV sgwrsio_VERB a_CONJ dw_AUX i_PRON 'di_PART gofyn_VERB "_PUNCT '_PUNCT Da_ADJ chi_PRON 'n_PART siarad_VERB Cymraeg_PROPN ?_PUNCT "_PUNCT a_CONJ mae_AUX nhw_PRON wedi_PART d'eud_VERB "_PUNCT na_INTJ "_PUNCT ._PUNCT
O_ADP fan_NOUN draw_ADV [_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
O_ADP reit_NOUN ._PUNCT
Yn_ADP y_DET cytia_VERB oedd_VERB 'na_ADV ddyn_NOUN Arriva_X yn_ADP ei_DET stondin_NOUN Arriva_PROPN ._PUNCT
'_PUNCT Nath_PROPN o_ADP stopio_VERB fi_PRON fel_ADP oni_NOUN 'n_PART pasio_VERB
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
A_CONJ '_PUNCT nath_NOUN o_ADP dd'eud_VERB "_PUNCT good_VERB morning_NOUN "_PUNCT
BeBe_ADV y_DET trene_AUX ti_PRON 'n_PART sôn_VERB am_ADP ie_INTJ ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Wel_CONJ cwmni_NOUN Arriva_X ia_X ._PUNCT
A_CONJ wedyn_ADV '_PUNCT nath_NOUN y_DET ferch_NOUN wrth_ADP ei_DET ochr_NOUN o_ADP fynd_VERB yn_PART reit_NOUN amddiffynnol_ADJ a_PART dweud_VERB "_PUNCT Wel_INTJ dw_AUX i_PRON 'n_PART siarad_VERB Cymraeg_PROPN ,_PUNCT "_PUNCT ond_CONJ y_DET dyn_NOUN arall_ADJ wnaeth_VERB stopio_VERB fi_PRON ._PUNCT
A_CONJ mi_PRON o'dd_NOUN 'na_ADV riwun_NOUN arall_ADJ nes_CONJ ymlaen_ADV wedyn_ADV ._PUNCT
O_ADP reit_INTJ [_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Sôn_VERB am_ADP Arriva_X hynny_PRON yw_VERB mae_VERB nhw_PRON yn_PART fan_NOUN 'na_ADV rwan_PRON ._PUNCT
Ydyn_VERB nhw_PRON yn_PART dal_ADV i_ADP fod_VERB yn_PART rhedag_NOUN bysus_NOUN pan_CONJ mama_AUX nhw_PRON wedi_PART colli_VERB y_DET trenau_NOUN ?_PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gw'bod_ADJ ._PUNCT
Dw_AUX i_PRON 'm_DET yn_PART gw'bod_ADJ ._PUNCT
Mae_AUX nhw_PRON 'di_PART colli_VERB contract_NOUN trenau_NOUN yn_PART do_VERB ?_PUNCT
Do_INTJ ._PUNCT
ond_CONJ 's_PART dim_DET sôn_VERB am_ADP [_PUNCT saib_NOUN ]_PUNCT bysus_NOUN
Ia_INTJ -_PUNCT con_NOUN contract_NOUN o_ADP Gaerdydd_PROPN i_ADP fyny_ADV ynde_ADP ?_PUNCT
enwg_VERB cyfenw_NOUN
Pwy_PRON ?_PUNCT
enwg_VERB cyfenw_NOUN ._PUNCT
Yn_PART cerdded_VERB draw_ADV fan_NOUN 'na_ADV ._PUNCT
Ie_INTJ mae_VERB hwn_PRON yn_PART le_NOUN da_ADJ jest_ADV i_ADP isda_VERB i_ADP wylio_VERB ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT edrych_VERB pwy_PRON sy_AUX 'n_PART mynd_VERB heibio_ADV ._PUNCT
People_VERB wathcing_NOUN go_ADV iawn_ADJ ._PUNCT
Ia_INTJ ._PUNCT
Ond_CONJ  _SPACE 'di_PART o_ADP 'm_DET gymaint_ADV lle_ADV â_ADP platiad_NOUN fel_ADP arfar_NOUN nac_CONJ 'di_PART ?_PUNCT
O_ADP bell_NOUN ffor_CONJ '_PUNCT ._PUNCT
Y_DET capital_NOUN cuisine_NOUN sy_VERB ar_ADP [_PUNCT -_PUNCT ]_PUNCT erm_NOUN
BeBe_VERB hwnna_PRON ar_ADP y_DET top_NOUN ?_PUNCT
Ia_INTJ hwnna_PRON mae_VERB na_PART lle_NOUN [_PUNCT -_PUNCT ]_PUNCT
Ia_ADJ ond_CONJ yn_PART fan_NOUN hyn_DET rwan_ADV de_NOUN capital_VERB cuisine_NOUN 'di_PART 'r_DET  _SPACE [_PUNCT -_PUNCT ]_PUNCT dyma_DET nhw_PRON oedd_AUX yn_PART gwneud_VERB o_ADP 'r_DET blaen_NOUN
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
O_ADP
Ond_CONJ wedi_PART dweud_VERB hynny_PRON mi_PRON oedd_VERB yr_DET  _SPACE y_DET platiad_NOUN o_ADP fwyd_NOUN gath_NOUN [_PUNCT aneglur_ADJ ]_PUNCT neithiwr_ADV yn_PART [_PUNCT aneglur_ADJ ]_PUNCT
O_ADP oedd_VERB
Oedd_VERB neis_ADJ iawn_ADV
[_PUNCT Tarfu_NOUN ar_ADP y_DET meicroffon_NOUN ]_PUNCT
Dw_AUX i_PRON yn_PART mynd_VERB i_PART gael_VERB hwnna_PRON [_PUNCT aneglur_ADJ ]_PUNCT eto_ADV
Ia_ADJ mae_VERB hwnna_PRON 'n_PART syniad_NOUN
Achos_ADP peth_NOUN 'di_PART dw_AUX i_ADP 'm_DET yn_PART mynd_VERB i_ADP unrhyw_DET gyngherdda_VERB arall_ADJ yn_ADP y_DET nos_NOUN ._PUNCT
Mae_VERB 'n_PART anodd_ADJ gen_ADP i_PRON feddwl_VERB y_PART bydda'_VERBUNCT i_PRON isio_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ella_ADV bydd_VERB rhaid_VERB imi_CONJ beidio_ADV dod_VERB i_ADP fewn_ADP tan_ADP ar_ADP ôl_NOUN cinio_NOUN rhyw_DET dd'wrnod_NOUN  _SPACE mwyn_NOUN gallu_ADV aros_VERB [_PUNCT aneglur_ADJ ]_PUNCT
Ia_ADJ a_CONJ cael_VERB dy_DET fwyd_NOUN yma_ADV ._PUNCT
Ia_ADJ bydd_VERB rhaid_VERB imi_ADP weld_VERB
Ia_ADJ bydd_VERB rhaid_VERB imi_ADP weld_VERB [_PUNCT -_PUNCT ]_PUNCT ond_CONJ mae_VERB [_PUNCT -_PUNCT ]_PUNCT ia_NOUN
Nos_NOUN Iau_ADJ efo_ADP Al_NOUN Lewis_PROPN faswn_VERB i_ADP 'm_DET yn_PART meindio_VERB
Ti_PRON isio_ADV gweld_VERB hwnna_PRON yn_PART dwyt_ADJ ?_PUNCT
Yndw_INTJ ._PUNCT
Ond_CONJ meddylia_VERB am_ADP yr_DET holl_DET llefydd_NOUN yna_ADV rwan_PRON sydd_VERB rownd_ADV y_DET ffrynt_NOUN yn_PART fan_NOUN 'na_ADV sy_VERB fel_ADP arfar_NOUN yn_PART '_PUNCT gorad_NOUN i_PART wneud_VERB bwyd_NOUN ._PUNCT
Ma'_VERBUNCT fyna_ADV i_ADP gyd_ADP i_ADP fyta_NOUN yno_ADV fo_PRON hefyd_ADV yn_ADP dy_PRON 'n_PART ?_PUNCT
<_SYM aneglur_ADJ 2_NUM >_SYM [_PUNCT =_SYM ]_PUNCT Yndi_PART yndi_ADJ [_PUNCT /=_PROPN ]_PUNCT a_CONJ 'r_DET dewis_NOUN ._PUNCT
Oes_VERB
Oes_VERB mae_VERB 'na_ADV le_NOUN Italian_PROPN da_ADJ a_CONJ mae_VERB na_PART le_NOUN pizza_NOUN da_ADJ iawn_ADV yna_ADV [_PUNCT saib_NOUN ]_PUNCT Ydi_PROPN
[_PUNCT Tarfu_NOUN ar_ADP y_DET meicroffon_NOUN ]_PUNCT
Wel_INTJ unwaith_ADV '_PUNCT da_ADJ ni_PRON 'di_PART byta_VERB 'r_DET petha_NOUN barbeciw_NOUN 'ma_ADV heno_ADV 's_CONJ dim_DET ots_NOUN wedyn_ADV os_CONJ nad_PART ydw_AUX i_PRON 'n_PART byta_VERB yn_ADP y_DET garafan_NOUN lawer_ADJ wedyn_ADV [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
Ia_INTJ
Wel_ADV dw_VERB i_ADP angen_NOUN siarad_VERB fory_ADV efo_ADP enwg_VERB neu_CONJ heno_ADV ._PUNCT
Un_NUM ai_PART mae_AUX nhw_PRON yn_PART dod_VERB atan_ADP ni_PRON i_ADP 'r_DET adlen_NOUN a_CONJ wnawn_VERB ni_PART neud_VERB rh'wbath_NOUN neu_CONJ  _SPACE '_PUNCT nai_PART ofyn_VERB iddi_ADP hi_PRON ffindio_VERB rh'wla_NOUN yn_ADP Cathedral_ADJ Road_NOUN ._PUNCT
Mae_VERB hi_PRON reit_NOUN pernickety_NOUN os_CONJ rh'wbath_NOUN ._PUNCT
'_PUNCT Sa_AUX nhw_PRON yn_PART medru_ADV dod_VERB atan_ADP ni_PRON am_ADP swpar_NOUN i_ADP fana_ADV ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON ._PUNCT
Ia_INTJ [_PUNCT /=_PROPN ]_PUNCT
'_PUNCT sa_AUX ni_PRON 'n_PART gwneud_VERB hyna_ADV ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB mai_PART hwnna_PRON 'di_PART 'r_DET atab_NOUN dweud_VERB y_DET gwir_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB ella_ADV bydd_VERB enwg_VERB yma_DET erbyn_ADP hynny_PRON
Wel_INTJ ia_PRON
Wel_INTJ hwyr_ADJ heno_ADV 'na_ADV 'th_NOUN o_ADP ddweud_VERB ond_CONJ ga_NOUN i_PRON w'bod_VERB yn_PART munud_NOUN caf_ADJ ?_PUNCT
Ti_PRON 'di_PART siarad_VERB efo_ADP fo_PRON bore_NOUN 'ma_ADV ?_PUNCT +_SYM
Bora_VERB 'ma_ADV ._PUNCT
Do_INTJ ._PUNCT +_SYM
+_VERB Jest_PROPN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP ia_DET peth_NOUN cynta'_ADJUNCT ia_PRON ?_PUNCT
+_VERB Jest_PROPN ia_PRON ._PUNCT
Oedd_AUX o_PRON yn_PART mynd_VERB i_ADP mewn_ADP erbyn_ADP hannar_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Na_INTJ
Doedd_AUX o_PRON 'm_DET yn_PART gw'bod_VERB dim_DET byd_NOUN erbyn_ADP hynny_PRON nag_CONJ oedd_VERB
'Di_PART o_ADP yn_PART mynd_VERB i_ADP ffonio_VERB chdi_NOUN ar_ADP ôl_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ddod_VERB allan_ADV yndi_ADJ ?_PUNCT
Yndi_INTJ ._PUNCT
Dw_AUX i_PRON 'di_PART gofyn_VERB iddo_ADP fo_PRON neud_VERB rwan_ADV
ym_ADP ._PUNCT
Ond_CONJ roish_NOUN i_ADP hwn_PRON i_ADP ffwrdd_ADV tra_CONJ o_ADP 'n_PART i_PRON yn_ADP y_DET lle_NOUN rwan_DET ._PUNCT
[_PUNCT swn_VERB gwynt_NOUN neu_CONJ darfu_VERB ar_ADP y_DET meicroffon_NOUN ]_PUNCT
[_PUNCT swn_VERB gwynt_NOUN neu_CONJ darfu_VERB ar_ADP y_DET meicroffon_NOUN ]_PUNCT
So_INTJ 'di_PART o_ADP 'm_DET yn_PART hollol_ADV siŵr_ADJ os_CONJ 'di_PART o_ADP 'n_PART mynd_VERB i_PART deithio_VERB heno_ADV neu_CONJ beidio_VERB nach_ADV 'di_PART ?_PUNCT
Gweld_VERB bebe_NOUN wneith_VERB y_DET meddyg_NOUN dd'eud_VERB rwan_ADV ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
[_PUNCT Tarfu_NOUN ar_ADP y_DET meicroffon_NOUN ]_PUNCT
Wel_INTJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB beth_PRON bynnag_PRON wneith_VERB y_DET meddyg_NOUN dd'eud_VERB fydd_VERB o_ADP isio_VERB dwad_NOUN i_ADP ganu_VERB ond_CONJ y_DET cwestiwn_NOUN ydy_VERB ydan_AUX ni_PRON 'n_PART mynd_VERB yn_PART ôl_NOUN adra_ADV wedyn_ADV ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
Os_CONJ ydy_VERB hi_PRON 'n_PART nos_NOUN '_PUNCT fory_ADV ne_NOUN '_PUNCT bora_VERB Dydd_PROPN Merchar_PROPN ond_CONJ gawn_VERB ni_PRON weld_VERB ._PUNCT
Ac_CONJ ella_ADV [_PUNCT -_PUNCT ]_PUNCT
Mae_VERB o_PRON isio_ADV dod_VERB i_ADP ganu_VERB gymaint_ADV â_ADP hynny_PRON yn_PART 'di_PART cradur_NOUN ?_PUNCT
Wel_CONJ dyna_DET 'r_DET peth_NOUN ._PUNCT
Ti_PRON 'di_PART bod_VERB [_PUNCT -_PUNCT ]_PUNCT [_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
Wel_INTJ dw_AUX i_PRON 'n_PART teimlo_VERB drosto_ADP fo_PRON bod_VERB o_PRON 'di_PART bod_VERB yn_PART [_PUNCT aneglur_ADJ ]_PUNCT efo_ADP fo_PRON ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'di_PART bod_VERB yn_ADP bob_DET ymarfar_NOUN ._PUNCT
Fasa_NOUN fo_PRON dipyn_NOUN bach_ADJ o_ADP [_PUNCT -_PUNCT ]_PUNCT
ti_PRON 'n_PART g'bobo_SYM [_PUNCT saib_NOUN ]_PUNCT Ia_INTJ [_PUNCT saib_NOUN ]_PUNCT [_PUNCT aneglur_ADJ ]_PUNCT battered_NOUN pride_NOUN
Pan_CONJ ti_PRON 'n_PART gwario_VERB blwyddyn_NOUN o_ADP gweithio_VERB ar_ADP y_DET [_PUNCT -_PUNCT ]_PUNCT yr_DET ymarferion_NOUN a_CONJ dysgu_VERB pob_DET peth_NOUN
Dw_VERB i_PRON 'n_PART siŵr_ADJ fod_AUX enwg_VERB yn_PART edrych_VERB ymlaen_ADV '_PUNCT leni_ADV enwg_VERB ac_CONJ __NOUN achos_CONJ nathon_VERB nhw_PRON fethu_ADV dod_VERB llynadd_ADJ oherwydd_CONJ salwch_NOUN __NOUN __NOUN __NOUN do_PRON
[_PUNCT Swn_PROPN chwerthin_VERB yn_ADP y_DET cefndir_NOUN ]_PUNCT
O_ADP do_PRON
[_PUNCT Swn_PROPN chwerthin_VERB a_CONJ phesychu_VERB yn_ADP y_DET cefndir_NOUN ]_PUNCT
'_PUNCT D_NUM '_PUNCT y_DET o_ADP 'm_DET yn_PART gwneud_VERB llawer_NOUN o_ADP wahania_VERB 'th_PRON sydd_VERB yn_PART [_PUNCT -_PUNCT ]_PUNCT
Ia_INTJ ._PUNCT
Ia_ADJ mae_VERB 'r_DET bwrdd_NOUN '_PUNCT mama_NOUN chydig_ADJ bach_ADJ yn_PART sigledig_ADJ yn_PART tydi_VERB ?_PUNCT
BeBe_NOUN ?_PUNCT
Tydi_VERB o_ADP ddim_PART yn_PART [_PUNCT -_PUNCT ]_PUNCT ?_PUNCT
Sigledig_PROPN
O_ADP reit_NOUN
'_PUNCT Da_ADJ ni_PRON 'di_PART treulio_VERB 'r_DET holl_DET flynyddoedd_NOUN acw_ADV yn_PART beio_VERB y_DET lloria_NOUN cam_PRON am_ADP bwr_NOUN '_PUNCT gegin_NOUN ni_PRON achos_NOUN [_PUNCT -_PUNCT ]_PUNCT
Mae_VERB 'n_PART woblo_VERB yn_ADP y_DET ty_NOUN newydd_ADJ
Mae_VERB 'n_PART woblo_VERB yn_ADP y_DET ty_NOUN newydd_ADJ
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT
So_NOUN ar_ADP ôl_NOUN ugian_NOUN mlynadd_NOUN '_PUNCT da_ADJ ni_PRON 'di_PART sylweddoli_VERB mai_PART ar_ADP y_DET bwr_NOUN 'ma_ADV 'r_DET bai_NOUN cos_NOUN '_PUNCT da_ADJ ni_PRON dal_ADV i_ADP roi_VERB coasters_NOUN o_ADP dan_ADP [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT pesychu_VERB ]_PUNCT
BeBe_VERB 'di_PART hwnna_PRON wan_ADJ ta_CONJ ?_PUNCT
Mae_VERB gen_ADP ti_PRON llwybyr_VERB comedi_NOUN ar_ADP y_DET maes_NOUN yn_PART fane_ADJ
Wel_CONJ mae_VERB 'na_ADV ._PUNCT
Os_CONJ ti_PRON 'n_PART edrych_VERB ar_ADP rhaglen_NOUN y_DET dydd_NOUN mae_VERB na_DET section_NOUN comedi_NOUN yna_ADV
Ie_INTJ
Oes_VERB ond_ADP mama_NOUN hwnna_PRON yn_PART lwybyr_ADJ comedi_NOUN wyt_AUX ti_PRON 'n_PART mynd_VERB o_ADP gwmpas_NOUN a_CONJ mae_AUX 'na_ADV bobl_NOUN yn_PART [_PUNCT -_PUNCT ]_PUNCT
Ia_INTJ ._PUNCT
O_ADP reit_NOUN
Mae_VERB 'na_ADV stand_NOUN ups_ADJ mewn_ADP gwahanol_ADJ lefydd_NOUN a_CONJ dw_AUX i_PRON 'n_PART meddwl_VERB mai_PART dyna_PRON 'di_PART o_ADP ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB '_PUNCT dw_AUX i_PRON 'di_PART darllan_NOUN rh'wbath_NOUN
Mae_VERB enwb_NOUN [_PUNCT aneglur_ADJ ]_PUNCT enwb_NOUN yn_PART gwneud_VERB stand_NOUN up_ADJ '_PUNCT wan_ADJ 's_X di_PRON ._PUNCT
Mae_AUX 'n_PART gwneud_VERB lot_PRON ._PUNCT
Ti_PRON 'n_PART gwbod_VERB enwb_NOUN cyfenw_NOUN ?_PUNCT
O_ADP ydy_VERB ?_PUNCT
Ti_PRON 'n_PART gw'bod_VERB enwb_VERB yr_DET hynaf_NOUN ?_PUNCT
Mae_AUX hi_PRON 'n_PART gwneud_VERB stand_NOUN up_NUM ._PUNCT
Mae_VERB hi_PRON 'n_PART un_NUM o_ADP 'r_DET unig_ADJ ferched_VERB dw_AUX i_PRON 'n_PART meddwl_VERB sy_AUX 'n_PART gwneud_VERB
O_ADP reit_NOUN ._PUNCT
Oce_VERB
[_PUNCT swn_NOUN aneglur_ADJ ailadroddus_ADJ ]_PUNCT
Su_NOUN '_PUNCT mae_VERB ?_PUNCT
Su_NOUN '_PUNCT '_PUNCT da_ADJ chi_PRON ?_PUNCT
Ew_VERB '_PUNCT helo_NOUN ._PUNCT
Su_NOUN '_PUNCT mae_VERB bois_VERB ?_PUNCT
Ti_PRON 'n_PART iawn_ADJ ?_PUNCT
Helo_INTJ
Un_NUM o_ADP 'r_DET manteision_NOUN gwisgo_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Dw_AUX i_PRON 'm_DET yn_PART gweld_VERB yn_PART rhy_ADV dda_ADJ ._PUNCT
Sbectol_NOUN yrru_VERB ._PUNCT
Ie_INTJ
O_ADP reit_NOUN <_SYM cherthin_NOUN >_SYM
Ond_CONJ un_NUM o_ADP 'r_DET manteision_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART gweld_VERB pobl_NOUN yn_PART well_ADJ ond_CONJ dydi_AUX bob_DET tro_NOUN pobl_NOUN ddim_PART yn_PART '_PUNCT y_DET '_PUNCT nabod_VERB i_ADP [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Sut_ADV mama_NOUN petha_NOUN 'n_PART mynd_VERB ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
Iawn_INTJ diolch_INTJ
Wel_INTJ +_SYM
+_VERB '_PUNCT da_ADJ ni_PRON 'n_PART ymarfer_VERB bore_NOUN fory_ADV [_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Gawn_VERB ni_PRON weld_VERB fory_ADV
Ia_INTJ
Dyna_ADV ni_PRON wedyn_ADV ._PUNCT
Cawn_VERB weld_VERB '_PUNCT de_NOUN
Ia_INTJ
A_CONJ 'r_DET prif_ADJ reswm_NOUN am_ADP ddod_VERB i_ADP mewn_ADP '_PUNCT fory_ADV ._PUNCT
'_PUNCT Da_ADJ ni_PRON Dydd_NOUN Sadwrn_PROPN a_CONJ '_PUNCT da_ADJ ni_PRON yma_ADV heddiw_ADV '_PUNCT lly_NOUN
Haha_PROPN
'_PUNCT Dw_VERB i_PRON 'n_PART falch_ADJ achos_ADP fy_DET 'di_PART 'n_PART gwrando_VERB ar_ADP y_DET cwbl_NOUN ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'n_PART deall_VERB rwan_ADV mae_VERB 'na_ADV saith_NUM ohonan_VERB ni_PRON yn'd_VERB oes_NOUN ?_PUNCT
Saith_NOUN cor_NOUN ond_CONJ mama_NOUN y_DET pobl_NOUN sy_AUX wedi_PART canu_VERB oedd_AUX adlais_NOUN yn_PART sôn_VERB bod_AUX nhw_PRON 'n_PART cael_VERB eu_DET rhoi_VERB mewn_ADP '_PUNCT stafall_NOUN yn_ADP y_DET cefn_NOUN jyst_X cyn_CONJ canu_VERB ._PUNCT
Ia_INTJ ._PUNCT
a_PART wedyn_ADV ti_PRON 'n_PART mynd_VERB ar_ADP y_DET llwyfan_NOUN a_CONJ wedyn_ADV ti_PRON 'n_PART goro_ADV mynd_VERB o_ADP 'r_DET pafiliwn_NOUN ._PUNCT
Fedri_VERB di_PRON ddim_PART dod_VERB yn_PART ôl_NOUN i_ADP fewn_ADP wedyn_ADV i_PART wrando_VERB ar_ADP y_DET lleill_NOUN ._PUNCT
Doe_VERB 'na_ADV 'm_DET lle_NOUN cefn_NOUN llwyfan_NOUN ._PUNCT
Mae_VERB 'na_ADV lot_PRON o_ADP le_NOUN fel_ADP arfar'd_PUNCT oes_VERB ?_PUNCT
Felly_CONJ ti_PRON jest_ADV ddim_PART yn_PART gwybod_VERB
Oes_VERB
Fath_NOUN â_ADP pafiliwn_NOUN pan_CONJ mae_VERB 'na_ADV le_NOUN ._PUNCT
A_CONJ 'di_PART o_ADP ddim_ADP y_DET math_NOUN o_ADP sefyllfa_NOUN lle_NOUN mama_NOUN gin_ADP ti_PRON amsar_NOUN jest_ADV i_PART ddod_VERB syth_ADJ oddi_ADP arna_ADP fo_PRON a_CONJ gwrando_VERB ar_ADP y_DET gweddill_NOUN neu_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_CONJ gan_CONJ bod_AUX ni_PRON 'n_PART chweched_VERB yn_PART canu_VERB +_SYM
Chweched_VERB ?_PUNCT
+_VERB [_PUNCT =_SYM ]_PUNCT chawn_VERB ni_PRON ddim_PART chawn_VERB ni_PRON ddim_ADV [_PUNCT /=_PROPN ]_PUNCT siŵr_ADJ o_ADP fod_VERB gwarano_NOUN ar_ADP [_PUNCT -_PUNCT ]_PUNCT
<_SYM SS_X >_SYM [_PUNCT =_SYM ]_PUNCT Na_VERB na_INTJ na_PART na_INTJ [_PUNCT /=_PROPN ]_PUNCT
'_PUNCT S_NUM na_CONJ 'm_DET modd_NOUN i_ADP ni_PRON fynd_VERB i_ADP fewn_ADP nag_CONJ oes_VERB ?_PUNCT
'_PUNCT S_NUM na_CONJ 'm_DET amsar_NOUN ichi_ADP fod_VERB yn_ADP y_DET gynulleidfa_NOUN a_CONJ piciad_NOUN allan_ADV nag_CONJ oes_VERB ?_PUNCT
Dyna_ADV wedd_NOUN arall_ADJ ar_ADP y_DET '_PUNCT steddfod_NOUN 'ma_ADV yn_PART de_NOUN ?_PUNCT +_SYM
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
+_VERB Hynny_PRON 'di_PART mae_VERB 'na_ADV lot_NOUN fawr_ADJ o_ADP betha'_VERBUNCT i_PART ddweud_VERB drosta_VERB fo_PRON <_SYM aneglur_ADJ 2_NUM >_SYM yn_ADP ei_DET erbyn_NOUN o_PRON ond_CONJ ia_PRON mae_VERB hwnna_PRON yn_PART un_NUM o_ADP 'r_DET petha_NOUN [_PUNCT -_PUNCT ]_PUNCT
'_PUNCT Dw_VERB i_PRON 'n_PART wir_ADJ yn_PART siomedig_ADJ ynglyn_VERB â_ADP hynna_PRON ._PUNCT
Mae_AUX '_PUNCT enwg_VERB yn_PART byw_VERB ym_ADP Mhenarth_PROPN ._PUNCT
Dyna_ADV sut_ADV '_PUNCT da_ADJ ni_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Sydd_VERB yn_PART hwylus_ADJ i_ADP ni_PRON ._PUNCT
Wnaetho_VERB ni_PRON gerdded_VERB i_ADP mewn_ADP ._PUNCT
Cerdded_VERB ar_ADP hyd_NOUN y_DET morglawdd_NOUN Dydd_NOUN Sadwrn_PROPN
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Ia_INTJ ._PUNCT
O_ADP reit_NOUN
[_PUNCT Swn_PROPN larwm_NOUN drydannol_ADJ yn_PART canu_VERB dwy_NUM waith_NOUN ]_PUNCT
ond_CONJ ia_PRON mae_VERB 'na_ADV gymydog_NOUN iddo_ADP fo_PRON sydd_AUX yn_PART canu_VERB yn_ADP  _SPACE Cor_NOUN Canna_PROPN ._PUNCT
Ie_INTJ ._PUNCT
A_PART mae_VERB hi_PRON 'n_PART Cor_NOUN '_PUNCT Steddfod_X ._PUNCT
O_ADP ia_PRON ._PUNCT
Ac_CONJ oedd_AUX hi_PRON 'n_PART d'eud_VERB "_PUNCT O_ADP fydd_VERB na_DET dri_NUM chant_NOUN a_CONJ hannar_PART fydd_VERB hi_PRON 'n_PART boeth_ADJ yno_ADV "_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
Ond_CONJ wrth_ADP gwrs_NOUN yn_ADP y_DET pafiliwn_NOUN <_SYM aneglur_ADJ 2_NUM >_SYM ond_CONJ mi_PART fydd_VERB gwres_NOUN 'di_PART cael_VERB ei_PRON reoli_VERB ._PUNCT
Mae_VERB 'na_ADV fanteision_VERB mawr_ADJ fel_ADP proffesiynoldeb_NOUN y_DET peth_NOUN ond_CONJ yr_DET anfantais_NOUN ydy_VERB bebe_NOUN ydach_AUX chi_PRON 'n_PART gorfod_ADV wneud_VERB na_PART chewch_AUX chi_PRON ddim_PART yn_PART cael_ADV mynd_VERB i_PART wrando_VERB ar_ADP y_DET gystadleueth_NOUN
O_ADP ie_INTJ ._PUNCT
Ie_INTJ 'n_PART hollol_ADV [_PUNCT =_SYM ]_PUNCT Oes_VERB oes_NOUN [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Na_INTJ mae_AUX 'n_PART biti_NOUN yn'd_VERB ydy_VERB ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Na_VERB na_INTJ [_PUNCT /=_PROPN ]_PUNCT
Ia_ADJ mae_VERB 'na_ADV enillion_NOUN a_CONJ collion_VERB yn'd_VERB oes_NOUN ?_PUNCT
O_ADP bobl_NOUN bach_ADJ oes_NOUN ._PUNCT
Jest_ADV i_ADP chi_PRON gael_ADV gw'bod_VERB '_PUNCT da_ADJ ni_PRON 'n_PART cael_VERB '_PUNCT yn_PART recordio_VERB ._PUNCT
'_PUNCT Dach_VERB chi_PRON ar_ADP record_NOUN ._PUNCT +_SYM
'_PUNCT S_NUM dim_PART rhaid_VERB chi_PRON sôn_VERB am_ADP bwy_PRON ydach_ADP chi_PRON na_CONJ neb_PRON felly_ADV so_VERB  _SPACE [_PUNCT -_PUNCT ]_PUNCT
+_SYM Fydd_PROPN rhaid_VERB chi_PRON arwyddo_VERB rhwbath_NOUN yn_PART munud_NOUN ._PUNCT
Fydd_VERB '_PUNCT ych_NOUN llais_PROPN chi_PRON wedi_PART cael_VERB ei_PRON recordio_VERB yn_PART fan_NOUN 'na_ADV rwan_PRON ._PUNCT
'_PUNCT Dw_AUX i_PRON 'di_PART cael_VERB '_PUNCT y_DET nal_VERB drws_NOUN nesa_ADJ heb_ADP neb_PRON dd'eud_VERB '_PUNCT tha_PART fi_PRON fod_VERB [_PUNCT -_PUNCT ]_PUNCT
Defnydd_NOUN o_ADP 'r_DET iaith_NOUN yn_PART naturiol_ADJ ._PUNCT
So_NOUN jest_ADV defnydd_NOUN o_ADP 'r_DET iaith_NOUN yn_PART naturiol_ADJ
No_NOUN it_ADP 's_X not_NOUN me_ADP mate_NOUN it_ADP 's_X some_VERB other_ADP guy_VERB [_PUNCT saib_NOUN ]_PUNCT I_ADP just_NOUN wear_ADJ these_ADV specs_NOUN you_CONJ know_VERB ?_PUNCT
<_SYM cherthin_NOUN >_SYM
And_NOUN when_NOUN I_ADP wear_ADJ them_NOUN I_ADP speak_X English_X ._PUNCT
Dyna_DET beth_PRON arall_ADJ '_PUNCT da_ADJ ni_PRON 'di_PART ffendio_VERB ._PUNCT
Gymaint_ADV mwy_PRON o_ADP Susnag_PROPN sy_VERB 'ma_ADV ._PUNCT
O_ADP ia_PRON ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_X ia_X ia_X [_PUNCT /=_PROPN ]_PUNCT
'Di_PART o_ADP 'm_DET fath_NOUN a_CONJ bod_VERB ar_ADP faes_NOUN eisteddfod_ADV ._PUNCT
Mae_VERB o_PRON 'n_PART '_PUNCT gorad_NOUN ond_CONJ [_PUNCT -_PUNCT ]_PUNCT
[_PUNCT swn_VERB chwerthin_VERB yn_ADP y_DET cefndir_NOUN ]_PUNCT
Mae_VERB hwnna_PRON ond_CONJ oherwydd_ADP natur_NOUN y_DET maes_NOUN ei_DET hun_PRON '_PUNCT t_PRON ydy_VERB ?_PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT
Ond_CONJ y_DET bobl_NOUN ti_PRON 'm_PRON '_PUNCT bobo_VERB yn_PART gweithio_VERB yma_ADV a_CONJ ballu_VERB hefyd_ADV de_NOUN
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_X ie_X ie_INTJ [_PUNCT /=_PROPN ]_PUNCT
'_PUNCT Dw_AUX i_PRON '_PUNCT eitha_ADV '_PUNCT hoffi_VERB  _SPACE y_PART cerdded_VERB o_ADP gwmpas_NOUN sydd_VERB yma_ADV a_CONJ bod_VERB o_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Da_ADJ chi_PRON 'm_DET yn_PART mynd_VERB rownd_ADV mewn_ADP cylch_NOUN ._PUNCT
Na_INTJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_X ie_X ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
'_PUNCT Na_VERB ni_PRON ._PUNCT
Ie_INTJ
Ond_CONJ '_PUNCT dw_AUX i_ADP 'm_DET 'di_PART gweld_VERB pob_DET peth_NOUN eto_ADV ._PUNCT
'_PUNCT Dw_AUX i_PRON ddim_PART 'di_PART bod_VERB yn_ADP y_DET Ty_NOUN Gwerin_NOUN i_PART wylio_VERB dim_DET byd_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON ddim_PART 'di_PART bod_VERB yn_PART fan_NOUN 'ma_ADV ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Wel_ADV mae_VERB 'n_PART gynnar_ADJ eto_ADV yn'd_VERB ydi_VERB ?_PUNCT +_SYM
Ydi_PROPN
+_VERB '_PUNCT M_NUM ond_CONJ Dydd_NOUN Llun_PROPN ydi_ADJ ._PUNCT
'_PUNCT Dw_VERB i_PRON ar_ADP ddyletswydd_NOUN am_ADP hanna'_VERBUNCT 'di_PART un_NUM ._PUNCT
Mae_VERB Cymdeithas_NOUN Enwa_PROPN Lleoedd_PROPN ._PUNCT
Ma'_VERBUNCT gynnan_VERB ni_PRON babell_NOUN yma_ADV
O_ADP ia_PRON ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'di_PART bod_VERB fforna_ADP ._PUNCT
Do_INTJ
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT saib_NOUN ]_PUNCT ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
A_PART ma_VERB 'i_PRON 'n_PART babell_ADV lai_ADV nag_CONJ arfar_NOUN wedyn_ADV ti_PRON 'n_PART '_PUNCT go_ADV '_PUNCT oedd_VERB gynnan_VERB ni_PRON bob_DET math_NOUN o_ADP riw_NOUN arddangosfeydd_NOUN a_CONJ pop_NOUN ups_ADJ beth_DET bynnag_PRON 'di_PART 'r_DET term_NOUN
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_X ie_X ie_INTJ [_PUNCT /=_PROPN ]_PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT
Dim_DET gymaint_ADV o_ADP le_NOUN i_ADP +_SYM
[_PUNCT =_SYM ]_PUNCT Na_VERB na_INTJ [_PUNCT /=_PROPN ]_PUNCT
Na_INTJ
+_VERB wedyn_ADV y_DET [_PUNCT -_PUNCT ]_PUNCT dw_VERB i_PRON newydd_ADV fod_VERB yn_PART [_PUNCT -_PUNCT ]_PUNCT enwg_VERB cyfenw_NOUN sy_VERB 'na_ADV 'n_PART bora_VERB +_SYM
O_ADP reit_NOUN
+_VERB finna_NOUN yn_PART pnawn_NOUN a_CONJ wedyn_ADV dw_AUX i_PRON 'di_PART bod_VERB draw_ADV i_PART weld_VERB bebe_NOUN 'di_PART 'r_DET petha_NOUN sydd_VERB angan_VERB imi_CONJ wneud_VERB ar_ADP ddiwadd_NOUN y_DET dydd_NOUN ._PUNCT
Reit_INTJ ._PUNCT
A_CONJ wedyn_ADV  _SPACE geith_NOUN Rhian_PROPN fynd_VERB 'i_PRON hun_NOUN wedyn_ADV ._PUNCT
'_PUNCT Na_VERB ni_PRON ia_NOUN
Ma_INTJ '_PUNCT 'i_PRON 'di_PART mynd_VERB i_ADP nol_NOUN coffi_NOUN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT Reit_PROPN +_SYM
"_PUNCT Dos_VERB di_PRON i_PART chwilio_VERB am_ADP fwrdd_NOUN "_PUNCT medda_VERB hi_PRON
+_VERB [_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT
O_ADP dowch_VERB chi_PRON i_ADP fan_NOUN 'ma_ADV ._PUNCT
'_PUNCT Nawn_NOUN ni_PRON symud_VERB cada_VERB 'r_DET ._PUNCT
'_PUNCT Nesh_PROPN i_ADP 'm_DET meddwl_VERB ._PUNCT
Dowch_VERB atan_ADP ni_PRON i_ADP fan_NOUN 'ma_ADV ._PUNCT
Dewch_VERB i_ADP nol_NOUN cada_VERB 'r_DET arall_NOUN ._PUNCT
'_PUNCT Chos_NOUN '_PUNCT da_ADJ ni_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Sori_INTJ '_PUNCT nesh_NOUN i_ADP 'm_DET cynnig_NOUN ._PUNCT
[_PUNCT Swn_PROPN cadair_NOUN yn_PART llusgo_VERB ar_ADP lawr_ADV ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT cada_VERB 'r_DET arall_NOUN i_ADP fan_NOUN 'na_ADV rwan_PRON
[_PUNCT swn_VERB crafu_VERB ]_PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART meddwl_VERB daw_VERB Hywel_PROPN yn_PART ôl_NOUN flwyddyn_NOUN nesa_ADJ ._PUNCT
Ia_INTJ reit_INTJ ia_PRON ._PUNCT
[_PUNCT Swn_PROPN cadair_NOUN yn_PART crafu_VERB ar_ADP y_DET llawr_NOUN ]_PUNCT
Os_CONJ '_PUNCT da_ADJ ni_PRON 'n_PART cario_VERB yn_PART 'n_PART blaena'_VERBUNCT ydach_VERB chi_PRON am_PART ddod_VERB yn_PART ôl_NOUN flwyddyn_NOUN nesa_ADJ ?_PUNCT
<_SYM chwerthina_NOUN >_SYM Cwestiwn_NOUN mawr_ADJ ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'di_PART cl'wad_NOUN ni_PRON rwan_ADV yn_PART do_PRON ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'di_PART cl'wad_NOUN ni_PRON ._PUNCT
'_PUNCT Da_ADJ ni_PRON wedi_PART gwella_VERB 'n_PART do_VERB ?_PUNCT
Wel_INTJ <_SYM aneglur_ADJ 2_NUM >_SYM 'di_PART gwella_VERB [_PUNCT =_SYM ]_PUNCT do_PART do_VERB do_PRON [_PUNCT /=_PROPN ]_PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART gw'bod_VERB '_PUNCT da_ADJ ni_PRON 'm_DET 'di_PART cyrra'dd_NOUN eto_ADV ond_CONJ '_PUNCT da_ADJ ni_PRON 'di_PART gwella_VERB ._PUNCT
Ma_INTJ '_PUNCT 'na_ADV safon_NOUN oes_VERB ?_PUNCT
'_PUNCT Da_ADJ ni_PRON 'n_PART dal_ADV i_ADP drio_VERB
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT
Mae_AUX enwb_NOUN yn_PART meddwl_VERB [_PUNCT =_SYM ]_PUNCT dyna_DET dyna_DET [_PUNCT /=_PROPN ]_PUNCT ddylan_VERB ni_PRON ga'l_VERB 'n_PART [_PUNCT =_SYM ]_PUNCT galw_VERB galw_VERB [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Dal_VERB i_ADP fynd_VERB [_PUNCT -_PUNCT ]_PUNCT
Dal_VERB ati_ADP
Dal_VERB ati_ADP ._PUNCT
Dal_VERB ati_ADP 'di_PART enw_NOUN flwyddyn_NOUN nesa'_ADJUNCT ._PUNCT
Dal_VERB ati_ADP ia_PRON ?_PUNCT
Oni_AUX 'n_PART meddwl_VERB ddylsan_VERB ni_PRON newid_VERB gwisg_NOUN y_DET côr_NOUN ._PUNCT
Dylian_VERB ni_PRON ga'l_VERB tee_NOUN shirt_NOUN dal_ADJ ati_ADP
[_PUNCT chwerthin_VERB ]_PUNCT
Ia_INTJ
Ond_CONJ '_PUNCT sw_AUX ni_PRON 'n_PART newid_VERB y_PART sillafu_VERB fod_VERB o_ADP yn_PART da_ADJ lati_NOUN
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT
Da_ADJ lati_NOUN [_PUNCT chwerthin_VERB ]_PUNCT
[_PUNCT =_SYM ]_PUNCT '_PUNCT Sa_X '_PUNCT sa_PRON '_PUNCT sa_PRON [_PUNCT /=_PROPN ]_PUNCT hwnna_DET "_PUNCT '_PUNCT Dow_NOUN bebe_NOUN 'di_PART hwn_DET ?_PUNCT "_PUNCT ._PUNCT
Rhyw_DET gôr_NOUN o_ADP 'r_DET Eidal_PROPN ne_NOUN '_PUNCT rh'wbath_NOUN felly_ADV 'di_PART 'r_DET côr_NOUN ._PUNCT
Da_ADJ lati_NOUN ._PUNCT
O_ADP da_ADJ iawn_ADV ._PUNCT
Ma_INTJ '_PUNCT pobl_NOUN yn_PART d'eud_VERB ar_ADP goedd_NOUN <_SYM fr_VERB >_SYM gair_NOUN =_SYM encore_NOUN <_SYM /_PUNCT fr_VERB >_SYM on_X cor_NOUN yn'd_PART ydyn_VERB nhw_DET ?_PUNCT
A_CONJ nid_PART dyna_DET ydan_VERB ni_PRON ._PUNCT
En_PART core_VERB [_PUNCT -_PUNCT ]_PUNCT
<_SYM SS_X >_SYM Ia_PROPN
En_PART cor_ADJ ._PUNCT
Achos_CONJ '_PUNCT dw_AUX i_PRON 'n_PART galw_VERB chi_PRON yn_PART on_X cor_NOUN a_CONJ '_PUNCT dw_AUX i_PRON 'n_PART gw'bod_VERB '_PUNCT dw_VERB i_PRON ar_ADP fai_NOUN
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT mynnu_VERB fod_AUX pawb_PRON yn_PART 'i_PRON dd'eud_VERB o_ADP yn_ADP y_DET ffor_NOUN '_PUNCT Gymreig_ADJ
<_SYM SS_X >_SYM En_X cor_ADJ ._PUNCT
Pan_CONJ '_PUNCT dw_AUX i_PRON 'n_PART siarad_VERB efo_ADP enwb_NOUN [_PUNCT =_SYM ]_PUNCT '_PUNCT dw_AUX i_PRON 'n_PART '_PUNCT dw_VERB i_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT d'eud_VERB bobo_VERB "_PUNCT o_ADP '_PUNCT dw_AUX i_PRON 'n_PART canu_VERB efo_ADP en_PRON cor_NOUN "_PUNCT yn_PART do_PART "_PUNCT o_ADP hen_ADJ cor_NOUN "_PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
O_ADP ia_PRON dyna_DET bebe_NOUN oedd_VERB peth_NOUN ohono_ADP fo_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT yn_PART de_NOUN ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Mae_VERB o_PRON 'n_PART enw_NOUN mae_VERB o_PRON 'n_PART enw_NOUN [_PUNCT /=_PROPN ]_PUNCT clyfar_ADJ felly_ADV ._PUNCT
Llawar_ADJ o_ADP waith_NOUN 'di_PART mynd_VERB idda_ADP fo_PRON ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT i_ADP 'r_DET Senedd_NOUN ._PUNCT
Mae_VERB hynny_PRON 'n_PART '_PUNCT wbath_NOUN ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'di_PART bod_VERB +_SYM
Wel_INTJ '_PUNCT da_ADJ chi_PRON 'n_PART argymall_ADJ ?_PUNCT
+_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT wedi_PART bod_VERB sawl_ADJ tro_NOUN o_ADP 'r_DET blaen_NOUN ond_CONJ mae_VERB o_PRON 'n_PART '_PUNCT gored_NOUN heddiw_ADV ._PUNCT
Ac_CONJ mi_PART ddoth_VERB Elin_PROPN Jones_PROPN o_ADP gwmpas_NOUN ._PUNCT
Ma'_VERBUNCT [_PUNCT aneglur_ADJ ]_PUNCT 'di_PART gweld_VERB hi_PRON tair_NUM gwaith_NOUN ._PUNCT
Tair_NOUN gwaith_NOUN ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Do_INTJ ._PUNCT
Da_ADJ iawn_ADV ._PUNCT
Mae_VERB o_PRON 'n_PART gyfle_NOUN da_ADJ i_ADP PR_NUM yn'd_VERB ydi_VERB efo_ADP petha'_NOUNUNCT fel_ADP hyn_PRON ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Yn_PART 'di_PART yn_PART 'di_PART [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Ydi_NOUN ._PUNCT
Unwaith_ADV eto_ADV mae_VERB hwnna_PRON 'n_PART wedd_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Yn_PART 'di_PART yn_PART 'di_PART yn_PART 'di_PART [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Ie_INTJ ._PUNCT
Alla'_VERBUNCT ni_PRON ymdopi_VERB efo_ADP fo_PRON ella_ADV '_PUNCT unwaith_ADV bob_DET rhyw_NOUN ddeng_NUM mlynadd_ADJ falla_VERB ?_PUNCT
Ond_CONJ '_PUNCT swn_NOUN i_ADP 'm_DET isio_VERB '_PUNCT fo_PRON ddod_VERB i_ADP fan_NOUN 'ma_ADV bob_DET tro_NOUN mae_AUX 'n_PART dod_VERB i_ADP 'r_DET de_NOUN de_NOUN ._PUNCT
Na_VERB o_ADP na_PART ._PUNCT
'_PUNCT Sa_NOUN raid_VERB iddan_VERB nhw_PRON ddyfeisio_VERB rhyw_DET system_NOUN lle_NOUN bod_AUX y_DET siroedd_NOUN fath_NOUN â_ADP Llanelwedd_PROPN a_CONJ bod_AUX y_DET siroedd_NOUN gwahanol_ADJ yn_PART noddi_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_X ia_X [_PUNCT /=_PROPN ]_PUNCT basa'_VERBUNCT rhaid_VERB ._PUNCT
'_PUNCT Dw_AUX i_PRON 'n_PART meddwl_ADV fydda'_VERBUNCT nhw_PRON yn_PART mynd_VERB i_PART benderfynu_VERB fod_VERB o_PRON 'di_PART bod_VERB yn_PART llwyddiannus_ADJ '_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB '_PUNCT cos_NOUN ti_PRON 'n_PART gw'bod_VERB 'di_PART 'm_DET ond_CONJ yn_ADP Ddy_PROPN '_PUNCT Llun_NOUN '_PUNCT ŵan_NOUN a_CONJ mama_NOUN petha_NOUN 'n_PART [_PUNCT -_PUNCT ]_PUNCT
Oedd_AUX rhywun_NOUN yn_PART d'eud_VERB petai_VERB 'di_PART bwrw_VERB glaw_NOUN +_SYM
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
O_ADP sa_VERB 'm_DET lle_NOUN ._PUNCT
+_VERB dros_ADP penwythnos_NOUN '_PUNCT mama_NOUN sa_ADP 'r_DET lle_NOUN 'n_PART wag_ADJ achos_ADP fasa_VERB pobl_NOUN ddim_PART yn_PART dod_VERB lawr_ADV yma_ADV i_PART grwydro_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Na_INTJ ._PUNCT
O_ADP '_PUNCT drycha_ADJ ._PUNCT
Mae_VERB o_ADP 'r_DET orsedd_NOUN yn_PART dod_VERB rŵan_ADV ._PUNCT
Dod_VERB yn_PART ôl_NOUN ._PUNCT
Ti_PRON 'n_PART sôn_VERB am_ADP  _SPACE ymwelwyr_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
BeBe_ADV oedd_VERB ymlaen_ADV bore_NOUN 'ma_ADV dwa_NOUN ?_PUNCT
O_ADP cylch_NOUN yr_DET orsedd_NOUN bore_NOUN 'ma_ADV de_NOUN ._PUNCT
'_PUNCT Dw_AUX i_PRON jest_ADV yn_PART gweld_VERB y_DET faner_NOUN ._PUNCT
Ia_VERB ti_PRON 'n_PART iawn_ADJ '_PUNCT cos_NOUN oeddan_ADP nhw_PRON 'n_PART ddeg_NOUN o_ADP gloch_NOUN yn_PART cychwyn_VERB o_ADP fan_NOUN 'na_ADV  _SPACE mwyn_NOUN bod_VERB yn_ADP y_DET [_NOUN aneglur_ADJ ]_PUNCT erbyn_ADP un_NUM ar_ADP ddeg_NOUN ._PUNCT
<_SYM aneglur_ADJ 4_NUM >_SYM basio_VERB ni_PRON rwan_ADV '_PUNCT li_NOUN ._PUNCT
A_CONJ wedyn_ADV [_PUNCT -_PUNCT ]_PUNCT
[_PUNCT Sŵn_NOUN gwynt_NOUN neu_CONJ darfu_VERB ar_ADP y_DET meicroffon_NOUN ]_PUNCT ._PUNCT
O'dd_NOUN plant_NOUN enwg_VERB '_PUNCT lly_X ma_VERB nhw_PRON 'n_PART saith_NUM chwech_NUM a_CONJ pedair_NUM a_CONJ hannar_ADJ ac_CONJ o_ADP weld_VERB yr_DET orsedd_NOUN sut_ADV '_PUNCT swn_AUX i_PRON 'n_PART egluro_VERB yr_DET orsedd_NOUN [_PUNCT -_PUNCT ]_PUNCT
O_ADP ie_INTJ ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
I_ADP riwin_NOUN de_NOUN ?_PUNCT
O_ADP '_PUNCT dw_VERB i_PRON 'n_PART falch_ADJ '_PUNCT nesh_NOUN i_PART weld_VERB nhw_PRON yn_PART mynd_VERB heibio_ADV rŵan_ADV ._PUNCT
Da_ADJ iawn_ADV ._PUNCT
Ma_INTJ '_PUNCT hwn_PRON [_PUNCT -_PUNCT ]_PUNCT hynny_PRON yw_VERB [_PUNCT -_PUNCT ]_PUNCT
'_PUNCT Cos_NOUN mynd_VERB yn_PART ôl_NOUN ma_VERB 'r_DET gwisgoedd_NOUN ._PUNCT
Ma_VERB 'r_DET gwisgoedd_NOUN yn_PART [_PUNCT -_PUNCT ]_PUNCT oedd_AUX enwg_VERB yn_PART d'eud_VERB o'dd_NOUN y_DET gwisgoedd_NOUN yn_ADP y_DET lle_NOUN Dr_X Who_PROPN ti_PRON 'n_PART gweld_VERB a_CONJ wedyn_ADV dyna_DET lle_NOUN oeddan_AUX nhw_PRON 'n_PART cael_VERB 'u_PRON gwisgoedd_NOUN
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT O_ADP reit_NOUN tu_NOUN ôl_NOUN imi_NOUN wan_ADJ ._PUNCT
Ia_INTJ ._PUNCT
O_ADP wela'_VERBUNCT i_PRON ._PUNCT
'_PUNCT Lly_NOUN 'di_PART enwg_VERB '_PUNCT im_ADJ ymama_NOUN li_NOUN ._PUNCT
Mae_AUX o_PRON 'di_PART goro_ADV mynd_VERB adra_ADV ._PUNCT
Ma_VERB 'i_PRON dad_NOUN o_ADP reit_NOUN wael_ADJ ._PUNCT
O_ADP yndi_VERB ?_PUNCT
So_PART mae_AUX o_PRON 'n_PART dod_VERB yn_PART ôl_NOUN heno_ADV  _SPACE mwyn_NOUN canu_VERB '_PUNCT fory_ADV ._PUNCT
O_ADP taw_ADP ._PUNCT
Ia_ADJ wrth_ADP gwrs_NOUN ._PUNCT
'_PUNCT Da_ADJ ni_PRON really_ADV '_PUNCT im_NOUN yn_PART gwbo_NOUN ydan_AUX ni_PRON 'n_PART mynd_VERB '_PUNCT nôl_ADV adra_ADV ta_CONJ bebe_NOUN [_PUNCT -_PUNCT ]_PUNCT jest_ADV yr_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ma_INTJ '_PUNCT hwnna_PRON 'n_PART edrach_ADJ fath_NOUN â_ADP [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
'_PUNCT Dw_VERB i_PRON yn_ADP y_DET garafan_NOUN ._PUNCT
'_PUNCT Dw_VERB i_PRON [_PUNCT -_PUNCT ]_PUNCT a_CONJ '_PUNCT dw_AUX i_PRON 'di_PART bwcio_VERB hwnna_PRON ers_ADP dwy_NUM flynadd_VERB yn_ADP Gerddi_NOUN Sofia_PROPN ._PUNCT
'_PUNCT S_NUM ddaethon_VERB ni_PRON lawr_ADV Dydd_PROPN Iau_X Dydd_NOUN Gwena_X [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ia_INTJ '_PUNCT dyn_NOUN gathon_VERB ni_PRON alwad_NOUN Dy_DET '_PUNCT Gwenar_PROPN yn_PART d'eud_VERB bod_VERB o_ADP 'm_DET yn_PART sbesh_ADJ a_CONJ ma_VERB 'i_PRON jest_NOUN yn_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Mae_VERB o_PRON ar_ADP dialysis_NOUN dwy_NUM waith_NOUN yr_DET w'sos_NOUN a_CONJ mae_AUX 'r_DET kidneys_NOUN wedi_PART gorffan_NOUN ._PUNCT
A_PART mae_VERB o_PRON 'n_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Dyn_NOUN mama_NOUN enwg_VERB druan_NOUN 'di_PART mynd_VERB adra_ADV ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART d'eud_VERB "_PUNCT Dos_ADJ na_CONJ bobo_VERB chdi_NOUN ar_ADP ffôn_NOUN i_ADP '_PUNCT Sbyty_NOUN Gwynedd_PROPN a_CONJ [_PUNCT -_PUNCT ]_PUNCT
O_ADP yn_ADP y_DET giarafan_NOUN ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Wedyn_ADV  _SPACE [_PUNCT -_PUNCT ]_PUNCT
Lle_ADV '_PUNCT da_ADJ chi_PRON 'ch_PRON dau_NUM 'n_PART aros_VERB te_NOUN ?_PUNCT
Drws_NOUN nesa'_ADJUNCT iddyn_ADP nhw_PRON ._PUNCT
'_PUNCT R_NUM un_NUM lle_NOUN ._PUNCT
Gerddi_VERB Sofia_PROPN ia_ADV ._PUNCT
Felly_CONJ gini_NOUN '_PUNCT igon_NOUN o_ADP minders_NOUN ._PUNCT
Dyna_ADV '_PUNCT swn_NOUN i_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_CONJ mama_NOUN gin_X i_ADP enwb_NOUN a_PART enwg_VERB yr_DET ochr_NOUN arall_ADJ ag_ADP __NOUN a_CONJ __NOUN __NOUN __NOUN hefo_ADP ni_PRON ._PUNCT
Ie_INTJ ._PUNCT
'_PUNCT Dw_VERB i_PRON 'n_PART iawn_ADJ ._PUNCT
Ond_CONJ mae_VERB o_PRON 'n_PART braf_ADJ iawn_ADV ._PUNCT
Mae_VERB o_PRON 'n_PART  _SPACE [_PUNCT -_PUNCT ]_PUNCT
'_PUNCT Da_ADJ ni_PRON 'di_PART ca'l_VERB rhyw_DET gornel_NOUN bach_ADJ reit_ADV dda_ADJ ._PUNCT
Do_INTJ ._PUNCT
A_PART ddaethon_VERB ni_PRON mewn_ADP i_ADP 'r_DET '_PUNCT steddfod_NOUN mewn_ADP ffordd_NOUN '_PUNCT dw_AUX i_ADP erioed_ADV wedi_ADP cyrra'dd_NOUN '_PUNCT steddfod_NOUN o_ADP blaen_NOUN sef_CONJ mewn_ADP cwch_NOUN ._PUNCT
A_PART mae_VERB 'r_DET tywydd_NOUN dydi_VERB [_PUNCT -_PUNCT ]_PUNCT
O_ADP ._PUNCT Mewn_ADP tacsi_NOUN ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Ie_INTJ ie_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Pedair_VERB punt_NOUN ._PUNCT
Pedair_VERB punt_NOUN ddim_PRON yn_PART ddrwg_ADJ o_ADP gwbl_NOUN ._PUNCT
A_CONJ dim_PRON ciwio_VERB ,_PUNCT oeddan_VERB ni_PRON 'n_PART cyrra'dd_NOUN fel_ADP oedd_VERB hi_PRON 'n_PART cyrra'dd_NOUN ._PUNCT
Dyna_ADV oedd_VERB ein_DET bwriad_NOUN ni_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT cwch_NOUN ola_ADP '_PUNCT hannar_ADV awr_NOUN wedi_ADP tri_NUM ._PUNCT
[_PUNCT Tarfu_NOUN ar_ADP y_DET meicroffon_NOUN ]_PUNCT ._PUNCT
O_ADP reit_NOUN ._PUNCT
<_SYM SS_X >_SYM Han_PRON '_PUNCT 'di_PART tri_NUM ?_PUNCT
Syth_ADJ fel_ADP 'na_ADV ar_ADP draws_ADJ ._PUNCT
A_CONJ '_PUNCT dyn_NOUN ma_VERB 'i_PRON 'n_PART wych_ADJ ._PUNCT
Oedd_VERB 'na_ADV rhyw_DET le_NOUN i_ADP blant_NOUN chwara'_VERBUNCT ._PUNCT
Oes_VERB ._PUNCT
Mae_VERB 'n_PART wych_ADJ ._PUNCT
Wedyn_ADV oedd_VERB hi_PRON 'n_PART bump_ADJ arnan_VERB ni_PRON 'n_PART cyrra'dd_NOUN fan_NOUN '_PUNCT yn_ADP Dydd_NOUN Sadwrn_PROPN ._PUNCT
'_PUNCT Cos_NOUN ma_ADV enwg_VERB a_CONJ fi_PRON yma_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART deu_VERB '_PUNCT '_PUNCT tha_PART chi_PRON yn'd_VERB o_PRON 'n_PART ?_PUNCT
Oeddan_VERB ni_PRON yma_ADV blwyddyn_NOUN yn_PART ôl_NOUN canwr_NOUN y_DET byd_NOUN gymro_VERB ni_PRON 'r_DET cwch_NOUN i_ADP Penarth_PROPN gan_CONJ feddwl_VERB awn_VERB i_ADP Penarth_PROPN o_ADP na_DET mama_NOUN isio_VERB cerddad_NOUN +_SYM
Oes_VERB mama_NOUN isio_ADV ._PUNCT
Ac_CONJ mi_PART wnaethon_VERB ni_PRON o_ADP ag_ADP i_ADP lawr_ADV a_CONJ wedyn_ADV 'r_DET un_NUM fath_NOUN ond_CONJ y_DET syniad_NOUN o'dd_NOUN bobo_VERB ni_PRON yn_PART cer'ad_SYM yn_PART ôl_NOUN ond_CONJ o'dda_AUX ni_PRON 'n_PART d'eud_VERB o_ADP '_PUNCT da_ADJ ni_PRON 'di_PART cer'ad_SYM digon_ADV i_ADP fyny_ADV ag_CONJ i_ADP lawr_ADV ._PUNCT
Natho_VERB ni_PRON gymryd_VERB y_DET gwch_NOUN yn_PART ôl_NOUN ._PUNCT
Wrth_ADP gwrs_NOUN efo_ADP 'r_DET plant_NOUN [_PUNCT -_PUNCT ]_PUNCT oedd_VERB y_DET plant_NOUN efo_ADP scooters_NOUN a_CONJ phetha_VERB [_PUNCT -_PUNCT ]_PUNCT
Ia_INTJ reit_VERB ia_X ia_PRON ._PUNCT
On_CONJ '_PUNCT cyn_ADP fi_PRON gyrra'dd_NOUN fan_NOUN '_PUNCT yn_PART [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT scooter_NOUN bob_DET un_NUM '_PUNCT lly_X [_PUNCT saib_NOUN ]_PUNCT '_PUNCT Dwn_PROPN i_ADP 'm_DET lle_NOUN mama_NOUN Rhian_X 'di_PART mynd_VERB ._PUNCT
Ma_AUX 'i_PRON 'di_PART gweld_VERB rhywun_NOUN siŵr_ADJ o_ADP fod_VERB ._PUNCT
[_PUNCT Tarfu_NOUN ar_ADP y_DET meicroffon_NOUN ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Prynu_VERB coffi_NOUN o'dd_ADP hi_PRON ?_PUNCT
Yn_PART fan_NOUN 'ma_ADV ia_PRON ?_PUNCT
'Di_PART hi_PRON 'n_PART gwbobo_VERB lle_NOUN '_PUNCT da_ADJ chi_PRON 'n_PART '_PUNCT isda_NOUN ?_PUNCT
"_PUNCT Dos_VERB di_PRON i_PART chwilio_VERB am_ADP set_NOUN "_PUNCT medda'_VERBUNCT hi_PRON ond_CONJ gesh_NOUN i_ADP fwy_PRON na_CONJ set_NOUN ._PUNCT
Gesh_VERB i_ADP gwmni_NOUN yn_PART do_VERB ?_PUNCT
O_ADP reit_NOUN ._PUNCT
Bron_ADV iawn_ADV ag_CONJ anghofio_VERB ._PUNCT
941.291_NOUN
