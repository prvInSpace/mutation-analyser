2_NUM Tachwedd_NOUN 2017_NUM
Banc_PROPN Datblygu_VERB Cymru_PROPN wedi_PART 'w_PRON lansio_VERB
Dyma_DET 'r_DET cyntaf_ADJ o_ADP 'i_PRON fath_NOUN yn_ADP y_DET DU_PROPN ac_CONJ mae_AUX wedi_PART cael_VERB ei_PRON sefydlu_VERB i_ADP 'w_PRON gwneud_VERB hi_PRON 'n_PART haws_ADJ i_ADP fusnesau_NOUN yng_ADP Nghymru_PROPN gael_ADV gafael_VERB ar_ADP y_DET cyllid_NOUN sydd_VERB ei_DET angen_NOUN arnynt_ADP i_PART lwyddo_VERB ._PUNCT
Gwyliwch_VERB y_DET fideo_NOUN hwn_PRON i_ADP ddysgu_VERB mwy_ADV ._PUNCT
Canfod_VERB mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Ydych_AUX chi_PRON 'n_PART storio_VERB gwybodaeth_NOUN bersonol_ADJ pobl_NOUN neu_CONJ 'n_PART marchnata_VERB i_PART ddarpar_VERB gwsmeriaid_NOUN ?_PUNCT
Mae_AUX 'r_DET Rheoliad_NOUN Diogelu_VERB Data_NOUN Cyffredinol_ADJ (_PUNCT GDPR_PROPN )_PUNCT yn_PART effeithio_VERB arnoch_ADP chi_PRON ._PUNCT
Ymunwch_VERB â_ADP Cyflymu_PROPN Cymru_PROPN i_ADP Fusnesau_PROPN a_CONJ Swyddfa_NOUN 'r_DET Comisiynydd_NOUN Gwybodaeth_PROPN mewn_ADP briff_VERB brecwast_NOUN cyfeillgar_ADJ i_PART wahaniaethu_VERB rhwng_ADP ffeithiau_NOUN a_CONJ ffuglen_NOUN ,_PUNCT a_CONJ gwneud_VERB yn_PART siŵr_ADJ bod_VERB eich_DET busnes_NOUN yn_PART barod_ADJ cyn_ADP mis_NOUN Mai_PROPN 2018_NUM ._PUNCT
Cofrestrwch_VERB yn_PART rhad_ADJ ac_CONJ am_ADP ddim_PRON nawr_ADV ._PUNCT
Canfod_VERB mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Briff_VERB Innovate_PROPN UK_SYM :_PUNCT cyflymu_VERB 'r_DET newid_NOUN i_ADP gerbydau_NOUN heb_ADP allyriadau_NOUN
Mae_AUX Llywodraeth_PROPN Cymru_PROPN yn_PART cefnogi_VERB Innovate_PROPN UK_PROPN a_CONJ 'r_DET Knowledge_NOUN Transfer_X Network_PROPN i_ADP annog_NOUN ceisiadau_NOUN ar_ADP gyfer_NOUN cymorth_NOUN ariannol_ADJ ar_ADP gyfer_NOUN YaD_PUNCT ar_ADP dechnoleg_NOUN cerbydau_NOUN heb_ADP allyriadau_NOUN ._PUNCT
Mae_VERB 'r_DET
Swyddfa_NOUN ar_ADP gyfer_NOUN Cerbydau_NOUN Allyriadau_NOUN Isel_PROPN (_PUNCT OLEV_X )_PUNCT
ac_CONJ
Innovate_NOUN UK_SYM
,_PUNCT trwy_ADP y_DET
Faraday_NOUN Industrial_X Strategy_X Challenge_X Fund_PROPN
,_PUNCT yn_PART mynd_VERB i_PART fuddsoddi_VERB hyd_NOUN at_ADP £_SYM 20miliwn_NUM yn_ADP y_DET gystadleuaeth_NOUN yma_ADV ._PUNCT
Am_ADP fwy_PRON o_ADP wybodaeth_NOUN ac_CONJ i_PART gadw_VERB eich_DET lle_NOUN
cliciwch_VERB yma_ADV
._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Taith_NOUN Bws_NOUN Sadwrn_PROPN y_DET Busnesau_NOUN Bach_ADJ
Bydd_AUX Bws_NOUN Sadwrn_PROPN y_DET Busnesau_NOUN Bach_ADJ yn_PART ymweld_VERB â_ADP 29_NUM o_ADP drefi_NOUN a_CONJ dinasoedd_NOUN gwahanol_ADJ rhwng_ADP 23_NUM Hydref_X a_CONJ 24_NUM Tachwedd_NOUN 2017_NUM ,_PUNCT ac_CONJ maent_AUX yn_PART edrych_VERB y_PART recriwtio_VERB mentoriaid_NOUN busnes_NOUN profiadol_ADJ ._PUNCT
Canfod_VERB mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
Enwebiadau_PROPN Gwobrau_PROPN Trydydd_X Sector_PROPN Cymru_PROPN 2017_NUM ar_ADP agor_VERB
Ydych_AUX chi_PRON 'n_PART gwybod_VERB am_ADP elusen_NOUN ,_PUNCT mudiad_NOUN gwirfoddol_ADJ neu_CONJ fenter_NOUN gymdeithasol_ADJ sy_AUX 'n_PART haeddu_VERB cydnabyddiaeth_NOUN am_ADP eu_DET cyflawniadau_NOUN ?_PUNCT
Mae_VERB gennych_VERB tan_ADP y_DET 10_NUM Tachwedd_NOUN i_ADP 'w_PRON henwebu_VERB nhw_PRON ._PUNCT
Canfod_VERB mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Cynghorion_X Allforio_VERB :_PUNCT Gweithdai_VERB goblygiadau_NOUN o_ADP ran_NOUN TAW_X
Hoffech_VERB chi_PRON ddeall_VERB TAW_X a_CONJ goblygiadau_NOUN i_ADP gwmnïau_VERB mewn_ADP perthynas_NOUN ag_CONJ allforio_VERB ?_PUNCT
Mynychwch_VERB weithdy_NOUN brecwast_NOUN rhyngweithiol_ADJ i_PART gael_VERB trosolwg_NOUN cyffredinol_ADJ ynghylch_ADP goblygiadau_NOUN TAW_X ,_PUNCT a_CONJ chyngor_NOUN ar_ADP ble_ADV i_PART gael_VERB rhagor_PRON o_ADP wybodaeth_NOUN ._PUNCT
Canfod_VERB mwy_ADJ ._PUNCT ._PUNCT ._PUNCT
