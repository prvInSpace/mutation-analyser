Roy_DET Noble_PROPN yn_PART dychwelyd_VERB i_ADP fro_VERB ei_DET febyd_NOUN
Taith_NOUN i_ADP gwm_NOUN sy_VERB 'n_PART agos_ADJ at_ADP ei_DET galon_NOUN sydd_VERB o_ADP dan_ADP sylw_NOUN 'r_DET darlledwr_NOUN Roy_PROPN Noble_PROPN nos_NOUN Fercher_NOUN ,_PUNCT 1_NUM Mawrth_PROPN ar_ADP S4C_PROPN ,_PUNCT yn_PART rhifyn_NOUN diweddaraf_ADJ y_DET gyfres_NOUN Cymoedd_PROPN Roy_PROPN Noble_PROPN ._PUNCT
Yn_ADP y_DET rhaglen_NOUN ar_ADP Ddydd_NOUN Gŵyl_NOUN Dewi_PROPN ,_PUNCT mae_AUX Roy_PROPN yn_PART dychwelyd_VERB i_ADP fro_VERB ei_DET febyd_NOUN yn_ADP Nyffryn_PROPN Aman_PROPN ac_CONJ i_ADP bentref_NOUN Brynaman_PROPN lle_ADV cafodd_VERB ei_PRON fagu_VERB ._PUNCT
"_PUNCT Mae_VERB 'r_DET cwm_NOUN yn_PART llawn_ADJ atgofion_VERB melys_ADJ ,_PUNCT "_PUNCT meddai_VERB Roy_PROPN Noble_PROPN sydd_AUX wedi_PART bod_AUX yn_PART byw_VERB ger_ADP Aberdâr_VERB ers_ADP dros_ADP 30_NUM mlynedd_NOUN ._PUNCT
"_PUNCT Brynaman_PROPN oedd_VERB fy_DET nghartref_NOUN a_CONJ dyma_DET 'r_DET lle_NOUN oedd_VERB yn_PART gyfrifol_ADJ am_ADP blentyndod_NOUN hyfryd_ADJ a_CONJ hapus_ADJ tu_NOUN hwnt_ADV i_ADP mi_PRON ._PUNCT
"_PUNCT Fi_PRON 'n_PART cofio_VERB chwarae_VERB Ben_PROPN Hur_PROPN ar_ADP iard_NOUN yr_DET ysgol_NOUN fabanod_NOUN ._PUNCT
Fi_PRON oedd_AUX yn_PART gyrru_VERB a_CONJ dau_NUM foi_NOUN o_ADP 'm_DET '_PUNCT mhlân_NOUN i_ADP oedd_VERB y_DET ceffyl_NOUN ._PUNCT
Roedden_AUX ni_PRON 'n_PART mynd_VERB rownd_ADV a_CONJ rownd_ADV ac_CONJ roedd_AUX sparks_NOUN yn_PART mynd_VERB i_ADP bobman_NOUN ,_PUNCT achos_CONJ roedd_VERB hoelion_NOUN yn_ADP y_DET sgidiau_NOUN bryd_NOUN '_PUNCT ny_PRON !_PUNCT "_PUNCT
Ond_CONJ nid_PART atgofion_VERB hapus_ADJ yn_PART unig_ADJ sydd_VERB gan_ADP Roy_PROPN o_ADP 'i_PRON blentyndod_NOUN ac_CONJ ar_ADP ei_DET daith_NOUN i_ADP lawr_ADV y_DET cwm_NOUN ,_PUNCT mae_AUX Roy_PROPN yn_PART anelu_VERB am_ADP bentref_NOUN Tairgwaith_PROPN lle_NOUN y_PART cafodd_VERB ei_DET dad-_NOUN cu_DET ei_DET ladd_VERB wrth_ADP weithio_VERB yn_PART un_NUM o_ADP 'r_DET tri_NUM phwll_NOUN glo_NOUN oedd_VERB o_PRON amgylch_NOUN y_DET pentref_NOUN ._PUNCT
"_PUNCT Cafodd_VERB fy_DET nhad_NOUN -_PUNCT cu_PART ei_DET ladd_VERB yn_ADP y_DET Steer_X Pit_X ,_PUNCT "_PUNCT esbonia_VERB Roy_PROPN ._PUNCT
"_PUNCT Dim_NOUN o_ADP dan_ADP ddaear_NOUN ond_CONJ ar_ADP y_DET wyneb_NOUN ._PUNCT
Cafodd_VERB ei_PRON fwrw_VERB gan_ADP dryc_NOUN ._PUNCT
Fi_PRON 'n_PART cofio_VERB 'r_DET diwrnod_NOUN ,_PUNCT o_ADP 'n_PART i_PRON adre_ADV 'n_PART dost_VERB gyda_ADP 'r_DET frech_NOUN goch_ADJ ,_PUNCT a_CONJ dyma_DET David_NOUN Jones_PROPN ,_PUNCT partner_VERB gwaith_NOUN fy_DET nhad_NOUN -_PUNCT cu_PROPN ,_PUNCT yn_PART dod_VERB i_ADP 'r_DET tŷ_NOUN yng_ADP nghanol_NOUN shifft_NOUN ._PUNCT
Roedd_VERB e_PRON 'n_PART blwmp_ADJ ac_CONJ yn_PART blaen_NOUN yn_PART dweud_VERB ,_PUNCT 'Ma_ADV '_PUNCT Francis_PROPN wedi_PART cael_VERB ei_DET ladd_VERB '_PUNCT ._PUNCT
Roedd_VERB e_DET tamaid_NOUN bach_ADJ mwy_ADV caredig_ADJ pan_CONJ aeth_VERB e_PRON lan_ADV i_PART ddweud_VERB wrth_ADP Mam_PROPN -_PUNCT gu_ADJ ._PUNCT "_PUNCT
Bydd_AUX Roy_PROPN hefyd_ADV yn_PART ymweld_VERB â_ADP Gwauncaegurwen_PROPN ac_CONJ yn_PART mynd_VERB i_ADP lawr_ADV am_ADP Lanaman_PROPN cyn_CONJ cyrraedd_VERB Rhydaman_PROPN ,_PUNCT tref_NOUN ar_ADP waelod_NOUN y_DET cwm_NOUN lle_NOUN y_PART cafodd_VERB Roy_PROPN ei_DET addysg_NOUN yn_ADP yr_DET ysgol_NOUN ramadeg_NOUN yn_ADP y_DET 1950au_NOUN ._PUNCT
Roedd_VERB e_PRON yn_ADP yr_DET un_NUM flwyddyn_NOUN ysgol_NOUN â_ADP Derec_PROPN Llwyd_PROPN Morgan_PROPN ,_PUNCT Dafydd_PROPN Iwan_PROPN a_CONJ John_PROPN Cale_PROPN o_ADP 'r_DET Velvet_X Underground_X ._PUNCT
"_PUNCT Roedd_VERB pasio_VERB 'r_DET eleven_NOUN plus_ADJ yn_PART beth_PRON mawr_ADJ ,_PUNCT "_PUNCT meddai_VERB Roy_PROPN ._PUNCT
"_PUNCT Weithie_NOUN ro'dd_NOUN eich_DET mam_NOUN yn_PART mynd_VERB â_ADP chi_PRON i_PART weld_VERB menyw_NOUN yng_ADP Ngwauncaegurwen_PROPN ._PUNCT
Roedd_VERB hi_PRON 'n_PART arbenigwr_NOUN ar_ADP sawl_ADJ peth_NOUN ond_CONJ am_ADP un_NUM peth_NOUN roedd_AUX hi_PRON 'n_PART artist_VERB -_PUNCT sef_CONJ torri_VERB 'r_DET llech_NOUN ._PUNCT
Rodd_AUX hi_PRON 'n_PART mynd_VERB â_ADP chi_PRON i_ADP mewn_ADP i_ADP 'r_DET parlwr_NOUN ac_CONJ yn_PART torri_VERB 'ch_PRON clust_VERB chi_PRON ._PUNCT
Ac_CONJ wrth_ADP i_ADP 'r_DET gwaed_NOUN gael_VERB ei_PRON ryddhau_VERB ,_PUNCT roedd_VERB eich_PRON gallu_AUX chi_PRON 'n_PART cael_VERB ei_PRON ryddhau_VERB hefyd_ADV ._PUNCT
O_ADP 'ch_PRON chi_PRON 'n_PART gallu_VERB bod_AUX yn_ADP Einstein_PROPN dros_ADP nos_NOUN ._PUNCT
Ac_CONJ os_CONJ oeddech_VERB chi_PRON 'n_PART dwp_ADV ofnadwy_ADJ ,_PUNCT ro'dd_AUX hi_PRON 'n_PART gwneud_VERB y_DET ddwy_NUM glust_NOUN i_ADP chi_PRON !_PUNCT "_PUNCT
Er_CONJ bod_VERB yr_DET hen_ADJ ddiwydiannau_NOUN trwm_ADJ wedi_PART hen_ADJ ddiflannu_VERB o_ADP 'r_DET cwm_NOUN ,_PUNCT bydd_AUX Roy_PROPN yn_PART ymweld_VERB â_ADP ffatri_NOUN lwyddiannus_ADJ sy_AUX 'n_PART creu_VERB dillad_NOUN gwlân_ADJ o_ADP 'r_DET safon_NOUN uchaf_ADJ ac_CONJ sy_AUX 'n_PART dod_VERB â_ADP llewyrch_NOUN yn_PART ôl_NOUN i_ADP 'r_DET ardal_NOUN ._PUNCT
Ac_CONJ wrth_ADP i_ADP 'w_PRON daith_NOUN drwy_ADP 'r_DET Dyffryn_NOUN ddirwyn_VERB i_ADP ben_NOUN ,_PUNCT faint_ADV o_ADP lwc_NOUN gaiff_VERB Roy_PROPN wrth_ADP osod_NOUN bet_X yn_ADP y_DET rasus_NOUN harnes_VERB yn_ADP Nhairgwaith_PROPN ?_PUNCT
Cymoedd_NOUN Roy_PROPN Noble_PROPN
Nos_NOUN Fercher_NOUN 1_NUM Mawrth_PROPN 7.30_NUM ,_PUNCT S4C_PROPN
Hefyd_ADV ,_PUNCT dydd_NOUN Iau_ADJ 2_NUM Mawrth_PROPN 3.00_NUM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Saesneg_PROPN
Ar_PART gael_VERB ar_ADP alw_VERB ar_ADP s4c.cymru_NOUN ,_PUNCT BBC_PROPN iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cynhyrchiad_NOUN Tinopolis_X ar_ADP gyfer_NOUN S4C_PROPN
