0.0_NOUN
Tôst_NOUN dw_AUX i_PRON 'di_PART rhoid_VERB menyn_NOUN ar_ADP eich_DET tylwydd_NOUN teg_ADJ chi_PRON ._PUNCT
enwbg_NOUN enwbg_NOUN ._PUNCT
Wellwch_VERB chi_PRON dod_VERB i_ADP 'r_DET cegin_NOUN ._PUNCT
Dw_AUX i_PRON meddwl_VERB bod_VERB enwbg_NOUN 'di_PART torri_VERB ._PUNCT
Mae_AUX 'n_PART cymysgu_VERB eu_DET geiriau_NOUN a_CONJ siarad_VERB tu_NOUN ôl_NOUN tu_NOUN blaen_NOUN ._PUNCT
[_PUNCT ocheneidio_VERB ]_PUNCT ._PUNCT
Pam_ADV mae_AUX 'n_PART gwneud_VERB hyny_X ?_PUNCT
Beth_PRON sy_AUX 'n_PART bod_VERB ar_ADP enwbg_NOUN cyfenw_NOUN ._PUNCT
[_PUNCT ocheneidio_VERB ]_PUNCT dw_AUX i_ADP 'm_DET yn_PART gwybod_VERB ond_CONJ '_PUNCT dy_DET ddim_PART hi_PRON hun_NOUN yn_PART de_NOUN ._PUNCT
Wel_INTJ ._PUNCT
Os_CONJ nag_CONJ yw_VERB hi_PRON hi_PRON hunan_VERB ._PUNCT
Pwy_PRON yw_VERB hi_PRON te_NOUN ?_PUNCT
Cwestiwn_NOUN dda_ADJ ._PUNCT
Well_ADJ i_ADP mi_PRON weld_VERB enwbg_NOUN '_PUNCT deud_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
183.939_NUM
Dw_AUX i_PRON 'n_PART gwybod_VERB yn_PART union_ADJ beth_PRON sy_AUX 'n_PART bod_VERB ar_ADP enwbg_NOUN ._PUNCT
Beth_PRON te_NOUN ?_PUNCT
Gweud_VERB wrtho_ADP 'n_PART ni_PRON enwg_VERB ._PUNCT
Mae_VERB 'n_PART hanner_NOUN cysgu_VERB ._PUNCT
Neu_CONJ 'n_PART hanner_NOUN effro_ADJ ._PUNCT
Mae_AUX gwyneb_NOUN hi_PRON dal_AUX yn_PART cysgu_VERB ._PUNCT
Drychwch_VERB enwbg_NOUN ._PUNCT
Siwd_VERB '_PUNCT yn_PART ni_PRON mynd_VERB i_PART thrwsio_VERB hi_PRON ?_PUNCT
Ie_INTJ ._PUNCT
Beth_PRON '_PUNCT nawn_NOUN ni_PART enwg_VERB ?_PUNCT
'_PUNCT Dan_PROPN ni_PRON 'n_PART mynd_VERB i_PART ddeffro_VERB ei_DET gwyneb_NOUN hi_PRON wrth_ADP gwrs_NOUN ._PUNCT
Dewch_INTJ ._PUNCT
Sydyn_PROPN ._PUNCT
At_ADP y_DET enwbg_NOUN ._PUNCT
Iawn_INTJ ta_CONJ ._PUNCT
Pawb_PRON yn_PART barod_ADJ i_ADP ganu_VERB ?_PUNCT
enwbg_NOUN ._PUNCT
Wyt_VERB ti_PRON 'n_PART barod_ADJ ?_PUNCT
Mmm_VERB ._PUNCT
'_PUNCT S'dim_ADV ots_NOUN ._PUNCT
Ar_ADP ôl_NOUN tri_NUM ._PUNCT
Un_NUM ._PUNCT
Dau_NUM ._PUNCT
Tri_INTJ ._PUNCT
237.391_ADJ
Wel_INTJ dydy_VERB gwyneb_NOUN enwbg_NOUN ddim_PART 'di_PART deffro_VERB 'n_PART iawn_ADJ heddiw_ADV a_PART mae_AUX rhaid_VERB i_ADP ni_PRON ddeffro_VERB fo_PRON dw_AUX i_PRON 'n_PART meddwl_VERB ._PUNCT
Ah_INTJ mae_AUX 'n_PART cymysgu_VERB geiriau_NOUN fel_CONJ dw_VERB 'i_PRON 'm_DET be_PRON ._PUNCT
Mi_PART fasa_VERB 'n_PART ofnadwy_ADJ tasa_VERB enwbg_NOUN 'n_PART methu_ADV odli_VERB yn_PART bysa_VERB ?_PUNCT
279.864_NOUN
[_PUNCT ocheneidio_VERB ]_PUNCT chi_PRON 'ch_PRON dau_NUM mae_VERB hyny_X 'n_PART anhygoel_ADJ ._PUNCT
Mi_PART nawn_NOUN ni_PART cyfansoddi_VERB cwlwm_NOUN tafod_NOUN iddi_ADP ._PUNCT
Mi_PART ddylia_VERB hyna_ADV wneud_VERB ei_DET gwyneb_NOUN hi_PRON symud_VERB a_CONJ deffro_VERB 'n_PART iawn_ADJ ._PUNCT
Beth_PRON yw_VERB cwlwm_NOUN tafod_NOUN ?_PUNCT
Llinell_PROPN sy_VERB 'n_PART anodd_ADJ i_PART dweud_VERB ._PUNCT
Ie_INTJ ._PUNCT
Llinell_PROPN sy_VERB 'n_PART mor_ADV anodd_ADJ i_ADP 'w_PRON ddweud_VERB os_CONJ wyt_AUX ti_PRON 'n_PART trio_VERB ei_DET dweud_VERB hi_PRON mae_VERB dy_DET wyneb_NOUN di_PRON yn_PART mynd_VERB fel_ADP hyn_PRON ._PUNCT
Fel_ADP hyn_PRON ._PUNCT
A_CONJ fel_ADP hyn_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ie_INTJ ._PUNCT
Iawn_ADV te_NOUN ._PUNCT
'_PUNCT Sgen_NOUN unrhywun_NOUN cwlwm_NOUN tafod_NOUN ?_PUNCT
328.517_NOUN
Mae_VERB 'n_PART wyneb_NOUN i_ADP 'di_PART deffro_VERB 'n_PART iawn_ADJ eto_ADV ._PUNCT
Am_ADP funud_NOUN o_ADP 'n_PART i_PRON 'n_PART meddwl_VERB na_PART faswn_VERB i_ADP byth_ADV yn_PART odli_VERB eto_ADV ._PUNCT
Diolch_INTJ ._PUNCT
Diolch_VERB bawb_PRON ._PUNCT
Ôô_NOUN mae_VERB 'n_PART iawn_ADJ ._PUNCT
'_PUNCT Sa_NOUN ni_PRON 'm_PART yn_PART gallu_VERB sefyll_VERB yn_PART fyma_VERB yn_PART edrych_VERB ar_ADP ein_DET ffrind_NOUN gorau_ADJ ni_PART 'n_PART cerdded_VERB o_ADP gwmpas_NOUN hefo_ADP gwyneb_NOUN sy_AUX 'n_PART dal_ADV i_ADP gysgu_VERB nafswm_NOUN ?_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Iawn_INTJ ._PUNCT
Sefwch_VERB 'n_PART ôl_NOUN ._PUNCT
Mae_VERB gen_ADP i_ADP glymau_NOUN tafod_NOUN i_PART guro_VERB pob_DET cwlwm_NOUN tafod_NOUN arall_ADJ ._PUNCT
Fydd_VERB na_PART 'm_DET un_NUM gwyneb_NOUN yn_PART cysgu_VERB eto_ADV enwg_VERB ._PUNCT
Oes_VERB gen_ADP ti_PRON a_CONJ enwbg_NOUN cerddoriaeth_NOUN sy_AUX 'n_PART troi_VERB a_CONJ throsi_VERB i_ADP ni_PRON ?_PUNCT
Ww_VERB dw_VERB i_PRON siŵr_ADJ fedrwn_VERB ni_PRON dod_VERB o_ADP hyd_NOUN i_ADP rhywbeth_NOUN enwbg_NOUN ._PUNCT
436.94_NOUN
[_PUNCT chwerthin_VERB ]_PUNCT Ydach_X chi_PRON gyd_ADP yn_PART barod_ADJ ?_PUNCT
Ydyn_VERB ._PUNCT
Mae_AUX 'n_PART amser_NOUN i_ADP ni_PRON troi_VERB 'n_PART fach_ADJ fach_ADJ a_CONJ mynd_VERB i_ADP fyd_NOUN o_ADP wenu_VERB ._PUNCT
Dewch_VERB i_ADP wlad_NOUN y_DET tylwydd_NOUN teg_ADJ i_ADP ddawnsio_VERB ac_CONJ i_ADP ganu_VERB ._PUNCT
481.752_CONJ
Ww_ADP enwbg_NOUN oedd_VERB y_DET gan_NOUN yna_DET yn_PART droellog_ADJ yn_PART glymog_ADJ ond_CONJ mor_ADV ardderchog_ADJ ._PUNCT
Ww_ADP enwbg_NOUN ._PUNCT
''_VERB Sa_PROPN i_PRON 'n_PART credu_VERB bydd_VERB fy_DET wyneb_NOUN byth_ADJ yr_DET un_NUM peth_NOUN eto_ADV ._PUNCT
Bydden_VERB ni_PRON byth_ADV yn_PART mynd_VERB i_ADP cysgu_VERB to_NOUN ._PUNCT
A_PART dw_AUX i_PRON 'n_PART meddwl_VERB bydd_VERB y_DET plant_NOUN [_PUNCT =_SYM ]_PUNCT bach_ADJ [_PUNCT /=_PROPN ]_PUNCT bach_ADJ yn_ADP y_DET byd_NOUN [_PUNCT =_SYM ]_PUNCT mawr_ADJ [_PUNCT /=_PROPN ]_PUNCT mawr_ADJ yn_PART cael_VERB hwyl_NOUN go_ADV iawn_ADJ wrth_ADP ceisio_VERB canu_VERB y_DET cân_NOUN yna_DET ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT Ella_PROPN ._PUNCT
Dylen_VERB nhw_PRON ei_PRON chanu_VERB hi_PRON yn_ADP y_DET drych_NOUN iddyn_ADP nhw_PRON gael_ADV gweld_VERB siapiau_NOUN mae_AUX gwynebau_AUX nhw_PRON 'n_PART neud_VERB ._PUNCT
<_SYM SS_X >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Ôô_NOUN dw_VERB i_PRON mor_ADV falch_ADJ fod_VERB pethau_NOUN fel_ADP oedden_ADP nhw_PRON ._PUNCT
Ond_CONJ mae_VERB gen_ADP i_PRON ofn_NOUN fod_VERB hi_PRON 'n_PART amser_NOUN i_ADP mi_PRON a_CONJ 'm_DET wyneb_NOUN hollol_ADV deffro_ADJ fynd_VERB rŵan_ADV ._PUNCT
Ôô_NOUN mor_ADV fuan_ADJ ?_PUNCT
Hmm_INTJ ond_CONJ mi_PART fyddai_VERB n'ôl_SYM cyn_ADP hir_ADJ ._PUNCT
620.79_NOUN
732.949_VERB
