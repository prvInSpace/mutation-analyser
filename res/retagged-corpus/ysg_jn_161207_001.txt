Ffilm_NOUN Nadolig_PROPN hudolus_ADJ :_PUNCT Albi_PROPN a_CONJ Noa_NOUN yn_PART Achub_PROPN yr_DET Iwnifyrs_PROPN
Ffrindiau_NOUN dychmygol_ADJ ,_PUNCT caneuon_VERB cofiadwy_ADJ a_CONJ dathliadau_VERB 'r_DET Nadolig_PROPN -_PUNCT cyfuniad_NOUN hudolus_ADJ ar_ADP gyfer_NOUN ffilm_NOUN newydd_ADJ sbon_ADV ,_PUNCT Albi_PROPN a_CONJ Noa_NOUN yn_PART Achub_PROPN yr_DET Iwnifyrs_PROPN -_PUNCT ffilm_NOUN gerddorol_ADJ fydd_VERB yn_PART siŵr_ADJ o_ADP swyno_VERB 'r_DET teulu_NOUN cyfan_ADJ ddydd_NOUN Nadolig_PROPN ._PUNCT
Sêr_VERB y_DET ffilm_NOUN yw_VERB Noa_PROPN ,_PUNCT bachgen_NOUN 7_NUM mlwydd_NOUN oed_NOUN a_CONJ 'i_PRON gyfaill_NOUN pennaf_ADJ ,_PUNCT ei_DET ffrind_NOUN dychmygol_ADJ Albi_PROPN ._PUNCT
Mae_AUX mam_NOUN a_CONJ thad_NOUN Noa_PROPN wastad_ADV yn_PART brysur_ADJ ond_CONJ dydy_VERB hynny_PRON ddim_PART yn_PART ei_PRON boeni_VERB am_ADP fod_VERB ganddo_ADP Albi_PROPN 'n_PART gwmni_NOUN ._PUNCT
Efan_NOUN Williams_PROPN o_ADP Gaerdydd_PROPN ,_PUNCT sy_AUX 'n_PART chwarae_VERB Noa_PROPN ,_PUNCT a_CONJ Gruff_PROPN Davies_PROPN o_ADP Parc_NOUN ,_PUNCT ger_ADP y_DET Bala_PROPN yw_VERB ei_DET gyfaill_NOUN o_ADP 'r_DET byd_NOUN dychmygol_ADJ ,_PUNCT Albi_PROPN ._PUNCT
"_PUNCT Mae_VERB Noa_PROPN 'n_PART fachgen_NOUN hoffus_ADJ iawn_ADV sydd_AUX yn_PART cael_VERB hwyl_NOUN wrth_ADP chwarae_VERB gyda_ADP 'i_PRON ffrind_NOUN dychmygol_ADJ ,_PUNCT Albi_PROPN ,_PUNCT "_PUNCT meddai_VERB Efan_PROPN Williams_PROPN ,_PUNCT sy_VERB 'n_PART 9_NUM oed_NOUN ._PUNCT
"_PUNCT Mae_VERB Albi_PROPN 'n_PART hwyl_NOUN ,_PUNCT yn_PART garedig_ADJ ac_CONJ yn_PART ffrind_NOUN da_ADJ i_ADP Noa_PROPN ,_PUNCT "_PUNCT meddai_VERB Gruff_PROPN Davies_PROPN ,_PUNCT sy_VERB 'n_PART 7_NUM oed_NOUN ._PUNCT
"_PUNCT Mae_AUX Albi_PROPN 'n_PART gallu_VERB bod_VERB yn_PART ddrygionus_ADJ ,_PUNCT ond_CONJ mae_VERB o_PRON 'n_PART fodlon_ADJ cymryd_VERB y_DET bai_NOUN yn_PART lle_NOUN Noa_NOUN bob_DET tro_NOUN ._PUNCT
Mae_VERB llawer_PRON o_ADP hwyl_NOUN i_ADP 'w_PRON gael_VERB gydag_ADP Albi_X !_PUNCT "_PUNCT
Mae_VERB 'r_DET ddau_NUM gyfaill_NOUN yn_PART cael_VERB anturiaethau_NOUN mawr_ADJ !_PUNCT
Ar_ADP eu_DET pennau_NOUN eu_DET hunain_PRON yn_PART ystafell_NOUN wely_NOUN Noa_PROPN ,_PUNCT mae_VERB 'r_DET ddau_NUM fachgen_NOUN yn_PART achub_VERB y_DET bydysawd_NOUN rhag_ADP gelynion_NOUN peryglus_ADJ ._PUNCT
Er_ADP mwyn_NOUN dianc_VERB rhag_ADP diflastod_NOUN parti_NOUN gwaith_NOUN tad_NOUN Noa_PROPN ,_PUNCT mae_VERB dychymyg_NOUN Noa_PROPN ac_CONJ Albi_PROPN yn_PART mynd_VERB â_ADP nhw_PRON ar_ADP antur_NOUN yng_ADP nghwmni_NOUN 'r_DET pysgod_NOUN ar_ADP waelod_NOUN y_DET môr_NOUN ._PUNCT
Ac_CONJ yng_ADP nghanol_NOUN marchnad_NOUN Nadoligaidd_ADJ ,_PUNCT mae_VERB 'r_DET ddau_NUM gyfaill_NOUN yn_PART chwerthin_VERB nerth_NOUN eu_DET boliau_NOUN wrth_ADP chwarae_VERB cuddio_VERB yng_ADP nghanol_NOUN y_DET dorf_NOUN ._PUNCT
Ond_CONJ mae_AUX 'n_PART ymddangos_VERB bod_VERB yr_DET actorion_NOUN ifanc_ADJ wedi_PART cael_VERB bron_ADV cymaint_ADV o_ADP hwyl_NOUN wrth_ADP ffilmio_VERB ag_CONJ y_PART cafodd_VERB eu_DET cymeriadau_NOUN yn_ADP y_DET ffilm_NOUN ._PUNCT
"_PUNCT Roedd_VERB o_PRON 'n_PART brofiad_NOUN anhygoel_ADJ ffilmio_VERB Albi_PROPN a_CONJ Noa_NOUN yn_PART Achub_PROPN yr_DET Iwnifyrs_PROPN ,_PUNCT "_PUNCT meddai_VERB Gruff_PROPN ._PUNCT
"_PUNCT Roedd_VERB gweithio_VERB gyda_ADP 'r_DET actorion_NOUN eraill_ADJ yn_PART grêt_ADJ ac_CONJ yn_PART lot_NOUN fawr_ADJ o_ADP hwyl_NOUN ._PUNCT
Wnes_VERB i_PRON fwynhau_VERB gymaint_ADV ,_PUNCT baswn_VERB i_PRON 'n_PART bendant_ADJ eisiau_ADV gwneud_VERB mwy_PRON o_ADP actio_VERB yn_ADP y_DET dyfodol_NOUN ._PUNCT "_PUNCT
Ymhlith_NOUN y_DET cast_NOUN yn_ADP y_DET ffilm_NOUN fywiog_ADJ yma_ADV mae_VERB Ryland_NOUN Teifi_PROPN a_CONJ Catrin_PROPN Mara_PROPN ,_PUNCT sef_CONJ rhieni_NOUN Noa_PROPN ;_PUNCT a_CONJ Glyn_NOUN Pritchard_PROPN a_CONJ Gwydion_NOUN Rhys_PROPN sy_AUX 'n_PART chwarae_VERB rhai_PRON o_ADP 'r_DET ffrindiau_NOUN dychmygol_ADJ ._PUNCT
Mae_AUX Richard_PROPN Elis_PROPN hefyd_ADV yn_PART rhan_NOUN o_ADP 'r_DET cast_NOUN ._PUNCT
Byd_NOUN y_DET ffrindiau_NOUN dychmygol_ADJ sydd_VERB wrth_ADP galon_NOUN y_DET ffilm_NOUN yma_ADV ,_PUNCT sy_VERB 'n_PART llawn_ADJ cerddoriaeth_NOUN a_CONJ chanu_VERB ._PUNCT
Caryl_PROPN Parry_PROPN Jones_PROPN a_CONJ Non_PROPN Parry_PROPN sydd_AUX wedi_PART creu_VERB 'r_DET ffilm_NOUN ,_PUNCT ond_CONJ pam_ADV dewis_VERB stori_NOUN am_ADP fachgen_NOUN bach_ADJ unig_ADJ a_CONJ 'i_PRON ffrind_NOUN dychmygol_ADJ ?_PUNCT
Meddai_VERB Caryl_PROPN Parry_PROPN Jones_PROPN ,_PUNCT "_PUNCT Mae_AUX pŵer_NOUN y_DET dychymyg_NOUN mor_ADV bwysig_ADJ ac_CONJ yn_ADP ein_DET barn_NOUN ni_PRON mae_VERB o_PRON 'n_PART rhywbeth_NOUN y_PART dylid_VERB ei_DET annog_NOUN a_CONJ 'i_PRON barchu_VERB yn_PART enwedig_ADJ mewn_ADP plant_NOUN ._PUNCT
Dwi_VERB a_CONJ Non_PROPN wrth_ADP ein_DET boddau_NOUN efo_ADP ffilmiau_NOUN teuluol_ADJ ,_PUNCT cynnes_VERB ,_PUNCT Nadoligaidd_ADJ beth_PRON bynnag_PRON ,_PUNCT felly_CONJ roedd_VERB cael_VERB creu_VERB un_NUM ein_DET hunain_DET yn_PART fraint_NOUN ._PUNCT
"_PUNCT '_PUNCT Dan_PROPN ni_PRON 'm_DET isio_VERB pregethu_VERB wrth_ADP gwrs_NOUN ond_CONJ ella_ADV bobo_AUX ni_PRON 'n_PART gwneud_VERB y_DET pwynt_NOUN ein_DET bod_VERB ni_PRON fel_ADP rhieni_NOUN yn_PART gweithio_VERB 'n_PART galed_ADJ er_ADP mwyn_NOUN cael_VERB y_DET gorau_NOUN i_ADP 'n_PART plant_NOUN ac_CONJ yn_PART mwynhau_VERB ein_DET gwaith_NOUN yn_ADP y_DET broses_NOUN ._PUNCT
Ond_CONJ mae_VERB amser_NOUN yn_PART hedfan_ADJ ac_CONJ mae_VERB 'r_DET blynyddoedd_NOUN prin_ADJ yna_ADV hefo_ADP 'n_PART plant_NOUN yn_PART diflannu_VERB ._PUNCT
Ac_CONJ mae_VERB 'r_DET quality_NOUN time_NOUN y_PART mae_VERB plant_NOUN a_CONJ 'u_PRON rhieni_NOUN yn_PART ei_PRON dreulio_VERB hefo_ADP 'i_PRON gilydd_NOUN mor_ADV bwysig_ADJ i_ADP 'r_DET rhieni_NOUN ag_CONJ ydy_VERB o_PRON i_ADP 'r_DET plant_NOUN ._PUNCT
Mae_VERB 'n_PART werth_ADJ y_DET byd_NOUN ._PUNCT "_PUNCT
Meddai_VERB Elen_PROPN Rhys_PROPN ,_PUNCT Comisiynydd_PROPN Adloniant_PROPN S4C_PROPN ,_PUNCT "_PUNCT Ry_AUX 'n_PART ni_PRON 'n_PART falch_ADJ iawn_ADV o_ADP gomisiynu_VERB a_CONJ darlledu_VERB 'r_DET ffilm_NOUN yma_ADV ._PUNCT
Mae_VERB 'n_PART bleser_NOUN allu_CONJ cyflwyno_VERB 'r_DET ffilm_NOUN i_ADP genhedlaeth_NOUN newydd_ADJ o_ADP blant_NOUN ac_CONJ oedolion_NOUN a_PART bydd_AUX yn_PART parhau_VERB fel_ADP un_NUM o_ADP 'r_DET clasuron_NOUN ._PUNCT "_PUNCT
Albi_VERB a_CONJ Noa_NOUN yn_PART Achub_PROPN yr_DET Iwnifyrs_PROPN
Dydd_NOUN Nadolig_PROPN 25_NUM Rhagfyr_NOUN 7.30_NUM ,_PUNCT S4C_PROPN
Hefyd_ADV ,_PUNCT Gŵyl_NOUN San_NOUN Steffan_PROPN 1.05_NUM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Saesneg_PROPN ar_ADP gael_VERB
Ar_ADP alw_VERB :_PUNCT s4c.cymru_NOUN ;_PUNCT BBC_X iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cynhyrchiad_NOUN Boom_NOUN Cymru_PROPN ar_ADP gyfer_NOUN S4C_PROPN
