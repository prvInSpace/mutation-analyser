Cyfarwyddiaeth_NOUN :_PUNCT Gwasanaethau_NOUN Cymdeithasol_ADJ
Gwasanaeth_NOUN :_PUNCT Tîm_NOUN Anabledd_NOUN Dysgu_PROPN
Teitl_NOUN y_DET swydd_NOUN :_PUNCT Swyddog_NOUN Gofal_NOUN Cymdeithasol_ADJ -_PUNCT Llety_PROPN
Cyfeirnod_NOUN y_DET swydd_NOUN :_PUNCT I_ADP 'w_PRON gadarnhau_VERB
Lleoliad_NOUN :_PUNCT Hen_X Goleg_NOUN
Manylion_NOUN am_ADP gyflog_NOUN :_PUNCT Gradd_NOUN F_NUM
Bydd_VERB y_DET gyfradd_NOUN Defnyddiwr_NOUN Car_PROPN Achlysurol_ADJ yn_PART cael_VERB ei_PRON thalu_VERB i_ADP bobl_NOUN sy_AUX 'n_PART defnyddio_VERB eu_DET cerbydau_NOUN eu_DET hunain_DET ,_PUNCT bydd_AUX trefniadau_NOUN eraill_ADJ yn_PART cael_VERB eu_PRON hystyried_VERB ar_ADP gyfer_NOUN staff_NOUN heb_ADP ddulliau_VERB trafnidiaeth_NOUN eu_DET hunain_DET neu_CONJ sy_AUX 'n_PART dewis_ADV peidio_VERB â_CONJ defnyddio_VERB eu_DET cerbyd_NOUN eu_DET hunain_DET ,_PUNCT fel_CONJ y_PART pennir_VERB gan_ADP y_DET Cyngor_NOUN ._PUNCT
Diwrnodau_NOUN /_PUNCT Oriau_NOUN Gwaith_NOUN :_PUNCT Dydd_NOUN Llun_PROPN -_PUNCT Dydd_NOUN Gwener_PROPN ,_PUNCT 25_NUM awr_NOUN
Parhaol_ADJ /_PUNCT Dros_ADP Dro_NOUN :_PUNCT Parhaol_PROPN
Disgrifiad_NOUN byr_ADJ o_ADP 'r_DET swydd_NOUN :_PUNCT Bydd_VERB deiliad_NOUN y_DET swydd_NOUN yn_PART cynorthwyo_VERB gyda_ADP gweithredu_VERB 'r_DET swyddogaeth_NOUN a_CONJ monitro_VERB 'r_DET gwasanaeth_NOUN sy_AUX 'n_PART darparu_VERB llety_NOUN i_ADP oedolion_NOUN ag_ADP anabledd_NOUN dysgu_NOUN ._PUNCT
Oes_VERB angen_NOUN gwiriad_NOUN y_DET Gwasanaeth_NOUN Datgelu_PROPN a_CONJ Gwahardd_PROPN ?_PUNCT :_PUNCT
Uwch_PROPN
Am_ADP ragor_NOUN o_ADP wybodaeth_NOUN cysylltwch_VERB â_ADP :_PUNCT
Cewch_VERB becyn_NOUN cais_NOUN oddi_ADP wrth_ADP :_PUNCT
Ar_ADP -_PUNCT lein_NOUN
