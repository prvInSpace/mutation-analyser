Gŵyl_NOUN Llên_NOUN Plant_NOUN Caerdydd_PROPN 2017_NUM
25_NUM -_PUNCT 26_NUM Mawrth_PROPN &_ADP 1_NUM -_SYM 2_NUM Ebrill_PROPN
Caerdydd_PROPN
Pob_DET sesiwn_NOUN Cymraeg_PROPN yn_PART £_SYM 4_NUM
Bydd_AUX Gŵyl_PROPN Llên_NOUN Plant_NOUN Caerdydd_PROPN 2017_NUM yn_PART dychwelyd_VERB eleni_ADV rhwng_ADP 25_NUM Mawrth_PROPN a_CONJ 2_NUM Ebrill_PROPN ,_PUNCT ac_CONJ wedi_PART dathlu_VERB canmlwyddiant_NOUN geni_VERB storïwr_NOUN gorau_ADJ 'r_DET byd_NOUN y_DET llynedd_ADV ,_PUNCT mae_AUX 'r_DET Brifddinas_NOUN yn_PART edrych_VERB ymlaen_ADV at_ADP ragor_NOUN o_ADP hwyl_NOUN ym_ADP myd_NOUN y_DET llyfrau_NOUN ._PUNCT
Gŵyl_NOUN Llên_NOUN Plant_NOUN Caerdydd_PROPN 2016_NUM oedd_VERB y_DET bennod_NOUN fwyaf_ADV llwyddiannus_ADJ yn_ADP ei_DET hanes_NOUN hyd_NOUN yma_ADV gyda_ADP dros_ADP 50_NUM o_ADP ddigwyddiadau_NOUN a_CONJ 5000_NUM o_ADP docynnau_NOUN wedi_PART eu_DET gwerthu_VERB ._PUNCT
Bydd_AUX Gŵyl_PROPN Llên_NOUN Plant_NOUN Caerdydd_PROPN 2017_NUM yn_PART well_ADJ fyth_ADV !_PUNCT
Mae_AUX 'r_DET digwyddiad_NOUN wedi_PART parhau_VERB i_ADP dyfu_VERB dros_ADP y_DET blynyddoedd_NOUN ac_CONJ wedi_ADP llwyddiant_NOUN yr_DET arbrawf_NOUN llynedd_ADV ,_PUNCT fe_PART 'i_PRON cynhelir_VERB dros_ADP ddau_NUM benwythnos_NOUN eto_ADV eleni_ADV ._PUNCT
Bydd_VERB awduron_NOUN a_CONJ darlunwyr_NOUN yn_PART rhoi_VERB bywyd_NOUN i_ADP 'w_PRON geiriau_NOUN a_CONJ 'u_PRON lluniau_NOUN gyda_ADP straeon_NOUN gwych_ADJ a_CONJ pherfformiadau_NOUN gyda_ADP chast_NOUN o_ADP gymeriadau_NOUN ardderchog_ADJ ._PUNCT
Mae_VERB 'r_DET ŵyl_NOUN yn_PART ddelfrydol_ADJ i_ADP 'r_DET rheini_NOUN sy_AUX 'n_PART caru_VERB llyfrau_NOUN plant_NOUN ._PUNCT
Plant_NOUN neu_CONJ oedolion_NOUN -_PUNCT mae_VERB croeso_NOUN i_ADP bawb_PRON !_PUNCT
Eleni_ADV mae_VERB llond_NOUN trol_NOUN o_ADP awduron_NOUN ,_PUNCT cymeriadau_NOUN ac_CONJ artistiaid_NOUN Cymraeg_PROPN yn_PART cymryd_VERB rhan_NOUN gan_CONJ gynnwys_VERB
Alun_NOUN Saunders_PROPN
,_PUNCT
Atebol_PROPN
,_PUNCT
Anni_NOUN Llŷn_PROPN
(_PUNCT Bardd_NOUN Plant_NOUN Cymru_PROPN )_PUNCT ,_PUNCT
Barti_NOUN Ddu_PROPN
,_PUNCT
Siân_NOUN Lewis_PROPN
,_PUNCT
Huw_VERB Aaron_PROPN
,_PUNCT
Manon_VERB Steffan_PROPN Ros_PROPN
,_PUNCT
Rufus_VERB Mufasa_PROPN
,_PUNCT
Siôn_PROPN Tomos_PROPN Owen_PROPN
,_PUNCT
Mair_PROPN Tomos_PROPN Ifans_PROPN
,_PUNCT
Dewi_PROPN Dewr_PROPN
,_PUNCT
Sali_PROPN
a_CONJ 'r_DET bownsiwr_NOUN gofod_NOUN gwyllt_ADJ ,_PUNCT
Cledwyn_PROPN
y_DET môr_NOUN -_PUNCT leidr_NOUN ,_PUNCT
Myfanwy_VERB 'r_DET
campyrfan_NOUN a_CONJ
Rhodri_PROPN Owen_PROPN
,_PUNCT y_DET cyflwynydd_NOUN teledu_NOUN ._PUNCT
Bydd_VERB digonedd_VERB o_ADP weithdai_VERB ysgrifennu_VERB a_CONJ sesiynau_ADV gwrando_VERB ar_ADP stori_NOUN neu_CONJ chwedl_NOUN ._PUNCT
Cewch_VERB hefyd_ADV gyfle_NOUN i_ADP ganu_VERB a_CONJ dawnsio_VERB ,_PUNCT mwynhau_VERB sioe_NOUN ddrama_NOUN un_NUM dyn_NOUN a_CONJ chymryd_VERB rhan_NOUN mewn_ADP cwis_NOUN i_ADP 'r_DET teulu_NOUN cyfan_ADJ ._PUNCT
Mae_VERB yno_ADV rywbeth_NOUN at_ADP ddant_NOUN pawb_PRON ._PUNCT
I_PART weld_VERB y_DET rhaglen_NOUN yn_PART llawn_ADJ ac_CONJ i_PART brynu_VERB tocynnau_NOUN ,_PUNCT ewch_VERB i_ADP :_PUNCT
cyfeiriad_NOUN
cyfeiriad_NOUN
cyfeiriad_NOUN
