Mae_VERB yna_ADV dri_NUM ymgeisydd_NOUN yn_PART ras_NOUN i_ADP fod_VERB yn_PART arweinydd_NOUN nesaf_ADJ Plaid_NOUN Cymru_PROPN ._PUNCT
Leanne_NOUN Wood_VERB yr_DET arweinydd_NOUN ar_ADP hyn_PRON o_ADP bryd_NOUN ._PUNCT
Aelod_NOUN Cynulliad_NOUN Ynys_PROPN Môn_PROPN ._PUNCT
Rhun_VERB ap_NOUN Iorwerth_PROPN ac_CONJ Adam_PROPN Price_PROPN sy_AUX 'n_PART cynrychioli_VERB Dwyrain_NOUN Caerfyrddin_PROPN a_CONJ 'r_DET Dinefwr_PROPN ._PUNCT
Fe_PART wnaeth_VERB Rhun_PROPN ap_NOUN Iorwerth_PROPN ac_CONJ Adam_PROPN Price_PROPN siarad_AUX yn_PART fyw_VERB ar_ADP Radio_NOUN Yes_X Cymru_PROPN yn_ADP yr_DET Eisteddfod_PROPN ._PUNCT
Nid_PART oedd_VERB Leanne_PROPN Wood_X yn_PART rhydd_ADJ ond_CONJ fe_PART wnaethom_VERB lwyddo_VERB 'i_PRON chyfweld_VERB ar_ADP faes_NOUN yr_DET Eisteddfod_PROPN ._PUNCT
Dyma_DET 'r_DET cyfweliad_NOUN ._PUNCT
'_PUNCT Chi_PRON wedi_PART bod_VERB yn_PART arweinydd_NOUN Plaid_NOUN Cymru_PROPN ers_ADP dros_ADP chwe_NUM mlynedd_NOUN ac_CONJ  _SPACE ambell_ADJ fuddugoliaeth_NOUN arbennig_ADJ fel_ADP eich_DET un_NUM chi_PRON yn_ADP y_DET Rhondda_PROPN mae_AUX nifer_NOUN yn_PART teimlo_VERB fod_VERB Plaid_PROPN Cymru_PROPN wedi_PART aros_VERB yn_ADP ei_DET hunfan_NOUN ._PUNCT
Beth_PRON byddai_VERB eich_DET ymateb_NOUN chi_PRON i_ADP hynny_PRON ?_PUNCT
Mae_AUX 'r_DET blaid_NOUN gyda_ADP 'r_DET nifer_NOUN fwya'_ADJUNCT o_PRON [_PUNCT =_SYM ]_PUNCT aelodau_NOUN [_PUNCT /=_PROPN ]_PUNCT aelodau_NOUN San_NOUN Steffan_PROPN a_CONJ sydd_AUX wedi_PART bod_VERB erioed_ADV ._PUNCT
Roedd_VERB canlyniad_NOUN yr_DET etholia_NOUN [_PUNCT -_PUNCT ]_PUNCT etholiadau_NOUN lleol_ADJ yr_DET ail_ADJ orau_ADJ erioed_ADV gyda_ADP canlyniadau_NOUN cryf_ADJ mewn_ADP nifer_NOUN fawr_ADJ o_ADP lefydd_NOUN fel_ADP Nedd_PROPN ._PUNCT
Pontypridd_PROPN ._PUNCT
Rhondda_VERB a_CONJ hefyd_ADV Ceredigion_PROPN a_CONJ Caerfyrddin_PROPN ._PUNCT
'_PUNCT Da_ADJ ni_PRON hefyd_ADV wedi_PART cipio_VERB dau_NUM allan_ADV o_ADP bedwar_NUM o_ADP 'r_DET Comisiynwyr_NOUN Heddlu_PROPN ._PUNCT
Ond_CONJ faint_NOUN bynnag_PRON '_PUNCT da_ADJ ni_PRON wedi_PART llwyddo_VERB fel_ADP pob_DET aelod_NOUN arall_ADJ o_ADP blaid_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART ysu_VERB am_ADP weld_VERB mwy_PRON o_ADP gynnydd_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART sefyll_VERB  _SPACE mwyn_NOUN tyfu_VERB 'r_DET blaid_NOUN ac_CONJ  _SPACE mwyn_NOUN torri_VERB trwyddo_VERB mewn_ADP llefydd_NOUN ar_ADP draws_ADJ Cymru_PROPN fel_ADP wnaethom_VERB ni_PRON yn_ADP y_DET Rhondda_PROPN ._PUNCT
'_PUNCT Chi_PRON eisioes_ADV wedi_PART datgan_VERB y_PART byddwch_VERB yn_PART sefyll_VERB lawr_ADV fel_ADP arweinydd_NOUN os_CONJ na_PART fyddwch_VERB yn_ADP Brif_PROPN Weinidog_PROPN wedi_PART etholiadau_NOUN  _SPACE ugain_NUM dau_NUM ddeg_NOUN un_NUM ._PUNCT
Ond_CONJ nid_PART ydy_VERB datgan_VERB hynny_PRON sy_VERB 'n_PART sefyllfa_NOUN afrealistig_ADJ iawn_ADV yn_PART ôl_NOUN yr_DET arolygon_VERB barn_NOUN yn_ADP eich_PRON gwanhau_VERB chi_PRON ?_PUNCT
Dw_AUX i_PRON ddim_PART yn_PART derbyn_VERB ei_PRON fod_VERB yn_PART afrealistig_ADJ ._PUNCT
Des_VERB i_PRON o_ADP fewn_ADP un_NUM pleidlais_NOUN i_ADP fod_VERB yn_ADP Brif_PROPN Weinidog_PROPN ar_ADP ôl_NOUN yr_DET etholiad_NOUN dwethaf_ADJ ._PUNCT
Pan_CONJ es_VERB i_ADP ddatgan_VERB fyd_NOUN mod_AUX i_PRON 'n_PART mynd_VERB i_ADP sefyll_VERB yn_ADP y_DET Rhondda_PROPN yn_PART erbyn_ADP Leighton_PROPN Andrews_PROPN roedd_VERB bron_ADV pob_DET sylwebydd_NOUN yn_PART dweud_VERB fod_AUX ddim_PART chance_VERB gen_ADP i_PRON ._PUNCT
Ond_CONJ roedden_VERB nhw_PRON 'n_PART anghywir_ADJ ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_VERB gosod_VERB [_PUNCT =_SYM ]_PUNCT her_NOUN i_ADP [_PUNCT /=_PROPN ]_PUNCT her_NOUN i_ADP fy_DET hunan_VERB ac_CONJ i_ADP 'n_PART cydymgyrchwyr_NOUN ._PUNCT
Ond_CONJ os_CONJ ydyn_AUX ni_PRON 'n_PART mynd_VERB i_ADP gyrraedd_VERB y_DET nod_NOUN ._PUNCT
Rhaid_VERB dod_VERB at_ADP ein_DET gilydd_NOUN  _SPACE [_PUNCT =_SYM ]_PUNCT mwyn_NOUN [_PUNCT /=_PROPN ]_PUNCT mwyn_NOUN ei_DET gyflawni_VERB ._PUNCT
Mae_VERB rhai_PRON yn_PART eich_PRON beirniadu_VERB am_ADP ganolbwynito_NOUN 'n_PART ormodol_ADJ ar_ADP faterion_NOUN menywod_NOUN ac_CONJ ar_ADP faterion_NOUN ochwith_NOUN yn_PART hytrach_ADV na_CONJ materion_NOUN cenedlaetholgar_ADJ fel_ADP annibyniaeth_NOUN ._PUNCT
Sut_ADV fyddech_AUX chi_PRON 'n_PART ymateb_VERB i_ADP hynny_PRON ?_PUNCT
Dw_AUX i_PRON wedi_PART clywed_VERB hwnna_PRON ond_CONJ mae_VERB popeth_PRON dw_AUX i_PRON 'n_PART neud_VERB yn_PART ane_NOUN [_PUNCT -_PUNCT ]_PUNCT anelu_VERB at_ADP ennill_VERB annibyniaeth_NOUN yn_ADP y_DET pendraw_NOUN ._PUNCT
Mae_VERB annibyniaeth_NOUN i_ADP mi_PRON yn_PART fodd_NOUN o_ADP ddatrys_VERB problemau_NOUN ein_DET cymdeithas_NOUN ._PUNCT
Dw_VERB i_PRON 'n_PART hollol_ADV glir_ADJ taw_ADP dim_PRON ond_ADP trwy_ADP gael_VERB hunanreolaeth_NOUN gallwn_VERB ni_PRON fynd_VERB i_ADP 'r_DET afael_NOUN efo_ADP 'r_DET probleme_NOUN sy_AUX 'n_PART wynebu_VERB ni_PRON ._PUNCT
Dw_AUX i_PRON ddim_PART yn_PART ymddiheuro_VERB am_ADP godi_VERB 'r_DET problemau_NOUN mae_VERB meny_NOUN [_PUNCT -_PUNCT ]_PUNCT menywod_NOUN yn_PART wynebu_VERB  _SPACE enghraifft_NOUN nid_PART nish_ADV yw_VERB pethe_NOUN yna_DET ond_CONJ diffygion_VERB yn_ADP ein_DET systemau_NOUN dw_AUX i_ADP eisie_ADV mynd_VERB i_ADP 'r_DET afael_NOUN â_ADP nhw_PRON ._PUNCT
Wrth_ADP esbonio_VERB a_CONJ chael_VERB platfform_NOUN glir_ADJ gallw_VERB [_PUNCT -_PUNCT ]_PUNCT gallwch_VERB apelio_VERB at_ADP bobl_NOUN na_PART fyddai_VERB yn_PART naturiol_ADJ yn_PART cael_VERB eu_DET denu_VERB at_ADP Blaid_NOUN Cymru_PROPN na_CONJ chwaith_ADV annibyniaeth_NOUN ._PUNCT
Mae_AUX nifer_NOUN fawr_ADJ wedi_ADP eich_DET llongyfarch_NOUN am_ADP ymdrechu_VERB i_ADP ddysgu_VERB 'r_DET Gymraeg_PROPN yn_PART ystod_NOUN eich_DET cyfnod_NOUN fel_ADP Aelod_PROPN Cynulliad_NOUN ._PUNCT
Ydych_AUX chi_PRON 'n_PART teimlo_VERB eich_PRON bod_VERB o_ADP dan_ADP anfantais_NOUN yn_ADP y_DET ras_NOUN i_ADP a_CONJ [_PUNCT -_PUNCT ]_PUNCT i_PART aros_VERB yn_PART arweinydd_NOUN gan_ADP eich_PRON bod_VERB ddim_PART yn_PART hollol_ADV rhugl_ADJ yn_ADP y_DET Gymraeg_PROPN ?_PUNCT
Mae_VERB 'r_DET ffaith_NOUN bod_VERB aelodau_NOUN cyffredin_ADJ y_DET blaid_NOUN wedi_PART bod_VERB yn_PART fodlon_ADJ dewis_VERB arweinydd_NOUN sydd_AUX ddim_PART yn_PART siarad_VERB Cymraeg_PROPN yn_PART rhugl_ADJ wedi_PART newid_VERB agwedd_NOUN rhai_PRON at_ADP y_DET blaid_NOUN a_CONJ gwneud_VERB iddyn_ADP nhw_PRON sylweddoli_VERB fod_VERB y_DET blaid_NOUN i_ADP bawb_PRON ._PUNCT
Ond_CONJ mae_VERB gen_ADP i_PRON rôl_NOUN fel_ADP dysgwr_NOUN i_ADP osod_VERB esiampl_NOUN i_ADP wynebu_VERB y_DET dyffi_NOUN [_PUNCT -_PUNCT ]_PUNCT y_DET diffyg_NOUN hyder_NOUN sydd_VERB gen_ADP i_PRON ac_CONJ i_PART edrych_VERB am_ADP fwy_PRON o_ADP gyfleoedd_NOUN i_ADP siarad_VERB Cymraeg_PROPN ._PUNCT
Dydi_VERB hi_PRON ddim_PART yn_PART hawdd_ADJ ond_CONJ mae_VERB 'r_DET gefnogaeth_NOUN ac_CONJ ewyllys_NOUN da_ADJ siaradwyr_NOUN Gymraeg_PROPN yn_PART help_NOUN mawr_ADJ ._PUNCT
Os_CONJ na_PART fyddech_VERB chi_PRON yn_ADP y_DET ras_NOUN ._PUNCT
Pwy_PRON fydd_AUX fyddech_VERB chi_PRON 'n_PART ffafrio_VERB rhwng_ADP Rhun_PROPN a_CONJ Adam_PROPN ?_PUNCT
Pa_PART un_NUM sydd_VERB agosaf_ADJ atoch_ADP chi_PRON o_ADP ran_NOUN gwleidyddiaeth_NOUN ?_PUNCT
Mae_VERB hwnna_PRON 'n_PART cwestiwn_NOUN anodd_ADJ oherwydd_ADP dw_AUX i_ADP wedi_PART gweithio_VERB yn_PART agos_ADJ iawn_ADV gyda_ADP 'r_DET ddau_NUM ._PUNCT
I_ADP mi_PRON un_NUM o_ADP 'r_DET cwestiynau_NOUN sylfaenol_ADJ yw_VERB beth_PRON yw_VERB eu_DET safbwynt_NOUN nhw_PRON tuag_ADP at_ADP gweithio_VERB gyda_ADP 'r_DET Torïaid_PROPN ?_PUNCT
Mi_PART fyddwn_AUX i_PRON 'n_PART ffeindio_VERB fe_PRON 'n_PART anodd_ADJ os_CONJ nad_ADV yn_PART amhosib_NOUN i_PART cefnogi_VERB ymgeisydd_NOUN sydd_VERB yn_ADP y_DET frwd_NOUN dros_ADP neud_VERB hynny_PRON na_PART chwaith_ADV ymgeisydd_NOUN sydd_VERB yn_PART fodlon_ADJ ystyried_VERB hynny_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART credu_VERB wrth_ADP wneud_VERB hynny_PRON neu_CONJ hyd_NOUN yn_PART oed_NOUN ei_PRON ystyried_VERB byddwn_AUX yn_PART colli_VERB cefnogaeth_NOUN pleidleisiwr_NOUN o_ADP bob_DET ran_NOUN o_ADP Gymru_PROPN ._PUNCT
Dw_AUX i_PRON 'n_PART gobeithio_VERB nad_PART ydyn_VERB nhw_PRON am_ADP ddilyn_VERB trywydd_NOUN y_DET Lib_NOUN Dems_VERB yng_ADP Nghymru_PROPN a_CONJ beni_VERB lan_ADV gyda_ADP plaid_NOUN sydd_AUX wedi_PART rhwygo_VERB ac_CONJ ar_ADP fin_NOUN diflannu_VERB ._PUNCT
Sut_ADV Eisteddfod_NOUN '_PUNCT chi_PRON wedi_PART ei_DET gael_VERB ?_PUNCT
Be_PRON ydy_VERB 'r_DET uchafbwynt_NOUN i_ADP chi_PRON hyd_NOUN yma_ADV ?_PUNCT
Mae_AUX wedi_PART bod_VERB yn_ADP Eisteddfod_NOUN bendigedig_ADJ ._PUNCT
Mae_VERB 'r_DET tywydd_NOUN yn_PART [_PUNCT -_PUNCT ]_PUNCT wedi_PART helpu_VERB ond_CONJ hefyd_ADV mae_VERB 'r_DET teimlad_NOUN yma_DET wedi_PART bod_VERB mor_ADV bositif_ADJ ._PUNCT
Mae_VERB gymaint_ADV o_ADP uchafbwyntiau_NOUN fel_CONJ gweld_VERB ffrindiau_NOUN fel_ADP Catrin_PROPN Dafydd_PROPN a_CONJ Manon_NOUN Steffan_PROPN Ros_PROPN yn_PART ennill_VERB rhai_PRON o_ADP prif_ADJ wobrau_NOUN ac_CONJ i_ADP fod_VERB yn_PART blwyfol_ADJ ._PUNCT
Gweld_VERB llwyddiannau_NOUN pobol_ADJ o_ADP Cwm_NOUN Rhondda_PROPN ._PUNCT
Ond_CONJ fel_ADP pob_DET Eisteddfod_NOUN arall_ADJ mae_AUX fe_PRON 'n_PART gyfle_NOUN i_PART weld_VERB rhai_DET pobol_ADJ ti_PRON ond_CONJ yn_PART gweld_VERB unwaith_ADV y_DET flwyddyn_NOUN ._PUNCT
Ewch_VERB at_ADP Yes_X dot_NOUN Cymru_PROPN blaenslaes_NOUN Radio_NOUN Rhydd_PROPN i_PART wrando_VERB ar_ADP gyfweliadau_NOUN Rhun_PROPN ap_NOUN Iorwerth_PROPN a_CONJ Adam_NOUN Price_PROPN ._PUNCT
Diolch_NOUN i_ADP chi_PRON gyd_ADP am_ADP wrando_VERB ._PUNCT
Hwyl_NOUN fawr_ADJ ._PUNCT
388.889_ADJ
