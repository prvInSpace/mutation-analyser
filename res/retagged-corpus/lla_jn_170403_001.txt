[_PUNCT Cerddoriaeth_NOUN agoriadol_ADJ y_DET rhaglen_NOUN yn_PART chwarae_VERB ]_PUNCT ._PUNCT
Os_CONJ '_PUNCT ych_AUX chi_PRON eisie_NOUN gw'bod_VERB shwt_NOUN ma_PRON neud_VERB hyn_PRON ._PUNCT
Hyn_PRON a_CONJ hyn_PRON ._PUNCT
Bydd_VERB y_DET ddeg_NOUN munud_NOUN nesaf_ADJ yn_PART hedfan_ADJ ond_CONJ yn_PART gynta_ADJ ._PUNCT
Enw_NOUN fi_PRON 'di_PART enwb_VERB a_CONJ heddiw_ADV dw_VERB i_PRON am_ADP drio_VERB i_ADP neud_VERB bouncy_NOUN balls_ADJ ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB i_ADP fynd_VERB am_ADP un_NUM o_ADP bob_DET dim_PRON ._PUNCT
Un_NUM piws_ADJ un_NUM glas_ADJ un_NUM gwyrdd_ADJ un_NUM oren_ADJ a_CONJ un_NUM melyn_NOUN ._PUNCT
Mae_AUX hwn_PRON yn_PART edrych_VERB yn_PART hwyl_NOUN ._PUNCT
Glas_PROPN ._PUNCT
[_PUNCT Cerddoriaeth_NOUN gefndirol_ADJ ]_PUNCT ._PUNCT
A_PART mae_AUX 'n_PART d'eud_VERB ar_ADP y_DET cyfarwyddiade_NOUN i_ADP dapio_VERB 'n_PART ofalus_ADJ ar_ADP y_DET plastig_NOUN so_AUX dyma_ADV ni_PRON 'n_PART tapio_VERB [_PUNCT Swn_PROPN tapio_VERB yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
A_CONJ oren_ADJ ._PUNCT
Mwy_PRON o_ADP 'r_DET tapio_VERB gofalus_ADJ [_PUNCT saib_NOUN ]_PUNCT a_CONJ gwyrdd_ADJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Oh_INTJ a_CONJ piws_ADJ ._PUNCT
Dw_AUX i_PRON 'n_PART rili_ADV hoffi_VERB 'r_DET lliw_NOUN yma_DET ._PUNCT
Mi_PART fydd_VERB hon_PRON yn_PART bêl_NOUN bouncy_NOUN amryliw_ADJ ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB '_PUNCT bach_ADJ o_ADP felyn_ADJ ?_PUNCT
Dyna_ADV ni_PRON [_PUNCT aneglur_ADJ ]_PUNCT pawb_PRON ._PUNCT
Mmm_VERB cofiwch_VERB cadw_VERB y_DET paced_NOUN ar_ADP y_DET bwrdd_NOUN cywir_ADJ ._PUNCT
Tip_NOUN defnyddiol_ADJ ._PUNCT
'_PUNCT Bach_ADJ o_ADP fess_NOUN ond_CONJ dyna_ADV ni_PRON ._PUNCT
Un_NUM i_ADP lawr_ADV ._PUNCT
Llall_VERB i_ADP fewn_ADP [_PUNCT Swn_PROPN rhywbeth_NOUN yn_PART cwympo_VERB i_ADP ddŵr_NOUN ]_PUNCT ._PUNCT
Www_INTJ ._PUNCT
Gobeithio_VERB bod_VERB hwnna_PRON 'n_PART iawn_ADJ ._PUNCT
Ac_CONJ yr_DET un_NUM olaf_ADJ ._PUNCT
A_CONJ dyna_ADV ni_PRON ._PUNCT
'_PUNCT Den_VERB ni_PRON wedi_PART [_PUNCT aneglur_ADJ ]_PUNCT neud_VERB ein_DET bouncy_NOUN balls_ADJ a_CONJ rhaid_VERB i_ADP ni_PRON gobeithio_VERB fod_VERB bob_DET dim_PRON yn_PART iawn_ADJ ._PUNCT
A_CONJ nawr_ADV jyst_ADV aros_VERB ._PUNCT
[_PUNCT Cerddoriaeth_NOUN gefndirol_ADJ ]_PUNCT ._PUNCT
Iawn_ADV so_PRON mae_AUX tri_NUM munud_NOUN wedi_PART bod_VERB a_CONJ '_PUNCT den_NUM ni_PRON am_PART weld_VERB sut_ADV mae_VERB y_DET bouncy_NOUN balls_PUNCT yn_PART mynd_VERB ._PUNCT
Oh_X cyffrous_NOUN ._PUNCT
'_PUNCT Co_NOUN ni_PRON off_ADV ._PUNCT
Agor_VERB y_DET cyntaf_NOUN ._PUNCT
Ahh_ADP +_SYM
Oh_INTJ ._PUNCT
Oh_INTJ ._PUNCT
Mae_AUX 'r_DET melyn_NOUN a_CONJ 'r_DET glas_NOUN a_CONJ 'r_DET oren_NOUN mor_ADV neis_ADJ ar_ADP y_DET top_NOUN ._PUNCT
+_VERB Ahh_ADP +_SYM
Ie_INTJ ._PUNCT
Iawn_INTJ ._PUNCT
Mae_AUX o_PRON wedi_PART gweithio_VERB fyswn_VERB i_PRON 'n_PART d'eud_VERB ._PUNCT
Nesa_ADJ ._PUNCT
Oh_INTJ mae_VERB hwn_PRON yn_PART un_NUM neis_ADJ [_PUNCT Swn_PROPN bownsio_VERB yn_ADP y_DET cefndir_NOUN ]_PUNCT ._PUNCT
Www_VERB mae_AUX hwnna_PRON 'n_PART bownsio_VERB ._PUNCT
Ydy_VERB ac_CONJ mae_AUX o_PRON 'n_PART gweithio_VERB ._PUNCT
So_PART allan_ADV o_ADP ddeg_NOUN i_ADP 'r_DET rhain_PRON fyswn_VERB i_PRON 'n_PART rhoid_VERB naw_NUM naw_NUM a_CONJ hannar_ADJ ._PUNCT
Dw_AUX i_PRON 'n_PART rhoid_VERB naw_NUM a_CONJ hannar_ADJ iddyn_ADP nhw_PRON ._PUNCT
E'lla_VERB ar_ADP ôl_NOUN iddyn_ADP nhw_PRON galedu_VERB fysan_VERB nhw_PRON 'n_PART bownsio_VERB ychydig_ADV mwy_ADJ ._PUNCT
Fysa_VERB hynna_PRON 'n_PART bosibl_NOUN dw_AUX i_PRON 'n_PART meddwl_VERB ._PUNCT
Dyna_ADV ni_PRON mae_AUX 'r_DET rhain_PRON wedi_PART gweithio_VERB ._PUNCT
Diolch_VERB enwb_NOUN ._PUNCT
Sialens_NOUN arall_ADJ nawr_ADV ._PUNCT
Dau_NUM o_ADP pob_DET tîm_NOUN ._PUNCT
Pwy_PRON all_ADV wneud_VERB y_DET fwyaf_NOUN o_ADP brechdanau_VERB mewn_ADP tri_NUM deg_NUM eiliad_NOUN ?_PUNCT
Barod_PROPN ?_PUNCT
Un_NUM dau_NUM tri_NUM ewch_NOUN ._PUNCT
Wel_CONJ am_ADP hwyl_NOUN bois_NOUN bach_ADJ ._PUNCT
'_PUNCT Neud_VERB brechdanau_VERB heb_ADP weld_VERB yn_PART iawn_ADJ beth_PRON '_PUNCT dech_AUX chi_PRON 'n_PART ei_PRON neud_VERB ._PUNCT
Syniad_NOUN pwy_PRON o'dd_NOUN hwn_PRON ?_PUNCT
Oh_INTJ na_PART mae_VERB 'n_PART panics_NOUN 'ma_ADV [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Y_DET tri_NUM deg_NUM eiliad_NOUN bron_ADV ar_ADP ben_NOUN ._PUNCT
Oh_INTJ '_PUNCT oedd_VERB hwnna_PRON 'n_PART ormod_NOUN o_ADP gyffro_VERB i_ADP mi_PRON bois_VERB ._PUNCT
A_CONJ phwy_PRON sydd_AUX wedi_PART ennill_VERB ?_PUNCT
Mae_VERB 'r_DET bechgyn_NOUN yn_PART hapus_ADJ ._PUNCT
Merched_VERB ddim_PART ._PUNCT
Mmm_VERB losin_NOUN iogwrt_X mmm_X mmm_NOUN ._PUNCT
Ffein_PROPN ._PUNCT
Mmm_VERB ._PUNCT
'_PUNCT Co_NOUN ni_PRON ym_ADP Mhrestatyn_PROPN mewn_ADP clwb_NOUN aerial_NOUN arts_NOUN a_CONJ dyma_DET un_NUM o_ADP aelodau_NOUN 'r_DET clwb_NOUN i_PART egluro_VERB mwy_ADV ._PUNCT
Fi_PRON yw_VERB enwb_NOUN a_CONJ dw_AUX i_PRON 'n_PART gwneud_VERB <_SYM an_DET >_DET aerial_NOUN arts_NOUN ._PUNCT
600.000_NOUN
