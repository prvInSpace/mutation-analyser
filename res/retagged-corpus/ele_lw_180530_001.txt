Noswaith_NOUN dda_ADJ ,_PUNCT
Bydd_PROPN
cyfeiriad_NOUN
a_CONJ Chymdeithas_PROPN yr_DET Iaith_NOUN yn_PART cynnal_VERB trafodaeth_NOUN ar_ADP iechyd_NOUN meddwl_VERB yn_ADP Eisteddfod_PROPN yr_DET Urdd_NOUN eleni_ADV ,_PUNCT ar_ADP
ddydd_NOUN Gwener_PROPN y_DET 1_NUM af_VERB o_ADP Fehefin_NOUN am_ADP 2_NUM pm_NOUN ar_ADP stondin_NOUN y_DET Gymdeithas_NOUN ._PUNCT
Yr_DET aelodau_NOUN ar_ADP y_DET panel_NOUN drafod_VERB fydd_VERB :_PUNCT
-_PUNCT
enwg1_VERB cyfenw1_ADJ __NOUN
-_PUNCT
Gwen_NOUN Goddard_PROPN
o_ADP 'r_DET elusen_NOUN Hyfforddiant_NOUN Mewn_ADP Meddwl_PROPN
-_PUNCT
enwg2_ADV cyfenw2_SYM __NOUN
ar_ADP ran_NOUN
cyfeiriad_NOUN
-_PUNCT
enwb3_VERB cyfenw3_ADJ __NOUN
o_ADP Gymdeithas_NOUN yr_DET Iaith_NOUN
Ymhlith_NOUN y_DET pynciau_NOUN a_CONJ drafodir_VERB fydd_VERB iechyd_NOUN meddwl_VERB ymhlith_ADP pobl_NOUN ifanc_ADJ ,_PUNCT a_CONJ gwasanaethau_VERB iechyd_NOUN meddwl_VERB Cymraeg_PROPN ._PUNCT
Yn_PART dilyn_VERB y_DET drafodaeth_NOUN bydd_VERB cyfle_NOUN am_ADP sgwrs_NOUN anffurfiol_ADJ ._PUNCT
Gobeithio_VERB y_PART gallwch_VERB alw_VERB draw_ADV i_ADP 'r_DET digwyddiad_NOUN os_CONJ byddwch_VERB ar_ADP y_DET maes_NOUN ,_PUNCT a_CONJ
rhannu_VERB 'r_DET manylion_NOUN
gyda_ADP 'ch_PRON cysylltiadau_NOUN ._PUNCT
Hwyl_INTJ ,_PUNCT
enwb4_NOUN
