Cyfres_NOUN newydd_ADJ yn_PART rhoi_VERB sylw_NOUN i_ADP genhedlaeth_NOUN newydd_ADJ o_ADP focswyr_NOUN
Mae_VERB bocsio_VERB yng_ADP Nghymru_PROPN yn_PART mwynhau_VERB cyfnod_NOUN llwyddiannus_ADJ ac_CONJ mae_AUX 'r_DET dyfodol_NOUN yn_PART edrych_VERB yn_PART ddisglair_ADJ yn_PART ôl_NOUN yr_DET ymladdwr_NOUN proffesiynol_ADJ o_ADP Bontyberem_X ,_PUNCT Zack_X Davies_PROPN ._PUNCT
Bydd_VERB y_DET bocsiwr_NOUN pwysau_NOUN is-_ADJ welter_NOUN ,_PUNCT sydd_AUX wedi_PART ennill_VERB saith_ADV a_CONJ cholli_VERB un_NUM o_ADP 'i_PRON ornestau_VERB hyd_NOUN yma_ADV ,_PUNCT yn_PART rhan_NOUN o_ADP dîm_NOUN cyflwyno_VERB cyfres_NOUN newydd_ADJ S4C_PROPN ,_PUNCT Y_DET Ffeit_NOUN ._PUNCT
Bydd_VERB y_DET gyfres_NOUN newydd_ADJ chwe_NUM rhan_NOUN yn_PART dangos_VERB uchafbwyntiau_NOUN gornestau_ADV proffesiynol_ADJ ac_CONJ amatur_NOUN o_ADP 'r_DET nosweithiau_NOUN bocsio_VERB ac_CONJ MMA_NOUN diweddaraf_ADJ bob_DET nos_NOUN Fercher_NOUN o_ADP 29_NUM Mawrth_PROPN ._PUNCT
Bydd_VERB y_DET rhaglen_NOUN gyntaf_ADJ yn_PART cynnwys_VERB uchafbwyntiau_NOUN o_ADP noson_NOUN o_ADP focsio_VERB yng_ADP Nghanolfan_NOUN Hamdden_PROPN Rhydycar_X ,_PUNCT Merthyr_PROPN Tudful_PROPN ._PUNCT
Ym_ADP mhrif_NUM ornest_NOUN y_DET noson_NOUN ,_PUNCT mi_PART fydd_VERB yr_DET ymladdwr_NOUN o_ADP Aberpennar_PROPN ,_PUNCT Tony_X Dixon_PROPN ,_PUNCT yn_PART herio_VERB Mike_PROPN Jones_PROPN ,_PUNCT o_ADP Wrecsam_PROPN i_ADP fod_VERB yn_ADP Bencampwr_PROPN Pwysau_NOUN Gor_PROPN Welter_X Cymru_PROPN ._PUNCT
Mae_AUX Zack_PROPN yn_PART edrych_VERB ymlaen_ADV at_PART gymryd_VERB ei_DET sedd_NOUN ar_ADP ochr_NOUN y_DET sgwâr_NOUN ._PUNCT
Sut_ADV daeth_VERB dy_DET ddiddordeb_NOUN mewn_ADP bocsio_VERB ?_PUNCT
Mae_AUX bocsio_VERB yn_ADP fy_DET ngwaed_NOUN i_PRON ._PUNCT
Fe_PART ddechreuodd_VERB fy_DET nhad_NOUN -_PUNCT cu_PART ei_DET gampfa_NOUN ei_DET hun_NOUN ym_ADP Mhontyberem_PROPN ,_PUNCT ac_CONJ roedd_VERB ei_DET bedwar_NUM mab_NOUN e_PRON 'n_PART bocsio_VERB ._PUNCT
Mae_VERB 'na_ADV hen_ADJ luniau_NOUN o_ADP 'n_PART nhad_NOUN i_ADP 'n_PART bocsio_VERB ar_ADP y_DET waliau_NOUN ,_PUNCT felly_CONJ byddwn_AUX i_PRON 'n_PART dweud_VERB mai_PART fy_DET nheulu_NOUN i_ADP dwi_AUX 'n_PART edrych_VERB lan_ADV at_ADP fwyaf_ADJ yn_ADP y_DET byd_NOUN bocsio_VERB ._PUNCT
Ond_CONJ daeth_VERB fy_DET niddordeb_NOUN mewn_ADP bocsio_VERB pan_CONJ oeddwn_VERB i_PRON tua_ADV 11_NUM oed_NOUN ._PUNCT
Roedd_AUX fy_DET mrawd_NOUN i_PRON 'n_PART henach_ADJ na_CONJ fi_PRON ac_CONJ roedd_AUX e_PRON 'n_PART cael_VERB lot_PRON o_ADP dariannau_NOUN a_CONJ lot_PRON o_ADP 'r_DET limelight_NOUN drwy_ADP focsio_VERB ,_PUNCT ac_CONJ roeddwn_VERB i_ADP eisiau_VERB dipyn_PRON o_ADP hynny_PRON ._PUNCT
Felly_CONJ trwy_ADP genfigen_NOUN nes_CONJ i_PART ddechrau_ADV bocsio_VERB a_CONJ dwi_VERB 'di_PART bod_VERB yn_PART hooked_NOUN ers_ADP hynny_PRON ._PUNCT
Be_PRON yw_VERB uchafbwyntiau_NOUN dy_DET yrfa_NOUN hyd_NOUN yma_ADV a_CONJ beth_PRON yw_VERB dy_DET uchelgais_NOUN ?_PUNCT
'_PUNCT Nes_ADV i_PART ymladd_VERB ar_ADP undercard_NOUN Roy_NOUN Jones_PROPN Jr_PROPN v_ADP Enzo_PROPN Maccarinelli_PROPN ym_ADP Moscow_PROPN yn_PART 2015_NUM ,_PUNCT a_CONJ '_PUNCT nes_CONJ i_ADP ennill_VERB ._PUNCT
Roedd_VERB e_PRON 'n_PART sioe_NOUN wych_ADJ mewn_ADP stadiwm_NOUN fawr_ADJ sy_VERB 'n_PART dal_ADV 20,000_NUM o_ADP bobl_NOUN ._PUNCT
Roedd_VERB e_PRON 'n_PART brofiad_NOUN wna_VERB i_PRON byth_ADV anghofio_VERB ._PUNCT
Fel_ADP bocsiwr_NOUN amatur_NOUN ,_PUNCT es_VERB i_ADP Gemau_NOUN 'r_DET Gymanwlad_PROPN yng_ADP Nglasgow_PROPN yn_PART 2014_NUM ,_PUNCT ac_CONJ roeddwn_VERB i_PRON 'n_PART rhan_NOUN o_ADP dîm_NOUN Prydain_PROPN am_ADP bedair_NUM blynedd_NOUN ._PUNCT
Yn_ADP y_DET dyfodol_NOUN ,_PUNCT dwi_AUX jyst_ADV eisiau_ADV cymryd_VERB pethau_NOUN fel_ADP maen_AUX nhw_PRON 'n_PART dod_VERB ,_PUNCT a_CONJ mynd_VERB mor_ADV bell_ADJ ag_CONJ y_PART galla_VERB i_PRON ._PUNCT
Does_VERB gen_ADP i_PRON ddim_PART gôl_NOUN i_ADP fod_VERB yn_ADP Bencampwr_PROPN y_DET Byd_NOUN erbyn_ADP rhyw_DET amser_NOUN penodol_ADJ ._PUNCT
Dwi_VERB moyn_ADV cael_VERB y_DET gorau_ADJ allan_ADV o_ADP 'm_DET mhotensial_NOUN ,_PUNCT cario_VERB '_PUNCT mlaen_NOUN i_PART weithio_VERB 'n_PART galed_ADJ a_CONJ bod_VERB mor_ADV ffit_ADJ â_ADP fi_PRON 'n_PART gallu_VERB bod_VERB ._PUNCT
Sut_ADV siâp_NOUN sydd_VERB ar_ADP focsio_VERB yng_ADP Nghymru_PROPN ar_ADP hyn_PRON o_ADP bryd_NOUN ?_PUNCT
Mae_AUX Lee_PROPN ac_CONJ Andrew_PROPN Selby_PROPN ar_ADP flaen_NOUN y_PART gad_VERB ar_ADP y_DET funud_NOUN ,_PUNCT ac_CONJ mae_VERB Nathan_PROPN Cleverly_X hefyd_ADV yn_PART gwneud_VERB yn_PART dda_ADJ ._PUNCT
Mae_VERB 'na_ADV foi_NOUN o_ADP 'n_PART gampfa_NOUN i_PRON ,_PUNCT Liam_PROPN Williams_PROPN ,_PUNCT yn_PART dechrau_ADV cael_VERB enw_NOUN iddo_ADP 'i_PRON hun_NOUN ac_CONJ mae_VERB e_PRON 'n_PART Bencampwr_PROPN y_DET Gymanwlad_PROPN ._PUNCT
Fi_PRON 'n_PART credu_VERB bod_VERB 'na_ADV lot_PRON o_ADP ymladdwyr_NOUN o_ADP ansawdd_NOUN yn_PART dod_VERB trwyddo_NOUN ar_ADP y_DET funud_NOUN ,_PUNCT pobl_NOUN fel_ADP Dale_NOUN Evans_PROPN ,_PUNCT Tony_X Dixon_PROPN ,_PUNCT Chris_PROPN Jenkins_PROPN ac_CONJ Alex_PROPN Hughes_PROPN ._PUNCT
Falle_VERB 'r_DET bois_NOUN yma_ADV fydd_VERB y_DET Cleverly_PROPN neu_CONJ 'r_DET Selby_PROPN nesaf_ADJ ,_PUNCT felly_CONJ mae_VERB 'n_PART grêt_ADJ bod_AUX ni_PRON 'n_PART gallu_ADV dilyn_VERB gyrfaoedd_NOUN y_DET bocswyr_NOUN Cymreig_ADJ 'ma_ADV o_ADP 'r_DET dechrau_NOUN ar_ADP Y_DET Ffeit_NOUN ._PUNCT
Pwy_PRON wyt_AUX ti_PRON 'n_PART edrych_VERB ymlaen_ADV at_ADP ei_PRON weld_VERB ar_ADP noson_NOUN gyntaf_NUM Y_DET Ffeit_NOUN ym_ADP Merthyr_PROPN ?_PUNCT
Tony_NOUN Dixon_PROPN yw_VERB 'r_DET enw_NOUN mawr_ADJ ar_ADP y_DET cerdyn_NOUN a_CONJ fi_PRON 'n_PART edrych_VERB ymlaen_ADV at_ADP ei_PRON weld_VERB e_PRON ._PUNCT
Mae_VERB Tony_PROPN yn_PART focsiwr_NOUN cyffrous_ADJ ac_CONJ yn_PART foi_NOUN cryf_ADJ ._PUNCT
Fe_PART gafodd_VERB ei_DET fwrw_NOUN mas_NOUN y_DET flwyddyn_NOUN ddiwethaf_ADJ ,_PUNCT ac_CONJ mae_AUX e_PRON wedi_PART ennill_VERB un_NUM ffeit_NOUN ers_ADP hynny_PRON ,_PUNCT ond_CONJ bydd_VERB hwn_PRON yn_PART brawf_NOUN mawr_ADJ iddo_ADP eto_ADV yn_ADP erbyn_ADP Mike_PROPN Jones_PROPN ,_PUNCT rhywun_NOUN fydd_AUX yn_PART edrych_VERB ymlaen_ADV at_ADP ffeit_NOUN fwyaf_ADJ ei_DET yrfa_NOUN hyd_NOUN yn_PART hyn_PRON ,_PUNCT a_CONJ rhywun_NOUN sydd_VERB wastad_ADV mewn_ADP siâp_NOUN da_ADJ ._PUNCT
Mae_AUX Dorian_NOUN Darch_X (_PUNCT prif_ADJ focsiwr_NOUN pwysau_NOUN trwm_ADJ Cymru_PROPN fydd_AUX yn_PART ymladd_VERB Chris_PROPN Healey_PROPN )_PUNCT ,_PUNCT yn_PART focsiwr_NOUN cryf_ADJ hefyd_ADV ,_PUNCT felly_CONJ bydd_VERB yr_DET ornest_NOUN yma_DET yn_PART werth_ADJ ei_PRON gweld_VERB hefyd_ADV ._PUNCT
Y_DET Ffeit_NOUN :_PUNCT Bocsio_PROPN
Nos_NOUN Fercher_NOUN 29_NUM Mawrth_PROPN 9.30_NUM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Saesneg_PROPN
Ar_PART gael_VERB ar_ADP alw_VERB ar_ADP s4c.cymru_NOUN ,_PUNCT BBC_PROPN iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cyd_ADJ -_PUNCT gynhyrchiad_NOUN Antena_PROPN a_CONJ Tanabi_X ar_ADP gyfer_NOUN S4C_PROPN
