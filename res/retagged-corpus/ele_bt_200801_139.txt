"_PUNCT Pecyn_NOUN y_DET Wasg_NOUN S4C_ADJ W_NUM 01_NUM Press_X Pack_X "_PUNCT ,_PUNCT "_PUNCT PECYN_NOUN Y_DET WASG_NOUN S4C_PROPN W_NUM 01_NUM PRESS_X PACK_PROPN
28_NUM Rhagfyr_NOUN December_PROPN 2019_NUM -_PUNCT 03_NUM Ionawr_NOUN January_PROPN 2020_NUM
1_NUM ._PUNCT
Goreuon_VERB Ryan_PROPN a_CONJ Ronnie_PROPN
Tudur_PROPN Owen_PROPN ,_PUNCT sy_AUX 'n_PART edrych_VERB yn_PART ôl_NOUN ar_ADP ddoniau_NOUN sêr_NOUN mwyaf_ADJ yn_PART hanes_NOUN comedi_NOUN Cymru_PROPN Ryan_PROPN a_CONJ Ronnie_PROPN ac_CONJ yn_PART edrych_VERB ar_ADP pam_ADV rydym_AUX ni_PRON fel_ADP cenedl_NOUN yn_PART eu_DET trysori_VERB cymaint_ADV ._PUNCT
2_NUM ._PUNCT
Heno_ADV Nos_NOUN Galan_PROPN
Bydd_AUX criw_NOUN Heno_PROPN yn_PART edrych_VERB nôl_ADV dros_ADP y_DET flwyddyn_NOUN mewn_ADP rhaglen_NOUN arbennig_ADJ o_ADP Heno_PROPN Nos_NOUN Galan_X ar_ADP S4C_PROPN Nos_NOUN Galan_PROPN am_ADP 10.30_NUM hyd_NOUN hanner_NOUN nos_NOUN ._PUNCT
Bydd_VERB gwesteion_NOUN arbennig_ADJ ,_PUNCT cerddoriaeth_NOUN a_CONJ llawer_PRON o_ADP hwyl_NOUN ._PUNCT
3_NUM ._PUNCT
Noson_PROPN Lawen_PROPN Robat_NOUN Arwyn_PROPN :_PUNCT Dathlu_VERB 'r_DET 60_NUM
Bydd_AUX y_DET canwr_NOUN Rhys_PROPN Meirion_PROPN yn_PART cyflwyno_VERB noson_NOUN o_ADP adloniant_NOUN ac_CONJ ambell_ADJ sypreis_NOUN i_ADP ddathlu_VERB pen_NOUN -_PUNCT blwydd_NOUN y_DET cerddor_NOUN a_CONJ 'r_DET cyfansoddwr_NOUN Robat_NOUN Arwyn_PROPN yn_PART 60_NUM ._PUNCT
HEFYD_NOUN
Pobol_ADJ y_DET Cwm_PROPN
BeBe_PRON sy_AUX 'n_PART digwydd_VERB yng_ADP Nghwmderi_PROPN ?_PUNCT
Rownd_NOUN a_CONJ Rownd_PROPN
Cyfle_NOUN i_ADP ddal_VERB i_ADP fyny_ADV â_ADP bywyd_NOUN yng_ADP Nghilbedlam_PROPN ._PUNCT
Goreuon_VERB Ryan_PROPN a_CONJ Ronnie_PROPN :_PUNCT Cymru_PROPN gyfan_ADJ ,_PUNCT gorllewin_NOUN Cymru_PROPN west_X Wales_X ._PUNCT
Heno_ADV Nos_NOUN Galan_X :_PUNCT Cymru_PROPN gyfan_ADJ
Noson_PROPN Lawen_PROPN Robat_NOUN Arwyn_PROPN :_PUNCT Dathlu_VERB 'r_DET 60_NUM :_PUNCT Gogledd_NOUN Cymru_PROPN
