[_PUNCT ]_PUNCT
Ti_PRON 'n_PART prynu_VERB un_NUM drosta'_VERBUNCT ni_PRON i_ADP gyd_ADV wyt_VERB ?_PUNCT
Diolch_INTJ yn_PART fawr_ADJ iawn_ADV ._PUNCT
<_SYM SS_X >_SYM [_PUNCT ]_PUNCT ._PUNCT
Chwara_VERB teg_ADJ i_ADP nain_NOUN am_ADP brynu_VERB un_NUM i_ADP ni_PRON ._PUNCT
<_SYM SS_X >_SYM [_PUNCT ]_PUNCT ._PUNCT
Ma_AUX 'r_DET haul_NOUN yn_PART dod_VERB allan_ADV i_ADP ni_PRON '_PUNCT wan_ADJ ._PUNCT
<_SYM S_NUM 2_NUM >_SYM Ydi_X [_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM 1_NUM >_SYM Ydi_X [_PUNCT ]_PUNCT t'isio_VERB i_ADP mi_PRON drio_VERB ffonio_VERB taid_NOUN ?_PUNCT
<_SYM S_NUM 3_NUM >_SYM [_PUNCT ]_PUNCT Dw_AUX 'm_PRON yn_PART meddwl_VERB fydd_VERB o_PRON 'n_PART ty_NOUN '_PUNCT wan_ADJ rhai_PRON '_PUNCT mi_PRON ddeud_VERB neu_CONJ ddim_PART yn_PART ty_NOUN ._PUNCT
<_SYM S_NUM 1_NUM >_SYM Be_NOUN am_ADP ty_NOUN [_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM 3_NUM >_SYM Amsar_PROPN cinio_NOUN [_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM 2_NUM >_SYM Allan_PROPN i_PART ginio_NOUN +_SYM
<_SYM S_NUM 4_NUM >_SYM BeBe_NOUN da_ADJ ni_PRON 'n_PART mynd_VERB i_ADP ennill_VERB ?_PUNCT
Can_VERB punt_NOUN gin_NOUN cangen_NOUN Llanfairpwll_PROPN hanner_NOUN can_NOUN punt_NOUN gan_ADP Llanfechell_PROPN +_SYM
Ti_PRON 'n_PART meddwl_VERB fydd_VERB taid_NOUN yn_PART ty_NOUN '_PUNCT wan_ADJ ?_PUNCT
<_SYM S_NUM 3_NUM >_SYM [_PUNCT aneglur_ADJ ]_PUNCT Llanfechell_PROPN dw_AUX i_PRON 'di_PART bo_VERB 'n_PART fan'na_PUNCT +_SYM
<_SYM S_NUM 1_NUM >_SYM [_PUNCT ]_PUNCT i_ADP ni_PRON ga'l_VERB deud_VERB bobo_VERB ni_PRON ddim_PART  _SPACE bod_VERB ddim_PART 'di_PART ca'l_VERB llwyfan_NOUN ._PUNCT
Ia_INTJ ._PUNCT
<_SYM S_NUM 2_NUM >_SYM Ia_INTJ ._PUNCT
<_SYM S_NUM 1_NUM >_SYM 'Na_ADV i_ADP drio_VERB fo_PRON eto_ADV ia_ADJ ?_PUNCT
<_SYM S_NUM 3_NUM >_SYM Beirniad_NOUN yn_PART pigo_VERB pwy_PRON o'dd_NOUN o_PRON 'n_PART nabod_VERB ia_PRON [_PUNCT ]_PUNCT C'wilydd_NOUN iddo_ADP fo_PRON de_NOUN ._PUNCT
'Di_PART ca'l_VERB cam_NOUN eto_ADV [_PUNCT ]_PUNCT +_SYM
[_PUNCT ]_PUNCT +_VERB be_ADP i_ADP 'r_DET jobyn_NOUN yna_ADV ?_PUNCT
<_SYM S_NUM 1_NUM >_SYM '_PUNCT da_ADJ ni_PRON di_PRON +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Recordio_VERB '_PUNCT da_ADJ ni_PRON ?_PUNCT
O_ADP [_PUNCT =_SYM ]_PUNCT siarad_VERB Cymraeg_PROPN siarad_VERB Cymraeg_PROPN [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
<_SYM S_NUM 1_NUM >_SYM Nawn_NOUN ni_PRON drio_VERB taid_NOUN eto_ADV '_PUNCT ta_X ia_PRON ?_PUNCT
D'on_NOUN i_PRON ddim_PART yn_PART dallt_VERB hynny_PRON [_PUNCT ]_PUNCT '_PUNCT nes_CONJ di_PRON adrodd_VERB yn_PART wych_ADJ yn_ADP y_DET rhagbrawf_NOUN ._PUNCT
Do_INTJ ._PUNCT
<_SYM S_NUM 4_NUM >_SYM [_PUNCT ]_PUNCT bobo_VERB chdi_NOUN ddim_PART di_PRON bod_VERB yn_PART llwyddianus_ADJ ._PUNCT
Ia_INTJ [_PUNCT ]_PUNCT y_DET bobol_NOUN cyfryngaga_NOUN 'na_ADV de_NOUN [_PUNCT ]_PUNCT edrych_VERB ar_ADP ol_NOUN 'i_PRON gilydd_NOUN ._PUNCT
Pa_PART gylch_NOUN fasach_NOUN chi_PRON wan_ADJ hefo_ADP 'r_DET pwytho_NOUN ?_PUNCT
O_ADP ia_PRON ,_PUNCT dwi_AUX 'm_DET yn_PART cymryd_VERB rhan_NOUN dwi_AUX 'm_DET yn_PART gallu_ADV g'neud_VERB gwaith_NOUN llaw_NOUN [_PUNCT ]_PUNCT +_SYM
Ma_INTJ '_PUNCT Dwyfor_PROPN yna_ADV a_CONJ Meirionnydd_PROPN Aberconwy_X be_DET fasach_NOUN chi_PRON ?_PUNCT
O_ADP haia_NOUN taid_NOUN '_PUNCT dych_VERB chi_PRON 'n_PART iawn_ADJ ?_PUNCT
Jyst_ADV gada'l_VERB i_ADP ti_PRON w'bod_VERB bod_AUX ni_PRON yma_ADV yn_ADP yr_DET haul_NOUN ond_CONJ 'di_PART g'neud_VERB yn_PART dda_ADJ ond_CONJ dim_DET llwyfan_NOUN chwaith_ADV naddo_VERB [_PUNCT =_SYM ]_PUNCT 'di_PART ca'l_VERB cam_NOUN 'di_PART ca'l_VERB cam_NOUN 'di_PART ca'l_VERB cam_NOUN [_PUNCT /=_PROPN ]_PUNCT ond_CONJ 'di_PART neud_VERB yn_PART andros_ADJ o_ADP dda'_ADJUNCT fyd_NOUN ond_ADP  _SPACE na_CONJ dim_DET llwyfan_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ma_INTJ '_PUNCT 'na_ADV enwa'_VERBUNCT gwahanol_ADJ ar_ADP hwn_PRON '_PUNCT does_VERB +_SYM
[_PUNCT =_SYM ]_PUNCT Dim_DET otsh_NOUN dim_DET otsh_NOUN [_PUNCT /=_PROPN ]_PUNCT '_PUNCT dy_DET chi_NOUN 'n_PART iawn_ADJ acw_ADV ?_PUNCT
Cacan_VERB gri_NOUN '_PUNCT dy_DET ni_PRON 'n_PART ddeud_VERB ynde_ADP ._PUNCT
'_PUNCT Nes_ADV i_PRON drio_VERB chi_PRON cynt_NOUN ._PUNCT
[_PUNCT ]_PUNCT enw_NOUN arall_ADJ pice_NOUN ar_ADP y_DET maen_NOUN ma'rhein_PRON yn_PART ddeud_VERB de_NOUN ._PUNCT
O_ADP reit_ADV ydi_ADV enwg_VERB 'n_PART byhafio_VERB ?_PUNCT
Cacan_VERB gri_NOUN ._PUNCT
Cacan_VERB gri_ADP +_SYM
Dych_AUX chi_PRON 'n_PART byhafio_VERB ?_PUNCT
Ia_INTJ +_SYM
B_NUM '_PUNCT yta_NOUN 'n_PART dda_ADJ o_ADP reit_NOUN dda_ADJ ._PUNCT
Pice_NOUN ar_ADP y_DET maen_NOUN +_SYM
<_SYM S_NUM 1_NUM >_SYM i_ADP chi_PRON ga'l_ADV mynd_VERB a_CONJ fo_VERB am_ADP spin_NOUN lawr_ADV [_PUNCT ]_PUNCT bell_NOUN o'rwrth_NOUN o_ADP fo_PRON ._PUNCT
<_SYM S_NUM 4_NUM >_SYM '_PUNCT On_PRON i_PRON 'n_PART meddwl_VERB mama_NOUN pice_NOUN ar_ADP y_DET maen_NOUN o'dd_NOUN  _SPACE [_PUNCT ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ia_INTJ na_CONJ '_PUNCT dych_VERB chi_PRON 'n_PART iawn_ADJ '_PUNCT lly_X '_PUNCT dach_VERB ?_PUNCT +_SYM
'Di_PART o_ADP ddim_PART yn_PART llwgu_ADJ ?_PUNCT
+_VERB S1_NOUN [_PUNCT =_SYM ]_PUNCT hiraeth_NOUN hiraeth_NOUN [_PUNCT /=_PROPN ]_PUNCT amdana_ADP '_PUNCT chdi_NOUN mam_NOUN ._PUNCT
[_PUNCT ]_PUNCT ._PUNCT
'_PUNCT Dych_NOUN chi_PRON 'di_PART g'neud_VERB bwyd_NOUN ?_PUNCT
'_PUNCT Dych_NOUN chi_PRON 'di_PART llosgi_VERB pitsa'_NOUNUNCT ta'_NOUNUNCT dych_VERB chi_PRON 'n_PART iawn_ADJ ?_PUNCT
Ddaru_VERB chi_PRON 'm_PART neud_VERB y_DET pitsa_NOUN +_SYM
O_ADP naddo_VERB ma_PRON 'n_PART ormod_NOUN o_ADP draffarth_NOUN ma_VERB 'n_PART siŵr_ADJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM +_SYM S1_NOUN '_PUNCT Dy_DET ni_PRON 'n_PART mynd_VERB i_ADP [_PUNCT ]_PUNCT mae_VERB  _SPACE gweld_VERB drama_NOUN [_PUNCT ]_PUNCT o_ADP [_PUNCT =_SYM ]_PUNCT '_PUNCT dych_VERB chi_PRON '_PUNCT dych_VERB chi_PRON [_PUNCT /=_PROPN ]_PUNCT <_SYM en_PRON >_SYM champion_VERB '_PUNCT lly_X +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
+_VERB S1_NOUN '_PUNCT o_PRON 'n_PART i_PRON jyst_ADV isio_VERB gada'l_PROPN i_ADP ti_PRON w'bod_VERB rhag_ADP ofn_NOUN bobo_VERB chi_PRON 'n_PART rhyw_DET hofran_NOUN [_PUNCT ]_PUNCT '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB i_PART weld_VERB ym_ADP mama_NOUN enwg2_PUNCT hogyn_NOUN enwg3_NOUN yn_ADP y_DET rhagbrawf_NOUN o_ADP gwmpas_NOUN y_DET dau_NUM 'ma_ADV so_VERB '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB i_ADP '_PUNCT fan'no_PUNCT i_ADP gl'wad_NOUN fo_PRON a_CONJ fydd_AUX o_PRON ddim_PART yn_PART ca'l_VERB gw'bod_VERB am_ADP yn_PART hir_ADJ '_PUNCT dydi_AUX 'i_PRON gystadleua_VERB 'th_NOUN o_ADP ddim_PRON ar_ADP y_DET llwyfan_NOUN tan_ADP saith_NUM heno_ADV '_PUNCT ballu_VERB [_PUNCT ]_PUNCT ok_X haia_NOUN __NOUN ti_PRON 'n_PART iawn_ADJ dyn_NOUN ?_PUNCT
Pwy_PRON sy_VERB 'na_ADV ?_PUNCT
Dy_DET fam_NOUN sy_VERB 'ma_ADV +_SYM
[_PUNCT chwerthin_VERB ]_PUNCT Pwy_PRON sy_VERB 'na_ADV ._PUNCT
+_VERB S1_NOUN O't_AUX ti_PRON 'n_PART meddwl_VERB mama_NOUN nain_NOUN o'dd_NOUN 'na_ADV gwranda_NOUN ti_PRON 'n_PART cofio_VERB ?_PUNCT +_SYM
'Di_PART o_ADP 'di_PART ca'l_VERB cinio_NOUN gofyn_NOUN iddo_ADP fo_PRON ydi_VERB taid_NOUN yn_PART lwgu_VERB fo_PRON ?_PUNCT
<_SYM S_NUM 1_NUM >_SYM Be_INTJ ?_PUNCT
Sori_VERB be_PART dd'udist_VERB di_PRON enwg_VERB ok_X Ydi_X enwg_VERB byhafio_VERB ?_PUNCT
O_ADP 'n_PART i_PRON 'n_PART deud_VERB fod_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
289.168_NOUN
Dydi_VERB 'r_DET beirniad_NOUN ddim_PART yn_PART gallu_VERB cl'wad_NOUN gutted_VERB enwb_NOUN '_PUNCT dydyn_AUX nhw_PRON ddim_PART yn_PART gw'bod_VERB bebe_NOUN 'di_PART adrodd_VERB ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
'_PUNCT Aru_PROPN mi_PRON dd'eud_VERB y_DET gwir'lly_NOUN do_INTJ ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_PART enwb_NOUN yn_PART deud_VERB [_PUNCT ]_PUNCT dda_ADJ i_ADP fynd_VERB yno_ADV ._PUNCT
O_ADP 'na_ADV fo_PRON dw_AUX i_ADP 'm_DET yn_PART cl'wad_NOUN chi_PRON 'n_PART dda_ADJ iawn_ADV boi_NOUN dw_AUX i_PRON 'n_PART mynd_VERB '_PUNCT wan_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT +_SYM
+_VERB S1_NOUN Steddfod_X '_PUNCT da_ADJ ni_PRON ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT gwithio_VERB yno_ADV '_PUNCT fyd_NOUN 'di_PART hi_PRON 'n_PART gw_NOUN '_PUNCT thio_VERB '_PUNCT no_PRON ?_PUNCT
[_PUNCT ]_PUNCT dad_NOUN yn_PART gofyn_VERB [_PUNCT saib_NOUN ]_PUNCT sori_VERB dw_AUX i_ADP 'm_DET yn_PART cl'wad_NOUN [_PUNCT =_SYM ]_PUNCT deud_VERB eto_ADV deud_VERB eto_ADV [_PUNCT /=_PROPN ]_PUNCT [_PUNCT =_SYM ]_PUNCT ia_X ia_X ia_X [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
O_ADP 's_X 'na_ADV r'ywun_VERB o_ADP ardal_NOUN y_DET gogledd_NOUN 'di_PART ennill_VERB +_SYM
Ia_INTJ 'na_ADV ti_PRON ia_PRON mae_AUX enwb_NOUN yn_PART gwrando_VERB 'n_PART well_ADJ na_CONJ enwg_VERB 'di_PART hynny_PRON 'm_PART yn_PART plesio_VERB '_PUNCT lly_X na_CONJ 'di_PART ?_PUNCT
O_ADP wel_NOUN ok_X hei_PRON dw_AUX i_PRON 'n_PART mynd_VERB '_PUNCT wan_ADJ '_PUNCT ta_CONJ wela_VERB i_ADP di_PRON boi_NOUN cym'bwyll_NOUN ta_CONJ ra_NOUN cariad_NOUN ta_CONJ ra_NOUN caru_VERB ti_PRON [_PUNCT =_SYM ]_PUNCT ta_CONJ ra_NOUN ta_X ra_NOUN [_PUNCT /=_PROPN ]_PUNCT boi_NOUN tara_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
y_DET ?_PUNCT
Ma'_VERBUNCT cwn_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT O_ADP enwb_NOUN 'di_PART ca'l_VERB hefo_ADP unawd_NOUN piano_NOUN ._PUNCT
BeBe_VERB llwyfan_NOUN ?_PUNCT
Ma_VERB 'n_PART amazing_NOUN '_PUNCT dydi_PRON ?_PUNCT
Lle_ADV mama_VERB hi_PRON 'n_PART byw_VERB ?_PUNCT
lleoliad_NOUN ?_PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT gweld_VERB yr_DET enwg_VERB o_ADP 'n_PART i_PRON '_PUNCT lly_X ._PUNCT
Mae_VERB y_DET [_PUNCT aneglur_ADJ ]_PUNCT eto_ADV fwy_ADJ na_CONJ neb_PRON arall_ADJ ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT enwb_VERB cyfenw_NOUN o_ADP adran_NOUN __NOUN 'di_PART ca'l_VERB llwyfan_NOUN efo_ADP unawd_NOUN merched_NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Na_VERB ma_PRON 'n_PART nhw_PRON 'n_PART ddiarth_NOUN i_ADP mi_PRON '_PUNCT lly_X ._PUNCT
<_SYM S_NUM 4_NUM >_SYM Ydi_X 'Di_PART 'r_DET sioe_NOUN gerdd_NOUN yna_ADV adag_CONJ yna_ADV ma_PART enwg_VERB meddat_NOUN ti'de_NOUN [_PUNCT aneglur_ADJ ]_PUNCT dyna_DET o_ADP 'n_PART i_PRON 'n_PART feddwl_VERB ._PUNCT
<_SYM S_NUM 1_NUM >_SYM [_PUNCT =_SYM ]_PUNCT Be_INTJ 'di_PART be_ADP 'di_PART [_PUNCT /=_PROPN ]_PUNCT dalgylch_NOUN chi_PRON p'nawn_VERB 'ma_ADV ?_PUNCT
'_PUNCT Dych_VERB chi_PRON ddim_PART yn_PART Ceredigion_PROPN Colwyn_PROPN nac_CONJ '_PUNCT dach_VERB ?_PUNCT
BeBe_ADV dach_VERB chi_PRON ?_PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ddim_PART yn_PART Colwyn_PROPN na'sa_NOUN ._PUNCT
Colwyn_INTJ ?_PUNCT
Ma_INTJ '_PUNCT Colwyn_NOUN yn_PART fan'na_PUNCT [_PUNCT aneglur_ADJ ]_PUNCT yr_DET ail_ADJ o_ADP 'r_DET pen_NOUN ._PUNCT
O_ADP ia_PRON +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT '_PUNCT im_NUM byd_NOUN arall_ADJ ._PUNCT
Conwy_VERB fan'na_SYM ._PUNCT
'_PUNCT Dych_VERB chi_PRON 'm_DET yn_ADP Aberconwy_PROPN ?_PUNCT
Arfon_PROPN ?_PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ond_CONJ ti_PRON 'm_DET yn_PART sylweddoli_VERB mor_ADV Saesneg_PROPN ydi_VERB o_ADP nes_CONJ ti_PRON 'n_PART mynd_VERB i_ADP 'r_DET pwyllgora_NOUN 'ma_ADV de_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
O_ADP ia_PRON 'di_PART o_ADP ddim_PART yn_PART sefydliad_NOUN ni_PRON yn_PART lleoliad_NOUN yn_PART hollol_ADJ Gymraeg_PROPN yn_PART '_PUNCT dydi_VERB '_PUNCT dyn_NOUN ti_PRON 'n_PART yn_PART meddwl_VERB nag_CONJ wyt_VERB nes_CONJ ti_PRON 'n_PART mynd_VERB i_ADP 'r_DET [_NOUN ]_PUNCT a_CONJ '_PUNCT dyn_NOUN o_ADP 'n_PART i_ADP lawr_ADV yn_PART __NOUN 'na_ADV 'n_PRON '_PUNCT do_PRON 'n_PART yn_ADP y_DET pwyllgor_NOUN 'na_ADV o_ADP oedd_VERB __NOUN __NOUN __NOUN a_CONJ fi_PRON [_PUNCT ]_PUNCT cinio_NOUN am_ADP ddim_PRON dwi_AUX 'm_DET yn_PART dod_VERB efo_ADP ti_PRON eto_ADV meddai'tha_VERB i_PRON [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Naddo_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM enwb_NOUN yn_PART deud_VERB bod_VERB y_DET beirniad_NOUN methu_VERB cl'wad_NOUN +_SYM
Reit_INTJ +_SYM
<_SYM S_NUM ?_PUNCT >_SYM enwb_NOUN yn_PART deud_VERB gutted_AUX  _SPACE enwb_NOUN yn_PART deud_VERB di_PRON o_ADP 'm_DET otsh_NOUN [_PUNCT aneglur_ADJ ]_PUNCT 'di_PART g'neud_VERB yn_PART dda_ADJ i_ADP fynd_VERB yna_ADV a_CONJ __NOUN yn_PART deud_VERB '_PUNCT dy_PRON 'n_PART 'w_PRON 'm_DET yn_PART gw'bod_VERB be_ADP 'di_PART [_PUNCT ]_PUNCT ._PUNCT
O_ADP reit_NOUN dda_ADJ [_PUNCT aneglur_ADJ ]_PUNCT da_ADJ de_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT 2_NUM >_SYM enwg_VERB +_SYM
enwg_VERB +_SYM
[_PUNCT =_SYM ]_PUNCT enwg_VERB enwg_VERB [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
<_SYM SS_X >_SYM [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ti_PRON 'n_PART dreifio_VERB lawr_ADV [_PUNCT ]_PUNCT be_ADP o'dd_NOUN o_PRON ._PUNCT
Tractor_NOUN [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ia_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT felly_CONJ '_PUNCT doedd_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM ?_PUNCT S_NUM 2_NUM >_SYM [_PUNCT ]_PUNCT gweithio_VERB 'n_PART dda_ADJ hefo_ADP 'i_PRON gilydd_NOUN '_PUNCT wan_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM SS_X >_SYM Ia_INTJ ._PUNCT
<_SYM S_NUM 1_NUM >_SYM A_CONJ ma_VERB 'n_PART gorwadd_ADJ ar_ADP [_PUNCT ]_PUNCT '_PUNCT wan_ADJ dydi_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT fwy_ADJ '_PUNCT wan_ADJ '_PUNCT dydyn_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
A_PART mae_VERB 'u_PRON traed_VERB nhw_PRON 'n_PART '_PUNCT lyb_NOUN o_ADP hyd_NOUN mewn_ADP ffor_NOUN '_PUNCT '_PUNCT dydi_PRON ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT ]_PUNCT dim_DET dafad_NOUN Gymraeg_PROPN ydi_ADV nace_NOUN ?_PUNCT
Ti_PRON 'di_PART ca'l_VERB g'neud_VERB [_PUNCT ]_PUNCT dim_PART llawar_NOUN o_ADP hwyl_NOUN ar_ADP [_PUNCT aneglur_ADJ ]_PUNCT na_CONJ naddo_VERB ?_PUNCT
O_ADP naddo_VERB lot_PRON o_ADP draffarth_NOUN hefo_ADP nhw_PRON braidd_ADV ._PUNCT
<_SYM S_NUM 3_NUM >_SYM Ma'gunnyn_NOUN nhw_PRON bump_ADJ ar_ADP werth_NOUN yn_ADP Dinbach_PROPN [_PUNCT ]_PUNCT +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
O_ADP reit_NOUN ._PUNCT
Ma_VERB 'r_DET wyn_NOUN bach_ADJ ti_PRON 'di_PART ga'l_VERB yn_PART lovely_NOUN ._PUNCT
Welis_VERB i_ADP nhw_PRON 'n_ADP Sbaen_PROPN '_PUNCT lwch_NOUN ._PUNCT
O'ddan_ADP nhw_PRON 'n_PART ddu_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Bora_VERB ddoe_ADV ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT smala_ADV mor_ADV ddu_ADJ hefo_ADP 'r_DET [_NOUN aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
O_ADP na_PART ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM 1_NUM >_SYM Dwy_NUM fel_ADP '_PUNCT a_CONJ sy_VERB 'na_ADV ?_PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT cadw_VERB un_NUM i_ADP 'r_DET ochor_NOUN ._PUNCT
<_SYM ?_PUNCT S_NUM 2_NUM >_SYM [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Be_INTJ ?_PUNCT
<_SYM ?_PUNCT S_NUM 2_NUM >_SYM [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT o_ADP 'n_PART ni_PRON 'n_PART dau_NUM +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT dechra_VERB rheini_NOUN ._PUNCT
Dechra_NOUN dod_VERB '_PUNCT wan_ADJ dydi_NOUN ._PUNCT
Ma_VERB 'n_PART amazing_NOUN bod_AUX nhw_PRON 'n_PART yn_PART +_SYM
Mor_ADV ddu_ADJ bitsh_NOUN ._PUNCT
Welis_VERB i_ADP nhw_PRON fu'st_NOUN di_PRON yn_PART [_PUNCT ]_PUNCT ne_NOUN '_PUNCT r'wbath_NOUN [_PUNCT ]_PUNCT o_ADP 'n_PART ni_PRON 'n_PART ca'l_VERB cramp_NOUN yn_PART d'on_NOUN yn_ADP y_DET nghoesa'_VERBUNCT fyd_NOUN [_PUNCT ]_PUNCT isda_NOUN side_VERB saddle_NOUN i_PART ddod_VERB yn_PART dol_ADJ o'dd_NOUN o_PRON 'n_PART deud_VERB arna_ADP i_PRON ._PUNCT
'_PUNCT Wan_PROPN dw_AUX i_PRON 'di_PART sylweddoli_VERB mama_NOUN anon_CONJ enwg_VERB <_SYM /_PUNCT anon_VERB >_SYM [_PUNCT ]_PUNCT 'di_PART o_ADP 'm_DET yn_PART canfasio_VERB i_ADP 'w_PRON lecsiwn_NOUN [_PUNCT ]_PUNCT trio_ADV mynd_VERB i_ADP fewn_ADP i_ADP 'r_DET Blaid_NOUN rwan_PRON i_ADP San_NOUN Steffan_PROPN de_NOUN ._PUNCT
Pwy_PRON ?_PUNCT
[_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM 3_NUM >_SYM O_ADP yndi_VERB ?_PUNCT
751.448_NOUN
O_ADP 'n_PART i_PRON 'n_PART d'eud_VERB wrth_ADP mam_NOUN [_PUNCT ]_PUNCT bobo_VERB ni_PRON 'm_DET yn_PART dalld_ADJ ._PUNCT
<_SYM S_NUM 4_NUM >_SYM Ti_PRON 'm_DET yn_PART dalld_ADJ '_PUNCT yst_NOUN 'i_PRON [_PUNCT ]_PUNCT bebe_NOUN o'dd_NOUN o_PRON 'n_PART holi_VERB a_CONJ bebe_NOUN o'ddan_ADP nhw_PRON di_PRON roi_VERB [_PUNCT aneglur_ADJ ]_PUNCT yn_PART lleoliad_NOUN de_NOUN ._PUNCT
'_PUNCT Sa_PRON neb_PRON yn_PART gw'bod_VERB lle_ADV ma_AUX 'r_DET arian_NOUN yn_PART mynd_VERB [_PUNCT ]_PUNCT ma_AUX 'r_DET dyn_NOUN claddu_VERB a_CONJ pryd_ADV o'dd_NOUN angladd_NOUN y_DET boi_NOUN 'na_ADV [_PUNCT ]_PUNCT i_ADP aildoi_VERB a_CONJ bob_DET peth_NOUN de_NOUN ._PUNCT
'_PUNCT Neud_VERB gwaith_NOUN di_PRON 'r_DET syniad_NOUN yn_ADP yr_DET uneda'_NOUNUNCT 'ma_ADV ._PUNCT
<_SYM SB_SYM >_SYM Wedyn_X mama_NOUN 'na_ADV helynt_NOUN 'di_PART bod_VERB [_PUNCT ]_PUNCT ._PUNCT
<_SYM ?_PUNCT S_NUM 4_NUM >_SYM O_ADP 'n_PART i_PRON isio_VERB gw'bod_NOUN pam_ADV [_PUNCT ]_PUNCT i_ADP be_DET mama_NOUN nhw_PRON 'n_PART rhoid_VERB yr_DET arian_NOUN [_PUNCT ]_PUNCT un_NUM person_NOUN sy_AUX 'n_PART ca'l_VERB yr_DET arian_NOUN 'na_ADV i_ADP gyd_ADP ynde_VERB i_PRON ailneud_VERB y_DET toi_NOUN [_PUNCT ]_PUNCT ._PUNCT Ma_X '_PUNCT 'na_ADV lot_NOUN fwy_ADV [_PUNCT ]_PUNCT ti_PRON 'm_DET yn_PART dallt_VERB '_PUNCT ysti_CONJ ma_PRON 'n_PART gymhleth_ADJ [_PUNCT ]_PUNCT dw_AUX i_PRON 'n_PART dallt_VERB yn_PART iawn_ADJ chdi_NOUN sy_AUX 'm_DET yn_PART atab_VERB fy_DET nghwestiyna'_VERBUNCT i_PRON 'n_PART blaen_NOUN ag_CONJ yn_PART diwadd_VERB o_ADP 'n_PART i_PRON 'n_PART cau_VERB sefyll_VERB lawr_ADV na_CONJ fo_PRON [_PUNCT ]_PUNCT rhai_PRON '_PUNCT ni_PRON ada'l_VERB o_ADP 'n_PART fan_NOUN '_PUNCT 'na_ADV rwan_PRON a_PART ges_VERB i_ADP 'm_DET atab_VERB ._PUNCT
A_CONJ '_PUNCT nes_CONJ i_PART ofyn_VERB cwestiwn_NOUN arall_ADJ '_PUNCT fyd_NOUN lle_NOUN ma_VERB y_DET llefydd_NOUN lle_NOUN '_PUNCT na'thon_VERB nhw_PRON wrthod_VERB i_PART enwg_VERB fuldio_VERB ty_NOUN '_PUNCT ndo_NOUN ar_ADP y_DET [_PUNCT ]_PUNCT lle_NOUN '_PUNCT da_ADJ ni_PRON [_PUNCT ]_PUNCT fod_AUX yn_PART affordable_VERB housing_NOUN [_PUNCT ]_PUNCT sy_VERB 'di_PART fagu_VERB yn_ADP y_DET pentra_NOUN ar_ADP hyd_NOUN 'i_PRON oes_VERB ._PUNCT
Os_CONJ gynno_VERB fo_PRON 'r_DET pres_NOUN i_PART neud_VERB o_ADP gada'l_NOUN iddo_ADP neud_VERB o_ADP de_NOUN ._PUNCT
Wel_INTJ ia_PRON ond_CONJ pam_ADV fod_AUX yn_PART rhaid_VERB iddo_ADP fo_PRON fod_VERB yn_PART [_PUNCT ]_PUNCT hundred_NOUN and_ADJ twenty_NOUN thousand_NOUN ty_NOUN [_PUNCT aneglur_ADJ ]_PUNCT rhy_ADV fawr_ADJ '_PUNCT sa_PRON 'n_PART edrych_VERB yn_PART wirion_ADJ bysa_VERB ?_PUNCT
'_PUNCT Ysti_ADP mama_NOUN 'di_PART mynd_VERB i_ADP fyw_VERB i_PRON [_PUNCT ]_PUNCT rwan_PRON a_CONJ 'di_PART colli_VERB dau_NUM o_ADP blant_NOUN bach_ADJ i_ADP [_PUNCT ]_PUNCT de_NOUN ._PUNCT
<_SYM S_NUM 1_NUM >_SYM Ma_X '_PUNCT '_PUNCT heina_VERB i_ADP 'w_PRON gweld_VERB 'di_PART [_PUNCT ]_PUNCT '_PUNCT wan_ADJ '_PUNCT dydyn_VERB ._PUNCT
Wel_CONJ do'dd_NOUN o_ADP ddim_PRON [_PUNCT ]_PUNCT drio_VERB o_ADP ddifri_NOUN de_NOUN ._PUNCT
BeBe_ADV ma_PRON 'n_PART nhw_PRON 'n_PART dd'eud_VERB 'di_PART hynny_PRON ?_PUNCT
Wedyn'nes_VERB i_PRON ofyn_VERB '_PUNCT ddyn_NOUN nhw_DET bebe_NOUN ydi_VERB 'r_DET llefydd_NOUN fasa_NOUN 'r_DET cyngor_NOUN [_PUNCT ]_PUNCT atab_VERB iddyn_ADP nhw_PRON '_PUNCT nes_CONJ i_PART ofyn_VERB wedyn_ADV faswn_VERB i_ADP 'n_PART licio_VERB i_ADP chi_PRON ffindio_VERB allan_ADV a_CONJ dod_VERB yn_PART dol_ADJ ataf_ADP fi_PRON i_PART dd'eud_VERB de_CONJ ges_VERB i_ADP byth_ADV atab_VERB ._PUNCT
Ma_VERB 'n_PART siŵr_ADJ mama_NOUN [_PUNCT ]_PUNCT ond_CONJ ges_VERB i_ADP byth_ADV atab_VERB ._PUNCT
enwg_VERB isio_ADV codi_VERB ty_ADP yn_PART lleoliad_NOUN de_NOUN [_PUNCT ]_PUNCT planning_DET eco_NOUN ty_CONJ eco_NOUN 'ma_ADV +_SYM
O_ADP ia_PRON ._PUNCT
A_CONJ mama_NOUN hynna_PRON [_PUNCT ]_PUNCT siap_NOUN gwahanol_ADJ '_PUNCT wan_ADJ [_PUNCT ]_PUNCT ._PUNCT
Ma'_VERBUNCT nhw_PRON 'n_PART amddifadu_VERB +_SYM
[_PUNCT ]_PUNCT ._PUNCT
Ma'_VERBUNCT nhw_PRON 'n_PART amddifadu_VERB cefn_NOUN gwlad_NOUN wedyn'sti_ADP [_PUNCT ]_PUNCT 'di_PART o_ADP 'm_DET fatha_NOUN bod_AUX bobol_NOUN yn_PART trio_ADV dod_VERB o_ADP tu_NOUN allan_ADJ na_CONJ chdi_NOUN '_PUNCT sti_NOUN [_PUNCT ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT ]_PUNCT 'di_PART magu_VERB a_CONJ 'di_PART tyfu_VERB fyny_ADV [_PUNCT ]_PUNCT yndydi_VERB '_PUNCT sti_NOUN ._PUNCT
<_SYM S_NUM 3_NUM >_SYM I_ADP mi_PRON [_PUNCT ]_PUNCT be_ADP licith_NOUN hi_PRON [_PUNCT ]_PUNCT o_ADP fewn_ADP rheswm_NOUN ._PUNCT
Aros_VERB [_PUNCT ]_PUNCT de_NOUN ._PUNCT
<_SYM S_NUM 3_NUM >_SYM I_NUM bebe_NOUN ma_VERB 'i_PRON 'n_PART da_ADJ aros_VERB yma_ADV ._PUNCT
<_SYM S_NUM 4_NUM >_SYM Ia_X '_PUNCT sa_AUX ti_PRON 'n_PART meddwl_VERB fod_AUX nhw_PRON 'n_PART g'neud_VERB  _SPACE efo_ADP adnodda_VERB [_PUNCT ]_PUNCT fasa_VERB fo_PRON +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Wel_INTJ ia_PRON dyna_DET faswn_VERB i_ADP 'n_PART feddwl_VERB ._PUNCT
<_SYM S_NUM 1_NUM >_SYM Wel_X '_PUNCT dy_DET '_PUNCT ni_PRON am_ADP dal_VERB i_ADP fynd_VERB rownd_ADV ?_PUNCT
Wel_INTJ ia_PRON ._PUNCT
<_SYM S_NUM 1_NUM >_SYM [_PUNCT ]_PUNCT 'di_PART blino_VERB rwan_ADV ._PUNCT
<_SYM SS_X >_SYM [_PUNCT ]_PUNCT
Dal_VERB i_ADP fyny_ADV yn_ADP y_DET diwadd_VERB ti_PRON isio_VERB nap_CONJ '_PUNCT wan_ADJ ._PUNCT
<_SYM S_NUM 1_NUM >_SYM [_PUNCT ]_PUNCT ._PUNCT
Tua_ADP pryd_NOUN nawn_NOUN ni_PART gyrra'dd_NOUN adra_ADV ?_PUNCT
<_SYM S_NUM 1_NUM >_SYM Dw_VERB 'm_PRON '_PUNCT bobo_VERB ._PUNCT
<_SYM S_NUM 3_NUM >_SYM Fedran_PROPN ni_PRON fynd_VERB ar_ADP ol_NOUN yr_DET eitam_NOUN [_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT 4_NUM >_SYM Fyddan_PROPN ni_PRON adra_ADV tua_ADP chwech_NUM +_SYM
[_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM 1_NUM >_SYM [_PUNCT ]_PUNCT fel_ADP dd'udodd_VERB 'i_PRON fam_NOUN o_ADP [_PUNCT ]_PUNCT hwnna_PRON o'dd_NOUN y_DET darn_NOUN wanna_VERB o'dd_NOUN gynno_VERB fo_PRON +_SYM
[_PUNCT ]_PUNCT Hwnna_X 'ma_ADV 'di_PART mynd_VERB trwadd_NOUN hefo_ADP fo_PRON ._PUNCT
<_SYM S_NUM 1_NUM >_SYM Hwnna_X mama_NOUN 'di_PART mynd_VERB trwadd_NOUN hefo_ADP fo_PRON +_SYM
<_SYM S_NUM ?_PUNCT 2_NUM >_SYM [_PUNCT ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT feirniad_NOUN [_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM 4_NUM >_SYM Ia_NOUN o_ADP 'r_DET academis_NOUN mama_NOUN lot_PRON ohonyn_ADP nhw_DET ne_NOUN '_PUNCT ym_ADP ._PUNCT
<_SYM S_NUM 5_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
G'neud_VERB yn_PART dda_ADJ iawn_ADV ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART meddwl_VERB fod_VERB gynni_VERB gyfla_VERB da_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ni_PRON 'di_PART g'rando_NOUN ._PUNCT
Positif_NOUN ond_CONJ o'dd_NOUN 'na_ADV fwy_PRON o'nyn_ADP nhw_PRON 'n_PART cyferbynnu_VERB hollol_ADV '_PUNCT d'oedd_NOUN ._PUNCT
Oedd_VERB +_SYM
O_ADP un_NUM i_ADP llall_PRON i_ADP be_PRON o_ADP sy_VERB 'di_PART mynd_VERB ar_ADP y_DET llwyfan_NOUN de_NOUN ._PUNCT
[_PUNCT ]_PUNCT y_DET beirniad_NOUN ._PUNCT
<_SYM SS_X >_SYM [_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
<_SYM ?_PUNCT S_NUM 5_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT [_PUNCT =_SYM ]_PUNCT o'dd_NOUN o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT o_ADP 'n_PART gystadleuaeth_NOUN anodd_ADJ i_PART ferniadu_VERB '_PUNCT swn_NOUN i_PRON 'n_PART feddwl_VERB +_SYM
Oedd_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM 5_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
Yr_DET hogan_NOUN [_PUNCT aneglur_ADJ ]_PUNCT 'na_ADV o'dd_VERB hi_PRON tu_NOUN allan_ADJ i_ADP Gymru_PROPN 'n_PART '_PUNCT doedd_VERB enwb_NOUN ne_PRON '_PUNCT r'wbath_NOUN o'dd_NOUN hi_PRON o'ddd_VERB hwnnw_PRON mor_ADV wahanol_ADJ ag_ADP o'dd_NOUN hin_NOUN ola'_NOUNUNCT hefyd_ADV de_NOUN mama_NOUN hynny_PRON 'n_PART fanta_VERB 's_PROPN w'ithia'_VERBUNCT ._PUNCT
[_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
'_PUNCT Dydi_VERB o_ADP ddim_PART ._PUNCT
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
Ia_INTJ +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT newid_VERB dy_DET feddwl_NOUN di_PRON '_PUNCT lly_X ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Na_INTJ do't_VERB ti_PRON ddim_PART yn_PART gynta'_ADJUNCT nag_CONJ o't_VERB na_CONJ yn_PART union_ADJ ._PUNCT
Na_INTJ '_PUNCT nath_NOUN hi_PRON neud_VERB yn_PART dda_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT 'di_PART blino_VERB '_PUNCT wan_ADJ do_PRON ?_PUNCT
Dwi_AUX 'di_PART blino_VERB '_PUNCT wan_ADJ fy_DET hun_PRON ma'isio_VERB nap_PRON ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT
Gathoch_VERB chi_PRON siwrna_VERB i_ADP lawr_ADV yn_PART iawn_ADJ
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
O_ADP naddo_VERB ?_PUNCT
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM 1_NUM >_SYM O_ADP ia_PRON ?_PUNCT
O'ddan_ADP ni_PRON yn_PART lleoliad_NOUN o'ddan_ADP ni_PRON 'n_PART aros_VERB o'dd_NOUN o_ADP reit_NOUN yn_PART ymyl_NOUN '_PUNCT doedd_VERB o'ddan_ADP ni_PRON '_PUNCT mond_ADV [_PUNCT ]_PUNCT chwartar_NOUN awr_NOUN ugian_NOUN munud_NOUN oeddan_ADP ni_PRON [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Dim_DET byd_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
O'ddan_ADP ni_PRON 'ma_ADV [_PUNCT ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT +_SYM
[_PUNCT aneglur_ADJ ]_PUNCT i_PART roid_VERB o_ADP 'n_PART ychwanegol_ADJ ylly_ADV ._PUNCT
Fyddan_VERB ni_PRON jyst_ADV rhyw_DET [_PUNCT __NOUN ]_PUNCT '_PUNCT dy_PRON ni'sio_VERB gweld_VERB enwg_VERB '_PUNCT does_NOUN wedyn_ADV '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB +_SYM
Ia_INTJ ._PUNCT
Ma'_VERBUNCT nhw_PRON ni_DET cyrra'dd_NOUN '_PUNCT fyd_NOUN medda_VERB chdi_NOUN '_PUNCT ndyn_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Fy_INTJ '_PUNCT raid_VERB i_ADP ni_PRON ga'l_VERB swpar_NOUN ar_ADP y_DET ffor_NOUN '_PUNCT adra_ADV bydd_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
O_ADP na_PART ni_PRON ._PUNCT
Gatho_VERB '_PUNCT ni_PRON swpar_NOUN neis_ADJ n'ithiwr_NOUN '_PUNCT fyd_NOUN [_PUNCT ]_PUNCT [_PUNCT __NOUN ]_PUNCT ._PUNCT
[_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
Ia_INTJ [_PUNCT ]_PUNCT ._PUNCT
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
Ia_INTJ '_PUNCT da_ADJ ni_PRON [_PUNCT ]_PUNCT iawn_INTJ ._PUNCT
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwyr_NOUN ]_PUNCT ._PUNCT
O_ADP 'di_PART o_ADP ddim_PRON jyst_X dydd_NOUN Llun_PROPN ?_PUNCT
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwr_NOUN ]_PUNCT ._PUNCT
O_ADP 'di_PART o_ADP dydd_NOUN Merchar_PROPN [_PUNCT ]_PUNCT ?_PUNCT
<_SYM S_NUM 5_NUM /_SYM 6_NUM >_SYM [_PUNCT Dim_DET caniatad_NOUN i_PART drawsgrifio_VERB 'r_DET siaradwyr_NOUN ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
<_SYM SS_X >_SYM [_PUNCT =_SYM ]_PUNCT ta_CONJ ra_NOUN ta_X ra_NOUN [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
no_PART pressure_VERB '_PUNCT lly_X [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT 2_NUM >_SYM Ar_ADP bebe_NOUN wan_ADJ ?_PUNCT
[_PUNCT ]_PUNCT lleoliad_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT 2_NUM >_SYM [_PUNCT aneglur_ADJ ]_PUNCT canu_VERB 'n_PART ysgol_NOUN dw_AUX i_ADP 'm_DET isio_ADV neud_VERB o_ADP ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Pw_NOUN '_PUNCT o'dd_NOUN y_DET bobol_NOUN yna_ADV ?_PUNCT
T_NUM 'w_PRON '_PUNCT bod_AUX  _SPACE drws_NOUN nesa_ADJ i_PRON [_PUNCT aneglur_ADJ ]_PUNCT ochor_NOUN ycha_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ia_INTJ mam_NOUN t_PRON '_PUNCT '_PUNCT w'bod_VERB yr_DET hen_ADJ ddyn_NOUN [_PUNCT ]_PUNCT y_DET ferch_NOUN 'di_PART honna_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT y_DET ferch_NOUN [_PUNCT ]_PUNCT nhw_PRON bia_NOUN 'r_DET ty_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT 2_NUM >_SYM [_PUNCT ]_PUNCT ._PUNCT
[_PUNCT =_SYM ]_PUNCT W'ithia_VERB w'ithia_VERB [_PUNCT /=_PROPN ]_PUNCT a_CONJ mama_NOUN 'na_ADV rh_VERB 'w_PRON ddynas_NOUN birdwatching_ADJ yn_PART dod_VERB yna_ADV w'thia'_VERBUNCT does_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ista_NOUN 'n_ADP yr_DET ar_NOUN '_PUNCT ._PUNCT
Ia_ADJ ond_CONJ ddim_ADP y_DET ddynas_NOUN birdwatching_NOUN 'di_PART honna_VERB naci_NOUN ?_PUNCT
Naci_PROPN ._PUNCT
Nhw_PRON bia_NOUN 'r_DET ty_NOUN ._PUNCT
Ma_INTJ '_PUNCT hi_PRON 'n_PART ferch_NOUN i_ADP pw_PRON '_PUNCT bia_NOUN 'r_DET ty_NOUN [_PUNCT ]_PUNCT mama_NOUN 'na_ADV ddwy_NUM ferch_NOUN +_SYM
Ddim_PART lle_NOUN o'ddach_PUNCT chi_PRON 'n_PART byw_VERB ?_PUNCT
[_PUNCT ]_PUNCT ._PUNCT
Ochor_NOUN ycha_VERB i_PRON [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
O_ADP [_PUNCT =_SYM ]_PUNCT reit_INTJ reit_INTJ [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Ty_NOUN ha_PRON '_PUNCT mewn_ADP ffor_NOUN '_PUNCT de_NOUN ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ia_X ia_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
[_PUNCT ]_PUNCT ._PUNCT
O'ddan_ADP nhw_PRON ddim_PART yn_PART dod_VERB 'na_ADV 'n_PART amal_ADJ ._PUNCT
[_PUNCT ]_PUNCT rioed_NOUN 'di_PART gweld_VERB nhw_PRON o_ADP '_PUNCT blaen_NOUN ._PUNCT
Do_AUX ti_PRON 'di_PART gweld_VERB nhw_PRON yna_DET o_ADP '_PUNCT blaen_NOUN do_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Ond_CONJ lwcus_ADJ iddi_ADP ddeud_VERB lleoliad_NOUN wrtha_ADP i_PRON '_PUNCT fyd_NOUN '_PUNCT cos_NOUN o_ADP 'n_PART i_PRON chydig_ADV bach_ADJ yn_PART [_PUNCT ]_PUNCT pw_INTJ '_PUNCT o'dd_NOUN hi_PRON ddechra_VERB ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT '_PUNCT chydig_ADJ bach_ADJ ._PUNCT
Ond_CONJ dw_VERB 'm_PRON yn_PART '_PUNCT nabod_VERB fo_PRON de_NOUN ._PUNCT
[_PUNCT ]_PUNCT ._PUNCT
Dw_AUX 'm_PRON yn_PART meddwl_VERB '_PUNCT mod_AUX i_PRON 'di_PART siarad_VERB llawar_NUM ._PUNCT
<_SYM S_NUM ?_PUNCT 2_NUM >_SYM lleoliad_NOUN dyna_DET o'dd_NOUN enw_NOUN 'r_DET ty_NOUN ._PUNCT
lleoliad_NOUN ia_PRON ._PUNCT
<_SYM S_NUM 4_NUM >_SYM Do_INTJ 'n_PART i_ADP 'm_DET hyd_NOUN yn_PART oed_NOUN yn_PART gw'bod_VERB hynny_PRON [_PUNCT ]_PUNCT [_PUNCT ]_PUNCT reit_INTJ '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB '_PUNCT wan_ADJ ._PUNCT
1509.595_ADJ
1512.498_VERB
1582.446_NUM
