Gweithgareddau_NOUN Gwyliau_PROPN
Gweithgareddau_NOUN Chwaraeon_NOUN Hanner_NOUN Tymor_X Chwefror_PROPN :_PUNCT
-_PUNCT 20.02_NUM ._PUNCT 1_NUM 7_NUM 24.02_NUM ._PUNCT 1_NUM 7_NUM
Mae_VERB llwyth_PRON o_ADP weithgareddau_NOUN chwaraeon_NOUN i_ADP 'ch_PRON cadw_VERB 'n_PART brysur_ADJ yr_DET hanner_NOUN tymor_NOUN hwn_DET ._PUNCT
Sicrhewch_VERB eich_DET lle_NOUN ar_ADP -_PUNCT lein_NOUN ,_PUNCT nawr_ADV !_PUNCT
Dydd_NOUN Llun_PROPN -_PUNCT Gwener_PROPN :_PUNCT
Nofio_VERB Dwys_PROPN i_ADP ddechreuwyr_NOUN
(_PUNCT £_SYM 30_NUM )_PUNCT
Dydd_NOUN Llun_PROPN :_PUNCT
Rygbi_VERB gyda_ADP 'r_DET Gleision_NOUN
(_PUNCT £_SYM 15_NUM -_PUNCT £_SYM 25_NUM )_PUNCT
Dawns_PROPN
(_PUNCT £_SYM 8_NUM )_PUNCT
Dydd_NOUN Mawrth_PROPN :_PUNCT
Gymnasteg_PROPN
(_PUNCT £_SYM 10_NUM )_PUNCT
Dydd_NOUN Mercher_PROPN :_PUNCT
Diwrnod_NOUN Chwaraeon_PROPN
(_PUNCT £_SYM 15_NUM )_PUNCT
Dydd_NOUN Gwener_PROPN :_PUNCT
Diwrnod_NOUN awyr_NOUN agored_NOUN
(_PUNCT £_SYM 35_NUM )_PUNCT
Hyfforddiant_NOUN Pêl_PROPN -_PUNCT droed_NOUN merched_NOUN
(_PUNCT £_SYM 10_NUM )_PUNCT
Pêl_ADV -_PUNCT droed_NOUN 6_NUM bob_DET ochr_NOUN i_ADP 'r_DET teulu_NOUN
(_PUNCT £_SYM 10_NUM )_PUNCT
Am_ADP wybodaeth_NOUN pellach_ADJ ,_PUNCT cysylltwch_VERB a_CONJ
cyfeiriad_NOUN -_PUNCT bost_NOUN
neu_CONJ i_PART archebu_VERB eich_DET lle_NOUN ar_ADP -_PUNCT lein_NOUN :_PUNCT
cyfeiriad_NOUN
Mewn_ADP partneriaeth_NOUN â_ADP 'r_DET Urdd_NOUN ._PUNCT
