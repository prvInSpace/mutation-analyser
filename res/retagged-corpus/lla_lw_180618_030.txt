[_PUNCT clecian_VERB ]_PUNCT  _SPACE <_SYM aneglru_NOUN >_SYM -_PUNCT gawn_VERB gawn_ADP ni_PRON roi_AUX pres_NOUN yn_PART ol_VERB am_ADP y_DET llwyth_NOUN i_ADP ni_PRON enwbg_NOUN ?_PUNCT
Iawn_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Naddo_PROPN nes_CONJ i_PART ffonio_VERB hi_PRON wan_ADV de_NOUN ych_NOUN -_PUNCT ond_CONJ a_PART wedyn_ADV wnaeth_VERB hi_PRON yrru_VERB neges_NOUN i_ADP enwb_VERB ond_CONJ oedd_VERB enwb_NOUN yn_PART gwaith_NOUN a_CONJ wedyn_ADV '_PUNCT dy_DET __NOUN ddim_PART ishio_VERB neud_VERB dim_DET byd_NOUN arall_ADJ am_ADP y_DET peth_NOUN a_CONJ wedyn_ADV mae_AUX hi_PRON 'n_PART mynd_VERB i_PART gael_VERB sgwrs_NOUN iawn_ADV hefo_ADP __NOUN __NOUN __NOUN am_ADP y_DET peth_NOUN -_PUNCT achos_ADP achos_ADP dw_AUX i_PRON 'n_PART teimlo_VERB oh_ADJ -_PUNCT dydy_VERB oh_ADJ mae_VERB gen_ADP i_PRON gwsmer_NOUN ._PUNCT
Na_VERB i_ADP ffonio_VERB chdi_NOUN i_ADP 'n_PART ol_VERB ._PUNCT
Ta_INTJ ._PUNCT
[_PUNCT Nid_PART oes_VERB angen_NOUN trawsgrifio_VERB hyn_PRON ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ia_INTJ ._PUNCT
Dw_VERB i_PRON 'n_PART ddiolchgar_ADJ bod_AUX chdi_NOUN wedi_PART g'neud_VERB y_DET llwyth_PRON cofia'_VERBUNCT swn_NOUN -_PUNCT i_ADP '_PUNCT swn_NOUN i_ADP heb_ADP 'di_PART trafferthu_VERB g'neud_VERB hi_PRON de_NOUN achos_NOUN '_PUNCT swn_NOUN i_ADP byth_ADV 'di_PART gallu_VERB g'neud_VERB hi_PRON de_NOUN felly_ADV trwy_ADP hyn_PRON i_ADP gyd_ADP dw_VERB i_PRON 'n_PART ddiolchgar_ADJ iawn_ADV okay_VERB <_SYM /_PUNCT eng_VERB >_SYM ?_PUNCT
[_PUNCT giglan_VERB ]_PUNCT  _SPACE bod_AUX fi_PRON 'n_PART swnio_VERB fel_ADP bobo_VERB fi_PRON ddim_PART mi_PRON ydw_VERB i_PRON a_CONJ dw_AUX i_PRON 'n_PART teimlo_VERB bod_VERB -_PUNCT o_ADP bod_VERB o_PRON ddim_PART yn_PART bebe_NOUN mae_VERB nhw_PRON 'di_PART [_PUNCT aneglur_ADJ ]_PUNCT fod_VERB ishio_VERB a_CONJ bod_AUX fi_PRON 'n_PART teimlo_VERB masiwr_NOUN fel_CONJ bod_VERB nhw_PRON ddim_PART 'di_PART talu_VERB ._PUNCT
Dyna_ADV ydw_AUX i_PRON 'n_PART deimlo_VERB ._PUNCT
Ia_INTJ ia_PRON ia_X dio_VERB 'm_DET yn_PART swnio_VERB fel_ADP daw_VERB hi_PRON de_NOUN [_PUNCT clecian_VERB ]_PUNCT Nadi_X ._PUNCT
Nadi_INTJ ._PUNCT
-_PUNCT BeBe_VERB bebe_NOUN mae_VERB enwb_NOUN 'di_PART deud_VERB dw_AUX i_PRON 'n_PART meddwl_VERB yn_ADP y_DET neges_NOUN bore_NOUN 'ma_ADV ydy_VERB neith_NOUN sylwi_VERB de_NOUN ._PUNCT
Oh_INTJ ia_PRON -_PUNCT ond_CONJ ond_ADP ti_PRON 'n_PART gwybod_VERB mae_AUX hi_PRON yn_PART mynd_VERB i_PART gael_VERB sgwrs_NOUN iawn_ADV hefo_ADP hi_PRON wan_NOUN dydy_VERB achos_NOUN yn_PART gwaith_NOUN oedd_VERB enwb_NOUN wan_ADJ ._PUNCT
Fydd_VERB rhaid_VERB fi_PRON cael_VERB sgwrs_NOUN iawn_ADV hefo_ADP hi_PRON eto_ADV a_CONJ wedyn_ADV os_CONJ na_PART ddaw_VERB enwbg_NOUN yn_PART ol_VERB atom_VERB ni_PRON dw_VERB -_PUNCT i_ADP dw_AUX i_ADP 'm_DET yn_PART cael_VERB g'neud_VERB dim_DET stwr_NOUN eto_ADV meddai_VERB [_PUNCT aneglur_ADJ ]_PUNCT am_ADP y_DET peth_NOUN [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ia_NOUN a_CONJ -_PUNCT ty_NOUN tydw_NOUN tyswi_VERB i_ADP ddim_PART ishio_VERB colli_VERB cwsmeriaid_NOUN nagoes_ADJ a_CONJ dw_VERB i_ADP ddim_PART ishio_VERB iddi_ADP hi_PRON -_PUNCT dw_VERB dw_AUX i_ADP 'm_DET yn_PART meddwl_VERB -_PUNCT dy_DET dy_DET dydy_VERB hi_PRON ddim_PART yn_PART flin_ADJ o_ADP gwbl_NOUN mae_AUX -_PUNCT hi_PRON 'n_PART mae_AUX hi_PRON 'n_PART derbyn_VERB dydy_VERB bod_VERB pethau_NOUN fel_ADP hyn_PRON yn_PART digwydd_VERB mae_VERB 'n_PART hollol_ADV hollol_ADJ iawn_ADJ am_ADP y_DET peth_NOUN -_PUNCT ond_CONJ ond_CONJ yn_PART nghefn_NOUN meddwl_VERB dw_AUX i_PRON 'n_PART teimlo_VERB oh_INTJ tasai_VERB rhoi_AUX pres_NOUN yn_PART ol_PRON dw_AUX i_PRON 'n_PART teimlo_VERB wedyn_ADV fod_AUX pethau_NOUN yn_PART mynd_VERB i_ADP fod_VERB yn_PART well_ADJ [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Yndi_VERB [_PUNCT clecian_VERB ]_PUNCT mae_VERB gan_ADP y_DET <_SYM celcian_VERB gwefusaU_DET >_SYM [_PUNCT saib_NOUN ]_PUNCT oh_CONJ ia_PRON -_PUNCT merch_NOUN oh_X ia_X ia_X enwb_NOUN sydd_VERB 'di_PART rhoi_VERB hi_PRON -_PUNCT i_ADP i_ADP 'w_PRON brawd_NOUN de_NOUN [_PUNCT clecian_VERB ]_PUNCT Ia_INTJ [_PUNCT saib_NOUN ]_PUNCT iawn_ADV okay_VERB <_SYM /_PUNCT eng_VERB >_SYM ._PUNCT
Na_INTJ ._PUNCT
Ia_INTJ byddat_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Ia_INTJ ._PUNCT
Ia_INTJ ._PUNCT
Nes_CONJ i_PART ddeud_VERB wrth-_NOUN wrth_ADP enwbg_NOUN at_ADP bore_NOUN 'ma_ADV -_PUNCT bobo_VERB bobo_VERB ni_PRON 'n_PART gallu_VERB ordro_ADP un_NUM arall_ADJ a_CONJ nes_CONJ i_PART ddeud_VERB yn_PART [_PUNCT aneglur_ADJ ]_PUNCT i_ADP gyd_ADP heb_PART ofyn_VERB i_ADP chdi_NOUN nes_CONJ i_PART ddeud_VERB '_PUNCT sa_PRON -_PUNCT ti_PRON '_PUNCT sa_AUX ti_PRON 'n_PART gallu_VERB -_PUNCT ym_ADP [_PUNCT clecian_VERB ]_PUNCT crafu_VERB eto_ADV de_CONJ llosgi_VERB eto_ADV ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Dyna_ADV fo_PRON ._PUNCT
Yndi_PART yndi_VERB Castell_PROPN Emlyn_PROPN ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Wedyn_ADV nawn_NOUN i_PART aros_VERB i_PRON glywed_VERB wan_NOUN a_CONJ wedyn_ADV os_CONJ na_PART chlywon_VERB ni_PRON ddim_PART byd_NOUN dan_ADP ni_PRON ddim_PART yn_PART cael_ADV neud_VERB dim_DET stwr_NOUN ._PUNCT
Ond_CONJ ga_NOUN i_PRON gysylltu_VERB 'n_PART nol_NOUN hefo_ADP i_ADP caf_VERB -_PUNCT a_CONJ a_CONJ deud_VERB bod_VERB ni_PART ishio_VERB talu_VERB pres_NOUN yn_PART ol_VERB ?_PUNCT
Iawn_INTJ neu_CONJ chdi_NOUN iawn_ADV [_PUNCT clecian_VERB ]_PUNCT A_CONJ gynnon_VERB -_PUNCT ni_PRON newid_VERB pwnc_NOUN wan_ADJ de_NOUN mae_VERB gynnon_VERB ni_PRON gardiau_NOUN sydd_VERB 'di_PART colli_VERB lliw_NOUN yn_ADP '_PUNCT Steddfod_X iawn_ADV achos_CONJ oedd_VERB gynnon_VERB ni_PRON r'un_NOUN r'un_NOUN raciau_NOUN allan_ADV yn_PART ganol_NOUN wsos_NOUN doedd_VERB a_CONJ o_AUX 'n_PART i_PRON 'n_PART amau_VERB sti_NOUN o_ADP 'n_PART i_PRON 'n_PART amau_VERB wrth_ADP bacio_VERB [_PUNCT aneglur_ADJ ]_PUNCT lliw_NOUN yr_DET amlenni_NOUN 'di_PART mynd_VERB de_NOUN a_CONJ wedyn_ADV enwb_VERB cyfenw_NOUN -_PUNCT __NOUN -_PUNCT a_CONJ __NOUN __NOUN __NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN yn_ADP Daid_NOUN ac_CONJ yn_ADP Nain_PROPN ers_ADP -_PUNCT dydd_NOUN oh_X '_PUNCT dy_DET __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ddim_PRON wrth_ADP gwrs_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN yn_ADP Daid_NOUN ers_ADP dydd_NOUN Iau_ADJ diwetha_ADJ de_NOUN ._PUNCT
A_CONJ wedyn_ADV mae_VERB rhywun_NOUN 'di_PART bod_VERB yma_ADV bore_NOUN 'ma_ADV enwb_NOUN twrnai_NOUN 'di_PART bod_AUX yma_ADV yn_PART chwilio_VERB am_ADP gerdyn_NOUN Nain_PROPN a_CONJ Taid_NOUN ag_CONJ on_X i_PRON 'n_PART sbio_VERB ar_ADP yr_DET amlen_NOUN ac_CONJ o_ADP 'n_PART i_PRON 'n_PART meddwl_VERB oh_ADJ dw_VERB i_PRON 'n_PART siŵr_ADJ bod_VERB hwn_PRON yn_PART hufennog_ADJ [_PUNCT clecian_VERB ]_PUNCT ._PUNCT
Nes_CONJ i_PART agor_VERB o_ADP ag_CONJ oedd_VERB 'na_ADV golli_VERB lliw_NOUN difrifol_ADJ a_CONJ wedyn_ADV nes_CONJ i_PART newid_VERB o_PRON ._PUNCT
BeBe_AUX ti_PRON 'n_PART meddwl_ADV neud_VERB yma_ADV bore_NOUN fory_ADV ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Hynny_PRON ydy_VERB s'gen_NOUN ti_PRON amser_NOUN -_PUNCT i_ADP i_PART ordro_VERB amlenni_X ?_PUNCT
Ond_CONJ oes_VERB ?_PUNCT
-_PUNCT Ma_X mae_VERB genna_VERB i_ADP ond_CONJ de_NOUN dw_VERB -_PUNCT i_ADP dw_VERB i_ADP ishio_VERB ordro_ADP petha'_NOUNUNCT [_PUNCT aneglur_ADJ ]_PUNCT <_SYM /_SYM eng_VERB >_SYM i_ADP fy_DET hun_DET de_NOUN ond_CONJ pwy_PRON a_CONJ wyr_NOUN pryd_ADV na_CONJ i_PRON de_NOUN e'llai_VERB na_CONJ i_PRON ddim_PRON am_ADP wythnosau_NOUN de_NOUN ._PUNCT
Nabod_VERB fi_PRON na_PART i_PRON ddim_PRON am_ADP wythnosau_NOUN ond_CONJ dw_VERB i_PART ishio_VERB neud_VERB wsos_NOUN yma_ADV ond_CONJ nabod_VERB fi_PRON na_PART i_PRON ddim_PART [_PUNCT clecian_VERB ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ia_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ia_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Oh_X wyt_AUX ti_PRON 'di_PART cael_VERB mwy_ADV dw_AUX i_PRON 'n_PART gweld_VERB mwy_PRON o_ADP rhai_PRON coch_ADV wan_ADJ yn_ADP y_DET bocs_NOUN cofi_VERB [_PUNCT clecian_VERB ]_PUNCT Ow_DET gwitchiad_NOUN am_ADP bach_NOUN wan_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT [_PUNCT Tybiaf_PROPN fod_AUX y_DET siaradwraig_NOUN wedi_PART cerdded_VERB i_ADP ffwrdd_ADV o_ADP 'r_DET teclyn_NOUN recordio_VERB felly_CONJ mae_VERB 'n_PART anodd_ADJ ei_DET chlywed_VERB hi_PRON 'n_PART glir_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM dipyn_NOUN bach_ADJ yn_PART fwy_ADJ 'na_ADV hyna_VERB ._PUNCT
Mae_VERB -_PUNCT nhw_PRON na_PART i_PRON adael_VERB -_PUNCT nhw_PRON fory_ADV fasa_AUX ti_PRON 'n_PART neud_VERB o_PRON ?_PUNCT
Yn_PART gwaith_NOUN ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Oh_X dim_PRON ond_ADP os_CONJ ti_PRON ishio_VERB dw_AUX i_PRON 'n_PART meddwl_VERB bod_AUX fi_PRON 'n_PART wastio_VERB dy_DET amser_NOUN di_PRON os_CONJ dw_AUX ti_PRON 'n_PART neud_VERB o_ADP adre_ADV '_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_DET Fasa_NOUN well_ADJ gen_ADP ti_PRON ?_PUNCT
Okay_VERB <_SYM /_SYM eng_VERB >_SYM iawn_ADV okay_VERB <_SYM /_PUNCT eng_VERB >_SYM  _SPACE iawn_ADV cant_NUM pedwar_NOUN -_PUNCT deg_NUM neu_CONJ na_PART -_PUNCT i_ADP na_PART i_ADP yrru_VERB fo_PRON i_ADP chdi_NOUN ._PUNCT
Cant_VERB pedwar_NOUN deg_NUM un_NUM centimedr_NOUN sgwar_NOUN ._PUNCT
Ia_INTJ ._PUNCT
Wel_CONJ dwi_AUX 'n_PART talu_VERB nhw_PRON -_PUNCT rwy_VERB dw_AUX i_PRON 'n_PART guesshio_VERB wan_ADJ  _SPACE tair_NUM ceiniog_NOUN ._PUNCT
Iawn_INTJ ._PUNCT
Iawn_INTJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT '_PUNCT dy_DET o_ADP yn_ADP y_DET gwaelod_NOUN 'ma_ADV ?_PUNCT
[_PUNCT Tybiaf_PROPN fod_AUX y_DET siaradwraig_NOUN wedi_PART cerdded_VERB i_ADP ffwrdd_ADV o_ADP 'r_DET teclyn_NOUN recordio_VERB felly_ADV anodd_ADJ ei_DET chlywed_VERB yn_PART glir_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ia_NOUN okay_VERB <_SYM /_PUNCT eng_VERB >_SYM ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ ti_PRON 'm_DET yn_PART cofio_VERB faint_NOUN sy_VERB 'n_PART rhydd_ADJ ohonon_ADP nhw_PRON nagwyt_NOUN ?_PUNCT [_PUNCT clecian_VERB ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM O_ADP fasa_VERB lliw_NOUN yn_PART ddrytach_ADJ dipyn_NOUN bach_ADJ yn_PART ddrytach_ADJ dw_AUX i_PRON 'n_PART meddwl_VERB de_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Ia_INTJ plis_INTJ ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM A_CONJ dw_AUX i_ADP 'm_DET yn_PART gwybod_VERB faint_ADV de_NOUN ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT  _SPACE oes_VERB hanner_NOUN cant_NUM [_PUNCT anelgur_DET ]_PUNCT dw_AUX i_PRON 'n_PART meddwl_VERB cofia_VERB ond_CONJ dydw_AUX i_ADP ddim_PART yn_PART gwybod_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
[_PUNCT Swn_PROPN y_DET cefndir_NOUN yn_PART trechu_VERB 'r_DET sgwrs_NOUN ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Cant_X ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Oh_X nawn_NOUN i_ADP hanner_NOUN cant_NUM i_ADP ddechrau_VERB [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Oh_X ia_X ia_SYM dw_AUX i_PRON 'n_PART gweld_VERB diolch_NOUN yn_PART fawr_ADV wan_ADJ diolch_NOUN [_PUNCT aneglur_ADJ ]_PUNCT de_NOUN a_CONJ babis_NOUN Nain_PROPN a_CONJ Taid_NOUN [_PUNCT aneglur_ADJ ]_PUNCT hefyd_ADV ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Iawn_X ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB neu_CONJ gwyn_ADJ [_PUNCT aneglur_ADJ ]_PUNCT ond_CONJ mi_PRON fasai_VERB lliw_NOUN yn_PART ddrytach_ADJ [_PUNCT clecian_VERB ]_PUNCT dw_AUX i_ADP wedi_PART ei_PRON yrru_VERB fo_PRON trwy_ADP Whatsapp_X <_SYM /_SYM eng_VERB >_SYM ._PUNCT
[_PUNCT clecian_VERB ]_PUNCT 'na_ADV ni_PRON okay_VERB <_SYM /_PUNCT eng_VERB >_SYM ._PUNCT
-_PUNCT Bore_NOUN bore_NOUN ti_PRON 'ma_ADV fory_ADV ia_ADJ ?_PUNCT
P'nawn_NUM mae_VERB cynhebrwng_NOUN ia_ADJ ?_PUNCT
okay_VERB <_SYM /_PUNCT eng_VERB >_SYM iawn_INTJ ._PUNCT
Ta_INTJ ._PUNCT
Ta_INTJ ._PUNCT
582.550_NOUN
