Wedyn_ADV ma_VERB hwn_PRON 'di_PART bod_AUX yn_PART mynd_VERB am_ADP mis_NOUN wedyn_ADV yndi_VERB ?_PUNCT
Ydi_NOUN ydi_PRON mae_AUX 'n_PART dechrau_VERB ym_ADP mis_NOUN Ionawr_PROPN a_CONJ wedyn_ADV beth_PRON sy_VERB gynnon_ADP ni_PRON ydi_ADV ydi_VERB  _SPACE felly_ADV fydd_VERB rhaid_VERB i_ADP ni_PRON gael_VERB fformat_NOUN gweddol_ADJ sgwarog_ADJ bydd_VERB ?_PUNCT
A_CONJ ie_INTJ debyg_ADJ i_ADP be_DET da_ADJ ni_PRON 'n_PART g'neud_VERB i_ADP hysbyseb_NOUN ._PUNCT
Debyg_ADJ i_ADP ffenest_NOUN ia_PRON hysbyseb_NOUN ie_INTJ ?_PUNCT
Wedyn_ADV  _SPACE gan_CONJ bod_AUX hwn_PRON yn_PART mynd_VERB ar_ADP y_DET wefan_NOUN  _SPACE on_X i_PRON 'n_PART meddwl_VERB '_PUNCT swn_NOUN i_PRON 'n_PART cael_VERB o_ADP tu_NOUN allan_ADV yn_PART yn_PART un_NUM bloc_NOUN a_CONJ fydd_AUX hwnnw_PRON ddim_PART yn_PART newid_VERB o_ADP gwbl_NOUN ._PUNCT
Oce_VERB a_CONJ un_NUM lliw_NOUN fasa_VERB hwnnw_DET ?_PUNCT
Un_NUM lliw_NOUN odyw_VERB on_X o_ADP syniad_NOUN o_ADP gal_NOUN rhyw_DET fath_NOUN o_ADP goch_ADJ di_PRON dim_PRON coch_ADJ [_PUNCT aneglur_ADJ ]_PUNCT llachar_ADJ ella_ADV +_SYM
O_ADP ia_X ie_X ie_INTJ ._PUNCT
ond_CONJ ond_ADP rhyw_DET fath_NOUN o_ADP goch_ADJ iddo_ADP ag_CONJ wedyn_ADV falle_ADV bod_VERB  _SPACE rei_NOUN o_ADP rhein_NOUN ella_ADV os_CONJ yw_VERB 'n_PART goch_ADJ eitha_ADV tywyll_NOUN sa_AUX 'n_PART edrych_VERB yn_PART well_ADJ sgwennu_VERB gwyn_ADJ fuasa_VERB ?_PUNCT
Ie_INTJ ._PUNCT
Yn_PART dod_VERB allan_ADV o_ADP 'r_DET coch_ADJ ella_ADV ie_INTJ ?_PUNCT
Ynde_INTJ ._PUNCT
ymm_CONJ ag_ADP wedyn_ADV ma_VERB pan_CONJ fydd_VERB enwb_NOUN yn_PART rhoid_VERB y_DET manylion_NOUN misol_ADJ i_ADP fewn_ADP bydd_AUX hi_PRON 'n_PART newid_VERB y_DET darn_NOUN yma_ADV a_CONJ wedyn_ADV mewn_ADP ffordd_NOUN ma_PRON na_PART gofyn_VERB bo_VERB pan_CONJ fydd_VERB y_DET boy_NOUN y_DET wefan_NOUN yn_PART g'neud_VERB hynna_PRON mae_AUX 'n_PART gorfod_ADV fod_VERB yn_PART bocs_NOUN ar_ADP wahan_NOUN mewn_ADP ffordd_NOUN de_NOUN ?_PUNCT
'_PUNCT Na_INTJ fo_PRON so_VERB ma_VERB 'r_DET cefndir_NOUN yn_PART aros_VERB run_NOUN fath_NOUN i_ADP bob_DET mis_NOUN ._PUNCT
Yndi_ADV wedyn_ADV fydd_VERB rhaid_VERB i_ADP hwnnw_PRON gael_VERB ei_DET greu_VERB fatha_NOUN un_NUM un_NUM PDJ_PROPN neu_CONJ JPEG_PROPN ti_PRON 'n_PART defnyddio_VERB ar_ADP y_DET wefan_NOUN ie_INTJ ?_PUNCT
Ie_INTJ JPEG_PROPN efo_ADP [_PUNCT aneglur_ADJ ]_PUNCT drwm_ADJ ynde_ADV ?_PUNCT
Ia_INTJ ie_INTJ na_CONJ fo_PRON felly_ADV bydd_VERB hwnna_PRON mewn_ADP un_NUM ond_CONJ fydd_VERB hwnna_PRON ar_ADP JPEG_PROPN arwahan_VERB +_SYM
Yn_PART mynd_VERB ar_ADP ei_DET ben_NOUN o_ADP ._PUNCT
Mynd_VERB ar_ADP ei_DET ben_NOUN o_ADP wedyn_NOUN ia_PRON ?_PUNCT
Neith_VERB hynna_PRON gweithio_VERB neith_ADV ._PUNCT
Wedyn_ADV sa_AUX ni_PRON 'n_PART yn_PART jyst_ADV yn_PART  _SPACE cal_NOUN rhywbeth_NOUN i_ADP drio_VERB fel_CONJ bod_VERB gennon_ADP ni_PRON yn_PART barod_ADJ wedyn_ADV ar_ADP gyfer_NOUN mis_NOUN Ionawr_PROPN fel_ADP da_ADJ ni_PRON isio_VERB ._PUNCT
Da_ADJ ni_PRON isio_VERB [_PUNCT aneglur_ADJ ]_PUNCT at_ADP at_ADP delwedd_NOUN sef_CONJ g'neud_VERB y_DET wefan_NOUN a_CONJ wedyn_ADV neud_VERB siŵr_ADV fod_AUX popeth_PRON yn_PART gweithio_VERB gynta_ADJ ._PUNCT
Na_INTJ fod_VERB iawn_ADV werth_ADJ chweil_ADV ._PUNCT
I_ADP 'r_DET dim_PRON ._PUNCT
Yndi_ADV grêt_ADJ ._PUNCT
Diolch_VERB yn_PART fowr_NOUN ._PUNCT
Odd_NOUN hyna_VERB 'n_PART da_ADJ iawn_ADV enwb_NOUN ._PUNCT
103.607_VERB
