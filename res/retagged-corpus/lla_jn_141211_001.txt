0.0_NOUN
Ahoi_VERB enw_NOUN ._PUNCT
<_SYM SS_X >_NUM Ahoi_X enw_NOUN ._PUNCT
Dyma_ADV ni_PRON ._PUNCT
Y_DET gist_NOUN ._PUNCT
A_PART mae_VERB hon_PRON yn_PART llawn_ADJ o_ADP aur_NOUN ac_CONJ arian_NOUN a_CONJ gemau_NOUN drud_ADJ ._PUNCT
Ac_CONJ i_PART agor_VERB y_DET gist_NOUN bydd_AUX r'aid_NOUN i_ADP chi_PRON chwarae_VERB pedair_NUM gêm_NOUN ._PUNCT
Ac_CONJ os_CONJ lwyddwch_VERB chi_PRON mi_PART gewch_VERB chi_PRON allwedd_NOUN aur_NOUN debyg_ADJ iawn_ADV i_ADP hon_PRON sydd_AUX wedi_PART cael_VERB ei_PRON cuddio_VERB yn_PART rh'wle_NOUN ar_ADP yr_DET ynys_NOUN ._PUNCT
Ydach_AUX chi_PRON 'n_PART deall_VERB ?_PUNCT
<_SYM SS_X >_SYM Yn_ADP enw_NOUN enw_NOUN ._PUNCT
'_PUNCT Mlaen_PROPN â_ADP ni_PRON i_ADP 'r_DET dasg_NOUN cyntaf_ADJ Ahoi_X ._PUNCT
<_SYM SS_X >_NUM Ahoi_X enw_NOUN ._PUNCT
I_ADP ffwr_NOUN '_PUNCT â_ADP ni_PRON [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ar_ADP yr_DET ynys_NOUN mae_VERB 'na_ADV saith_NUM ciwb_NOUN ._PUNCT
A_CONJ 'r_DET dasg_NOUN i_ADP chi_PRON heddiw_ADV fydd_VERB casglu_VERB 'r_DET saith_NOUN ciwb_NOUN i_PART sillafu_VERB enw_NOUN y_DET person_NOUN mwya'_ADJUNCT pwysig_ADJ a_CONJ clyfar_ADJ a_CONJ golygus_ADJ ar_ADP yr_DET ynys_NOUN ._PUNCT
Felly_ADV ydach_VERB chi_PRON 'n_PART barod_ADJ ?_PUNCT
<_SYM SS_X >_SYM Yn_ADP enw_NOUN enw_NOUN ._PUNCT
I_ADP ffwr_NOUN '_PUNCT â_ADP chi_PRON enwg_VERB cychwyn_VERB y_DET cloc_NOUN ._PUNCT
Ma'_VERBUNCT rhaid_VERB chi_PRON frysio_VERB ._PUNCT
Mmm_VERB ble_ADV mama_VERB nhw_PRON i_ADP gyd_ADP ?_PUNCT
'_PUNCT S_NUM gwn_VERB i_PRON ?_PUNCT
Mmm_VERB iawn_ADV ._PUNCT
By_VERB i_PRON gychwyn_VERB ._PUNCT
N_NUM ._PUNCT Mmm_VERB [_PUNCT =_SYM ]_PUNCT By_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
By_PRON ._PUNCT
yyy_CONJ ._PUNCT
'Di_PART ddim_PART mor_ADV hawdd_ADJ na_CONJ 'di_PART ?_PUNCT
<_SYM SS_X >_SYM Na_INTJ [_PUNCT Yn_PART siarad_VERB gyda_ADP 'i_PRON gilydd_NOUN am_ADP y_DET dasg_NOUN ]_PUNCT ._PUNCT
Br_INTJ ?_PUNCT
<_SYM SS_X >_SYM Ah_X ._PUNCT
yyy_ADV ty_NOUN ?_PUNCT
O_ADP rwy_AUX 'n_PART mynd_VERB i_ADP eistedd_VERB [_PUNCT ocheneidio_VERB ]_PUNCT ._PUNCT
<_SYM SS_X >_SYM Lle_NOUN mae_VERB e_PRON ?_PUNCT
Gwaeddwch_VERB pan_CONJ '_PUNCT dach_VERB chi_PRON 'di_PART gorffen_VERB ._PUNCT
Bydda'_VERBUNCT i_PRON 'n_PART cysgu_VERB ._PUNCT
<_SYM SS_NOUN >_SYM ym_ADP ._PUNCT
Fy_DET enw_NOUN [_PUNCT -_PUNCT ]_PUNCT ie_INTJ iawn_ADV [_PUNCT Yn_PART rhoi_VERB cynnig_NOUN ar_ADP rhan_NOUN gynta_ADJ 'r_DET ateb_NOUN cywir_ADJ ]_PUNCT ._PUNCT
<_SYM SS_X >_SYM Gorffan_PROPN ._PUNCT
Chi_PRON 'di_PART gorffen_VERB ?_PUNCT
<_SYM SS_X >_NUM Do_INTJ ._PUNCT
Dowch_VERB 'n_PART ôl_NOUN i_ADP fan_NOUN hyn_DET i_ADP fi_PRON ga'l_VERB golwg_NOUN enw_NOUN ._PUNCT
Bendigedig_ADJ ._PUNCT
'_PUNCT Dach_AUX chi_PRON wedi_PART llwyddo_VERB ._PUNCT
<_SYM SS_X >_SYM Hwre_X ._PUNCT
Felly_CONJ gewch_VERB chi_PRON weld_VERB y_DET map_NOUN gyntaf_ADJ ._PUNCT
Ffwr_NOUN '_PUNCT â_ADP chi_PRON i_ADP 'r_DET cwt_NOUN ._PUNCT
Hei_INTJ ._PUNCT
Aroswch_VERB amdanaf_ADP fi_PRON ._PUNCT
Fewn_ADP â_ADP chi_PRON i_PART weld_VERB y_DET map_NOUN ._PUNCT
Dyma_ADV ni_PRON ._PUNCT
Y_DET map_NOUN cynta'_ADJUNCT ._PUNCT
Ac_CONJ ar_ADP y_DET map_NOUN mae_VERB 'na_ADV groes_NOUN sy_AUX 'n_PART dangos_VERB ble_ADV mae_AUX 'r_DET allwedd_NOUN yn_PART cuddio_VERB ._PUNCT
Felly_CONJ enw_NOUN ._PUNCT
Ble_ADV mae_VERB 'r_DET allwedd_NOUN ?_PUNCT
<_SYM SS_X >_SYM Wrth_ADP ymyl_NOUN y_DET sleid_NOUN ._PUNCT
I_ADP ffwrdd_ADV â_ADP chi_PRON ._PUNCT
Llithrwch_VERB i_ADP ffwrdd_ADV ._PUNCT
Llithrwch_VERB ar_ADP y_DET llithren_NOUN ._PUNCT
<_SYM SS_X >_SYM Lle_NOUN mae_VERB o_PRON ?_PUNCT
Oes_VERB 'na_ADV allwedd_NOUN ?_PUNCT
<_SYM SS_X >_SYM '_PUNCT Dan_PROPN ni_PRON '_PUNCT im_PRON yn_PART weld_VERB o_ADP ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART meddwl_VERB bod_AUX chi_PRON 'n_PART gw'bod_VERB ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'di_PART mynd_VERB yn_PART dawel_ADJ iawn_ADV enw_NOUN ._PUNCT
Ble_ADV mae_VERB 'r_DET allwedd_NOUN ?_PUNCT
Rhaid_VERB i_ADP chi_PRON frysio_VERB neu_CONJ mi_PART fydd_VERB hi_PRON 'n_PART nos_NOUN ._PUNCT
<_SYM SS_X >_SYM Ââ_X ._PUNCT
Dyma_ADV hi_PRON ._PUNCT
BeBe_NOUN ?_PUNCT
<_SYM SS_X >_SYM Dyma_ADV hi_PRON ._PUNCT
Dyma_ADV hi_PRON bebe_NOUN ?_PUNCT
<_SYM SS_X >_SYM Yr_DET allwedd_NOUN ._PUNCT
Wel_CONJ o_ADP 'r_DET diwedd_NOUN ._PUNCT
'_PUNCT N_NUM ôl_NOUN ._PUNCT
Lle_ADV fuoch_VERB chi_PRON mor_ADV hir_ADJ ?_PUNCT
Ar_ADP goll_ADJ ?_PUNCT
Fuoch_AUX chi_PRON 'n_PART nofio_VERB ?_PUNCT
<_SYM SS_X >_NUM Do_INTJ ._PUNCT
Do_INTJ ?_PUNCT
Swnio_VERB fel_ADP '_PUNCT ny_PRON ._PUNCT
Iawn_INTJ ._PUNCT
Beth_PRON bynnag_PRON ._PUNCT
Dyma_ADV hi_PRON ._PUNCT
Yr_DET allwedd_NOUN gynta'_ADJUNCT ._PUNCT
'_PUNCT Na_VERB i_ADP r'oid_VERB hon_DET yn_PART fan_NOUN hyn_DET i_ADP enw_NOUN edrych_VERB ar_ADP 'i_PRON hôl_NOUN i_PRON yn_PART ofalus_ADJ ._PUNCT
Ac_CONJ mi_PART awn_VERB ni_PRON '_PUNCT mlaen_NOUN i_ADP 'r_DET ail_ADJ dasg_NOUN ._PUNCT
368.979_ADJ
Dyma_ADV ni_PRON ._PUNCT
Yr_DET allwedd_NOUN nesa'_ADJUNCT ._PUNCT
Llongyfarchiade_VERB '_PUNCT ._PUNCT
'_PUNCT Mlaen_PROPN â_ADP ni_PRON i_ADP tasg_NOUN tri_NUM ._PUNCT
Dyma_DET un_NUM o_ADP longe_NOUN '_PUNCT enw_NOUN ._PUNCT
Ond_CONJ yn_PART anffodus_ADJ ma_VERB 'r_DET llong_NOUN yma_DET ar_ADP fin_NOUN suddo_VERB ._PUNCT
Felly_CONJ y_DET dasg_NOUN i_ADP chi_PRON fydd_VERB trwsio_VERB 'r_DET twll_NOUN yn_ADP y_DET llong_NOUN ag_CONJ achub_VERB y_DET llong_NOUN rhag_CONJ suddo_VERB enwg_VERB cychwyn_VERB y_DET cloc_NOUN ._PUNCT
I_ADP ffwr_NOUN '_PUNCT â_ADP chi_PRON ._PUNCT
O_ADP da_ADJ iawn_ADV wir_ADV ._PUNCT
Daliwch_VERB i_ADP fynd_VERB ._PUNCT
Ie_INTJ ._PUNCT
Ww_ADP ._PUNCT
Iawn_INTJ ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'n_PART glyfar_ADJ ._PUNCT
Daliwch_VERB i_ADP bwmpio_VERB ._PUNCT
Ma_VERB 'r_DET llong_NOUN yn_PART dal_ADV i_ADP suddo_VERB ._PUNCT
<_SYM SS_X >_SYM Fel_ADP oeddach_ADP chdi_NOUN 'n_PART neud_VERB o_ADP ._PUNCT
BeBe_VERB '_PUNCT dach_VERB chi_PRON fod_VERB i_PART neud_VERB ?_PUNCT
<_SYM SS_X >_SYM Pwmpio_VERB fo_PRON i_ADP fyny_ADV ._PUNCT
Pwmpio_VERB fo_PRON i_ADP fyny_ADV ?_PUNCT
<_SYM SS_X >_SYM Naci_PROPN [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Pwmpio_VERB fo_PRON ar_ADP draws_ADJ ?_PUNCT
Daliwch_VERB i_ADP fynd_VERB ._PUNCT
Ie_INTJ ._PUNCT
O_ADP mae_AUX 'n_PART gwella_VERB ._PUNCT
[_PUNCT anadlu_VERB ]_PUNCT ._PUNCT
Ond_CONJ mae_VERB 'r_DET amser_NOUN yn_PART brin_ADJ iawn_ADV ._PUNCT
<_SYM SS_X >_SYM [_PUNCT Pawb_PROPN yn_PART dathlu_VERB ]_PUNCT ._PUNCT
Mor_ADV syml_ADJ â_ADP hynny_PRON ._PUNCT
Llongyfarchiade_VERB '_PUNCT ._PUNCT
<_SYM SS_X >_SYM Hwre_X ._PUNCT
Gewch_VERB chi_PRON weld_VERB y_DET map_NOUN ._PUNCT
Felly_CONJ enw_NOUN ._PUNCT
Ble_ADV mae_VERB 'r_DET allwedd_NOUN ?_PUNCT
<_SYM SS_NOUN >_SYM Ar_ADP ymyl_NOUN yr_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Wrth_ADP ymyl_NOUN y_DET lanfa_NOUN ?_PUNCT
<_SYM SS_X >_SYM Ia_INTJ ._PUNCT
I_ADP ffwr_NOUN '_PUNCT â_ADP chi_PRON ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'n_PART dal_VERB yma_ADV ?_PUNCT
Ffwr_NOUN '_PUNCT â_ADP chi_PRON ._PUNCT
Rhaid_VERB chi_PRON frysio_VERB ._PUNCT
Peidiwch_VERB â_ADP syrthio_VERB i_ADP 'r_DET dŵr_NOUN achos_CONJ '_PUNCT dw_AUX i_ADP ddim_PART yn_PART mynd_VERB i_PART wlychu_VERB i_PART achub_VERB chi_PRON ._PUNCT
Ara'_VERBUNCT deg_NUM ._PUNCT
<_SYM SS_X >_SYM Lle_NOUN mae_VERB o_PRON ?_PUNCT
[_PUNCT =_SYM ]_PUNCT Dyma_ADV hi_PRON [_PUNCT /=_PROPN ]_PUNCT ._PUNCT
Dyma_ADV hi_PRON [_PUNCT Pawb_PROPN yn_PART dathlu_VERB ]_PUNCT ._PUNCT
BeBe_NOUN ?_PUNCT
'_PUNCT Dw_AUX i_PRON 'm_DET yn_PART clywed_VERB dim_DET byd_NOUN ._PUNCT
'_PUNCT N_NUM ôl_NOUN â_ADP chi_PRON ._PUNCT
Ble_ADV mae_VERB 'r_DET allwedd_NOUN ?_PUNCT
<_SYM SS_X >_SYM Yn_PART fan_NOUN 'ma_ADV ._PUNCT
Gen_VERB '_PUNCT ti_PRON ma'_VERBUNCT 'i_PRON eto_ADV ?_PUNCT
Dyma_ADV ni_PRON ._PUNCT
Allwedd_NOUN rhif_NOUN tri_NUM ._PUNCT
'_PUNCT Na_VERB i_ADP r'oid_VERB hon_PRON hefo_ADP 'r_DET lleill_NOUN i_ADP enw_NOUN yn_PART fan_NOUN hyn_DET ._PUNCT
Ond_CONJ mae_VERB 'na_ADV un_NUM dasg_NOUN ar_ADP ôl_NOUN ._PUNCT
944.960_VERB
