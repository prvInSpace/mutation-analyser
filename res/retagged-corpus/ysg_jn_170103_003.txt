DEUNYDD_VERB TRYDYDD_NUM PARTI_PROPN
Cyfeiriad_NOUN Ebost_PROPN :_PUNCT [_PUNCT ._PUNCT ._PUNCT ._PUNCT ]_PUNCT
Rhif_NOUN Cynhyrchu_PROPN
Teitl_NOUN y_DET Gyfres_NOUN /_PUNCT Rhaglen_PROPN
Rhif_NOUN y_DET Bennod_PROPN a_CONJ Theitl_PROPN y_DET Bennod_PROPN yn_ADP y_DET Gyfres_PROPN
Cwmni_NOUN Cynhyrchu_PROPN
Dyddiad_NOUN Darlledu_PROPN
Rhaid_VERB clirio_VERB lleiafswm_NOUN yr_DET hawliau_NOUN fel_ADP a_PART anodwyd_VERB ym_ADP mharagraff_NOUN A_CONJ yn_ADP yr_DET Atodlen_NOUN cadarnhau_VERB Hawliau_PROPN (_PUNCT Atodlen_NOUN 3_NUM )_PUNCT yn_ADP y_DET Cytundeb_NOUN Comisiynnu_PROPN ._PUNCT
NODYN_NOUN :_PUNCT Rhaid_VERB cynnwys_VERB copiau_NOUN o_ADP drwyddedau_NOUN (_PUNCT gan_ADP gynnwys_VERB trwyddedau_NOUN cerddoriaeth_NOUN )_PUNCT ac_CONJ Ildiadau_NOUN Hawliau_NOUN Moesol_ADJ ._PUNCT
Lle_ADV nad_PART yw_VERB 'n_PART bosib_ADJ cael_VERB Trwydded_PROPN Trydydd_NOUN Parti_PROPN ,_PUNCT dyai_VERB 'r_DET cyflenwr_NOUN ddarparu_VERB unrhyw_DET wybodaeth_NOUN berthnasol_ADJ (_PUNCT enw_NOUN a_CONJ chyfeiriad_NOUN y_DET trwyddedwr_NOUN ,_PUNCT ffi_PART dyledus_ADJ ,_PUNCT defnydd_NOUN a_CONJ gliriwyd_VERB a_CONJ ffioedd_NOUN sy_VERB 'n_PART ddyledus_ADJ os_CONJ oes_VERB ymelwa_VERB pellach_ADJ )_PUNCT gan_CONJ nodi_VERB bod_AUX y_DET drwydded_NOUN i_PART ddilyn_VERB cyn_ADP gynted_NOUN ag_CONJ y_PART bo_VERB modd_NOUN
Nodiadau_PROPN
Yr_DET holl_DET ddeunydd_NOUN I_ADP gynnwys_VERB 35_NUM niwrnod_NOUN o_ADP we_NOUN ddarlledu_VERB
Math_NOUN o_ADP Ddeunydd_PROPN
Trwydded_PROPN wedi_PART 'i_PRON derbyn_VERB
Disgrifiad_NOUN y_DET Deunydd_PROPN
Ffi_NOUN
Costau_NOUN Hawliau_PROPN Pellach_PROPN
Ffi_NOUN Blaendal_PROPN
Manylion_PROPN
Cod_NOUN Amser_PROPN
Hyd_PROPN
Manylion_NOUN a_CONJ Sicrhawyd_PROPN
Nifer_NOUN o_ADP Ddarllediadau_PROPN
O_ADP (_PUNCT dd_VERB /_PUNCT mm_PRON /_PUNCT bbbb_NOUN )_PUNCT
Hyd_ADP (_PUNCT aa.mm.ee.ff_NOUN )_PUNCT
Hyd_PROPN
NIfer_NOUN TX_PROPN
Gwe_VERB Ddarlledu_NOUN
Teitl_PROPN
Enw_PROPN
Cyfenw_PROPN
Ffôn_PROPN
Facs_NOUN
Ebost_NOUN
Y_DET Wê_NOUN
Cwmni_PROPN
Llinell_NOUN 1_NUM
Llinell_NOUN 2_NUM
Tref_PROPN
Sir_NOUN
Côd_NOUN Post_NOUN
Nodiadau_PROPN
Math_NOUN o_ADP Ddeunydd_PROPN
Trwydded_PROPN wedi_PART 'i_PRON derbyn_VERB
Disgrifiad_NOUN y_DET Deunydd_PROPN
Ffi_NOUN
Costau_NOUN Hawliau_PROPN Pellach_PROPN
Ffi_NOUN Blaendal_PROPN
Manylion_PROPN
Cod_NOUN Amser_PROPN
Hyd_PROPN
Manylion_NOUN a_CONJ Sicrhawyd_PROPN
Nifer_NOUN o_ADP Ddarllediadau_PROPN
O_ADP (_PUNCT dd_VERB /_PUNCT mm_PRON /_PUNCT bbbb_NOUN )_PUNCT
Hyd_ADP (_PUNCT aa.mm.ee.ff_NOUN )_PUNCT
Hyd_PROPN
NIfer_NOUN TX_PROPN
Gwe_VERB Ddarlledu_NOUN
Teitl_PROPN
Enw_PROPN
Cyfenw_PROPN
Ffôn_PROPN
Facs_NOUN
Ebost_NOUN
Y_DET Wê_NOUN
Cwmni_PROPN
Llinell_NOUN 1_NUM
Llinell_NOUN 2_NUM
Tref_PROPN
Sir_NOUN
Côd_NOUN Post_NOUN
Nodiadau_PROPN
