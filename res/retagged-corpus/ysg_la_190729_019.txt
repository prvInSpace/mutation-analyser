Manyleb_PROPN Person_PROPN
Rhif_NOUN y_DET Swydd_NOUN
Diben_NOUN :_PUNCT Swyddog_NOUN Project_NOUN y_DET Map_NOUN Terfynol_PROPN
Is_ADJ -_PUNCT adran_NOUN :_PUNCT Swyddog_NOUN Hawliau_NOUN Tramwy_PROPN
Cwblhawyd_VERB Gan_ADP :_PUNCT
Mae_AUX 'r_DET Fanyleb_NOUN Person_PROPN yn_PART nodi_VERB 'r_DET prif_ADJ briodoleddau_NOUN y_PART mae_AUX eu_DET hangen_NOUN i_ADP gyflawni_VERB 'r_DET swydd_NOUN hon_DET yn_PART foddhaol_ADJ ._PUNCT
Wrth_PART lunio_VERB 'r_DET fanyleb_NOUN ,_PUNCT edrychwyd_VERB yn_PART fanwl_ADJ ar_ADP y_DET disgrifiad_NOUN swydd_NOUN i_PART amlygu_VERB elfennau_NOUN o_ADP 'r_DET swydd_NOUN sydd_AUX wedi_PART 'u_PRON pennu_VERB 'n_PART rhai_PRON hanfodol_ADJ ._PUNCT
Bwriad_NOUN y_DET Fanyleb_NOUN Person_PROPN yw_VERB rhoi_VERB gwell_ADJ dealltwriaeth_NOUN i_ADP ddarpar_VERB ymgeiswyr_NOUN o_ADP ofynion_NOUN y_DET swydd_NOUN ._PUNCT
Caiff_VERB ei_PRON defnyddio_VERB 'n_PART rhan_NOUN o_ADP 'r_DET broses_NOUN recriwtio_VERB i_ADP lunio_VERB rhestr_NOUN fer_ADJ o_ADP ymgeiswyr_NOUN ._PUNCT
Dylech_VERB sôn_VERB am_ADP bob_DET pwynt_NOUN sydd_VERB yn_ADP y_DET Fanyleb_NOUN Person_PROPN ,_PUNCT gan_CONJ roi_VERB tystiolaeth_NOUN o_ADP 'ch_PRON sgiliau_NOUN ,_PUNCT eich_DET profiad_NOUN a_CONJ 'r_DET wybodaeth_NOUN sydd_VERB gennych_ADP ym_ADP mhob_DET un_NUM o_ADP 'r_DET meysydd_NOUN hyn_PRON ._PUNCT
Byddwch_VERB yn_PART benodol_ADJ bob_DET amser_NOUN ._PUNCT
Peidiwch_VERB â_ADP defnyddio_VERB ymadroddion_NOUN cyffredinol_ADJ megis_NOUN "_PUNCT Mae_VERB gennyf_ADP y_DET sgiliau_NOUN angenrheidiol_ADJ ._PUNCT ._PUNCT ._PUNCT "_PUNCT neu_CONJ "_PUNCT Rwy_VERB 'n_PART hyderus_ADJ y_PART gallaf_VERB wneud_VERB y_DET swydd_NOUN yn_PART dda_ADJ ._PUNCT ._PUNCT ._PUNCT "_PUNCT ._PUNCT
Bydd_VERB angen_NOUN i_ADP 'r_DET panel_NOUN sy_AUX 'n_PART llunio_VERB 'r_DET rhestr_NOUN fer_ADJ wybod_VERB sut_ADV rydych_VERB yn_PART bodloni_VERB 'r_DET meini_NOUN prawf_NOUN ar_ADP sail_NOUN y_DET dystiolaeth_NOUN rydych_AUX yn_PART ei_PRON rhoi_VERB ._PUNCT
Hanfodol_ADJ
1_NUM ._PUNCT
Profiad_NOUN :_PUNCT
Profiad_NOUN o_ADP reoli_VERB projectau_NOUN a_CONJ /_PUNCT neu_CONJ raglenni_VERB
Dymunol_ADJ
Profiad_NOUN o_ADP reoli_VERB contractwyr_NOUN ,_PUNCT gwirfoddolwyr_NOUN a_CONJ phrojectau_VERB gan_ADP gynnwys_VERB monitro_VERB ac_CONJ adrodd_VERB ar_ADP gyflawni_VERB projectau_NOUN
Profiad_NOUN o_ADP weithio_VERB yn_PART rhan_NOUN o_ADP ddull_NOUN amlasiantaeth_NOUN i_ADP gyflawni_VERB amcanion_NOUN
2_NUM ._PUNCT
Gwybodaeth_NOUN :_PUNCT
Ymwybyddiaeth_NOUN o_ADP gyd-_NOUN destun_NOUN polisi_NOUN yn_PART berthnasol_ADJ i_ADP reolaeth_NOUN gynaliadwy_ADJ dros_ADP adnoddau_NOUN naturiol_ADJ a_CONJ hyrwyddo_VERB llesiant_VERB yng_ADP Nghymru_PROPN ._PUNCT
Gwybodaeth_NOUN am_ADP y_DET fframwaith_NOUN cyfreithiol_ADJ ar_ADP gyfer_NOUN rheoli_VERB Hawliau_NOUN Tramwy_NOUN Cyhoeddus_ADJ
3_NUM ._PUNCT
Sgiliau_NOUN a_CONJ galluoedd_NOUN :_PUNCT
Y_PART gallu_VERB i_ADP gynhyrchu_VERB adroddiadau_NOUN cryno_VERB
Dymunol_ADJ
Y_PART gallu_VERB i_ADP ymgysylltu_VERB ag_ADP amrywiaeth_NOUN eang_ADJ o_ADP bartneriaid_NOUN drwy_ADP gyflwyniadau_NOUN ac_CONJ ymarferion_NOUN ymgynghori_VERB ._PUNCT
Y_PART gallu_VERB i_PART ddadansoddi_VERB data_NOUN gan_CONJ ddefnyddio_VERB pecynnau_NOUN technoleg_NOUN gwybodaeth_NOUN
Gallu_VERB cyfathrebu_VERB 'n_PART effeithiol_ADJ ar_ADP lafar_NOUN ,_PUNCT yn_PART ysgrifenedig_ADJ a_CONJ gan_CONJ ddefnyddio_VERB technoleg_NOUN gwybodaeth_NOUN ._PUNCT
4_NUM ._PUNCT
Gweithio_VERB fel_ADP aelod_NOUN o_ADP dîm_NOUN
Y_PART gallu_VERB i_ADP drefnu_VERB llwyth_PRON gwaith_NOUN personol_ADJ yn_PART effeithiol_ADJ ,_PUNCT i_PART ddilyn_VERB cyfarwyddiadau_NOUN ,_PUNCT i_PART gwrdd_VERB â_ADP therfynau_NOUN amser_NOUN ,_PUNCT a_CONJ gweithio_VERB 'n_PART dda_ADJ dan_ADP bwysau_NOUN ,_PUNCT wrth_ADP gynnal_VERB safonau_NOUN o_ADP ansawdd_NOUN uchel_ADJ
Dymunol_ADJ :_PUNCT
Y_PART gallu_VERB i_ADP ddefnyddio_VERB GIS_PROPN (_PUNCT e_PRON ._PUNCT e_PRON ._PUNCT QGIS_PROPN )_PUNCT gan_CONJ gynnwys_VERB gwirio_VERB data_NOUN gan_CONJ ddefnyddio_VERB SQL_X a_CONJ newid_ADV gwrthrychau_VERB map_NOUN ar_ADP haenau_NOUN ._PUNCT
5_NUM ._PUNCT
Cymwysterau_NOUN a_CONJ Hyfforddiant_PROPN
Safon_NOUN A_X neu_CONJ gyfatebol_ADJ yn_PART ogystal_ADJ â_ADP T.G.A.U_PUNCT ._PUNCT Saesneg_PROPN a_CONJ Mathemateg_PROPN
6_NUM ._PUNCT
Agwedd_NOUN a_CONJ Chymhelliant_PROPN
Rhaid_VERB meddu_VERB ar_ADP safonau_NOUN uchel_ADJ o_ADP integriti_NOUN personol_ADJ a_CONJ chyhoeddus_ADJ
Dymunol_ADJ :_PUNCT
Hunangymhellol_PROPN a_CONJ 'r_DET gallu_NOUN i_PART weithio_VERB ar_ADP eich_DET liwt_NOUN eich_DET hun_NOUN
Agwedd_NOUN gadarnhaol_ADJ tuag_ADP at_ADP ofal_NOUN cwsmeriaid_NOUN ._PUNCT
7_NUM ._PUNCT
Arall_ADJ (_PUNCT rhowch_VERB fanylion_NOUN )_PUNCT :_PUNCT Y_PART gallu_VERB i_PART yrru_VERB /_PUNCT teithio_VERB ar_ADP hyd_NOUN a_CONJ lled_VERB y_DET rhanbarth_NOUN (_PUNCT Pen_PROPN -_PUNCT Y_NOUN -_PUNCT Bont_X Ar_ADP Ogwr_PROPN ,_PUNCT Bro_PROPN Morgannwg_PROPN ,_PUNCT Caerdydd_PROPN ,_PUNCT Rhondda_PROPN Cynon_PROPN Taf_PROPN ,_PUNCT Merthyr_PROPN )_PUNCT neu_CONJ rhwng_ADP lleoliadau_NOUN yn_PART ôl_NOUN yr_DET angen_NOUN
Dymunol_ADJ :_PUNCT
Y_PART gallu_VERB i_ADP fod_VERB yn_PART drefnus_ADJ ,_PUNCT yn_PART systematig_ADJ a_CONJ chyda_VERB chymhelliant_NOUN uchel_ADJ a_CONJ 'r_DET gallu_NOUN i_PART weithio_VERB 'n_PART rhan_NOUN o_ADP dîm_NOUN ond_CONJ gan_ADV weithio_VERB 'n_PART annibynnol_ADJ yn_PART rheolaidd_ADJ wrth_ADP gyflawni_VERB gweithgareddau_NOUN corfforol_ADJ parhaus_ADJ yn_ADP yr_DET awyr_NOUN agored_NOUN
Hyblygrwydd_NOUN o_ADP ran_NOUN agwedd_NOUN /_PUNCT trefniadau_NOUN gweithio_VERB i_ADP fodloni_VERB gofynion_NOUN darparu_VERB 'r_DET gwasanaeth_NOUN
