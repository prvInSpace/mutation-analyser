Templed_NOUN Rhestr_NOUN Eiddo_PROPN a_CONJ Reolir_VERB Asiantau_PROPN
Enw_NOUN 'r_DET Asiant_NOUN :_PUNCT
Nifer_NOUN yr_DET Eiddo_PROPN a_CONJ Reolir_PROPN :_PUNCT
Rhaid_VERB i_ADP chi_PRON ddiweddaru_VERB ac_CONJ uwchlwytho_VERB eich_DET Rhestr_NOUN Eiddo_PROPN a_CONJ Reolir_VERB o_ADP leiaf_ADJ unwaith_ADV bob_DET 12_NUM mis_NOUN ._PUNCT
Cofiwch_VERB gynnwys_VERB yr_DET eiddo_NOUN yr_PART ydych_AUX yn_PART ei_PRON reoli_VERB ar_ADP ran_NOUN eraill_ADJ yn_PART unig_ADJ -_PUNCT peidiwch_VERB â_ADP chynnwys_VERB eich_DET eiddo_NOUN eich_DET hun_NOUN neu_CONJ eiddo_NOUN a_CONJ osodir_VERB yn_PART unig_ADJ
Os_CONJ oes_VERB gennych_ADP eich_DET eiddo_NOUN eich_DET hun_PRON dylai_VERB 'r_DET rhain_PRON fod_VERB mewn_ADP Cofrestriad_NOUN Landlord_PROPN ar_ADP wahân_NOUN fel_ADP y_DET nodir_VERB yn_ADP Neddf_PROPN Tai_PROPN (_PUNCT Cymru_PROPN )_PUNCT 2014_NUM
Mae_VERB fflat_NOUN un_NUM ystafell_NOUN yn_PART uned_NOUN sengl_ADJ mewn_ADP eiddo_NOUN sy_AUX 'n_PART cynnwys_VERB lle_NOUN byw_ADJ /_PUNCT cysgu_VERB y_PART mae_VERB gan_ADP y_DET preswylydd_NOUN yr_DET hawl_NOUN i_ADP 'w_PRON feddiannu_VERB 'n_PART llwyr_ADJ ._PUNCT
Fodd_NOUN bynnag_PRON ,_PUNCT mae_AUX 'r_DET tenant_NOUN yn_PART rhannu_VERB rhai_DET cyfleusterau_NOUN â_ADP deiliaid_NOUN eraill_ADJ yn_ADP yr_DET adeilad_NOUN e_PRON ._PUNCT e_PRON ._PUNCT cegin_NOUN a_CONJ /_PUNCT neu_CONJ ystafell_NOUN ymolchi_PRON ._PUNCT
Eiddo_NOUN a_CONJ Reolir_PROPN
Rhaid_VERB i_ADP chi_PRON gwblhau_VERB 'r_DET ffurflen_NOUN hon_DET yn_ADP ei_DET chyfanrwydd_NOUN ._PUNCT
Bydd_VERB unrhyw_DET wybodaeth_NOUN goll_ADJ yn_PART arwain_VERB at_ADP oedi_VERB gyda_ADP 'ch_PRON cais_NOUN am_ADP drwydded_NOUN ._PUNCT
Enw_NOUN 'r_DET Landlord_NOUN :_PUNCT (_PUNCT Enw_NOUN llawn_ADJ os_CONJ gwelwch_VERB yn_PART dda_ADJ )_PUNCT
Cyfeiriad_NOUN Llawn_PROPN y_DET Landlord_NOUN :_PUNCT
Rhif_NOUN Ffôn_PROPN y_DET Landlord_NOUN :_PUNCT
Cyfeiriad_NOUN E_NUM -_PUNCT bost_NOUN y_DET Landlord_NOUN :_PUNCT (_PUNCT gadewch_VERB yn_PART wag_ADJ os_CONJ nad_PART oes_VERB e_PRON -_PUNCT bost_NOUN ar_ADP gael_VERB )_PUNCT
Cyfeiriad_NOUN yr_DET Eiddo_NOUN Rhent_X :_PUNCT
Cod_NOUN Post_NOUN yr_DET Eiddo_NOUN Rhent_X :_PUNCT
Ydych_AUX chi_PRON 'n_PART credu_VERB bod_VERB y_DET landlord_NOUN hwn_DET wedi_PART cofrestru_VERB ?_PUNCT
(_PUNCT Y_PART /_SYM N_NUM )_PUNCT
Os_CONJ ydych_AUX yn_PART rheoli_VERB eiddo_NOUN ar_ADP gyfer_NOUN perthnasau_NOUN yn_PART unig_ADJ ,_PUNCT nodwch_VERB eich_DET perthynas_NOUN i_ADP 'r_DET landlord_NOUN :_PUNCT
Yw_VERB 'r_DET eiddo_NOUN hwn_DET yn_ADP '_PUNCT Fflat_PROPN Un_NUM Ystafell_PROPN '_PUNCT ?_PUNCT
(_PUNCT Y_PART /_SYM N_NUM )_PUNCT
