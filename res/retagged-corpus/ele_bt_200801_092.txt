"_PUNCT *_SYM *_SYM *_SYM Late_PROPN Change_PROPN S4C_PROPN -_PUNCT Newid_NOUN Hwyr_ADJ S4C_PROPN *_SYM *_SYM *_SYM -_PUNCT Wythnos_PROPN 22_NUM "_PUNCT ,_PUNCT "_PUNCT *_SYM *_SYM Late_PROPN Change_PROPN S4C_PROPN -_PUNCT Newid_NOUN Hwyr_ADJ S4C_PROPN *_SYM *_SYM
Wythnos_VERB 22_NUM
28_NUM /_SYM 5_NUM /_SYM 20_NUM Dydd_NOUN Iau_PROPN
21_NUM :_PUNCT 00_NUM Dim_DET Newid_NOUN Lleisiau_NOUN Aur_PROPN y_DET 70_NUM '_PUNCT au_NUM
22_NUM :_PUNCT 05_NUM Amser_NOUN Newydd_ADJ Caru_VERB Siopa_PROPN
22_NUM :_PUNCT 40_NUM Amser_NOUN Newydd_ADJ Hansh_PROPN
23_NUM :_PUNCT 10_NUM Amser_NOUN Newydd_ADJ Eisteddfod_PROPN T_NUM (_PUNCT R_NUM )_PUNCT
00_NUM :_PUNCT 15_NUM Amser_NOUN Newydd_ADJ Diwedd_PROPN
22_NUM :_PUNCT 05_NUM
CARU_VERB SIOPA_PROPN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT SC_X )_PUNCT (_PUNCT AD_X )_PUNCT (_PUNCT HD_X )_PUNCT
Mae_VERB 'r_DET gyflwynwraig_NOUN ffasiwn_NOUN Lara_NOUN Catrin_PROPN yn_ADP Llanelli_PROPN y_DET tro_NOUN hwn_DET ,_PUNCT i_ADP gwrdd_VERB a_CONJ dwy_NUM sydd_VERB wrth_ADP eu_DET boddau_NOUN yn_PART prynu_VERB a_CONJ steilio_VERB dillad_NOUN ._PUNCT
Bydd_VERB Ffion_PROPN a_CONJ Hannah_PROPN yn_PART cael_VERB eu_DET herio_VERB i_ADP ddefnyddio_VERB cyllideb_NOUN o_ADP £_SYM 300_NUM er_ADP mwyn_NOUN prynu_VERB dillad_NOUN o_ADP siopau_NOUN elusen_NOUN ac_CONJ ail_ADJ -_PUNCT law_NOUN ,_PUNCT eu_PRON trawsnewid_VERB a_CONJ 'u_PRON gwerthu_VERB eto_ADV mewn_ADP ffair_NOUN '_PUNCT vintage_NOUN '_PUNCT ynghanol_ADP y_DET dref_NOUN ._PUNCT
Mae_VERB Ffion_PROPN yn_PART fyfyrwraig_NOUN cynllunio_VERB dillad_NOUN tra_CONJ bo_AUX Hannah_PROPN yn_PART modelu_VERB rhan_NOUN amser_NOUN ._PUNCT
Pwy_PRON ddaw_VERB i_ADP 'r_DET brig_NOUN ?_PUNCT
22_NUM :_PUNCT 40_NUM
HANSH_ADP (_PUNCT S_NUM )_PUNCT (_PUNCT HD_X )_PUNCT
Tiwns_INTJ ,_PUNCT comedi_NOUN a_CONJ lleisiau_NOUN ffres_ADJ ._PUNCT
Blas_NOUN o_ADP gynnwys_VERB arlein_NOUN @hanshs4c_NOUN ._PUNCT
23_NUM :_PUNCT 10_NUM
EISTEDDFOD_SYM T_NUM 2020_NUM (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT HD_X )_PUNCT
Eisteddfod_VERB T_NUM o_ADP 'r_DET ty_NOUN i_ADP ti_PRON !_PUNCT
Ymunwch_VERB â_ADP Trystan_NOUN Ellis_X Morris_PROPN ac_CONJ Heledd_NOUN Cynwal_PROPN am_ADP holl_DET uchafbwyntiau_NOUN pedwerydd_NOUN diwrnod_NOUN yr_DET eisteddfod_NOUN deledu_NOUN rithiol_ADJ gyntaf_ADJ erioed_ADV ._PUNCT
Hefyd_ADV heno_ADV ,_PUNCT cystadleuaethau_VERB Côr_PROPN Merched_PROPN /_PUNCT Bechgyn_PROPN Bl_PROPN ._PUNCT 7_NUM a_CONJ dan_ADP 25_NUM oed_NOUN a_CONJ Teulu_NOUN Talent_PROPN !_PUNCT
00_NUM :_PUNCT 15_NUM
DIWEDD_PROPN
29_NUM /_SYM 5_NUM /_SYM 20_NUM Dydd_NOUN Gwener_PROPN
21_NUM :_PUNCT 00_NUM Dim_DET Newid_NOUN Dyddiau_NOUN Da_ADJ
22_NUM :_PUNCT 05_NUM Amser_NOUN Newydd_ADJ '_PUNCT Run_NOUN Sbit_NOUN
22_NUM :_PUNCT 35_NUM Amser_NOUN Newydd_ADJ 35_NUM Diwrnod_PROPN
23_NUM :_PUNCT 35_NUM Amser_NOUN Newydd_ADJ Eisteddfod_PROPN T_NUM (_PUNCT R_NUM )_PUNCT
00_NUM :_PUNCT 40_NUM Amser_NOUN Newydd_ADJ Diwedd_PROPN
22_NUM :_PUNCT 05_NUM
'_PUNCT RUN_VERB SBIT_NOUN :_PUNCT Dwylo_NOUN Dros_ADP Amour_PROPN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT HD_X )_PUNCT
Mae_VERB addewid_NOUN meddwol_ADJ gan_ADP Linda_PROPN yn_PART golygu_VERB bod_AUX yn_PART rhaid_VERB i_ADP 'r_DET cwmni_NOUN ymddangos_VERB ar_ADP raglen_NOUN deledu_ADJ fyw_VERB a_CONJ dim_PRON ond_ADP tebygwr_NOUN Dafydd_PROPN Iwan_PROPN all_ADP helpu_VERB ._PUNCT
22_NUM :_PUNCT 35_NUM
35_NUM DIWRNOD_PROPN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT AD_X )_PUNCT (_PUNCT HD_X )_PUNCT
Mae_VERB 'r_DET criw_NOUN o_ADP ffrindiau_NOUN -_PUNCT ac_CONJ ambell_ADJ un_NUM ychwanegol_ADJ -_PUNCT yn_PART cyrraedd_VERB t_PRON ?_PUNCT
glan_ADJ môr_NOUN moethus_ADJ am_ADP benwythnos_NOUN Parti_PROPN Plu_PROPN Beth_PRON ond_CONJ dydi_AUX 'r_DET trefniadau_NOUN ddim_PART yn_PART rhedeg_VERB yn_PART llyfn_ADJ ._PUNCT
Gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrîn_NOUN ._PUNCT
23_NUM :_PUNCT 35_NUM
EISTEDDFOD_SYM T_NUM 2020_NUM (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT HD_X )_PUNCT
Eisteddfod_VERB T_NUM o_ADP 'r_DET ty_NOUN i_ADP ti_PRON !_PUNCT
Ymunwch_VERB â_ADP Trystan_NOUN Ellis_X Morris_PROPN ac_CONJ Heledd_NOUN Cynwal_PROPN am_ADP holl_DET uchafbwyntiau_NOUN diwrnod_NOUN olaf_ADJ yr_DET eisteddfod_NOUN deledu_NOUN rithiol_ADJ gyntaf_ADJ erioed_ADV ._PUNCT
Hefyd_ADV heno_ADV cystadleuaethau_VERB Côr_NOUN SATB_PROPN 14_NUM -_PUNCT 25_NUM oed_NOUN dros_ADP 20_NUM o_ADP leisiau_NOUN a_CONJ Teulu_NOUN Talent_PROPN !_PUNCT
00_NUM :_PUNCT 40_NUM
DIWEDD_PROPN
