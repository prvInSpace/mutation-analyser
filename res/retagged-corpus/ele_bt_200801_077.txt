"_PUNCT Coronafeirws_PROPN :_PUNCT Llythyr_PROPN i_ADP fusnesau_NOUN "_PUNCT ,_PUNCT "_PUNCT Cael_VERB trafferth_NOUN gweld_VERB yr_DET e_PRON -_PUNCT bost_NOUN hwn_DET ?_PUNCT
Darllenwch_VERB yn_ADP eich_DET porwr_NOUN enw_NOUN
19_NUM Mawrth_PROPN 2020_NUM
Cyllid_VERB enw_NOUN |_SYM
Dechrau_VERB a_CONJ Chynllunio_PROPN Busnes_PROPN enw_NOUN |_SYM
Marchnata_VERB enw_NOUN |_SYM
Sgiliau_NOUN a_CONJ Hyfforddiant_NOUN enw_NOUN |_SYM
Syniadau_NOUN Busnes_PROPN enw_NOUN |_SYM
TG_VERB enw_NOUN
Coronafeirws_NOUN :_PUNCT Llythyr_PROPN i_ADP fusnesau_NOUN enw_NOUN
Mae_AUX Gweinidog_NOUN yr_DET Economi_PROPN ,_PUNCT Trafnidiaeth_NOUN a_CONJ Gogledd_NOUN Cymru_PROPN ,_PUNCT Ken_PROPN Skates_PROPN ,_PUNCT wedi_PART cyhoeddi_VERB llythyr_NOUN i_ADP roi_VERB sicrwydd_NOUN i_ADP fusnesau_NOUN yn_PART sgil_NOUN yr_DET achosion_NOUN diweddar_ADJ o_ADP Coronafeirws_PROPN (_PUNCT Covid_PROPN -_PUNCT 19_NUM )_PUNCT ._PUNCT
Yn_PART dilyn_VERB y_DET pecyn_NOUN i_ADP fusnesau_NOUN werth_ADJ £_SYM 200_NUM m_NOUN a_CONJ chafodd_VERB ei_DET gyhoeddi_VERB yn_PART gynharach_ADJ wythnos_NOUN yma_DET mae_VERB yna_ADV nawr_ADV ychwanegiad_NOUN i_ADP 'r_DET pecyn_NOUN ac_CONJ mi_PART fydd_VERB yna_ADV nawr_ADV cymorth_NOUN gwerth_ADJ bron_ADV iawn_ADV i_ADP £_SYM 1.4_NUM biliwn_NUM ar_ADP gael_VERB i_ADP helpu_VERB i_ADP ddelio_VERB ag_ADP effaith_NOUN coronafeirws_NOUN ._PUNCT
Am_ADP y_DET wybodaeth_NOUN ddiweddaraf_ADJ ewch_VERB i_ADP dudalen_NOUN Cymorth_VERB Llywodraeth_PROPN i_ADP Fusnesau_PROPN enw_NOUN
Edrychwch_VERB ar_ADP wefan_NOUN Banc_PROPN Datblygu_VERB Cymru_PROPN enw_NOUN am_ADP wybodaeth_NOUN ynglŷn_ADP y_DET gwasanaethau_NOUN maen_VERB nhw_PRON yn_PART gynnig_NOUN
Darllenwch_VERB ganllawiau_NOUN Llywodraeth_PROPN y_DET DU_PROPN ar_ADP gyfer_NOUN cyflogwyr_NOUN a_CONJ busnesau_NOUN am_ADP Covid_PROPN -_PUNCT 19_NUM enw_NOUN
Darllenwch_VERB y_DET llythyr_NOUN yn_ADP ei_DET gyfanrwydd_NOUN isod_ADV :_PUNCT
