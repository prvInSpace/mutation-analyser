0.0_NOUN
Mm_CONJ da_ADJ iawn_ADV anti_ADP enwb_NOUN ond_CONJ beth_PRON am_ADP pysgodyn_NOUN neu_CONJ sglodyn_NOUN neu_CONJ bodyn_NOUN neu_CONJ nodyn_NOUN ?_PUNCT
Hmm_VERB beth_PRON am_ADP [_PUNCT clirio_VERB ]_PUNCT blodyn_NOUN siap_NOUN pysgodyn_NOUN hefo_ADP 'i_PRON fodyn_NOUN tra_CONJ 'n_PART byta_VERB sglodyn_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT Na_INTJ 'di_PART o_ADP ddim_PART yn_PART gweithio_VERB rhywsut_ADV nac_CONJ 'di_PART ._PUNCT
Ti_PRON 'n_PART iawn_ADJ enwg_VERB '_PUNCT dio_NOUN ddim_PART yn_PART iawn_ADJ [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
BeBe_PRON sy_AUX 'n_PART dod_VERB a_CONJ enwb_NOUN odli_VERB i_ADP [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT y_DET cloc_NOUN heddiw_ADV d'wa_ADJ ?_PUNCT
Wel_INTJ oni_NOUN 'n_PART meddwl_VERB bod_VERB angen_NOUN glanhau_VERB a_CONJ dystio_VERB chydig_ADJ ar_ADP yr_DET hen_ADJ le_NOUN 'ma_ADV a_CONJ w'rach_X 'na_ADV i_ADP symud_VERB y_DET dodrefn_NOUN hefyd_ADV [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ma'_VERBUNCT isio_ADV symud_VERB petha_NOUN o_ADP bryd_NOUN i_ADP gilydd_NOUN ti_PRON 'm_DET yn_PART meddwl_ADV enwg_VERB ?_PUNCT
Wel_CONJ fel_ADP byddai_AUX 'n_PART deud_VERB drwy_ADP 'r_DET amser_NOUN [_PUNCT saib_NOUN ]_PUNCT aros_VERB yn_PART llonydd_ADJ yn_PART rhy_ADV hir_ADJ ag_CONJ ei_DET di_PRON '_PUNCT im_NOUN i_ADP n'unlla_SYM ._PUNCT
<_SYM SS_NOUN >_SYM Ar_ADP fy_DET ngwir_ADJ +_SYM
Ar_ADP fy_DET llw_NOUN [_PUNCT saib_NOUN ]_PUNCT ngwir_ADJ [_PUNCT saib_NOUN ]_PUNCT [_PUNCT swn_NOUN hudol_ADJ yn_ADP y_DET cefndir_NOUN ]_PUNCT
[_PUNCT anadlu_VERB ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ble_ADV ges_VERB di_PRON 'r_DET blodyn_NOUN 'na_ADV enwb_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT
Beth_PRON ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT Pa_PART flodyn_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT
Hwnna_PRON sy_VERB yn_ADP dy_DET ddwylo_NOUN di_PRON [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
BeBe_PRON sy_AUX 'n_PART bod_VERB arno_ADP fe_PRON ?_PUNCT
Pam_ADV bod_VERB o_PRON 'n_PART <_SYM aneglur_ADJ 1_NUM >_SYM mor_ADV dost_VERB ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT
Ah_INTJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT plis_INTJ paid_VERB a_CONJ gweud_VERB wrth_ADP unrhyw_DET un_NUM amdano_ADP fe_PRON ._PUNCT
'_PUNCT wi_NOUN 'n_PART trial_ADJ ei_DET gwato_VERB fe_PRON fel_ADP bod_AUX neb_PRON yn_PART ei_PRON weld_VERB '_PUNCT e_PRON a_CONJ neb_PRON yn_PART gwneud_VERB niwed_NOUN iddo_ADP fe_PRON [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Mm_CONJ [_PUNCT saib_NOUN ]_PUNCT wel_CONJ mai_PART 'n_PART drueni_NOUN bod_VERB y_DET blodyn_NOUN bach_ADJ yn_PART edrych_VERB fel_ADP 'na_ADV enwb_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Lle_ADV '_PUNCT dachi_VERB 'n_PART cadw_ADV clytia'_VERBUNCT yn_ADP y_DET lle_NOUN 'ma_ADV ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
O_ADP helo_VERB enwb_NOUN bebe_NOUN sy_VERB gen_ADP ti_PRON yn_PART fana_ADV dwed_NOUN ?_PUNCT
dim_PRON [_PUNCT saib_NOUN ]_PUNCT wel_CONJ [_PUNCT ochneidio_VERB ]_PUNCT blodyn_NOUN [_PUNCT saib_NOUN ]_PUNCT '_PUNCT wi_NOUN 'n_PART credu_VERB fod_AUX e_PRON 'n_PART dost_VERB ._PUNCT
Ôô_NOUN bechod_ADV gad_VERB i_ADP mi_PRON weld_VERB o_PRON ._PUNCT
Na_INTJ [_PUNCT saib_NOUN ]_PUNCT '_PUNCT sneb_NOUN i_ADP fod_VERB i_PART weld_VERB '_PUNCT e_PRON [_PUNCT saib_NOUN ]_PUNCT oni_CONJ 'n_PART mynd_VERB i_ADP 'w_PRON gwato_VERB fe_PRON ._PUNCT
O_ADP na_PART enwb_NOUN paid_ADJ a_CONJ 'i_PRON guddio_VERB fo_PRON  _SPACE mwyn_NOUN i_ADP flodyn_NOUN dyfu_VERB 'n_PART iach_ADJ ac_CONJ yn_PART lliwgar_ADJ mae_AUX 'n_PART rhaid_VERB iddo_ADP fo_PRON weld_VERB yr_DET haul_NOUN ac_CONJ yfad_PART dwr_NUM ._PUNCT
A_PART dw_AUX i_PRON 'di_PART clywed_VERB fo_PRON '_PUNCT blode_NOUN 'n_PART teimlo_VERB 'n_PART hapusach_ADJ pan_CONJ mama_VERB nhw_PRON tu_NOUN allan_ADV [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
A_CONJ pham_ADV ddaw_VERB 'na_ADV awel_NOUN fach_ADJ heibio_ADV ma_VERB 'r_DET hada_NOUN [_PUNCT =_SYM ]_PUNCT bach_ADJ bach_ADJ bach_ADJ bach_ADJ [_PUNCT /=_PROPN ]_PUNCT bach_ADJ sy_VERB tu_NOUN mewn_ADP i_ADP 'r_DET blodyn_NOUN yn_PART hedfan_VERB i_ADP bob_DET man_NOUN a_CONJ wedyn_ADV un_NUM dau_NUM tri_NUM wir_ADV i_ADP ti_PRON mi_PRON fydd_VERB 'na_ADV floda'_NOUNUNCT bach_ADJ newydd_ADV sbon_ADV yn_PART tyfu_VERB [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
ââ_INTJ '_PUNCT wi_NOUN 'n_PART deall_VERB nawr_ADV a_CONJ fi_PRON i_ADP 'r_DET gegin_NOUN i_ADP nôl_VERB dwr_NUM iddo_ADP fe_PRON ._PUNCT
[_PUNCT ochneidio_VERB ]_PUNCT Ma_X '_PUNCT blodyn_NOUN bach_ADJ enwb_NOUN wedi_PART g'neud_VERB i_ADP 'n_PRON nhrwyn_NOUN i_ADP gosi_VERB a_CONJ i_PRON 'n_PART llygaid_NOUN i_PART rowlio_VERB a_CONJ ma_VERB 'n_PART mysadd_ADJ i_PRON 'n_PART dawnsio_VERB ma_PRON 'n_PART mhenglinia_VERB i_PRON 'n_PART cnocio_VERB mae_AUX 'n_PART nhraed_NOUN i_ADP 'n_PART mynd_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
BeBe_PRON am_ADP i_ADP ni_PRON gyfansoddi_VERB cân_NOUN am_PART adael_VERB i_ADP bethe_NOUN hardd_ADJ fel_ADP blode_NOUN dyfu_VERB yn_ADP yr_DET heulwen_NOUN tu_NOUN allan_ADV fel_CONJ bod_VERB pawb_PRON yn_ADP y_DET byd_NOUN [_PUNCT =_SYM ]_PUNCT mawr_ADJ [_PUNCT /=_PROPN ]_PUNCT mawr_ADJ yn_PART gallu_ADV gweld_VERB nhw_PRON [_PUNCT saib_NOUN ]_PUNCT at_ADP y_DET <_SYM aneglur_ADJ 1_NUM >_SYM ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT [_PUNCT swn_NOUN hudol_ADJ ]_PUNCT dewch_VERB ta_NOUN ._PUNCT
Pawb_PRON yn_PART barod_ADJ i_PART ddefro_VERB 'r_DET <_SYM aneglur_ADJ 1_NUM >_SYM ?_PUNCT
<_SYM SS_X >_SYM Yndan_PROPN ._PUNCT
enwb_VERB ydi_VERB 'r_DET blodyn_NOUN bach_ADJ yn_PART barod_ADJ ?_PUNCT
'_PUNCT Wi_PROPN 'n_PART credu_VERB ._PUNCT
Ôô_NOUN iawn_ADV ta_CONJ pawb_PRON yn_PART barod_ADJ ?_PUNCT
Un_NUM dau_NUM tri_NUM ._PUNCT
316.207_ADJ
Iawn_INTJ siŵr_ADV o_ADP dani_VERB 'm_DET isio_VERB ryw_DET hen_ADJ lwch_NOUN yn_PART gorwedd_VERB dros_ADP bob_DET dim_PRON nac_ADP '_PUNCT dan_ADP +_SYM
O_ADP na_PART [_PUNCT saib_NOUN ]_PUNCT aros_VERB yn_PART llonydd_ADJ yn_PART rhy_ADV hir_ADJ ag_CONJ ei_DET di_PRON '_PUNCT im_NOUN i_ADP nunlle_NOUN ar_ADP fy_DET ngwir_ADJ +_SYM
Ar_ADP fy_DET llw_NOUN ngwir_ADJ ._PUNCT
Wel_INTJ '_PUNCT sa_PART well_ADJ i_ADP mi_PRON throi_VERB hi_PRON rwan_DET dw_AUX i_PRON 'n_PART meddwl_VERB ._PUNCT
enwb_VERB fydde_VERB '_PUNCT ti_PRON 'n_PART fodlon_ADJ i_ADP ni_PRON blannu_VERB 'r_DET blodyn_NOUN yn_ADP dy_DET ardd_NOUN di_PRON fel_ADP bod_AUX e_PRON 'n_PART gallu_VERB tyfu_VERB 'n_PART hapus_ADJ a_CONJ gobeithio_VERB daw_VERB 'na_ADV ddigonedd_ADJ o_ADP flode_NOUN bach_ADJ eraill_ADJ o_ADP 'u_PRON hade_VERB fe_PRON ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT
Syniad_NOUN gwych_ADJ enwb_VERB wrth_ADP gwrs_NOUN ._PUNCT
Wel_INTJ hwyl_NOUN [_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Ie_INTJ enwb_NOUN fach_ADJ syniad_NOUN gwych_ADJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT tywydd_NOUN sych_ADJ a_CONJ gwlyb_ADJ a_CONJ gwyntog_ADJ [_PUNCT chwerthin_VERB ]_PUNCT wedyn_ADV mi_PART fydd_VERB y_DET plant_NOUN [_PUNCT =_SYM ]_PUNCT bach_ADJ [_PUNCT /=_PROPN ]_PUNCT bach_ADJ yn_ADP y_DET byd_NOUN [_PUNCT =_SYM ]_PUNCT mawr_ADJ [_PUNCT /=_PROPN ]_PUNCT mawr_ADJ yn_PART gallu_ADV gweld_VERB y_DET blode_NOUN hardd_ADJ yn_PART tyfu_VERB ar_ADP y_DET ffordd_NOUN i_ADP 'r_DET ysgol_NOUN ._PUNCT
639.875_ADJ
745.941_NOUN
