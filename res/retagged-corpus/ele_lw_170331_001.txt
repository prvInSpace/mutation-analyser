Cymorth_VERB Cyntaf_PROPN Rhieni_PROPN Newydd_ADJ
Cwrs_NOUN delfrydol_ADJ i_ADP unigolion_NOUN sy_AUX 'n_PART dymuno_ADV dysgu_VERB mwy_ADV am_ADP agweddau_NOUN sylfaenol_ADJ o_ADP Gymorth_NOUN Cyntaf_PROPN i_ADP fabis_NOUN a_CONJ phlant_NOUN bach_ADJ
Byddwch_AUX yn_PART dysgu_VERB sut_ADV i_ADP drin_VERB ac_CONJ ymateb_VERB i_ADP ddamweiniau_NOUN ac_CONJ anafiadau_NOUN cyffredin_ADJ
Ar_ADP orffen_VERB y_DET cwrs_NOUN yn_PART llwyddiannus_ADJ ,_PUNCT byddwch_VERB yn_PART derbyn_VERB tystysgrif_NOUN a_CONJ chymhwyster_NOUN Cymorth_VERB Cyntaf_PROPN Lefel_NOUN 3_NUM a_CONJ fydd_VERB yn_PART ddilys_ADJ am_ADP 3_NUM mlynedd_NOUN ._PUNCT
Cynhelir_VERB y_DET cwrs_NOUN trwy_ADP gyfrwng_NOUN y_DET Gymraeg_PROPN yn_PART swyddfa_NOUN Menter_PROPN Caerdydd_PROPN gan_ADP Reactive_X First_X Aid_X ._PUNCT
Croeso_NOUN i_ADP chi_PRON ddod_VERB a_CONJ babis_NOUN gyda_ADP chi_PRON ar_ADP y_DET cwrs_NOUN
10_NUM -_SYM 1_NUM pm_NOUN
£_SYM 40_NUM
Ebrill_NOUN 26_NUM 2017_NUM
am_ADP fwy_PRON o_ADP wybodaeth_NOUN -_PUNCT
cyfeiriad_NOUN -_PUNCT bost_NOUN
