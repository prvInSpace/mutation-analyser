"_PUNCT Coronafeirws_PROPN :_PUNCT Llythyr_PROPN i_ADP fusnesau_NOUN "_PUNCT ,_PUNCT "_PUNCT Cael_VERB trafferth_NOUN gweld_VERB yr_DET e_PRON -_PUNCT bost_NOUN hwn_DET ?_PUNCT
Darllenwch_VERB yn_ADP eich_DET porwr_NOUN enw_NOUN
9_NUM Ebrill_PROPN 2020_NUM
Cyllid_VERB enw_NOUN |_SYM
Dechrau_VERB a_CONJ Chynllunio_PROPN Busnes_PROPN enw_NOUN |_SYM
Marchnata_VERB enw_NOUN |_SYM
Sgiliau_NOUN a_CONJ Hyfforddiant_NOUN enw_NOUN |_SYM
Syniadau_NOUN Busnes_PROPN enw_NOUN |_SYM
TG_VERB enw_NOUN
Coronafeirws_NOUN :_PUNCT Canllawiau_NOUN i_ADP gyflogwyr_NOUN a_CONJ busnesau_NOUN enw_NOUN
Mae_AUX Gweinidog_NOUN yr_DET Economi_PROPN ,_PUNCT Trafnidiaeth_NOUN a_CONJ Gogledd_NOUN Cymru_PROPN ,_PUNCT Ken_PROPN Skates_PROPN ,_PUNCT wedi_PART cyhoeddi_VERB llythyr_NOUN i_ADP dynnu_VERB eich_DET sylw_NOUN at_ADP y_DET canllawiau_NOUN canlynol_ADJ a_CONJ ddiweddarwyd_VERB :_PUNCT
Canllawiau_NOUN i_ADP gyflogwyr_NOUN a_CONJ busnesau_NOUN ar_ADP coronafeirws_NOUN (_PUNCT COVID_PROPN -_PUNCT 19_NUM )_PUNCT ,_PUNCT enw_NOUN a_CONJ Covid_PROPN -_PUNCT 19_NUM -_PUNCT offer_NOUN diogelu_VERB personol_ADJ (_PUNCT PPE_NUM )_PUNCT ._PUNCT
enw_NOUN
Gellir_VERB dod_VERB o_ADP hyd_NOUN i_ADP 'r_DET wybodaeth_NOUN ddiweddaraf_ADJ am_ADP y_DET gefnogaeth_NOUN sydd_VERB ar_ADP gael_VERB i_ADP fusnesau_NOUN ar_ADP ein_DET tudalen_NOUN Covid_PROPN -_PUNCT 19_NUM :_PUNCT cymorth_NOUN i_ADP fusnes_VERB enw_NOUN ._PUNCT
Darllenwch_VERB y_DET llythyr_NOUN yn_ADP ei_DET gyfanrwydd_NOUN isod_ADV :_PUNCT
enw_NOUN
Gallwch_VERB ddiweddaru_VERB eich_DET tanysgrifiadau_NOUN ,_PUNCT newid_VERB eich_DET cyfrinair_NOUN neu_CONJ eich_DET cyfeiriad_NOUN e_PRON -_PUNCT bost_NOUN ,_PUNCT neu_CONJ ddileu_VERB tanysgrifiadau_NOUN ar_ADP unrhyw_DET adeg_NOUN drwy_ADP fewngofnodi_VERB i_ADP 'ch_PRON cyfrif_VERB enw_NOUN ._PUNCT
