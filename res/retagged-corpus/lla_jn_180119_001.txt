<_SYM S_NUM ?_PUNCT >_SYM [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Yr_DET ola'_NOUNUNCT i_PRON fynd_VERB amdani_ADP ydi_VERB enwb_NOUN ._PUNCT
Mae_VERB enwb_NOUN ar_ADP 'i_PRON ffordd_NOUN i_ADP fyny_ADV '_PUNCT wan_ADJ a_CONJ mae_VERB fatha_NOUN malwen_NOUN ._PUNCT
Dal_VERB i_ADP fynd_VERB enwb_VERB un_NUM cam_NOUN ar_ADP ôl_NOUN y_DET llall_PRON ._PUNCT
[_PUNCT dyheu_VERB ]_PUNCT O_ADP fy_DET Nuw_NOUN <_SYM ebychiad_NOUN >_SYM ._PUNCT
Dal_VERB i_ADP ddod_VERB i_ADP fyny_ADV enwb_NOUN ._PUNCT
Hai_PROPN ._PUNCT
Dal_VERB i_ADP ddod_VERB i_ADP fyny_ADV ._PUNCT
Dw_VERB i_PRON 'n_PART falch_ADJ o_ADP glywad_NOUN dy_DET lais_NOUN 'di_PART enwg_VERB ._PUNCT
Dal_VERB i_ADP ddod_VERB i_ADP fyny_ADV ._PUNCT
S'nam_NOUN llawer_PRON o_ADP m'byd_NOUN dw_VERB i_ADP ofn_NOUN ond_CONJ '_PUNCT swn_NOUN i_ADP 'm_DET yn_PART teimlo_VERB 'n_PART gyfforddus_ADJ iawn_ADV yn_PART llechio_VERB fy_DET hun_NOUN oddi_ADP ar_ADP adeilad_NOUN neu_CONJ g'neud_VERB '_PUNCT wbath_NOUN dw_AUX i_ADP ddim_PART wedi_PART g'neud_VERB o_ADP blaen_NOUN ._PUNCT
Ofn_PART disgyn_VERB ofn_NOUN brifo_VERB ia_PRON ._PUNCT
Genai_VERB gymaint_ADV o_ADP geg_NOUN sych_ADJ s'genoch_ADP chi_PRON 'm_DET syniad_NOUN de_NOUN ._PUNCT
O_ADP ie_INTJ ?_PUNCT
Tyd_VERB efo_ADP fi_PRON '_PUNCT wan_ADJ ta_CONJ ._PUNCT
Oce_INTJ ._PUNCT
Ti_PRON 'n_PART crynu_VERB fatha_NOUN deilan_NOUN ._PUNCT
Yndw_INTJ ?_PUNCT
Wyt_INTJ ._PUNCT
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Mae_VERB 'n_PART cymyd_VERB dipyn_PRON o_ADP amsar_NOUN i_ADP mi_PRON fedri_VERB ymddiried_VERB mewn_ADP rywun_NOUN ond_CONJ o_ADP dan_ADP amgylchiada_NOUN ella_ADV '_PUNCT sa_PRON rhaid_VERB i_ADP mi_PRON neud_VERB wedyn_ADV ma_VERB hyny_X 'n_PART '_PUNCT wbath_NOUN '_PUNCT sa_PRON gallu_VERB bod_VERB reit_NOUN heriol_ADJ ._PUNCT
Mewn_ADP munud_NOUN dw_AUX i_PRON 'n_PART mynd_VERB i_ADP gyfri_NOUN lawr_ADV tri_NUM dau_NUM un_NUM ._PUNCT
Dw_VERB i_PRON am_ADP ddeu'tha_VERB ch_PRON 'di_PART am_ADP gamu_VERB oce_CONJ so_PART tyd_NOUN a_CONJ dy_DET law_NOUN chwith_ADJ i_ADP fi_PRON ._PUNCT
Cerdda_VERB mlaen_NOUN yn_PART ara_VERB deg_NUM ._PUNCT
Tria_NOUN anadlu_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Oce_INTJ ._PUNCT
A_CONJ tria_NOUN ymlacio_VERB ._PUNCT
Ti_PRON 'n_PART gwasgu_VERB mysedd_NOUN i_ADP de_NOUN fatha_NOUN dynas_NOUN sy_AUX 'n_PART cael_VERB babi_NOUN cofia_NOUN ._PUNCT
[_PUNCT griddfan_NOUN ]_PUNCT ._PUNCT
Ara_ADV deg_NUM cyma_DET gam_NOUN bach_ADJ ymlaen_ADV ara_ADV deg_NUM ara_ADV deg_NUM tria_NOUN beidio_ADV crynnu_VERB ._PUNCT
Tria_NOUN ymlacio_VERB '_PUNCT chydig_ADJ bach_ADJ ryw_DET chwe_NUM modfedd_NOUN arall_ADJ i_PART fynd_VERB yn_ADP dy_DET flaen_NOUN ._PUNCT
Mlaen_PROPN ?_PUNCT
Iep_INTJ ._PUNCT
Oce_VERB gret_VERB yn_PART fan'na_PUNCT oce_NOUN ._PUNCT
Oce_VERB ti_PRON 'n_PART barod_ADJ ?_PUNCT
Yndw_INTJ ._PUNCT
Tri_PART dau_NUM un_NUM ._PUNCT
'Di_PART hynna_PRON 'n_PART syniad_NOUN da_ADJ enwg_VERB ?_PUNCT
Os_CONJ '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB am_ADP dro_NOUN drwy_ADP 'r_DET coed_NOUN '_PUNCT swn_AUX ni_PRON 'n_PART licio_VERB meddwl_VERB nad_PART ydi_AUX o_PRON 'n_PART rowlio_VERB fan'na_PUNCT wrth_ADP i_ADP ni_PRON gerdded_VERB dros_ADP ryw_DET foncyffion_NOUN a_CONJ hyn_PRON a_CONJ llall_ADV ?_PUNCT
So_NOUN os_CONJ 'di_PART o_ADP 'di_PART cael_VERB ei_PRON glymu_VERB mewn_ADP a_CONJ hi_PRON 'di_PART cael_VERB ei_DET clymu_VERB mewn_ADP sut_ADV ma_AUX nhw_PRON 'n_PART roi_VERB eu_DET dwylo_NOUN i_ADP fyny_ADV i_PART edrych_VERB ar_ADP ôl_NOUN eu_PRON gwyneba_VERB ?_PUNCT
Dyna_DET lle_NOUN ma_AUX nhw_PRON 'n_PART strapio_VERB fan_NOUN 'ma_ADV ar_ADP gefn_NOUN ffwtbal_NOUN rownd_ADV y_DET coesa_VERB ._PUNCT
Jyst_ADV y_PART coesa_VERB ?_PUNCT
Ia_INTJ ia_PRON
Jyst_ADV i_PRON roi_VERB ryw_DET fath_NOUN o_ADP stability_NOUN ._PUNCT
Neu_CONJ jyst_X rownd_ADV fan_NOUN 'ma_ADV ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'n_PART hapus_ADJ efo_ADP hynna_PRON ?_PUNCT
Yndan_VERB good_VERB points_NOUN ._PUNCT
Gyda_ADP phum_NOUN munud_NOUN ar_ADP ôl_NOUN mae_AUX enwg_VERB yn_PART profi_VERB 'r_DET stretcher_NOUN am_ADP y_DET tro_NOUN cynta_ADJ ._PUNCT
Un_NUM dau_NUM tri_NUM ._PUNCT
Eww_VERB ma_VERB hwnna_PRON 'n_PART un_NUM da_ADJ f'yna_ADV ._PUNCT
Yndi_ADV ?_PUNCT
I_ADP brofi_VERB 'r_DET stetchers_NOUN bydd_VERB y_DET ddau_NUM dim_PRON yn_PART cymryd_VERB rhan_NOUN mewn_ADP ras_NOUN o_ADP amgylch_NOUN y_DET goedwig_NOUN gan_CONJ gario_VERB un_NUM aelod_NOUN o_ADP 'i_PRON tîm_NOUN yn_ADP y_DET stretcher_NOUN ._PUNCT
Os_CONJ 'na_ADV isho_NOUN rywbeth_NOUN '_PUNCT da_ADJ ni_PRON 'n_PART gallu_ADV neud_VERB i_PART neud_VERB fo_PRON 'n_PART well_ADJ rhag_ADP ofn_NOUN ?_PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB bod_VERB o_PRON 'n_PART iawn_ADJ '_PUNCT sti_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Newn_ADP ni_PART adal_ADJ o_ADP fel_ADP mae_VERB o_ADP dw_AUX i_PRON 'n_PART meddwl_VERB ia_PRON ?_PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB ia_PRON ._PUNCT
BeBe_NOUN da_ADJ chi_PRON 'n_PART feddwl_VERB ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB ._PUNCT
Da_ADJ iawn_ADV '_PUNCT gia_NOUN ._PUNCT
Ia_NOUN da_ADJ iawn_ADV enwg_VERB ._PUNCT
Da_ADJ iawn_ADV pawb_PRON de_NOUN ._PUNCT
Da_ADJ iawn_ADV pawb_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ie_INTJ briliant_VERB ._PUNCT
Dw_AUX i_PRON 'n_PART credu_VERB bod_VERB nhw_PRON '_PUNCT da_ADJ lot_PRON o_ADP ffydd_NOUN yn_ADP y_DET stretcher_NOUN yma_DET ond_CONJ pe_CONJ bydde_VERB un_NUM o_ADP 'r_DET brige_NOUN 'na_ADV 'n_PART torri_VERB wel_ADV ma_VERB 'r_DET sialens_NOUN drosodd_ADV i_ADP 'r_DET tîm_NOUN 'ma_ADV ._PUNCT
Felly_CONJ dw_AUX i_PRON 'n_PART credu_VERB bod_VERB nhw_PRON bach_ADJ rhy_ADV cwl_NOUN ar_ADP hyn_PRON o_ADP bryd_NOUN ._PUNCT
Oce_VERB bois_VERB erbyn_ADP '_PUNCT wan_ADJ de_NOUN swn_VERB ni_PRON 'n_PART disgwl_ADJ bobo_AUX chi_PRON 'n_PART testio_VERB fo_PRON a_CONJ ddim_PART yn_PART adeiladu_VERB o_ADP ._PUNCT
Oce_VERB '_PUNCT da_ADJ chi_PRON 'di_PART cael_VERB chwarter_NOUN awr_NOUN i_ADP neud_VERB hynny_PRON so_VERB pum_NUM munud_NOUN s'genach_X chi_PRON ar_ADP ôl_NOUN oce_NOUN ._PUNCT
Pum_ADJ munud_NOUN genoch_VERB chi_PRON ar_ADP ôl_NOUN ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'n_PART mynd_VERB i_PART gychwyn_VERB jyst_X ._PUNCT
Bydd_VERB enwg_ADV neud_VERB o_ADP edrych_VERB yn_PART proper_NOUN proffesiynol_ADJ ._PUNCT
Ma_AUX hwnna_PRON 'n_PART mynd_VERB i_ADP fod_VERB yn_PART ryw_DET grefftwaith_NOUN yndi_ADJ ._PUNCT
Bydd_VERB o_ADP fatha_NOUN gwely_NOUN ._PUNCT
Os_CONJ 'di_PART nhw_PRON 'm_DET yn_PART ._PUNCT
Bydd_VERB o_PRON 'n_PART brafiach_ADJ 'na_ADV bebe_NOUN da_ADJ ni_PRON 'n_PART gysgu_VERB arno_ADP fo_PRON ._PUNCT
Na_INTJ mae_VERB 'n_PART iawn_ADJ chi_PRON fel'a_SYM ._PUNCT
Jyst_ADV testiwch_VERB y_PART raffa_VERB i_ADP weld_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Munud_NOUN ar_ADP ôl_NOUN ._PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Stopio_VERB nhw_PRON ti_PRON 'n_PART neud_VERB rili_ADV ._PUNCT
Ma_AUX nhw_PRON 'n_PART rhoi_VERB dail_NOUN a_CONJ bob_DET dim_PRON ._PUNCT
Bydd_AUX hwnna_PRON drososdd_ADV ond_CONJ newn_ADP ni_PRON jyst_ADV 'i_PRON godi_VERB fo_PRON gynta_ADJ ._PUNCT
Sut_ADV ma_VERB hwnna_PRON 'n_PART edrach_ADJ ?_PUNCT
Ti_PRON 'n_PART teimlo_VERB 'n_PART wan_ADJ unman_ADV ?_PUNCT
<_SYM S_NUM ?_PUNCT >_SYM Na_INTJ iawn_INTJ ._PUNCT
Gorfadd_VERB yn_PART ôl_NOUN gorfadd_ADJ ._PUNCT
Paid_VERB a_CONJ 'i_PRON falu_VERB o_ADP [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Ffwr_NOUN a_CONJ ni_PRON [_PUNCT griddfan_NOUN ]_PUNCT Oce_X iawn_INTJ ._PUNCT
Oce_VERB bois_VERB ._PUNCT
'_PUNCT Na_VERB fo_PRON amser_NOUN drosodd_ADV ._PUNCT
1432.981_NUM
