Crynodeb_NOUN Pobol_ADJ y_DET Cwm_NOUN W_NUM 29_NUM
Ar_ADP ôl_NOUN sylweddoli_VERB bod_VERB angen_NOUN iddo_ADP weithio_VERB 'n_PART galed_ADJ er_ADP mwyn_NOUN bod_VERB yn_PART rhan_NOUN o_ADP fywyd_NOUN Arwen_PROPN ,_PUNCT mae_AUX Macs_PROPN yn_PART gwneud_VERB ei_DET orau_NOUN i_PART brofi_VERB i_ADP Ffion_PROPN ei_DET fod_VERB eisiau_NOUN bod_VERB yn_PART dad_NOUN iddi_ADP ._PUNCT
Mae_AUX Ffion_PROPN yn_PART cytuno_VERB i_PART adael_VERB iddo_ADP weld_VERB Arwen_PROPN am_ADP ychydig_ADJ oriau_NOUN gyda_ADP 'r_DET nos_NOUN ,_PUNCT ond_CONJ pan_PRON mae_AUX Gethin_PROPN yn_PART dychwelyd_VERB adref_ADV i_ADP Gysgod_NOUN y_DET Glyn_NOUN ,_PUNCT nid_PART yw_VERB 'n_PART hapus_ADJ i_PART weld_VERB y_DET ddau_NUM yn_PART agosáu_VERB ._PUNCT
Ar_ADP ôl_NOUN ystyried_VERB yn_PART ddwys_ADJ ,_PUNCT mae_AUX Elgan_PROPN yn_PART cael_VERB traed_VERB oer_ADJ ynglŷn_ADP â_ADP symud_VERB i_ADP Gaeredin_PROPN ,_PUNCT a_CONJ heb_ADP drafod_VERB gyda_ADP Gaynor_PROPN mae_AUX 'n_PART gwrthod_VERB y_DET swydd_NOUN er_ADP mwyn_NOUN cael_VERB aros_VERB gyda_ADP hi_PRON yng_ADP Nghwmderi_PROPN ._PUNCT
Mae_AUX 'n_PART rhaid_VERB i_ADP Linda_PROPN gyfaddef_VERB ei_DET bwriad_NOUN hirdymor_NOUN ar_ADP ôl_NOUN i_ADP Cadno_PROPN ddod_VERB o_ADP hyd_NOUN i_ADP 'w_PRON thabledi_NOUN ._PUNCT
I_ADP atal_VERB unrhyw_DET un_NUM arall_ADJ rhag_CONJ dod_VERB o_ADP hyd_NOUN iddynt_ADP ,_PUNCT mae_AUX Cadno_PROPN yn_PART cytuno_VERB i_ADP 'w_PRON cuddio_VERB ._PUNCT
Ond_CONJ pan_CONJ ddaw_VERB Sara_PROPN a_CONJ Bobi_VERB draw_ADV i_ADP Benrhewl_PROPN mae_AUX Bobi_PROPN 'n_PART llwyddo_VERB i_PART gael_VERB gafael_VERB ar_ADP y_DET tabledi_NOUN sy_VERB 'n_PART dychryn_NOUN Cadno_PROPN ._PUNCT
Pobol_ADJ y_DET Cwm_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 8.00_NUM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Pob_DET prynhawn_NOUN Sul_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Ar_ADP alw_VERB :_PUNCT s4c.cymru_NOUN ;_PUNCT BBC_X iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cynhyrchiad_NOUN BBC_NOUN Cymru_PROPN
