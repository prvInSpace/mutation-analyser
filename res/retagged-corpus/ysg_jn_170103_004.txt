At_ADP :_PUNCT [_PUNCT BANC_PROPN ]_PUNCT
[_PUNCT CYFEIRIAD_PROPN ]_PUNCT
Annwyl_NOUN Syr_NOUN /_PUNCT Madam_PROPN
CYF_X :_PUNCT CYFRIF_NOUN YMDDIRIEDOLAETH_NUM RHIF_PROPN :_PUNCT [_PUNCT TEITL_PROPN Y_DET CYFRIF_NOUN ]_PUNCT (_PUNCT "_PUNCT y_DET Cyfrif_PROPN "_PUNCT )_PUNCT
Mae_AUX 'r_DET arian_NOUN sydd_AUX yn_PART cael_VERB ei_PRON gadw_VERB o_ADP bryd_NOUN i_ADP 'w_PRON gilydd_NOUN yn_ADP y_DET Cyfrif_PROPN yn_PART cael_VERB ei_DET ddal_VERB ar_ADP ymddiried_VERB er_ADP budd_NOUN S4C_PROPN ._PUNCT
Yr_PART ydym_VERB drwy_ADP hyn_PRON yn_PART -_PUNCT awdurdodi_VERB :_PUNCT
(_PUNCT i_ADP )_PUNCT S4C_PROPN i_ADP dderbyn_VERB copïau_NOUN o_ADP 'r_DET cyfrifon_NOUN sy_VERB 'n_PART berthnasol_ADJ i_ADP 'r_DET Cyfrif_PROPN gan_ADP y_DET Banc_PROPN ;_PUNCT
(_PUNCT ii_NUM )_PUNCT S4C_PROPN i_PRON wneud_VERB cais_NOUN un_NUM ai_CONJ ar_ADP y_DET ffôn_NOUN neu_CONJ mewn_ADP ysgrifen_NOUN i_ADP 'r_DET Banc_PROPN i_ADP gael_ADV gwybod_VERB beth_PRON yw_VERB fantol_NOUN y_DET Cyfrif_NOUN ar_ADP y_DET pryd_NOUN ;_PUNCT
(_PUNCT iii_NUM )_PUNCT y_DET Banc_PROPN i_ADP ryddhau_VERB i_ADP S4C_PROPN copïau_NOUN o_ADP bob_DET cyfrifen_NOUN sy_VERB 'n_PART berthnasol_ADJ i_ADP 'r_DET Cyfrif_PROPN ;_PUNCT
(_PUNCT iv_INTJ )_PUNCT y_DET Banc_PROPN i_ADP roi_VERB i_ADP S4C_PROPN unrhyw_DET wybodaeth_NOUN sy_VERB 'n_PART berthnasol_ADJ i_ADP fantol_NOUN y_DET Cyfrif_NOUN ar_ADP y_DET pryd_NOUN un_NUM ai_CONJ ar_ADP y_DET ffôn_NOUN neu_CONJ yn_PART ysgrifenedig_ADJ ;_PUNCT
(_PUNCT v_ADP )_PUNCT S4C_PROPN i_PART gymryd_VERB drosodd_ADV rheolaeth_NOUN o_ADP 'r_DET Cyfrif_NOUN lle_NOUN bo_AUX S4C_PROPN yn_PART cymryd_VERB drosodd_ADV y_DET cyfrifoldeb_NOUN o_ADP gynhyrchu_VERB 'r_DET Rhaglen_PROPN (_PUNCT ni_PRON )_PUNCT y_PART mae_AUX 'r_DET Cyfrif_PROPN yn_PART perthyn_VERB iddi_ADP /_PUNCT ynt_NOUN ._PUNCT
A_PART wnewch_VERB chi_PRON gadarnhau_VERB os_CONJ gwelwch_VERB yn_PART dda_ADJ bod_VERB yr_DET arian_NOUN a_CONJ gedwir_VERB yn_ADP y_DET Cyfrif_PROPN yn_PART cael_VERB ei_DET ddal_VERB ar_ADP ymddiried_VERB er_ADP budd_NOUN S4C_PROPN ac_CONJ ni_PART fydd_AUX y_DET Banc_PROPN yn_PART ceisio_VERB gweithredu_VERB yn_PART erbyn_ADP unrhyw_DET arian_NOUN a_CONJ gedwir_VERB yn_ADP y_DET Cyfrif_PROPN unrhyw_DET hawl_NOUN i_ADP wrth_ADP gyfrif_NOUN neu_CONJ unrhyw_DET hawliad_NOUN sydd_VERB gan_ADP naill_PRON ai_CONJ 'r_DET Banc_PROPN neu_CONJ unrhyw_DET drydydd_NOUN parti_NOUN yn_PART erbyn_ADP [_PUNCT Enw_NOUN 'r_DET Cyflenwr_NOUN ]_PUNCT a_CONJ /_PUNCT neu_CONJ 'r_DET rhai_PRON sy_AUX 'n_PART rhedeg_VERB y_DET Cyfrif_PROPN ._PUNCT
Yn_PART gywir_ADJ
Rydym_AUX ni_PRON 'n_PART cytuno_VERB ac_CONJ yn_PART cadarnhau_VERB 'r_DET uchod_NOUN ac_CONJ yn_PART cytuno_VERB i_ADP weithredu_VERB 'n_PART unol_ADJ â_ADP 'r_DET cyfarwyddiadau_NOUN uchod_ADJ ._PUNCT
ARWYDDWYD_VERB GAN_NUM
._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT
DROS_VERB AC_X AR_X RAN_PROPN
[_PUNCT Y_DET Banc_PROPN ]_PUNCT
