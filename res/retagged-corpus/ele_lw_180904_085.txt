Do_INTJ diolch_INTJ ,_PUNCT roedd_VERB hi_PRON 'n_PART hynod_ADJ o_ADP braf_ADJ am_ADP y_DET pedwar_NOUN diwrnod_NOUN yn_ADP enw_NOUN ,_PUNCT mae_AUX 'n_PART teimlo_VERB braidd_ADV fel_ADP oes_VERB yn_PART ™_ADP l_CONJ erbyn_ADP hyn_PRON ._PUNCT
O_ADP ran_NOUN rhagor_PRON o_ADP waith_NOUN ,_PUNCT byddaf_AUX i_ADP ffwrdd_ADV yng_ADP enw_NOUN fory_ADV ac_CONJ yn_PART ™_ADP l_NUM fore_ADJ dydd_NOUN Gwener_PROPN ._PUNCT
Ond_CONJ mae_VERB gen_ADP i_PRON ryw_DET dair_NUM mil_NUM o_ADP eiriau_NOUN ar_ADP ™_ADP l_PRON i_PART 'w_PRON cyfieithu_VERB o_ADP adroddiad_NOUN deunaw_NUM mil_NUM ,_PUNCT ac_CONJ angen_NOUN edrych_VERB drosto_ADP cyn_CONJ bydd_VERB gwaith_NOUN prawf_NOUN ddarllen_VERB arall_ADJ yn_PART cyrraedd_VERB ddydd_NOUN Mawrth_PROPN ._PUNCT ._PUNCT ._PUNCT felly_ADV cwpwl_ADP o_ADP ddarnau_NOUN 'r_DET prynhawn_NOUN 'ma_ADV 'n_PART unig_ADJ ,_PUNCT a_CONJ dweud_VERB y_DET gwir_NOUN gan_ADP fod_VERB angen_NOUN egwyl_NOUN o_ADP 'r_DET adroddiad_NOUN ._PUNCT
Dau_NUM ddarn_NOUN ,_PUNCT falle_ADV ?_PUNCT
Braidd_ADJ yn_PART flinedig_ADJ a_CONJ dweud_VERB y_DET gwir_NOUN ,_PUNCT sydd_AUX ddim_PART yn_PART argoeli_VERB 'n_PART dda_ADJ ._PUNCT
Gobeithio_VERB bod_VERB hyn_PRON o_ADP ryw_NOUN gymorth_NOUN ._PUNCT
Diolch_INTJ ._PUNCT
Hwyl_INTJ ,_PUNCT
enw_NOUN
