"_PUNCT Gwybodaeth_PROPN ,_PUNCT Cyngor_PROPN ,_PUNCT Arweiniad_NOUN #_ADJ Covid19_PROPN "_PUNCT ,_PUNCT "_PUNCT Cael_VERB trafferth_NOUN gweld_VERB yr_DET e_PRON -_PUNCT bost_NOUN hwn_DET ?_PUNCT
Darllenwch_VERB yn_ADP eich_DET porwr_NOUN enw_NOUN
5_NUM Mehefin_PROPN 2020_NUM
Cyllid_VERB enw_NOUN |_SYM
Dechrau_VERB a_CONJ Chynllunio_PROPN Busnes_PROPN enw_NOUN |_SYM
Marchnata_VERB enw_NOUN |_SYM
Sgiliau_NOUN a_CONJ Hyfforddiant_NOUN enw_NOUN |_SYM
Syniadau_NOUN Busnes_PROPN enw_NOUN |_SYM
TG_VERB enw_NOUN
Coronafeirws_NOUN :_PUNCT y_DET wybodaeth_NOUN a_CONJ 'r_DET cymorth_NOUN busnes_NOUN diweddaraf_ADJ enw_NOUN
Y_DET cyngor_NOUN ,_PUNCT y_DET canllawiau_NOUN a_CONJ 'r_DET pecynnau_NOUN cymorth_NOUN busnes_NOUN diweddaraf_ADJ ar_ADP gael_VERB ,_PUNCT i_ADP 'ch_PRON helpu_VERB i_ADP reoli_VERB effaith_NOUN Covid_PROPN -_PUNCT 19_NUM ._PUNCT
enw_NOUN
Dychwelyd_VERB i_ADP 'r_DET gwaith_NOUN yn_PART ddiogel_ADJ ._PUNCT
Canllawiau_NOUN i_ADP gyflogwyr_NOUN yng_ADP Nghymru_PROPN enw_NOUN
Yn_PART dibynnu_VERB ar_ADP y_DET math_NOUN o_ADP fusnes_NOUN yr_PART ydych_AUX yn_PART ei_PRON redeg_VERB ,_PUNCT bydd_VERB nifer_NOUN o_ADP faterion_NOUN i_ADP 'w_PRON hystyried_VERB wrth_ADP sicrhau_ADV bod_VERB eich_DET busnes_NOUN yn_PART ail-_VERB ddechrau_VERB a_CONJ sicrhau_ADV bod_AUX pob_DET gweithiwr_NOUN yn_PART gallu_VERB dychwelyd_VERB i_ADP 'r_DET gwaith_NOUN yn_PART ddiogel_ADJ ._PUNCT
enw_NOUN
Ymestyn_VERB y_DET Cynllun_NOUN Cymorth_VERB Incwm_PROPN i_ADP 'r_DET Hunangyflogedig_PROPN drwy_ADP gyfnod_NOUN Coronafeirws_PROPN a_CONJ ffyrlo_NOUN -_PUNCT cadarnhau_VERB 'r_DET camau_NOUN nesaf_ADJ enw_NOUN
Cyhoeddwyd_VERB manylion_NOUN ail_ADJ grant_NOUN a_CONJ 'r_DET olaf_ADJ drwy_ADP 'r_DET Cynllun_NOUN Cymorth_VERB Incwm_PROPN i_ADP 'r_DET Hunangyflogedig_PROPN wedi_PART 'i_PRON gapio_VERB ar_ADP £_SYM 6,570_NUM ,_PUNCT a_CONJ manylion_NOUN strwythur_NOUN y_DET cynllun_NOUN ffyrlo_NOUN yn_ADP ystod_NOUN y_DET misoedd_NOUN nesaf_ADJ ._PUNCT
enw_NOUN
Profi_VERB ,_PUNCT Olrhain_PROPN ,_PUNCT Diogelu_PROPN :_PUNCT canllawiau_NOUN i_ADP gyflogwyr_NOUN enw_NOUN
Mae_AUX 'n_PART egluro_VERB sut_ADV y_PART gall_VERB cyflogwyr_NOUN yng_ADP Nghymru_PROPN chwarae_VERB eu_DET rhan_NOUN i_ADP helpu_VERB i_ADP wireddu_VERB strategaeth_NOUN Cymru_PROPN ar_ADP gyfer_NOUN Profi_PROPN ,_PUNCT Olrhain_PROPN ,_PUNCT Diogelu_PROPN er_ADP mwyn_NOUN arafu_VERB lledaeniad_NOUN y_DET feirws_NOUN ,_PUNCT diogelu_VERB ein_DET systemau_NOUN iechyd_NOUN a_CONJ gofal_NOUN ac_CONJ achub_VERB bywydau_NOUN ._PUNCT
enw_NOUN
Adnodd_VERB Asesu_PROPN Risg_PROPN COVID_PROPN -_PUNCT 19_NUM i_ADP 'r_DET Gweithlu_X Cymru_PROPN gyfan_ADJ enw_NOUN
Mae_AUX 'r_DET Adnodd_PROPN yn_PART gofyn_VERB nifer_NOUN o_ADP gwestiynau_NOUN sydd_AUX wedi_PART 'u_PRON llunio_VERB i_ADP nodi_VERB os_CONJ yw_VERB rhywun_NOUN mewn_ADP perygl_NOUN uwch_ADJ o_ADP Covid_PROPN -_PUNCT 19_NUM ._PUNCT
enw_NOUN
Dydd_NOUN Sadwrn_PROPN y_DET Busnesau_NOUN Bach_ADJ 2020_NUM enw_NOUN
Mae_AUX Dydd_NOUN Sadwrn_PROPN y_DET Busnesau_NOUN Bach_ADJ unwaith_ADV eto_ADV eleni_ADV yn_PART tynnu_VERB sylw_NOUN at_ADP 100_NUM o_ADP fusnesau_NOUN bach_ADJ ,_PUNCT un_NUM y_DET diwrnod_NOUN am_ADP y_DET 100_NUM diwrnod_NOUN sy_AUX 'n_PART arwain_VERB at_ADP Ddydd_NOUN Sadwrn_PROPN y_DET Busnesau_NOUN Bach_ADJ 5_NUM Rhagfyr_NOUN 2020_NUM ._PUNCT
Ceisiadau_NOUN nawr_ADV ar_ADP agor_VERB !_PUNCT
enw_NOUN
Clare_VERB Walters_PROPN Healing_PROPN enw_NOUN
Dyma_DET hanes_NOUN arallgyfeirio_VERB cyflym_ADJ un_NUM busnes_NOUN yn_PART ystod_NOUN y_DET cloi_VERB mawr_ADJ ._PUNCT
