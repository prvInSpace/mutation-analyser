Cân_VERB T_NUM ._PUNCT Llew_PROPN
Rhwng_ADP bob_DET gair_NOUN a_CONJ rhwng_ADP y_DET clorie_NOUN ,_PUNCT
Rhwng_ADP bob_DET eiliad_NOUN ,_PUNCT rhwng_ADP yr_DET orie_NOUN ,_PUNCT
Ymhob_ADP llun_NOUN ac_CONJ yn_ADP y_DET lliwie_NOUN Y_PART mae_VERB hud_NOUN y_DET dewin_NOUN geirie_NOUN ._PUNCT
Ymhob_ADP ton_NOUN ac_CONJ ar_ADP bob_DET comin_NOUN ,_PUNCT
Mae_AUX 'r_DET gwynt_NOUN yn_PART sibrwd_VERB enw_NOUN rhywun_NOUN ,_PUNCT Ymhob_ADP bwthyn_NOUN ,_PUNCT ymhob_ADP ogof_NOUN ,_PUNCT Enw_PROPN nad_CONJ aiff_VERB byth_ADV yn_PART angof_NOUN !_PUNCT
Cytgan_PROPN
Mae_VERB 'n_PART gawr_ADJ ._PUNCT
Mae_VERB 'n_PART arwr_NOUN ._PUNCT
Ni_PART welwn_VERB ei_PRON debyg_ADJ ._PUNCT
Un_NUM glew_ADJ ,_PUNCT T_NUM ._PUNCT Llew_PROPN ,_PUNCT Brenin_NOUN dychymyg_NOUN !_PUNCT
Cwtsio_VERB 'n_PART braf_ADJ o_ADP dan_ADP y_DET cwrlid_NOUN ,_PUNCT
Stori_NOUN dda_ADJ ddaw_ADJ â_ADP breuddwyd_NOUN ,_PUNCT
Breuddwyd_VERB ddaw_VERB â_ADP 'r_DET chwarae_VERB fory_ADV ,_PUNCT Chwarae_PROPN ddaw_VERB â_ADP geiriau_NOUN 'r_DET stori_NOUN !_PUNCT
Geiriau_NOUN dathlu_VERB ,_PUNCT geiriau_NOUN tirion_NOUN ,_PUNCT
Geiriau_NOUN rhannu_VERB ,_PUNCT geiriau_NOUN 'r_DET galon_NOUN ,_PUNCT Geiriau_NOUN cariad_NOUN ,_PUNCT geiriau_NOUN chwerthin_VERB ,_PUNCT Geiriau_NOUN 'n_PART hiaith_NOUN a_CONJ geiriau_NOUN perthyn_NOUN !_PUNCT
Cytgan_PROPN
Nos_NOUN da_ADJ leuad_NOUN ,_PUNCT nos_NOUN da_ADJ heulwen_NOUN ,_PUNCT
Nos_NOUN da_ADJ gwmwl_NOUN ,_PUNCT nos_NOUN da_ADJ seren_NOUN ,_PUNCT
Nos_NOUN da_ADJ ddewin_NOUN chwedlau_ADJ llawen_NOUN ,_PUNCT Nos_NOUN da_ADJ T_NUM ._PUNCT Llew_PROPN ,_PUNCT nos_NOUN da_ADJ awen_NOUN !_PUNCT
Cytgan_PROPN
Dwynwen_NOUN Lloyd_PROPN Llywelyn_PROPN
Fy_DET ngardd_NOUN fach_ADJ i_ADP
Mae_VERB gen_ADP i_PRON ardd_NOUN o_ADP flodau_NOUN
Sy_VERB 'n_PART lliwgar_ADJ yn_ADP yr_DET haf_NOUN ,_PUNCT A_CONJ 'r_DET coed_NOUN yn_PART drwm_ADJ a_CONJ gwyrdd_ADJ gan_ADP ddail_NOUN ,_PUNCT A_CONJ 'r_DET haul_NOUN yn_PART t_PRON '_PUNCT wynnu_VERB 'n_PART braf_ADJ ._PUNCT
Bydd_VERB dail_NOUN y_DET coed_NOUN yn_PART disgyn_VERB
Yn_PART felyn_ADJ ,_PUNCT brown_ADJ a_CONJ choch_ADJ ,_PUNCT
Pan_CONJ ddaw_VERB hen_ADJ wynt_NOUN yr_DET hydref_NOUN I_ADP chwipio_VERB ar_ADP fy_DET moch_NOUN ._PUNCT
A_CONJ phan_PRON ddaw_VERB 'r_DET gwanwyn_NOUN heibio_ADV ,_PUNCT
Daw_VERB blodau_NOUN tlws_NOUN di_PRON -_PUNCT ri_NOUN ,_PUNCT
A_CONJ dail_NOUN bach_ADJ melfed_NOUN ar_ADP y_DET coed_NOUN I_PART lenwi_VERB '_PUNCT ngardd_NOUN fach_ADJ i_PRON ._PUNCT
Lis_NOUN Jones_PROPN
Eurgain_PROPN
Ti_PRON yw_VERB 'r_DET Athro_NOUN sydd_AUX yn_PART gwybod_VERB
Am_ADP ofidiau_NOUN cudd_ADJ dan_ADP glo_NOUN ,_PUNCT
Ac_CONJ yn_PART barod_ADJ iawn_ADV i_PART wrando_VERB
Ar_ADP weddïau_NOUN mud_ADJ bob_DET tro_NOUN ,_PUNCT
Ti_PRON yw_VERB 'r_DET Athro_NOUN A_CONJ fydd_VERB gennyf_ADP gydol_ADJ f'oes_NOUN ._PUNCT
Ti_PRON yw_VERB 'r_DET ffrind_NOUN sydd_VERB yno_ADV 'n_PART gyson_ADJ ,_PUNCT
Er_CONJ mor_ADV wamal_ADJ ydwyf_VERB fi_PRON ,_PUNCT
Rwyt_VERB ti_PRON 'n_PART ffyddlon_ADJ er_ADP bob_DET cweryl_NOUN ,_PUNCT
Trechu_VERB 'r_DET bwli_NOUN a_CONJ wnei_VERB di_PRON ,_PUNCT
Ffrind_PROPN wyt_VERB beunydd_ADV A_PART fydd_VERB gennyf_ADP gydol_ADJ f'oes_NOUN ._PUNCT
Ti_PRON sy_VERB 'n_PART dad_NOUN a_CONJ mam_NOUN i_ADP lawer_ADV
Pan_CONJ fo_AUX teulu_NOUN 'n_PART mynd_VERB ar_ADP drai_NOUN ,_PUNCT
Pan_CONJ fo_AUX clymau_NOUN 'n_PART dechrau_VERB datod_VERB
Ti_PRON rydd_NOUN gysur_NOUN heb_ADP weld_VERB bai_NOUN ,_PUNCT
Ti_PRON yw_VERB 'm_DET teulu_NOUN A_CONJ fydd_VERB gennyf_ADP gydol_ADJ f'oes_NOUN ._PUNCT
Rwyf_VERB yn_PART swil_ADJ i_ADP dy_PRON gydnabod_VERB
Yn_ADP y_DET byd_NOUN sydd_AUX heddiw_ADV 'n_PART bod_VERB ,_PUNCT
Pan_CONJ fo_VERB arwyr_NOUN ar_ADP deledu_NOUN
A_CONJ 'r_DET maes_NOUN chwarae_AUX 'n_PART mynnu_VERB clod_ADJ ,_PUNCT
Arwr_NOUN cyson_ADJ ,_PUNCT Ti_PRON fydd_VERB gennyf_ADP gydol_ADJ f'oes_NOUN ._PUNCT
Pan_CONJ fo_VERB gwersi_NOUN anodd_ADJ bywyd_NOUN
Yn_PART achosi_VERB rhwystrau_NOUN lu_ADV ,_PUNCT
Dy_DET lawlyfr_NOUN sydd_VERB yn_PART cynnig_ADJ
Ateb_VERB eglur_ADJ i_ADP bob_DET cri_NOUN ,_PUNCT
Yn_ADP y_DET Beibl_NOUN Ceir_VERB arweiniad_NOUN gydol_ADJ oes_VERB ._PUNCT
Gillian_VERB Jones_PROPN
