Unrhyw_DET wers_NOUN arall_ADJ heddiw_ADV ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
lleoliad_NOUN ._PUNCT
Pob_DET un_NUM ._PUNCT
Trw_NOUN 'r_DET dydd_NOUN ._PUNCT
ym_ADP ._PUNCT
Dim_DET y_DET wers_NOUN olaf_ADJ ?_PUNCT
Ôô_NOUN ._PUNCT
Dim_PART cofrestru_VERB ._PUNCT
Ww_ADP ie_INTJ ._PUNCT
Na_INTJ so_AUX wi_NOUN 'n_PART mynd_VERB ar_ADP ôl_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_CONJ pwnc_NOUN ._PUNCT
pwnc_NOUN ._PUNCT
Gyda_ADP Mr_NOUN cyfenw_NOUN ._PUNCT
Ww_ADP ie_INTJ [_PUNCT -_PUNCT ]_PUNCT Ma_X '_PUNCT fe_PRON 'n_PART gwers_NOUN fi_PRON 'n_PART hoffi_VERB ond_CONJ mae_VERB yn_PART ._PUNCT
Os_CONJ angen_NOUN i_ADP rywun_NOUN fynd_VERB i_ADP ôl_NOUN enwg_VERB nawr_ADV fel_CONJ bod_AUX e_PRON 'n_PART gweld_VERB le_NOUN mae_VERB un_NUM fe_NOUN plis_INTJ ?_PUNCT
S6_VERB ?_PUNCT [_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Un_NUM o_ADP chi_PRON jyst_ADV fel_CONJ bod_VERB [_PUNCT -_PUNCT ]_PUNCT bod_AUX e_PRON 'n_PART gwbod_VERB lle_ADV mae_VERB un_NUM fe_NOUN wedyn_ADV ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Felly_CONJ nawr_ADV mama_NOUN fe_PRON 'n_PART poethi_VERB 'r_DET gwely_NOUN i_ADP fyny_ADV i_ADP saith_NUM deg_NUM gradd_NOUN celsiws_NOUN ._PUNCT
[_PUNCT S_NUM ?_PUNCT n_DET peiriant_NOUN yn_PART gweithio_VERB ]_PUNCT ._PUNCT
A_CONJ wedyn_ADV fydd_AUX e_PRON 'n_PART poethi_VERB yr_DET darn_NOUN metel_NOUN  _SPACE mwyn_NOUN toddi_VERB 'r_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT S_NUM ?_PUNCT n_DET peiriant_NOUN yn_PART gweithio_VERB ]_PUNCT ._PUNCT
Plastic_PROPN ._PUNCT
[_PUNCT S_NUM ?_PUNCT n_DET peiriant_NOUN yn_PART gweithio_VERB ]_PUNCT ._PUNCT
Pwy_PRON sy_AUX 'n_PART cofio_VERB sut_ADV o'dd_NOUN yr_DET [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Peiriant_AUX yn_PART gweithio_VERB ?_PUNCT
<_SYM SG_VERB >_X Why_PROPN ?_PUNCT
Sut_ADV mae_AUX 'r_DET haenau_NOUN yn_PART gweithio_VERB ?_PUNCT
Pwy_PRON sy_AUX 'n_PART cofio_VERB ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
[_PUNCT Peiriant_PROPN yn_PART gweithio_VERB ]_PUNCT ._PUNCT
Iawn_ADV enwg_VERB ?_PUNCT
Ie_INTJ ._PUNCT
Pwy_PRON sy_AUX 'n_PART cofio_VERB ?_PUNCT
Beth_PRON ?_PUNCT
Gweda_VERB sut_ADV mae_AUX 'r_DET haenau_NOUN 'n_PART gweithio_VERB enwg_VERB ._PUNCT
O_ADP fi_PRON 'n_PART meddwl_VERB ti_PRON 'n_PART neud_VERB e_PRON yn_PART order_VERB ._PUNCT
Na_INTJ ._PUNCT
Sut_ADV mae_AUX 'r_DET haenau_NOUN yn_PART gweithio_VERB [_PUNCT -_PUNCT ]_PUNCT ar_ADP y_DET peiriant_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Beth_PRON sy_AUX 'n_PART digwydd_VERB i_ADP 'r_DET plastic_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
S7_VERB ?_PUNCT Mae_AUX 'n_PART meltio_VERB ._PUNCT
Beth_PRON yw_VERB hwnna_PRON yn_ADP Gymraeg_PROPN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM SG_VERB >_SYM Twitch_PROPN ._PUNCT
<_SYM SS_X >_SYM Toddi_VERB ._PUNCT
Toddi_VERB ok_X ._PUNCT
<_SYM SG_VERB >_SYM Watching_X sioe_NOUN ._PUNCT
So_PART mae_AUX 'n_PART torri_VERB ac_CONJ yna_ADV mae_AUX e_PRON yn_PART caledu_VERB mewn_ADP i_ADP haenau_NOUN ac_CONJ yn_PART adeiladu_VERB yr_DET gwaith_NOUN tri_NUM ddimensiwn_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
So_PART beth_PRON awen_NOUN ar_ADP reit_NOUN ._PUNCT
So_NOUN wedyn_ADV mama_NOUN fe_PRON 'n_PART paratoi_VERB ._PUNCT
[_PUNCT S_NUM ?_PUNCT n_DET peiriant_NOUN yn_PART gweithio_VERB ]_PUNCT ._PUNCT
Ac_CONJ mae_VERB newydd_ADV anfon_VERB y_DET gwybodaeth_NOUN draw_ADV i_ADP 'r_DET peiriant_NOUN [_PUNCT -_PUNCT ]_PUNCT a_CONJ mama_NOUN 'na_ADV pum_NUM deg_NUM haen_ADV i_ADP gyd_ADP ._PUNCT
[_PUNCT S_NUM ?_PUNCT n_DET peiriant_NOUN yn_PART gweithio_VERB ]_PUNCT ._PUNCT
A_PART mama_AUX nhw_PRON 'n_PART mynd_VERB i_PART gymryd_VERB pedwar_NOUN deg_NUM naw_NUM munud_NOUN ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
A_CONJ faint_ADV o'dd_AUX e_PRON 'n_PART costio_VERB ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
<_SYM SS_X >_SYM Un_NUM deg_NUM tri_NUM ceiniog_NOUN ._PUNCT
Un_NUM deg_NUM tri_NUM ceiniog_NOUN mama_NOUN fe_PRON 'n_PART costio_VERB i_ADP neud_VERB pob_DET un_NUM ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Beth_PRON di_PRON 'r_DET rhan_NOUN ni_PRON 'n_PART neud_VERB i_ADP jyst_ADV yn_PART un_NUM deg_NUM tri_NUM ceiniog_NOUN pob_DET amser_NOUN ti_PRON 'n_PART defnyddio_VERB fe_PRON ?_PUNCT
Na_INTJ ._PUNCT
Mae_AUX 'n_PART dibynnu_VERB faint_NOUN o_ADP plastic_NOUN sy_AUX 'n_PART cael_VERB ei_PRON ddefnyddio_VERB ._PUNCT
So_PART mama_NOUN -_PUNCT fe_PART mama_NOUN fe_PRON 'n_PART  _SPACE cyfrifo_VERB faint_NOUN o_ADP plastic_NOUN sydd_VERB i_ADP bob_DET un_NUM ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
so_VERB mae_AUX 'n_PART dibynnu_VERB ar_ADP maint_NOUN y_DET gwrthrych_NOUN a_CONJ sawl_DET un_NUM ohonyn_ADP nhw_PRON sydd_VERB ._PUNCT
So_PART mama_NOUN hwn_DET nawr_ADV yn_PART cynhesu_VERB lan_ADV i_ADP dau_NUM gant_NOUN a_CONJ phump_NUM gradd_NOUN celsiws_NOUN ._PUNCT
[_PUNCT Peiriant_PROPN yn_PART cynhesu_VERB ]_PUNCT ._PUNCT
Mae_AUX 'n_PART cynhesu_VERB eitha'_ADJUNCT cloi_VERB ._PUNCT
[_PUNCT Peiriant_PROPN yn_PART cynhesu_VERB ]_PUNCT ._PUNCT
Pwy_PRON sy_AUX 'n_PART cofio_VERB sawl_ADV [_PUNCT -_PUNCT ]_PUNCT ydy_AUX e_PRON 'n_PART neud_VERB haen_NOUN pob_DET un_NUM gynta'_ADJUNCT neu_CONJ ydy_AUX e_PRON 'n_PART neud_VERB pob_DET ciwb_NOUN [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART ar_ADP wahân_NOUN ?_PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
enwg_VERB ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
Mae_AUX 'n_PART neud_VERB haenau_NOUN i_ADP nhw_PRON wedyn_ADV mynd_VERB ar_ADP yr_DET un_NUM nesaf_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ]_PUNCT ._PUNCT
Mae_AUX 'n_PART neud_VERB haen_NOUN un_NUM pob_DET un_NUM gyntaf_ADJ ._PUNCT
Wedyn_ADV yn_PART adeiladu_VERB lan_ADV yr_DET haenau_NOUN Ok_X so_X nawr_ADV mae_VERB e_PRON yn_PART [_PUNCT -_PUNCT ]_PUNCT cael_VERB gwared_NOUN ar_ADP gwastraff_NOUN [_PUNCT -_PUNCT ]_PUNCT yn_PART gyntaf_ADJ ._PUNCT
[_PUNCT S_NUM ?_PUNCT n_DET peiriant_NOUN yn_PART gweithio_VERB ]_PUNCT ._PUNCT
A_PART gwirio_VERB bod_VERB e_PRON yn_PART ddigon_ADJ  _SPACE o_ADP feddal_ADJ bod_VERB y_DET [_PUNCT -_PUNCT ]_PUNCT bod_AUX e_PRON 'di_PART mynd_VERB mewn_ADP i_ADP hylif_NOUN ._PUNCT
[_PUNCT Peiriant_PROPN yn_PART cael_VERB ei_PRON ddefnyddio_VERB ]_PUNCT ._PUNCT
Reit_AUX so_PRON yw_VERB pwy_PRON yw_VERB 'r_DET un_NUM cynta'_ADJUNCT ?_PUNCT
Fi_PRON ._PUNCT
enwg_VERB ._PUNCT
enwg_VERB ._PUNCT
[_PUNCT Peiriant_NOUN yn_PART argraffu_VERB ]_PUNCT ._PUNCT
A_CONJ 'r_DET un_NUM nesaf_ADJ ?_PUNCT
enwb_VERB ._PUNCT
[_PUNCT Peiriant_NOUN yn_PART argraffu_VERB ]_PUNCT ._PUNCT
Ok_VERB unrhyw_DET gwestiwn_NOUN am_ADP hwnna_PRON nawr_ADV ?_PUNCT
Na_INTJ ._PUNCT
Ok_ADV so_PART bydd_VERB rhaid_VERB chi_PRON ddod_VERB nol_NOUN amser_NOUN cinio_NOUN i_ADP bigo_VERB nhw_PRON lan_ADV ._PUNCT
Iawn_INTJ ._PUNCT
Ok_INTJ ?_PUNCT
Ie_INTJ ._PUNCT
Reit_VERB ewn_ADP ni_PRON nol_NOUN nawr_ADV ten_ADJ ._PUNCT
Newn_ADP ni_PRON adael_VERB hwnna_PRON i_ADP dorri_VERB tra_CONJ bod_VERB ni_PRON [_PUNCT -_PUNCT ]_PUNCT chi_PRON 'n_PART cario_VERB '_PUNCT mlaen_NOUN gyda_ADP 'ch_PRON camau_NOUN nawr_ADV ._PUNCT
[_PUNCT saib_NOUN ]_PUNCT ._PUNCT
275.958_VERB
