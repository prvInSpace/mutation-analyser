0.0_NOUN
Mae_VERB 'na_ADV bethe_NOUN '_PUNCT rhyfedd_ADJ iawn_ADV yn_PART digwydd_VERB ar_ADP yr_DET ynys_NOUN yma_ADV ._PUNCT
Ambell_ADJ waith_NOUN pan_CONJ fo_VERB 'r_DET môr_NOUN yn_PART arw_ADJ mae_AUX 'r_DET tonnau_NOUN yn_PART codi_VERB a_CONJ mae_AUX 'r_DET pysgod_NOUN yn_PART neidio_VERB o_ADP 'r_DET môr_NOUN ._PUNCT
Felly_CONJ yn_PART syml_ADJ iawn_ADV y_DET dasg_NOUN i_ADP chi_PRON fydd_VERB dal_ADJ y_DET pysgod_NOUN ac_CONJ os_CONJ daliwch_VERB chi_PRON ugain_NUM o_ADP bysgod_NOUN mi_PART gewch_VERB chi_PRON weld_VERB y_DET map_NOUN cyntaf_ADJ ._PUNCT
Ydych_AUX chi_PRON 'n_PART deall_VERB ?_PUNCT
Yn_PART bendant_ADJ capten_NOUN ._PUNCT
Gewni_VERB weld_VERB enwg_VERB cychwyn_VERB y_DET cloc_NOUN ac_CONJ i_ADP ffwrdd_ADV â_ADP chi_PRON ._PUNCT
Pob_DET lwc_NOUN ._PUNCT
Os_CONJ daliwch_VERB chi_PRON bysgodyn_NOUN dewch_VERB â_ADP fo_PRON i_ADP fi_PRON ar_ADP frys_NOUN ._PUNCT
Ia_INTJ un_NUM ._PUNCT
Faint_ADV s'gen_NOUN ti_PRON ?_PUNCT
Dau_NUM [_PUNCT -_PUNCT ]_PUNCT
Genai_VERB dau_NUM ._PUNCT
Tri_INTJ ._PUNCT
ôô_NOUN da_ADJ iawn_ADV ._PUNCT
Pedwar_PROPN ._PUNCT
Pump_NUM ._PUNCT
Chwech_VERB enwg_VERB tyd_NOUN â_ADP nhw_PRON i_ADP fi_PRON ._PUNCT
Saith_INTJ ._PUNCT
Wyth_INTJ ._PUNCT
Naw_INTJ ._PUNCT
Deg_INTJ ._PUNCT
Un_NUM deg_NUM un_NUM ._PUNCT
Un_NUM deg_NUM dau_NUM ._PUNCT
BeBe_AUX s'gen_NOUN ti_PRON enwb_NOUN ?_PUNCT
Un_NUM deg_NUM tri_NUM ._PUNCT
Un_NUM deg_NUM pedwar_NUM ._PUNCT
'_PUNCT Da_ADJ chi_PRON 'n_PART agos_ADJ iawn_ADV mae_AUX 'r_DET pysgod_NOUN yn_PART mynd_VERB yn_PART brin_ADJ ._PUNCT
'_PUNCT Dach_VERB chi_PRON angen_NOUN chwech_NUM arall_ADJ ._PUNCT
Rhaid_VERB chi_PRON frysio_VERB enwb_NOUN mama_NOUN un_NUM ._PUNCT
Un_NUM deg_NUM saith_NUM ._PUNCT
Tri_NOUN arall_ADJ ._PUNCT
Gofalus_ADJ gofalus_ADJ ._PUNCT
Un_NUM deg_NUM wyth_NUM ._PUNCT
Un_NUM deg_NUM naw_NUM ._PUNCT
Un_NUM arall_ADJ ._PUNCT
A_CONJ dyma_ADV ni_PRON ._PUNCT
Ugain_NUM o_ADP bysgod_NOUN ._PUNCT
'_PUNCT Dach_AUX chi_PRON wedi_PART llwyddo_VERB ._PUNCT
[_PUNCT sgrechian_VERB ]_PUNCT Yay_PROPN ._PUNCT
Llongyfarchiade_VERB '_PUNCT ._PUNCT
Dewch_VERB â_ADP 'r_DET rhwydi_VERB i_ADP fi_PRON ._PUNCT
Ac_CONJ mi_PART gewch_VERB chi_PRON weld_VERB y_DET map_NOUN cynta'_ADJUNCT yn_ADP y_DET cwt_NOUN ._PUNCT
Iawn_ADV dyma_ADV ni_PRON fap_NOUN o_ADP 'r_DET ynys_NOUN ac_CONJ ar_ADP y_DET map_NOUN mae_VERB 'na_ADV groes_NOUN sy_AUX 'n_PART dangos_VERB ble_ADV mae_AUX 'r_DET allwedd_NOUN yn_PART cuddio_VERB ._PUNCT
Felly_CONJ fôr_NOUN -_PUNCT ladron_VERB ble_ADV mae_VERB 'r_DET allwedd_NOUN ?_PUNCT
Ble_ADV [_PUNCT -_PUNCT ]_PUNCT
Yn_ADP fana_ADV ._PUNCT
Fana_ADV ?_PUNCT
Ar_ADP y_DET lanfa_NOUN ?_PUNCT
Ia_INTJ ._PUNCT
Ewch_VERB i_ADP chwilio_VERB ._PUNCT
Gobeithio_VERB bobo_VERB chi_PRON 'n_PART gywir_ADJ neu_CONJ fydd_VERB 'na_ADV ddim_PART trysor_NOUN i_ADP chi_PRON enwg_VERB lle_NOUN wyt_AUX ti_PRON 'n_PART mynd_VERB ?_PUNCT
Wel_INTJ bendibwmbwls_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB bobo_VERB nhw_PRON ar_ADP goll_ADJ ._PUNCT
Yay_PROPN ._PUNCT
'_PUNCT Dach_VERB chi_PRON 'di_PART ga'l_VERB o_PRON ?_PUNCT
Do_INTJ ._PUNCT
'_PUNCT Nôl_ADV â_ADP chi_PRON felly_ADV ._PUNCT
Hei_INTJ hei_PRON hei_X hei_X ._PUNCT
Da_ADJ iawn_ADV fôr_ADP -_PUNCT ladron_VERB ._PUNCT
Dyma_ADV ni_PRON yr_DET allwedd_NOUN gyntaf_ADJ ._PUNCT
'_PUNCT Na_VERB i_ADP roi_VERB hon_DET i_ADP enwbg_NOUN edrych_VERB ar_ADP ei_DET hôl_NOUN hi_PRON ._PUNCT
Llongyfarchiade_VERB '_PUNCT ._PUNCT
'_PUNCT Mlaen_PROPN â_ADP ni_PRON i_ADP 'r_DET ail_ADJ dasg_NOUN ._PUNCT
Ahoi_INTJ ._PUNCT
[_PUNCT Cerddoriaeth_NOUN yn_PART chwarae_VERB ]_PUNCT ._PUNCT
Yn_ADP y_DET dasg_NOUN yma_DET fydd_VERB rhaid_VERB i_ADP chi_PRON arwain_VERB y_DET bêl_NOUN yma_DET o_ADP 'r_DET hen_ADJ ganon_NOUN yn_PART fan_NOUN hyn_DET i_ADP 'r_DET gist_NOUN ar_ADP y_DET traeth_NOUN ond_CONJ heb_ADP gyffwrdd_NOUN y_DET bêl_NOUN ._PUNCT
Gewch_VERB chi_PRON ddefnyddio_VERB popeth_PRON sydd_VERB ar_ADP y_DET llawr_NOUN ar_ADP y_DET traeth_NOUN ._PUNCT
Dyma_ADV ni_PRON y_DET cynnig_NOUN cynta'_ADJUNCT ._PUNCT
I_ADP ffwr_NOUN '_PUNCT â_ADP chi_PRON ._PUNCT
370.432_NOUN
Ww_ADP bach_ADV [_PUNCT -_PUNCT ]_PUNCT
Ia_INTJ ._PUNCT
'_PUNCT Ei_DET da_ADJ iawn_ADV wir_ADV ._PUNCT
Da_ADJ iawn_ADV ._PUNCT
Daliwch_VERB i_ADP fynd_VERB enwg_VERB a_CONJ ww_DET llyw_NOUN y_DET llong_NOUN oedd_VERB honna_NOUN ._PUNCT
'_PUNCT Ei_DET da_ADJ iawn_ADV '_PUNCT da_ADJ chi_PRON 'n_PART dod_VERB yn_PART nes_ADJ ._PUNCT
enwb_VERB '_PUNCT wan_ADJ ._PUNCT
Gist_NOUN [_PUNCT -_PUNCT ]_PUNCT
Gist_NOUN yn_PART lle_NOUN ?_PUNCT
Ie_INTJ wir_ADJ '_PUNCT da_ADJ chi_PRON hanner_NOUN ffordd_NOUN ._PUNCT
'_PUNCT Da_ADJ chi_PRON 'n_PART gweithio_VERB 'n_PART wych_ADJ fel_ADP tîm_NOUN da_ADJ iawn_ADV chi_PRON ._PUNCT
Llyw_VERB y_DET llong_NOUN ._PUNCT
Lle_ADV mae_VERB 'r_DET llall_PRON ?_PUNCT
Ww_PART canon_VERB oedd_VERB hwnna_PRON ._PUNCT
Hwna_INTJ ._PUNCT
Ie_INTJ wir_ADJ ._PUNCT
Tri_ADV ar_ADP ôl_NOUN ._PUNCT
Fi_PRON nesa'_ADJUNCT ._PUNCT
Neu_PART fydd_AUX enwg_VERB yn_PART gweiddi_VERB efo_ADP 'i_PRON amser_NOUN ar_ADP ben_NOUN ._PUNCT
Cleddyf_ADP ia_PRON ._PUNCT
Dau_NUM arall_ADJ ._PUNCT
Dewch_VERB o_ADP na_PART ._PUNCT
Rhaid_VERB chi_PRON frysio_VERB ._PUNCT
Brysiwch_VERB ._PUNCT
Brysiwch_VERB ôô_NOUN mor_ADV agos_ADJ ._PUNCT
Ie_INTJ wir_ADV un_NUM arall_ADJ ._PUNCT
Un_NUM arall_ADJ ._PUNCT
ôô_NOUN o'dd_NOUN honna_VERB 'n_PART agos_ADJ ._PUNCT
Da_ADJ iawn_ADV fôr_ADP -_PUNCT ladron_VERB ._PUNCT
'_PUNCT Dach_AUX chi_PRON wedi_PART llwyddo_VERB eto_ADV ._PUNCT
Felly_CONJ dewch_VERB i_PART weld_VERB y_DET map_NOUN nesaf_ADJ ._PUNCT
Felly_CONJ fôr_NOUN -_PUNCT ladron_VERB ble_ADV mae_VERB 'r_DET allwedd_NOUN ?_PUNCT
Yn_ADP y_DET tywod_NOUN ._PUNCT
Yn_ADP y_DET tywod_NOUN ._PUNCT
Yn_PART lle_NOUN yn_ADP y_DET tywod_NOUN ?_PUNCT
Fana_PROPN ._PUNCT
Fana_PROPN ._PUNCT
I_ADP ffwr_NOUN '_PUNCT â_ADP chi_PRON i_PART chwilio_VERB ._PUNCT
Pob_DET lwc_NOUN ._PUNCT
Gobeithio_VERB bobo_VERB chi_PRON 'n_PART iawn_ADJ neu_CONJ fydd_VERB 'na_ADV ddim_DET trysor_NOUN ._PUNCT
Rhaid_VERB i_ADP chi_PRON frysio_VERB neu_CONJ fydd_VERB hi_PRON 'n_PART nos_NOUN ._PUNCT
Dyma_ADV fo_PRON Yay_PROPN ._PUNCT
Dw_AUX i_PRON 'n_PART clywed_VERB sŵn_NOUN yn_PART ôl_NOUN â_ADP chi_PRON ._PUNCT
Da_ADJ iawn_ADV wir_ADV ._PUNCT
Ble_ADV mae_VERB 'r_DET allwedd_NOUN ?_PUNCT
Gen_PART enwg_VERB ._PUNCT
Bendigedig_ADJ ._PUNCT
Dyma_ADV hi_PRON ._PUNCT
Yr_DET allwedd_NOUN ._PUNCT
'_PUNCT Na_VERB i_ADP roid_VERB hon_DET efo_ADP 'r_DET llall_NOUN i_ADP enwbg_NOUN ._PUNCT
A_CONJ mama_NOUN 'na_ADV un_NUM dasg_NOUN ar_ADP ôl_NOUN ._PUNCT
Ond_CONJ cyn_ADP hynny_PRON '_PUNCT da_ADJ ni_PRON 'n_PART mynd_VERB i_ADP ga'l_VERB hoe_NOUN ._PUNCT
624.999_VERB
955.947_VERB
