<_SYM img_VERB /_PUNCT >_SYM
O_ADP !_PUNCT
Na_VERB beth_PRON i_PART 'w_PRON wneud_VERB yn_ADP y_DET glaw_NOUN ?_PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT ._PUNCT
GLAW_PROPN GLAW_PROPN GLAW_PROPN OND_CONJ peidiwch_VERB a_CONJ phoeni_VERB dyma_DET gipolwg_NOUN o_ADP 'r_DET digwyddiadau_NOUN Cymraeg_PROPN i_ADP blant_NOUN ,_PUNCT pobl_NOUN ifanc_ADJ ,_PUNCT teuluoedd_NOUN ac_CONJ oedolion_NOUN sydd_VERB ar_ADP eich_DET stepen_NOUN drws_NOUN chi_PRON ._PUNCT
Heno_PROPN
Daeth_VERB Gorffennaf_PROPN ar_ADP ein_DET gwarthaf_VERB a_CONJ hanner_NOUN nosweithiau_NOUN enw_NOUN am_ADP eleni_ADV wedi_PART gwibio_VERB heibio_ADV yn_PART llawn_ADJ hwyl_NOUN ._PUNCT
Y_DET nos_NOUN Wener_NOUN yma_DET cawn_VERB y_DET fraint_NOUN o_ADP groesawu_VERB un_NUM o_ADP gewri_VERB y_DET byd_NOUN gwerin_ADJ yn_ADP Ewrop_PROPN ,_PUNCT enwb1_NOUN cyfenw1_ADJ __NOUN ,_PUNCT i_ADP __NOUN __NOUN ._PUNCT
Daeth_VERB y_DET llais_NOUN cyfoethog_ADJ i_ADP amlygrwydd_NOUN gyntaf_ADJ gyda_ADP 'r_DET enw_NOUN ,_PUNCT ond_CONJ dros_ADP gyfnod_NOUN o_ADP chwarter_NOUN canrif_NOUN cawson_VERB 9_NUM albwm_NOUN unigryw_ADJ o_ADP hyfrydwch_VERB pur_ADJ ._PUNCT
Mae_AUX ei_DET chyngherddau_NOUN byw_ADJ bob_DET amser_NOUN yn_PART wefreiddiol_ADJ ac_CONJ rwy_VERB 'n_PART siŵr_ADJ y_PART bydd_VERB nos_NOUN Wener_NOUN yr_DET un_NUM modd_NOUN ._PUNCT
Felly_CONJ ,_PUNCT gobeithio_VERB y_PART caiff_VERB y_DET enw_NOUN groeso_NOUN mwyn_NOUN gennym_ADP nos_NOUN Wener_NOUN ._PUNCT
Fel_CONJ y_DET gwyddoch_VERB ,_PUNCT mae_VERB 'n_PART noson_NOUN acwstig_ADJ mewn_ADP ystafell_NOUN glos_ADJ sydd_VERB ond_CONJ yn_PART dal_ADJ oddeutu_ADP 50_NUM o_ADP bobl_NOUN ._PUNCT
Os_CONJ am_ADP sicrhau_VERB eich_DET lle_NOUN -_PUNCT byddwch_VERB yno_ADV 'n_PART gynnar_ADJ !_PUNCT
Gyda_ADP chyflwyno_NOUN cyffrous_ADJ gan_ADP y_DET digrifwr_NOUN lleol_ADJ ,_PUNCT enwg2_SYM cyfenw2_SYM __NOUN ,_PUNCT trefn_NOUN y_DET noson_NOUN fydd_VERB :_PUNCT
8.30_VERB pm_NOUN -_PUNCT enwg2_SYM cyfenw2_SYM __NOUN
9_NUM pm_NOUN -_PUNCT enwb1_NOUN cyfenw1_ADJ __NOUN
Disgwylir_VERB y_DET bydd_AUX y_DET noson_NOUN yn_PART tynnu_VERB at_ADP derfyn_NOUN oddeutu_ADV chwarter_NOUN wedi_PART deg_NUM ._PUNCT
Fel_CONJ y_DET gwyddoch_VERB ,_PUNCT mae_VERB 'r_DET nosweithiau_NOUN hyn_PRON i_ADP gyd_NOUN am_ADP ddim_PRON ac_CONJ yn_PART cael_VERB eu_DET trefnu_VERB 'n_PART hollol_ADV wirfoddol_ADJ ._PUNCT
Gwerthfawrogwn_VERB eich_DET cefnogaeth_NOUN ._PUNCT
Y_DET nos_NOUN Wener_NOUN hon_DET bydd_VERB bwced_NOUN i_ADP godi_VERB arian_NOUN at_ADP achos_NOUN Cymraeg_PROPN lleol_ADJ -_PUNCT dewch_VERB a_CONJ 'ch_PRON ceiniogau_NOUN sbâr_ADJ !_PUNCT
Dylai_VERB fod_VERB yn_PART noson_NOUN a_CONJ hanner_NOUN !_PUNCT
Wela_VERB i_ADP chi_PRON yno_ADV !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Eisiau_VERB Gwirfoddoli_PROPN ?_PUNCT
Rydym_VERB dy_DET angen_NOUN di_PRON !_PUNCT
Ie_INTJ TI_PROPN !_PUNCT
Gyda_ADP enw_NOUN yn_PART dathlu_VERB pen_NOUN -_PUNCT blwydd_NOUN yn_PART 30_NUM mlwydd_NOUN oed_NOUN eleni_ADV !_PUNCT
Rydym_AUX yn_PART chwilio_VERB am_ADP wirfoddolwyr_NOUN newydd_ADJ i_ADP fod_VERB yn_PART rhan_NOUN o_ADP 'n_PART tîm_NOUN bach_ADJ ond_CONJ prysur_ADJ !_PUNCT
Mae_VERB gennym_ADP ystod_NOUN o_ADP gyfleoedd_NOUN i_PART wirfoddoli_VERB trwy_ADP gyfrwng_NOUN y_DET Gymraeg_PROPN yn_PART enw_NOUN boed_NOUN yn_PART siop_NOUN siarad_VERB ar_ADP fore_NOUN dydd_NOUN Sadwrn_PROPN ,_PUNCT enw_NOUN __NOUN ar_ADP nos_NOUN Wener_NOUN ,_PUNCT __NOUN __NOUN neu_CONJ Radio_NOUN Dysgwyr_PROPN mae_VERB cyfleoedd_VERB di_PRON -_PUNCT ri_NOUN ar_ADP gael_VERB ._PUNCT
Gyda_ADP mwy_PRON o_ADP ganolfannau_NOUN cymdeithasol_ADJ yn_PART cau_VERB yng_ADP Nghymru_PROPN mae_AUX angen_NOUN sicrhau_VERB y_PART bydd_VERB enw_NOUN yma_ADV i_PART aros_VERB !_PUNCT
Dyma_DET 'r_DET LLE_NOUN I_ADP FOD_X !_PUNCT
Cysylltwch_VERB â_ADP enwb3_VERB cyfenw3_ADJ __NOUN ar_ADP
cyfeiriad_NOUN -_PUNCT bost_NOUN
i_PART ddangos_VERB diddordeb_NOUN !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
Dyddiadau_NOUN i_ADP 'w_PRON rhoi_VERB yn_ADP y_DET dyddiadur_NOUN !_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
ADRAN_VERB Y_DET SIOP_NOUN
Helo_INTJ ,_PUNCT Gwenwch_PROPN mai_PART 'n_PART ddydd_NOUN Gwener_PROPN !_PUNCT
HWRE_VERB !_PUNCT
Beth_PRON sydd_VERB gennai_VERB i_ADP gynnig_NOUN i_ADP chi_PRON heddiw_ADV ?_PUNCT
Beth_PRON am_ADP y_DET Daliwr_NOUN Gwydr_PROPN a_CONJ Gwin_NOUN hyn_PRON ?_PUNCT
Anrheg_NOUN berffaith_VERB os_CONJ rydych_VERB am_ADP ymweld_VERB â_ADP ffrind_NOUN neu_CONJ deulu_NOUN dros_ADP yr_DET haf_NOUN ?_PUNCT
<_SYM img_VERB /_PUNCT >_SYM
<_SYM img_VERB /_PUNCT >_SYM
