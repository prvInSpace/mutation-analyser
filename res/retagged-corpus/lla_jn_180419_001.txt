â_ADP 0.0_NOUN
Manics_PROPN a_CONJ Hold_PROPN Me_X Like_X A_CONJ Heaven_PROPN ar_ADP Radio_NOUN Cymru_PROPN oddi_ADP ar_ADP ei_DET albwm_NOUN newydd_ADJ Resistance_PROPN Is_PROPN Futile_PROPN ._PUNCT
Ges_VERB i_ADP 'r_DET plesur_NOUN o_ADP gyfweld_VERB â_ADP enwg_VERB cyfenw_NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT Manics_PROPN ym_ADP mewn_ADP siop_NOUN record_NOUN Rough_PROPN Trade_PROPN yn_ADP Llundain_PROPN wythnos_NOUN diwetha_ADJ nos_NOUN Sadwrn_PROPN a_CONJ cael_ADV holi_VERB nhw_PRON [_PUNCT =_SYM ]_PUNCT am_ADP [_PUNCT /=_PROPN ]_PUNCT am_ADP yr_DET albwm_NOUN yma_ADV ac_CONJ mae_VERB gan_ADP bron_ADV i_ADP bob_DET cân_NOUN stori_NOUN anhygoel_ADJ ar_ADP y_DET record_NOUN yma_ADV Stories_NOUN diddorol_ADJ am_ADP  _SPACE am_ADP gamgymeriade_VERB ._PUNCT
Am_ADP bobl_NOUN maen_AUX nhw_PRON wedi_PART cael_VERB eu_PRON dylanwadu_VERB gan_ADP ._PUNCT
Artistiaid_NOUN a_CONJ enwg_VERB cyfenw_NOUN a_CONJ ffotograffwyr_NOUN diddorol_ADJ iawn_ADV felly_CONJ mae_VERB pob_DET cân_NOUN ar_ADP yr_DET albwm_NOUN dw_AUX i_PRON 'n_PART meddwl_VERB gyda_ADP stori_NOUN  _SPACE tu_NOUN nôl_NOUN iddo_ADP fe_PRON ac_CONJ  _SPACE ie_INTJ mae_AUX 'n_PART edrych_VERB fel_ADP mae_AUX 'n_PART mynd_VERB i_ADP fod_VERB yn_PART rhif_NOUN un_NUM yn_PART siartiau_NOUN '_PUNCT fory_ADV ._PUNCT
Manics_VERB oedden_ADP nhw_PRON sy_AUX 'n_PART fod_VERB i_PART chwarae_VERB yng_ADP Nghaerdydd_PROPN a_CONJ Llandudno_PROPN mis_NOUN nesa'_ADJUNCT ac_CONJ enwg_VERB cyfenw_NOUN ydy_VERB hwn_PRON oddi_ADP ar_ADP ei_DET albwm_NOUN e_PRON sy_VERB 'n_PART mas_NOUN yn_PART fuan_ADJ ar_ADP label_NOUN Sbrigyn_NOUN Ymborth_PROPN ._PUNCT
Dyma_ADV Pan_CONJ Fydda_PROPN Ni_PRON 'n_PART Symud_PROPN ._PUNCT
930.149_ADJ
Mae_VERB emynau_NOUN yn_PART gyfarwydd_ADJ i_ADP ni_PRON i_ADP gyd_ADP ._PUNCT
Ond_CONJ tybed_ADV sut_ADV fydde_VERB 'r_DET emyn_NOUN yn_PART swnio_VERB fel_ADP trac_NOUN cyfoes_ADV ?_PUNCT
enwb_VERB cyfenw_NOUN ._PUNCT
Bob_DET wythnos_NOUN dyna_DET 'r_DET her_NOUN dw_AUX i_PRON 'n_PART ei_DET osod_VERB i_ADP gerddorion_NOUN ._PUNCT
Trawsnewid_VERB emyn_NOUN i_ADP fod_VERB yn_PART gân_NOUN fodern_ADJ ._PUNCT
Emyn_VERB roc_NOUN a_CONJ rôl_NOUN ._PUNCT
Yn_ADP y_DET rhaglen_NOUN nesa'_ADJUNCT Mr_NOUN cyfenw_NOUN sy_AUX 'n_PART derbyn_VERB yr_DET her_NOUN ._PUNCT
A_CONJ 'r_DET ermyn_NOUN sy_AUX 'n_PART cael_VERB ei_PRON drawsnewid_VERB ydy_VERB Builth_PROPN ._PUNCT
Mae_VERB 'n_PART emyn_NOUN mawr_ADJ t'mod_ADV mae_AUX 'na_ADV sŵn_NOUN mawr_ADJ iddo_ADP ._PUNCT
Dw_AUX i_PRON 'n_PART mynd_VERB i_ADP ailcreu_VERB cân_NOUN hollol_ADV newydd_ADJ sbon_ADV allan_ADV ohono_ADP fo_PRON ._PUNCT
Emyn_VERB roc_NOUN a_CONJ rôl_NOUN ._PUNCT
Dydd_NOUN Gwener_PROPN am_ADP hanner_NOUN awr_NOUN wedi_PART deuddeg_VERB ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN ._PUNCT
1945.481_PROPN
Band_PROPN sy_AUX 'n_PART wneud_VERB pethau_NOUN  _SPACE yn_PART hollol_ADV unigryw_ADJ ag_ADP  _SPACE ar_ADP liwt_NOUN eu_DET hunain_DET ._PUNCT
Goat_ADJ ydy_VERB rheina_NOUN a_CONJ Let_X It_X Burn_PROPN yw_VERB 'r_DET sengl_NOUN newydd_ADJ ._PUNCT
Maen_AUX nhw_PRON 'n_PART dod_VERB o_ADP Sweden_PROPN ._PUNCT
Maen_AUX nhw_PRON 'n_PART gwisgo_VERB [_PUNCT saib_NOUN ]_PUNCT masks_ADP a_CONJ chlogynnau_NOUN ar_ADP y_DET llwyfan_NOUN ._PUNCT
Mae_VERB 'na_ADV wastad_ADV lot_PRON o_ADP dân_NOUN a_CONJ pethau_NOUN felly_ADV  _SPACE ar_ADP y_DET llwyfan_NOUN pan_CONJ mae_AUX Goat_PROPN yn_PART perfformio_VERB ac_CONJ  _SPACE ie_INTJ mae_VERB honno_PRON yn_PART newydd_ADJ ganddyn_ADP nhw_PRON ._PUNCT
yyy_ADV reit_NOUN nesa'_ADJUNCT ar_ADP y_DET rhaglen_NOUN ._PUNCT
Dyma_ADV chi_PRON Mr_NOUN cyfenw_NOUN gyda_ADP enwb_NOUN __NOUN yn_PART canu_VERB '_PUNCT fyd_NOUN ._PUNCT
Nawr_ADV mae_AUX Mr_NOUN cyfenw_NOUN yn_PART gigio_VERB nos_NOUN '_PUNCT fory_ADV yn_ADP y_DET TramShed_X yng_ADP Nghaerdydd_PROPN yn_PART cefnogi_VERB Mungos_PROPN Hifi_PROPN ._PUNCT
ymm_PART fydd_VERB yn_PART noson_NOUN wych_ADJ i_ADP fod_VERB yn_PART onest_ADJ ._PUNCT
Mae_VERB Mungos_PROPN Hifi_PROPN yn_PART brill_NOUN ._PUNCT
Maen_AUX nhw_PRON 'n_PART wneud_VERB math_NOUN o_ADP dub_NOUN reggae_NOUN o_ADP 'r_DET Alban_PROPN ac_CONJ  _SPACE ie_INTJ [_PUNCT =_SYM ]_PUNCT dyma_DET [_PUNCT /=_PROPN ]_PUNCT dyma_DET fe_PRON gyda_ADP enwb_NOUN [_PUNCT =_SYM ]_PUNCT Mae_VERB [_PUNCT /=_PROPN ]_PUNCT mae_VERB hon_PRON yn_PART diwn_NOUN '_PUNCT fyd_NOUN mor_ADV dda_ADJ ._PUNCT
2850.056_ADJ
Pan_CONJ dydy_VERB enwg_VERB cyfenw_NOUN ddim_PART yn_PART chwarae_VERB 'r_DET iwcalilis_NOUN neu_CONJ yn_PART dw_AUX i_ADP ddim_PART yn_PART gwybod_ADV mynd_VERB out_ADP gyda_ADP __NOUN __NOUN __NOUN __NOUN ar_ADP y_DET llwyfan_NOUN neu_CONJ  _SPACE beth_PRON arall_ADJ gwedwch_NOUN ._PUNCT
O_ADP mae_AUX fe_PRON wedi_PART wneud_VERB lot_PRON ._PUNCT
Bob_DET math_NOUN o_ADP bethau_NOUN on'd_ADJ ydy_VERB ?_PUNCT
Cynhyrchu_VERB bandiau_NOUN rhedeg_VERB label_NOUN ._PUNCT
ym_ADP mae_AUX yn_PART swnio_VERB felly_ADV Tra_X Fyddaf_VERB Fyw_PROPN  _SPACE albwm_NOUN enwg_VERB cyfenw_NOUN yn_PART dod_VERB mas_NOUN ar_ADP Fehefin_NOUN nawfed_ADJ ar_ADP hugain_NOUN a_PART wedi_PART 'i_PRON recordio_VERB ar_ADP ben_NOUN ei_DET hun_NOUN yng_ADP Nghaerdydd_PROPN dros_ADP dwy_NUM flwyddyn_NOUN ._PUNCT
Gyda_ADP enwb_NOUN cyfenw_NOUN a_CONJ __NOUN __NOUN __NOUN __NOUN yn_PART ymuno_VERB ag_ADP e_PRON ._PUNCT
A_PART mae_AUX enwg_VERB yn_PART mynd_VERB i_PART fod_VERB yn_PART lawnsio_VERB 'r_DET albwm_NOUN yn_PART galeri_VERB Caernarfon_PROPN ar_ADP y_DET chweched_VERB o_ADP Orffennaf_PROPN hefyd_ADV ._PUNCT
ymm_INTJ [_PUNCT saib_NOUN ]_PUNCT dyna_DET pryd_NOUN mae_AUX 'n_PART mynd_VERB i_ADP fod_VERB mas_NOUN enwg_VERB yn_PART mynd_VERB i_PART fod_AUX yn_PART cefnogi_VERB enwg_VERB fan_NOUN 'na_ADV ac_CONJ  _SPACE ie_INTJ mwy_PRON o_ADP gigs_NOUN hefyd_ADV  _SPACE i_ADP 'w_PRON cyhoeddi_VERB yn_PART fuan_ADJ ._PUNCT
Nawr_ADV nes_CONJ ymlaen_ADV yn_ADP y_DET rhaglen_NOUN fyddwn_AUX ni_PRON 'n_PART siarad_VERB gyda_ADP enwg_VERB cyfenw_NOUN ._PUNCT
Fydd_AUX yn_PART talu_VERB tynged_NOUN i_ADP Chef_PRON enwg_VERB cyfenw_NOUN o'dd_NOUN yn_ADP y_DET band_NOUN Tystion_PROPN gyda_ADP fe_PRON ._PUNCT
Ond_CONJ hiphop_NOUN newydd_ADJ i_ADP chi_PRON nesa'_ADJUNCT a_CONJ mae_VERB hwn_PRON yn_PART remix_NOUN o_ADP 'r_DET Tri_NOUN Hŵr_PROPN Doeth_NOUN gan_ADP y_DET [_NOUN aneglur_ADJ ]_PUNCT Melyn_PROPN ._PUNCT
Felly_CONJ enwbg_NOUN cyfenw_NOUN yn_PART remicsio_VERB Tri_X Hŵr_PROPN Doeth_NOUN gyda_ADP Smocio_PROPN Hustlo_PROPN a_CONJ Dwyn_PROPN o_ADP Tesco_PROPN ._PUNCT
3903.722_NOUN
enwbg_NOUN cyfenw_NOUN ar_ADP Radio_NOUN Cymru_PROPN heno_ADV __NOUN __NOUN __NOUN __NOUN sy_VERB 'ma_ADV a_CONJ mae_VERB __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN yn_PART cynhyrchu_VERB 'r_DET rhaglen_NOUN yma_ADV ._PUNCT
Hei_INTJ enwg_VERB ._PUNCT
Hei_INTJ ._PUNCT
Ti_PRON 'n_PART iawn_ADJ enwg_VERB [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Helo_INTJ ._PUNCT
Bydd_VERB yn_PART onest_ADJ gyda_ADP 'r_DET grandwyr_NOUN nawr_ADV enwg_VERB ._PUNCT
Ie_INTJ ._PUNCT
Pa_PART raglen_NOUN sy_VERB 'n_PART well_ADJ gen_ADP ti_PRON gynhyrchu_VERB ?_PUNCT
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Rhaglen_VERB yma_ADV nos_NOUN Iau_ADJ [_PUNCT saib_NOUN ]_PUNCT [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ie_INTJ ._PUNCT
Neu_PART enwg_VERB cyfenw_NOUN ar_ADP Ddydd_NOUN Gwener_PROPN ?_PUNCT
Mae_VERB hwn_PRON yn_PART haws_ADJ i_PRON [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Na_INTJ ._PUNCT
Dw_AUX i_PRON 'n_PART gwybod_VERB mai_PART enwg_VERB ydy_VERB dy_DET ffefryn_NOUN di_PRON enwg_VERB ._PUNCT
Ac_CONJ [_PUNCT =_SYM ]_PUNCT mae_VERB [_PUNCT /=_PROPN ]_PUNCT mae_AUX enwg_VERB yn_PART prynu_VERB pastries_VERB i_ADP ti_PRON on'd_VERB ydy_VERB ?_PUNCT
Yndy_INTJ ._PUNCT
Ond_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
A_PART mae_VERB 'n_PART ddoniol_ADJ ac_CONJ yn_PART charming_NOUN ._PUNCT
Yndy_INTJ ._PUNCT
Fasech_VERB chdi_NOUN wneud_VERB yr_DET un_NUM peth_NOUN os_CONJ '_PUNCT dych_VERB chdi_NOUN yn_ADP y_DET gogledd_NOUN ._PUNCT
Mmm_VERB ._PUNCT
'_PUNCT Falle_NOUN ddim_PART ._PUNCT
Na_INTJ ?_PUNCT
Wel_INTJ ._PUNCT
Wi_AUX 'n_PART licio_VERB meddwl_VERB fasech_NOUN chdi_ADJ ._PUNCT
Ie_INTJ '_PUNCT falle_NOUN ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Mae_AUX enwg_VERB cyfenw_NOUN yn_PART gallu_ADV dod_VERB â_ADP straeon_NOUN diddorol_ADJ am_ADP ei_DET travels_NOUN a_CONJ wneud_VERB comedi_NOUN rownd_ADV Prydain_PROPN i_ADP ti_PRON hefyd_ADV on'd_X ydy_VERB ?_PUNCT
Yndy_INTJ ._PUNCT
Ond_CONJ ti_PRON efo_ADP storis_NOUN difr_NOUN am_ADP +_SYM
Mmm_VERB ._PUNCT
+_VERB mynd_VERB i_ADP gigs_NOUN a_CONJ gŵyl_NOUN +_SYM
Weithiau_ADV ._PUNCT
+_VERB [_PUNCT =_SYM ]_PUNCT dros_ADP [_PUNCT /=_PROPN ]_PUNCT dros_ADP Prydain_PROPN +_PROPN
Ie_INTJ ._PUNCT
+_VERB ac_CONJ y_DET byd_NOUN ._PUNCT
Weithiau_ADV ._PUNCT
Ie_INTJ weithiau_ADV ._PUNCT
Ie_INTJ ._PUNCT
enwg_VERB nôl_ADV '_PUNCT fory_ADV a_CONJ Dydd_NOUN Sadwrn_PROPN ._PUNCT
Yndy_INTJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT Ti_PRON 'n_PART dewis_VERB lot_ADV [_PUNCT /=_PROPN ]_PUNCT ti_PRON 'n_PART dewis_VERB [_PUNCT aneglur_ADJ ]_PUNCT rhaglenni_NOUN ?_PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB ._PUNCT
Do_INTJ ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB wnaethon_VERB nhw_PRON blesio_VERB ._PUNCT
Gobeithio_VERB ._PUNCT
Da_ADJ ._PUNCT
Da_ADJ da_ADJ da_ADJ [_PUNCT anadlu_VERB ]_PUNCT ._PUNCT
Beth_PRON ydy_VERB 'r_DET hen_ADJ beth_NOUN weiarles_VERB heno_ADV ?_PUNCT
Wel_PART esboniwn_VERB ni_PRON gynta'_ADJUNCT bebe_NOUN ydy_VERB 'r_DET hen_ADJ beth_NOUN weiarles_NOUN ?_PUNCT
Wel_CONJ fi_PRON sy_VERB 'di_PART bod_AUX yn_PART pori_VERB trwy_ADP 'r_DET archifoedd_NOUN [_PUNCT saib_NOUN ]_PUNCT [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
M_NUM -_PUNCT hm_NOUN ._PUNCT
Yma_ADV yn_ADP y_DET BBC_PRON ._PUNCT
Ie_INTJ ._PUNCT
Os_CONJ ydych_VERB chi_PRON eisiau_VERB clywed_VERB mwy_PRON ar_ADP stwff_NOUN archif_NOUN cofiwch_VERB mae_VERB Cofio_PROPN ymlaen_ADV ar_ADP Ddydd_NOUN Sul_PROPN a_CONJ mae_VERB 'na_ADV rhaglen_NOUN newydd_ADJ hefyd_ADV o_ADP 'r_DET enw_NOUN Co_NOUN '_PUNCT Bach_X ar_ADP nos_NOUN Lun_PROPN ._PUNCT
O_ADP reit_NOUN ._PUNCT
Efo_PART enwg_VERB cyfenw_NOUN yn_PART cyflwyno_VERB +_SYM
ââ_INTJ ._PUNCT
+_VERB pytiau_NOUN o_ADP 'r_DET archif_NOUN a_CONJ enwg_VERB cyfenw_NOUN yn_PART wneud_VERB darluniau_NOUN sain_NOUN i_ADP gyd-_ADJ fynd_VERB [_PUNCT =_SYM ]_PUNCT â_ADP 'r_DET [_NOUN /=_PROPN ]_PUNCT â_ADP 'r_DET deunydd_NOUN yna_DET ._PUNCT
Wow_PROPN ._PUNCT
Mae_AUX hwnna_PRON 'n_PART swnio_VERB 'n_PART ddiddorol_ADJ ._PUNCT
Sydd_VERB yn_PART plyg_ADV proffesiynol_ADJ yno_ADV heddiw_ADV ._PUNCT
Oedd_VERB ._PUNCT
So_NOUN nos_NOUN Lun_PROPN mae_VERB enwg_VERB cyfenw_NOUN +_SYM
Ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
+_VERB ymlaen_ADV ife_ADV ?_PUNCT
Reit_PROPN OK_X ._PUNCT
Rhaglen_NOUN diddorol_ADJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Cyn_CONJ <_SYM aneglur_ADJ 2_NUM >_SYM ._PUNCT
So_INTJ yn_ADP yr_DET archif_NOUN yma_DET mae_VERB 'na_ADV lot_PRON o_ADP ganeuon_NOUN sydd_VERB ddim_PART fel_ADP arfer_NOUN yn_PART cael_VERB eu_PRON chwarae_VERB ar_ADP y_DET radio_NOUN ._PUNCT
Wel_INTJ mae_VERB 'na_ADV lot_PRON o_ADP rhaglenni_NOUN radio_NOUN +_SYM
Ie_INTJ ._PUNCT
+_VERB sydd_VERB yn_PART amlwg_ADJ ddim_PART 'di_PART cael_VERB eu_DET darlledu_VERB ers_ADP y_DET chwechdegau_NOUN ._PUNCT
Reit_INTJ ._PUNCT
Ac_CONJ hefyd_ADV oherwydd_CONJ mae_VERB 'n_PART archif_NOUN mor_ADV fawr_ADJ dydy_VERB bob_DET deunydd_NOUN ddim_PART wedi_PART cael_VERB eu_DET labeli_NOUN yn_PART gywir_ADJ oherwydd_CONJ mae_AUX pobl_NOUN 'di_PART cael_VERB heidio_VERB tapiau_NOUN [_PUNCT =_SYM ]_PUNCT heb_ADP [_PUNCT /=_PROPN ]_PUNCT [_PUNCT saib_NOUN ]_PUNCT heb_ADP enwau_NOUN iddo_ADP fe_PRON neu_CONJ heb_ADP gwaith_NOUN papur_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Does_VERB neb_PRON quite_NOUN yn_PART siŵr_ADJ bebe_NOUN '_PUNCT dy_DET bebe_NOUN wedyn_ADV dw_VERB i_PRON 'n_PART dueddol_ADJ o_ADP roi_VERB rhyw_DET keywords_NOUN i_ADP mewn_ADP +_SYM
Ie_INTJ ._PUNCT
+_VERB a_CONJ wedyn_ADV ti_PRON 'n_PART cael_VERB caneuon_VERB weitihie_VERB  _SPACE enghraifft_NOUN wnes_VERB i_PRON roi_VERB pop_NOUN Cymraeg_PROPN i_ADP mewn_ADP ._PUNCT
Reit_INTJ ._PUNCT
A_PART mae_VERB 'r_DET gân_NOUN yma_ADV [_PUNCT =_SYM ]_PUNCT 'di_PART [_PUNCT /=_PROPN ]_PUNCT 'di_PART troi_VERB fyny_ADV ar_ADP rhestr_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB o_ADP bosib_NOUN bod_AUX mae_AUX 'n_PART dod_VERB o_ADP gyfres_NOUN o_ADP 'r_DET enw_NOUN Tipyn_PROPN o_ADP Fynd_VERB ._PUNCT
Okay_INTJ ._PUNCT
O_ADP bosib_NOUN ._PUNCT
Allai_VERB [_PUNCT aneglur_ADJ ]_PUNCT yn_PART anghywir_ADJ ._PUNCT
Tipyn_NOUN o_ADP Fynd_VERB +_SYM
Ie_INTJ ._PUNCT
+_VERB oddi_ADP ar_ADP Radio_NOUN Cymru_PROPN fan_NOUN hyn_DET ie_INTJ ?_PUNCT
Ie_INTJ ._PUNCT
Yn_PART chwech_NUM deg_NUM pedwar_NUM ._PUNCT
Wow_NOUN Okay_X ._PUNCT
ac_CONJ o'dd_NOUN bobl_NOUN fel_ADP enwb_NOUN cyfenw_NOUN ._PUNCT
Ie_INTJ ._PUNCT
Watshia_VERB Di_PRON Dy_DET Hun_PROPN ._PUNCT
Watshia_VERB fy_DET hun_NOUN yn_PART camenw_NOUN fo_PRON ._PUNCT
Ac_CONJ o'dd_NOUN enwg_VERB cyfenw_NOUN yn_PART amlwg_ADJ +_SYM
[_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
+_VERB yn_PART [_PUNCT aneglur_ADJ ]_PUNCT o_ADP ._PUNCT
Wow_PROPN ._PUNCT
Wedyn_ADV [_PUNCT saib_NOUN ]_PUNCT yma_ADV yn_ADP y_DET BBC_NOUN '_PUNCT dyn_NOUN ni_PRON 'n_PART helpu_VERB ein_DET gilydd_NOUN ._PUNCT
O_ADP 'n_PART ni_PRON 'n_PART gwrando_VERB i_ADP 'r_DET gân_NOUN 'ma_ADV a_CONJ ni_PRON 'n_PART nabod_VERB y_DET [_PUNCT aneglur_ADJ ]_PUNCT gan_ADP ni_PRON ddim_PART syniad_NOUN bebe_NOUN o'dd_NOUN o_PRON ._PUNCT
Ie_INTJ ._PUNCT
Ac_CONJ o_ADP 'n_PART i_PRON "_PUNCT O_ADP dw_VERB i_PRON 'n_PART siŵr_ADJ dw_AUX i_PRON 'n_PART nabod_VERB "_PUNCT a_CONJ hefyd_ADV mae_AUX enwg_VERB yn_PART canu_VERB mewn_ADP rhyw_DET dôn_NOUN weddol_ADV isel_ADJ ._PUNCT
Felly_ADV wnes_VERB i_PRON ffonio_VERB enwg_VERB cyfenw_NOUN ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART gwybod_VERB baset_VERB ti_PRON 'n_PART ffonio_VERB enwg_VERB cyfenw_NOUN ._PUNCT
Pwy_PRON arall_ADJ ?_PUNCT
4563.788_NOUN
Dim_PRON ond_ADP diolch_NOUN yn_PART fawr_ADJ alla_VERB i_PART ddeud_VERB ._PUNCT
Oedd_VERB hynna_PRON yn_PART hollol_ADV gorgeous_ADJ y_DET trac_NOUN yna_ADV ._PUNCT
O'dd_NOUN hi_PRON yn_PART ._PUNCT
A_PART t'mod_VERB mae_VERB ansawdd_NOUN y_DET cyfnod_NOUN '_PUNCT te_NOUN o_ADP ran_NOUN [_PUNCT saib_NOUN ]_PUNCT [_PUNCT =_SYM ]_PUNCT ddim_PART bod_VERB [_PUNCT /=_PROPN ]_PUNCT [_PUNCT giglan_VERB ]_PUNCT ddim_PART bod_VERB eu_DET ansawdd_NOUN nhw_PRON ddim_PART yn_PART +_SYM
Ie_INTJ ._PUNCT
+_VERB ond_CONJ mae_VERB bobl_NOUN fel_ADP 'na_ADV '_PUNCT dyn_NOUN nhw_PRON '_PUNCT mond_ADV dod_VERB rownd_ADV unwaith_ADV mewn_ADP cenhedlaeth_NOUN so_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ti_PRON 'n_PART meddwl_VERB bod_AUX ni_PRON 'di_PART wneud_VERB digon_PRON o_ADP ddathlu_VERB enwg_VERB cyfenw_NOUN ?_PUNCT
Achos_CONJ oedd_VERB e_PRON 'n_PART gymaint_ADV [_PUNCT =_SYM ]_PUNCT o_ADP o_ADP o_ADP [_PUNCT /=_PROPN ]_PUNCT o_ADP lejen_NOUN yn_PART hollol_ADV unigryw_ADJ on'd_X oedd_VERB ?_PUNCT
Oedd_VERB ._PUNCT
Ond_CONJ o'dd_NOUN o_PRON 'n_PART rhan_NOUN o_ADP 'r_DET cyfnod_NOUN lle_NOUN [_PUNCT =_SYM ]_PUNCT o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT o'dd_NOUN adlonianwyr_NOUN +_SYM
Mmm_VERB ._PUNCT
+_VERB os_CONJ i_ADP alw_VERB nhw_PRON rheina_NOUN oedd_AUX yn_PART neud_VERB bob_DET dim_DET on'd_NOUN oedd_VERB ?_PUNCT
Oedd_VERB oedd_VERB ._PUNCT
Canu_VERB dawnsio_VERB actio_VERB comedi_NOUN ._PUNCT
Ac_CONJ wneud_VERB o_PRON 'n_PART dda_ADJ ._PUNCT
Mae_VERB 'na_ADV +_SYM
Ie_INTJ ._PUNCT
+_VERB glip_NOUN arno_ADP fo_PRON ar_ADP Youtube_PROPN ar_ADP archif_NOUN HTV_NUM ._PUNCT
Ie_INTJ ._PUNCT
Lle_ADV maen_AUX nhw_PRON 'n_PART wneud_VERB acen_NOUN gogleddol_ADJ ._PUNCT
O_ADP ie_INTJ ._PUNCT
A_PART mae_VERB 'n_PART anhygoel_ADJ to_NOUN +_SYM
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
+_VERB o_ADP ran_NOUN mae_VERB 'na_ADV un_NUM yna_DET fel_ADP gogleddol_ADJ a_CONJ wedyn_ADV mae_VERB 'na_ADV clip_NOUN arall_ADJ <_SYM aneglur_ADJ 2_NUM >_SYM chwibianu_VERB ._PUNCT
O_ADP ie_INTJ mae_VERB hwnna_PRON 'n_PART dda_ADJ ._PUNCT
Mae_AUX 'r_DET clip_NOUN yna_DET yn_PART ffantastig_ADJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ond_CONJ dydy_VERB e_PRON ddim_PART ._PUNCT
Yndy_INTJ ?_PUNCT
Na_INTJ ._PUNCT
Na_INTJ ._PUNCT
Mae_VERB e_PRON jyst_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Dalentog_ADJ iawn_ADV iawn_ADV iawn_ADV ._PUNCT
Ydy_VERB ._PUNCT
[_PUNCT =_SYM ]_PUNCT A_CONJ 'r_DET [_PUNCT /=_PROPN ]_PUNCT a_CONJ 'r_DET llais_NOUN yna_ADV yn_PART hyfryd_ADJ hefyd_ADV ._PUNCT
yyy_ADV Ryan_PROPN At_ADP The_X Rank_X ydy_VERB 'r_DET albwm_NOUN enwog_ADJ on'd_VERB e_PRON ?_PUNCT
Ie_INTJ ._PUNCT
A_CONJ ac_CONJ dyna_PRON sydd_VERB yn_PART rhyfedd_ADJ ydy_VERB efo_ADP 'n_PART un_NUM o_ADP rhain_DET fath_NOUN o_ADP enwg_VERB cyfenw_NOUN hefyd_ADV +_SYM
Ie_INTJ ._PUNCT
+_VERB ar_ADP ei_DET [_PUNCT anelgur_DET ]_PUNCT ._PUNCT
Ie_INTJ ._PUNCT
[_PUNCT =_SYM ]_PUNCT O'dd_NOUN o'ddd_NOUN [_PUNCT /=_PROPN ]_PUNCT o'dd_NOUN yn_PART llwyddo_VERB pontio_VERB 'r_DET ddau_NUM fyd_NOUN 'na_ADV [_PUNCT =_SYM ]_PUNCT Y_DET y_DET [_NOUN /=_PROPN ]_PUNCT yr_DET Cymry_PROPN Cymraeg_PROPN ac_CONJ y_DET +_SYM
Mmm_VERB ._PUNCT
Mmm_VERB mmm_PRON ._PUNCT
+_VERB Cymry_PROPN di-_NOUN Gymraeg_PROPN felly_ADV ._PUNCT
Ac_CONJ [_PUNCT saib_NOUN ]_PUNCT dim_PART hwnna_PRON 'n_PART digwydd_VERB yn_PART aml_ADJ yng_ADP Nghymru_PROPN ._PUNCT
Na_INTJ ._PUNCT
Ond_CONJ t'mod_VERB [_PUNCT =_SYM ]_PUNCT mae_VERB [_PUNCT /=_PROPN ]_PUNCT mae_VERB 'r_DET stwff_NOUN yma_DET ._PUNCT
Mae_VERB 'na_ADV fwy_PRON yna_DET yn_ADP y_DET rhaglen_NOUN yma_DET o_ADP enwg_VERB ._PUNCT
Ddim_PART lot_ADV [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Oes_VERB 'na_ADV ?_PUNCT
ym_ADP ._PUNCT
So_PART gobeithio_VERB alla_VERB i_PRON [_PUNCT aneglur_ADJ ]_PUNCT e_PRON -_PUNCT bost_NOUN i_ADP rywun_NOUN neu_CONJ siarad_VERB efo_ADP rhywun_NOUN i_PART weld_VERB oes_VERB gan_ADP nhw_PRON diddordeb_NOUN [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART wneud_VERB rhywbeth_NOUN efo_ADP nhw_PRON jyst_X +_SYM
Hyfryd_ADJ ._PUNCT
+_VERB mae_VERB 'n_PART lovely_NOUN ffeindio_VERB pethau_NOUN fel_ADP 'na_ADV ._PUNCT
Mor_ADV neis_ADJ ._PUNCT
Ti_PRON ;_PUNCT n_PART mynd_VERB i_PART chwarae_VERB hynna_PRON at_ADP  _SPACE enwg_VERB '_PUNCT fory_ADV ?_PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB wna_VERB i_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB wna_VERB i_PRON ._PUNCT
4858.405_NOUN
Mellt_PROPN a_CONJ Rebel_PROPN ar_ADP Radio_NOUN Cymru_PROPN Brill_PROPN o_ADP gân_NOUN ._PUNCT
Mae_VERB 'r_DET albwm_NOUN allan_ADV '_PUNCT fory_ADV o_ADP 'r_DET enw_NOUN Mae_VERB 'n_PART Hawdd_PROPN Pan_CONJ Ti_PRON 'n_PART Ifanc_PROPN ._PUNCT
Fi_PRON 'n_PART edrych_VERB ymlaen_ADV i_PART wrando_VERB ar_ADP yr_DET albwm_NOUN yna_DET yn_ADP ei_DET gyfanrwydd_NOUN ._PUNCT
A_PART mae_AUX 'n_PART amser_NOUN am_ADP y_DET Mix_NOUN Gwaith_NOUN Cartre_X ._PUNCT
Mae_VERB bach_ADJ yn_PART gynharach_ADJ heno_ADV achos_CONJ nes_CONJ ymlaen_ADV fyddwn_AUX ni_PRON 'n_PART siarad_VERB gyda_ADP enwg_VERB cyfenw_NOUN yn_PART talu_VERB tynged_NOUN i_ADP Chef_PROPN __NOUN __NOUN __NOUN __NOUN oedd_VERB yn_PART aelod_NOUN o_ADP 'r_DET Tystion_NOUN gyda_ADP fe_PRON ._PUNCT
Ond_CONJ mae_AUX Mix_NOUN Gwaith_NOUN Cartre_NOUN ymlaen_ADV nawr_ADV a_CONJ mae_AUX 'n_PART dod_VERB gan_ADP Mellt_PROPN ._PUNCT
Maen_AUX nhw_PRON wedi_PART rhoi_VERB [_PUNCT anadlu_VERB ]_PUNCT wel_AUX lot_PRON o_ADP ganeuon_NOUN ffantastig_ADJ yn_ADP y_DET mix_NOUN yma_ADV ._PUNCT
Chi_PRON 'n_PART mynd_VERB i_PART glywed_VERB bach_ADJ o_ADP 'r_DET Beach_PROPN Boys_PROPN chi_PRON 'n_PART mynd_VERB i_PART glywed_VERB Los_PROPN Blancos_PROPN ._PUNCT
Ysgol_NOUN Sul_PROPN ._PUNCT
FFUG_PROPN ._PUNCT
yyy_ADV Orioles_PROPN ._PUNCT
Gwenno_VERB ._PUNCT
Lot_PRON o_ADP bethau_NOUN ffantastig_ADJ ._PUNCT
Mae_AUX 'n_PART dechrau_VERB gyda_ADP Super_NOUN Furry_PROPN Animals_PROPN ._PUNCT
Mix_NOUN Gwaith_NOUN Cartref_NOUN Mellt_PROPN ar_ADP Radio_NOUN Cymru_PROPN ._PUNCT
Bant_ADV â_ADP ni_PRON ._PUNCT
5705.035_NOUN
Diolch_NOUN i_ADP chi_PRON Mellt_PROPN am_ADP yr_DET albwm_NOUN mas_ADJ '_PUNCT fory_ADV ar_ADP label_NOUN JigCal_PROPN o_ADP 'r_DET enw_NOUN Mae_VERB 'n_PART Hawdd_PROPN Pan_CONJ Ti_PRON 'n_PART Ifanc_PROPN ._PUNCT
Arbennig_ADJ iawn_ADV ._PUNCT
ymm_CONJ diolch_NOUN yn_PART fawr_ADJ iawn_ADV i_ADP chi_PRON am_ADP  _SPACE am_ADP fod_VERB ar_ADP y_DET rhaglen_NOUN heno_ADV enwg_VERB cyfenw_NOUN sy_VERB yma_ADV ar_ADP Radio_NOUN Cymru_PROPN Nos_NOUN Iau_ADV trowdd_NOUN tan_CONJ deg_NUM o_ADP 'r_DET gloch_NOUN a_CONJ fi_PRON 'n_PART falch_ADJ iawn_ADV i_PART gweud_VERB bod_VERB __NOUN __NOUN __NOUN __NOUN yn_PART ymuno_VERB gyda_ADP fi_PRON ar_ADP y_DET rhaglen_NOUN heno_ADV ._PUNCT
Ond_CONJ o_ADP dan_ADP ymgylchiadau_NOUN trist_ADJ achos_ADP '_PUNCT dyn_NOUN ni_PRON yn_PART talu_VERB tynged_NOUN i_PART enwg_VERB cyfenw_NOUN Chef_ADJ oedd_VERB yn_ADP y_DET Tystion_PROPN gyda_ADP __NOUN __NOUN __NOUN __NOUN a_CONJ 'r_DET aeoldau_NOUN eraill_ADJ wrth_ADP gwrs_NOUN ._PUNCT
Cyn_CONJ i_ADP ni_PRON ddechrau_VERB gyda_ADP trac_NOUN gan_ADP y_DET Tystion_PROPN oddi_ADP ar_ADP yr_DET albwm_NOUN Rhaid_VERB i_ADP Rywbeth_PROPN Ddigwyddo_PROPN un_NUM naw_NUM naw_NUM saith_NUM ._PUNCT
Dyma_DET Gwyddbwyll_NOUN ._PUNCT
8292.229_NOUN
Gwyddbwyll_VERB gan_ADP y_DET Tystion_NOUN o_ADP 'r_DET albwm_NOUN o_ADP un_NUM naw_NUM naw_NUM saith_NUM a_CONJ mae_VERB enwg_VERB cyfenw_NOUN ar_ADP ben_NOUN arall_ADJ y_DET lein_NOUN o_ADP Berlin_PROPN yn_ADP yr_DET Almaen_PROPN ._PUNCT
Hei_INTJ cyfenw_NOUN ._PUNCT
Helo_INTJ enwg_VERB ._PUNCT
Sut_ADV mae_VERB ?_PUNCT
Ti_PRON 'n_PART iawn_ADJ ?_PUNCT
Helo_INTJ iawn_INTJ diolch_INTJ ._PUNCT
Diolch_INTJ yn_PART fawr_ADJ hefyd_ADV am_PART ymuno_VERB gyda_ADP fi_PRON heno_ADV ._PUNCT
'_PUNCT Dyn_NOUN ni_PRON 'n_PART mynd_VERB i_ADP [_PUNCT anadlu_VERB ]_PUNCT  _SPACE siarad_VERB am_ADP y_DET talu_VERB tynged_NOUN i_PART enwg_VERB cyfenw_NOUN Chef_ADJ oedd_VERB yn_PART aelod_NOUN o_ADP Tystion_PROPN wrth_ADP gwrs_NOUN ._PUNCT
Ac_CONJ o_ADP 'n_PART i_PRON 'n_PART meddwl_VERB mae_AUX 'n_PART rhaid_VERB i_ADP ni_PRON gael_VERB cyfenw_NOUN 'ma_ADV achos_NOUN oedd_VERB Tystion_PROPN yn_PART  _SPACE [_PUNCT =_SYM ]_PUNCT yn_PART yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART griw_NOUN achos_NOUN iawn_ADV on'd_VERB o_ADP 'ch_PRON chi_PRON ?_PUNCT
Pryd_ADV wnest_VERB ti_PRON gwrdd_VERB â_ADP Chef_PROPN cyfenw_NOUN ?_PUNCT
wnes_VERB i_PRON gyfarfod_VERB â_ADP Chef_PROPN o_ADP 'n_PART ni_PRON 'n_PART mynd_VERB i_ADP 'r_DET un_NUM ysgol_NOUN ond_CONJ oedd_VERB e_PRON 'n_PART rhyw_DET dau_NUM flynedd_NOUN yn_PART ieungach_ADJ ond_CONJ tua_ADV naw_NUM degau_NOUN cynnar_ADJ ._PUNCT
O_ADP 'n_PART i_PRON 'di_PART bod_VERB i_ADP Woolworths_PROPN a_CONJ wnes_VERB i_PRON fachu_VERB caset_NOUN [_PUNCT saib_NOUN ]_PUNCT  _SPACE gan_ADP grwp_VERB House_PROPN Of_X Pain_PROPN ._PUNCT
Reit_INTJ ._PUNCT
cân_NOUN nhw_PRON yw_VERB Jump_X Around_X ._PUNCT
A_CONJ dim_PRON o'dd_NOUN gen_ADP i_ADP Walkman_PROPN neu_CONJ dictaffon_NOUN neu_CONJ rywbeth_NOUN t'mod_VERB ._PUNCT
Moeth_NOUN ._PUNCT
ymm_ADV o_PRON 'n_PART i_PRON 'n_PART  _SPACE bach_ADJ yn_PART bachgen_NOUN drwg_ADJ o_ADP 'n_PART i_PRON 'n_PART smocio_VERB yn_ADP yr_DET ysgol_NOUN a_CONJ pwy_PRON arall_ADJ oedd_AUX yn_PART smocio_VERB hefyd_ADV oedd_AUX  _SPACE enwg_VERB ._PUNCT
A_CONJ [_PUNCT =_SYM ]_PUNCT dyma_DET [_PUNCT /=_PROPN ]_PUNCT dyma_DET fi_PRON 'n_PART chwarae_VERB 'r_DET miwsig_NOUN iddo_ADP fe_PRON a_CONJ 'r_DET ddau_NUM yn_PART eitha_ADJ "_PUNCT Wow_PROPN ._PUNCT
Mae_VERB hwn_PRON jyst_ADV yn_ADP jyst_X amazing_NOUN "_PUNCT so_PART wnaethon_VERB ni_PRON kind_VERB of_CONJ bondio_VERB dros_ADP House_PROPN of_X Pain_PROPN a_CONJ wedyn_ADV '_PUNCT nny_NOUN  _SPACE ie_PRON o_ADP 'n_PART ni_PRON 'n_PART mynd_VERB allan_ADV [_PUNCT =_SYM ]_PUNCT i_PRON [_PUNCT /=_PROPN ]_PUNCT i_ADP clybiau_NOUN a_CONJ bars_NOUN a_CONJ pethau_NOUN yn_ADP y_DET dre_NOUN a_CONJ wedyn_ADV '_PUNCT nny_PRON o_ADP 'n_PART i_PRON 'n_PART aml_ADJ yn_PART cyfarfod_ADV [_PUNCT =_SYM ]_PUNCT â_ADP [_PUNCT /=_PROPN ]_PUNCT â_ADP Chef_PROPN  _SPACE so_VERB ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
Ac_CONJ ar_ADP y_DET pryd_NOUN  _SPACE [_PUNCT aneglur_ADJ ]_PUNCT yn_PART sôn_VERB am_ADP hip_NOUN hop_NOUN Cymraeg_PROPN ac_CONJ yn_ADP y_DET blaen_NOUN [_PUNCT =_SYM ]_PUNCT doedd_VERB [_PUNCT /=_PROPN ]_PUNCT doedd_VERB e_PRON ddim_PART wir_ADJ yn_PART bodoli_VERB oedd_VERB e_PRON ?_PUNCT
Rap_ADJ Cymraeg_PROPN ?_PUNCT
Ond_CONJ ti_PRON 'di_PART bod_VERB yn_PART aelod_NOUN cynnar_ADJ iawn_ADV o_ADP Gorky_NOUN 's_X Zygotic_X Mynci_PROPN on'd_X wyt_VERB ti_PRON cyfenw_NOUN ?_PUNCT
Cyn_CONJ hynny_PRON ?_PUNCT
do_INTJ [_PUNCT =_SYM ]_PUNCT O_ADP 'n_PART i_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT [_PUNCT giglan_VERB ]_PUNCT o_ADP 'n_PART i_PRON 'n_PART chwarae_VERB 'r_DET ffidl_NOUN gyda_ADP Gorky_NOUN 's_X am_ADP gyfnod_NOUN ._PUNCT
Oedd_AUX Chef_PROPN wedi_PART bod_VERB mewn_ADP band_NOUN  _SPACE [_PUNCT saib_NOUN ]_PUNCT [_PUNCT =_SYM ]_PUNCT cyn_ADP [_PUNCT /=_PROPN ]_PUNCT cyn_ADP y_DET Tystion_PROPN ?_PUNCT
Oedd_VERB e_PRON 'di_PART [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Oedd_VERB ._PUNCT
Oedd_VERB e_PRON mewn_ADP rhyw_DET indie_NOUN band_NOUN dw_AUX i_PRON 'di_PART anghofio_VERB arnyn_ADP nhw_PRON though_NOUN ond_CONJ oedd_AUX e_PRON 'n_PART ch'mod_AUX yn_PART chwarae_VERB 'r_DET gitar_NOUN a_CONJ pethau_NOUN +_SYM
Felly_CONJ ar_ADP ôl_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
+_VERB ond_CONJ wrth_ADP gwrs_NOUN o_ADP 'n_PART ni_PRON 'n_PART mynd_VERB i_ADP lot_PRON o_ADP gigs_NOUN Cymraeg_PROPN hefyd_ADV yn_PART ystod_NOUN y_DET cyfnod_NOUN 'ma_ADV yn_ADP y_DET naw_NUM degau_NOUN ._PUNCT
Oedd_VERB 'na_ADV ryw_DET llwythi_NOUN o_ADP gigs_NOUN ymlaen_ADV bob_DET penwythnos_NOUN  _SPACE so_PRON o_PRON 'n_PART ni_PRON 'n_PART mynd_VERB wedyn_ADV '_PUNCT nny_PRON ._PUNCT
Fel_CONJ i_ADP Aberystwyth_PROPN i_ADP Lambedr_PROPN i_ADP Abertawe_PROPN ai_CONJ Llandysul_X  _SPACE Aberaeron_PROPN bron_ADV pob_DET penwythnos_NOUN a_CONJ t'mod_VERB [_PUNCT saib_NOUN ]_PUNCT yn_PART jyst_ADV esgus_ADJ am_ADP piss_NOUN -_PUNCT up_NUM oedd_VERB e_PRON basically_ADV wrth_ADP gwrs_NOUN ._PUNCT
A_PART pobl_NOUN ddim_PART gymaint_ADV yn_PART mynd_VERB am_ADP y_DET miwsig_NOUN ond_CONJ jyst_ADV yn_PART mynd_VERB i_ADP t'mod_VERB i_ADP gael_VERB hwyl_NOUN ond_CONJ  _SPACE t'mod_VERB [_PUNCT =_SYM ]_PUNCT lot_PRON [_PUNCT /=_PROPN ]_PUNCT lot_PRON o_ADP bobl_NOUN  _SPACE o_ADP 'r_DET ysgol_NOUN yn_PART mynd_VERB a_CONJ [_PUNCT =_SYM ]_PUNCT o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT o'dd_NOUN Chef_PROPN yna_ADV t'mod_VERB yn_ADP y_DET gigs_NOUN hefyd_ADV [_PUNCT =_SYM ]_PUNCT gyda_ADP [_PUNCT /=_PROPN ]_PUNCT gydan_VERB ni_PRON ._PUNCT
Ie_INTJ ._PUNCT
A_CONJ pa_PART fath_NOUN o_ADP bandiau_NOUN fasech_ADV chi_PRON 'n_PART mynd_VERB i_PART weld_VERB cyfenw_NOUN ?_PUNCT
Cerrig_VERB Melys_PROPN lots_NOUN ._PUNCT
yyy_ADV jas_NOUN am_ADP un_NUM gyfnod_NOUN +_SYM
Ie_INTJ ._PUNCT
+_VERB ond_CONJ [_PUNCT =_SYM ]_PUNCT jyst_X [_PUNCT /=_PROPN ]_PUNCT jyst_ADV lot_PRON o_ADP fandiau_NOUN o_ADP 'r_DET cyfnod_NOUN  _SPACE ond_CONJ t'mod_VERB pethau_NOUN eitha'_ADJUNCT diddorol_ADJ yn_PART digwydd_VERB hefyd_ADV ._PUNCT
8677.183_VERB
Oedd_VERB 'na_ADV lot_PRON ohonoch_ADP chi_PRON yn_ADP y_DET Tystion_PROPN ._PUNCT
Lot_PRON o_ADP aelodau_NOUN 'n_PART mynd_VERB a_CONJ dod_VERB ar_ADP hyd_NOUN y_DET blynyddoedd_NOUN ._PUNCT
ymm_VERB shwd_NOUN berson_NOUN oedd_VERB Chef_PROPN i_ADP fod_VERB gyda_ADP yn_ADP y_DET stiwdio_NOUN ac_CONJ mewn_ADP gigs_NOUN cyfenw_NOUN ?_PUNCT
O't_VERB ti_PRON bob_DET tro_NOUN yn_PART cael_VERB laugh_NOUN gyda_ADP Chef_X ._PUNCT
O'dd_NOUN e_PRON 'n_PART wastad_ADV yn_PART chwerthin_VERB ._PUNCT
yyy_ADV boi_NOUN really_ADV ddoniol_ADJ ac_CONJ yn_PART tynnu_VERB coes_NOUN  _SPACE boi_NOUN o'dd_NOUN yn_PART boi_NOUN jyst_ADV really_CONJ cwl_NOUN t'mod_NOUN ._PUNCT
O'dd_NOUN y_DET cool_NOUN dude_ADJ  _SPACE [_PUNCT saib_NOUN ]_PUNCT  _SPACE yn_ADP yr_DET ysgol_NOUN a_CONJ  _SPACE o_ADP 'n_PART ni_PRON 'n_PART bob_DET tro_NOUN ie_INTJ t'mod_VERB gigs_ADJ o_ADP 'n_PART ni_PRON 'n_PART cael_VERB lot_PRON o_ADP hwyl_NOUN  _SPACE [_PUNCT saib_NOUN ]_PUNCT  _SPACE a_CONJ wedyn_ADV '_PUNCT nny_PUNCT ie_PRON yn_ADP y_DET stiwdio_NOUN [_PUNCT =_SYM ]_PUNCT o_ADP 'n_PART ni_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT o_ADP 'n_PART ni_PRON 'n_PART t'mod_VERB [_PUNCT =_SYM ]_PUNCT o_ADP 'n_PART ni_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT o_ADP 'n_PART ni_PRON 'n_PART gweithio_VERB 'n_PART dda_ADJ gyda_ADP 'n_PART gilydd_NOUN ._PUNCT
Oedd_VERB wastad_ADV yn_PART blesur_ADJ i_PART gael_VERB e_PRON o_ADP gwmpas_NOUN ._PUNCT
A_CONJ bob_DET tro_NOUN o_ADP 'n_PART i_PRON 'n_PART cwrdd_NOUN â_ADP Chef_PROPN yn_PART gigs_ADJ y_DET Tystion_PROPN a_CONJ ti_PRON 'n_PART gwybod_VERB faint_ADV o_ADP 'n_PART i_PRON 'n_PART caru_VERB 'r_DET Tystion_PROPN a_CONJ [_PUNCT =_SYM ]_PUNCT o_ADP 'n_PART i_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT o_ADP 'n_PART i_PRON 'n_PART +_SYM
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
+_VERB obsessed_NOUN ac_CONJ o_ADP 'n_PART i_PRON 'n_PART meddwl_VERB bod_VERB chi_PRON 'n_PART anhygoel_ADJ ac_CONJ o'dd_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Groupie_NOUN arall_ADJ ?_PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
O_ADP 'n_PART i_PRON yn_PART groupie_ADJ cyfenw_NOUN ._PUNCT
O_ADP 'n_PART i_PRON yn_PART +_SYM
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
+_SYM groupie_ADJ ._PUNCT
Fi_PRON 'n_PART prowd_VERB i_PART weud_VERB o_ADP 'n_PART i_PRON 'n_PART groupie_ADJ ._PUNCT
Dim_DET byd_NOUN dodgy_NOUN y_DET Tystion_PROPN ._PUNCT
O_ADP 'n_PART i_PRON 'n_PART fan_NOUN mawr_ADJ ohonoch_ADP chi_PRON +_SYM
Na_INTJ [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
+_VERB di-_NOUN dilyn_VERB chi_PRON o_ADP 'r_DET cnap_NOUN <_SYM aneglur_ADJ 2_NUM >_SYM  _SPACE i_ADP 'r_DET gigs_NOUN o_ADP 'ch_PRON chi_PRON 'n_PART wneud_VERB yn_PART Clwb_PROPN Ifor_NOUN Bach_ADJ o_ADP amgylch_NOUN Caerdydd_PROPN wrth_ADP gwrs_NOUN ._PUNCT
Ond_CONJ  _SPACE ie_INTJ [_PUNCT =_SYM ]_PUNCT o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT o'dd_NOUN Chef_X [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ac_CONJ yn_ADP Y_DET Weriniaeth_NOUN Tsiec_PROPN [_PUNCT giglan_VERB ]_PUNCT ._PUNCT
Ac_CONJ yn_ADP Weriniaeth_NOUN Tsiec_X hefyd_ADV ._PUNCT
Do_INTJ ._PUNCT
O'dd_NOUN y_DET +_SYM
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
+_VERB '_PUNCT dyn_NOUN ni_PRON 'n_PART dod_VERB mas_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT achos_ADP o'dd_NOUN Tystion_PROPN yn_PART wneud_VERB gigs_NOUN yna_ADV ._PUNCT
Mae_VERB 'r_DET sesiwn_NOUN  _SPACE [_PUNCT saib_NOUN ]_PUNCT Maida_X Vale_X wnaethoch_VERB chi_PRON gyda_ADP Sleifar_PROPN a_CONJ 'r_DET teulu_NOUN wnes_VERB i_ADP bennu_VERB lan_ADV yn_PART darlledu_VERB hwnna_PRON ar_ADP Radio_NOUN One_X achos_CONJ oedd_VERB +_SYM
Ie_INTJ ._PUNCT
+_VERB y_DET DJ_NOUN yna_DET called_NOUN enwg_VERB cyfenw_NOUN wedi_PART marw_VERB ac_CONJ o'dd_AUX e_PRON wedi_PART addo_VERB sesiwn_NOUN i_ADP chi_PRON os_CONJ wi_AUX 'n_PART cofio_VERB 'n_PART iawn_ADJ __NOUN ._PUNCT
Dyna_DET beth_PRON ddigwyddodd_VERB ie_INTJ ?_PUNCT
So_PRON o_ADP 'n_PART ni_PRON 'di_PART bod_VERB yn_PART nhŷ_NOUN Maida_X Vale_X o_ADP 'r_DET blaen_NOUN yn_PART gwneud_VERB sesiynau_NOUN stiwdio_VERB a_CONJ wnaethon_VERB ni_PRON wneud_VERB -_PUNCT dau_NUM tri_NUM ?_PUNCT
Na_INTJ dau_NUM [_PUNCT saib_NOUN ]_PUNCT sesiwn_ADV byw_VERB [_PUNCT =_SYM ]_PUNCT ar_ADP [_PUNCT /=_PROPN ]_PUNCT ar_ADP Radio_NOUN Un_NUM a_CONJ sioe_NOUN enwg_VERB cyfenw_NOUN a_CONJ [_PUNCT =_SYM ]_PUNCT o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT o'dd_NOUN Chef_ADJ efo_ADP ni_PRON [_PUNCT =_SYM ]_PUNCT am_ADP y_DET [_PUNCT /=_PROPN ]_PUNCT am_ADP y_DET sesiynau_NOUN yna_DET ._PUNCT
A_CONJ shwd_NOUN brofiad_NOUN oedd_VERB wneud_VERB y_DET sesiynau_NOUN yna_DET yn_ADP Maida_PROPN Vale_X i_PART enwg_VERB cyfenw_NOUN __NOUN ?_PUNCT
So_PART ie_INTJ wnaethon_VERB ni_PRON  _SPACE fynd_VERB am_ADP bryd_NOUN o_ADP fwyd_NOUN  _SPACE efo_ADP enwg_VERB cyfenw_NOUN ._PUNCT
Tystion_VERB enwg_VERB cyfenw_NOUN a_CONJ criw_NOUN cynhyrchu_VERB [_PUNCT saib_NOUN ]_PUNCT  _SPACE __NOUN ._PUNCT
Ond_CONJ  _SPACE ie_INTJ wnaethon_VERB ni_PRON [_PUNCT saib_NOUN ]_PUNCT wnaethon_VERB ni_PRON laugh_NOUN a_CONJ [_PUNCT =_SYM ]_PUNCT o'dd_NOUN [_PUNCT /=_PROPN ]_PUNCT o'dd_NOUN Chef_ADJ mor_ADV chuffed_ADJ o'dd_NOUN e_PRON 'n_PART fyw_VERB ar_ADP Radio_NOUN Un_NUM ._PUNCT
yyy_CONJ a_PART wnaethon_VERB ni_PRON wneud_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT wneud_VERB shout_NOUN outs_NOUN i_ADP iddo_ADP gyda_ADP pobl_NOUN o_ADP 'n_PART ni_PRON 'n_PART nabod_VERB so_VERB [_PUNCT =_SYM ]_PUNCT o'dd_NOUN e_PRON 'n_PART [_PUNCT /=_PROPN ]_PUNCT o'dd_NOUN e_PRON 'n_PART  _SPACE wrth_ADP ei_DET fodd_NOUN fi_PRON 'n_PART cofio_VERB ._PUNCT
'_PUNCT Dyn_NOUN ni_PRON 'n_PART mynd_VERB i_PART chwarae_VERB y_DET trac_NOUN Hip_NOUN Hop_PROPN Cymraeg_PROPN nawr_ADV o_ADP dwy_NUM fil_NUM ac_CONJ un_NUM deg_NUM pedwar_NUM a_CONJ ni_PRON 'n_PART siarad_VERB mwy_PRON ar_ADP ôl_NOUN hon_PRON ._PUNCT
9185.297_NOUN
Hip_AUX Hop_PROPN Cymraeg_PROPN yn_PART chwarae_VERB ar_ADP BBC_PROPN Radio_VERB Cymru_PROPN gyda_ADP Chef_X ar_ADP y_DET meicroffon_NOUN fynna'_NOUNUNCT fyd_NOUN ._PUNCT
ymm_AUX yn_PART edrych_VERB yn_PART ôl_NOUN Chef_PROPN oedd_VERB '_PUNCT da_ADJ fe_CONJ lot_PRON o_ADP ffrindiau_NOUN '_PUNCT da_ADJ fe_CONJ lot_PRON o_ADP deulu_NOUN wrth_ADP gwrs_NOUN o'dd_NOUN yn_PART ei_DET garu_VERB fe_PRON lot_NOUN fawr_ADJ a_CONJ wi_NOUN 'n_PART siŵr_ADJ o'dd_NOUN yr_DET angladd_NOUN yn_PART ddiweddar_ADJ yn_PART  _SPACE [_PUNCT clecian_VERB ]_PUNCT yn_PART brofiad_NOUN emosiynol_ADJ iawn_ADV [_PUNCT =_SYM ]_PUNCT i_PRON [_PUNCT /=_PROPN ]_PUNCT i_ADP ti_PRON a_CONJ pawb_PRON o'dd_NOUN yna_ADJ ._PUNCT
Oedd_VERB yn_PART achlysur_NOUN  _SPACE hapus_ADJ hefyd_ADV cyfenw_NOUN ?_PUNCT
oedd_VERB yn_PART wasanaeth_NOUN hyfryd_ADJ ._PUNCT
Oedd_AUX yn_PART  _SPACE fel_ADP fasai_VERB Chef_PROPN wedi_PART licio_VERB  _SPACE o'dd_NOUN yn_PART jyst_ADJ yn_PART anffurfiol_ADJ a_CONJ lot_PRON o_ADP storis_NOUN really_ADV ddoniol_ADJ  _SPACE amdano_ADP ._PUNCT
Amdano_ADP enwg_VERB ._PUNCT
Ie_INTJ ._PUNCT
A_PART  _SPACE oedd_AUX yn_PART  _SPACE ie_INTJ oedd_VERB yn_PART brofiad_NOUN emosiynol_ADJ hefyd_ADV achos_CONJ [_PUNCT saib_NOUN ]_PUNCT yr_DET amgylchiadau_NOUN ond_CONJ hefyd_ADV  _SPACE ie_INTJ [_PUNCT =_SYM ]_PUNCT jyst_X [_PUNCT /=_PROPN ]_PUNCT jyst_X gymaint_ADV ._PUNCT
Oedd_VERB yn_PART boi_NOUN t'mod_VERB really_ADV poblogaidd_ADJ t'mod_VERB ._PUNCT
ymm_INTJ [_PUNCT saib_NOUN ]_PUNCT t'mod_VERB ie_INTJ [_PUNCT =_SYM ]_PUNCT lot_PRON [_PUNCT /=_PROPN ]_PUNCT lot_ADV [_PUNCT =_SYM ]_PUNCT Oedd_VERB yn_PART [_PUNCT /=_PROPN ]_PUNCT oedd_AUX yn_PART gwneud_VERB ffrindiau_NOUN t'mod_VERB +_SYM
Ie_INTJ ._PUNCT
+_VERB yn_PART hawdd_ADJ so_ADV oedd_VERB lot_PRON ._PUNCT
Oedd_AUX lot_PRON o_ADP bobl_NOUN yna_ADV a_CONJ +_SYM
Ie_INTJ ._PUNCT
+_VERB wnaethon_VERB ni_PRON roi_VERB send_NOUN off_ADV dda_ADJ iddo_ADP fe_PRON t'mod_VERB +_SYM
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
+_VERB [_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
Gweud_VERB wrtha_VERB i_PRON am_ADP y_DET maestri_NOUN EP_PROPN y_DET Tystion_PROPN ._PUNCT
O'dd_NOUN Chef_ADJ yn_PART rhan_NOUN mawr_ADJ o_ADP 'r_DET EP_PROPN yna_DET yn_PART benodol_ADJ on'd_VERB o'dd_NOUN e_PRON ?_PUNCT
ie_INTJ ._PUNCT
yyy_ADV oedd_VERB ._PUNCT
So_PART wnaethon_VERB ni_PRON recordio_VERB hwn_DET ar_ADP ôl_NOUN  _SPACE [_PUNCT saib_NOUN ]_PUNCT Hen_ADJ Gelwydd_NOUN Prydain_PROPN Newydd_ADJ dw_AUX i_PRON 'n_PART meddwl_VERB ._PUNCT
Do_INTJ ._PUNCT
Do_INTJ ._PUNCT
A_PART  _SPACE [_PUNCT saib_NOUN ]_PUNCT wi_AUX 'n_PART meddwl_VERB mae_VERB Chef_X ar_ADP bob_DET bron_ADV ie_INTJ bron_ADV bob_DET trac_NOUN [_PUNCT =_SYM ]_PUNCT ar_ADP y_DET [_PUNCT /=_PROPN ]_PUNCT ar_ADP yr_DET ie_NOUN ar_ADP y_DET pedwar_NOUN trac_NOUN ar_ADP yr_DET  _SPACE ar_ADP yr_DET EP_PROPN ._PUNCT
O_ADP 'n_PART ni_PRON 'n_PART gweithio_VERB  _SPACE [_PUNCT saib_NOUN ]_PUNCT  _SPACE [_PUNCT saib_NOUN ]_PUNCT yng_ADP Nghaerdydd_PROPN os_CONJ dwi_AUX 'n_PART cofia_VERB yn_ADP y_DET stiwdio_NOUN [_PUNCT giglan_VERB ]_PUNCT ie_INTJ ._PUNCT
ymm_CONJ a_CONJ o'dd_NOUN Chef_ADJ [_PUNCT =_SYM ]_PUNCT yn_PART [_PUNCT /=_PROPN ]_PUNCT yn_PART rhan_NOUN mawr_ADJ o_ADP 'r_DET prosiect_NOUN yna_DET ._PUNCT
Ie_INTJ ._PUNCT
yyy_ADV lot_PRON o_ADP atgofion_VERB a_CONJ wrth_ADP gwrs_NOUN mae_VERB 'n_PART arbennig_ADJ o_ADP drist_ADJ pan_CONJ mae_VERB rhywun_NOUN mor_ADV ifanc_ADJ a_CONJ hyfryd_ADJ yn_PART ein_PRON gadael_VERB ni_PRON ond_CONJ pan_CONJ mae_VERB rhywun_NOUN mor_ADV ifanc_ADJ a_CONJ talentog_ADJ yn_PART gadael_VERB a_CONJ sy_VERB 'di_PART recordio_VERB gymaint_ADV o_ADP gerddoriaeth_NOUN  _SPACE efalle_ADV ddim_PART yn_ADP y_DET prif_ADJ ffrwd_NOUN ond_CONJ wedi_PART creu_VERB farc_NOUN yn_ADP y_DET byd_NOUN cerddoriaeth_NOUN  _SPACE tan_ADP ddaearol_ADJ a_CONJ wedi_PART rhoi_VERB gymaint_ADV o_ADP egni_NOUN a_CONJ talent_VERB mewn_ADP i_ADP berfformiadau_NOUN mae_VERB 'r_DET gerddoriaeth_NOUN yna_DET yn_PART byw_VERB am_ADP byth_ADV wedyn_ADV on'd_VERB yw_VERB e_DET cyfenw_NOUN ?_PUNCT
[_PUNCT giglan_VERB ]_PUNCT ydy_VERB [_PUNCT giglan_VERB ]_PUNCT ie_INTJ ._PUNCT
A_CONJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Dyna_DET pam_ADV '_PUNCT dyn_NOUN [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Ie_INTJ mae_VERB 'n_PART ._PUNCT
Na_INTJ mae_AUX jyst_X yn_PART drueni_NOUN achos_ADP o'dd_NOUN ganddo_ADP gymaint_ADV [_PUNCT =_SYM ]_PUNCT i_PRON [_PUNCT /=_PROPN ]_PUNCT i_ADP gynnig_NOUN ym_ADP +_SYM
Ie_INTJ ._PUNCT
+_VERB ond_CONJ ie_INTJ ._PUNCT
A_CONJ dyna_PRON pam_ADV '_PUNCT dyn_NOUN ni_PRON 'n_PART lwcus_ADJ bod_AUX ni_PRON 'n_PART gallu_ADV chwarae_VERB traciau_NOUN heno_ADV i_ADP  _SPACE [_PUNCT =_SYM ]_PUNCT i_PRON [_PUNCT /=_PROPN ]_PUNCT i_ADP +_SYM
Ie_INTJ ._PUNCT
Ie_INTJ ._PUNCT
+_VERB gofio_VERB fe_PRON ._PUNCT
Lots_NOUN o_ADP draciau_NOUN gyda_ADP [_PUNCT anadlu_VERB ]_PUNCT gyda_ADP   _SPACE Chef_X yn_PART rhan_NOUN mawr_ADJ ohonyn_ADP nhw_PRON wrth_ADP gwrs_NOUN fel_ADP aelod_NOUN o_ADP 'r_DET Tystion_PROPN ._PUNCT
Wnewn_VERB ni_PRON orffen_VERB gyda_ADP M.O.M.Y.F.G_PROPN a_CONJ wedyn_ADV wnewn_ADP ni_PRON chwarae_VERB Swci_PROPN gyda_ADP Coc_PROPN Roc_PROPN [_PUNCT saib_NOUN ]_PUNCT  _SPACE o_ADP dwy_NUM fil_NUM a_CONJ saith_NUM ._PUNCT
Trac_NOUN arall_ADJ o'dd_NOUN enwg_VERB cyfenw_NOUN Chef_ADJ yn_PART rhan_NOUN ohono_ADP fe_PRON ._PUNCT
9757.763_VERB
10591.723_NOUN
