Arweinyddion_NOUN Cynllun_NOUN Gofal_NOUN
ANGEN_NOUN
Arweinyddion_PROPN
Cynlluniau_NOUN Gofal_NOUN Gwyliau_PROPN
3_NUM swydd_NOUN i_ADP 'w_PRON llenwi_VERB
Mae_VERB Menter_PROPN enw_NOUN a_CONJ Menter_PROPN enw_NOUN __NOUN yn_PART chwilio_VERB am_ADP dri_NUM unigolyn_NOUN addas_ADJ ,_PUNCT cymwys_VERB a_CONJ brwdfrydig_VERB i_PART arwain_VERB eu_DET Cynlluniau_NOUN Gofal_NOUN yn_PART Ysgolion_NOUN __NOUN __NOUN (_PUNCT __NOUN __NOUN __NOUN __NOUN )_PUNCT ,_PUNCT Ysgol_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ac_CONJ Ysgol_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN (_PUNCT __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN )_PUNCT yn_ADP ystod_NOUN cyfnodau_NOUN Gwyliau_PROPN Ysgol_PROPN ._PUNCT
£_SYM 10.40_NUM yr_DET awr_NOUN
8:15_ADV am_ADP -_PUNCT 5:45_VERB pm_PRON yn_PART ystod_NOUN pob_DET cyfnod_NOUN gwyliau_NOUN ysgol_NOUN (_PUNCT Llun_PROPN -_PUNCT Gwener_PROPN )_PUNCT
&_ADP hyd_NOUN at_ADP ddau_NUM ddiwrnod_NOUN y_DET tymor_NOUN ar_ADP sail_NOUN hyblyg_ADJ i_PART gydlynu_VERB 'r_DET amserlen_NOUN ._PUNCT
Mae_VERB cymhwyster_NOUN Lefel_NOUN 3_NUM neu_CONJ uwch_ADJ mewn_ADP Gofal_NOUN Plant_PROPN /_PUNCT Chwarae_PROPN a_CONJ phrofiad_NOUN o_ADP weithio_VERB ym_ADP maes_NOUN Gofal_NOUN Plant_PROPN yn_PART hanfodol_ADJ ar_ADP gyfer_NOUN y_DET swyddi_NOUN hyn_PRON
Dyddiad_NOUN Cau_PROPN :_PUNCT 17_NUM /_SYM 03_NUM /_SYM 2017_NUM 12_NUM pm_NOUN
Cyfweliadau_NOUN :_PUNCT 21_NUM &_SYM 22_NUM /_SYM 03_NUM /_SYM 2017_NUM 6_NUM -_SYM 8_NUM pm_NOUN
Am_ADP swydd_NOUN ddisgrifiad_NOUN a_CONJ ffurflen_NOUN gais_VERB cysylltwch_VERB â_ADP
cyfeiriad_NOUN -_PUNCT bost_NOUN
/_PUNCT rhif_NOUN
