"_PUNCT Cylchlythyr_NOUN Haf_NOUN meddwl.org","Y_NOUN diweddaraf_ADJ o_ADP wefan_NOUN meddwl_VERB ._PUNCT org_NOUN
Cyfleoedd_AUX i_PRON ymuno_VERB â_ADP meddwl_VERB ._PUNCT org_NOUN yn_ADP Eisteddfod_NOUN Llanrwst_PROPN
Crys_INTJ -_PUNCT T_NUM "_PUNCT "_PUNCT Paid_VERB a_CONJ Bod_PROPN Ofn_PROPN "_PUNCT "_PUNCT -_PUNCT Eden_PROPN
Mae_AUX 'r_DET grwp_NOUN Eden_PROPN wedi_PART creu_VERB crysau_NOUN -_PUNCT T_NUM '_PUNCT Paid_VERB â_ADP Bod_PROPN Ofn_PROPN '_PUNCT gydag_ADP elw_NOUN yn_PART mynd_VERB tuag_ADP at_ADP meddwl_VERB ._PUNCT org_NOUN
Fe_PART werthodd_VERB y_DET stoc_NOUN cyntaf_ADJ mewn_ADP dim_PRON o_ADP dro_NOUN ._PUNCT
Ond_CONJ y_DET newyddion_NOUN da_ADJ yw_VERB ei_PRON bod_VERB ar_ADP gael_VERB unwaith_ADV eto_ADV !_PUNCT
Gallwch_VERB brynu_VERB crys_NOUN am_ADP £_SYM 15_NUM (_PUNCT +_VERB cost_NOUN postio_VERB )_PUNCT drwy_ADP gysylltu_VERB 'n_PART uniongyrchol_ADJ gydag_ADP Eden_X ._PUNCT
enw_NOUN
Fe_PART fydd_VERB y_DET crysau_NOUN -_PUNCT T_NUM hefyd_ADV ar_ADP gael_VERB o_ADP stondin_NOUN Adra_PROPN ar_ADP faes_NOUN yr_DET Eisteddfod_NOUN Genedlaethol_ADJ ._PUNCT
Ewch_VERB i_ADP siopa_NOUN cyn_ADP iddynt_ADP werthu_VERB allan_ADV eto_ADV !_PUNCT
Diolch_NOUN i_ADP Eden_PROPN am_ADP bob_DET cefnogaeth_NOUN i_ADP meddwl_VERB ._PUNCT org_NOUN !_PUNCT
Eisteddfod_NOUN Genedlaethol_ADJ -_PUNCT 9_NUM Awst_PROPN ,_PUNCT 12_NUM yp_DET
'_PUNCT Perfformio_PROPN -_PUNCT ydi_VERB o_ADP 'n_PART dda_ADJ i_ADP ni_PRON ?_PUNCT '_PUNCT
Eisteddfod_NOUN Genedlaethol_ADJ -_PUNCT 10_NUM Awst_PROPN 2.30_NUM yp_DET
'_PUNCT Darllen_PROPN yn_ADP Well_X :_PUNCT Iechyd_NOUN Meddwl_PROPN '_PUNCT
Bathodynnau_NOUN meddwl_VERB ._PUNCT org_NOUN
enw_NOUN
Bydd_VERB bathodynnau_VERB meddwl_VERB ._PUNCT org_NOUN ar_ADP gael_VERB o_ADP 'r_DET stondinau_NOUN isod_ADV ar_ADP faes_NOUN yr_DET Eisteddfod_NOUN Genedlaethol_ADJ am_ADP gyfraniad_NOUN bach_ADJ :_PUNCT
Cymeithas_NOUN yr_DET Iaith_NOUN Gymraeg_PROPN
Cyngor_NOUN Llyfrau_PROPN Cymru_PROPN
Cadwyn_PROPN
Adra_PROPN
Diolch_VERB i_ADP bob_DET un_NUM am_ADP eu_DET cefnogaeth_NOUN parod_ADJ ._PUNCT
Llyfryn_NOUN iechyd_NOUN meddwl_VERB ar_ADP gael_VERB i_ADP athrawon_VERB
enw_NOUN
Mae_AUX pob_DET ysgol_NOUN uwchradd_AUX wedi_PART derbyn_VERB y_DET pecyn_NOUN '_PUNCT Mae_VERB gan_ADP Bawb_PROPN Iechyd_NOUN Meddwl_PROPN '_PUNCT ond_CONJ fe_PART fydd_VERB copiau_NOUN o_ADP 'r_DET llyfryn_NOUN '_PUNCT Cefnogi_PROPN iechyd_NOUN meddwl_VERB a_CONJ lles_NOUN mewn_ADP ysgolion_NOUN uwchradd_ADJ '_PUNCT sy_VERB 'n_PART addas_ADJ at_ADP CA_X 3_NUM ,_PUNCT ar_ADP gael_VERB o_ADP stondin_NOUN UCAC_PROPN ar_ADP faes_NOUN yr_DET Eisteddfod_PROPN ._PUNCT
Diolch_VERB i_ADP UCAC_PROPN am_ADP gytuno_VERB i_ADP gael_VERB rhain_PRON ar_ADP eu_DET stondin_NOUN ._PUNCT
Fideos_NOUN
Delio_VERB ag_ADP iselder_NOUN ar_ADP ôl_NOUN gadael_VERB prifysgol_NOUN :_PUNCT BBC_NOUN Cymru_PROPN Fyw_PROPN
Non_PROPN Parry_PROPN a_CONJ Gareth_PROPN Glyn_PROPN yn_PART trafod_VERB pyliau_NOUN o_ADP banig_NOUN :_PUNCT Heno_NOUN S4C_PROPN
Cliciwch_VERB enw_NOUN am_ADP ragor_NOUN o_ADP fideos_NOUN ar_ADP iechyd_NOUN meddwl_VERB
Erthyglau_PROPN
Gorflino_NOUN -_PUNCT a_PART fydd_AUX eich_DET gwyliau_NOUN Haf_NOUN yn_PART ei_PRON ddatrys_VERB ?_PUNCT
Gwneud_VERB yr_DET anweledig_NOUN yn_PART weladwy_ADJ :_PUNCT Codeword_NOUN Pineapple_PROPN
Cliciwch_VERB enw_NOUN am_ADP ragor_NOUN o_ADP erthyglau_NOUN ar_ADP iechyd_NOUN meddwl_VERB
Myfyrdodau_PROPN
Argyfwng_NOUN #_ADJ iechydmeddwl_NOUN y_DET Gymraeg_PROPN -_PUNCT enwg_VERB cyfenw_NOUN
Yoga_INTJ ,_PUNCT Therapi_PROPN a_CONJ Siarad_PROPN -_PUNCT enwb_VERB enwb_NOUN
Cliciwch_VERB enw_NOUN am_ADP ragor_NOUN o_ADP fyfyrdodau_NOUN ar_ADP iechyd_NOUN meddwl_VERB
Sut_ADV i_ADP gysylltu_VERB neu_CONJ gyfrannu_VERB :_PUNCT
Rydym_VERB yn_PART eich_PRON croesawu_VERB i_ADP gysylltu_VERB os_CONJ hoffech_VERB rannu_VERB eich_DET profiadau_NOUN chi_PRON am_ADP unrhyw_DET agwedd_NOUN o_ADP iechyd_NOUN meddwl_VERB ._PUNCT
Beth_PRON am_ADP :_PUNCT
*_SYM gyfrannu_VERB darn_NOUN i_ADP 'r_DET adran_NOUN '_PUNCT Myfyrdodau_NOUN '_PUNCT ?_PUNCT
*_SYM gynnig_NOUN awgrymiadau_ADJ ar_ADP sut_ADV i_ADP ddatblygu_VERB 'r_DET wefan_NOUN neu_CONJ rannu_VERB syniadau_NOUN ?_PUNCT
*_SYM gyfieithu_VERB gwybodaeth_NOUN am_ADP iechyd_NOUN meddwl_VERB i_ADP 'r_DET Gymraeg_PROPN ?_PUNCT
*_SYM bod_VERB ar_ADP gael_VERB i_ADP siarad_VERB yn_PART gyhoeddus_ADJ dros_ADP meddwl_VERB ._PUNCT org_NOUN enw_NOUN ?_PUNCT
*_SYM gyfrannu_VERB yn_PART ariannol_ADJ enw_NOUN i_ADP gynnal_VERB a_CONJ chadw_VERB meddwl_VERB ._PUNCT org_NOUN enw_NOUN ?_PUNCT
e_PRON -_PUNCT bostiwch_VERB -_PUNCT ebost_NOUN
