Crynodeb_NOUN Rownd_PROPN a_CONJ Rownd_X W_X 16_NUM
Tra_CONJ bo_AUX Barry_PROPN yn_ADP Lerpwl_PROPN yn_PART ceisio_VERB adfer_NOUN y_DET sefyllfa_NOUN gyda_ADP 'r_DET rhai_PRON oedd_VERB yn_PART gyfrifol_ADJ am_ADP dorri_VERB i_ADP mewn_ADP i_ADP 'r_DET fflat_NOUN ,_PUNCT mae_VERB effaith_NOUN y_DET digwyddiad_NOUN yn_PART dal_ADV i_ADP ffrwtian_VERB ._PUNCT
Mae_AUX Kay_PROPN yn_PART parhau_VERB i_PART fygwth_ADV mynd_VERB at_ADP yr_DET heddlu_NOUN ac_CONJ mae_AUX Carys_PROPN yn_PART teimlo_VERB bod_VERB rhyw_DET anffawd_NOUN ar_ADP droed_NOUN yn_ADP y_DET tŷ_NOUN newydd_ADJ ._PUNCT
Mae_AUX Barry_PROPN yn_PART dychwelyd_VERB gan_CONJ sicrhau_VERB pawb_PRON bod_AUX pethau_NOUN wedi_PART eu_DET lleddfu_VERB ,_PUNCT ond_CONJ tybed_ADV ydy_VERB hyn_PRON yn_PART wir_ADJ ?_PUNCT
Mae_AUX Dani_PROPN yn_PART bygwth_VERB ymweld_VERB â_ADP 'r_DET salon_NOUN ac_CONJ mae_AUX pawb_PRON yn_PART ofni_VERB y_PART bydd_VERB gwrthdaro_PART rhyngddi_VERB hi_PRON a_CONJ Sophie_X ._PUNCT
Mae_VERB trwbl_NOUN ar_ADP droed_NOUN draw_NOUN yn_PART fflat_ADJ Wyn_PROPN hefyd_ADV wrth_ADP i_ADP Wyn_PROPN a_CONJ Vince_PROPN ymuno_VERB mewn_ADP cynllun_NOUN i_ADP dwyllo_VERB Cathryn_PROPN ._PUNCT
Caiff_VERB Wil_PROPN goblyn_NOUN o_ADP ffrae_NOUN gan_ADP John_PROPN wedi_PART iddo_ADP ddarganfod_VERB ei_PRON fod_AUX wedi_PART derbyn_VERB llythyr_NOUN am_ADP oryrru_VERB ._PUNCT
A_PART fydd_AUX Wil_PROPN yn_PART medru_ADV cadw_VERB cyfrinach_NOUN Rhys_PROPN dan_ADP y_DET fath_NOUN bwysau_NOUN ?_PUNCT
Rownd_NOUN a_CONJ Rownd_PROPN
Mawrth_NOUN ac_CONJ Iau_VERB 7.30_SYM ,_PUNCT S4C_PROPN
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Omnibws_VERB dydd_NOUN Sul_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Gwefan_NOUN :_PUNCT s4c.cymru_NOUN
Cynhyrchiad_NOUN Rondo_NOUN Media_PROPN ar_ADP gyfer_NOUN S4C_PROPN
