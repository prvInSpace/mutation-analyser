Prynhawn_NOUN da_ADJ i_ADP chi_PRON a_CONJ chroeso_VERB i_ADP enw_NOUN ._PUNCT
Am_ADP y_DET pedair_NUM wythnos_NOUN nesa'_ADJUNCT fe_PRON fyddwn_AUX ni_PRON 'n_PART clywed_VERB straeon_NOUN o_ADP bedwar_NUM ban_DET byd_NOUN trw_NOUN '_PUNCT lyged_VERB rhai_PRON o_ADP 'r_DET Cymry_PROPN Cymraeg_PROPN sydd_AUX yn_PART byw_VERB ac_CONJ yn_PART bod_VERB dramor_ADJ ._PUNCT
Fe_PART gawn_VERB ni_PRON gip_NOUN ar_ADP fywyd_NOUN mewn_ADP gwledydd_NOUN gwahanol_ADJ a_CONJ chlywed_VERB am_ADP yr_DET hyn_PRON sy_AUX 'n_PART cael_VERB sylw_NOUN yn_ADP y_DET newyddion_NOUN ymhell_ADV oddi_ADP wrthom_VERB ni_PRON yma_ADV yng_ADP lleoliad_NOUN ._PUNCT
Yn_ADP y_DET rhaglen_NOUN gynta'_ADJUNCT hon_DET sgwrs_NOUN gyda_ADP llysgennad_NOUN lleoliad_NOUN yn_PART un_NUM o_ADP wledydd_NOUN lleoliad_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT i_ADP fod_VERB yn_PART gynrychiolydd_NOUN llywodraeth_NOUN yn_PART __NOUN ._PUNCT
Fe_PART fyddwn_AUX ni_PRON hefyd_ADV yn_PART crwydro_VERB prif_ADJ ddinas_NOUN lleoliad_NOUN lle_NOUN mama_NOUN olion_NOUN hanes_NOUN cythryblus_ADJ y_DET wlad_NOUN i_ADP 'w_PRON gweld_VERB yn_PART glir_ADJ ._PUNCT
Ac_CONJ yn_PART clywed_VERB am_ADP ddylanwad_NOUN chwaraeon_NOUN ac_CONJ effaith_NOUN helyntion_NOUN ym_ADP myd_NOUN y_DET campe_NOUN '_PUNCT ar_ADP drigolion_NOUN lleoliad_NOUN ._PUNCT
Ond_CONJ 'i_PRON ni_PRON 'n_PART dechre_ADJ '_PUNCT yn_PART lleoliad_NOUN ._PUNCT
Ma'_VERBUNCT gwrthdaro_PART diplomyddol_ADJ rhwng_ADP enwg_VERB cyfenw_NOUN a_CONJ gwledydd_NOUN y_DET gorllewin_NOUN wedi_PART bod_VERB yn_PART amlwg_ADJ iawn_ADV dros_ADP yr_DET wythnose_NOUN '_PUNCT d'wetha_VERB gyda_ADP diplomyddion_NOUN yn_PART cael_VERB eu_PRON hanfon_VERB yn_PART ôl_NOUN i_ADP __NOUN o_ADP __NOUN __NOUN __NOUN ac_CONJ un_NUM deg_NUM naw_NUM o_ADP wledydd_NOUN yyy'ill_VERB gan_ADP gynnwys_VERB yr_DET __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ac_CONJ __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Yn_PART dilyn_VERB yr_DET achos_NOUN o_ADP wenwyno_VERB 'r_DET [_NOUN aneglur_ADJ ?_PUNCT ]_PUNCT a_CONJ 'i_PRON ferch_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM yng_ADP lleoliad_NOUN yn_PART ôl_NOUN ym_ADP mis_NOUN Mawrth_PROPN ._PUNCT
Ma_VERB 'r_DET llywodraeth_NOUN  _SPACE lleoliad_NOUN wedi_PART gwadu_VERB bod_VERB ag_ADP unrhyw_DET rhan_NOUN yn_ADP yr_DET hyn_PRON ddigwyddodd_VERB ond_CONJ mae_AUX e_PRON wedi_PART codi_VERB tensiyne_NOUN '_PUNCT rhwng_ADP lleoliad_NOUN a_CONJ 'r_DET gorllewin_NOUN ._PUNCT
A_PART dw_AUX i_PRON 'di_PART bod_AUX yn_PART siarad_VERB â_ADP Chymraes_NOUN o_ADP lleoliad_NOUN yn_PART wreiddiol_ADJ sy_VERB 'di_PART bod_AUX yn_PART byw_VERB yn_ADP y_DET wlad_NOUN ers_ADP blynyddoedd_NOUN ._PUNCT
Ma'_VERBUNCT enwb_NOUN cyfenw_NOUN wedi_PART bod_AUX yn_PART byw_VERB yn_PART ninas_NOUN __NOUN yn_PART __NOUN __NOUN __NOUN ._PUNCT
dw_AUX i_PRON 'n_PART dysgu_VERB Saesneg_PROPN mewn_ADP ysgol_NOUN breifat_ADJ  _SPACE sy_AUX 'n_PART dysgu_VERB plant_NOUN ac_CONJ oedolion_NOUN yn_PART lleoliad_NOUN lle_NOUN '_PUNCT da_ADJ ni_PRON dwy_NUM fil_NUM milltir_NOUN o_ADP lleoliad_NOUN ._PUNCT
A_CONJ pa_PART fath_NOUN o_ADP ddinas_NOUN yw_VERB lleoliad_NOUN wedyn_ADV ?_PUNCT
ym_ADP mae_VERB 'n_PART ddinas_NOUN reit_NOUN fawr_ADJ ._PUNCT
Ma'_VERBUNCT '_PUNCT ne_NOUN tua_ADV chwe_NUM chan_VERB mil_NUM o_ADP bobl_NOUN yn_PART byw_VERB 'ma_ADV ._PUNCT
Ma'_VERBUNCT popeth_PRON '_PUNCT da_ADJ chi_PRON angen_NOUN yma_DET yn_ADP y_DET ddinas_NOUN ._PUNCT
Ma'_VERBUNCT nhw_PRON 'n_PART galw_VERB hi_PRON 'n_PART prif_ADJ ddinas_NOUN oil_NOUN and_X gas_NOUN yn_PART lleoliad_NOUN ._PUNCT
Mae_AUX 'r_DET rhan_NOUN fwyaf_ADJ o_ADP 'r_DET cwmnioedd_NOUN sydd_AUX yn_PART gweithio_VERB yn_ADP y_DET diwylliant_NOUN olew_NOUN a_CONJ gas_NOUN ._PUNCT
Mae_VERB gen_ADP y_DET nhw_PRON 'i_PRON headquarters_NOUN yn_ADP y_DET ddinas_NOUN yma_DET sy_AUX 'n_PART golygu_VERB bod_VERB 'na_ADV lot_PRON o_ADP bobl_NOUN yn_PART gorfod_ADV dysgu_VERB Saesneg_PROPN gan_CONJ bod_VERB 'na_ADV dipyn_PRON o_ADP  _SPACE fusnesoedd_NOUN yma_ADV sydd_VERB yn_PART o_ADP lleoliad_NOUN o_ADP [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT [_PUNCT -_PUNCT ]_PUNCT
Ac_CONJ felly_ADV pa_ADV mor_ADV bwysig_ADJ fydde_VERB '_PUNCT chi_PRON 'n_PART gweud_VERB yw_VERB 'r_DET ddinas_NOUN i_ADP lleoliad_NOUN fel_ADP gwlad_NOUN oherwydd_CONJ 'i_PRON ni_PRON 'n_PART clywed_VERB am_ADP pa_ADV mor_ADV bwysig_ADJ yw_VERB 'r_DET diwydiant_NOUN ynni_NOUN i_ADP economi_NOUN lleoliad_NOUN ._PUNCT
Ma_VERB 'n_PART ddinas_NOUN digon_ADV llewyrchus_ADJ oherwydd_CONJ wel_ADV gan_CONJ fod_VERB 'na_ADV lot_PRON o_ADP bres_VERB yn_PART dod_VERB i_ADP fewn_ADP trwy_ADP 'r_DET ddinas_NOUN ._PUNCT
'Di_PART pawb_PRON yn_PART lleoliad_NOUN hyd_NOUN yn_PART oed_NOUN ddim_PART yn_PART gw'bod_VERB am_ADP lleoliad_NOUN ._PUNCT
'_PUNCT Di_AUX nhw_PRON 'm_PART yn_PART dallt_VERB bod_VERB hi_PRON 'n_PART ddinas_NOUN mor_ADV fawr_ADJ achos_ADP gan_CONJ bod_VERB lleoliad_NOUN mor_ADV fawr_ADJ mae_VERB 'n_PART anodd_ADJ i_ADP bobl_NOUN o_ADP lleoliad_NOUN hyd_NOUN yn_PART oed_NOUN ddallt_VERB bod_VERB '_PUNCT ne_NOUN fywyd_NOUN tu_NOUN allan_ADV i_ADP __NOUN yma_ADV ._PUNCT
Ma'_VERBUNCT straeon_NOUN o_ADP lleoliad_NOUN ._PUNCT
Ma_INTJ '_PUNCT lleoliad_NOUN wedi_PART bod_VERB yn_PART -_PUNCT yn_PART amlwg_ADJ iawn_ADV yn_ADP y_DET newyddion_NOUN draw_ADV fan_NOUN hyn_DET '_PUNCT nôl_NOUN yng_ADP lleoliad_NOUN dros_ADP yr_DET wythnose_NOUN '_PUNCT a_CONJ 'r_DET misoedd_NOUN diwetha'_ADJUNCT ._PUNCT
188.867_NOUN
O_ADP 'ch_PRON safbwynt_NOUN chi_PRON o_ADP le_NOUN 'i_PRON chi_NOUN 'n_PART '_PUNCT ishte_NOUN ac_CONJ yn_PART gwylio_VERB ac_CONJ yn_PART gwrando_VERB bebe_NOUN '_PUNCT newch_NOUN chi_PRON o_ADP 'r_DET berthynas_NOUN  _SPACE rhwng_ADP lleoliad_NOUN a_CONJ lleoliad_NOUN a_CONJ gwledydd_NOUN yyy'ill_PROPN y_DET gorllewin_NOUN y_DET dyddie_NOUN '_PUNCT yma_ADV ?_PUNCT
Wel_INTJ mama_NOUN hwn_DET yn_PART sgwrs_NOUN '_PUNCT da_ADJ ni_PRON 'n_PART gael_VERB yn_PART reit_ADV aml_ADJ yma_ADV gan_CONJ fod_AUX pobl_NOUN yn_PART dallt_VERB bod_AUX y_DET gorllewin_NOUN ddim_PART yn_PART hollol_ADV positif_ADJ o_ADP hyd_NOUN am_ADP lleoliad_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB na_PART 'r_DET broblem_NOUN fwyaf_ADJ ydy_AUX bod_AUX neb_PRON yn_PART dallt_VERB lleoliad_NOUN ._PUNCT
Mae_VERB 'n_PART wlad_NOUN digon_ADV gwahanol_ADJ i_ADP lleoliad_NOUN i_ADP lleoliad_NOUN i_ADP gwledydd_NOUN eraill_ADJ yn_PART __NOUN ._PUNCT
Di_VERB 'm_PRON wastad_ADV yn_PART barod_ADJ i_PART neud_VERB fel_ADP mama_NOUN <_SYM lleoliad_NOUN '_PUNCT isio_VERB iddi_ADP neud_VERB a_CONJ dw_AUX i_PRON 'n_PART meddwl_VERB na_CONJ dyna_DET bebe_NOUN 'di_PART 'r_DET broblem_NOUN fwyaf_ADJ ._PUNCT
Yr_DET [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT neu_CONJ mama_VERB nhw_PRON jest_ADV yn_PART gwneud_VERB fel_ADP mama_NOUN lleoliad_NOUN '_PUNCT isio_VERB iddi_ADP neud_VERB ._PUNCT
Ag_NOUN ym_ADP mama_NOUN 'na_ADV lot_PRON o_ADP wledydd_NOUN yn_PART barod_ADJ i_PART gytuno_VERB efo_ADP bopeth_PRON a_CONJ 'di_PART lleoliad_NOUN ddim_PART wastad_ADV yn_PART -_PUNCT yn_PART barod_ADJ i_PART gytuno_VERB ac_CONJ oherwydd_CONJ hynny_PRON mae_AUX 'n_PART cael_VERB probleme_VERB '_PUNCT rhyngwladol_ADJ ._PUNCT
Ma'_VERBUNCT gen_ADP y_DET ni_PRON sanctions_VERB rŵan_ADV ond_CONJ 'di_PART nhw_PRON 'm_DET wir_ADJ yn_PART -_PUNCT gallu_VERB dw_VERB i_ADP 'm_DET wir_ADJ yn_PART teimlo_VERB 'r_DET sanctions_NOUN heblaw_PRON am_ADP fod_VERB hi_PRON 'n_PART anodd_ADJ prynu_VERB caws_NOUN yma_ADV ._PUNCT
A_CONJ 'r_DET Arlywydd_NOUN cyfenw_NOUN wedi_PART 'i_PRON ail-_NOUN ethol_VERB unwaith_ADV eto_ADV rhai_PRON misoedd_NOUN yn_PART ôl_NOUN ._PUNCT
Doedd_AUX neb_PRON yn_PART -_PUNCT yn_PART synnu_VERB gyda_ADP 'r_DET canlyniad_NOUN yna_DET amwn_NOUN i_PRON ?_PUNCT
Na_INTJ ._PUNCT
Na_INTJ doedd_VERB 'na_ADV neb_PRON yn_PART disgwyl_VERB dim_DET -_PUNCT dim_PART byd_NOUN arall_ADJ ._PUNCT
Wel_CONJ y_DET broblem_NOUN ydy_VERB does_NOUN 'na_ADV neb_PRON yn_PART ei_DET erbyn_ADP o_PRON ._PUNCT
Does_VERB 'na_ADV ddim_PART gwleidyddwyr_NOUN fysa_VERB 'n_PART gallu_ADV gwneud_VERB ei_DET waith_NOUN o_ADP ._PUNCT
Ma'_VERBUNCT -_PUNCT mama_NOUN rhai_PRON eraill_ADJ mama_VERB nhw_PRON 'n_PART -_PUNCT 'di_PART nhw_PRON -_PUNCT s'gen_NOUN y_DET nhw_PRON ddim_PART  _SPACE profiad_NOUN gwleidyddol_ADJ felly_ADV '_PUNCT swn_NOUN i_ADP 'm_DET yn_PART barod_ADJ i_ADP drystio_VERB neb_PRON arall_ADJ wir_ADV ._PUNCT
Dw_AUX i_PRON 'n_PART meddwl_VERB bod_VERB o_PRON 'n_PART fwy_ADJ i_ADP neud_VERB hefo_ADP propaganda_NOUN ._PUNCT
Ma_INTJ '_PUNCT cyfenw_NOUN yn_PART glyfar_ADJ iawn_ADV yn_ADP hynny_PRON ._PUNCT
Mae_AUX o_PRON 'n_PART gwerthu_VERB 'i_PRON hyn_PRON fel_ADP dyn_NOUN sydd_AUX ddim_PART yn_PART mynd_VERB i_PART adael_VERB i_ADP lleoliad_NOUN neud_VERB be_ADP mama_NOUN nhw_PRON 'n_PART d'eud_VERB ._PUNCT
Mae_VERB o_PRON 'n_PART barod_ADJ i_PART edrych_VERB ar_ADP ôl_NOUN lleoliad_NOUN i_PART edrych_VERB ar_ADP ôl_NOUN y_DET bobl_NOUN ond_CONJ mama_NOUN hynny_PRON 'n_PART fwy_PRON o_ADP image_NOUN na_CONJ be_DET mama_NOUN pobl_NOUN wir_ADJ yn_PART feddwl_VERB am_ADP y_DET peth_NOUN ._PUNCT
Mm_CONJ erbyn_ADP hyn_PRON mama_NOUN pobl_NOUN yn_PART dechre_ADJ '_PUNCT gweld_VERB drw_NOUN '_PUNCT hynny_PRON i_ADP gyd_ADP hefyd_ADV hefo_ADP 'r_DET holl_DET "_PUNCT dw_VERB i_PRON 'n_PART gry_NOUN '_PUNCT dw_VERB i_PRON 'n_PART gry_NOUN '_PUNCT "_PUNCT ._PUNCT
Felly_CONJ pa_PART fath_NOUN o_ADP straeon_NOUN sy_AUX -_PUNCT sy_AUX 'n_PART mynd_VERB â_ADP sylw_NOUN lle_NOUN 'i_PRON chi_NOUN ar_ADP hyn_PRON o_ADP bryd_NOUN enwb_NOUN ?_PUNCT
Beth_PRON yw_VERB 'r_DET pennawde_NOUN '_PUNCT  _SPACE yn_PART lleoliad_NOUN ar_ADP hyn_PRON o_ADP bryd_NOUN ?_PUNCT
Wel_INTJ mama_NOUN dechra'_VERBUNCT Mai_PROPN wastad_ADV yn_PART amser_NOUN gwylia'_VERBUNCT yma_ADV achos_NOUN mama_NOUN gen_ADP y_DET ni_PRON rŵan_ADV dri_NUM diwrnod_NOUN i_ADP ffwrdd_ADV ._PUNCT
A_CONJ hefyd_ADV wrth_ADP reswm_NOUN mama_NOUN gen_ADP y_DET ni_PRON  _SPACE y_DET nawfed_NOUN o_ADP Fai_NOUN sydd_VERB yn_PART victory_NOUN day_NUM ._PUNCT
Ma'_VERBUNCT hwnne_NOUN '_PUNCT dal_VERB yn_PART andros_ADJ o_ADP bwysig_ADJ yma_ADV a_CONJ bydd_VERB 'na_ADV parades_VERB yn_ADP bob_DET tre_NOUN mama_NOUN hynny_DET wrth_ADP reswm_NOUN ar_ADP hyn_PRON o_ADP bryd_NOUN yn_PART reit_NOUN bwysig_ADJ ._PUNCT
ymm_ADP bebe_NOUN arall_ADJ ?_PUNCT
Wel_INTJ mama_NOUN pawb_PRON yn_PART edrych_VERB ar_ADP lleoliad_NOUN a_CONJ lleoliad_NOUN ._PUNCT
378.792_VERB
Dw_AUX i_PRON 'n_PART sefyll_VERB yma_DET yn_PART un_NUM o_ADP brif_ADJ atyniadau_NOUN prif_ADJ ddinas_NOUN lleoliad_NOUN ._PUNCT
Hen_ADJ gastell_NOUN a_CONJ chaer_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM ._PUNCT
Drws_NOUN nesa'_ADJUNCT i_ADP fi_PRON mae_VERB 'r_DET cawr_NOUN o_ADP gofgolofn_NOUN yr_DET enillydd_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT neu_CONJ enwg_VERB fel_ADP mama_NOUN pobl_NOUN yn_PART hoff_ADJ o_ADP 'i_PRON alw_VERB ef_PRON ._PUNCT
Ma_INTJ '_PUNCT enwg_VERB yn_PART syllu_VERB tua_ADP 'r_DET pellter_NOUN gyda_ADP cholomen_NOUN yn_ADP ei_DET law_NOUN uwchben_ADP aber_NOUN yr_DET afon_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Ar_ADP draws_ADJ y_DET <_SYM aneglur_ADJ 1_NUM >_SYM mae_AUX 'n_PART teimlo_VERB fel_ADP pe_CONJ bawn_VERB i_ADP 'n_PART gallu_VERB gweld_VERB yr_DET holl_DET ffordd_NOUN at_ADP y_DET ffin_NOUN gyda_ADP lleoliad_NOUN  _SPACE ei_PRON fod_VERB dros_ADP ddau_NUM gant_NOUN kilometre_ADJ i_ADP ffwrdd_ADV ._PUNCT
'_PUNCT Mond_PROPN un_NUM neu_CONJ ddau_NUM o_ADP fynyddoedd_NOUN sydd_VERB i_ADP 'r_DET gogledd_NOUN i_PRON <_SYM aneglur_ADJ 1_NUM >_SYM gyda_ADP tirwedd_NOUN fflat_ADJ yn_PART gartref_NOUN i_ADP amaethyddiaeth_NOUN mwya'_ADJUNCT ffrwythlon_ADJ lleoliad_NOUN ._PUNCT
I_ADP 'r_DET chwith_NOUN ar_ADP hyd_NOUN y_DET <_SYM aneglur_ADJ 1_NUM >_SYM ma_VERB 'r_DET tirwedd_NOUN yn_PART hollol_ADV wahanol_ADJ ._PUNCT
Ar_ADP un_NUM ochr_NOUN yr_DET afon_NOUN mae_VERB [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT gyda_ADP 'i_PRON flociau_NOUN enfawr_ADJ o_ADP fflatiau_NOUN sosialaidd_ADJ ._PUNCT
Ac_CONJ ar_ADP yr_DET ochr_NOUN arall_ADJ ychydig_ADV o_ADP gilomedre_NOUN '_PUNCT i_PRON ffwrdd_ADV mae_VERB bryn_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
Y_DET mynydd_NOUN cyntaf_ADJ ar_ADP y_DET ffordd_NOUN i_ADP 'r_DET de_NOUN o_ADP <_SYM aneglur_ADJ 1_NUM >_SYM sy_VERB 'n_PART nodweddiadol_ADJ o_ADP dirwedd_NOUN de_NOUN a_CONJ chanol_ADJ lleoliad_NOUN ._PUNCT
yyy_ADV i_PART enwg_VERB '_PUNCT mond_ADV gael_VERB ei_DET godi_VERB ym_ADP mil_NUM naw_NUM dau_NUM wyth_NUM mae_VERB ei_DET leoliad_NOUN yn_PART symbolaidd_ADJ iawn_ADV ._PUNCT
Yn_PART wir_ADJ yr_DET afonydd_NOUN yma_DET mae_AUX 'n_PART goruchwylio_VERB oedd_VERB y_DET ffin_NOUN rhwng_ADP dau_NUM o_ADP ymerodraethe_VERB '_PUNCT mwya'_ADJUNCT lleoliad_NOUN ._PUNCT
Roedd_AUX y_DET fan_NOUN yma_DET lle_NOUN wi_AUX 'n_PART sefyll_VERB yn_PART rhan_NOUN o_ADP 'r_DET ymerodraeth_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM gydag_ADP ochr_NOUN draw_NOUN 'r_DET afonydd_NOUN tan_CONJ mil_NUM naw_NUM un_NUM wyth_NUM yn_ADP berchen_NOUN i_ADP 'r_DET ymerodraeth_NOUN <_SYM aneglur_ADJ 1_NUM >_SYM ._PUNCT
Dw_AUX i_PRON nawr_ADV yn_PART sefyll_VERB ar_ADP brif_ADJ sgwar_NOUN y_DET ddinas_NOUN ._PUNCT
Sgwar_NOUN y_DET weriniaeth_NOUN sydd_VERB ychydig_ADV yn_PART brysurach_NOUN na_CONJ [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT gysglyd_ADJ ._PUNCT
'_PUNCT Mond_VERB rhyw_DET bum_ADJ can_NOUN llath_NOUN o_ADP [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT dw_AUX i_ADP wedi_PART cerdded_VERB ar_ADP hyd_NOUN y_DET brif_ADJ stryd_NOUN gerdded_VERB a_CONJ siopa'_NOUNUNCT [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT sydd_AUX wedi_PART 'i_PRON enwi_VERB ar_ADP ôl_NOUN y_DET brenin_NOUN [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT ._PUNCT
O'ng_VERB nghwmpas_NOUN i_ADP ma_VERB 'r_DET <_SYM aneglur_ADJ 1_NUM >_SYM o_ADP gaffis_NOUN yn_PART gorlifo_VERB i_ADP 'r_DET sgwar_NOUN o_ADP 'r_DET strydoedd_NOUN cyfagos_ADJ gyda_ADP ffrindie_NOUN '_PUNCT yn_PART dala_VERB lan_ADV gyda_ADP 'i_PRON gilydd_NOUN yng_ADP nghalon_NOUN y_DET ddinas_NOUN ._PUNCT
Does_VERB dim_PRON ots_NOUN pa_PART amser_NOUN y_DET dydd_NOUN yw_VERB hi_PRON fydd_VERB o_PRON leia'_ADJUNCT un_NUM person_NOUN yma_DET yn_PART aros_VERB am_ADP ffrind_NOUN neu_CONJ gariad_NOUN yn_PART edrych_VERB yn_PART nerfus_ADJ at_ADP ei_DET ffôn_NOUN neu_CONJ oriawr_NOUN ._PUNCT
Os_CONJ d'wedwch_VERB chi_PRON [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT wrth_ADP y_DET ceffyl_NOUN i_ADP unrhyw_DET [_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT rhywun_NOUN o_ADP <_SYM aneglur_ADJ 1_NUM >_SYM bydd_AUX y_DET nhw_PRON 'n_PART gw'bod_VERB yn_PART union_ADJ ble_ADV i_ADP gwrdd_VERB â_ADP chi_PRON ._PUNCT
Y_DET tu_NOUN ôl_NOUN i_ADP 'r_DET ceffyl_NOUN mama_NOUN amgueddfa_NOUN genedlaethol_ADJ lleoliad_NOUN gyda_ADP dros_ADP pedwar_NOUN chan_NUM mil_NUM o_ADP delwedde_VERB '_PUNCT a_CONJ darne_NOUN '_PUNCT o_ADP bwys_NOUN hanesyddol_ADJ ._PUNCT
Ma_VERB 'r_DET dryse_NOUN '_PUNCT wedi_PART bod_VERB ar_ADP gau_VERB ers_ADP dwy_NUM fil_NUM a_CONJ thri_NUM ._PUNCT
Ma_INTJ '_PUNCT 'na_ADV sôn_VERB y_PART bydd_AUX y_DET lle_NOUN 'n_PART agor_VERB haf_NOUN yma_DET ar_ADP un_NUM o_ADP brif_ADJ ddyddiade_NOUN '_PUNCT hanesyddol_ADJ lleoliad_NOUN ._PUNCT
538.861_NOUN
1634.88_VERB
