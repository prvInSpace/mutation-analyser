Gwaith_PROPN
wrth_ADP deithio_VERB i_ADP 'r_DET swyddfa_NOUN un_NUM bore_NOUN ,_PUNCT
r'on_NOUN ar_ADP fin_NOUN blin_ADJ rhoi_VERB 'r_DET gorau_ADJ
i_ADP 'r_DET hen_ADJ hyll_ADJ waith_NOUN -_PUNCT
ag_CONJ hir_ADJ hyll_ADJ daith_NOUN -_PUNCT
am_PART dreulio_VERB bywyd_NOUN hawsterau_NOUN
Taten_PROPN
does_VERB dim_DET byd_NOUN yn_PART well_ADJ na_CONJ 'r_DET hen_ADJ daten_NOUN
ond_CONJ efallai_ADV mai_PART un_NUM neu_CONJ dwy_NUM gacen_NOUN
yn_PART gogleisio_VERB 'r_DET tafod_NOUN
ac_CONJ arwain_VERB i_ADP drallod_NOUN
a_PART chwythu_VERB 'r_DET bol_NOUN bach_ADJ hyd_ADP at_ADP gasgen_NOUN
Cerdded_VERB -_PUNCT seiclo_VERB
wrth_ADP gerdded_VERB mynyddoedd_NOUN a_CONJ seiclo_VERB
dwi_AUX 'n_PART teimlo_VERB fy_DET hun_PRON fel_ADP hen_ADJ seico_NOUN
mae_VERB 'r_DET gwyntoedd_NOUN mor_ADV llym_ADJ
nes_CONJ rhewi_VERB fy_DET nhin_NOUN
ag_CONJ achosi_VERB llawer_ADV deigryn_NOUN a_CONJ gwylltio_VERB
Llwyni_PROPN
Tra_PART estyn_VERB i_ADP ganol_NOUN y_DET llwyni_VERB
'_PUNCT Dwi_AUX 'n_PART cyffwrdd_VERB rhywbeth_NOUN fel_ADP bryntni_VERB
Beth_PRON sy_VERB dan_ADP fy_DET llaw_NOUN ?_PUNCT
gobeithio_VERB nid_PART baw_NOUN ._PUNCT
os_CONJ oes_VERB -_PUNCT rhy_ADV hwyr_ADJ dechrau_VERB poeni_VERB ._PUNCT
Scrabble_PROPN
es_VERB i_ADP un_NUM tro_NOUN i_ADP gwrdd_VERB Scrabble_PROPN ,_PUNCT
doedd_VERB siw_NOUN na_DET miw_NOUN na_CONJ pharabl_NOUN ._PUNCT
didoli_VERB 'r_DET dyfnder_NOUN geirfa_NOUN
mor_ADV ddwys_ADJ â_ADP pheth_NOUN lladdfa_NOUN ;_PUNCT
a_CONJ 'r_DET tawelwch_NOUN yn_PART hollol_ADV ddigabl_ADJ ._PUNCT
Angau_NOUN
dwedir_VERB bod_VERB angau_NOUN yn_PART agos_ADJ
amser_NOUN ei_DET dyfodiad_NOUN yn_PART amwys_ADJ
ar_ADP adeg_NOUN bydd_VERB chwaliad_NOUN
rhyw_DET fath_NOUN o_ADP ddiddymiad_NOUN
byddech_VERB chithau_PRON llai_ADJ na_CONJ ddibwys_ADJ
Ffidil_PROPN
Roedd_VERB ffidil_NOUN yn_ADP y_DET to_NOUN yn_PART meddwl_VERB ,_PUNCT
'_PUNCT pam_ADV wyf_AUX i_PRON o_ADP hyd_NOUN mewn_ADP mwdwl_NOUN ?_PUNCT '_PUNCT
'_PUNCT Gall_VERB fy_DET nhonau_NOUN i_ADP gyd_ADP
swyno_VERB 'r_DET holl_DET fyd_NOUN
ond_CONJ all_VERB dim_PRON fy_PRON achub_VERB o_ADP fod_VERB yn_PART ffŵl_ADJ '_PUNCT
Bwyd_PROPN
Ar_ADP ôl_NOUN wedi_PART cwrdd_VERB â_ADP 'r_DET blonegyddion_NOUN
ro_ADP 'n_PART i_VERB ar_ADP ben_NOUN pen_NOUN fy_DET nigon_NOUN ._PUNCT
bron_ADV â_ADP llwgu_VERB gan_ADP nwyd_NOUN
gwelais_VERB ormodedd_NOUN o_ADP fwyd_NOUN
ond_CONJ yn_PART fuan_ADJ oedd_VERB lestri_NOUN yn_PART weigion_VERB
Tylluan_NOUN
Mae_VERB 'r_DET coed_NOUN ,_PUNCT cefn_NOUN nos_NOUN ,_PUNCT yn_PART dawel_ADJ
Mae_AUX tylluanod_NOUN yn_PART ffrwtian_VERB awel_NOUN
a_CONJ chodi_VERB cryn_ADJ ddychryn_NOUN
fel_ADP adar_NOUN du_ADJ drycin_ADJ
cuddia_VERB 'r_DET wawr_NOUN tu_NOUN hwynt_PRON y_DET gorwel_NOUN
Priodas_PROPN
Roedd_VERB bachgen_NOUN yn_PART hoff_ADJ iawn_ADV o_ADP regi_VERB ,_PUNCT
ei_DET gymar_NOUN ymarfer_VERB or-_CONJ rechi_VERB ._PUNCT
Ar_ADP ôl_NOUN pob_DET ffrwydrad_NOUN
byddai_AUX 'n_PART bloeddio_VERB fel_ADP bardd_NOUN
a_CONJ naws_NOUN eu_DET cyfathrach_NOUN yn_PART rhewi_VERB
Bywyd_PROPN
Bywyd_VERB sy_VERB 'n_PART fyr_ADJ ,_PUNCT yn_PART fyrrach_ADJ na_CONJ fflach_NOUN
yn_PART oed_NOUN yr_DET addewid_NOUN dwi_AUX 'n_PART dal_VERB yn_PART iach_ADJ ._PUNCT
i_ADP raddau_NOUN arafu_VERB
ag_CONJ atgofion_VERB sy_AUX 'n_PART crafu_VERB
bob_DET hyn_PRON a_CONJ hyn_PRON dwi_AUX 'n_PART byw_VERB dan_ADP eu_DET llach_NOUN
Dwi_AUX 'n_PART byw_VERB fel_ADP breuddwyd_NOUN gwarchod_VERB
mewn_ADP byd_NOUN mwg_NOUN ,_PUNCT sgrech_NOUN a_CONJ phechod_NOUN ._PUNCT
Mae_AUX 'r_DET cymdogion_NOUN yn_PART cwyno_VERB ,_PUNCT
a_PART cheisio_VERB fy_DET ffrwyno_NOUN ._PUNCT
Ond_CONJ dwi_VERB ddim_PART yn_PART un_NUM am_ADP ufudd_ADJ -_PUNCT dod_VERB
Mae_VERB 'n_PART bwysig_ADJ cyn_CONJ meddwl_VERB priodi_VERB
cael_VERB hoe_NOUN fach_ADJ ,_PUNCT seibiant_NOUN ac_CONJ oedi_VERB ._PUNCT
Uno_VERB 'r_DET chwant_NOUN gyda_ADP chariad_NOUN
dyna_DET 'r_DET wir_ADJ fwriad_NOUN
bydda_VERB 'n_PART sicr_ADJ ,_PUNCT mewn_ADP lle_NOUN edifaru_VERB
Glaw_NOUN slei_ADJ
'_PUNCT Roedd_VERB hi_PRON 'n_PART arfer_VERB bwrw_NOUN glaw_NOUN
ei_DET cherydd_NOUN a_CONJ 'i_PRON chosbi_VERB ar_ADP y_DET naw_NUM ._PUNCT
Dagrau_VERB ffug_ADJ y_DET cwmwl_NOUN
oedd_VERB jest_ADV i_PART osgoi_VERB trwbwl_NOUN
a_CONJ dianc_VERB heb_ADP glatshen_NOUN o_ADP 'i_PRON llaw_NOUN ._PUNCT
Sarff_NOUN
yn_PART ddyddiau_NOUN deg_NUM pan_CONJ oedd_VERB Adda_PROPN yn_PART blentyn_NOUN
di-_NOUN nam_NOUN ,_PUNCT diniwed_ADJ ond_CONJ daeth_VERB y_DET neidr_NOUN wedyn_ADV ._PUNCT
cenau_NOUN ar_ADP ei_DET gefn_NOUN
lliwiau_VERB dallol_ADJ yn_PART drefn_NOUN
tywysydd_NOUN y_DET ffordd_NOUN dwyllodrus_ADJ ddiderfyn_ADJ
Dameg_NOUN mam_NOUN
dameg_NOUN mam_NOUN oedd_VERB cropian_VERB cyn_CONJ cerdded_VERB
mae_VERB angen_NOUN amser_NOUN ac_CONJ adeg_NOUN am_ADP aeddfed_ADJ ._PUNCT
paid_VERB meddwl_VERB am_ADP redeg_VERB
llai_ADJ byth_ADV am_ADP ehedeg_VERB
yn_ADP y_DET pen_NOUN draw_ADV i_ADP ti_PRON daw_VERB dy_DET deyrnged_NOUN
