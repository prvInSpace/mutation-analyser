"_PUNCT *_SYM *_SYM *_SYM Late_PROPN Change_PROPN S4C_PROPN -_PUNCT Newid_NOUN Hwyr_ADJ S4C_PROPN *_SYM *_SYM *_SYM -_PUNCT Wythnos_PROPN 41_NUM -_PUNCT Tuesday_PROPN 08_NUM /_SYM 10_NUM /_SYM 2019_NUM Dydd_NOUN Mawrth_PROPN "_PUNCT ,_PUNCT "_PUNCT *_SYM *_SYM Late_PROPN Change_PROPN S4C_PROPN -_PUNCT Newid_NOUN Hwyr_ADJ S4C_PROPN *_SYM *_SYM
Wythnos_VERB 41_NUM
08_NUM /_SYM 10_NUM /_SYM 2019_NUM Dydd_NOUN Mawrth_PROPN
22_NUM :_PUNCT 30_NUM Amser_NOUN Newydd_ADJ Ar_ADP y_DET Dibyn_PROPN
23_NUM :_PUNCT 00_NUM Amser_NOUN Newydd_ADJ Ralio_PROPN :_PUNCT Rali_NOUN Cymru_PROPN GB_PROPN
22_NUM :_PUNCT 30_NUM
AR_VERB Y_DET DIBYN_NOUN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT HD_X )_PUNCT
Yn_ADP y_DET gyfres_NOUN hon_PRON bydd_VERB deg_NUM anturiaethwr_NOUN yn_PART brwydro_VERB i_ADP ennill_VERB pecyn_NOUN antur_NOUN gwerth_ADJ deng_NUM mil_NUM o_ADP bunnoedd_NOUN a_CONJ theitl_NOUN Pencampwr_PROPN Ar_ADP y_DET Dibyn_PROPN ,_PUNCT Blwyddyn_NOUN Antur_PROPN Cymru_PROPN ._PUNCT
Yn_ADP y_DET bennod_NOUN hon_PRON bydd_VERB ffydd_NOUN yr_DET anturiaethwyr_NOUN yn_ADP Dilwyn_PROPN ac_CONJ yn_ADP ei_DET gilydd_NOUN yn_PART cael_VERB ei_PRON phrofi_VERB wrth_ADP iddyn_ADP nhw_PRON barhau_VERB gyda_ADP 'u_PRON hymgais_VERB i_ADP ennill_VERB y_DET gystadleuaeth_NOUN ._PUNCT
23_NUM :_PUNCT 00_NUM
RALIO_NUM :_PUNCT RALI_NOUN CYMRU_ADJ GB_PROPN :_PUNCT Uchafbwyntiau_NOUN Rali_NOUN Cymru_PROPN GB_PROPN (_PUNCT R_NUM )_PUNCT (_PUNCT S_NUM )_PUNCT (_PUNCT HD_X )_PUNCT
Byddwn_AUX ni_PRON 'n_PART edrych_VERB nôl_ADV ar_ADP uchafbwyntiau_NOUN Rali_NOUN Cymru_PROPN GB_PROPN 2019_NUM ._PUNCT
Byddwn_AUX hefyd_ADV yn_PART cael_VERB uchafbwyntiau_NOUN 'r_DET Rali_NOUN Genedlaethol_ADJ sy_AUX 'n_PART rhedeg_VERB wrth_ADP ymyl_NOUN y_DET brif_ADJ rali_NOUN ,_PUNCT ac_CONJ yn_PART sgwrsio_VERB gyda_ADP 'r_DET Cymry_PROPN sy_AUX 'n_PART cystadlu_VERB yn_ADP y_DET rali_NOUN honno_DET ._PUNCT
