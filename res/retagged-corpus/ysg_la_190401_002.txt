Helo_INTJ ̂_ADP ffrindiau_NOUN !_PUNCT
Mae_VERB hi_PRON 'n_PART fis_NOUN Ebrill_PROPN ._PUNCT
Beth_PRON sy_AUX 'n_PART digwydd_VERB yn_PART mis_NOUN Ebrill_PROPN ?_PUNCT
Gwyliau_NOUN 'r_DET Pasg_NOUN !_PUNCT
Wyt_AUX ti_PRON yn_PART hoffi_VERB wyau_NOUN Pasg_PROPN ?_PUNCT
Dw_AUX i_PRON 'n_PART hoffi_VERB wyau_NOUN Pasg_ADJ yn_PART fawr_ADJ !_PUNCT
Iym_VERB iym_ADP !_PUNCT
Mae_VERB yna_ADV rywun_NOUN arall_ADJ yn_PART hoffi_VERB wyau_NOUN Pasg_PROPN -_PUNCT darllen_VERB Gwesty_NOUN 'r_DET Ynys_PROPN i_PART weld_VERB pwy_PRON !_PUNCT
Beth_PRON arall_ADJ sydd_VERB yn_PART Ebrill_PROPN ?_PUNCT
Diwrnod_NOUN Ffw_NOUN ̂_ADJ l_DET Ebrill_PROPN ._PUNCT
Wyt_AUX ti_PRON yn_PART mynd_VERB i_PART chwarae_VERB triciau_NOUN ar_ADP y_DET teulu_NOUN a_CONJ ffrindiau_NOUN ?_PUNCT
Mae_AUX rhywun_NOUN yn_PART chwarae_VERB triciau_NOUN ar_ADP Cai_PROPN Clustiau_PROPN a_CONJ 'i_PRON ffrindiau_NOUN hefyd_ADV !_PUNCT
Mae_AUX plant_NOUN Ysgol_NOUN Gynradd_NOUN lleoliad_NOUN yn_ADP Abertawe_PROPN wedi_PART anfon_VERB llythyrau_NOUN ata_ADJ i_PRON -_PUNCT diolch_NOUN yn_PART fawr_ADJ iawn_ADV ._PUNCT
Mae_VERB eich_DET Cymraeg_PROPN yn_PART wych_ADJ !_PUNCT
Os_CONJ wyt_VERB ti_PRON eisiau_VERB gweld_VERB dy_DET hun_NOUN yn_ADP Bore_NOUN Da_ADJ ,_PUNCT galli_VERB anfon_VERB llythyr_NOUN ar_ADP e_PRON -_PUNCT bost_NOUN i_ADP boreda@urdd.org_NOUN ._PUNCT
Mi_PART fyddai_VERB llun_NOUN yn_PART gre_NOUN ̂_ADJ t_PRON hefyd_ADV ._PUNCT
Hwyl_NOUN am_ADP y_DET tro_NOUN !_PUNCT
Mistar_NOUN Urdd_PROPN
Helo_INTJ ̂_PUNCT !_PUNCT
enwb_VERB dw_VERB i_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART saith_NUM oed_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART byw_VERB yn_ADP Abertawe_PROPN ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV chwarae_VERB rygbi_NOUN ._PUNCT
Dw_AUX i_PRON ddim_PART yn_PART hoffi_ADV chwarae_VERB pe_CONJ ̂_SYM l_PRON -_PUNCT rwyd_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV gwisgo_VERB crys_NOUN -_PUNCT T_NUM glas_ADJ a_CONJ melyn_VERB a_CONJ ji_VERB ̂_PUNCT ns_PART llwyd_ADJ ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_VERB bwyta_VERB mefus_ADJ a_CONJ siocled_NOUN achos_ADP maen_VERB nhw_PRON 'n_PART flasus_ADJ ._PUNCT
Dw_AUX i_PRON ddim_PART yn_PART hoffi_ADV bwyta_VERB tomato_NOUN achos_CONJ mae_AUX 'n_PART ych_VERB a_CONJ fi_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV yfed_VERB sudd_NOUN afal_NOUN achos_CONJ mae_VERB 'n_PART iachus_ADJ ._PUNCT
Dw_AUX i_PRON ddim_PART yn_PART hoffi_ADV yfed_VERB sudd_NOUN oren_ADJ achos_CONJ mae_AUX 'n_PART ych_VERB a_CONJ fi_PRON ._PUNCT
Hwyl_INTJ !_PUNCT
enwb_VERB
Helo_INTJ ̂_PUNCT !_PUNCT
Shwmae_VERB !_PUNCT
enwg_VERB dw_VERB i_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART saith_NUM oed_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART byw_VERB yn_ADP Abertawe_PROPN mewn_ADP ty_NOUN ̂_ADJ semi_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_VERB bwyta_VERB selsig_ADJ a_CONJ sglodion_NOUN a_CONJ dw_AUX i_ADP ddim_PART yn_PART hoffi_VERB pys_NOUN a_CONJ moron_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV chwarae_VERB gemau_NOUN pe_CONJ ̂_SYM l_PRON -_PUNCT droed_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV gwisgo_VERB trowsus_NOUN a_CONJ chrys_NOUN -_PUNCT T_NUM ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV yfed_VERB sudd_NOUN afal_NOUN ._PUNCT
Da_ADJ bo_VERB !_PUNCT
enwg_VERB
Helo_INTJ ̂_PUNCT !_PUNCT
Shwmae_VERB !_PUNCT
enwb_VERB dw_VERB i_PRON ._PUNCT
Dw_AUX i_PRON 'n_PART saith_NUM oed_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART byw_VERB yn_ADP Pennard_PROPN ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV bwyta_VERB moron_NOUN ,_PUNCT selsig_ADJ a_CONJ siocled_NOUN ._PUNCT
Dw_AUX i_PRON ddim_PART yn_PART hoffi_ADV bwyta_VERB pysgod_NOUN a_CONJ chig_NOUN ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV chwarae_VERB tag_NOUN a_CONJ thenis_NOUN ._PUNCT
Dw_AUX i_PRON ddim_PART yn_PART hoffi_ADV chwarae_VERB pe_CONJ ̂_SYM l_PRON -_PUNCT droed_NOUN a_CONJ rygbi_NOUN ._PUNCT
Hwyl_INTJ !_PUNCT
enwb_VERB
Dillad_NOUN !_PUNCT
Sut_ADV un_NUM yw_VERB dy_DET wisg_NOUN ysgol_NOUN ?_PUNCT
Lliwia_VERB 'r_DET isod_NOUN yn_PART lliwiau_VERB 'r_DET ysgol_NOUN ._PUNCT
Yna_ADV ysgrifenna_VERB 'r_DET enwau_NOUN cywir_ADJ ar_ADP y_DET labeli_NOUN ._PUNCT
Siwmper_NOUN
Tei_NOUN
Crys_PROPN
Trowsus_PROPN
Esgidiau_PROPN
Sgert_PROPN
Sannau_PROPN
Gorffen_VERB y_DET brawddegau_NOUN isod_ADV ._PUNCT
Dw_AUX i_PRON yn_PART mynd_VERB i_ADP Ysgol_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN
Lliwiau_VERB gwisg_NOUN Ysgol_NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN yw_VERB __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN
Dw_VERB i_PRON __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN gwisgo_VERB gwisg_NOUN ysgol_NOUN ._PUNCT
Disgrifia_VERB dy_DET ddillad_NOUN bob_DET dydd_NOUN (_PUNCT casual_NOUN /_PUNCT everyday_NOUN clothes_NOUN )_PUNCT ._PUNCT
Dw_AUX i_PRON 'n_PART hoffi_ADV gwisgo_VERB (_PUNCT dilledyn_NOUN )_PUNCT __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN (_PUNCT lliw_NOUN )_PUNCT __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Dw_AUX i_PRON hefyd_ADV yn_PART hoffi_ADV gwisgo_VERB __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ._PUNCT
Tynna_VERB lun_NOUN o_ADP beth_PRON fyddi_AUX di_PRON yn_PART ei_PRON wneud_VERB dros_ADP y_DET Pasg_NOUN a_CONJ gorffen_VERB y_DET frawddeg_NOUN ._PUNCT ._PUNCT ._PUNCT
__NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN ydw_VERB i_PRON ._PUNCT
Dyma_ADV fi_PRON yn_PART __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN __NOUN (_PUNCT disgrifia_VERB beth_PRON wyt_AUX ti_PRON yn_PART ei_PRON wneud_VERB yn_ADP y_DET llun_NOUN )_PUNCT ._PUNCT
mwynhau_VERB hefo_ADP 'r_DET teulu_NOUN
Y_DET Pasg_NOUN !_PUNCT
Beth_PRON wyt_AUX ti_PRON yn_PART wneud_VERB dros_ADP y_DET Pasg_NOUN ?_PUNCT
Mae_AUX Mistar_NOUN Urdd_PROPN yn_PART hoffi_ADV bwyta_VERB wyau_NOUN Pasg_ADJ !_PUNCT
Labela_VERB 'r_DET lliwiau_NOUN yn_ADP y_DET llun_NOUN yma_ADV
Cynnig_NOUN arbennig_ADJ i_ADP aelodau_NOUN 'r_DET Urdd_PROPN
Defnyddia_VERB dy_DET gerdyn_NOUN aelodaeth_NOUN i_ADP fynd_VERB i_ADP Folly_PROPN Farm_PROPN !_PUNCT
Os_CONJ wyt_VERB ti_PRON 'n_PART aelod_NOUN o_ADP 'r_DET Urdd_NOUN galli_ADV gael_VERB mynediad_NOUN am_ADP ddim_PRON os_CONJ wyt_AUX ti_PRON 'n_PART mynd_VERB yno_ADV gyda_ADP dau_NUM oedolyn_NOUN sy_AUX 'n_PART talu_VERB 'n_PART llawn_ADJ ._PUNCT
Y_DET cwbl_NOUN sydd_VERB angen_NOUN i_ADP ti_PRON ei_PRON wneud_VERB yw_VERB dangos_VERB dy_DET gerdyn_NOUN aelodaeth_NOUN Urdd_PROPN wrth_ADP y_DET fynedfa_NOUN !_PUNCT
*_SYM Nid_PROPN i_ADP 'w_PRON ddefnyddio_VERB gyda_ADP chynnig_NOUN arbennig_ADJ arall_ADV ._PUNCT
Mae_AUX 'r_DET cynnig_NOUN ar_ADP gael_VERB hyd_NOUN at_ADP y_DET 30_NUM Ebrill_PROPN ._PUNCT
Am_ADP amserau_NOUN agor_VERB ,_PUNCT ewch_VERB i_ADP :_PUNCT cyfeiriad_NOUN
Amser_NOUN Lliwio_PROPN !_PUNCT
Wyt_AUX ti_PRON wedi_PART cael_VERB llawer_NOUN o_ADP wyau_NOUN Pasg_ADJ eto_ADV ?_PUNCT
Mae_AUX Mistar_NOUN Urdd_PROPN wedi_PART cael_VERB llawer_NOUN ._PUNCT
Ond_CONJ '_PUNCT dyw_VERB Mistar_NOUN Urdd_NOUN ddim_PART yn_PART gallu_ADV aros_VERB tan_ADP y_DET Pasg_NOUN ._PUNCT
Mae_AUX 'n_PART eu_DET bwyta_VERB nawr_ADV !_PUNCT
Mistar_NOUN Urdd_NOUN drwg_ADJ !_PUNCT
Beth_PRON am_ADP liwio_VERB 'r_DET llun_NOUN ?_PUNCT
Defnyddia_VERB lawer_PRON o_ADP liwiau_NOUN gwahanol_ADJ -_PUNCT paid_ADV anghofio_VERB coch_ADJ ,_PUNCT gwyn_ADJ a_CONJ gwyrdd_ADJ !_PUNCT
Gwesty_NOUN 'r_DET Ynys_PROPN
Mae_VERB Owain_PROPN ,_PUNCT Beth_PRON a_CONJ Sioned_PROPN yn_PART byw_VERB mewn_ADP gwesty_NOUN ._PUNCT
Maen_AUX nhw_PRON 'n_PART byw_VERB mewn_ADP gwesty_NOUN gyda_ADP Dad_PROPN a_CONJ Gel_NOUN y_DET ci_NOUN ._PUNCT
Gwesty_NOUN 'r_DET Ynys_PROPN ydi_VERB enw_NOUN 'r_DET gwesty_NOUN ,_PUNCT achos_CONJ mae_VERB 'r_DET gwesty_NOUN ar_ADP Ynys_PROPN Cenllib_PROPN ._PUNCT
'_PUNCT Dw_AUX i_PRON wedi_PART dweud_VERB wrth_ADP pawb_PRON am_PART beidio_ADV prynu_VERB wyau_NOUN Pasg_ADJ i_ADP chi_PRON eleni_ADV ,_PUNCT '_PUNCT meddai_VERB Dad_PROPN ._PUNCT
'_PUNCT Beeeeeeeeeeeeth_PROPN ?_PUNCT !_PUNCT '_PUNCT meddai_VERB Owain_PROPN ,_PUNCT Beth_PRON a_CONJ Sioned_PROPN ._PUNCT
'_PUNCT Wel_INTJ ,_PUNCT '_PUNCT meddai_VERB Dad_PROPN ,_PUNCT '_PUNCT roedd_VERB 16_NUM ŵy_NOUN Pasg_NOUN yma_DET y_DET llynedd_NOUN -_PUNCT llawer_ADV gormod_ADV ,_PUNCT ac_CONJ mae_AUX gormod_PRON o_ADP siocled_NOUN yn_PART ddrwg_ADJ ichi_ADP ._PUNCT
Felly_CONJ dw_AUX i_ADP wedi_PART prynu_VERB un_NUM ŵy_NOUN anferth_ADJ inni_VERB ei_DET rannu_VERB ar_ADP o_PRON ̂_SYM l_PRON bod_VERB yn_ADP y_DET capel_NOUN ar_ADP fore_NOUN dydd_NOUN Sul_PROPN y_DET Pasg_NOUN ._PUNCT '_PUNCT
'_PUNCT Waw_PROPN !_PUNCT '_PUNCT meddai_VERB pawb_PRON ._PUNCT
'_PUNCT Mae_VERB hwnna_PRON mor_ADV fawr_ADJ a_CONJ ̂_NUM balŵn_NOUN !_PUNCT '_PUNCT meddai_VERB Owain_PROPN ._PUNCT
Dydi_AUX Owain_PROPN ddim_PART eisiau_ADV aros_VERB tan_ADP ddydd_NOUN Sul_PROPN y_DET Pasg_NOUN i_ADP fwyta_VERB 'r_DET ŵy_NOUN Pasg_PROPN ._PUNCT
Mae_AUX Owain_PROPN wedi_PART cael_VERB syniad_NOUN ._PUNCT
Mae_VERB Owain_PROPN yn_PART '_PUNCT benthyg_ADJ '_PUNCT yr_DET ŵy_NOUN Pasg_PROPN ac_CONJ yn_PART mynd_VERB i_ADP 'w_PRON lofft_NOUN ._PUNCT
Mae_VERB balŵn_NOUN anferth_ADJ gan_ADP Owain_PROPN yn_ADP y_DET llofft_NOUN ._PUNCT
'_PUNCT Perffaith_X !_PUNCT '_PUNCT meddai_VERB Owain_PROPN ._PUNCT
'_PUNCT Mae_VERB 'r_DET balŵn_NOUN yma_DET yr_DET un_NUM maint_NOUN a_CONJ ̂_VERB 'r_DET ŵy_NOUN Pasg_PROPN ._PUNCT '_PUNCT
Yna_ADV ,_PUNCT mae_AUX Owain_PROPN yn_PART agor_VERB y_DET bocs_NOUN sy_AUX 'n_PART dal_VERB yr_DET ŵy_NOUN Pasg_PROPN ac_CONJ yn_PART tynnu_VERB 'r_DET ŵy_NOUN allan_ADV yn_PART ofalus_ADJ ._PUNCT
Yna_ADV mae_AUX 'n_PART tynnu_VERB 'r_DET papur_NOUN sgleiniog_ADJ yn_PART ofalus_ADJ ,_PUNCT ofalus_ADJ ,_PUNCT oddi_ADP ar_ADP yr_DET ŵy_NOUN Pasg_PROPN ac_CONJ yn_PART lapio_VERB 'r_DET balŵn_NOUN gyda_ADP 'r_DET papur_NOUN sgleiniog_ADJ ._PUNCT
Yna_ADV mae_AUX 'n_PART rhoi_VERB 'r_DET balŵn_NOUN yn_PART o_ADJ ̂_SYM l_PRON yn_ADP y_DET bocs_NOUN ._PUNCT
'_PUNCT Perffaith_X !_PUNCT '_PUNCT meddai_VERB Owain_PROPN eto_ADV ._PUNCT
'_PUNCT Mae_AUX 'r_DET balŵn_NOUN yn_PART edrych_VERB fel_ADP ŵy_NOUN Pasg_NOUN nawr_ADV !_PUNCT '_PUNCT
Mae_AUX Owain_PROPN yn_PART cuddio_VERB 'r_DET ŵy_NOUN Pasg_NOUN mewn_ADP bocs_NOUN o_ADP dan_ADP y_DET gwely_NOUN ac_CONJ yn_PART bwyta_VERB ychydig_ADV bob_DET nos_NOUN
Nos_NOUN Sadwrn_PROPN y_DET Pasg_NOUN mae_AUX 'n_PART edrych_VERB yn_ADP y_DET bocs_NOUN o_ADP dan_ADP y_DET gwely_NOUN ._PUNCT
'_PUNCT O_ADP na_ADV !_PUNCT
Dw_AUX i_PRON wedi_PART bwyta_VERB 'r_DET ŵy_NOUN Pasg_NOUN i_ADP gyd_ADP !_PUNCT '_PUNCT meddai_VERB Owain_PROPN ._PUNCT
Mae_VERB Owain_PROPN yn_PART drist_ADJ ei_PRON fod_AUX wedi_PART gorffen_VERB yr_DET ŵy_NOUN Pasg_PROPN ._PUNCT
Mae_AUX Owain_PROPN yn_PART teimlo_VERB 'n_PART euog_ADJ achos_CONJ does_VERB dim_DET ŵy_NOUN Pasg_NOUN ar_ADP o_PRON ̂_SYM l_ADP i_ADP Sioned_PROPN a_CONJ Beth_PRON ._PUNCT
Bore_NOUN dydd_NOUN Sul_PROPN ,_PUNCT maen_AUX nhw_PRON 'n_PART mynd_VERB i_ADP 'r_DET capel_NOUN ._PUNCT
'_PUNCT Gwisgwch_VERB eich_DET dillad_NOUN gorau_ADJ !_PUNCT '_PUNCT meddai_VERB Dad_PROPN ._PUNCT
'_PUNCT Rydyn_AUX ni_PRON yn_PART gwisgo_VERB ein_DET dillad_NOUN gorau_ADJ !_PUNCT '_PUNCT meddai_VERB Sioned_PROPN ._PUNCT
Mae_AUX Sioned_PROPN yn_PART gwisgo_VERB trowsus_NOUN gwyn_ADJ a_CONJ thop_NOUN pinc_NOUN ._PUNCT
Mae_AUX Beth_PRON yn_PART gwisgo_VERB sgert_NOUN hir_ADJ las_NOUN golau_NOUN a_CONJ blowsen_NOUN wen_NOUN ._PUNCT
Mae_AUX Owain_PROPN yn_PART cael_VERB trafferth_NOUN cau_VERB ei_DET drowsus_NOUN -_PUNCT mae_AUX ei_DET fol_NOUN wedi_PART mynd_VERB fel_ADP balŵn_NOUN achos_CONJ ei_PRON fod_AUX wedi_PART bwyta_VERB 'r_DET siocled_NOUN !_PUNCT
Ar_ADP o_PRON ̂_ADV l_PART dod_VERB adre_ADV o_ADP 'r_DET capel_NOUN mae_VERB Dad_PROPN yn_PART no_NOUN ̂_ADJ l_ADP yr_DET ŵy_NOUN Pasg_NOUN o_ADP dop_NOUN y_DET dreser_NOUN ._PUNCT
Mae_VERB Sioned_PROPN a_CONJ Beth_PRON yn_PART agor_VERB yr_DET ŵy_NOUN Pasg_PROPN ._PUNCT
'_PUNCT O_ADP na_ADV !_PUNCT
Dad_VERB !_PUNCT
Beth_PRON yw_VERB hyn_PRON ?_PUNCT
Jo_CONJ ̂_ADV c_PRON ?_PUNCT '_PUNCT meddai_VERB Sioned_PROPN ._PUNCT
'_PUNCT Jo_PROPN ̂_ADV c_CONJ wael_ADJ iawn_ADV !_PUNCT '_PUNCT meddai_VERB Beth_PRON ._PUNCT
'_PUNCT Does_VERB gen_ADP i_PRON ddim_PART syniad_NOUN ble_ADV mae_AUX 'r_DET ŵy_NOUN siocled_NOUN wedi_PART mynd_VERB !_PUNCT '_PUNCT meddai_VERB Dad_PROPN ._PUNCT
'_PUNCT Owain_X ?_PUNCT !_PUNCT '_PUNCT meddai_VERB pawb_PRON ._PUNCT
Mae_AUX Owain_PROPN yn_PART cochi_VERB !_PUNCT
Mae_AUX Owain_PROPN yn_PART teimlo_VERB cywilydd_NOUN ei_PRON fod_AUX wedi_PART bwyta_VERB 'r_DET ŵy_NOUN Pasg_NOUN i_ADP gyd_ADP ._PUNCT
'_PUNCT Sori_PROPN ,_PUNCT '_PUNCT meddai_VERB Owain_PROPN yn_PART drist_ADJ ._PUNCT
'_PUNCT Bolgi_NOUN !_PUNCT '_PUNCT meddai_VERB Sioned_PROPN ._PUNCT
'_PUNCT Helo_INTJ ̂_ADP !_PUNCT '_PUNCT meddai_VERB Taid_NOUN a_CONJ Nain_PROPN '_PUNCT Dyma_DET wyau_NOUN Pasg_ADJ i_ADP chi_PRON !_PUNCT "_PUNCT
Mae_AUX Owain_PROPN yn_PART rhoi_VERB ei_DET ŵy_NOUN Pasg_NOUN o_ADP i_ADP Sioned_PROPN a_CONJ Beth_PRON ._PUNCT
'_PUNCT Sori_PROPN ferched_VERB ._PUNCT
Dw_AUX i_PRON byth_ADV yn_PART mynd_VERB i_PART wneud_VERB rhywbeth_NOUN fel_ADP yna_ADV eto_ADV ,_PUNCT '_PUNCT meddai_VERB Owain_PROPN ._PUNCT
'_PUNCT Wel_INTJ ,_PUNCT gobeithio_VERB dy_PRON fod_AUX wedi_PART dysgu_VERB dy_DET wers_NOUN !_PUNCT '_PUNCT meddai_VERB Dad_PROPN ._PUNCT
'_PUNCT Pasg_NOUN Hapus_ADJ i_ADP bawb_PRON !_PUNCT '_PUNCT
Rwyt_AUX ti_PRON 'n_PART gallu_ADV darllen_VERB y_DET stori_NOUN yn_ADP y_DET lluniau_NOUN hefyd_ADV !_PUNCT
CACEN_NOUN FORON_PROPN Y_DET PASG_NOUN
Dyma_DET gacen_NOUN hawdd_ADJ a_CONJ blasus_ADJ ._PUNCT
Mae_VERB hefyd_ADV yn_PART siawns_ADJ i_ADP gael_VERB hwyl_NOUN !_PUNCT
Defnyddia_VERB dy_DET ddychymyg_NOUN !_PUNCT
Pasg_NOUN hapus_ADJ i_ADP ti_PRON !_PUNCT
Cynhwysion_NOUN !_PUNCT
•_NUM 4_NUM wy_NOUN
•_NUM 310_NUM olew_NOUN llysiau_NOUN
•_NOUN 1_NUM llwy_NOUN de_NOUN o_ADP fanilla_VERB
•_NUM 330_NUM g_NOUN siwgr_NOUN ma_PRON ̂_SYM n_ADJ
•_NOUN 300_NUM g_NOUN o_ADP flawd_NOUN codi_NOUN
•_NOUN 2_NUM llwy_NOUN de_NOUN bicaronad_NOUN o_ADP soda_NOUN
•_NUM 100_NUM g_ADP cnau_NOUN amrywiol_ADJ wedi_PART eu_DET torri_VERB 'n_PART fan_NOUN
•_NUM 3_NUM moryn_NOUN wedi_PART eu_DET gratio_VERB
Eisin_PROPN
•_NUM 250_NUM g_NOUN o_ADP fenyn_NOUN meddal_ADJ heb_ADP halen_NOUN
•_NUM 250_NUM g_NOUN o_ADP gaws_NOUN hufennog_ADJ meddal_ADJ
•_NOUN 500_NUM g_NOUN o_ADP siwgr_NOUN eisin_NOUN wedi_PART ei_DET hidlo_VERB
•_NUM Croen_NOUN 2_NUM lemwn_NOUN wedi_PART gratio_VERB 'n_PART fa_VERB ̂_ADJ n_ADJ
•_NOUN 2_NUM lond_NOUN llwy_NOUN de_NOUN o_ADP sudd_NOUN lemwn_NOUN
Camau_NOUN …_PUNCT
1_NUM ._PUNCT
Cynhesu_VERB 'r_DET popty_NOUN i_ADP 180_NUM °_SYM C_NUM ._PUNCT
2_NUM ._PUNCT
Iro_NOUN a_CONJ leinio_VERB tun_NOUN cacen_NOUN eitha'_ADJUNCT dwfn_ADJ ._PUNCT
3_NUM ._PUNCT
Chwipio_VERB 'r_DET wyau_NOUN ,_PUNCT olew_NOUN a_CONJ fanila_VERB mewn_ADP powlen_NOUN ._PUNCT
4_NUM ._PUNCT
Hidlo_VERB 'r_DET siwgr_NOUN ,_PUNCT blawd_NOUN ,_PUNCT bicarbonad_NOUN a_CONJ phinsiad_NOUN o_ADP halen_NOUN mewn_ADP powlen_NOUN ._PUNCT
5_NUM ._PUNCT
Cymysgu_VERB cynnwys_NOUN y_DET powlenni_NOUN ac_CONJ ychwanegu_VERB 'r_DET moron_NOUN a_CONJ chnau_NOUN ._PUNCT
6_NUM ._PUNCT
Cymysgu_VERB 'r_DET cyfan_NOUN yn_PART dda_ADJ a_CONJ llenwi_VERB 'r_DET tun_NOUN ._PUNCT
7_NUM ._PUNCT
Rhoi_VERB yn_ADP y_DET popty_NOUN ._PUNCT
8_NUM ._PUNCT
Wedi_ADP 45_NUM munud_NOUN ,_PUNCT profi_VERB hefo_ADP sgiwer_NOUN ._PUNCT
Os_CONJ yw_AUX 'r_DET sgiwer_NOUN yn_PART dod_VERB allan_ADV yn_PART la_PRON ̂_SYM n_NUM ,_PUNCT mae_VERB 'n_PART barod_ADJ ._PUNCT
Os_CONJ nad_PART ,_PUNCT rhowch_VERB yn_PART o_PRON ̂_SYM l_PRON yn_ADP y_DET popty_NOUN nes_CONJ ei_PRON fod_AUX wedi_PART coginio_VERB ._PUNCT
9_NUM ._PUNCT
Wedi_PART coginio_VERB ,_PUNCT gadael_VERB i_ADP oeri_VERB ar_ADP rac_NOUN weiren_NOUN ._PUNCT
EISIN_PROPN
Chwipio_VERB 'r_DET menyn_NOUN a_CONJ chaws_NOUN hufennog_ADJ ._PUNCT
Ychwanegu_VERB 'r_DET siwgr_NOUN eisin_VERB bob_DET yn_PART llwyaid_ADJ ,_PUNCT tra_CONJ 'n_PART chwipio_VERB ._PUNCT
Ychwanegu_VERB 'r_DET sudd_NOUN a_CONJ chroen_NOUN lemwn_NOUN a_CONJ chwipio_NOUN nes_CONJ yn_PART ysgafn_ADJ ._PUNCT
GORFFEN_PROPN
Wedi_CONJ i_ADP 'r_DET gacen_NOUN oeri_VERB ,_PUNCT sleisio_VERB 'n_PART ddwy_ADJ ._PUNCT
Gorchuddio_VERB wyneb_NOUN un_NUM gyda_ADP haen_NOUN o_ADP eisin_NOUN a_CONJ gosod_VERB y_DET darn_NOUN arall_ADJ ar_ADP ei_DET phen_NOUN ._PUNCT
Gorchuddio_VERB 'r_DET cyfan_NOUN hefo_ADP gweddill_NOUN yr_DET eisin_NOUN ._PUNCT
ADDURNO_PROPN
Malu_VERB bisgedi_VERB siocled_NOUN neu_CONJ oreos_NOUN i_PART edrych_VERB fel_ADP pridd_NOUN a_CONJ gosod_VERB ar_ADP dop_NOUN y_DET gacen_NOUN ._PUNCT Siapio_PROPN a_CONJ pheintio_VERB marsipa_ADJ ́_NUM n_ADV i_PART edrych_VERB fel_ADP moron_NOUN (_PUNCT bydd_VERB angen_NOUN lliw_NOUN gwyrdd_ADJ a_CONJ chymysgu_VERB lliw_NOUN melyn_ADJ a_CONJ choch_VERB ar_ADP gyfer_NOUN eisin_NOUN oren_ADJ )_PUNCT ._PUNCT
Defnyddia_VERB dy_DET ddychymyg_NOUN !_PUNCT
Galli_VERB ychwanegu_VERB cwningen_NOUN Pasg_NOUN siocled_NOUN ,_PUNCT wyau_NOUN Pasg_NOUN bach_ADJ ,_PUNCT cywion_VERB bach_ADJ ac_CONJ yn_ADP y_DET blaen_NOUN ._PUNCT
Dy_DET gacen_NOUN di_PRON -_PUNCT dy_DET ddychymyg_NOUN di_PRON !_PUNCT
LLYFR_VERB Y_DET MIS_NOUN !_PUNCT
Ailgylchu_VERB gyda_ADP Cyw_NOUN Addas_PROPN ._PUNCT
Anni_NOUN Lly_PROPN ̂_SYM n_ADJ
Y_DET Lolfa_NOUN
£_SYM 3.95_NUM
Llyfrau_NOUN perffaith_NOUN i_ADP blant_NOUN bach_ADJ i_ADP ddysgu_VERB geirfa_NOUN sy_VERB 'n_PART gysylltiedig_ADJ a_CONJ ̂_VERB 'r_DET cymeriadau_NOUN poblogaidd_ADJ sydd_VERB ar_ADP S4C_PROPN ac_CONJ ailadrodd_VERB patrymau_NOUN iaith_NOUN syml_ADJ ._PUNCT
Mae_AUX 'r_DET llyfr_NOUN hwn_DET yn_PART dysgu_VERB enwau_NOUN lliwiau_NOUN 'r_DET biniau_NOUN penodol_ADJ i_ADP roi_VERB gwahanol_ADJ ddeunyddiau_VERB -_PUNCT papur_NOUN ,_PUNCT tuniau_NOUN ,_PUNCT gwydr_NOUN ,_PUNCT plastig_ADJ ._PUNCT ._PUNCT ._PUNCT
