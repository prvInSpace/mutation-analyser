Ti_PRON am_ADP fynd_VERB allan_ADV i_PART chwarae_VERB efo_ADP enwg_VERB '_PUNCT te_NOUN boi_NOUN ?_PUNCT
Yndw_VERB tad_NOUN mam_NOUN ._PUNCT
Wyt_VERB boi_NOUN ?_PUNCT
Yndw_INTJ ._PUNCT
Ti_PRON am_ADP fynd_VERB a_CONJ ffwtbal_NOUN ch_ADJ 'di_PART '_PUNCT ta_CONJ di_PRON '_PUNCT o_PRON 'n_PART mynd_VERB a_CONJ un_NUM fo_PRON ?_PUNCT
nai_NOUN fynd_VERB a_CONJ un_NUM fi_PRON ia_PRON ?_PUNCT
Ia_ADJ cofia_VERB peidio_ADV mynd_VERB yn_PART bellach_ADJ 'na_ADV 'r_DET bumps_NOUN oce_NOUN enwg_VERB ?_PUNCT
Oce_INTJ ._PUNCT
Bydd_VERB enwg_VERB a_CONJ dad_NOUN adre_ADV 'n_PART munud_NOUN ._PUNCT
Dw_VERB 'm_PRON yn_PART siŵr_ADJ iawn_ADV os_CONJ 'di_PART dy_DET dad_NOUN yn_PART mynd_VERB i_ADP nol_NOUN enwg_VERB a_CONJ 'i_PRON beidio_VERB heno_ADV ._PUNCT
Oce_VERB mam_NOUN ._PUNCT
Iawn_ADV boi_NOUN nai_PART ffonio_VERB fo_PRON 'n_PART munud_NOUN ._PUNCT
Diolch_VERB am_ADP y_DET bocs_NOUN bwyd_NOUN neis_ADJ ._PUNCT
Croeso_INTJ ._PUNCT
Teimlo_VERB 'n_PART ofandwy_ADJ bobo_VERB fi_PRON 'm_PART 'di_PART neud_VERB bech'dan_ADP i_ADP ch_PRON 'di_PART yn_ADP y_DET dy_DET focs_NOUN bwyd_NOUN heddiw_ADV ._PUNCT
[_PUNCT chwerthin_VERB ]_PUNCT ._PUNCT
A_PART mae_VERB gen_ADP ti_PRON fara_NOUN ?_PUNCT
O_ADP mae_VERB gennai_VERB fara_NOUN rwan_ADV ._PUNCT
Doedd_VERB 'na_ADV 'm_DET digon_ADJ 'ma_ADV bora_VERB ma_PRON nagoedd_NOUN ._PUNCT
Na_INTJ ._PUNCT
Ond_CONJ ges_VERB ti_PRON dy_DET lenwi_VERB efo_ADP bob_DET dim_PRON arall_ADJ ._PUNCT
O_ADP 'n_PART ni_PRON 'di_PART rhoi_VERB digon_PRON o_ADP ffrwytha_NOUN a_CONJ ballu_VERB i_ADP ch_PRON 'di_PART n'don_NOUN ?_PUNCT
Do_VERB do_PRON do_ADP tad_NOUN ._PUNCT
Ti_PRON 'di_PART yfad_ADV digo_VERB heddiw_ADV ?_PUNCT
Do_INTJ ?_PUNCT
Do_INTJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Nesh_ADV i_PART brynnnu_VERB '_PUNCT heina_VERB heddiw_ADV yn_PART sefydliad_NOUN ._PUNCT
'_PUNCT Wbath_PROPN newydd_ADJ 'di_PART heina_VERB cup_NOUN corns_NOUN dw_AUX 'm_DET 'di_PART gweld_VERB nhw_PRON o_ADP 'r_DET blaen_NOUN ._PUNCT
Ti_PRON ?_PUNCT
Na_INTJ ._PUNCT
Dio_NOUN 'm_DET fatha_NOUN cornet_NOUN hir_ADJ rei_NOUN bach_ADJ ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT ._PUNCT
A_CONJ nes_CONJ i_PART brynnu_VERB hufen_NOUN ia_PRON newydd_ADJ ._PUNCT
yes_VERB ._PUNCT
Siocled_PROPN ?_PUNCT
na_PART un_NUM fanila_NOUN ._PUNCT
Ww_ADP un_NUM arall_ADJ fanila_NOUN ._PUNCT
Ie_INTJ nes_CONJ i_ADP 'm_DET meddwl_VERB bebe_NOUN sa_DET chi_PRON 'di_PART licio_VERB ._PUNCT
Siocled_PROPN ._PUNCT
Oh_INTJ nes_CONJ i_ADP 'm_PRON gweld_VERB -_PUNCT nes_CONJ i_ADP 'm_DET meddwl_VERB '_PUNCT sti_NOUN o_ADP 'n_PART ni_PRON jyst_ADV 'di_PART meddwl_VERB '_PUNCT sa_AUX chi_PRON 'n_PART licio_VERB fanila_NOUN ._PUNCT
Oh_INTJ rhaid_VERB i_ADP mam_NOUN sbio_VERB tro_NOUN nesa_ADJ [_PUNCT -_PUNCT ]_PUNCT ._PUNCT
Dw_VERB isho_NOUN gwahanol_ADJ bob_DET diwrnod_NOUN ._PUNCT
Oh_INTJ nes_CONJ i_ADP 'm_DET meddwl_VERB am_ADP hynna_PRON ._PUNCT
Weithia_VERB fedri_VERB 'di_PART gael_VERB yr_DET un_NUM efo_ADP tri_NUM fedri_VERB  _SPACE ._PUNCT
O_ADP hwnna_PRON dw_AUX i_PRON 'n_PART hoffi_VERB ._PUNCT
Ia_INTJ 'di_PART nhw_PRON 'm_DET yn_PART neud_VERB hwnna_PRON 'n_PART sefydliad_NOUN lleoliad_NOUN dw_AUX 'm_DET yn_PART meddwl_VERB de_NOUN ._PUNCT
Nai_PART sbio_VERB 'n_PART iawn_ADJ tro_NOUN nesa_ADJ sori_VERB boi_NOUN ._PUNCT
Lle_ADV ges_VERB ti_PRON 'r_DET llall_NOUN '_PUNCT ta_X ?_PUNCT
yn_PART sefydliad_NOUN ges_VERB i_ADP 'r_DET llall_PRON ._PUNCT
O_ADP 'n_PART ni_PRON 'n_PART meddwl_VERB sefydliad_NOUN ._PUNCT
Na_INTJ ._PUNCT
Fan'na_ADV ges_VERB ti_PRON cereal_NOUN favourite_NOUN fi_PRON de_NOUN ?_PUNCT
Ia_ADJ o_PRON na_CONJ ma_PRON ._PUNCT
'_PUNCT Da_ADJ ni_PRON 'n_PART mynd_VERB i_ADP fan_NOUN 'na_ADV 'n_PART aml_ADJ ._PUNCT
i_PART love_VERB you_NOUN mam_NOUN ._PUNCT
i_PART love_VERB you_X too_ADJ washi_NOUN bach_ADJ ._PUNCT
[_PUNCT aneglur_ADJ ?_PUNCT ]_PUNCT i_PART love_VERB you_X to_X the_X world_NOUN and_X back_ADJ ._PUNCT
Ôô_NOUN ._PUNCT
even_VERB if_NOUN i_ADP would_VERB go_ADV away_NOUN i_ADP would_VERB still_PRON love_ADJ you_NOUN ._PUNCT
O_ADP ti_PRON 'n_PART deud_VERB ryw_NOUN betha_VERB neis_ADJ wyt_VERB nghariad_NOUN i_PRON ._PUNCT
ymm_CONJ be_PRON o_PRON 'n_PART ni_PRON 'n_PART mynd_VERB i_PART ddeud_VERB ie_INTJ dw_AUX i_PRON 'di_PART cael_VERB trît_NOUN bach_ADJ iddoch_ADP chi_PRON heno_ADV i_ADP  _SPACE peldroed_NOUN '_PUNCT dyn_NOUN os_CONJ '_PUNCT da_ADJ chi_PRON 'n_PART blant_NOUN da_ADJ dw_AUX i_PRON 'n_PART meddwl_VERB neith_VERB enwg_VERB aros_VERB i_ADP fyny_ADV iddo_ADP fo_PRON ._PUNCT
Be_AUX ti_PRON 'n_PART feddwl_VERB ?_PUNCT
Be_AUX ti_PRON 'n_PART feddwl_AUX ti_PRON 'n_PART meddwl_VERB neith_ADV aros_VERB i_ADP fo_PRON ?_PUNCT
Ie_INTJ dw_AUX 'm_DET yn_PART meddwl_VERB fydd_AUX o_PRON 'di_PART blino_VERB ar_ADP ôl_NOUN ti_PRON '_PUNCT betha_VERB mae_AUX 'n_PART rhaid_VERB ._PUNCT
Bydd_AUX swn_AUX ni_PRON 'n_PART meddwl_VERB '_PUNCT fyd_NOUN ._PUNCT
Ti'o_VERB be_PRON oedd_AUX enwb_NOUN 'di_PART ffeindio_VERB heddiw_ADV ?_PUNCT
Be_INTJ ?_PUNCT
Esgid_VERB fo_PRON ._PUNCT
[_PUNCT ebychu_VERB ]_PUNCT Ar_ADP ôl_NOUN ch_PRON 'di_PART brynnu_VERB rei_NOUN newydd_ADJ ._PUNCT
A_PART dw_AUX i_PRON 'di_PART taflu_VERB 'r_DET llall_PRON achos_ADP mond_CONJ un_NUM o_ADP '_PUNCT 'na_ADV 'n_PART de_NOUN achos_ADP o'dd_NOUN o_PRON 'di_PART colli_VERB fo_PRON '_PUNCT dyn_NOUN nes_CONJ i_PART brynnu_VERB rei_NOUN newydd_ADJ a_CONJ wedyn_ADV mae_VERB 'di_PART ffeindio_VERB nhw_PRON heddiw_ADV ._PUNCT
O_ADP dim_DET ots_NOUN ._PUNCT
O_ADP dim_DET ots_NOUN ._PUNCT
O'dd_NOUN o_ADP angan_VERB rai_DET newydd_ADV i_PART weud_VERB gwir_NOUN ._PUNCT
Wast_NOUN o_ADP -_PUNCT wast_NOUN o_ADP bres_VERB hefyd_ADV de_NOUN ._PUNCT
Punt_NOUN naw_NUM deg_NUM naw_NUM oedd_VERB o_PRON ._PUNCT
Mae_VERB 'n_PART iawn_ADJ ti'sho_VERB fi_PRON agor_VERB hwnna_PRON i_ADP ch_PRON 'di_PART ?_PUNCT
Ia_INTJ plis_INTJ mam_NOUN ._PUNCT
Mond_VERB un_NUM o_ADP hein_PRON sy_VERB ar_ADP ôl_NOUN so_VERB dw_AUX i_PRON 'n_PART meddwl_VERB 'na_ADV ch_PRON 'di_PART boi_NOUN ._PUNCT
Iawn_INTJ ?_PUNCT
Ww_PART mae_VERB enwg_VERB cyfenw_NOUN 'di_PART ddefro_ADJ ._PUNCT
<_SYM aneglur_ADJ 2_NUM >_SYM ._PUNCT
Helo_INTJ ._PUNCT
Ia_ADJ helo_VERB a'thon_VERB ni_PRON i_PART chemist_VERB g'yna_VERB jyst_ADV i_PART ddeud_VERB helo_VERB ac_CONJ oedd_AUX o_PRON 'n_PART gwenu_VERB ar_ADP bawb_PRON yn_PART doeddach'd_VERB ?_PUNCT
[_PUNCT giglan_VERB ]_PUNCT ._PUNCT
O_ADP oedd_VERB o_PRON wrth_ADP 'i_PRON fodd_NOUN efo_ADP sylw_NOUN hwn_DET yndwyt_VERB ?_PUNCT
Ti_PRON wrth_ADP dy_DET fodd_NOUN efo_ADP sylw_NOUN ._PUNCT
Wyt_VERB ti_PRON yn_PART ia_ADJ ._PUNCT
A_CONJ '_PUNCT nath_NOUN o_ADP helpu_VERB heddiw_ADV do_ADJ ?_PUNCT
Ll'nau_VERB stafall_NOUN enwg_VERB do_INTJ nes_CONJ ti_PRON helpu_VERB fi_PRON ._PUNCT
Do_VERB wir_ADV do_INTJ ._PUNCT
Mae_VERB o_PRON 'n_PART lan_ADV neis_ADJ '_PUNCT wan_ADV enwg_VERB so_ADV nei_X 'di_PART gadw_VERB fo_PRON 'n_PART lan_ADJ i_ADP fi_PRON ._PUNCT
Cei_VERB tad_NOUN dos_VERB i_ADP sbio_VERB 'n_PART sydyn_ADJ ._PUNCT
Ofalus_ADJ mae_VERB 'r_DET ffenestri_NOUN 'n_PART gorad_NOUN oce_NOUN cariad_NOUN ._PUNCT
Iawn_INTJ ._PUNCT
207.447_NOUN
