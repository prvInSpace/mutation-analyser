Crynodeb_NOUN Pobol_ADJ y_DET Cwm_NOUN W_SYM 14_NUM
Mae_AUX Sam_PROPN yn_PART dychwelyd_VERB i_ADP 'r_DET pentref_NOUN ac_CONJ yn_PART rhoi_ADV gwybod_VERB i_ADP Colin_PROPN ei_PRON fod_AUX yn_PART dioddef_VERB o_ADP glefyd_NOUN siwgr_ADJ ._PUNCT
Mae_VERB Eileen_X a_CONJ Sam_PROPN yn_PART closio_VERB dros_ADP wydraid_NOUN o_ADP win_NOUN ,_PUNCT ond_CONJ pan_CONJ gaiff_VERB Sam_PROPN bendro_NOUN mae_AUX Eileen_X yn_ADP ei_PRON helpu_VERB i_ADP 'w_PRON wely_NOUN ._PUNCT
Mae_AUX 'n_PART gorwedd_VERB gyda_ADP fe_PRON ac_CONJ yn_PART syrthio_VERB i_ADP gysgu_VERB ,_PUNCT ond_CONJ caiff_VERB sioc_NOUN fawr_ADJ pan_CONJ mae_AUX 'n_PART deffro_VERB nes_CONJ ymlaen_ADV ._PUNCT
Mae_AUX Kelly_X a_CONJ Hywel_PROPN yn_PART trefnu_VERB gwasanaeth_NOUN coffa_VERB i_ADP Ed_ADV ._PUNCT
Er_ADP iddynt_PRON geisio_ADV cadw_VERB 'r_DET peth_NOUN rhag_ADP Sioned_PROPN ,_PUNCT mae_AUX hi_PRON 'n_PART cerdded_VERB i_ADP mewn_ADP yn_PART ystod_NOUN y_DET gwasanaeth_NOUN ,_PUNCT ond_CONJ nid_PART yw_AUX 'n_PART ymateb_VERB yn_PART un_NUM y_DET maen_AUX nhw_PRON 'n_PART ei_DET ddisgwyl_VERB ._PUNCT
Mae_AUX Hannah_PROPN yn_PART teimlo_VERB ei_PRON bod_VERB yn_PART barod_ADJ i_ADP 'w_PRON pherthynas_NOUN â_ADP Chester_PROPN symud_VERB cam_NOUN ymlaen_ADV ._PUNCT
Mae_AUX Chester_PROPN yn_PART ceisio_ADV creu_VERB argraff_NOUN ar_ADP Diane_PROPN a_CONJ Dai_PROPN drwy_ADP goginio_VERB pryd_NOUN iddynt_ADP ._PUNCT
Ond_CONJ yn_PART anffodus_ADJ ,_PUNCT aiff_VERB pethau_NOUN o_ADP chwith_NOUN ac_CONJ mae_AUX Diane_PROPN yn_PART bygwth_VERB anfon_VERB Hannah_PROPN '_PUNCT nôl_NOUN i_ADP Awstralia_PROPN !_PUNCT
Pobol_ADJ y_DET Cwm_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 8.00_NUM ,_PUNCT S4C_PROPN ._PUNCT
Isdeitlau_NOUN Cymraeg_PROPN a_CONJ Saesneg_PROPN
Llun_NOUN -_PUNCT Gwener_PROPN 6.00_NUM ,_PUNCT S4C_PROPN gydag_ADP isdeitlau_NOUN Saesneg_PROPN ar_ADP y_DET sgrin_NOUN
Ar_ADP alw_VERB :_PUNCT s4c.cymru_NOUN ;_PUNCT BBC_X iPlayer_ADJ a_CONJ llwyfannau_NOUN eraill_ADJ
Cynhyrchiad_NOUN BBC_NOUN Cymru_PROPN
