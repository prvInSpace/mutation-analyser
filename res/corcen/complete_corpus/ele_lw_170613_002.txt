Taith_Ebu Sain_Ebu Ffagan_Gwest 
WYTHNOS_Ebu TAFWYL_Gwacr |_Gwsym 
TAFWYL_Gwacr FRINGE_Gwest FESTIVAL_Gwest 
24.06_Gwdig ._Atdt 1_Gwdig 7_Gwdig -_Atdcys 03.07_Gwdig ._Atdt 1_Gwdig 7_Gwdig 
DYDD_Egu SUL_Egu 25.06_Gwdig ._Atdt 1_Gwdig 7_Gwdig 
Taith_Ebu Sain_Ebu Ffagan_Gwest 
Taith_Ebu arbennig_Anscadu i_Arsym weld_Be datblygiadau_Egll diweddaraf_Anscadu Amgueddfa_Ebu Werin_Ebu Cymru_Epb Sain_Ebu Ffagan_Gwest ._Atdt 
Ym_Arsym mis_Egu Gorffennaf_Egu eleni_Adf ,_Atdcan bydd_Bdyf3u cam_Egu ddiweddaraf_Anscadu prosiect_Egu ail-_Ublaen ddatblygu_Be Sain_Ebu Ffagan_Gwest yn_Arsym cael_Be ei_Rhadib3bu chwblhau_Be ._Atdt 
Yn_Arsym yr_YFB Haf_Egu ,_Atdcan bydd_Bdyf3u ymwelwyr_Egll yn_Arsym medru_Be cael_Be mynediad_Egu i_Arsym 'r_YFB Amgueddfa_Ebu drwy_Arsym 'r_YFB prif_Anseith adeilad_Egbu sydd_Bpres3perth wedi_Uberf ei_Rhadib3bu adnewyddu_Be 'n_Utra llwyr_Anscadu ._Atdt 
Dyma_Adf gyfle_Egu i_Rhapers1u chi_Rhapers2ll fynd_Be ar_Arsym daith_Ebu i_Arsym weld_Be y_YFB Prif_Anseith Adeilad_Egbu a_Cyscyd 'r_YFB Gweithdy_Egu -_Atdcys adeilad_Egbu newydd_Anscadu sbon_Adf sy_Bpres3perth 'n_Uberf dathlu_Be sgiliau_Egll crefftwyr_Egll ddoe_Adf a_Cyscyd heddiw_Adf -_Atdcys cyn_Arsym i_Arsym rannau_Ebll ohonynt_Ar3ll agor_Be yn_Arsym swyddogol_Anscadu i_Arsym 'r_YFB cyhoedd_Egu ._Atdt 
Lleoliad_Egu 
:_Atdcan Amgueddfa_Ebu Werin_Ebu Cymru_Epb Sain_Ebu Ffagan_Gwest ,_Atdcan Caerdydd_Ep ,_Atdcan CF_Gwacr 5_Gwdig 6_Gwdig XB_Gwacr 
Amser_Egu :_Atdcan 
11.00_Gwdig 
Cost_Ebu :_Atdcan 
AM_Arsym DDIM_Egu 