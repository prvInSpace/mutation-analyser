[_Atdchw Cerddoriaeth_Ebu gefndirol_Anscadu dros_Arsym y_YFB sgript_Ebu ]_Atdde ._Atdt 
S1_Anon Tu_Egu hwnt_Anscadu i_Arsym bob_Egll bro_Ebu a_Cyscyd bryn_Egu coedwig_Ebu cwm_Egu a_Arsym llyn_Egu saif_Bpres3u castell_Egu gudd_Anscadu sy_Bpres3perth 'n_Uberf cadw_Be 'i_Rhadib3gu lygaid_Egll ar_Arsym holl_Bancynnar gampau_Ebll 'r_YFB creaduriaid_Egll ._Atdt 
Dewch_Bgorch2ll yn_Arsym nes_Anscym fe_Uberf gewch_Bpres2ll eich_Rhadib2ll synnu_Be gan_Arsym fyd_Egu mawr_Anscadu enw_rhaglen_Anon digri_Anscadu [_Atdchw Cerddoriaeth_Ebu yn_Arsym stopio_Be ]_Atdde ._Atdt 
S1_Anon Mae_Bpres3u hi_Rhapers3bu 'n_Utra ddiwrnod_Egu braf_Anscadu o_Rhapers3gu haf_Egu ac_Cyscyd mae_Bpres3u 'r_YFB llyn_Egu yn_Arsym dawel_Anscadu [_Atdchw Saib_Egu ]_Atdde [_Atdchw Cerddoriaeth_Ebu gefndirol_Anscadu yn_Arsym ail_Rhitrefd ddechrau_Egu ]_Atdde ._Atdt 
S1_Anon Mae_Bpres3u 'r_YFB crucaid_unk a_Cyscyd 'r_YFB sglefrwyr_Egll dŵr_Egu wrth_Arsym eu_Banmedd3ll bodd_Egu yn_Arsym sglefrio_Be ar_Arsym wyneb_Egu y_YFB llyn_Egu ._Atdt 
Mae_Bpres3u coese_Ebll blewog_Anscadu yn_Arsym eu_Rhadib3ll cadw_Be 'n_Utra sych_Anscadu a_Cyscyd 'u_Rhadib3ll helpu_Be i_Arsym symud_Be yn_Arsym slic_Anscadu ._Atdt 
S2_Anon <_Atdchw gwichia_Bgorch2u >_Atdde Wiii_Gwest bwmp_Egu mas_Adf o_Arsym ffor_Ebu ._Atdt 
S3_Anon O_Rhapers3gu ma_Bpres3u '_Atddyf mhen_Egu i_Rhapers1u yn_Arsym troi_Be fel_Cyscyd top_Egu ._Atdt 
S4_Anon [ebychu]_Gwann ._Atdt 
O_Rhapers3gu ma_Bpres3u '_Atddyf nghoes_Ebu i_Rhapers1u 'n_Uberf cosi_Be ._Atdt 
W_Ebych yn_Arsym fan_Ebu hyn_Rhadangd ._Atdt 
O_Rhapers3gu o_Rhapers3gu o_Arsym y_YFB piwiad_unk piws_Anscadu ._Atdt 
S5_Anon [ebychu]_Gwann Ga_Bpres1u i_Rhapers1u ddawnsio_Be efo_Arsym chdi_Rhapers2u ?_Atdt 
S1_Anon Ma_Egbu enwg_Anon y_YFB ceffyl_Egu dŵr_Egu bolwyn_unk yn_Arsym dangos_Be 'i_Rhadib3bu hun_Rhaatb wrth_Arsym nofio_Be ar_Arsym ei_Rhadib3gu gefn_Egu ._Atdt 
S6_Anon [ebychu]_Gwann O_Arsym go_Egu dda_Anscadu rwan_Adf de_Egu ._Atdt 
Dyma_Adf 'r_YFB bywyd_Egu i_Rhapers1u chdi_Rhapers2u ._Atdt 
Torheulio_Gwest ar_Arsym y_YFB llyn_Egu ._Atdt 
S7_Anon Ei_Rhadib3bu ._Atdt 
Gwylia_Bgorch2u dy_Rhadib2u hun_Rhifol y_YFB pen_Egu dafad_Ebu ._Atdt 
Ti_Rhapers2u 'n_Uberf cuddio_Be 'r_YFB haul_Egu ._Atdt 
S8_Anon O_Arsym y_YFB ddrwg_Anscadu da_Anscadu fi_Rhapers1u enwg_Anon [=]_Gwann y_YFB [/=]_Gwann ffilish_unk i_Arsym weld_Be ti_Rhapers2u nawr_Adf fana_Anseith ._Atdt 
S1_Anon Bydd_Bdyf3u y_YFB sglefrwyr_Egll dŵr_Egu yn_Arsym treulio_Be orie_Ebll yn_Arsym sglefrio_Be 'n_Utra hapus_Anscadu fel_Cyscyd hyn_Rhadangd [_Atdchw Saib_Egu gyda_Arsym cherddoriaeth_Ebu gefndirol_Anscadu ]_Atdde ._Atdt 
S1_Anon O_Arsym dan_Egu y_YFB dŵr_Egu ma_Bandangg enwb_Anon 'r_YFB falwoden_unk a_Cyscyd 'i_Rhadib3bu ffrind_Egu y_YFB penbwl_unk yn_Arsym mwynhau_Be 'r_YFB sioe_Ebu ._Atdt 
S9_Anon Y_YFB pwy_Egu wyt_Bpres2u ti_Rhapers2u 'n_Uberf hoffi_Be ora_Anseith ?_Atdt 
S10_Anon Y_YFB [=]_Gwann wi_Bpres1u 'n_Rhadib1ll [/=]_Gwann wi_Bpres1u 'n_Uberf lico_Be 'r_YFB ddau_Rhifol sy_Bpres3perth 'n_Uberf dawnsio_Be ar_Arsym bwyll_Egu enwg_Anon ._Atdt 
Ma_Bpres3u un_Rhaamh yn_Arsym pwyntio_Be lan_Adf a_Cyscyd 'r_YFB llall_Rhaamh yn_Arsym pwyntio_Be lawr_Adf [_Atdchw Saib_Egu yn_Arsym y_YFB sgript_Ebu a_Cyscyd cherddoriaeth_Ebu gefndirol_Anscadu yn_Arsym parhau_Be ]_Atdde ._Atdt 
S11_Anon Hen_Anscadu bryfaid_unk bach_Anscadu y_YFB Bala_Egu ._Atdt 
Does_Bpres3amhen na_Uneg 'm_Rhadib1u llonydd_Anscadu i_Arsym gael_Be i_Arsym dorheulo_Be wir_Ebych i_Rhapers1u chi_Rhapers2ll ._Atdt 
116.367_Gwdig 
119.881_Gwdig 
126.966_Gwdig 
133.425_Gwdig 
136.982_Gwdig 
143.128_Gwdig 
299.989_Gwdig 