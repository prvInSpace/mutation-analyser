0.0_Gwdig 
S1_Anon Am_Arsym y_YFB tro_Egu cynta_Rhitrefd '_Atddyf erioed_Adf eleni_Adf bydd_Bdyf3u gŵyl_Ebu newydd_Anscadu yn_Arsym cael_Be ei_Rhadib3bu chynnal_Be yng_Arsym Nghaerfyrddin_Ep y_YFB penwythnos_Egu hwn_Rhadangg sy_Bpres3perth 'n_Uberf dod_Be ._Atdt 
Mi_Uberf fydd_Bdyf3u 'na_Adf ddigwyddiade_Egll ar_Arsym hyd_Egu a_Cyscyd lled_Egu y_YFB dre_Ebu ar_Arsym sawl_Banmeint llwyfan_Egu ac_Cyscyd amrywiol_Anscadu bebyll_Ebll ._Atdt 
Ac_Cyscyd yma_Adf i_Arsym sôn_Egu mwy_Anscym [-]_Gwann wel_Ebych enwb_Anon cyfenw_Anon a_Cyscyd _enwg__Anon __cyfenw___Anon o_Rhapers3gu ___sefydliad____Anon ._Atdt 
Croeso_Egu i_Rhapers1u chi_Rhapers2ll 'ch_Rhadib2ll dau_Rhifol ._Atdt 
S2_Anon Diolch_Egu ._Atdt 
S1_Anon Neis_Anscadu iawn_Adf i_Arsym gael_Be eich_Banmedd2ll cwmni_Egu chi_Rhapers2ll ._Atdt 
Mae_Bpres3u 'n_Uberf swnio_Be 'n_Utra dipyn_Egu o_Rhapers3gu ŵyl_Ebu a_Cyscyd be_Rhagof '_Atddyf sy_Bpres3perth 'n_Utra neis_Anscadu hefyd_Adf mae_Bpres3u '_Atddyf pyn_unk bach_Anscadu yn_Arsym wahanol_Anscadu i_Arsym 'r_YFB arfer_Egu 'n_Arsym '_Atddyf d_Gwllyth 'di_Arsym ?_Atdt 
S3_Anon Ydi_Bpres3u ._Atdt 
Mae_Bpres3u 'n_Utra dipyn_Egu o_Rhapers3gu ŵyl_Ebu ._Atdt 
Mae_Bpres3u hi_Rhapers3bu 'di_Uberf tyfu_Be lot_Ebu mwy_Anscym na_Cyscyd be_Rhagof '_Atddyf oeddan_Bamherff1ll ni_Rhapers1ll 'di_Uberf feddwl_Be wrth_Arsym i_Arsym ni_Rhapers1ll drefnu_Be ._Atdt 
Yn_Arsym wreiddiol_Anscadu gathon_unk ni_Rhapers1ll ein_Rhadib1ll cysylltu_Be gan_Arsym nifer_Egu o_Arsym ysgolion_Ebll yn_Arsym yr_YFB ardal_Ebu yn_Arsym gofyn_Be "_Atddyf fysach_Bpres2ll chi_Rhapers2ll 'n_Utra fodlon_Anscadu '_Atddyf neud_Be rh'wbath_Rhaamh "_Atddyf i_Arsym ddathlu_Be 'r_YFB gwaith_Ebu mae_Bpres3u nhw_Rhapers3ll 'n_Arsym '_Atddyf neud_Be tuag_Arsym at_Arsym y_YFB Siarter_Egbu Iaith_Ebu ?_Atdt 
A_Cyscyd wedyn_Adf '_Atddyf naethon_Bgorff3ll ni_Rhapers1ll weld_Be [-]_Gwann wel_Ebych mae_Bpres3u 'na_Adf alw_Egu yn_Arsym yr_YFB ardal_Ebu [-]_Gwann '_Atddyf d_Gwllyth oes_Bpres3amhen 'na_Adf ddim_Egu gŵyl_Anscadu yn_Arsym agos_Anscadu iawn_Adf i_Arsym dre_Ebu Caerfyrddin_Ep ._Atdt 
Felly_Adf mae_Bpres3u 'na_Adf alw_Egu i_Arsym ni_Rhapers1ll '_Atddyf neud_Be rh'wbath_Rhaamh mwy_Rhaamh ._Atdt 
Gŵyl_Ebu go_Adf iawn_Egu '_Atddyf lly_Bgorch2u ._Atdt 
Wedyn_Adf [=]_Gwann mae_Bpres3u o_Rhapers3gu [/=]_Gwann mae_Bpres3u o_Rhapers3gu 'di_Uberf tyfu_Be '_Atddyf ŵan_Adf i_Arsym fod_Be yn_Arsym glamp_Egu o_Arsym beth_Egu hefo_Arsym nifer_Egu o_Rhapers3gu llwyfanne_Egll [-]_Gwann lot_Rhaamh o_Arsym bebyll_Ebll mawr_Anscadu lliwgar_Anscadu fath_Banhwyr â_Arsym rhei_Banmeint syrcas_Ebu '_Atddyf lly_Bgorch2u [-]_Gwann yn_Arsym wahanol_Anscadu lefydd_Egll '_Atddyf lly_Bgorch2u ._Atdt 
Wedyn_Adf [=]_Gwann mae_Bpres3u o_Rhapers3gu 'n_Arsym [/=]_Gwann mae_Bpres3u o_Rhapers3gu 'n_Utra gyffrous_Anscadu iawn_Adf i_Arsym ni_Rhapers1ll ._Atdt 
'N_Arsym '_Atddyf d_Gwllyth 'di_Arsym ._Atdt 
S4_Anon Mae_Bpres3u nifer_Egu o_Arsym wyliau_Ebll wrth_Arsym gwrs_Egu yn_Arsym cael_Be eu_Rhadib3ll cynnal_Be mewn_Arsym cae_Egu '_Atddyf falle_Adf '_Atddyf ar_Arsym gyrion_Egll pentre_Egu neu_Cyscyd ar_Arsym gyrion_Egll y_YFB dre_Ebu ._Atdt 
Ond_Arsym fel_Cyscyd mae_Bpres3u 'r_YFB enw_Egu 'n_Uberf awgrymu_Be fan_Ebu hyn_Rhadangd [-]_Gwann canol_Egu dre_Ebu amdani_Ar3bu ._Atdt 
S3_Anon Wel_Ebych ia_Adf ._Atdt 
Mae_Bpres3u o_Rhapers3gu yn_Arsym cael_Be ei_Rhadib3gu gynnal_Be yn_Arsym yr_YFB [-]_Gwann i_Arsym 'r_YFB rhei_Rhaamh sy_Bpres3perth 'n_Uberf nabod_Be y_YFB dre_Ebu '_Atddyf lly_Bgorch2u [-]_Gwann yn_Arsym yr_YFB hen_Anscadu ysgol_Ebu enw_Anon enw_Anon mae_Bpres3u nhw_Rhapers3ll 'n_Uberf galw_Be fo_Rhapers3gu bellach_Adf [-]_Gwann sydd_Bpres3perth reit_Ebych drws_Egu nesa_Anseith '_Atddyf lle_Rhagof mae_Bpres3u 'r_YFB siopa_Be a_Cyscyd 'r_YFB meysydd_Egll parcio_Be ._Atdt 
'_Atddyf Dyn_Bpres3ll mae_Bpres3u 'r_YFB parcio_Be am_Arsym ddim_Egu ar_Arsym y_YFB dydd_Egu [-]_Gwann sydd_Bpres3perth yn_Arsym handi_Anscadu iawn_Adf ._Atdt 
'_Atddyf Lly_Bgorch2u mae_Bpres3u pobl_Ebu yn_Arsym medru_Be cerdded_Be o_Arsym 'r_YFB meysydd_Egll parcio_Be fewn_Arsym yne_Bandangd ._Atdt 
Felly_Adf '_Atddyf dan_Bpres1ll ni_Rhapers1ll 'n_Utra ffodus_Anscadu iawn_Adf bod_Be 'na_Adf leoliad_Egu mor_Adf dda_Anscadu yng_Arsym nghanol_Egu dre_Ebu Caerfyrddin_Ep i_Arsym ni_Rhapers1ll ddefnyddio_Be ._Atdt 
S4_Anon Ia_Adf ._Atdt 
Gwych_Anscadu ._Atdt 
Ia_Adf ._Atdt 
S1_Anon A_Cyscyd mae_Bpres3u 'na_Adf dipyn_Egu o_Rhapers3gu line_Gwest -_Atdcys up_Gwest hefyd_Adf 'n_Arsym '_Atddyf d_Gwllyth oes_Bpres3amhen i_Arsym ddenu_Be pawb_Rhaamh i_Arsym 'r_YFB ŵyl_Ebu newydd_Anscadu sbon_Adf 'ma_Adf ?_Atdt 
S2_Anon O_Rhapers3gu 's_Gwllyth ._Atdt 
O_Rhapers3gu 's_Gwllyth ._Atdt 
Y_YFB 'n_Arsym ni_Rhapers1ll 'di_Uberf bod_Be wrthi_Ar3bu yn_Arsym [aneglur]_Gwann am_Arsym yr_YFB artistiaid_Egll y_YFB 'n_Utra ni_Rhapers1ll eisie_Egu ._Atdt 
So_Adf ma_Bpres3u '_Atddyf '_Atddyf dan_Bpres1ll ni_Rhapers1ll rhestr_Ebu eitha_Adf '_Atddyf hir_Anscadu enwb_Anon cyfenw_Anon _enw__Anon __enwg___Anon ___cyfenw____Anon ____enw_____Anon ._Atdt 
Band_Egu pres_Anscadu enw_Anon enw_Anon a_Cyscyd _enw__Anon ._Atdt 
S4_Anon A_Cyscyd ma_Bpres3u '_Atddyf hwnna_Rhadangg 'n_Utra rhywbeth_Rhaamh sy_Bpres3perth 'n_Uberf mynd_Be i_Arsym apelio_Be at_Arsym bawb_Rhaamh ._Atdt 
Bydd_Bdyf3u rhywun_Egu yn_Arsym lecio_unk rhywun_Rhaamh ._Atdt 
Bydd_Bdyf3u pawb_Rhaamh yn_Arsym lecio_unk rhywun_Rhaamh sy_Bpres3perth 'n_Uberf perfformio_Be yndefe_unk ._Atdt 
S2_Anon Ie_Adf ._Atdt 
Mae_Bpres3u 'n_Rhadib1ll gymwsg_unk ._Atdt 
Dyna_Adf beth_Rhagof '_Atddyf naethon_Bgorff3ll ni_Rhapers1ll amdan_Arsym ._Atdt 
Oeddan_Bamherff1ll ni_Rhapers1ll 'n_Utra mo'yn_Egu targedu_Be 'r_YFB teulu_Egu cyfan_Anscadu [-]_Gwann yndefe_unk [-]_Gwann a_Cyscyd pobl_Ebu ifanc_Anscadu [-]_Gwann fel_Cyscyd bod_Be nhw_Rhapers3ll 'n_Uberf dod_Be i_Arsym 'r_YFB dre_Ebu a_Arsym bod_Be nhw_Rhapers3ll 'n_Uberf gwario_Be 'r_YFB pnawn_Egu gydan_Ar3ll ni_Rhapers1ll wedyn_Adf yn_Arsym enw_Anon ._Atdt 
S4_Anon Mm_Ebych ._Atdt 
Ie_Adf ._Atdt 
Ond_Arsym ti_Rhapers2u 'n_Arsym '_Atddyf bod_Be [-]_Gwann yn_Arsym ogystal_Anscyf â_Arsym 'r_YFB bandie_Egll ag_Arsym ati_Ar3bu mae_Bpres3u 'na_Adf bebyll_Ebll yn_Arsym cynnal_Be gweithgareddau_Egll amrywiol_Anscadu iawn_Adf 'n_Arsym '_Atddyf d_Gwllyth oes_Bpres3amhen ?_Atdt 
S2_Anon O_Rhapers3gu 's_Gwllyth ._Atdt 
Bydd_Bdyf3u gweithgaredde_Egll yn_Arsym mynd_Be o_Arsym wahanol_Anscadu chwaraeon_Egll a_Rhaperth fydd_Bdyf3u '_Atddyf dan_Bpres1ll ni_Rhapers1ll gwahanol_Anscadu gweithdai_Egll wedyn_Adf yn_Arsym mynd_Be o_Arsym flogio_Be i_Rhapers1u enw_Anon gyda_Arsym enwg_Anon ._Atdt 
Barddoni_Egu gyda_Arsym enwb_Anon ._Atdt 
A_Cyscyd wedyn_Adf y_YFB 'n_Arsym ni_Rhapers1ll 'n_Uberf symyd_Be '_Atddyf mlaen_Adf wedyn_Adf i_Rhapers1u pethe_Egll ar_Arsym gyfer_Egu plant_Egll ._Atdt 
So_Adf ma_Bpres3u '_Atddyf gydan_Ar3ll ni_Rhapers1ll enw_Anon yn_Arsym dod_Be i_Rhapers1u '_Atddyf neud_Be sesiwn_Ebu a_Cyscyd sesiwn_Ebu '_Atddyf neud_Be stori_Ebu ._Atdt 
Ond_Arsym mae_Bpres3u dros_Arsym pumdeg_Rhifold o_Arsym weithgareddau_Egll i_Arsym gyd_Egu ._Atdt 
S1_Anon Yn_Arsym ogystal_Anscyf â_Arsym 'r_YFB [aneglur]_Gwann a_Arsym hwyl_Ebu a_Arsym sbri_Egbu ma_Bpres3u '_Atddyf 'na_Bandangd babell_Ebu fusnes_Egbu hefyd_Adf 'n_Arsym '_Atddyf d_Gwllyth oes_Bpres3amhen sydd_Bpres3perth ar_Arsym gyfer_Egu tu_Egu hwnt_Anscadu ?_Atdt 
S2_Anon Bydd_Bdyf3u ._Atdt 
Bydd_Bdyf3u ._Atdt 
Bydd_Bdyf3u busnesau_Egbll a_Cyscyd stondinwyr_unk yn_Arsym dod_Be i_Rhapers1u cael_Be gwerthu_Be eu_Banmedd3ll cynnyrch_Egu [-]_Gwann i_Rhapers1u cael_Be y_YFB cyfle_Egu i_Arsym ddod_Be i_Arsym 'r_YFB dre_Ebu ._Atdt 
So_Adf mae_Bpres3u nhw_Rhapers3ll 'n_Uberf cael_Be ffenest_Ebu siop_Ebu fel_Cyscyd petai_Cyscyd ._Atdt 
Ond_Arsym na_Rhaperth ._Atdt 
Ma_Bpres3u '_Atddyf '_Atddyf dan_Bpres1ll ni_Rhapers1ll wedyn_Adf llwyfan_Egu hefyd_Adf lle_Egu bydd_Bdyf3u y_YFB cyfle_Egu i_Rhapers1u cyflwyno_Be paneli_Egll fydd_Bdyf3u yn_Arsym sôn_Egu am_Gwtalf pwysigrwydd_Egu y_YFB Gymraeg_Ebu yn_Arsym y_YFB byd_Egu busnes_Egbu ._Atdt 
260.414_Gwdig 
436.579_Gwdig 
S5_Anon Ers_Arsym blynydde_Ebll mae_Bpres3u ysgol_Ebu enw_Anon wedi_Uberf bod_Be yn_Arsym cyfeillio_unk gydag_Arsym ysgolion_Ebll ar_Arsym draws_Arsym Ewrop_Ep ._Atdt 
A_Cyscyd '_Atddyf nawr_Adf ynghyd_Adf ag_Arsym ysgol_Ebu enw_Anon mae_Bpres3u nhw_Rhapers3ll 'n_Uberf croesawu_Be plant_Egll o_Rhapers3gu Ddenmarc_Gwest [-]_Gwann Ffrainc_Epb [-]_Gwann ac_Cyscyd athrawon_Egll o_Arsym 'r_YFB Eidal_Ep i_Arsym ymweld_Be â_Arsym nhw_Rhapers3ll ._Atdt 
A_Cyscyd phwrpas_Egu yr_YFB ymweliad_Egu ?_Atdt 
Cael_Be blas_Egu o_Arsym Sir_Ebu Gâr_Bpres3u ac_Cyscyd o_Arsym Gymru_Epb ._Atdt 
S6_Anon Wel_Ebych ma_Bpres3u '_Atddyf fe_Rhapers3gu wedi_Uberf bod_Be yn_Arsym brofiad_Egu hollol_Anscadu newydd_Anscadu i_Arsym ni_Rhapers1ll yn_Arsym y_YFB prosiect_Egu y_YFB tro_Egu yma_Adf gan_Arsym yn_Arsym y_YFB gorffennol_Egu '_Atddyf d_Gwllyth yw_Bpres3u plant_Egll ddim_Egu wedi_Uberf dod_Be i_Arsym ymweld_Be â_Arsym 'r_YFB ysgol_Ebu ._Atdt 
Y_YFB 'n_Arsym ni_Rhapers1ll wedi_Uberf bod_Be yn_Arsym cyfathrebu_Be trwy_Arsym Skype_Ep ac_Cyscyd wrth_Arsym gwrs_Egu mae_Bpres3u pethau_Egll wedi_Uberf symyd_Be '_Atddyf mlaen_Adf erbyn_Arsym hyn_Rhadangd gyda_Arsym Facetime_Ep a_Cyscyd gwahanol_Anscadu pethe_Egll ._Atdt 
A_Cyscyd wedyn_Adf '_Atddyf ni_Rhapers1ll 'di_Uberf gallu_Be cydgweithio_unk a_Cyscyd gweld_Be wel_Ebych pa_Bangof syniade_Egll sydd_Bpres3perth yn_Arsym gweithio_Be [-]_Gwann ond_Arsym be_Rhagof '_Atddyf sy_Bpres3perth 'n_Utra bwysig_Anscadu [-]_Gwann ar_Arsym draws_Arsym y_YFB byd_Egu ._Atdt 
Felly_Adf mae_Bpres3u Caerfyrddin_Ep [-]_Gwann enw_Egu [-]_Gwann wedi_Uberf 'i_Rhadib3bu chysylltu_Be â_Arsym 'r_YFB byd_Egu ._Atdt 
S7_Anon Y_YFB 'n_Arsym ni_Rhapers1ll 'n_Uberf cydweithio_Be gyda_Arsym ysgol_Ebu enw_Anon ymm_Ebych yyy_Ebych mwyn_Egu datblygu_Be 'r_YFB elfen_Ebu rhyngwladol_Anscadu 'ma_Adf [-]_Gwann yyy_Ebych mwyn_Egu sicrhau_Be bod_Be y_YFB plant_Egll yn_Arsym cael_Be ystod_Ebu o_Rhapers3gu brofiade_Egll i_Arsym ddatblygu_Be yn_Arsym ddinasyddion_Egll egwyddorol_Anscadu yng_Arsym Nghymru_Epb ac_Cyscyd yn_Arsym y_YFB byd_Egu ._Atdt 
Felly_Adf erbyn_Arsym hyn_Rhadangd [-]_Gwann wrth_Arsym i_Arsym ni_Rhapers1ll gynllunio_Be gweithgaredde_Egll [-]_Gwann mae_Bpres3u 'r_YFB plant_Egll wedi_Uberf elwa_Be llawer_Rhaamh [-]_Gwann mae_Bpres3u 'n_Utra rhaid_Egu i_Rhapers1u mi_Rhapers1u ddweud_Be ._Atdt 
S8_Anon Mae_Bpres3u 'n_Utra bwysig_Anscadu 'di_Uberf roi_Be cyfleoedd_Egll i_Arsym blant_Egll ddatblygu_Be fel_Cyscyd dinasyddion_Egll byd-eang_Anscadu ._Atdt 
Mae_Bpres3u 'n_Utra bwysig_Anscadu codi_Bpres2u 'u_Banmedd3ll ymwybyddiaeth_Ebu nhw_Rhapers3ll o_Arsym ddiwylliannau_Egll yyy'ill_unk [-]_Gwann o_Arsym brofiadau_Egll gwledydd_Ebll yyy'ill_unk ._Atdt 
Mae_Bpres3u o_Arsym wedi_Adf rhoi_Be cyfle_Egu iddyn_Ar3ll nhw_Rhapers3ll ddatblygu_Be sgiliau_Egll cyfathrebu_Be yn_Arsym bennaf_Egu ._Atdt 
Oherwydd_Cyscyd nid_Uneg yn_Arsym unig_Anscadu mae_Bpres3u iaith_Ebu arall_Anscadu ond_Arsym hefyd_Adf mae_Bpres3u 'r_YFB ffordd_Ebu mae_Bpres3u 'r_YFB plant_Egll wedi_Uberf arfer_Be hefo_Arsym 'u_Banmedd3ll bywyd_Egu ysgol_Ebu yn_Arsym hollol_Anscadu wahanol_Anscadu ._Atdt 
Ag_Arsym oedd_Bamherff3u o_Rhapers3gu 'n_Utra braf_Anscadu gweld_Be y_YFB plant_Egll yn_Arsym rhoi_Be cwtsh_Egu i'w_Arsym gilydd_Egu ag_Arsym yn_Arsym [-]_Gwann mewn_Arsym ffordd_Ebu [-]_Gwann oeddan_Bamherff1ll nhw_Rhapers3ll 'n_Utra drist_Anscadu bod_Be nhw_Rhapers3ll 'n_Uberf ymadael_Be ond_Arsym yn_Arsym edrych_Egu ymlaen_Adf at_Arsym y_YFB profiadau_Egll oedd_Bamherff3u yn_Arsym digwydd_Be yn_Arsym weddill_Egu yr_YFB wythnos_Ebu '_Atddyf lly_Bgorch2u ._Atdt 
S5_Anon Ac_Cyscyd ar_Arsym ôl_Anscadu wythnos_Ebu llawn_Anscadu o_Rhapers3gu weithgaredde_Egll penllanw_Egu 'r_YFB cyfan_Egu oedd_Bamherff3u y_YFB te_Egu ryngwladol_Anscadu ._Atdt 
Cyfle_Egu i_Arsym rannu_Be bwyd_Egu o_Arsym Gymru_Epb a_Cyscyd rhannu_Be diwylliannau_Egll 'r_YFB holl_Bancynnar wledydd_Ebll ._Atdt 
S6_Anon Wel_Ebych mae_Bpres3u heno_Adf yn_Arsym gyffrous_Anscadu iawn_Adf ._Atdt 
Mae_Bpres3u gan_Arsym pob_Egll gwlad_Ebu eitem_Ebu ._Atdt 
Tame_Egll 'd_Gwllyth bach_Anscadu o_Arsym ganu_Egu ._Atdt 
Tame_Egll 'd_Gwllyth bach_Anscadu o_Arsym ddawnsio_Be i_Arsym ddangos_Be ein_Banmedd1ll diwyllianne_Egll ni_Rhapers1ll a_Cyscyd 'n_Utra Cymreicdod_Gwest ._Atdt 
S9_Anon Fi_Rhapers1u 'n_Uberf hoffi_Be chwarae_Egu pêl_Ebu droed_Egbu â_Arsym 'r_YFB plant_Egll a_Arsym chwarae_Egu gydan_Ar3ll nhw_Rhapers3ll a_Cyscyd siarad_Be ._Atdt 
A_Cyscyd dysgu_Be nhw_Rhapers3ll '_Atddyf pyn_unk bach_Anscadu o_Rhapers3gu geiriau_Egll yn_Arsym Cymraeg_Ebu ._Atdt 
S10_Anon Yn_Arsym yr_YFB wythnos_Ebu 'r_YFB ydw_Bpres1u i_Rhapers1u wedi_Adf hoffi_Be dysgu_Be 'u_Banmedd3ll ieithoedd_Ebll nhw_Rhapers3ll ._Atdt 
606.241_Gwdig 
724.706_Gwdig 
S1_Anon Ma_Bpres3u '_Atddyf 'i_Rhadib3gu dal_Be yn_Arsym dwym_Anscadu a_Cyscyd 'r_YFB tywydd_Egu twym_unk 'ma_Adf i_Arsym barha_Bpres3u '_Atddyf am_Arsym dipyn_Egu eto_Adf yn_Arsym ôl_Egu y_YFB sôn_Egu ._Atdt 
Wyt_Bpres2u ti_Rhapers2u 'n_Uberf mwynhau_Be 'r_YFB tywydd_Egu 'ma_Adf ?_Atdt 
S11_Anon Fi_Rhapers1u yn_Arsym un_Rhaamh sy_Bpres3perth 'n_Rhadib1ll lecio_unk 'r_YFB haul_Egu ._Atdt 
Ydw_Bpres1u ._Atdt 
Ond_Arsym mae_Bpres3u 'r_YFB haul_Egu un_Rhaamh mor_Adf danbe_unk 'd_Gwllyth mae_Bpres3u hyd_Egu yn_Arsym oed_Egu fi_Rhapers1u 'n_Uberf mynd_Be i_Rhapers1u '_Atddyf wilio_unk am_Arsym gysgod_Egu ._Atdt 
A_Cyscyd 'na_Adf 'r_YFB peth_Egu doeth_Anscadu i_Rhapers1u '_Atddyf neud_Be mewn_Arsym ffordd_Ebu yndefe_unk ._Atdt 
Yn_Arsym lle_Egu [-]_Gwann achos_Egu '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf credu_Be [-]_Gwann be_Rhagof '_Atddyf sy_Bpres3perth 'di_Uberf digwydd_Be [-]_Gwann y_YFB 'n_Arsym ni_Rhapers1ll 'di_Uberf colli_Be ar_Arsym ein_Banmedd1ll hune_Ebll 'n_Rhadib1ll wrth_Arsym weld_Be yr_YFB haul_Egu ._Atdt 
Achos_Cyscyd '_Atddyf sa_Bgorb1u ni_Rhapers1ll 'di_Uberf gweld_Be lot_Ebu o_Rhapers3gu '_Atddyf no_Egu fe_Rhapers3gu ._Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S11_Anon Ond_Arsym mae_Bpres3u e_Rhapers3gu '_Atddyf 'di_Uberf par'a_Be '_Atddyf o_Arsym ddydd_Egu [=]_Gwann i_Arsym ddydd_Egu [/=]_Gwann i_Arsym ddydd_Egu 'n_Arsym '_Atddyf d_Gwllyth yw_Bpres3u e_Rhapers3gu '_Atddyf ?_Atdt 
So_Adf o_Rhapers3gu 's_Gwllyth '_Atddyf im_unk rhaid_Egu i_Rhapers1u chi_Rhapers2ll fynd_Be mas_Egu a_Arsym gorwedd_Be yn'o_Ar3gu fe_Rhapers3gu trwy_Arsym 'r_YFB amser_Egu [-]_Gwann ._Atdt 
S1_Anon Trwyr_Gwest dydd_Egu ._Atdt 
Na_Rhaperth ._Atdt 
S4_Anon Ie_Adf ._Atdt 
Achos_Cyscyd y_YFB 'n_Utra ni_Rhapers1ll gyd_Anscadu yn_Arsym poeni_Be "_Atddyf o_Rhapers3gu efallai_Adf mai_Cyscyd heddi_Adf fydd_Bdyf3u y_YFB diwrnod_Egu ffein_Anscadu ola_Anscadu '_Atddyf so_Adf '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Arsym '_Atddyf neud_Be yn_Arsym fawr_Anscadu ohono_Ar3gu fe_Rhapers3gu "_Atddyf yndefe_unk ._Atdt 
yyy_Ebych ma_Bpres3u '_Atddyf 'i_Rhadib3bu 'n_Utra gymylog_Anscadu heddi_Adf 'n_Utra fan_Ebu hyn_Rhadangd yn_Arsym lleoliad_Anon ond_Arsym ma_Bpres3u '_Atddyf 'i_Rhadib3gu dal_Be yn_Arsym dwym_Anscadu ._Atdt 
S11_Anon Ydi_Bpres3u ._Atdt 
Ma_Bpres3u '_Atddyf 'i_Rhadib3bu 'n_Utra drymedd_Egu on_Gwest 'd_Gwllyth yw_Bpres3u 'i_Rhadib3bu ._Atdt 
Ond_Arsym cofiwch_Bgorch2ll hyd_Egu yn_Arsym oed_Egu pan_Egu bod_Be hi_Rhapers3bu 'n_Utra gymylog_Anscadu mae_Bpres3u 'r_YFB haul_Egu yn_Arsym gallu_Egu llosgi_Be eich_Banmedd2ll croen_Egu chi_Rhapers2ll ._Atdt 
So_Adf mae_Bpres3u 'n_Utra bwysig_Anscadu i_Arsym ddefnyddio_Be digon_Egu o_Arsym eli_Egu haul_Egu ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u di_Rhapers2u gweld_Be ôô_Ebych [-]_Gwann chi_Rhapers2ll 'n_Arsym '_Atddyf bod_Be [-]_Gwann achosion_Egll erchyll_Anscadu o_Rhapers3gu llosg_Anscadu haul_Egu blwyddyn_Ebu hyn_Rhadangd ._Atdt 
A_Cyscyd ma_Bpres3u '_Atddyf pobl_Ebu '_Atddyf falle_Adf '_Atddyf ddim_Egu yn_Arsym '_Atddyf styried_Be a_Cyscyd '_Atddyf dyn_Bpres3ll nhw_Rhapers3ll ddim_Adf yn_Arsym ail_Rhitrefd ddodi_Be fe_Rhapers3gu arno_Ar3gu ._Atdt 
Mae_Bpres3u nhw_Rhapers3ll 'n_Uberf meddwl_Be "_Atddyf dodi_Be fe_Rhapers3gu unwaith_Adf y_YFB dydd_Egu [-]_Gwann bydd_Bdyf3u hwnna_Rhadangg 'n_Utra llawn_Anscadu digon_Rhaamh "_Atddyf ._Atdt 
'_Atddyf D_Gwllyth yw_Bpres3u e_Rhapers3gu '_Atddyf ddim_Egu ._Atdt 
Achos_Cyscyd mae_Bpres3u 'r_YFB haul_Egu un_Rhaamh mor_Adf dwyn_Be mae_Bpres3u 'n_Utra rhaid_Egu i_Rhapers1u chi_Rhapers2ll barhau_Be i_Arsym ddodi_Be fe_Rhapers3gu drwy_Arsym 'r_YFB amser_Egu ._Atdt 
S1_Anon A_Cyscyd nid_Uneg dim_Egu ond_Arsym y_YFB llosg_Anscadu y_Rhadib1u 'ch_Rhadib2ll chi_Rhapers2ll 'n_Uberf gweld_Be gydach_Ar2ll llygad_Egu ._Atdt 
Mae_Bpres3u 'n_Uberf gallu_Be yn_Arsym amlwg_Anscadu creu_Be niwed_Egu erchyll_Anscadu 'n_Arsym '_Atddyf d_Gwllyth 'di_Arsym ?_Atdt 
S11_Anon Ydi_Bpres3u mae_Bpres3u e_Rhapers3gu '_Atddyf ._Atdt 
Achos_Cyscyd [-]_Gwann chi_Rhapers2ll 'n_Arsym '_Atddyf bod_Be [-]_Gwann llosg_Anscadu yw_Bpres3u llosg_Anscadu ._Atdt 
Os_Cyscyd y_Rhadib1u 'ch_Rhadib2ll chi_Rhapers2ll 'n_Uberf llosgi_Be 'ch_Rhadib2ll hunan_Rhaatb ar_Arsym tân_Egu neu_Cyscyd rhywbeth_Rhaamh fel_Cyscyd 'na_Adf [-]_Gwann wel_Ebych yr_YFB un_Bancynnar peth_Egu mae_Bpres3u 'r_YFB haul_Egu yn_Arsym '_Atddyf neud_Be ._Atdt 
Ac_Cyscyd mae_Bpres3u 'n_Uberf gallu_Be achosi_Be difrod_Egu ofnadw_Anscadu '_Atddyf i_Arsym 'r_YFB haene_Ebll '_Atddyf o_Arsym dan_Egu y_YFB croen_Egu ych_Bpres2ll chi_Rhapers2ll 'n_Uberf gallu_Be gweld_Be ._Atdt 
Nid_Uneg dim_Egu ond_Arsym bod_Be chi_Rhapers2ll 'n_Uberf cael_Be llosg_Anscadu ond_Arsym mae_Bpres3u 'n_Uberf mynd_Be mwy_Rhaamh twfn_unk mewn_Arsym iddo_Ar3gu fe_Rhapers3gu ._Atdt 
S4_Anon Achos_Cyscyd pan_Egu y_YFB 'n_Arsym ni_Rhapers1ll ar_Arsym ein_Banmedd1ll gwylie_Ebll '_Atddyf falle_Adf '_Atddyf y_YFB 'n_Arsym ni_Rhapers1ll 'n_Utra ofalus_Anscadu ._Atdt 
Achos_Cyscyd y_YFB 'n_Arsym ni_Rhapers1ll 'n_Uberf meddwl_Be "_Atddyf y_YFB 'n_Arsym ni_Rhapers1ll ar_Arsym ein_Banmedd1ll gwylie_Ebll a_Cyscyd mae_Bpres3u e_Rhapers3gu '_Atddyf 'n_Utra dwym_Anscadu "_Atddyf ._Atdt 
A_Cyscyd ti_Rhapers2u 'n_Uberf clywed_Be pobl_Ebu yn_Arsym dweud_Be "_Atddyf o_Rhapers3gu '_Atddyf s_Gwllyth 'm_Rhadib1u o_Arsym 'r_YFB haul_Egu 'r_YFB un_Bancynnar peth_Banmeint fan_Ebu hyn_Rhadangd "_Atddyf ._Atdt 
Wel_Ebych yr_YFB un_Bancynnar haul_Egu yw_Bpres3u e_Rhapers3gu '_Atddyf [-]_Gwann ._Atdt 
S11_Anon Yr_YFB un_Bancynnar haul_Egu yw_Bpres3u e_Rhapers3gu '_Atddyf bois_Egll bach_Anscadu <_Atdchw Aneglur_Anscadu 1_Gwdig >_Atdde ._Atdt 
A_Cyscyd mae_Bpres3u 'n_Uberf gallu_Be '_Atddyf neud_Be [-]_Gwann a_Cyscyd mae_Bpres3u hwnna_Rhadangg 'n_Utra bwynt_Egu pwysig_Anscadu iawn_Adf ._Atdt 
Y_YFB 'n_Arsym ni_Rhapers1ll yn_Arsym dueddol_Anscadu o_Arsym anghofio_Be ._Atdt 
Ydyn_Bpres3ll ni_Rhapers1ll 'n_Utra dda_Anscadu iawn_Adf am_Arsym gofio_Be dodi_Be fe_Rhapers3gu arno_Ar3gu pan_Egu y_YFB 'n_Arsym ni_Rhapers1ll tramor_Anscadu ._Atdt 
Ond_Arsym '_Atddyf dan_Bpres1ll ni_Rhapers1ll ddim_Adf [-]_Gwann "_Atddyf o_Arsym gartre_Egu '_Atddyf '_Atddyf s_Gwllyth '_Atddyf im_unk eisie_Egu fe_Rhapers3gu "_Atddyf ._Atdt 
Oes_Bpres3amhen ma_Bpres3u '_Atddyf eisie_Egu fe_Rhapers3gu ._Atdt 
S1_Anon A_Cyscyd beth_Rhagof yw_Bpres3u 'r_YFB gwahaniaeth_Egu rhwng_Arsym d'wedwch_Bgorch2ll ffactor_Egu pymtheg_Rhifold a_Cyscyd ffactor_Egu trideg_Rhifold o_Arsym ran_Ebu siw_Egu {_Atdchw \_Atdcys }_Atdde d_Gwllyth mae_Bpres3u e_Rhapers3gu '_Atddyf 'n_Arsym eich_Rhadib2ll helpu_Be chi_Rhapers2ll i_Arsym osgoi_Be 'r_YFB haul_Egu [-]_Gwann neu_Cyscyd 'r_YFB amser_Egu ?_Atdt 
S11_Anon Yr_YFB amser_Egu ._Atdt 
Wel_Ebych ffactor_Egu un_Rhaamh deg_Rhifol pump_Rhifol yyy_Ebych enghraifft_Ebu [-]_Gwann mae_Bpres3u 'n_Uberf golygu_Be bod_Be chi_Rhapers2ll 'n_Uberf gallu_Be sefyll_Be pymtheg_Rhifold gwaith_Ebu yn_Arsym hirach_Anscym yn_Arsym yr_YFB haul_Egu cyn_Arsym llosgi_Be ._Atdt 
So_Adf ffactor_Egu trideg_Rhifold [-]_Gwann trideg_Rhifold gwaith_Ebu yn_Arsym hirach_Anscym yn_Arsym yr_YFB haul_Egu cyn_Arsym bod_Be chi_Rhapers2ll 'n_Uberf llosgi_Be ._Atdt 
Ond_Arsym byddwch_Bdyf2ll yn_Arsym ofalus_Anscadu [-]_Gwann chi_Rhapers2ll 'n_Arsym '_Atddyf bod_Be [-]_Gwann yn_Arsym enwedig_Anscadu gyda_Arsym plant_Egll a_Cyscyd babis_Egu bach_Anscadu ._Atdt 
Yn_Arsym y_YFB gwres_Egu hyn_Rhadangd '_Atddyf dyn_Bpres3ll nhw_Rhapers3ll ddim_Adf yn_Arsym ymdopi_Be yn_Arsym dda_Anscadu iawn_Adf ._Atdt 
Ma_Bpres3u '_Atddyf nhw_Rhapers3ll 'n_Uberf blino_Be ._Atdt 
Gwnewch_Bgorch2ll yn_Arsym siŵr_Anscadu bod_Be nhw_Rhapers3ll 'n_Uberf yfed_Be digon_Egu o_Arsym ddŵr_Egu ._Atdt 
A_Cyscyd ninnau_Rhacys1ll hefyd_Adf ._Atdt 
Y_YFB 'n_Arsym ni_Rhapers1ll 'n_Utra dueddol_Anscadu o_Arsym anghofio_Be ._Atdt 
Mae_Bpres3u 'n_Utra rhaid_Egu i_Arsym ni_Rhapers1ll gadw_Be lefel_Ebu y_YFB dŵr_Egu yn_Arsym y_YFB corff_Egu lan_Adf ._Atdt 
Achos_Cyscyd 'na_Adf fe_Rhapers3gu [-]_Gwann ma_Bpres3u '_Atddyf pobl_Ebu yn_Arsym deud_Be "_Atddyf ma_Bpres3u '_Atddyf pen_Egu tost_Anscadu hefo_Arsym 'r_YFB haul_Egu "_Atddyf ._Atdt 
Be_Rhagof '_Atddyf sy_Bpres3perth 'di_Uberf digwydd_Be '_Atddyf dach_Bpres2ll chi_Rhapers2ll 'di_Uberf colli_Be gormod_Egu o_Arsym ddŵr_Egu mas_Adf o_Arsym 'r_YFB corff_Egu ._Atdt 
Chi_Rhapers2ll 'n_Uberf mynd_Be yn_Arsym dehydrated_Gwest ._Atdt 
So_Adf mae_Bpres3u 'n_Utra bwysig_Anscadu i_Arsym gadw_Be hwnna_Rhadangg lan_Adf ._Atdt 
2522.987_Gwdig 