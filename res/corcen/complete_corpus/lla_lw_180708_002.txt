S1_Anon Yn_Arsym enw_Egu 'r_YFB tad_Egu a_Cyscyd 'r_YFB mab_Egu a_Arsym 'r_YFB Ysbryd_Egu Glan_Anscadu ._Atdt 
Amen_Ep ._Atdt 
<_Atdchw SS_Gwest >_Atdde Amen_Ep ._Atdt 
S1_Anon '_Atddyf Newch_Bgorch2ll chi_Rhapers2ll '_Atddyf ista_Be os_Egu gwelwch_Bgorch2ll yn_Arsym dda_Anscadu [_Atdchw Cynulleidfa_Ebu yn_Arsym eistedd_Be ]_Atdde ._Atdt 
Ma_Bpres3u '_Atddyf hi_Rhapers3bu 'di_Uberf dod_Be yn_Arsym dymor_Egu gwylia_Bgorch2u ._Atdt 
Mae_Bpres3u hefyd_Adf wedi_Uberf dod_Be i_Arsym ni_Rhapers1ll '_Atddyf Mhen_Egu Llyn_Ep tymor_Egu pererinion_Bgorff1ll ._Atdt 
ym_Ebych ._Atdt 
Fel_Cyscyd y_Rhaperth gwyddoch_Bpres2ll chi_Rhapers2ll '_Atddyf dan_Bpres1ll ni_Rhapers1ll 'n_Utra byw_Be ar_Arsym lwybr_Egu hynafol_Anscadu iawn_Adf sy_Bpres3perth 'n_Uberf arwain_Be at_Arsym ymm_Ebych [_Atdchw baban_Egu yn_Arsym ebychu_Be ]_Atdde ymm_Ebych Ynys_Ebu Enlli_Gwest ._Atdt 
Ac_Cyscyd mewn_Arsym unr'yw_Anscadu bererindod_Egbu ma_Bpres3u '_Atddyf 'na_Adf bedwar_Rhifol peth_Egu yn_Arsym dis-_Ublaen [=]_Gwann yn_Arsym [/=]_Gwann yn_Arsym digwydd_Be ._Atdt 
Yn_Arsym gynta_Rhitrefd '_Atddyf ma_Bpres3u '_Atddyf pobol_Ebu ne_Anscadu '_Atddyf ma_Bpres3u '_Atddyf pererinion_Bgorff1ll yn_Arsym paratoi_Be ._Atdt 
Yr_YFB ail_Rhitrefd ma_Bpres3u 'r_YFB pererinon_unk yn_Arsym teithio_Be ._Atdt 
Yn_Arsym drydydd_Rhitrefd mae_Bpres3u pererinion_Bgorff1ll yn_Arsym cyrra'dd_unk ._Atdt 
Ac_Cyscyd yn_Arsym bedwerydd_Rhitrefd ma_Bpres3u '_Atddyf pererinion_Bgorff1ll yn_Arsym mynd_Be adra_Adf '_Atddyf ._Atdt 
S1_Anon Pob_Banmeint pererindod_Egbu ._Atdt 
Dim_Adf ots_Egu [=]_Gwann ba_Adf [/=]_Gwann pa_Adf grefydd_Ebu ._Atdt 
'_Atddyf M'ots_Gwest lle_Anscadu '_Atddyf dach_Bpres2ll chi_Rhapers2ll 'n_Uberf mynd_Be ._Atdt 
Himalayas_Ep Ganges_Ep Jeriwsalem_Ep Mecca_Gwest ._Atdt 
Dim_Adf ots_Egu ._Atdt 
Ma_Bpres3u 'r_YFB pedwar_Rhifol yna_Adf yn_Arsym bwysig_Anscadu ym_Arsym mhob_Egll pererindod_Egbu ._Atdt 
A_Cyscyd -_Atdcys fy_Rhadib1u fyddai_Bamod3u 'n_Rhadib1ll lecio_unk meddwl_Be w'ithia_unk '_Atddyf bod_Be dod_Be i_Arsym eglwys_Ebu ar_Arsym fora_Egu dy_Rhadib2u '_Atddyf Sul_Egu 'di_Arsym '_Atddyf o_Arsym 'm_Rhadib1u yn_Arsym gweithio_Be os_Egu '_Atddyf dach_Bpres2ll chi_Rhapers2ll 'n_Uberf mynd_Be yn_Arsym p'nawn_Egu a_Arsym 'di_Arsym '_Atddyf o_Arsym 'm_Rhadib1u yn_Arsym gweithio_Be os_Egu '_Atddyf dach_Bpres2ll chi_Rhapers2ll 'n_Uberf mynd_Be ar_Arsym ddy_Rhadib2u '_Atddyf Mawrth_Egu ne_Anscadu '_Atddyf dy_Rhadib2u '_Atddyf Merchar_Gwest ._Atdt 
Rhaid_Egu '_Atddyf ddo_Rhapers3gu ddigwydd_Be ymm_Ebych bora_Bpres3u dy_Rhadib2u '_Atddyf Sul_Egu ._Atdt 
A_Cyscyd ma_Bpres3u '_Atddyf 'na_Adf r'wbath_Rhaamh sydd_Bpres3perth yn_Arsym allweddol_Anscadu bwysig_Anscadu efo_Arsym meddwl_Egu bo_Adf '_Atddyf dod_Be i_Arsym eglwys_Ebu ar_Arsym fora_Egu dy_Rhadib2u '_Atddyf Llun_Egu yr_YFB union_Anscadu '_Atddyf r'un_unk fath_Banhwyr a_Cyscyd pererindod_Egbu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde '_Atddyf Dan_Bpres1ll ni_Rhapers1ll 'n_Uberf paratoi_Be i_Arsym ddod_Be yma_Adf ._Atdt 
Y_YFB chi_Rhapers2ll ferched_Ebll efo_Arsym 'ch_Rhadib2ll lipstics_unk a_Cyscyd 'ch_Rhadib2ll hetia_Ebll crand_Anscadu ._Atdt 
ym_Ebych ._Atdt 
A_Cyscyd wedyn_Adf [-]_Gwann ._Atdt 
'Di_Arsym 'm_Rhadib1u 'di_Arsym cl'wad_unk ._Atdt 
ymm_Ebych ymm_Ebych ym_Ebych [=]_Gwann Ma_Bpres3u '_Atddyf [/=]_Gwann ma_Bpres3u 'r_YFB ymm_Ebych paratoi_Bamherff3u ._Atdt 
Wedyn_Adf ma_Bpres3u 'r_YFB daith_Ebu yma_Adf ._Atdt 
Wedyn_Adf ma_Bpres3u '_Atddyf 'na_Adf gyrra'dd_unk yn_Arsym digwydd_Be ._Atdt 
A_Cyscyd wedyn_Adf ma_Bpres3u '_Atddyf 'na_Adf mynd_Be adra_Adf '_Atddyf ._Atdt 
A_Cyscyd peth_Egu arall_Anscadu fydda_Bdyf1u '_Atddyf i_Rhapers1u 'n_Arsym lecio_unk am_Arsym y_YFB syniad_Egu yna_Adf ydi_Bpres3u pryd_Egu felly_Adf ma_Bpres3u '_Atddyf dy_Rhadib2u '_Atddyf Sul_Egu os_Cyscyd '_Atddyf dan_Bpres1ll ni_Rhapers1ll 'n_Uberf defnyddio_Be y_YFB thema_Ebu o_Arsym bererindod_Egbu pryd_Rhagof yn_Arsym y_YFB gwasanaeth_Egu ma_Bpres3u '_Atddyf dy_Rhadib2u '_Atddyf Sul_Egu yn_Arsym peidio_Be bod_Be diwrnod_Egu ola_Anscadu '_Atddyf yr_YFB wythnos_Ebu ac_Cyscyd yn_Arsym troi_Be i_Arsym fod_Be yn_Arsym dd'wrnod_unk cynta_Rhitrefd yr_YFB w'thnos_unk ._Atdt 
S1_Anon Oherwydd_Cyscyd ma_Bpres3u '_Atddyf 'na_Adf r'wbath_Rhaamh am_Arsym bererindota_Be sydd_Bpres3perth yn_Arsym newid_Be bywyd_Egu ne_Anscadu '_Atddyf ma_Bpres3u '_Atddyf 'na_Bandangd botensial_Egu o_Arsym newid_Be bywyd_Egu ._Atdt 
A_Cyscyd yr_YFB un_Rhaamh fath_Banhwyr yn_Arsym fam_Ebu 'ma_Adf ._Atdt 
'_Atddyf D_Gwllyth '_Atddyf a_Cyscyd 'n_Arsym ni_Rhapers1ll 'm_Rhadib1u o_Rhapers3gu 'ma_Adf yr_YFB un_Rhaamh fath_Banhwyr o_Arsym bobol_Ebu ag_Arsym o'ddan_unk ni_Rhapers1ll pan_Egu ddo'thon_Bgorff1ll ni_Rhapers1ll yma_Adf ._Atdt 
Felly_Adf pryd_Egu i_Rhapers1u chi_Rhapers2ll ma_Bpres3u '_Atddyf dy_Rhadib2u '_Atddyf Sul_Egu yn_Arsym troi_Be o_Arsym fod_Be y_YFB d'wrnod_unk ola_Anscadu 'r_YFB wythnos_Ebu i_Arsym fod_Be yn_Arsym dd'wrnod_unk cynta_Rhitrefd 'r_YFB wythnos_Ebu ._Atdt 
Ydi_Bpres3u o_Rhapers3gu pan_Egu '_Atddyf dach_Bpres2ll chi_Rhapers2ll 'n_Uberf dod_Be trw_Arsym 'r_YFB drws_Egu ?_Atdt 
Ydi_Bpres3u o_Rhapers3gu pan_Egu '_Atddyf dach_Bpres2ll chi_Rhapers2ll 'n_Uberf cyffesu_Be [saib]_Gwann ac_Cyscyd yn_Arsym ca'l_Ebu dechreuad_Egu newydd_Anscadu ?_Atdt 
Ydi_Bpres3u o_Arsym wrth_Egu i_Arsym ni_Rhapers1ll wrando_Be ar_Arsym air_Egu Duw_Egu ?_Atdt 
Ydi_Bpres3u o_Arsym wrth_Egu i_Arsym ni_Rhapers1ll weddio_Be ?_Atdt 
Ydi_Bpres3u o_Arsym wrth_Egu i_Arsym ni_Rhapers1ll dderbyn_Be y_YFB cymun_Egu ?_Atdt 
Ydi_Bpres3u o_Rhapers3gu yn_Arsym ystod_Ebu y_YFB fendith_Ebu ?_Atdt 
'_Atddyf Ta_Bpres3u ydi_Bpres3u o_Rhapers3gu fel_Cyscyd '_Atddyf dach_Bpres2ll chi_Rhapers2ll 'n_Uberf mynd_Be allan_Adf o_Arsym 'r_YFB eglwys_Ebu ?_Atdt 
S1_Anon '_Atddyf Dw_Bpres1u 'i_Rhadib3bu 'm_Adf yn_Arsym mynd_Be i_Arsym atab_Egu y_YFB cwestiwn_Egu ._Atdt 
Ond_Arsym os_Egu ydi_Bpres3u 'r_YFB y_YFB newid_Be yna_Adf o_Arsym ddy_Rhadib2u '_Atddyf Sul_Egu d'wrnod_unk d'wytha_Anscadu '_Atddyf i_Arsym ddy_Rhadib2u '_Atddyf Sul_Egu d'wrnod_unk cynta_Rhitrefd '_Atddyf ma_Bpres3u '_Atddyf na_Bandangd bosibilrwydd_Egu ne_Anscadu '_Atddyf ma_Bpres3u '_Atddyf 'na_Adf -_Atdcys bot_Egu potensial_Egu i_Arsym hynny_Rhadangd ddigwydd_Be wrth_Arsym i_Arsym ni_Rhapers1ll addoli_Be hefo_Arsym 'n_Utra gilydd_Egu ._Atdt 
I_Rhapers1u mi_Uberf a_Cyscyd '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Arsym lecio_unk chwara_Egu '_Atddyf efo_Arsym 'r_YFB syniad_Egu yma_Adf mae_Bpres3u o'n_Bamherff1u syniad_Egu pwysig_Anscadu i_Rhapers1u mi_Rhapers1u wrth_Arsym i_Arsym ni_Rhapers1ll gyd-_Ublaen adrodd_Be yr_YFB <_Atdchw la_Egbu >_Atdde Agnus_Gwest De_Egu '_Atddyf <_Atdchw /_Gwsym la_Egbu >_Atdde ._Atdt 
"_Atddyf Oen_Egu Duw_Egu sy_Bpres3perth 'n_Utra dwyn_Be ymaith_Adf bechodau_Egll 'r_YFB byd_Egu ._Atdt 
Trugarha_Bgorch2u wrthym_Ar1ll ._Atdt "_Atddyf <_Atdchw la_Egbu >_Atdde Et_Gwest cetera_unk <_Atdchw /_Gwsym la_Egbu >_Atdde ._Atdt 
S1_Anon Oherwydd_Cyscyd fydda_Bdyf1u 'i_Rhadib3bu 'n_Uberf cofio_Be ar_Arsym gychwyn_Egu un_Rhaamh o_Arsym 'r_YFB efengyla_Bgorch2u ._Atdt 
Marc_Egu a_Cyscyd Ioan_Epg ._Atdt 
Ma_Bpres3u '_Atddyf Ioan_Ep Fedyddiwr_Gwest yn_Arsym sbio_Be ar_Arsym Iesu_Epg a_Cyscyd mae_Bpres3u o'n_Bamherff1u d'eud_Be "_Atddyf Wele_Bamherff3u oen_Egu Duw_Egu ._Atdt 
Wele_Bamherff3u oen_Egu Duw_Egu "_Atddyf ._Atdt 
Ag_Arsym w_Ebych 'th_Rhadib2u godi_Bpres2u y_YFB bara_Egu '_Atddyf dw_Bpres1u 'i_Rhadib3bu hefyd_Adf yn_Arsym r_Gwllyth 'w_Rhadib3gu feddwl_Be mmm_Ebych '_Atddyf dw'inna_unk '_Atddyf hefyd_Adf yn_Arsym medru_Be gweld_Be oen_Egu Duw_Egu ._Atdt 
Felly_Adf r_Gwllyth 'w_Rhadib3gu feddwl_Be bach_Egu pause_Gwest for_Gwest thought_Gwest ._Atdt 
R_Gwllyth 'w_Rhadib3gu feddwl_Be bach_Egu ar_Arsym ymm_Ebych bwysigrwydd_Egu pererindod_Egbu a_Cyscyd pwysigrwydd_Egu gweld_Be dod_Be i_Arsym 'r_YFB eglwys_Ebu ar_Arsym fora_Egu dy_Rhadib2u '_Atddyf Sul_Egu yn_Arsym [_Atdchw baban_Egu yn_Arsym ebychu_Be ]_Atdde bererindod_Egbu ynddi_Ar3bu 'i_Rhadib3bu hun_Rhaatb ._Atdt 
Gweddiwn_Bamherff1u [_Atdchw Cynulleidfa_Ebu yn_Arsym sefyll_Be ]_Atdde ._Atdt 
257.95_Gwdig 