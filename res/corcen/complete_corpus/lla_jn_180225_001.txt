0.0_Gwdig 
S1_Anon Mae_Bpres3u 'i_Rhadib3gu gofgolofn_Ebu o_Rhapers3gu ar_Arsym stryd_Ebu fawr_Anscadu lleoliad_Anon a_Cyscyd mae_Bpres3u o_Rhapers3gu hefyd_Adf wedi_Arsym ca'l_Ebu 'i_Rhadib3gu gladdu_Be ym_Arsym mynwent_Ebu [aneglur]_Gwann ._Atdt 
Mi_Uberf gafodd_Bgorff3u o_Arsym 'i_Rhadib3gu eni_Be yma_Adf yn_Arsym y_YFB flwyddyn_Ebu un_Rhaamh wyth_Rhifol pump_Rhifol naw_Rhifol ._Atdt 
Mi_Uberf gafodd_Bgorff3u o_Arsym 'i_Rhadib3bu '_Atddyf neud_Be yn_Arsym aelod_Egu seneddol_Anscadu yn_Arsym yn_Arsym y_YFB flwyddyn_Ebu un_Rhaamh wyth_Rhifol wyth_Rhifol chwech_Rhifol efo_Arsym 'r_YFB blaid_Ebu rhydfrydol_unk lly_Bgorch2u yn_Arsym lleoliad_Anon ._Atdt 
S2_Anon Felly_Adf mae_Bpres3u e_Rhapers3gu 'n_Arsym d'imlad_unk braf_Anscadu ?_Atdt 
S1_Anon [=]_Gwann Yndi_Bpres3u yndi_Bpres3u [/=]_Gwann 
S2_Anon Mae_Bpres3u 'n_Utra siŵr_Anscadu [=]_Gwann i_Rhapers1u i_Arsym [/=]_Gwann rannu_Be 'r_YFB cartre_Egu '_Atddyf gyda_Arsym rywun_Egu fel_Cyscyd fe_Rhapers3gu ._Atdt 
Mae_Bpres3u 'n_Utra neis_Anscadu bod_Be [aneglur]_Gwann yn_Arsym le_Egu bendigedig_Anscadu a_Cyscyd ni_Rhapers1ll 'di_Uberf dod_Be ar_Arsym ddi'rnod_unk anhygoel_Anscadu fan_Ebu hyn_Rhadangd heddi_Adf '_Atddyf ._Atdt 
S1_Anon [=]_Gwann Yndi_Bpres3u yndi_Bpres3u [/=]_Gwann dy_Rhadib2u 'ch_Rhadib2ll chi_Rhapers2ll 'di_Uberf bod_Be yn_Arsym ffodus_Anscadu iawn_Adf ag_Arsym o_Arsym ddod_Be yma_Adf heddiw_Adf dw_Bpres1u i_Rhapers1u 'm_Adf yn_Arsym meddwl_Egu w'rach_Ebu dydan_Bpres1ll ni_Rhapers1ll ddim_Adf yn_Arsym yyy_Ebych gwerthfawrogi_Be neu_Cyscyd yn_Arsym ti'mod_Ebych yn_Arsym sylweddoli_Be be_Rhagof '_Atddyf sy_Bpres3perth 'na_Adf on_Gwest '_Atddyf e_Rhapers3gu cwmpas_Egu ni_Rhapers1ll ._Atdt 
Ma_Bpres3u 'r_YFB ardal_Ebu yma_Adf [=]_Gwann yn_Arsym yn_Arsym [/=]_Gwann ardal_Ebu diwylliedig_Anscadu iawn_Adf yn_Arsym ardal_Ebu y_YFB pethe_Egll '_Atddyf ma_Bpres3u 'n_Rhadib1ll 'na_Bandangd ddigon_Egu o_Arsym gyfle_Egu yma_Adf [=]_Gwann ma_Bpres3u '_Atddyf ma_Bpres3u '_Atddyf [/=]_Gwann lleoliad_Anon yn_Arsym werth_Egu ymweld_Be â_Arsym a_Arsym lleoliad_Anon ._Atdt 
[_Atdchw Cerddoriaeth_Ebu yn_Arsym chwarae_Egu ]_Atdde 
S2_Anon Gyda_Arsym golygfeydd_Ebll godidog_Anscadu i_Rhapers1u ca'l_Ebu wrth_Arsym edrych_Egu i_Arsym lawr_Egu tua_Arsym 'r_YFB dref_Ebu mae_Bpres3u 'n_Utra hawdd_Anscadu iawn_Adf gweld_Be pam_Adf fod_Be cym'ent_unk o_Arsym bobol_Ebu yn_Arsym ymweld_Be â_Arsym 'r_YFB ardal_Ebu hyfryd_Anscadu yma_Adf a_Arsym 'r_YFB un_Bancynnar atyniad_Egu mwya_Anseith '_Atddyf ydy_Bpres3u lleoliad_Anon ._Atdt 
[_Atdchw Cerddoriaeth_Ebu yn_Arsym chwarae_Egu ]_Atdde ._Atdt 
S3_Anon Mae_Bpres3u 'n_Rhadib1ll 'na_Adf cymaint_Anscadu o_Arsym bobol_Ebu 'di_Uberf cael_Be 'u_Rhadib3ll denu_Be at_Arsym lleoliad_Anon dros_Arsym y_YFB blynydde_Ebll '_Atddyf ._Atdt 
A_Cyscyd 'r_YFB wythnos_Ebu yma_Adf dw_Bpres1u i_Rhapers1u 'n_Uberf mynd_Be ar_Arsym 'u_Banmedd3ll trywydd_Egu nhw_Rhapers3ll ._Atdt 
Rwy_Bpres1u 'n_Uberf dechrau_Be fan_Ebu hyn_Rhadangd yn_Arsym lleoliad_Anon cyn_Arsym mynd_Be lan_Adf tua_Arsym 'r_YFB gogledd_Egu am_Arsym yr_YFB [aneglur]_Gwann fawr_Anscadu yn_Arsym ôl_Egu trwy_Arsym 'r_YFB lleoliad_Anon a_Arsym diweddu_Be lan_Adf ar_Arsym ochor_Ebu arall_Anscadu y_YFB llyn_Egu ._Atdt 
Ma_Bpres3u '_Atddyf lleoliad_Anon y_YFB llyn_Egu naturiol_Anscadu mwyaf_Anseith yng_Arsym lleoliad_Anon yn_Arsym denu_Be miloedd_Ebll bob_Anscadu blwyddyn_Ebu o_Rhapers3gu _lleoliad__Anon o_Rhapers3gu __lleoliad___Anon a_Cyscyd '_Atddyf falle_Adf jyst_Adf '_Atddyf falle_Adf yn_Arsym bellach_Adf na_Adf ni_Rhapers1ll 'n_Uberf disgwyl_Be ._Atdt 
Ar_Arsym noson_Ebu oer_Anscadu o_Arsym Ionawr_Egu yn_Arsym un_Rhaamh naw_Rhifol saith_Rhifold pedwar_Rhifol tra_Cyscyd bod_Be teuluoedd_Egll lleoliad_Anon yn_Arsym setlo_Be lawr_Adf o_Rhapers3gu fla_unk 'n_Utra y_YFB teledu_Egu clywson_Bgorff3ll nhw_Rhapers3ll ffrwydriad_Egu anferthol_Anscadu a_Cyscyd crynnu_Be dan_Arsym 'u_Banmedd3ll traed_Egbu ._Atdt 
Rhuthron_Bgorff1ll nhw_Rhapers3ll at_Arsym y_YFB ffensestri_unk a_Cyscyd ma_Bpres3u '_Atddyf rhai_Rhaamh 'di_Uberf dweud_Be bod_Be nhw_Rhapers3ll 'di_Uberf gweld_Be goleuade_Egll '_Atddyf oren_Egbu yn_Arsym disgyn_Be i_Arsym 'r_YFB dwyrain_Egu ._Atdt 
Cafodd_Bgorff3u awyrenau_unk 'u_Rhadib3ll [aneglur]_Gwann o_Rhapers3gu lleoliad_Anon i_Arsym edrych_Egu am_Arsym olion_Egll rhywbeth_Egu yr_YFB esboniad_Egu swyddogol_Anscadu yw_Bpres3u bod_Be dim_Banmeint byd_Egu wedi_Uberf ei_Rhadib3gu ddarganfod_Be yn_Arsym answyddogol_Anscadu mae_Bpres3u pobol_Ebu ar_Arsym y_YFB we_Egbu yn_Arsym dweud_Be nhw_Rhapers3ll 'di_Uberf gweld_Be darnau_Egll o_Rhapers3gu llong_Ebu ofod_Egu a_Cyscyd chyrff_Egll yn_Arsym cael_Be eu_Rhadib3ll cuddio_Be gan_Arsym y_YFB fyddin_Ebu yng_Arsym nghanol_Egu y_YFB nos_Ebu ._Atdt 
Na_Adf 'th_Banmedd2u yr_YFB un_Bancynnar peth_Egu digwydd_Be yn_Arsym lleoliad_Anon yn_Arsym un_Rhaamh naw_Rhifol pedwar_Rhifol saith_Rhifold wel_Ebych chi_Rhapers2ll 'di_Uberf clywed_Be am_Arsym area_Gwest fifty_Gwest one_Gwest ife_Bpres3u hwn_Rhadangg yw_Bpres3u area_Gwest fifty_Gwest wa_Gwest ?_Atdt 
Mae_Bpres3u 'n_Rhadib1ll 'na_Bandangd ffyrdd_Egll o_Arsym esbonio_Be hyn_Rhadangd i_Arsym gyd_Egu o'dd_Bamherff3u y_YFB goleuade_Egll '_Atddyf medden_Bgorch3ll nhw_Rhapers3ll naill_Anscadu ai_Cyscyd yn_Arsym rhan_Ebu o_Arsym ymgyrch_Ebu filwrol_Anscadu neu_Cyscyd '_Atddyf falle_Adf yn_Arsym gawod_Ebu o_Arsym sêr_Ebll wib_Egbu gafodd_Bgorff3u eu_Rhadib3ll gweld_Be dros_Arsym lleoliad_Anon y_YFB noson_Ebu honno_Rhadangb ._Atdt 
[_Atdchw Diwedd_Egu y_YFB clip_Egu gyntaf_Anscadu ]_Atdde ._Atdt 
[_Atdchw Cerddoriaeth_Ebu yn_Arsym chwarae_Egu ]_Atdde ._Atdt 
S4_Anon Dyma_Adf hi_Rhapers3bu y_YFB falwen_unk lyslafeddog_unk ._Atdt 
S5_Anon Wel_Ebych dw_Bpres1u i_Rhapers1u 'n_Uberf gw'bod_Be o_Arsym astudio_Be archeoleg_unk yyy_Ebych yn_Arsym gw'bod_Be os_Egu ti_Rhapers2u 'n_Uberf edrych_Be ar_Arsym cregyn_Ebll malwod_Egll a_Cyscyd ma_Bpres3u '_Atddyf [-]_Gwann ma_Bpres3u 'n_Arsym nhw_Rhapers3ll 'n_Utra dueddol_Anscadu o_Arsym aros_Be mewn_Arsym un_Bancynnar cynefin_Anscadu arbennig_Anscadu ac_Cyscyd wrth_Arsym edrych_Egu ar_Arsym 'u_Banmedd3ll cregyn_Ebll nhw_Rhapers3ll wyt_Bpres2u ti_Rhapers2u 'n_Arsym gywbod_unk pa_Adf fath_Egu o_Arsym gynefinoedd_Egll o'dd_Bamherff3u yn_Arsym arfer_Egu bod_Be yna_Adf yn_Arsym y_YFB gorffennol_Egu ._Atdt 
Bydde_Bamod3u fe_Rhapers3gu 'n_Utra bosib_Anscadu '_Atddyf neud_Be rh'wbeth_unk y_YFB [-]_Gwann yr_YFB un_Bancynnar peth_Egu gyda_Arsym rhein_Rhadangll ?_Atdt 
S4_Anon Wel_Ebych be_Rhagof '_Atddyf sy_Bpres3perth 'n_Utra dipyn_Egu yn_Arsym wahanol_Anscadu hefo_Arsym rhein_Rhadangll ydy_Bpres3u na_Bandangd mantell_Egbu sy_Bpres3perth genon_unk nhw_Rhapers3ll sy_Bpres3perth 'n_Uberf dod_Be drost_Arsym y_YFB falwen_unk fel_Cyscyd da_Anscadu 'ch_Rhadib2ll chi_Rhapers2ll 'n_Uberf gweld_Be o_Arsym 'r_YFB lliwie_Egll ._Atdt 
Lliwie_Egll '_Atddyf brown_Anscadu lliwie_Egll '_Atddyf gwyrdd_Anscadu a_Cyscyd fel_Cyscyd specio_unk aur_Egu ar_Arsym 'i_Rhadib3bu hyd_Arsym o_Rhapers3gu a_Arsym mae_Bpres3u 'n_Utra siŵr_Anscadu i_Arsym bobol_Ebu 'ma_Adf sydd_Bpres3perth yn_Arsym astudio_Be nhw_Rhapers3ll bod_Be [-]_Gwann bod_Be hi_Rhapers3bu 'n_Arsym falwen_unk hardd_Anscadu iawn_Adf '_Atddyf lly_Bgorch2u ._Atdt 
Be_Rhagof '_Atddyf roe_Gwest '_Atddyf 'na_Bandangd sôn_Egu o_Arsym ran_Ebu diogelwch_Egu mae_Bpres3u siŵr_Anscadu bod_Be y_YFB fantell_Egbu 'ma_Adf sydd_Bpres3perth fel_Cyscyd jeli_Egu i_Arsym rywbeth_Egu sy_Bpres3perth 'n_Uberf trio_Be i_Rhapers1u f'yta_Be fo_Rhapers3gu bydde_Bamod3u o'n_Bamherff1u anodd_Anscadu iawn_Adf i_Rhapers1u ga'l_Ebu gafael_Ebu arno_Ar3gu fo_Adf '_Atddyf lly_Bgorch2u ._Atdt 
Mae_Bpres3u 'n_Rhadib1ll falwen_unk eithriadol_Anscadu o_Arsym brin_Anscadu a_Cyscyd mae_Bpres3u ar_Arsym restr_Ebu o_Rhapers3gu rywogaethe_Ebll '_Atddyf sy_Bpres3perth 'n_Utra bosib_Anscadu iddon_unk nhw_Rhapers3ll farw_Anscadu allan_Adf un_Rhaamh deg_Rhifol wyth_Rhifol o_Rhapers3gu rywogaethe_Ebll '_Atddyf sydd_Bpres3perth yn_Arsym bosib_Anscadu i_Arsym ni_Rhapers1ll golli_Be nhw_Rhapers3ll ._Atdt 
S5_Anon A_Cyscyd mae_Bpres3u mor_Adf fach_Egu s'im_Egbu ryfedd_Anscadu 'i_Rhadib3bu bod_Be wedi_Uberf bod_Be ar_Arsym goll_Egu am_Arsym hanner_Egu gan_Arsym '_Atddyf mlynedd_Ebll ._Atdt 
S4_Anon Yn_Arsym hollol_Anscadu ._Atdt 
[saib]_Gwann [_Atdchw Cerddoriaeth_Ebu yn_Arsym chwarae_Egu ]_Atdde ._Atdt 
S2_Anon Ry_Bpres3u 'n_Arsym ni_Rhapers1ll 'n_Uberf crwydro_Be lleoliad_Anon ardal_Ebu amaethyddol_Anscadu plygeiniol_unk sy_Bpres3perth 'n_Uberf deffro_Be 'n_Utra gynnar_Anscadu yn_Arsym y_YFB bore_Egu ._Atdt 
A_Cyscyd digon_Rhaamh posib_Anscadu bod_Be y_YFB diwydrwydd_Egu yma_Adf yn_Arsym un_Rhaamh o_Arsym 'r_YFB rhesyme_Egll '_Atddyf pam_Adf fod_Be rhai_Banmeint o_Rhapers3gu gymeriade_Egu 'r_YFB fro_Ebu wedi_Uberf llwyddo_Be i_Rhapers1u '_Atddyf neud_Be 'u_Banmedd3ll marc_Egu ym_Ebych mhell_Anscadu iawn_Adf o_Rhapers3gu adre_Adf '_Atddyf ._Atdt 
S3_Anon Wy_Bpres1u bellach_Adf yn_Arsym lleoliad_Anon i_Arsym 'r_YFB gogledd_Egu orllewin_Egu o_Rhapers3gu lleoliad_Anon ar_Arsym drywydd_Egu un_Rhaamh o_Arsym 'r_YFB enwogion_Egll mwya_Anseith '_Atddyf sydd_Bpres3perth gan_Arsym _lleoliad__Anon i_Arsym gynnig_Egu [saib]_Gwann Ca'l_Ebu 'u_Rhadib3ll denu_Be yma_Adf ma_Bpres3u '_Atddyf cymaint_Anscadu o_Rhapers3gu gymeriade_Egu '_Atddyf ond_Arsym gad'el_Be na_Adf 'th_Banmedd2u hi_Rhapers3bu i_Arsym deithio_Be 'r_YFB byd_Egu ._Atdt 
Trwy_Arsym hyn_Rhadangd i_Arsym gyd_Egu anghofiodd_Bgorff3u hi_Rhapers3bu byth_Adf o_Arsym ble_Egbu o'dd_Bamherff3u hi_Rhapers3bu 'n_Uberf dod_Be ._Atdt 
Yma_Adf yn_Arsym un_Bancynnar saith_Rhifold wyth_Rhifol naw_Rhifol cafodd_Bgorff3u enwb_Anon cyfenw_Anon 'i_Rhadib3gu geni_Bpres2u ._Atdt 
Ni_Rhapers1ll 'n_Utra gyfarwydd_Anscadu iawn_Adf dy_Rhadib2u 'i_Rhadib3bu henw_Egu erbyn_Arsym hyn_Rhadangd mae_Bpres3u i_Arsym weld_Be yn_Arsym pob_Egll ysbyty_Egu ar_Arsym draws_Arsym gogledd_Egu lleoliad_Anon ond_Arsym yma_Adf yn_Arsym lleoliad_Anon yw_Bpres3u lle_Egu dechreuodd_Bgorff3u popeth_Egu iddi_Ar3bu [saib]_Gwann 
S3_Anon Ar_Arsym ôl_Anscadu cweryla_Be '_Atddyf da_Anscadu 'i_Rhadib3bu theulu_Egu a_Cyscyd o'dd_Bamherff3u e_Rhapers3gu 'n_Uberf digwydd_Be lot_Ebu lawr_Adf fan_Ebu 'na_Adf yn_Arsym lleoliad_Anon o'dd_Bamherff3u enwb_Anon yn_Arsym dianc_Be lan_Adf fan_Ebu hyn_Rhadangd i_Rhapers1u _lleoliad__Anon [saib]_Gwann Dyma_Adf 'r_YFB ogofau_Ebll oedd_Bamherff3u yn_Arsym bwydo_Be dychymg_unk y_YFB ferch_Ebu ifanc_Anscadu o'dd_Bamherff3u yn_Arsym hoff_Anscadu o_Rhapers3gu gerddori'eth_unk a_Cyscyd dawnsio_Be ei_Rhadib3bu meddwl_Be wastad_Adf yn_Arsym bell_Anscadu o_Arsym 'i_Rhadib3bu gwaith_Ebu ._Atdt 
Fan_Ebu hyn_Rhadangd dechreuodd_Bgorff3u enwb_Anon feddwl_Be am_Arsym y_YFB tro_Egu cyntaf_Anscadu am_Arsym diroedd_Egll yyy'ill_unk ._Atdt 
A_Cyscyd 'r_YFB breuddwydion_Egbll 'ma_Adf byse_Bamherff3u 'n_Arsym 'i_Rhadib3bu harwain_Be hi_Rhapers3bu un_Bancynnar dydd_Egu tua_Arsym lleoliad_Anon lleoliad_Anon _lleoliad__Anon __lleoliad___Anon ___lleoliad____Anon a_Cyscyd ____lleoliad_____Anon ._Atdt 
[_Atdchw Diwedd_Egu yr_YFB ail_Rhitrefd glip_Egu ]_Atdde ._Atdt 
S5_Anon Ond_Arsym ma_Bpres3u 'n_Rhadib1ll 'na_Bandangd stori_Ebu amdano_Ar3gu fe_Rhapers3gu fel_Cyscyd tipyn_Egu o_Rhapers3gu [aneglur]_Gwann ._Atdt 
S6_Anon O_Rhapers3gu oedd_Bamherff3u [aneglur]_Gwann portiar_unk oe_unk '_Atddyf ._Atdt 
S5_Anon Pysgod_Egll a_Cyscyd ballu_Be oedd_Bamherff3u 'i_Rhadib3bu ia_Adf ?_Atdt 
S6_Anon Wel_Ebych ie_Adf [aneglur?]_Gwann hwrach_unk o'dd_Bamherff3u o'n_Bamherff1u portshio_unk [aneglur]_Gwann hefyd_Adf ._Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann 
S7_Anon Oedd_Bamherff3u enwg_Anon yn_Arsym sôn_Egu am_Gwtalf bortshio_unk yn_Arsym doedd_Bamherff3u ?_Atdt 
S5_Anon [=]_Gwann Ie_Adf ie_Adf [/=]_Gwann oedd_Bamherff3u ._Atdt 
S7_Anon ymm_Ebych wel_Ebych o'dd_Bamherff3u gyno_unk fo_Rhapers3gu look_Gwest out_Gwest o'dd_Bamherff3u yn_Arsym blismon_Egu lleol_Anscadu a_Cyscyd wedyn_Adf ryw_Egbu flash_Gwest bach_Anscadu iddo_Ar3gu fo_Adf wybod_Egu bo_Adf '_Atddyf 'na_Bandangd rywun_Egu o_Arsym gwmpas_Egu chi'mo_unk '_Atddyf ._Atdt 
S5_Anon [=]_Gwann Ie_Adf ie_Adf [/=]_Gwann ._Atdt 
S7_Anon Wel_Ebych bob_Banmeint hyn_Rhadangd a_Cyscyd hyn_Rhadangd o'dd_Bamherff3u y_YFB plismon_Egu yn_Arsym ca'l_Ebu [aneglur]_Gwann yn_Arsym doedd_Bamherff3u ?_Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann 
S5_Anon A_Rhaperth gafodd_Bgorff3u e_Rhapers3gu byth_Adf 'i_Rhadib3gu ddal_Be felly_Adf na_Rhaperth ?_Atdt 
S6_Anon Na_Uneg se_Bdibdyf1ll neb_Rhaamh yn_Arsym dal_Be enwg_Anon dwi_Bpres1u 'm_Rhadib1u yn_Arsym meddwl_Egu '_Atddyf lly_Bgorch2u ._Atdt 
S5_Anon Neb_Rhaamh wedi_Uberf 'i_Rhadib3gu ddal_Be e_Rhapers3gu ar_Arsym y_YFB motorbeic_unk yn_Arsym sicr_Anscadu ._Atdt 
S6_Anon Na_Uneg sy_Bpres3perth na_Uneg [aneglur?]_Gwann ._Atdt 
[_Atdchw Cerddoriaeth_Ebu yn_Arsym chwarae_Egu ]_Atdde ._Atdt 
S5_Anon yyy_Ebych mai_Egu motorbeics_unk a_Cyscyd barddoni_Egu o'dd_Bamherff3u y_YFB ddau_Rhifol beth_Egu mawr_Anscadu yn_Arsym bywyd_Egu enwg_Anon cyfenw_Anon o_Rhapers3gu _lleoliad__Anon dim_Egu ond_Arsym unwaith_Adf y_Rhadib1u daeth_Bgorff3u y_YFB ddau_Rhifol fyd_Egu ynghŷd_Adf ganddo_Ar3gu a_Arsym hynny_Rhadangd yn_Arsym y_YFB Saesneg_Ebu [_Atdchw Darllen_Be barddoniaeth_Ebu Saesneg_Ebu ]_Atdde oedd_Bamherff3u y_YFB ffordd_Ebu y_Rhadib1u disgrifiodd_Bgorff3u __enwg___Anon dra_Cyscyd 'th_Rhadib2u enwog_Anscadu y_YFB TT_Gwacr ar_Arsym ___lleoliad____Anon ._Atdt 
Ac_Cyscyd yyy_Ebych mwyn_Egu paratoi_Bamherff3u 'i_Rhadib3bu hun_Rhaatb ar_Arsym gyfer_Egu y_YFB TT_Gwacr roedd_Bamherff3u gan_Arsym enwg_Anon gwrs_Egu TT_Gwacr ei_Rhadib3bu hun_Rhaatb yma_Adf yn_Arsym lleoliad_Anon ac_Cyscyd erbyn_Arsym hyn_Rhadangd mae_Bpres3u 'na_Adf griw_Egu o_Rhapers3gu fotobeicwyr_unk lleol_Anscadu yn_Arsym dilyn_Be ôl_Egu teiars_Egll y_YFB fellten_Ebu goch_Anscadu [saib]_Gwann 
S5_Anon Arferai_Bamherff3u enwg_Anon cyfenw_Anon wibio_Be trwy_Arsym _lleoliad__Anon __lleoliad___Anon a_Cyscyd ___lleoliad____Anon ymlaen_Adf am_Arsym ____lleoliad_____Anon dros_Arsym y_YFB _____lleoliad______Anon ______lleoliad_______Anon _______lleoliad________Anon ________lleoliad_________Anon a_Arsym thros_Arsym y_YFB garnen_unk wen_Anscadbu i_Rhapers1u _________lleoliad__________Anon gan_Arsym ceisio_Be osgoi_Be pob_Egll plismon_Egu ._Atdt 
Yn_Arsym ffodus_Anscadu i_Rhapers1u mi_Rhapers1u dy_Rhadib2u 'n_Arsym ni_Rhapers1ll 'n_Arsym 'i_Rhadib3bu chymryd_Be hi_Rhapers3bu '_Atddyf chydig_Banmeint mwy_Rhaamh hamddenol_Anscadu na_Bandangd ro'dd_Ebu enwg_Anon cyfenw_Anon [saib]_Gwann ._Atdt 
S8_Anon Dy_Rhadib2u 'n_Arsym ni_Rhapers1ll 'n_Uberf mynd_Be yr_YFB un_Bancynnar cwrs_Anscadu ag_Arsym o'dd_Bamherff3u o'n_Bamherff1u mynd_Be i_Arsym destio_Be 'r_YFB beic_Egu allan_Adf de_Egu o_Arsym fynd_Be trw_Arsym '_Atddyf lleoliad_Anon lleoliad_Anon _lleoliad__Anon __lleoliad___Anon dod_Be nôl_Be i_Arsym fan_Egbu ma_Bpres3u '_Atddyf a_Cyscyd o'dd_Bamherff3u [-]_Gwann o'dd_Bamherff3u o'n_Bamherff1u debyg_Anscadu i_Rhapers1u ___lleoliad____Anon de_Egu [-]_Gwann gwrs_Egu tebyg_Anscadu i_Rhapers1u [-]_Gwann 
S5_Anon Dyna_Adf pam_Adf na_Adf 'th_Banmedd2u e_Gwllyth ddewis_Be e_Rhapers3gu felly_Adf ?_Atdt 
S8_Anon 'Na_Bandangd pam_Adf o'dd_Bamherff3u o_Rhapers3gu [aneglur]_Gwann ddewis_Be o_Rhapers3gu ._Atdt 
S5_Anon [=]_Gwann Ie_Adf ie_Adf ie_Adf [/=]_Gwann ac_Cyscyd ers_Arsym pryd_Egu ma_Bandangg 'i_Rhadib3bu 'di_Uberf bod_Be yn_Arsym mynd_Be i_Arsym 'r_YFB ralis_Ebll felly_Adf ?_Atdt 
S8_Anon yyy_Ebych dau_Rhifol ddeg_Rhifol un_Rhaamh o_Arsym flynyddoedd_Ebll ._Atdt 
S5_Anon Dau_Rhifol ddeg_Rhifol un_Rhaamh ?_Atdt 
Tipyn_Egu o_Arsym amser_Egu te_Bpres3u felly_Adf ?_Atdt 
Da_Anscadu 'ch_Rhadib2ll chi_Rhapers2ll 'n_Uberf cofio_Be enwg_Anon cyfenw_Anon a_Cyscyd i_Rhapers1u chi_Rhapers2ll wedi_Uberf cymryd_Be rhan_Ebu yn_Arsym y_YFB rali_Ebu yma_Adf hefyd_Adf ?_Atdt 
S8_Anon Ydw_Bpres1u ._Atdt 
S5_Anon Ag_Arsym o'dd_Bamherff3u hi_Rhapers3bu o'dd_Bamherff3u [-]_Gwann 
S9_Anon Motorbeics_Gwest [aneglur?]_Gwann 
S5_Anon Ie_Adf ._Atdt 
S9_Anon A_Cyscyd dyna_Adf be_Rhagof '_Atddyf o_Arsym ni_Rhapers1ll 'n_Arsym 'i_Rhadib3gu ddangos_Be y_YFB llyfr_Egu +_Gwsym 
S5_Anon Ie_Adf ._Atdt 
S9_Anon +_Gwsym yyy_Ebych wyth_Rhifol deg_Rhifol oed_Egu '_Atddyf a_Cyscyd 'r_YFB llyfr_Egu 'ma_Adf jest_Adf +_Gwsym 
S5_Anon O_Rhapers3gu reit_Ebych ie_Adf ._Atdt 
S9_Anon yyy_Ebych a_Cyscyd [saib]_Gwann beibl_Egu yyy_Ebych motobeicwyr_unk ._Atdt 
S5_Anon Reit_Ebych ._Atdt 
S9_Anon yyy_Ebych a_Cyscyd pwy_Rhagof sy_Bpres3perth yno_Adf fo_Rhapers3gu [saib]_Gwann ._Atdt 
[_Atdchw Diwedd_Egu y_YFB glip_Egu olaf_Anscadu ]_Atdde ._Atdt 
2867.989_Gwdig 