[_Atdchw Pobl_Ebu yn_Arsym siarad_Be ymysg_Arsym ei_Rhadib3gu gilydd_Egu yn_Arsym y_YFB cefndir_Egu ]_Atdde 
[_Atdchw Cerddoriaeth_Ebu yn_Arsym y_YFB cefndir_Egu tu_Egu allan_Adf i_Arsym 'r_YFB babell_Ebu ]_Atdde 
S1_Anon Gwych_Anscadu ma_Adf enwb_Anon yn_Arsym iawn_Ans ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Faint_Rhaamh ohonoch_Ar2ll chi_Rhapers2ll sy_Bpres3perth 'n_Uberf dysgu_Be Cymraeg_Ebu ?_Atdt 
How_Gwest many_Gwest of_Gwest you_Gwest are_Gwest lurning_Gwest Welsh_Gwest ?_Atdt Faint_Rhaamh ohonon_Ar1ll -_Atdcys ni_Rhapers1ll lot_Rhaamh ohonoch_Ar2ll chi_Rhapers2ll lot_Rhaamh 's_Gwllyth of_Gwest you_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Gwych_Anscadu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Pa_Bangof lefelau_Ebll '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf neud_Be ?_Atdt 
what_Gwest levels_Gwest ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Oes_Bpres3amhen unrhyw_Anscadu un_Bancynnar mynediad_Egu ?_Atdt 
any_Gwest mynediad_Egu ?_Atdt 
Ok_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Sylfaen_Ebu ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Na_Adf ni_Rhapers1ll cwpl_Egu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Canolradd_Anscadu ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Un_Bancynnar person_Egu dau_Rhifol berson_Egu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Uwch_Anscadu ?_Atdt 
[saib]_Gwann ôô_Ebych dim_Adf uwch_Anscadu Ok_Gwest so_Adf mynediad_Egu sylfaen_Ebu a_Cyscyd chanolradd_Anscadu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Dyma_Adf enwg_Anon mae_Bpres3u enwg_Anon yn_Arsym gweithio_Be i_Rhapers1u _prifysgol__Anon fel_Cyscyd __teitl_swydd___Anon ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ie_Adf <_Atdchw /_Gwsym anon_Gwest >_Atdde teitl_Egu swydd_Ebu <_Atdchw /_Gwsym anon_Gwest >_Atdde a_Cyscyd mae_Bpres3u e_Rhapers3gu yn_Arsym mynd_Be i_Arsym siarad_Be am_Arsym ddulliau_Egll dysgu_Be ._Atdt 
Some_Gwest learning_Gwest methods_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Felly_Adf bydd_Bdyf3u e_Rhapers3gu 'n_Uberf helpu_Be chi_Rhapers2ll gobeithio_Be ._Atdt 
To_Gwest help_Gwest you_Gwest hopefully_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ie_Adf felly_Adf dyma_Adf enwg_Anon dyma_Adf enwg_Anon ._Atdt 
[_Atdchw Cymeradwyaeth_Ebu ]_Atdde 
S2_Anon Pnawn_Egu da_Anscadu pawb_Rhaamh diolch_Egu yn_Arsym fawr_Anscadu diolch_Egu ._Atdt 
[_Atdchw Cymeradwyaeth_Ebu yn_Arsym dod_Be i_Arsym ben_Egu ]_Atdde 
S2_Anon [=]_Gwann ymm_Ebych so_Adf fel_Cyscyd dywedodd_Bgorff3u ymm_Ebych [aneglur]_Gwann [/=]_Gwann -_Atdcys Dwi_Bpres1u enwg_Anon dw_Bpres1u i_Rhapers1u ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Dw_Bpres1u i_Rhapers1u 'n_Arsym teitl_swydd_Anon yn_Arsym prifysgol_Anon ._Atdt 
So_Bpres1u I_Rhapers1u 'm_Adf a_Cyscyd teitl_swydd_Anon in_Gwest the_Bpres3u prifysgol_Anon ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde A_Cyscyd heddiw_Adf dw_Bpres1u i_Rhapers1u 'n_Uberf mynd_Be i_Arsym siarad_Be am_Arsym ddulliau_Egll o_Arsym ddysgu_Be ail_Rhitrefd iaith_Ebu So_Gwest ymm_Gwest second_Gwest language_Gwest learning_Gwest method_Gwest ymm_Gwest I_Gwest know_Gwest that_Gwest you_Gwest 're_Anscadu all_Gwest different_Gwest levels_Gwest so_Gwest what_Gwest I_Rhapers1u 'll_Gwest do_Gwest is_Gwest ymm_Ebych siarad_Be yn_Arsym Gymraeg_Ebu a_Cyscyd hefyd_Adf siarad_Be yn_Arsym Saesneg_Ebu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
[_Atdchw Ceroddoriaeth_Gwest yn_Arsym dechrau_Egu chwarae_Egu yn_Arsym gryfach_Anscym yn_Arsym y_YFB cefndir_Egu ]_Atdde 
S2_Anon Felly_Adf ymm_Ebych yn_Arsym y_YFB sesiwn_Ebu yma_Adf fel_Cyscyd dywedais_Bgorff1u i_Rhapers1u byddai_Bamod3u 'n_Uberf amlinellu_Be y_YFB problemau_Egbll sy_Bpres3perth 'n_Uberf wynebu_Be ymm_Ebych dysgwyr_Egll ail_Rhitrefd iaith_Ebu yn_Arsym gyffredinol_Anscadu a_Cyscyd dysgwyr_Egll y_YFB Gymraeg_Ebu yn_Arsym benodol_Anscadu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon ymm_Ebych dwi_Bpres1u ddim_Egu yn_Arsym mynd_Be i_Arsym roi_Be rhyw_Egbu fath_Banhwyr o_Rhapers3gu magic_Gwest wand_Gwest i_Rhapers1u chi_Rhapers2ll adael_Be yr_YFB ystafell_Ebu a_Cyscyd adael_Be y_YFB babell_Ebu efo_Arsym ryw_Egbu fath_Banhwyr o_Rhapers3gu [aneglur]_Gwann troedigaeth_unk rhyw_Egbu fath_Banhwyr o_Rhapers3gu ym_Ebych ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon So_Adf dw_Bpres1u i_Rhapers1u ddim_Adf yma_Adf i_Arsym ddeud_Be ôô_Ebych wel_Ebych dyle_Bamherff3u chi_Rhapers2ll neud_Be hyn_Rhadangd yyy_Ebych mwyn_Egu dysgu_Be 'r_YFB Gymraeg_Ebu achos_Egu tydi_Bpres3u hynny_Rhadangd ddim_Adf yn_Arsym mynd_Be i_Arsym weithio_Be i_Rhapers1u chi_Rhapers2ll ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Be_Rhagof '_Atddyf sy_Bpres3perth 'n_Uberf mynd_Be i_Arsym weithio_Be i_Rhapers1u chi_Rhapers2ll ydi_Bpres3u ffeindio_Be allan_Adf beth_Rhagof sydd_Bpres3perth orau_Anseith i_Rhapers1u chi_Rhapers2ll ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ymm_Ebych o_Arsym ran_Ebu ymm_Ebych fy_Banmedd1u mhrofiad_Egu i_Rhapers1u felly_Adf ymm_Ebych dw_Bpres1u i_Rhapers1u 'n_Uberf gwneud_Be ymchwil_Egu yn_Arsym prifysgol_Anon so_Bpres1u I_Rhapers1u do_Egu research_Gwest in_Gwest the_Bpres3u prifysgol_Anon ac_Cyscyd fy_Banmedd1u maes_Egu ymchwil_Egu i_Rhapers1u ydi_Bpres3u sosioieithyddiaeth_Ebu ymm_Ebych sociolinguistics_Gwest ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon Fel_Cyscyd dysgwyr_Egll ymm_Ebych '_Atddyf da_Anscadu chi_Rhapers2ll yn_Arsym rhan_Ebu o_Arsym gymuned_Ebu o_Arsym bobl_Ebu sy_Bpres3perth 'n_Uberf siarad_Be Cymraeg_Ebu [aneglur]_Gwann '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Utra rhan_Ebu o_Arsym ddiwylliant_Egu y_YFB Gymraeg_Ebu ac_Cyscyd mae_Bpres3u hynny_Rhadangd yn_Arsym ddiddorol_Anscadu iawn_Adf imi_Ar1u ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saeaneg_Ep ]_Atdde 
S2_Anon So_Adf dyma_Adf pam_Adf dw_Bpres1u i_Rhapers1u yma_Adf heddiw_Adf ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ym_Ebych ma_Bpres3u na_Rhaperth ymm_Ebych reswm_Egu bersonol_Anscadu hefyd_Adf ymm_Ebych dw_Bpres1u i_Rhapers1u hefyd_Adf wedi_Uberf dysgu_Be 'r_YFB Gymraeg_Ebu so_Adf dw_Bpres1u i_Rhapers1u 'n_Uberf dod_Be o_Rhapers3gu lleoliad_Anon yn_Arsym Gogledd_Egu Cymru_Epb yn_Arsym un_Rhaamh -_Atdcys o_Rhapers3gu enwb_Anon comes_Gwest from_Gwest _lleoliad__Anon as_Egu well_Anscym ._Atdt 
S2_Anon Nice_Gwest to_Gwest see_Gwest we_Gwest are_Gwest represented_Gwest +_Gwsym 
[_Atdchw Cynulleidfa_Ebu yn_Arsym chwerthin_Be ]_Atdde 
S2_Anon +_Gwsym ymm_Gwest for_Gwest once_Gwest ._Atdt 
S2_Anon ymm_Ebych ond_Arsym dw_Bpres1u i_Rhapers1u di_Rhapers2u dysgu_Be 'r_YFB Gymraeg_Ebu ymm_Ebych a_Cyscyd hefyd_Adf felly_Adf dyna_Adf pam_Adf mae_Bpres3u geni_Be ddiddordeb_Egu ymm_Ebych yn_Arsym y_YFB maes_Egu [aneglur]_Gwann ._Atdt 
So_Gwest that_Gwest 's_Gwllyth why_Gwest I_Rhapers1u 'm_Rhadib1u intersted_Gwest in_Gwest ymm_Gwest this_Gwest in_Gwest this_Gwest area_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde So_Adf ymm_Ebych ymlaen_Adf a_Cyscyd ni_Rhapers1ll felly_Adf ymm_Ebych na_Rhaperth i_Arsym siarad_Be na_Rhaperth i_Arsym ddechrau_Egu trwy_Arsym sôn_Egu am_Arsym ymm_Ebych y_YFB broblem_Egbu ymm_Ebych un_Bancynnar broblem_Egbu sy_Bpres3perth 'n_Uberf wynebu_Be dysgwy_Bdibdyf1u 'r_YFB ymm_Ebych ail_Rhitrefd iaith_Ebu ._Atdt 
So_Gwest I_Rhapers1u 'll_Gwest talk_Gwest about_Gwest the_Gwest first_Gwest ymm_Gwest problem_Gwest that_Gwest effects_Gwest second_Gwest language_Gwest learners_Gwest and_Gwest that_Gwest 's_Gwllyth cymhelliad_Egu neu_Cyscyd motivation_Gwest ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon So_Adf mae_Bpres3u cymhelliad_Egu yn_Arsym bwysig_Anscadu iawn_Adf fel_Cyscyd ffactor_Egu sydd_Bpres3perth yn_Arsym ymm_Ebych sy_Bpres3perth 'n_Uberf wynebu_Be dysgwyr_Egll y_YFB Gymraeg_Ebu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Mae_Bpres3u un_Rhaamh o_Arsym 'r_YFB ymchwilwyr_Egll one_Gwest of_Gwest the_Gwest researchers_Gwest says_Gwest "_Atddyf motivation_Gwest is_Gwest responsible_Gwest for_Gwest why_Gwest people_Gwest decide_Gwest to_Gwest do_Gwest something_Gwest how_Gwest long_Gwest they_Gwest 're_Anscadu willing_Gwest to_Gwest sustain_Gwest the_Gwest activity_Gwest and_Gwest how_Gwest hard_Gwest they_Gwest 're_Anscadu gonna_Gwest ymm_Gwest they_Gwest 're_Anscadu going_Gwest to_Gwest persue_Gwest it_Gwest "_Atddyf ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde So_Adf pam_Adf bod_Be cymhelliad_Egu yn_Arsym bwysig_Anscadu felly_Adf os_Egu '_Atddyf da_Anscadu chi_Rhapers2ll yn_Arsym dysgu_Be ail_Rhitrefd iaith_Ebu why_Gwest is_Gwest ym_Gwest motivation_Gwest important_Gwest if_Gwest you_Gwest 're_Anscadu learning_Gwest a_Gwest second_Gwest language_Gwest 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Wel_Ebych mae_Bpres3u motivation_Gwest ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon So_Adf ma_Bpres3u '_Atddyf 'na_Bandangd ymdrech_Egbu ma_Bpres3u '_Atddyf 'na_Bandangd fwynhad_Egu a_Cyscyd ma_Bpres3u '_Atddyf 'na_Bandangd fuddsoddiad_Egu ._Atdt 
It_Gwest 's_Gwllyth also_Gwest gonna_Gwest dictate_Gwest how_Gwest much_Gwest you_Gwest 've_Gwest enjoyed_Gwest learning_Gwest Welsh_Gwest or_Gwest any_Gwest other_Gwest language_Gwest as_Gwest well_Gwest ._Atdt 
[=]_Gwann ymm_Ebych so_Adf y_YFB rheswm_Egu [/=]_Gwann Y_YFB peth_Egu cynta_Rhitrefd hoffwn_Bamherff1u i_Rhapers1u ddeud_Be ydi_Bpres3u bod_Be yn_Arsym bwysig_Anscadu i_Arsym ddysgwyr_Egll feddwl_Be am_Arsym y_YFB rhesymau_Egll pam_Adf fod_Be nhw_Rhapers3ll 'n_Uberf dysgu_Be 'r_YFB iaith_Ebu '_Atddyf dan_Arsym sylw_Egu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon So_Adf y_YFB tacteg_Ebu cyntaf_Anscadu '_Atddyf swn_Egu i_Rhapers1u 'n_Uberf deud_Be ydi_Bpres3u meddwl_Egu yn_Arsym glir_Anscadu am_Arsym y_YFB rhesymau_Egll pam_Adf bo_Adf '_Atddyf chi_Rhapers2ll 'n_Uberf siarad_Be Cymraeg_Ebu a_Cyscyd byddwch_Bdyf2ll yn_Arsym ymwybodol_Anscadu hefyd_Adf fod_Be cymhelliad_Egu yn_Arsym gallu_Egu ymm_Ebych gallu_Be 'ch_Rhadib2ll helpu_Be chi_Rhapers2ll dros_Arsym amser_Egu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saseneg_Ep ]_Atdde 
S2_Anon ymm_Ebych yr_YFB ail_Rhitrefd ymm_Ebych yr_YFB ail_Rhitrefd beth_Egu ydi_Bpres3u ymm_Ebych hyder_Egu a_Cyscyd ma_Bpres3u '_Atddyf hyder_Egu hefyd_Adf yn_Arsym broblem_Egbu sy_Bpres3perth 'n_Uberf wynebu_Be llawer_Rhaamh o_Rhapers3gu -_Atdcys ddysgu_Be o_Arsym ddysgwyr_Egll ymm_Ebych ail_Rhitrefd iaith_Ebu ac_Cyscyd wrth_Arsym gwrs_Egu yn_Arsym y_YFB Gymraeg_Ebu '_Atddyf da_Anscadu ni_Rhapers1ll 'n_Uberf clywed_Be yn_Arsym aml_Anscadu iawn_Adf bod_Be hyder_Egu yn_Arsym gallu_Egu effeithio_Be ar_Arsym ar_Arsym sut_Egbu mae_Bpres3u dysgwyr_Egll yn_Arsym teimlo_Be ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon So_Adf [saib]_Gwann ma_Bpres3u '_Atddyf 'na_Bandangd ddyfyniad_Egu yma_Adf gan_Arsym un_Rhaamh o_Arsym 'r_YFB ymchwilwyr_Egll yn_Arsym y_YFB maes_Egu sy_Bpres3perth 'n_Rhadib1ll ymm_Ebych yn_Arsym atgoffa_Egu fi_Rhapers1u yn_Arsym aml_Anscadu iawn_Adf o_Arsym 'r_YFB o_Arsym 'r_YFB pwysigrwydd_Egu o_Arsym 'r_YFB pwysigrwydd_Egu y_YFB ffactor_Egu hwn_Rhadangg ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon A_Cyscyd dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be bod_Be hynny_Rhadangd yn_Arsym deud_Be y_YFB cyfan_Egu that_Gwest says_Gwest it_Gwest all_Gwest really_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Dyma_Adf dw_Bpres1u i_Rhapers1u 'n_Utra siŵr_Anscadu sut_Rhagof ymm_Ebych dyma_Adf sut_Egbu ma_Bpres3u '_Atddyf ran_Ebu fwyaf_Anseith ohonan_Ar3ll ni_Rhapers1ll yn_Arsym teimlo_Be weithiau_Adf ydi_Bpres3u bod_Be ni_Rhapers1ll ddim_Adf '_Atddyf efo_Arsym 'r_YFB hyder_Egu i_Arsym ddefnyddio_Be 'r_YFB iaith_Ebu ._Atdt 
Where_Gwest we_Gwest feel_Gwest that_Gwest we_Gwest don't_Bpres3ll have_Gwest the_Gwest confidence_Gwest to_Gwest use_Gwest the_Gwest to_Gwest use_Gwest the_Gwest language_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ymm_Ebych so_Adf '_Atddyf o_Arsym ni_Rhapers1ll ishio_Be gofyn_Be cwestiwn_Egu i_Rhapers1u chi_Rhapers2ll ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Pam_Adf '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf teimlo_Be fod_Be dysgwyr_Egll yn_Arsym dueddol_Anscadu o_Arsym deimlo_Be o_Arsym deimlo_Be 'n_Utra ddihyder_Anscadu ?_Atdt 
Why_Gwest do_Gwest you_Gwest think_Gwest ymm_Gwest learners_Gwest might_Gwest feel_Gwest under_Egu -_Atdcys confident_Gwest ?_Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu a_Cyscyd 'r_YFB gynulleidfa_Ebu yn_Arsym ateb_Egu cwestiynau_Egll ]_Atdde 
S2_Anon So_Adf ma_Bpres3u '_Atddyf 'na_Adf [saib]_Gwann bobl_Ebu sy_Bpres3perth 'n_Uberf pryderu_Be bod_Be 'na_Adf embaras_Egu am_Arsym ddysgu_Be 'r_YFB Gymraeg_Ebu ma_Bpres3u '_Atddyf 'na_Bandangd bobl_Ebu sy_Bpres3perth 'n_Uberf credu_Be bod_Be 'na_Adf ymm_Ebych bod_Be 'na_Adf ymm_Ebych ymagweddau_Ebll -_Atdcys ne_Egu negyddol_Anscadu tuag_Arsym at_Arsym ddysgwyr_Egll ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ond_Arsym sut_Egbu i_Rhapers1u gwella_Be hyder_Egu fel_Cyscyd dysgwyr_Egll ?_Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu 
S2_Anon So_Gwest who_Gwest is_Gwest doing_Gwest mynediad_Egu ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Mynediad_Egu ie_Adf ._Atdt 
So_Gwest if_Gwest you_Gwest 're_Anscadu doing_Gwest mynediad_Egu you_Gwest might_Gwest wanna_Gwest this_Gwest this_Gwest weekend_Gwest you_Gwest might_Gwest wanna_Gwest think_Gwest about_Gwest a_Gwest situation_Gwest where_Gwest you_Gwest 're_Anscadu gonna_Gwest use_Gwest Welsh_Gwest ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon ymm_Ebych doh_Ebych gallwch_Bgorch2ll chi_Rhapers2ll ddweud_Be hwnna_Rhadangg eto_Adf ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Neu_Cyscyd fyddach_Bdyf2ll chi_Rhapers2ll 'n_Uberf deud_Be ymm_Ebych sori_Ebych dw_Bpres1u i_Rhapers1u 'n_Uberf dysgu_Be 'r_YFB Gymraeg_Ebu neu_Cyscyd fyddach_Bdyf2ll chi_Rhapers2ll 'n_Uberf neud_Be be_Rhagof '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf hoffi_Be neud_Be sef_Cyscyd [-]_Gwann 
[_Atdchw S_Gwllyth 2_Gwdig yn_Arsym gwneud_Be ystym_unk sydd_Bpres3perth yn_Arsym achosi_Be i_Arsym 'r_YFB gynulleidfa_Ebu ymateb_Be wrth_Arsym chwerthin_Be ]_Atdde 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon Dw_Bpres1u -_Atdcys i_Arsym gai_Bamherff3u gwrw_Egu os_Cyscyd gwelwch_Bgorch2ll yn_Arsym dda_Anscadu ?_Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon If_Gwest you_Gwest 're_Anscadu canolradd_Anscadu yyy_Ebych enghraifft_Ebu os_Cyscyd '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Arsym ymm_Ebych barod_Anscadu i_Arsym fynd_Be i_Arsym rywun_Egu i_Arsym ysgrifennu_Be yn_Arsym y_YFB Gymraeg_Ebu ac_Cyscyd yn_Arsym poeni_Be am_Arsym gamgymeriadau_Egll ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde -_Atdcys Poeni_Be worrying_Gwest about_Gwest mistakes_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Efallai_Adf meddwl_Egu wel_Ebych be_Rhagof galla_Bpres1u '_Atddyf i_Rhapers1u neud_Be ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon Os_Cyscyd '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf poeni_Be am_Gwtalf neud_Be camgymeriadau_Egll y_YFB bobl_Ebu effalla_unk '_Atddyf sydd_Bpres3perth wedi_Uberf cyrraedd_Be y_Rhadib1u lefela_Bgorch2u '_Atddyf uwch_Anscadu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Dw_Bpres1u i_Rhapers1u 'n_Utra siŵr_Anscadu na_Uneg fydd_Bdyf3u y_YFB person_Egu sy_Bpres3perth 'n_Uberf gwrando_Be arnoch_Ar2ll chi_Rhapers2ll yn_Arsym gwbod_Be bo_Adf chi_Rhapers2ll wedi_Uberf gwneud_Be camgymeriad_Egu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon A_Cyscyd hefyd_Adf ymm_Ebych efalla_Adf '_Atddyf bod_Be eisiau_Egu meddwl_Be am_Gwtalf be_Rhagof '_Atddyf ma_Bpres3u '_Atddyf seicolegwyr_Egll yn_Arsym galw_Egu yn_Arsym -_Atdcys ha_Egu ymm_Ebych sori_Ebych unhelpful_Gwest thinking_Gwest habits_Gwest ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon Oh_Gwest my_Gwest God_Gwest if_Gwest I_Gwest don't_Bpres3ll do_Gwest this_Gwest treiglad_Egu I_Rhapers1u 'm_Rhadib1u a_Gwest rubbish_Gwest learner_Gwest of_Gwest Welsh_Gwest ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon Ydi_Bpres3u hynna_Anscadu 'n_Uberf neud_Be synnwyr_Egu ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ymm_Ebych felly_Adf byddwn_Bdyf1ll ni_Rhapers1ll 'n_Arsym gwglio_unk unhelpful_Gwest thinking_Gwest habits_Gwest I_Gwest know_Gwest that_Gwest 's_Gwllyth a_Gwest bit_Gwest hippie_Gwest as_Gwest well_Gwest but_Gwest it_Gwest does_Gwest work_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ymm_Ebych byddwn_Bdyf1ll ni_Rhapers1ll 'n_Uberf osgoi_Be cymariaethau_Ebll a_Cyscyd phobl_Ebu erill_Anscadu ._Atdt 
So_Gwest try_Gwest to_Gwest avoid_Gwest comparisons_Gwest with_Gwest other_Gwest people_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ond_Arsym hefyd_Adf mae_Bpres3u gosod_Be targedau_Egll bach_Anscadu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon ymm_Ebych so_Adf dw_Bpres1u i_Rhapers1u 'di_Uberf sôn_Be am_Arsym gymhelliad_Egu a_Cyscyd hefyd_Adf dw_Bpres1u i_Rhapers1u 'di_Uberf sôn_Be am_Arsym y_YFB bwysigrwydd_Egu pwysigrwydd_Egu o_Arsym fod_Be yn_Arsym awyddus_Anscadu i_Arsym siarad_Be ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon ymm_Ebych y_YFB peth_Egu nesa_Anseith ymm_Ebych ydi_Bpres3u ymm_Ebych rhywbeth_Rhaamh sy_Bpres3perth 'n_Uberf dod_Be allan_Adf o_Arsym 'r_YFB ymchwil_Egu i_Arsym gaffael_Egu ail_Rhitrefd iaith_Ebu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu [_Atdchw 
S2_Anon Dw_Bpres1u i_Rhapers1u 'n_Uberf credu_Be bod_Be hyn_Rhadangd yn_Arsym bwysig_Anscadu achos_Egu efallai_Adf bod_Be ni_Rhapers1ll 'n_Uberf canolbwyntio_Be yn_Arsym y_YFB dosbarth_Egu ond_Arsym wedyn_Adf ar_Arsym ôl_Anscadu y_YFB dosbarth_Egu '_Atddyf da_Anscadu ni_Rhapers1ll ddim_Egu really_Gwest yn_Arsym canolbwyntio_Be ar_Arsym ymm_Ebych yr_YFB iaith_Ebu Gymraeg_Ebu sydd_Bpres3perth o_Arsym gwmpas_Egu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon So_Adf be_Rhagof '_Atddyf '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf gallu_Be neud_Be ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Wel_Ebych sylwi_Be a_Cyscyd 'r_YFB batrymau_Egll yn_Arsym yr_YFB iaith_Ebu ymm_Ebych gwneud_Be tasgau_Ebll ym_Ebych meddwl_Be am_Gwtalf geiriau_Egll newydd_Anscadu neu_Cyscyd seiniau_Ebll newydd_Anscadu neu_Cyscyd ymm_Ebych rheolau_Ebll gramadegol_Anscadu tra_Cyscyd '_Atddyf bo_Adf chi_Rhapers2ll 'n_Uberf gwrando_Be ar_Arsym y_YFB iaith_Ebu neu_Cyscyd yn_Arsym defnyddio_Be ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon ymm_Ebych [aneglur]_Gwann sut_Rhagof mae_Bpres3u 'r_YFB amser_Egu ?_Atdt 
[_Atdchw Aelod_Egu o_Arsym 'r_YFB gynulleidfa_Ebu yn_Arsym ateb_Egu ]_Atdde 
S3_Anon Pum_Rhifol munud_Egu ar_Arsym hygain_Anscadu [aneglur]_Gwann ._Atdt 
S2_Anon ââ_Ebych Ok_Gwest [saib]_Gwann sori_Gwest dw_Bpres1u i_Rhapers1u 'n_Uberf gwbod_Be '_Atddyf bo_Adf chi_Rhapers2ll 'n_Uberf aros_Be i_Arsym Bobl_Ebu Y_YFB Cwm_Egu ._Atdt 
[_Atdchw Cynulleidfa_Ebu yn_Arsym chwerthin_Be <_Atdchw /_Gwsym N_Gwllyth 
S2_Anon Ond_Arsym ymm_Ebych fyddai_Bamod3u -_Atdcys ddim_Egu y_YFB peth_Egu ola_Anscadu ymm_Ebych hoffwn_Bamherff1u i_Rhapers1u sôn_Be amdan_Arsym y_YFB fo_Rhapers3gu ydi_Bpres3u ymm_Ebych rhywbeth_Rhaamh sy_Bpres3perth 'n_Uberf ymwneud_Be a_Cyscyd chymhelliant_Egu i_Arsym fod_Be yn_Arsym onest_Anscadu ma_Bpres3u '_Atddyf hynny_Rhadangd yn_Arsym ymwneud_Be â_Arsym strategaethau_Ebll dysgu_Be yn_Arsym gyffredinol_Anscadu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon ymm_Ebych felly_Adf '_Atddyf da_Anscadu ni_Rhapers1ll 'n_Uberf galw_Be hyn_Rhadangd yn_Arsym metacognitive_Gwest awareness_Gwest sef_Cyscyd planning_Gwest strategies_Gwest ymm_Gwest thinking_Gwest about_Gwest your_Gwest learning_Gwest and_Gwest also_Gwest planning_Gwest your_Gwest learing_Gwest as_Gwest well_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Dw_Bpres1u i_Rhapers1u 'n_Uberf gwbod_Be '_Atddyf bo_Adf gennych_Ar2ll chi_Rhapers2ll ymm_Ebych daflen_Ebu dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be efo_Arsym Cymraeg_Ebu i_Arsym oedolion_Egll lle_Anscadu '_Atddyf da_Anscadu chi_Rhapers2ll rhestru_Be be_Rhagof hoffwch_Bgorch2ll chi_Rhapers2ll neud_Be yn_Arsym y_YFB dosbarth_Egu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon ymm_Ebych [saib]_Gwann so_Adf pethau_Egll fel_Cyscyd os_Cyscyd '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf meddwl_Be am_Arsym strategaethau_Ebll strategies_Gwest be_Rhagof galloch_Bgorff2ll chi_Rhapers2ll neud_Be yyy_Ebych enghraifft_Ebu ydi_Bpres3u meddwl_Egu ydi_Bpres3u chwilio_Be am_Arsym adnoddau_Egll mewn_Arsym ieithoedd_Ebll eraill_Anscadu yyy_Ebych enghraifft_Ebu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon So_Adf mi_Uberf fydd_Bdyf3u yna_Adf bethau_Egll '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf gallu_Be ei_Banmedd3gu cymhwyso_Be nw_Bpres1u at_Arsym ymm_Ebych eich_Banmedd2ll taith_Ebu chi_Rhapers2ll ._Atdt 
And_Gwest that_Gwest 's_Gwllyth why_Gwest I_Rhapers1u 'm_Rhadib1u not_Gwest really_Gwest being_Gwest that_Gwest specific_Gwest today_Gwest because_Gwest as_Gwest I_Gwest said_Gwest at_Gwest the_Gwest start_Gwest be_Rhagof sy_Bpres3perth 'n_Uberf mynd_Be i_Arsym weithio_Be chi_Rhapers2ll fydd_Bdyf3u yn_Arsym wahanol_Anscadu i_Rhapers1u be_Rhagof '_Atddyf sy_Bpres3perth 'n_Uberf gweithio_Be i_Arsym fi_Rhapers1u ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde A_Cyscyd ma_Bpres3u '_Atddyf hynny_Rhadangd yn_Arsym bwysig_Anscadu i_Rhapers1u chi_Rhapers2ll sylweddoli_Be ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde So_Adf pwy_Egu yyy_Ebych enghraifft_Ebu ymm_Ebych os_Cyscyd na_Adf unrhywun_Rhaamh sydd_Bpres3perth wedi_Uberf neud_Be dadawgrymeg_unk neu_Cyscyd suggestopedia_Gwest have_Gwest you_Gwest come_Gwest accross_Gwest that_Gwest before_Gwest that_Gwest 's_Gwllyth the_Gwest -_Atdcys wo_unk ymm_Gwest a_Gwest way_Gwest of_Gwest learining_Gwest that_Gwest has_Gwest been_Gwest very_Gwest successful_Gwest in_Gwest Welsh_Gwest ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon Bach_Egu o_Arsym drilio_Be [aneglur]_Gwann os_Cyscyd '_Atddyf da_Anscadu chi_Rhapers2ll ie_Adf '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf gwbod_Be be_Rhagof ydi_Bpres3u drilio_Be ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ymm_Ebych a_Cyscyd ma_Bpres3u hynny_Rhadangd 'n_Utra fine_Gwest be_Rhagof '_Atddyf dw_Bpres1u '_Atddyf i_Rhapers1u 'n_Uberf trio_Be deud_Be ydi_Bpres3u bod_Be pethau_Egll 'n_Uberf mynd_Be i_Arsym weithio_Be yn_Arsym wahanol_Anscadu i_Rhapers1u chi_Rhapers2ll ymm_Ebych o_Arsym gymharu_Be a_Cyscyd phobl_Ebu eraill_Anscadu ._Atdt 
So_Gwest things_Gwest will_Gwest be_Gwest different_Gwest for_Gwest each_Gwest of_Gwest us_Gwest ._Atdt 
S2_Anon ymm_Ebych felly_Adf mae_Bpres3u 'n_Utra bwysig_Anscadu i_Rhapers1u chi_Rhapers2ll feddwl_Be be_Rhagof '_Atddyf sy_Bpres3perth 'n_Uberf mynd_Be i_Arsym weithio_Be i_Rhapers1u chi_Rhapers2ll ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ma_Egbu hefyd_Adf yn_Arsym bwysig_Anscadu ymm_Ebych i_Arsym feddwl_Egu am_Arsym bethau_Egll '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf gallu_Be neud_Be efallai_Adf tu_Egu allan_Adf i_Arsym 'r_YFB dosbarth_Egu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon '_Atddyf Sa_Bgorb1u chi_Rhapers2ll 'n_Uberf gallu_Be newid_Be sut_Egbu '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf dysgu_Be ymm_Ebych geiriau_Egll newydd_Anscadu trwy_Arsym ddefnyddio_Be cardiau_Ebll flach_unk ar_Arsym lein_Ebu yyy_Ebych enghraifft_Ebu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon ymm_Ebych and_Gwest that_Gwest 's_Gwllyth ymm_Ebych cardiau_Ebll flach_unk on_Gwest line_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ymm_Ebych galloch_Bgorff2ll chi_Rhapers2ll greu_Be ymarferion_Ebll ymm_Ebych gramadegol_Anscadu os_Cyscyd '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf hoffi_Be gramadeg_Egbu a_Cyscyd 'i_Rhadib3bu rhannu_Be nhw_Rhapers3ll hefo_Arsym pobl_Ebu yn_Arsym y_YFB dosbarth_Egu ._Atdt 
So_Gwest if_Gwest you_Gwest like_Gwest gramadeg_Egbu grammar_Gwest ?_Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon [chwerthin]_Gwann Dw_Bpres1u i_Rhapers1u 'n_Uberf hoffi_Be rheiolau_unk sori_Ebych ymm_Ebych ok_Gwest na_Rhaperth i_Arsym symyd_Be ymlaen_Adf ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ymm_Ebych ond_Arsym galloch_Bgorff2ll chi_Rhapers2ll hefyd_Adf ymm_Ebych gosod_Be targedau_Egll ar_Arsym gyfer_Egu y_YFB penwythnos_Egu yma_Adf so_Adf galloch_Bgorff2ll chi_Rhapers2ll ddweud_Be dw_Bpres1u i_Rhapers1u yn_Arsym mynd_Be i_Arsym fynd_Be i_Arsym archebu_Be cwrw_Egu ymm_Ebych yn_Arsym y_YFB Gymraeg_Ebu y_YFB penwythnos_Egu yma_Adf ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ma_Bpres3u '_Atddyf hynny_Rhadangd yn_Arsym darged_Egu ymm_Ebych penodol_Anscadu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saseneg_Ep ]_Atdde 
S2_Anon So_Adf y_YFB tacteg_Ebu ola_Anscadu felly_Adf ydi_Bpres3u i_Rhapers1u adfyfyrio_unk yn_Arsym aml_Anscadu ._Atdt 
So_Gwest reflect_Gwest ymm_Gwest often_Gwest on_Gwest pam_Adf '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf hoffi_Be dysgu_Be 'r_YFB Gymraeg_Ebu ymm_Ebych pam_Adf '_Atddyf da_Anscadu chi_Rhapers2ll ishio_Be defnyddio_Be 'r_YFB yyy_Ebych dysgu_Be 'r_YFB Gymraeg_Ebu ymm_Ebych sut_Rhagof '_Atddyf da_Anscadu chi_Rhapers2ll '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf gallu_Be ymm_Ebych helpu_Be 'ch_Rhadib2ll hun_Rhaatb i_Arsym ddefnyddio_Be mwy_Rhaamh o_Arsym Gymraeg_Ebu a_Cyscyd hefyd_Adf sut_Egbu galloch_Bgorff2ll chi_Rhapers2ll weithio_Be ar_Arsym eich_Banmedd2ll hyder_Egu i_Rhapers1u ddysgur_unk gymraeg_Ebu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Felly_Adf ymm_Ebych dyma_Adf fo_Rhapers3gu ymm_Ebych os_Cyscyd os_Cyscyd ganddoch_Ar2ll chi_Rhapers2ll unrhyw_Bancynnar gwestiynau_Egll o_Arsym gwbwl_Anscadu ._Atdt 
[_Atdchw Darn_Egu hir_Anscadu o_Arsym Saesneg_Ebu ]_Atdde 
S2_Anon Diolch_Egu yn_Arsym fawr_Anscadu ._Atdt 
[_Atdchw Cynulleidfa_Ebu yn_Arsym cymeradwyo_Be ]_Atdde 
S1_Anon Diolch_Egu enwg_Anon ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Oes_Bpres3amhen cwestiynau_Egll o_Arsym gwbl_Egu ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ydi_Bpres3u rywun_Egu eisiau_Egu gofyn_Be cwestiwn_Egu does_Gwest anyone_Gwest want_Gwest to_Gwest ask_Gwest a_Gwest question_Gwest enwb_Anon ?_Atdt 
[_Atdchw Saib_Egu wrth_Arsym iddynt_Ar3ll fynd_Be a_Arsym meic_Egu at_Arsym S_Gwllyth ]_Atdde 
S4_Anon Ydi_Bpres3u 'n_Uberf credu_Be bod_Be yyy_Ebych cael_Be mentor_Egu yn_Arsym bwysig_Anscadu iawn_Adf i_Arsym ddysgwyr_Egll rwyun_unk sy_Bpres3perth 'n_Uberf helpu_Be nhw_Rhapers3ll yn_Arsym reolaidd_Anscadu ?_Atdt 
S2_Anon Ydw_Bpres1u yn_Arsym sicr_Anscadu mae_Bpres3u 'r_YFB ym_Arsym mentor_Egu tu_Egu allan_Adf i_Arsym 'r_YFB i_Arsym 'r_YFB dosbarth_Egu ?_Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ydw_Bpres1u dw'in_Ymadr meddwl_Be bod_Be hynny_Rhadangd yn_Arsym yn_Arsym rhywbeth_Egu jyst_Adf rhywun_Egu '_Atddyf da_Anscadu chi_Rhapers2ll 'n_Uberf gallu_Be fel_Cyscyd tsiecio_Be mewn_Arsym hefo_Arsym a_Cyscyd jyst_Adf siarad_Be am_Arsym ymm_Ebych sut_Rhagof ma_Bpres3u '_Atddyf pethau_Egll yn_Arsym mynd_Be '_Atddyf efo_Arsym 'r_YFB Gymraeg_Ebu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde ymm_Ebych dw_Bpres1u i_Rhapers1u ddim_Adf yn_Arsym meddwl_Egu hyd_Egu yn_Arsym oed_Egu bod_Be siarad_Be yn_Arsym Saesneg_Ebu am_Arsym y_YFB pethau_Egll 'ma_Adf yn_Arsym yn_Arsym effeithio_Be 'n_Utra negyddol_Anscadu ar_Arsym y_YFB Gymraeg_Ebu dw_Bpres1u i_Rhapers1u jyst_Adf meddwl_Egu bod_Be o'n_Bamherff1u bwysig_Anscadu fel_Cyscyd fel_Cyscyd dysgwyr_Egll i_Rhapers1u neud_Be hynny_Rhadangd ._Atdt 
S1_Anon Unrhyw_Bancynnar gwestiynau_Egll eraill_Anscadu ?_Atdt 
[saib]_Gwann na_Rhaperth ._Atdt 
S2_Anon Aros_Be am_Arsym y_YFB seléb_Egu 's_Gwllyth i_Arsym gyd_Egu ._Atdt 
S1_Anon [chwerthin]_Gwann 
S2_Anon I_Gwest don't_Bpres3ll take_Gwest it_Gwest personally_Gwest ._Atdt 
S1_Anon Am_Arsym dau_Bpres3u o_Arsym 'r_YFB gloch_Ebu bydd_Bdyf3u ymm_Ebych actorion_Egll Pobl_Ebu Y_YFB Cwm_Egu yma_Bandang os_Egu '_Atddyf da_Anscadu chi_Rhapers2ll eisiau_Egu aros_Be a_Cyscyd dw_Bpres1u i_Rhapers1u 'n_Utra siŵr_Anscadu bydd_Bdyf3u enwg_Anon yn_Arsym aros_Be hefyd_Adf os_Egu '_Atddyf da_Anscadu chi_Rhapers2ll eisiau_Egu gofyn_Be cwestiwn_Egu ._Atdt 
If_Gwest you_Gwest 're_Anscadu a_Cyscyd bit_Egu nervous_Gwest about_Gwest asking_Gwest a_Arsym question_Gwest in_Gwest front_Gwest of_Anscadu an_Ebu audience_Gwest I_Rhapers1u 'm_Rhadib1u sure_Bamherff3u enwg_Anon will_Gwest be_Rhagof happy_Gwest to_Egu answer_Gwest your_Gwest questions_Gwest ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Diolch_Egu yn_Arsym fawr_Anscadu diolch_Egu enwg_Anon ._Atdt 
S2_Anon Diolch_Egu ._Atdt 
<_Atdchw SB_Gwacr >_Atdde Diolch_Egu ._Atdt 
[_Atdchw Cynulleidfa_Ebu yn_Arsym cymeradwyo_Be ]_Atdde 
1567.556_Gwdig 