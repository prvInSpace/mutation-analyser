"_Atddyf Gwahoddiad_Egu :_Atdcan Dathlu_Be 200_Gwdig mlwyddiant_Egu Casgliad_Egu Salisbury_Ep "_Atddyf ,_Atdcan "_Atddyf Ymunwch_Bgorch2ll â_Arsym ni_Rhapers1ll am_Arsym noson_Ebu i_Arsym ddathlu_Be un_Rhaamh o_Arsym gasglwyr_Egll mwyaf_Anseith craff_Anscadu Cymru_Epb a_Cyscyd 'i_Rhadib3gu lyfrgell_Egu hynod_Anscadu ,_Atdcan mewn_Arsym digwyddiad_Egu bywiog_Anscadu yn_Arsym Archifau_Egll 'r_YFB Brifysgol_Ebu ._Atdt 
Gweld_Be yr_YFB ebost_Egu yn_Arsym eich_Banmedd2ll porwr_Egu /_Gwsym View_Gwest this_Gwest email_Gwest in_Gwest your_Gwest browser_Gwest enw_gwefan_Anon 
enw_gwefan_Anon 
Gwahoddiad_Egu :_Atdcan Dathlu_Be 200_Gwdig mlwyddiant_Egu Casgliad_Egu Salisbury_Ep 
7_Gwdig Tachwedd_Egu ,_Atdcan 6_Gwdig pm_Gwtalf -_Atdcys 7.30_Gwdig pm_Gwtalf 
Casgliadau_Egll Arbennig_Anscadu ac_Cyscyd Archifau_Egll enw_gwefan_Anon 
Prifysgol_Ebu Caerdydd_Ep 
Ymunwch_Bgorch2ll â_Arsym ni_Rhapers1ll am_Arsym noson_Ebu i_Arsym ddathlu_Be un_Rhaamh o_Arsym gasglwyr_Egll mwyaf_Anseith craff_Anscadu Cymru_Epb a_Cyscyd 'i_Rhadib3gu lyfrgell_Egu hynod_Anscadu ,_Atdcan mewn_Arsym digwyddiad_Egu bywiog_Anscadu yn_Arsym Archifau_Egll 'r_YFB Brifysgol_Ebu ._Atdt 
Ganed_Bgorffamhers y_YFB casglwr_Egu llyfrau_Egll ,_Atdcan aelod_Egu seneddol_Anscadu ,_Atdcan dyn_Bpres3ll busnes_Egbu a_Cyscyd chyfreithiwr_Egu ,_Atdcan Enoch_Ep Salisbury_Ep ,_Atdcan ar_Arsym y_YFB 7_Gwdig ed_Gwest o_Arsym Dachwedd_Egu ,_Atdcan 1819_Gwdig ._Atdt 
Ar_Arsym ôl_Anscadu derbyn_Be llyfr_Egu Cymraeg_Ebu yn_Arsym anrheg_Ebu pen_Egu blwydd_Ebu pan_Egu yn_Arsym blentyn_Egu ,_Atdcan fe_Uberf dreuliodd_Bgorff3u weddill_Egu ei_Banmedd3gu oes_Bpres3amhen yn_Arsym casglu_Be ,_Atdcan a_Cyscyd darllen_Be ,_Atdcan pob_Banmeint math_Ebu o_Arsym lyfrau_Egll am_Arsym Gymru_Epb ._Atdt 
Wedi_Arsym oes_Ebu o_Arsym geisio_Be creu_Be '_Atddyf llyfrgell_Egu Gymreig_Anscadu '_Atddyf ar_Arsym ei_Rhadib3gu ben_Egu ei_Rhadib3gu hun_Rhaatb ,_Atdcan gwerthodd_Bgorff3u Enoch_Ep Salisbury_Ep ei_Rhadib3gu gasgliad_Egu ym_Ebych 1886_Gwdig i_Arsym 'r_YFB Brifysgol_Ebu newydd_Anscadu sbon_Adf yng_Arsym Nghaerdydd_Ep ._Atdt 
A_Cyscyd dyna_Adf '_Atddyf ble_Rhagof mae_Bpres3u 'r_YFB casgliad_Egu yn_Arsym dal_Be i_Arsym fod_Be heddiw_Adf -_Atdcys yn_Arsym parhau_Be i_Arsym ysbrydoli_Be ymchwilwyr_Egll ,_Atdcan artistiaid_Egll ,_Atdcan beirdd_Egll a_Cyscyd llawer_Banmeint mwy_Rhaamh ._Atdt 
I_Arsym ddathlu_Be cyfraniad_Egu Salisbury_Ep i_Arsym fywyd_Egu diwylliannol_Anscadu ein_Banmedd1ll cenedl_Ebu ,_Atdcan ac_Cyscyd i_Arsym glywed_Be rhagor_Rhaamh am_Gwtalf ei_Rhadib3gu gasgliad_Egu hynod_Anscadu -_Atdcys dewch_Bgorch2ll draw_Adf i_Arsym 'r_YFB archifau_Egll ,_Atdcan am_Arsym ddigwyddiad_Egu yng_Arsym nghwmni_Egu enwg_Anon enwg_Anon _cyfenw__Anon ,_Atdcan __enwb___Anon ___cyfenw____Anon ,_Atdcan ____enwg_____Anon _____cyfenw______Anon a_Cyscyd mwy_Rhaamh ._Atdt 
RSVP_Gwacr :_Atdcan ebost_Anon 
Noddir_Bpresamhers y_YFB digwyddiad_Egu trwy_Arsym garedigrwydd_Egu 
Rhwydwaith_Egbu y_YFB Gymraeg_Ebu ,_Atdcan Prifysgol_Ebu Caerdydd_Ep ._Atdt 
Digwyddiad_Egu dwyieithog_Anscadu ._Atdt 
Darperir_Bpresamhers cyfieithu_Be ar_Arsym y_YFB pryd_Egu o_Arsym Gymraeg_Ebu i_Arsym Saesneg_Ebu ._Atdt 