Cynigion_Bgorff1ll Bwyd_Egu Amser_Egu Cinio_Egu yn_Arsym Nhafarn_Egbu y_YFB Bachgen_Egu Du_Anscadu 
COFIWCH_Bgorch2ll DANGOS_Be EICH_Banmedd2ll CERDYN_Egu TEYRNGARWCH_Egu IR_Anscadu STAFF_Egll CYN_Arsym RHOI_Be EICH_Banmedd2ll ARCHEB_Ebu FEL_Cyscyd A_Cyscyd GALLENT_Bamherff3ll GWEITHREDU_Be EICH_Banmedd2ll DISGOWNT_Egu TEYRNGARWCH_Egu ._Atdt 
Yr_Rhaperth ydym_Bpres1ll yn_Arsym hoffi_Be gwobrwyo_Be ein_Banmedd1ll cwsmeriaid_Egll ._Atdt ._Atdt ._Atdt 
Ar_Arsym gyfer_Egu ein_Banmedd1ll gwesteion_Egll gyda_Arsym chardiau_Ebll teyrngarwch_Egu ,_Atdcan mae_Bpres3u gennym_Ar1ll nifer_Egu o_Arsym gynigion_Egll ar_Arsym fwyd_Egu a_Cyscyd diodydd_Ebll yn_Arsym Nhafarn_Egbu y_YFB Bachgen_Egu Du_Anscadu yr_YFB wythnos_Ebu hon_Rhadangb ._Atdt 
[_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Mae_Bpres3u ein_Banmedd1ll cynigion_Egll diod_Ebu ar_Arsym gael_Be drwy_Arsym 'r_YFB dydd_Egu ,_Atdcan bob_Banmeint dydd_Egu ._Atdt 
(_Atdchw Cynnig_Be yn_Arsym dod_Be i_Arsym ben_Egu 29_Gwdig /_Gwsym 09_Gwdig /_Gwsym 2017_Gwdig )_Atdde 
Sylwch_Bgorch2ll fod_Be y_YFB cerdyn_Egu teyrngarwch_Egu hefyd_Adf yn_Arsym rhoi_Be cyfraddau_Ebll gostyngol_Anscadu ar_Arsym y_YFB rhan_Ebu fwyaf_Anseith o'n_Bamherff1u diodydd_Ebll bar_Egu eraill_Anscadu hefyd_Adf ._Atdt 
Mwy_Anscym o_Rhapers3gu gynnigion_Bgorff1ll ._Atdt ._Atdt ._Atdt 
Mwynhewch_Bgorch2ll bwyd_Egu blasus_Anscadu gyda_Arsym ni_Rhapers1ll ._Atdt ._Atdt ._Atdt 
Mae_Bpres3u gennym_Ar1ll amrywiaeth_Ebu o_Rhapers3gu gynnigion_Bgorff1ll ar_Arsym fwydydd_Egll dethol_Be o'n_Bamherff1u bwydlen_Ebu bwyd_Egu cinio_Egu a_Rhaperth fydd_Bdyf3u ar_Arsym gael_Be yn_Arsym ystod_Ebu dyddiau_Egll 'r_YFB wythnos_Ebu yn_Arsym unig_Anscadu ._Atdt 
[_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Mae_Bpres3u 'r_YFB ddewislen_Ebu amser_Egu cinio_Egu ar_Arsym gael_Be rhwng_Arsym 12_Gwdig y_YFB pnawn_Egu -_Atdcys 6_Gwdig y_YFB pnawn_Egu 
Llun_Egu -_Atdcys Gwener_Ebu 
(_Atdchw Cynnig_Be yn_Arsym dod_Be i_Arsym ben_Egu 29_Gwdig /_Gwsym 09_Gwdig /_Gwsym 2017_Gwdig )_Atdde 