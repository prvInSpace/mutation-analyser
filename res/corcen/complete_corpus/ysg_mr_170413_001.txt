PAPUR_Egu WYTHNOSOL_Anscadu YR_YFB ANNIBYNWYR_Egll CYMRAEG_Ebu 
Y_YFB TYST_Egu 
Sefydlwyd_Bgorffamhers 1867_Gwdig Cyfrol_Ebu 150_Gwdig Rhif_Egu 15_Gwdig Ebrill_Egu 13_Gwdig ,_Atdcan 2017_Gwdig 50_Gwdig c_Gwllyth ._Atdt 
Capel_Egu Newydd_Anscadu ,_Atdcan Llandeilo_Ep 
CWRDD_Be SEFYDLU'R_Gwacr PARCHEDIG_Anscadu IWAN_Gwacr VAUGHAN_Gwacr EVANS_Gwacr 
Cafwyd_Bgorffamhers oedfa_Ebu arbennig_Anscadu yn_Arsym y_YFB Capel_Egu Newydd_Anscadu ,_Atdcan Llandeilo_Ep i_Arsym sefydlu_Be 'r_YFB Parchg_Ep Iwan_Epg Vaughan_Ep Evans_Ep yn_Arsym weinidog_Egu ._Atdt 
Daeth_Bgorff3u cynulleidfa_Ebu niferus_Anscadu ynghyd_Adf i_Arsym ddathlu_Be 'r_YFB achlysur_Egu a_Cyscyd chafwyd_Bgorffamhers gwasanaeth_Egu amrywiol_Anscadu a_Cyscyd bendithiol_Anscadu ._Atdt 
Llywydd_Egu y_YFB cwrdd_Egu sefydlu_Be oedd_Bamherff3u y_YFB Parchg_Ep Edward_Epg Griffiths_Ep ,_Atdcan cyn-weinidog_Egu y_YFB Capel_Egu Newydd_Anscadu ers_Arsym 1987_Gwdig ,_Atdcan gydag_Arsym Owain_Ep Gruffydd_Epg wrth_Arsym yr_YFB organ_Ebu ._Atdt 
Roedd_Bamherff3u yn_Arsym bleser_Egbu cael_Be cwmni_Egu 'r_YFB plant_Egll a_Cyscyd 'r_YFB bobl_Ebu ifanc_Anscadu ,_Atdcan a_Rhaperth gymerodd_Bgorff3u ran_Ebu flaenllaw_Anscadu yn_Arsym yr_YFB oedfa_Ebu ._Atdt 
Cyflwynodd_Bgorff3u y_YFB plant_Egll cynradd_Anscadu berfformiad_Egu cyfoes_Anscadu a_Rhaperth baratowyd_Bgorffamhers gan_Arsym Dyfrig_Ep Davies_Gwest a_Cyscyd chafwyd_Bgorffamhers caneuon_Ebll gan_Arsym go_Egu ̂_Gwsym r_Gwllyth yr_YFB Ysgol_Ebu Sul_Egu ._Atdt 
Cymerodd_Bgorff3u y_YFB bobl_Ebu ifanc_Anscadu wedyn_Adf at_Arsym y_YFB darlleniad_Egu a_Cyscyd chyflwyno_Be 'r_YFB emynau_Egll ._Atdt 
Diolch_Egu i_Arsym athrawon_Egll yr_YFB Ysgol_Ebu Sul_Egu a_Cyscyd phawb_Rhaamh fu_Bgorff3u wrthi_Ar3bu 'n_Uberf paratoi_Be cyfraniadau_Egll graenus_Anscadu gan_Arsym y_YFB to_Egu ifanc_Anscadu ._Atdt 
Hanes_Egu yr_YFB Alwad_Egbu 
Braf_Anscadu oedd_Bamherff3u cael_Be cwmni_Egu nifer_Egu o_Arsym gyfeillion_Egll a_Rhaperth ddaeth_Bgorff3u i_Arsym ddangos_Be cefnogaeth_Ebu i_Arsym Iwan_Epg a_Cyscyd nifer_Egu hefyd_Adf yn_Arsym cymryd_Be rhan_Ebu yn_Arsym yr_YFB oedfa_Ebu ._Atdt 
Cyflwynwyd_Bgorffamhers y_YFB weddi_Egu gan_Arsym y_YFB Parchedig_Anscadu Guto_Ep Llywelyn_Epg a_Cyscyd 'r_YFB Urdd_Ebu Weddi_Egu gan_Arsym y_YFB Parchedig_Anscadu Guto_Ep Prys_Ep ap_Arsym Gwynfor_Ep ._Atdt 
Croesawyd_Bgorffamhers Iwan_Epg i_Arsym 'r_YFB Capel_Egu Newydd_Anscadu gan_Arsym Mervyn_Ep Jones_Ep gan_Arsym roi_Be ychydig_Rhaamh o_Arsym hanes_Egu yr_YFB alwad_Egbu ,_Atdcan yn_Arsym ogystal_Anscyf a_Cyscyd ̂_Gwsym diolch_Egu i_Arsym Edward_Epg am_Gwtalf ei_Rhadib3gu wasanaeth_Be i_Arsym 'r_YFB Eglwys_Ebu dros_Arsym y_YFB ddeng_Rhifol mlynedd_Ebll ar_Arsym hugain_Rhifold diwethaf_Anscadu ._Atdt 
Trosglwyddo_Be 'r_YFB Fantell_Egbu 
Yn_Arsym ystod_Ebu y_YFB sefydlu_Be ,_Atdcan cyflawnodd_Bgorff3u Edward_Epg yr_YFB arwyddion_Egbll gan_Arsym drosglwyddo_Be 'r_YFB awenau_Ebll i_Rhapers1u 'n_Utra gweinidog_Egu newydd_Anscadu ._Atdt 
Nododd_Bgorff3u Edward_Epg hefyd_Adf ei_Rhadib3gu fod_Be wedi_Uberf cael_Be blynyddoedd_Ebll o_Arsym hapusrwydd_Egu a_Cyscyd boddhad_Egu yn_Arsym y_YFB Capel_Egu Newydd_Anscadu ac_Cyscyd y_Rhaperth byddai_Bamod3u hynny_Rhadangd sicr_Anscadu o_Arsym fod_Be yn_Arsym wir_Ebych i_Arsym Iwan_Epg am_Arsym y_YFB cyfnod_Egu nesaf_Anseith ._Atdt 
Yn_Arsym dilyn_Be y_YFB sefydlu_Be ,_Atdcan derbyniwyd_Bgorffamhers neges_Ebu drwy_Arsym fideo_Egu ar_Arsym sgriniau_Ebll 'r_YFB capel_Egu oddi_Arsym wrth_Arsym Y_YFB Parchg_Ep Walford_Ep Jones_Ep ,_Atdcan sy_Bpres3perth 'n_Arsym un_Rhaamh arall_Anscadu o_Arsym bump_Rhifol cyn-weinidog_Egu y_YFB Capel_Egu Newydd_Anscadu ,_Atdcan a_Cyscyd chafwyd_Bgorffamhers cymeradwyaeth_Ebu wresog_Anscadu i'w_Arsym neges_Ebu ,_Atdcan ac_Cyscyd yntau_Rhacys3gu 'n_Arsym 96_Gwdig mlwydd_Ebu oed_Egu ._Atdt 
Aelodaeth_Ebu Sefydlog_Anscadu 
Braf_Anscadu oedd_Bamherff3u clywed_Be ,_Atdcan yn_Arsym yr_YFB oes_Ebu sydd_Bpres3perth ohoni_Ar3bu ,_Atdcan fod_Be aelodaeth_Ebu yr_YFB Eglwys_Ebu wedi_Uberf aros_Be yn_Arsym gyson_Anscadu ers_Arsym troad_Egu y_YFB ganrif_Ebu gyda_Arsym thua_Arsym 130_Gwdig o_Arsym aelodau_Egll a_Cyscyd dros_Arsym 40_Gwdig o_Arsym blant_Egll a_Arsym phobl_Ebu ifanc_Anscadu ar_Arsym lyfrau_Egll 'r_YFB Ysgol_Ebu Sul_Egu ._Atdt 
Diddorol_Anscadu hefyd_Adf oedd_Bamherff3u cael_Be ein_Banmedd1ll hatgoffa_Egu yn_Arsym y_YFB cwrdd_Egu sefydlu_Be fod_Be 240_Gwdig aelod_Egu cyntaf_Anscadu y_YFB Capel_Egu Newydd_Anscadu wedi_Uberf cael_Be eu_Rhadib3ll '_Atddyf rhyddhau_Be '_Atddyf o_Arsym 'r_YFB fam_Ebu -_Atdcys eglwys_Ebu yn_Arsym y_YFB Tabernacl_Egbu ,_Atdcan Ffairfach_Gwest ,_Atdcan yn_Arsym 1902_Gwdig ._Atdt 
Cyfarchion_Egll a_Cyscyd Siars_Egbu 
Cafwyd_Bgorffamhers cyfarchion_Egll gan_Arsym Peter_Gwest Harries_Ep ar_Arsym ran_Ebu Cyfundeb_Egu Dwyrain_Egu Caerfyrddin_Ep a_Cyscyd Brycheiniog_Ep ;_Atdcan Bob_Banmeint Roberts_Gwest ar_Arsym ran_Ebu y_YFB Tabernacl_Egbu ,_Atdcan Ffairfach_Gwest ;_Atdcan a_Cyscyd Lynette_Ep Jones_Ep ar_Arsym ran_Ebu Philadelphia_Gwest ,_Atdcan Nantycaws_Gwest ,_Atdcan sef_Cyscyd y_YFB ddwy_Rhifol eglwys_Ebu y_Rhadib1u byddwn_Bdyf1ll yn_Arsym cydweithio_Be a_Cyscyd ̂_Gwsym nhw_Rhapers3ll dan_Arsym ofal_Egu ein_Banmedd1ll gweinidog_Egu newydd_Anscadu ._Atdt 
Daeth_Bgorff3u yr_YFB oedfa_Ebu i_Arsym ben_Egu gyda_Arsym phregeth_Ebu a_Arsym neges_Ebu bwrpasol_Anscadu gan_Arsym y_YFB Parchg_Ep Tom_Gwest Defis_Gwest ,_Atdcan yntau_Rhacys3gu hefyd_Adf yn_Arsym dymuno_Be pob_Egll iechyd_Egu ,_Atdcan hapusrwydd_Egu a_Cyscyd bendith_Ebu i_Arsym Iwan_Epg i_Arsym 'r_YFB dyfodol_Egu ,_Atdcan a_Cyscyd chasglwyd_Bgorffamhers at_Arsym Ape_Ep ̂_Gwsym l_Gwllyth Dwyrain_Egu Affrica_Epb Cymorth_Egu Cristnogol_Gwest ._Atdt 
Yn_Arsym dilyn_Be yr_YFB oedfa_Ebu ,_Atdcan cafodd_Bgorff3u lluniaeth_Egbu bendigedig_Anscadu ei_Rhadib3gu ddarparu_Be a_Cyscyd chyfle_Egu i_Arsym gymdeithasu_Be a_Cyscyd mwynhau_Be ._Atdt 
Diolch_Egu i_Arsym bawb_Rhaamh a_Rhaperth fu_Bgorff3u o_Arsym gymorth_Egu i_Arsym sicrhau_Be prynhawn_Egu i'w_Arsym gofio_Be a_Cyscyd phob_Banmeint dymuniad_Egu da_Anscadu i_Arsym Iwan_Epg a_Arsym 'i_Rhadib3gu deulu_Egu yn_Arsym ein_Banmedd1ll plith_Egu ._Atdt 
Cofiwch_Bgorch2ll fod_Be oedfa_Ebu ac_Cyscyd Ysgol_Ebu Sul_Egu yn_Arsym wythnosol_Anscadu am_Arsym 9.30_Gwdig am_Gwtalf a_Cyscyd chroeso_Egu cynnes_Anscadu i_Arsym bawb_Rhaamh ._Atdt 
(_Atdchw Cynhaliwyd_Bgorffamhers yr_YFB oedfa_Ebu ar_Arsym 25_Gwdig Mawrth_Egu )_Atdde 
Owain_Epg Sio_Be ̂_Gwsym n_Gwllyth Gruffydd_Epg 
BREXIT_Gwest -_Atdcys YMATEB_Be CRISTNOGOL_Gwacr 
Yn_Arsym Llanuwchllyn_Ep y_YFB llynedd_Adf ,_Atdcan cytunodd_Bgorff3u Undeb_Egu yr_YFB Annibynwyr_Egll i_Arsym wahodd_Egu enwadau_Egll Cymru_Epb i_Arsym ffurfio_Be gweithgor_Egu i_Arsym roi_Be mewnbwn_Egu Cristnogol_Gwest i_Arsym 'r_YFB drafodaeth_Ebu am_Gwtalf Brexit_Gwest ._Atdt 
Yn_Arsym dilyn_Be gwaith_Ebu aruthrol_Anscadu gan_Arsym Dr_Gwtalf Noel_Ep Davies_Gwest (_Atdchw isod_Adf )_Atdde ,_Atdcan mae_Bpres3u 'r_YFB gweithgor_Egu -_Atdcys sydd_Bpres3perth bellach_Adf o_Arsym dan_Egu adain_Ebu Cytu_Gwest ̂_Gwsym n_Gwllyth -_Atdcys wedi_Uberf rhoi_Be tystiolaeth_Ebu i_Arsym bedwar_Rhifol Pwyllgor_Egu Dethol_Be yn_Arsym y_YFB Cynulliad_Egu a_Cyscyd San_Egu Steffan_Epg ._Atdt 
Wrth_Arsym i_Arsym Brif_Anseith Weinidog_Egu Prydain_Epb ,_Atdcan Theresa_Gwest May_Gwest ,_Atdcan sbarduno_Be Erthygl_Ebu 50_Gwdig ,_Atdcan fe_Uberf wnaeth_Bgorff3u y_YFB gweithgor_Egu gyhoeddi_Be rhestr_Ebu o_Arsym egwyddorion_Ebll y_Rhadib1u credir_Bpresamhers ddylai_Bamherff3u fod_Be yn_Arsym sylfaen_Ebu i_Arsym safiad_Egu y_YFB Deyrnas_Ebu Gyfunol_Anscadu yn_Arsym ystod_Ebu y_YFB trafodaethau_Ebll sydd_Bpres3perth ar_Arsym gychwyn_Egu ._Atdt 
Maent_Bpres3ll yn_Arsym cynnwys_Egu amddiffyn_Be statws_Egu a_Cyscyd hawliau_Ebll plant_Egll ac_Cyscyd ieuenctid_Egu ,_Atdcan yr_YFB anabl_Anscadu a_Cyscyd 'r_YFB oedrannus_Anscadu ._Atdt 
Dywedant_Bpres3ll fod_Be rhaid_Egu parchu_Be hawliau_Ebll dynol_Anscadu parthed_Egu yr_YFB iaith_Ebu Gymraeg_Ebu a_Cyscyd lleiafrifoedd_Egll ethnig_Anscadu ,_Atdcan yn_Arsym ogystal_Anscyf a_Cyscyd ̂_Gwsym chyfreithiau_Ebll sy_Bpres3perth 'n_Uberf gwarchod_Be yr_YFB amgylchedd_Egu a_Cyscyd chefn_Egu gwlad_Ebu ,_Atdcan gan_Arsym gynnwys_Egu bywoliaeth_Ebu y_YFB rhai_Rhaamh sy_Bpres3perth 'n_Uberf gweithio_Be mewn_Arsym diwydiannau_Egll gwledig_Anscadu ._Atdt 
Mae_Bpres3u 'r_YFB adroddiad_Egu yn_Arsym ychwanegu_Be bod_Be angen_Egu i_Arsym 'r_YFB rhaniadau_Egll dyfnion_Anscadu a_Cyscyd amlygodd_Bgorff3u eu_Rhadib3ll hunain_Rhaatb yn_Arsym ystod_Ebu y_YFB refferendwm_Egu ar_Arsym yr_YFB Undeb_Egu Ewropeaidd_Anscadu (_Atdchw UE_Gwacr )_Atdde gael_Be eu_Rhadib3ll cydnabod_Be a_Cyscyd 'u_Rhadib3ll hiachau_Be ._Atdt 
Mentro_Be i_Arsym Ddyfroedd_Egll Dyfnion_Anscadu 
"_Atddyf Wrth_Arsym i_Arsym ni_Rhapers1ll fentro_Be i_Arsym ddyfroedd_Egll dieithr_Anscadu ,_Atdcan yn_Arsym wleidyddol_Anscadu ,_Atdcan yn_Arsym ddiwylliannol_Anscadu ac_Cyscyd yn_Arsym economaidd_Anscadu ,_Atdcan mae_Bpres3u 'n_Utra hanfodol_Anscadu ein_Rhadib1ll bod_Be ni_Rhapers1ll fel_Cyscyd Cristnogion_Egll yn_Arsym cyfrannu_Be mewn_Arsym modd_Egu adeiladol_Anscadu a_Cyscyd chadarn_Anscadu at_Arsym y_YFB ddadl_Ebu a_Arsym 'r_YFB broses_Ebu ddemocrataidd_Anscadu ,_Atdcan "_Atddyf meddai_Bamherff3u 'r_YFB Dr_Gwtalf Geraint_Egll Tudur_Ep ,_Atdcan Ysgrifennydd_Egu Cyffredinol_Anscadu Undeb_Egu yr_YFB Annibynwyr_Egll Cymraeg_Ebu ._Atdt 
"_Atddyf Wrth_Arsym i_Arsym adael_Be yr_YFB UE_Gwacr ddod_Be yn_Arsym realiti_Ebu ,_Atdcan rhaid_Egu i_Arsym ni_Rhapers1ll fod_Be yn_Arsym gefn_Egu i_Arsym bobl_Ebu y_Rhadib1u mae_Bpres3u eu_Banmedd3ll bywydau_Egll wedi_Uberf newid_Be mewn_Arsym ffyrdd_Egll na_Uneg fyddent_Bamod3ll fyth_Adf yn_Arsym eu_Rhadib3ll dymuno_Be ,_Atdcan "_Atddyf meddai_Bamherff3u Dr_Gwtalf Patrick_Ep Coyle_Gwest ,_Atdcan Cadeirydd_Egu Cytu_Gwest ̂_Gwsym n_Gwllyth ,_Atdcan ar_Arsym ran_Ebu y_YFB gweithgor_Egu ._Atdt 
"_Atddyf Rhaid_Egu deall_Be yr_YFB anniddigrwydd_Egu sydd_Bpres3perth wedi_Uberf ei_Rhadib3gu ddatguddio_Be mewn_Arsym cymunedau_Ebll sydd_Bpres3perth heb_Arsym elwa_Be o_Arsym 'r_YFB globaleiddio_unk a_Rhaperth ddigwyddodd_Bgorff3u a_Cyscyd chymryd_Be camau_Egll i_Arsym geisio_Be meithrin_Be datblygiad_Egu yr_YFB ardaloedd_Ebll hynny_Rhadangd ._Atdt 
Rhaid_Egu wynebu_Be 'r_YFB hiliaeth_Ebu a_Cyscyd 'r_YFB senoffobia_Egu cudd_Anscadu a_Rhaperth ddatgelwyd_Bgorffamhers gan_Arsym ymgyrch_Ebu y_YFB refferendwm_Egu ._Atdt 
Bydd_Bdyf3u rhaid_Egu i_Arsym 'r_YFB eglwysi_Ebll fod_Be yno_Adf gyda_Arsym 'r_YFB sawl_Rhaamh sydd_Bpres3perth angen_Egu eu_Rhadib3ll cysuro_Be a_Cyscyd 'u_Rhadib3ll calonogi_Be ac_Cyscyd ,_Atdcan yn_Arsym bennaf_Egu oll_Anscadu ,_Atdcan i_Arsym feithrin_Be heddwch_Egu a_Cyscyd chymod_Egu ._Atdt "_Atddyf 
O_Arsym ̂_Gwsym l_Gwllyth yr_Rhaperth Hoelion_Bgorff1ll 
D'oedd_Bamherff3u Tomos_Ep ddim_Egu hefo_Arsym nhw_Rhapers3ll pan_Egu ymddangosodd_Bgorff3u Iesu_Epg am_Arsym y_YFB tro_Egu cyntaf_Anscadu i_Arsym 'r_YFB disgyblion_Egll eraill_Anscadu ._Atdt 
Dyma_Adf nhw_Rhapers3ll yn_Arsym dweud_Be wrtho_Ar3gu ,_Atdcan yn_Arsym llawn_Anscadu brwdfrydedd_Egu yn_Arsym ystod_Ebu yr_YFB wythnos_Ebu ddilynol_Anscadu ,_Atdcan eu_Rhadib3ll bod_Be wedi_Uberf Ei_Rhadib3gu weld_Be a_Cyscyd ̂_Gwsym 'u_Banmedd3ll llygaid_Egll eu_Rhadib3ll hunain_Rhaatb ._Atdt 
Credai_Bamherff3u Tomos_Ep nad_Rhaperth oeddent_Bamherff3ll yn_Arsym llawn_Anscadu llathen_Ebu a_Cyscyd gwrthodai_Bamherff3u gredu_Be ._Atdt 
D'oedd_Bamherff3u pobl_Ebu marw_Anscadu ddim_Egu yn_Arsym dod_Be yn_Arsym o_Arsym ̂_Gwsym l_Gwllyth yn_Arsym fyw_Be ._Atdt 
Roedd_Bamherff3u o_Rhapers3gu yn_Arsym gwybod_Egu mai_Cyscyd pydru_Be mewn_Arsym bedd_Egu oedd_Bamherff3u hanes_Egu pawb_Rhaamh ._Atdt 
Er_Arsym mwyn_Egu iddo_Ar3gu gredu_Be hynny_Rhadangd roedd_Bamherff3u rhaid_Egu cael_Be tystiolaeth_Ebu goncrit_Egu ._Atdt 
Felly_Adf ,_Atdcan dywedodd_Bgorff3u wrthynt_Ar3ll nad_Rhaperth oedd_Bamherff3u am_Arsym gredu_Be oni_Cyscyd bai_Egu ei_Rhadib3gu fod_Be o_Rhapers3gu yn_Arsym gweld_Be Iesu_Epg a_Cyscyd ̂_Gwsym 'i_Rhadib3gu lygaid_Egll ei_Rhadib3gu hun_Rhaatb ._Atdt 
Yn_Arsym fwy_Rhaamh na_Cyscyd hynny_Rhadangd ,_Atdcan roedd_Bamherff3u 
eisiau_Egu gweld_Be o_Arsym ̂_Gwsym l_Gwllyth yr_Rhaperth hoelion_Bgorff1ll yn_Arsym ei_Rhadib3gu ddwylo_Ebll a_Cyscyd rhoi_Be ei_Rhadib3gu fys_Egu ym_Arsym mriw_Egu 'r_YFB bicell_Ebu a_Rhaperth wthiwyd_Bgorffamhers i_Rhapers1u mewn_Arsym i'w_Arsym ochr_Ebu ar_Arsym y_YFB groes_Ebu ._Atdt 
Wythnos_Ebu Gron_Anscadbu 
Aeth_Egu wythnos_Ebu gron_Anscadbu heibio_Arsym ._Atdt 
Daeth_Bgorff3u dilynwyr_Egll Iesu_Epg at_Arsym ei_Rhadib3gu gilydd_Egu eto_Adf yn_Arsym y_YFB stafell_Ebu lle_Anscadu yr_Rhaperth oedd_Bamherff3u wedi_Uberf bwyta_Be gwledd_Ebu y_YFB Pasg_Egu gyda_Arsym nhw_Rhapers3ll ._Atdt 
Roedd_Bamherff3u cymysgfa_Ebu o_Arsym ofn_Egu a_Cyscyd chyffro_Egu yn_Arsym eu_Banmedd3ll calonnau_Ebll ._Atdt 
Ofn_Egu yr_YFB awdurdodau_Egbll Rhufeinig_Anscadu ac_Cyscyd Iddewig_Anscadu ond_Arsym cyffro_Egu oherwydd_Cyscyd yr_YFB hanesion_Egll cynyddol_Anscadu am_Arsym bobl_Ebu oedd_Bamherff3u wedi_Uberf gweld_Be yr_YFB Iesu_Epg yn_Arsym fyw_Be ._Atdt 
Y_YFB tro_Egu yma_Adf roedd_Bamherff3u Tomos_Ep gyda_Arsym nhw_Rhapers3ll ._Atdt 
Shalo_Gwest ̂_Gwsym m_Gwllyth 
Er_Arsym bod_Be y_YFB drysau_Egll wedi_Uberf eu_Rhadib3ll cloi_Bamherff3u yn_Arsym dynn_Anscadu ,_Atdcan yn_Arsym ystod_Ebu gyda_Arsym 'r_YFB nos_Ebu ,_Atdcan ymddangosodd_Bgorff3u Iesu_Epg yn_Arsym eu_Banmedd3ll mysg_Egu ._Atdt 
Dywedodd_Bgorff3u wrthynt_Ar3ll ,_Atdcan "_Atddyf Shalo_Gwest ̂_Gwsym m_Gwllyth ,_Atdcan "_Atddyf -_Atdcys tangnefedd_Egbu i_Rhapers1u chi_Rhapers2ll ._Atdt 
Y_YFB tro_Egu hwn_Rhadangg cymerodd_Bgorff3u sylw_Egu arbennig_Anscadu o_Rhapers3gu Tomos_Epg ._Atdt 
Gwyddai_Bamherff3u 'r_YFB Arglwydd_Egu beth_Rhagof oedd_Bamherff3u ar_Arsym ei_Rhadib3gu galon_Ebu a_Cyscyd deliodd_Bgorff3u gyda_Arsym 'r_YFB anhawster_Egu oedd_Bamherff3u yn_Arsym ei_Rhadib3bu atal_Be rhag_Arsym credu_Be ._Atdt 
Meddai_Bamherff3u wrtho_Ar3gu ,_Atdcan "_Atddyf Edrych_Be ar_Arsym fy_Banmedd1u arddyrnau_Egbll a_Cyscyd 'm_Adf hochr_Ebu a_Arsym rho_Bgorch2u dy_Rhadib2u fys_Egu ar_Arsym y_YFB briwiau_unk ._Atdt 
Stopia_Bgorch2u amau_Egu !_Atdt 
Creda_Bgorch2u ._Atdt "_Atddyf 
Tystiolaeth_Ebu 
Yno_Ar3gu yn_Arsym sefyll_Be o_Arsym flaen_Egu Tomos_Ep yr_Rhaperth oedd_Bamherff3u tystiolaeth_Ebu goncrit_Egu ddeublyg_Anscadu a_Cyscyd chwalodd_Bgorff3u ei_Rhadib3bu 
angrhediniaeth_unk ._Atdt 
Yn_Arsym gyntaf_Rhitrefd ,_Atdcan ar_Arsym gorff_Egu yr_YFB Iesu_Epg yr_Rhaperth oedd_Bamherff3u arwyddion_Egbll o_Arsym 'i_Rhadib3bu aberth_Egu ar_Arsym 
y_YFB groes_Ebu dros_Arsym bechodau_Egll 'r_YFB byd_Egu ._Atdt 
O_Arsym ̂_Gwsym l_Gwllyth yr_Rhaperth hoelion_Bgorff1ll ._Atdt 
O_Arsym ̂_Gwsym l_Gwllyth y_YFB bicell_Ebu ._Atdt 
O_Arsym ̂_Gwsym l_Gwllyth y_YFB 
dioddef_Egu ._Atdt 
Hwn_Rhadangg oedd_Bamherff3u Iesu_Epg ,_Atdcan Oen_Egu Duw_Egu ._Atdt 
Bu_Bgorff3u farw_Anscadu yno_Adf yn_Arsym ein_Banmedd1ll lle_Rhagof ni_Rhapers1ll gan_Arsym dderbyn_Be y_YFB gosb_Ebu oedd_Bamherff3u 
yn_Arsym ddyledus_Anscadu i_Arsym ni_Rhapers1ll ._Atdt 
Dyma_Adf gariad_Egbu ,_Atdcan pwy_Rhagof a_Cyscyd 'i_Rhadib3bu traetha_Bgorch2u ?_Atdt 
Yn_Arsym 
ail_Rhitrefd ,_Atdcan roedd_Bamherff3u Iesu_Epg yno_Adf yn_Arsym fyw_Be er_Arsym ei_Rhadib3gu fod_Be wedi_Uberf marw_Anscadu ,_Atdcan yn_Arsym 
dystiolaeth_Ebu ddiymwad_Anscadu o_Arsym 'i_Rhadib3gu fuddugoliaeth_Ebu dros_Arsym angau_Egbu ._Atdt 
Wrth_Arsym 
weld_Be y_YFB dystiolaeth_Ebu chwalwyd_Bgorffamhers anffyddiaeth_Egbu Tomos_Epg a_Cyscyd phlygodd_Bgorff3u o_Arsym flaen_Egu yr_YFB Iesu_Epg gan_Arsym ddweud_Be ,_Atdcan "_Atddyf Fy_Banmedd1u Arglwydd_Egu a_Cyscyd 'm_Adf Duw_Egu ._Atdt "_Atddyf Credodd_Bgorff3u mai_Cyscyd Iesu_Epg oedd_Bamherff3u y_YFB Meseia_Egu a_Cyscyd newidiwyd_Bgorffamhers ei_Rhadib3gu fywyd_Egu yn_Arsym llwyr_Anscadu ._Atdt 
Beth_Rhagof yw_Bpres3u dy_Banmedd2u sefyllfa_Ebu di_Uberf heddiw_Adf o_Arsym flaen_Egu Duw_Egu wrth_Arsym ddarllen_Be y_YFB geiriau_Egll hyn_Rhadangd ?_Atdt 
Wyt_Bpres2u ti_Rhapers2u fel_Cyscyd Tomos_Epg yn_Arsym amheuwr_Egu ,_Atdcan yn_Arsym anghredadun_Anscadu ?_Atdt 
Mae_Bpres3u Iesu_Epg y_YFB Pasg_Egu hwn_Rhadangg yn_Arsym dy_Rhadib2u herio_Be i_Arsym gredu_Be ynddo_Ar3gu ac_Cyscyd i_Arsym roi_Be dy_Rhadib2u fywyd_Egu iddo_Ar3gu ._Atdt 
Mae_Bpres3u 'r_YFB dystiolaeth_Ebu welodd_Bgorff3u Tomos_Ep yr_YFB un_Rhaamh mor_Adf wir_Ebych heddiw_Adf ,_Atdcan mae_Bpres3u 'r_YFB Iesu_Epg wedi_Uberf marw_Anscadu trosom_Ar1ll ac_Cyscyd y_Rhaperth mae_Bpres3u Iesu_Epg heddiw_Adf 'n_Utra fyw_Be ._Atdt 
Wrth_Arsym gredu_Be ynddo_Ar3gu cawn_Egll faddeuant_Bpres3ll a_Cyscyd dechrau_Be ar_Arsym ein_Banmedd1ll perthynas_Ebu gyda_Arsym Duw_Egu ._Atdt 
Wrth_Arsym gredu_Be ynddo_Ar3gu cawn_Egll fywyd_Egu newydd_Anscadu a_Cyscyd bywyd_Egu tragwyddol_Anscadu yn_Arsym rhodd_Ebu ._Atdt 
Stopia_Bgorch2u Amau_Egu ._Atdt 
Creda_Bgorch2u a_Cyscyd llawenha_Bgorch2u ym_Arsym muddugoliaeth_Ebu Iesu_Epg ._Atdt 
Pasg_Egu hapus_Anscadu ._Atdt 
Efengyl_Ebu Ioan_Epg ,_Atdcan pennod_Anscadu 20_Gwdig ,_Atdcan adnodau_Egbll 24_Gwdig -_Atdcys 28_Gwdig 
MUDIAD_Egu CHWIORYDD_Ebll YR_YFB ANNIBYNWYR_Egll 
Adran_Ebu Dwyrain_Egu Caerfyrddin_Ep a_Cyscyd Brycheiniog_Ep 
CYFARFOD_Be BLYNYDDOL_Anscadu 
yng_Arsym Nghapel_Egu Bethania_Ep ,_Atdcan Tymbl_Ep Uchaf_Anscadu Nos_Ebu Fawrth_Egu ,_Atdcan 25_Gwdig Ebrill_Egu 2017_Gwdig 
am_Arsym 7.00_Gwdig yh_Gwtalf 
Siaradwr_Egu :_Atdcan Y_YFB Parchedig_Anscadu Jill_Epb -_Atdcys Hayley_Ep Harries_Ep 
Casgliad_Egu tuag_Arsym at_Arsym :_Atdcan Bugeiliaid_Egll y_YFB Stryd_Ebu a_Cyscyd Trais_Egu Domestig_Anscadu 
Llywydd_Egu :_Atdcan 
Mrs_Gwtalf Esme_Ep Lloyd_Epg 
Ysgrifennydd_Egu :_Atdcan 
Miss_Ebu Ann_Ebu Davies_Gwest 
Trysorydd_Egu :_Atdcan 
Mrs_Gwtalf Eirlys_Egu Morgan_Epg Pris_Egu y_YFB Rhaglen_Ebu :_Atdcan 50_Gwdig c_Gwllyth 
Cynhadledd_Ebu Cymdeithas_Be Annibynwyr_Egll y_YFB Byd_Egu 
Cynhelir_Bpresamhers cynhadledd_Ebu Cymdeithas_Be Annibynwyr_Egll y_YFB Byd_Egu ym_Arsym Mhrifysgol_Ebu Stellenbosch_Gwest ger_Egu Cape_Ep Town_Bamherff1u ,_Atdcan De_Egu Affrica_Epb ,_Atdcan 6_Gwdig -_Atdcys 11_Gwdig Gorffennaf_Egu eleni_Adf ._Atdt 
Gan_Arsym fod_Be y_YFB trefnwyr_Egll yn_Arsym gwahodd_Egu pobl_Ebu i_Arsym gofrestru_Be ,_Atdcan gofynnir_Bpresamhers i_Arsym 'r_YFB sawl_Rhaamh sydd_Bpres3perth a_Cyscyd ̂_Gwsym diddordeb_Egu i_Arsym fynd_Be yno_Adf i_Arsym gysylltu_Be a_Cyscyd ̂_Gwsym Thy_Egu ̂_Gwsym John_Epg Penri_Ep cyn_Adf gynted_Anscyf a_Arsym ̂_Gwsym phosibl_Anscadu er_Arsym mwyn_Egu i_Arsym ni_Rhapers1ll fedru_Be anfon_Be taflenni_Ebll gwybodaeth_Ebu a_Cyscyd ffurflen_Ebu gofrestru_Be atoch_Ar2ll ._Atdt 
Gellir_Bpresamhers cael_Be mwy_Rhaamh o_Arsym wybodaeth_Ebu am_Arsym y_YFB Gymdeithas_Ebu ac_Cyscyd am_Arsym y_YFB gynhadledd_Ebu trwy_Arsym ymweld_Be a_Cyscyd ̂_Gwsym 'r_YFB [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Barn_Ebu Annibynnol_Anscadu 
Ffasiwn_Egu a_Cyscyd Marchnata_Be 
Wyddoch_Bpres2ll chi_Rhapers2ll y_Rhadib1u bu_Bgorff3u 'r_YFB Undeb_Egu ar_Arsym y_YFB blaen_Egu ym_Arsym myd_Egu ffasiwn_Egu ?_Atdt 
Dwy_Rhifol ddim_Egu yn_Arsym gwybod_Egu pwy_Egu oedd_Bamherff3u yn_Arsym gyfrifol_Anscadu am_Arsym fagiau_Egll '_Atddyf Diwrnod_Egu i_Arsym 'r_YFB Brenin_Egu '_Atddyf yn_Arsym 2012_Gwdig ,_Atdcan ond_Arsym roedd_Bamherff3u ar_Arsym y_YFB blaen_Egu nid_Uneg yn_Arsym unig_Anscadu ym_Arsym myd_Egu ffasiwn_Egu ond_Arsym ym_Arsym myd_Egu marchnata_Be hefyd_Adf ._Atdt 
Eurwen_Ep Richards_Ep 
Bagiau_Egll 
Bu_Bgorff3u bagiau_Egll (_Atdchw fel_Cyscyd Gucci_Gwest ,_Atdcan Vivienne_Ep Westwood_Gwest a_Cyscyd 'u_Rhadib3ll tebyg_Anscadu )_Atdde yn_Arsym symbolau_Egll pwysig_Anscadu i_Arsym 'r_YFB wraig_Ebu ffasiynol_Anscadu ,_Atdcan yn_Arsym enwedig_Anscadu o_Arsym 'i_Rhadib3bu statws_Egu personol_Anscadu ._Atdt 
Yn_Arsym ddiweddar_Anscadu ,_Atdcan daeth_Bgorff3u y_YFB bagiau_Egll sy_Bpres3perth 'n_Uberf cael_Be eu_Rhadib3ll defnyddio_Be i_Arsym gario_Be bwyd_Egu a_Cyscyd nwyddau_Egll o'n_Bamherff1u siopau_Ebll ,_Atdcan yn_Arsym ddatganiad_Egu pwysig_Anscadu o_Arsym deyrngarwch_Egu pobl_Ebu i'w_Arsym cred_Ebu bersonol_Anscadu ,_Atdcan boed_Bgorch3u yn_Arsym grefyddol_Anscadu neu_Cyscyd 'n_Utra wleidyddol_Anscadu ,_Atdcan heb_Gwtalf so_Adf ̂_Gwsym n_Gwllyth am_Arsym gymdeithasol_Anscadu ._Atdt 
Daeth_Bgorff3u y_YFB neges_Ebu ar_Arsym y_YFB bag_Egu yn_Arsym fodd_Egu i_Arsym greu_Be '_Atddyf brand_Egu '_Atddyf a_Cyscyd theyrngarwch_Egu heb_Gwtalf ei_Banmedd3gu ail_Rhitrefd ._Atdt 
Diwrnod_Egu i_Arsym 'r_YFB Brenin_Egu 
Ys_Cyscyd gwn_Egu i_Rhapers1u faint_Egu o_Arsym fagiau_Egll '_Atddyf Diwrnod_Egu i_Arsym 'r_YFB Brenin_Egu '_Atddyf sydd_Bpres3perth ar_Arsym gael_Be erbyn_Be hyn_Rhadangd ,_Atdcan a_Cyscyd faint_Rhaamh ohonynt_Ar3ll sy_Bpres3perth 'n_Uberf cael_Be eu_Rhadib3ll defnyddio_Be ?_Atdt 
Nid_Uneg oedd_Bamherff3u y_YFB rhai_Rhaamh cyntaf_Rhitrefd yn_Arsym addas_Anscadu ond_Arsym i_Arsym ddal_Be Caneuon_Ebll Ffydd_Ebu ._Atdt 
Gwelwyd_Bgorffamhers yn_Arsym syth_Anscadu bod_Be angen_Egu un_Rhifold mwy_Rhaamh o_Arsym faint_Egu a_Cyscyd dyna_Adf gafwyd_Bgorffamhers ._Atdt 
Aeth_Egu pum_Rhifol mlynedd_Ebll heibio_Arsym ,_Atdcan a_Cyscyd bellach_Adf does_Bpres3amhen neb_Rhaamh bron_Ebu yn_Arsym cofio_Be am_Arsym ,_Atdcan nac_Cyscyd yn_Arsym defnyddio_Be bag_Egu '_Atddyf Diwrnod_Egu i_Arsym 'r_YFB Brenin_Egu '_Atddyf ._Atdt 
Beth_Rhagof aeth_Bgorff3u o_Arsym 'i_Rhadib3gu le_Rhagof ?_Atdt 
Ymateb_Be y_YFB rhai_Rhaamh sy_Bpres3perth 'n_Uberf cynllunio_Be '_Atddyf brand_Egu '_Atddyf yw_Bpres3u nad_Rhaperth oedd_Bamherff3u y_YFB geiriau_Egll noeth_Anscadu '_Atddyf Diwrnod_Egu i_Arsym 'r_YFB Brenin_Egu '_Atddyf yn_Arsym ddigon_Egu o_Arsym hysbys_Anscadu ,_Atdcan yn_Arsym ddigon_Egu i_Arsym dynnu_Be sylw_Egu ._Atdt 
Beth_Rhagof sydd_Bpres3perth ,_Atdcan neu_Cyscyd oedd_Bamherff3u ei_Rhadib3bu angen_Egu ,_Atdcan oedd_Bamherff3u '_Atddyf logo_Egu '_Atddyf yn_Arsym ogystal_Anscyf a_Cyscyd ̂_Gwsym 'r_YFB geiriau_Egll ._Atdt 
Mae_Bpres3u 'na_Adf '_Atddyf logo_Egu '_Atddyf gennym_Ar1ll ,_Atdcan ac_Cyscyd fe_Uberf gofiwn_Bamherff1u amdano_Ar3gu yn_Arsym arbennig_Anscadu adeg_Ebu y_YFB Pasg_Egu ,_Atdcan ond_Arsym i_Arsym lawer_Rhaamh mae_Bpres3u defnyddio_Be 'r_YFB Groes_Ebu ar_Arsym fag_Egu siopa_Be neu_Cyscyd fel_Cyscyd darn_Egu o_Arsym emwaith_Egu yn_Arsym wrthwn_unk ._Atdt 
Brandpower_Gwest 
Daw_Bpres3u gwybodaeth_Ebu i_Arsym 'r_YFB ty_Egu ̂_Gwsym bron_Ebu yn_Arsym wythnosol_Anscadu a_Arsym ̂_Gwsym chyfarwyddiadau_Egll a_Arsym fydd_Bdyf3u ,_Atdcan y_Rhadib1u dywedir_Bpresamhers ,_Atdcan o_Arsym gymorth_Egu i_Rhapers1u mi_Rhapers1u farchnata_Be fy_Banmedd1u musnes_Egbu yn_Arsym fwy_Rhaamh effeithiol_Anscadu ._Atdt 
Ymysg_Arsym y_Rhadib1u diweddara_Bgorch2u oedd_Bamherff3u yr_YFB angen_Egu i_Arsym greu_Be '_Atddyf brandpower_unk '_Atddyf ._Atdt 
Bydd_Bdyf3u hwn_Rhadangg meddai_Bamherff3u 'r_YFB arbenigwr_Egu ym_Arsym myd_Egu marchnata_Be ,_Atdcan yn_Arsym sicrhau_Be gwelliant_Egu ,_Atdcan ac_Cyscyd yn_Arsym fy_Rhadib1u ngalluogi_Be i_Arsym wneud_Be yn_Arsym well_Anscym -_Atdcys beth_Rhagof bynnag_Anscadu yw_Bpres3u y_YFB gwell_Anscym hwnnw_Rhadangg ._Atdt 
Anwybodaeth_Ebu 
Rhai_Banmeint dyddiau_Egll yn_Arsym o_Arsym ̂_Gwsym l_Gwllyth cefais_Bgorff1u fy_Rhadib1u atgoffa_Be o_Arsym rai_Rhaamh o_Arsym ystadegau_Ebll 'r_YFB Feibl_Egu Gymdeithas_Ebu am_Arsym y_YFB diffyg_Egu gwybodaeth_Ebu sydd_Bpres3perth ymysg_Arsym ein_Banmedd1ll plant_Egll a_Cyscyd 'n_Utra pobl_Ebu ifanc_Anscadu am_Arsym hanesion_Egll y_YFB Beibl_Egu ._Atdt 
Mae_Bpres3u 'r_YFB ffigurau_Ebll yn_Arsym echrydus_Anscadu ,_Atdcan ac_Cyscyd os_Cyscyd aiff_Bpres3u pethe_Egll yn_Arsym eu_Banmedd3ll blaen_Egu fel_Cyscyd hyn_Rhadangd ,_Atdcan canran_Egu bychan_Anscadu o_Arsym Gristnogion_Egll fydd_Bdyf3u yng_Arsym Nghymru_Epb mewn_Arsym degawd_Egu ._Atdt 
Ffo_Egu ̂_Gwsym n_Gwllyth symudol_Anscadu 
Mae_Bpres3u '_Atddyf brandpower_unk '_Atddyf mwyaf_Anseith ein_Banmedd1ll byd_Egu yn_Arsym ein_Banmedd1ll dwylo_Ebll ,_Atdcan ond_Arsym nid_Uneg ydym_Bpres1ll yn_Arsym ei_Rhadib3gu ddefnyddio_Be ._Atdt 
Y_YFB mae_Bpres3u ffasiynau_Egll yn_Arsym newid_Be ,_Atdcan ond_Arsym nid_Uneg yw_Bpres3u 'r_YFB dasg_Ebu o_Arsym farchnata_Be ._Atdt 
Nid_Uneg ydym_Bpres1ll fel_Cyscyd Cristnogion_Egll wedi_Uberf dysgu_Be 'r_YFB wers_Ebu i_Arsym ddefnyddio_Be offer_Egll y_YFB byd_Egu i_Rhapers1u 'n_Utra pwrpas_Egu ni_Rhapers1ll ,_Atdcan i_Rhapers1u so_Adf ̂_Gwsym n_Gwllyth am_Arsym Grist_Epg a_Cyscyd 'i_Rhadib3gu farwol_Anscadu glwy_Egu ._Atdt 
Diflannodd_Bgorff3u yr_YFB ysgol_Ebu Sul_Egu mewn_Arsym rhannau_Ebll helaeth_Anscadu o'n_Bamherff1u gwlad_Ebu ._Atdt 
Nid_Uneg yw_Bpres3u plant_Egll yn_Arsym arfer_Egu dod_Be i_Arsym 'r_YFB cwrdd_Egu ,_Atdcan ac_Cyscyd mae_Bpres3u oedfa_Ebu yn_Arsym ddieithr_Egu i_Arsym lawer_Rhaamh heddiw_Adf ._Atdt 
Rwy_Bpres1u 'n_Utra siw_Egu ̂_Gwsym r_Gwllyth bod_Be eraill_Rhaamh fel_Cyscyd fi_Rhapers1u wedi_Uberf cael_Be y_YFB profiad_Egu o_Arsym fod_Be mewn_Arsym ystafell_Ebu o_Arsym bobl_Ebu ifanc_Anscadu a_Cyscyd phawb_Rhaamh bron_Ebu yn_Arsym '_Atddyf siarad_Be '_Atddyf a_Cyscyd ̂_Gwsym 'i_Rhadib3gu gilydd_Egu trwy_Arsym tecstio_unk ._Atdt 
Mae_Bpres3u 'r_YFB ffo_Egu ̂_Gwsym n_Gwllyth symudol_Anscadu yma_Adf i_Arsym aros_Be ,_Atdcan wel_Ebych ar_Arsym hyn_Rhadangd o_Arsym bryd_Egu ._Atdt 
Darfu_Be cyfnod_Egu yr_YFB areithwyr_unk mawr_Anscadu ,_Atdcan a_Cyscyd phobl_Ebu yn_Arsym tyrru_Be i_Arsym wrando_Be ar_Arsym bregethwyr_Egll o_Arsym fri_Egu ._Atdt 
Cyfryngau_Egll at_Arsym y_YFB Cyfryngwr_Egu 
Mae_Bpres3u angen_Egu ffeindio_Be 'r_YFB ffordd_Ebu i_Arsym farchnata_Be 'r_YFB '_Atddyf brandpower_unk '_Atddyf yn_Arsym y_YFB byd_Egu rydym_Bpres1ll yn_Arsym byw_Be ynddo_Ar3gu heddiw_Adf ._Atdt 
Diolch_Egu am_Arsym Feibl_Egu ._Atdt net_Gwest a_Cyscyd 'r_YFB cyfle_Egu sydd_Bpres3perth gan_Arsym ein_Banmedd1ll plant_Egll i_Arsym ddarganfod_Be gair_Egu Duw_Egu ar_Arsym eu_Banmedd3ll ffonau_Egll symudol_Anscadu ._Atdt 
Faint_Rhaamh ohonom_Ar1ll sy_Bpres3perth 'n_Uberf defnyddio_Be '_Atddyf Facebook_Gwest '_Atddyf a_Cyscyd '_Atddyf Twitter_Gwest '_Atddyf ?_Atdt 
Mae_Bpres3u 'n_Utra dda_Anscadu cael_Be ambell_Banmeint drydar_Egbu gan_Arsym ein_Banmedd1ll Hysgrifennydd_Egu Cyffredinol_Anscadu ,_Atdcan ac_Cyscyd rwy_Bpres1u 'n_Uberf synhwyro_Be fod_Be y_YFB negeseuau_unk hyn_Rhadangd yn_Arsym cyrraedd_Be mwy_Rhaamh o_Arsym bobl_Ebu nag_Cyscyd yw_Bpres3u 'r_YFB Tyst_Egu !_Atdt 
Mae_Bpres3u angen_Egu arnom_Ar1ll i_Arsym fynd_Be ati_Ar3bu o_Arsym 'r_YFB newydd_Anscadu a_Arsym defnyddio_Be 'r_YFB offer_Egll sydd_Bpres3perth gennym_Ar1ll heddiw_Adf i_Arsym farchnata_Be 'r_YFB newydd_Anscadu am_Arsym Iesu_Epg a_Cyscyd 'i_Rhadib3gu farwol_Anscadu glwy_Egu ._Atdt 
Mae_Bpres3u adnoddau_Egll 'n_Uberf cael_Be eu_Rhadib3ll darparu_Be yn_Arsym y_YFB Gymraeg_Ebu ,_Atdcan adnoddau_Egll ac_Cyscyd offer_Egll sy_Bpres3perth 'n_Uberf apelio_Be i_Rhapers1u 'n_Utra pobl_Ebu ifanc_Anscadu ._Atdt 
Da_Anscadu chi_Rhapers2ll bobl_Ebu hy_Anscadu ̂_Gwsym n_Gwllyth ,_Atdcan peidiwch_Bgorch2ll ceisio_Be cadw_Be 'r_YFB oes_Ebu euraidd_Anscadu a_Rhaperth fu_Bgorff3u ._Atdt 
Gadewch_Bgorch2ll i_Arsym ni_Rhapers1ll sicrhau_Be fod_Be ein_Banmedd1ll plant_Egll yn_Arsym cael_Be y_YFB cyfle_Egu i_Arsym wybod_Egu am_Arsym ac_Cyscyd i_Arsym ddod_Be i_Arsym gredu_Be yn_Arsym y_YFB Crist_Epg ._Atdt 
"_Atddyf O_Rhapers3gu boed_Bgorch3u i_Arsym blant_Egll ein_Rhadib1ll hoes_Bpres3amhen a_Cyscyd 'n_Utra gwlad_Ebu ,_Atdcan fel_Cyscyd cynt_Anscadu gwna_Bgorch2u ̂_Gwsym i_Rhapers1u plant_Egll Caersalem_Gwest dref_Ebu ,_Atdcan roi_Be clod_Egbu a_Cyscyd mawl_Bpres3u i_Arsym 'r_YFB Iesu_Epg mad_Anscadu :_Atdcan Hosanna_Gwest iddo_Ar3gu ef_Rhaatb ._Atdt 
Eurwen_Ep Richards_Ep 
(_Atdchw Nid_Uneg yw_Bpres3u cynnwys_Egu y_YFB Barn_Ebu Annibynnol_Anscadu o_Arsym reidrwydd_Egu yn_Arsym adlewyrchu_Be safbwynt_Egu Undeb_Egu yr_YFB Annibynwyr_Egll na_Uneg 'r_YFB ti_Rhapers2u ̂_Gwsym m_Gwllyth golygyddol_Anscadu ._Atdt )_Atdde 
Pe_Cyscyd ̂_Gwsym r_Gwllyth Ganiadau_Egbll 
'_Atddyf Mi_Uberf dafla_Bpres3u maich_Egbu oddi_Arsym ar_Arsym fy_Banmedd1u ngwar_Egu '_Atddyf 
Roedd_Bamherff3u Pantycelyn_Ep yn_Arsym Gristion_Egu ac_Cyscyd yn_Arsym hyddysg_Anscadu iawn_Adf yn_Arsym Y_YFB Gair_Egu ._Atdt 
Roedd_Bamherff3u hefyd_Adf yn_Arsym llenor_Egu ._Atdt 
Nid_Uneg oes_Bpres3amhen syndod_Egu o_Arsym gwbl_Egu fod_Be Syr_Egu John_Epg Morris_Ep Jones_Ep wedi_Uberf awgrymu_Be fod_Be yr_YFB emyn_Egu dan_Arsym sylw_Egu 'n_Utra '_Atddyf delyneg_Ebu berffaith_Anscadu '_Atddyf ._Atdt 
Thema_Ebu amlwg_Anscadu yng_Arsym ngwaith_Ebu William_Epg Williams_Gwest yw_Bpres3u honno_Rhadangb am_Arsym fywyd_Egu fel_Cyscyd pererindod_Egbu ._Atdt 
Awn_Bamherff1u ar_Arsym daith_Ebu gyda_Arsym 'r_YFB emynydd_Egll o_Arsym bennill_Egu i_Arsym bennill_Egu ,_Atdcan o_Arsym wirionedd_Egu i_Arsym wirionedd_Egu ,_Atdcan a_Cyscyd chlywn_Bamherff1u yr_YFB awdur_Egu yn_Arsym traethu_Be gydag_Arsym angerdd_Egu ac_Cyscyd argyhoeddiad_Egu llwyr_Anscadu am_Arsym aruthredd_Egu aberth_Egu y_YFB Groes_Ebu ._Atdt 
Llwyddodd_Bgorff3u i_Arsym gynnwys_Egu popeth_Egu yn_Arsym yr_YFB emyn_Egu hwn_Rhadangg ._Atdt 
Cychwyn_Egu 
Man_Egbu cychwyn_Be y_YFB daith_Ebu yw_Bpres3u delwedd_Ebu sy_Bpres3perth 'n_Uberf adleisio_Be 'r_YFB profiad_Egu a_Rhaperth gafodd_Bgorff3u Cristion_Egu yng_Arsym ngwaith_Ebu enwog_Anscadu John_Epg Bunyan_Gwest ,_Atdcan Taith_Ebu y_YFB Pererin_Egu -_Atdcys gwaith_Ebu a_Rhaperth oedd_Bamherff3u yn_Arsym arbennig_Anscadu o_Arsym boblogaidd_Anscadu yng_Arsym Nghymru_Epb yn_Arsym y_YFB ddeunawfed_Rhitrefd ganrif_Ebu -_Atdcys a_Cyscyd Cristion_Egu a_Arsym 'i_Rhadib3gu bwn_Egu yn_Arsym cael_Be ei_Rhadib3gu godi_Bpres2u neu_Cyscyd ei_Rhadib3gu ddiosg_Be wrth_Arsym y_YFB Groes_Ebu ._Atdt 
Cawn_Bamherff1u weld_Be yma_Bandang onestrwydd_Egu yr_YFB emynydd_Egll am_Gwtalf ei_Rhadib3gu euogrwydd_Egu ei_Rhadib3gu hun_Rhaatb a_Cyscyd gre_Ebu ̈_Gwsym wyd_Egu gan_Arsym bechod_Egu ,_Atdcan ac_Cyscyd am_Arsym gariad_Egbu Duw_Egu ar_Arsym waith_Ebu ynom_Ar1ll yn_Arsym ein_Rhadib1ll hiacha_Be ́_Gwsym u_Gwllyth o_Arsym 'r_YFB boen_Egbu o_Arsym gario_Be 'n_Utra pechod_Egu ._Atdt 
Mi_Uberf dafla_Bpres3u '_Atddyf '_Atddyf maich_Egbu oddi_Arsym ar_Arsym fy_Banmedd1u ngwar_Egu wrth_Arsym deimlo_Be dwyfol_Anscadu loes_Ebu ;_Atdcan euogrwydd_Egu fel_Cyscyd mynyddoedd_Egll byd_Egu dry_Bpres3u 'n_Uberf ganu_Be wrth_Arsym dy_Egu groes_Ebu ._Atdt 
Mawredd_Egu Crist_Epg 
Yn_Arsym wir_Ebych ,_Atdcan mae_Bpres3u 'r_YFB emyn_Egu trwyddo_Ar3gu yn_Arsym canolbwyntio_Be ar_Arsym y_YFB berthynas_Ebu rhwng_Arsym y_YFB pererin_Egu a_Cyscyd 'i_Rhadib3gu Waredwr_Egu ac_Cyscyd ar_Arsym rinweddau_Egbll Croes_Ebu Calfaria_Gwest a_Arsym chawn_Egll ein_Banmedd1ll cymell_Be i_Arsym rannu_Be 'r_YFB profiad_Egu gyda_Arsym 'r_YFB emynydd_Egll ._Atdt 
Does_Bpres3amhen neb_Rhaamh yn_Arsym dod_Be yn_Arsym agos_Anscadu at_Arsym fawredd_Egu Crist_Epg ._Atdt 
Mae_Bpres3u Iesu_Epg yn_Arsym anghymharol_Anscadu ._Atdt 
Os_Cyscyd edrych_Be wnaf_Bpres1u i_Rhapers1u 'r_YFB dwyrain_Egu draw_Adf os_Egu edrych_Be wnaf_Bpres1u i_Rhapers1u 'r_YFB de_Egu ,_Atdcan 
ymhlith_Arsym a_Rhaperth fu_Bgorff3u ,_Atdcan neu_Cyscyd ynteu_Cyscyd ddaw_Bpres3u ,_Atdcan '_Atddyf does_Bpres3amhen debyg_Anscadu iddo_Ar3gu fe_Rhapers3gu ._Atdt 
Trawsnewidydd_Egu 
Crist_Epg sy_Bpres3perth 'n_Uberf trawsnewid_Be bywydau_Egll yw_Bpres3u 'r_YFB Crist_Epg byw_Anscadu ._Atdt 
Rwy_Bpres1u 'n_Utra hoff_Anscadu iawn_Adf o_Arsym 'r_YFB llinellau_Ebll yn_Arsym yr_YFB emyn_Egu sy_Bpres3perth 'n_Uberf pwysleisio_Be 'r_YFB trawsnewidiad_Egu ym_Arsym mywyd_Egu y_YFB sawl_Anscadu a_Rhaperth ddaw_Bpres3u at_Arsym y_YFB Groes_Ebu -_Atdcys try_Bpres3u 'r_YFB brwnt_Anscadu yn_Arsym wyn_Egll ,_Atdcan y_YFB gwan_Anscadu yn_Arsym nerthol_Anscadu ,_Atdcan y_YFB marw_Anscadu yn_Arsym fyw_Be ._Atdt 
Fe_Uberf roes_Bgorff1u ei_Rhadib3gu ddwylo_Ebll pur_Anscadu ar_Arsym led_Egu ,_Atdcan fe_Uberf wisgodd_Bgorff3u goron_Ebu ddrain_Egbll ,_Atdcan er_Arsym mwyn_Egu i_Arsym 'r_YFB brwnt_Anscadu gael_Be bod_Be yn_Arsym wyn_Egll fel_Cyscyd hyfryd_Anscadu liain_Anscadu main_Anscadu ._Atdt 
Esgyn_Be a_Rhaperth wnaeth_Bgorff3u i_Rhapers1u entrych_unk nef_Ebu i_Arsym eiriol_Anscadu dros_Arsym y_YFB gwan_Anscadu ;_Atdcan 
fe_Uberf sugna_Bgorch2u f'enaid_unk innau_Rhacys1u 'n_Utra la_Egbu ̂_Gwsym n_Gwllyth i'w_Arsym fynwes_Ebu yn_Arsym y_YFB man_Egbu ._Atdt 
Ei_Rhadib3bu hyfryd_Anscadu wedd_Ebu 
Mae_Bpres3u grym_Egu y_YFB groes_Ebu yn_Arsym abl_Anscadu i_Arsym drawsnewid_Egu unigolyn_Egu yn_Arsym llwyr_Anscadu ,_Atdcan profiad_Egu y_Rhadib1u cafodd_Bgorff3u William_Ep Williams_Gwest flas_Egu ohono_Ar3gu ar_Arsym o_Arsym ̂_Gwsym l_Gwllyth y_YFB bore_Egu bythgofiadwy_Anscadu hwnnw_Rhadangg pan_Cyscyd glywodd_Bgorff3u lais_Egu Duw_Egu ym_Arsym mhregeth_Ebu Howell_Ep Harris_Ep ym_Arsym mynwent_Ebu Talgarth_Ep ._Atdt 
Ond_Arsym gwyddai_Egu hefyd_Adf mai_Cyscyd datblygiad_Egu graddol_Anscadu yn_Arsym y_YFB ffydd_Ebu yw_Bpres3u rhan_Ebu pob_Anscadu Cristion_Egu ,_Atdcan a_Cyscyd phechod_Egu a_Arsym gras_Egbu yn_Arsym taro_Egu 'n_Uberf erbyn_Be ei_Rhadib3gu gilydd_Egu yn_Arsym ei_Banmedd3gu enaid_Egu cyn_Arsym cyrraedd_Be pen_Egu y_YFB daith_Ebu ._Atdt 
Mae_Bpres3u gwynfyd_Egu y_YFB diweddglo_Egu 'n_Utra syfrdanol_Anscadu :_Atdcan 
Ac_Cyscyd yna_Adf caf_Bpres1u fod_Be gydag_Arsym ef_Rhaatb 
pan_Egu e_Rhapers3gu ̂_Gwsym l_Gwllyth y_YFB byd_Egu ar_Arsym da_Egu ̂_Gwsym n_Gwllyth ,_Atdcan 
ac_Cyscyd edrych_Be yn_Arsym ei_Rhadib3gu hyfryd_Anscadu wedd_Ebu ,_Atdcan gan_Arsym '_Atddyf harddach_unk nag_Cyscyd o_Arsym 'r_YFB blaen_Egu ._Atdt 
Diolch_Egu am_Arsym fynegiant_Egu godidog_Anscadu o_Arsym ffydd_Ebu unigolyn_Egu i_Arsym 'r_YFB Gwaredwr_Egu a_Rhaperth garodd_Bgorff3u gymaint_Anscadu ._Atdt 
Beti_Bpres2u -_Atdcys Wyn_Egll James_Gwest 
Erthyglau_Ebll ,_Atdcan llythyrau_Egll at_Arsym y_YFB Prif_Anseith Olygydd_Egu :_Atdcan 
Y_YFB Parchg_Ep Ddr_Gwtalf Alun_Ep Tudur_Ep [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Golygyddion_Egll 
Y_YFB Parchg_Ep Iwan_Epg Llewelyn_Ep Jones_Ep [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Alun_Ep Lenny_Epg [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Archebion_Ebll a_Cyscyd thaliadau_Egll i_Arsym 'r_YFB Llyfrfa_Ep :_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Dalier_Bgorchamhers Sylw_Egu !_Atdt 
Cyhoeddir_Bpresamhers y_YFB Pedair_Rhifol Tudalen_Egbu Gydenwadol_Gwest gan_Arsym Bwyllgor_Egu Llywio_Be 'r_YFB Pedair_Rhifol Tudalen_Egbu ac_Cyscyd nid_Uneg gan_Arsym Undeb_Egu yr_YFB Annibynwyr_Egll Cymraeg_Ebu ._Atdt 
Nid_Uneg oes_Bpres3amhen a_Rhaperth wnelo_Bdibdyf3u Golygyddion_Egll Y_YFB Tyst_Egu ddim_Egu â_Arsym chynnwys_Egu y_YFB Pedair_Rhifol Tudalen_Egbu ._Atdt 
Eglwys_Ebu 
Y_YFB TABERNACL_Egbu MAENCLOCHOG_Gwacr 
Ar_Arsym ddiwedd_Egu oedfa_Ebu Sul_Egu 19_Gwdig eg_Gwtalf Chwefror_Egu ,_Atdcan cyflwynwyd_Bgorffamhers llun_Egu i_Rhapers1u Denzil_Ep Jenkins_Ep i_Arsym ddangos_Be ein_Banmedd1ll gwerthfawrogiad_Egu am_Gwtalf ei_Rhadib3gu wasanaeth_Be diflino_Anscadu a_Cyscyd chydwybodol_Anscadu fel_Cyscyd Ysgrifennydd_Egu Ariannol_Anscadu yr_YFB eglwys_Ebu am_Arsym dros_Arsym ddeugain_Rhifold mlynedd_Ebll ._Atdt 
Un_Rhaamh o_Arsym fugeiliaid_Egll mynydd_Egu y_YFB Preseli_Ep yw_Bpres3u Denzil_Ep felly_Adf mae_Bpres3u 'r_YFB llun_Egu yn_Arsym un_Rhaamh pwrpasol_Anscadu i_Arsym fugail_Egu gofalus_Anscadu ._Atdt 
Y_YFB Parchedig_Anscadu Ken_Ep Thomas_Epg wnaeth_Bgorff3u gyflwyno_Be 'r_YFB llun_Egu ,_Atdcan ac_Cyscyd yntau_Rhacys3gu wedi_Arsym cyd-_Ublaen weithio_Be yn_Arsym hwylus_Anscadu a_Cyscyd ̑_Gwsym Denzil_Gwest ,_Atdcan fel_Cyscyd trysorydd_Egu yr_YFB eglwys_Ebu am_Arsym 35_Gwdig mlynedd_Ebll ,_Atdcan cyn_Arsym iddo_Ar3gu yntau_Rhacys3gu gael_Be ei_Rhadib3bu ordeinio_Be a_Cyscyd 'i_Rhadib3bu sefydlu_Be 'n_Utra weinidog_Egu ar_Arsym Gymdeithas_Ebu Annibynwyr_Egll Bro_Ebu Cerwyn_Gwest yn_Arsym 2014_Gwdig ._Atdt 
K_Gwllyth ._Atdt T_Gwllyth ._Atdt 
Cyhoeddwyd_Bgorffamhers gan_Arsym Undeb_Egu yr_YFB Annibynwyr_Egll Cymraeg_Ebu ac_Cyscyd argraffwyd_Bgorffamhers gan_Arsym Wasg_Ebu Morgannwg_Epb ,_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde Cofrestrwyd_Bgorffamhers yn_Arsym y_YFB Swyddfa_Ebu Bost_Egu ._Atdt 