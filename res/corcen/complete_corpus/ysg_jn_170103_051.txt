Cyfleu_Be Darllediadau_Egll Gwleidyddol_Anscadu ,_Atdcan Etholiadol_Anscadu ac_Cyscyd Ymgyrchoedd_Ebll Refferendwm_Egu i_Arsym S4C_Ebu (_Atdchw PPB_Gwacr ,_Atdcan PEB_Gwacr ,_Atdcan RCBs_Gwest )_Atdde 
Hydref_Egu 2015_Gwdig 
1_Gwdig ._Atdt 
Hyd_Arsym y_YFB Darllediad_Egu 
Ar_Arsym gyfer_Egu pob_Anscadu darllediad_Egu teledu_Be gallwch_Bgorch2ll ddewis_Egu un_Rhaamh o_Arsym dri_Rhifol hyd_Egu darlledu_Be gwahanol_Anscadu :_Atdcan naill_Anscadu ai_Cyscyd 2_Gwdig '_Atddyf 40_Gwdig "_Atddyf ,_Atdcan 3_Gwdig '_Atddyf 40_Gwdig "_Atddyf neu_Cyscyd 4_Gwdig '_Atddyf 40_Gwdig "_Atddyf ._Atdt 
Byddwn_Bdyf1ll angen_Egu hysbysiad_Egu o_Arsym 'r_YFB hyd_Egu a_Rhaperth ddewiswyd_Bgorffamhers ar_Arsym gyfer_Egu y_YFB gyfres_Ebu gyfan_Anscadu o_Arsym ddarllediadau_Egll o_Arsym leiaf_Anseith bythefnos_Egbu cyn_Arsym dyddiad_Egu y_YFB darllediad_Egu ._Atdt 
Unwaith_Adf y_Rhadib1u byddwch_Bdyf2ll wedi_Uberf dewis_Egu ,_Atdcan bydd_Bdyf3u disgwyl_Egu ichi_Ar2ll gyfleu_Be ar_Arsym yr_YFB hyd_Egu a_Rhaperth gytunwyd_Bgorffamhers ._Atdt 
2_Gwdig ._Atdt 
Cymhareb_Ebu Agwedd_Ebu 
Rhaid_Egu ichi_Ar2ll gyfleu_Be ar_Arsym Prif_Anseith Da_Anscadu ̂_Gwsym p_Gwllyth Digibeta_Gwest neu_Cyscyd HDcam_Gwest mewn_Arsym sgrin_Ebu lydan_Anscadu ar_Arsym fformat_Egu 16_Gwdig x_Gwllyth 9_Gwdig uchder_Egu llawn_Anscadu anamorffig_Anscadu (_Atdchw nid_Uneg 16_Gwdig x_Gwllyth 9_Gwdig deep_Gwest letterbox_unk )_Atdde ._Atdt 
Dylid_Egu cyfyngu_Be graffeg_Ebu o_Arsym fewn_Arsym maes_Egu diogel_Anscadu 16_Gwdig :_Atdcan 9_Gwdig ._Atdt 
Gweler_Bgorchamhers '_Atddyf Safonau_Ebll Technegol_Anscadu Cyfleu_Be Rhaglenni_Ebll Teledu_Be i_Arsym S4C_Ebu '_Atddyf ,_Atdcan sydd_Bpres3perth ar_Arsym gael_Be ar_Arsym wefan_Ebu gynhyrchu_Be S4C_Ebu ,_Atdcan am_Arsym ragor_Rhaamh o_Arsym wybodaeth_Ebu :_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Safonau_Ebll Technegol_Anscadu ar_Arsym gyfer_Egu PPB_Gwacr ,_Atdcan PEB_Gwacr a_Cyscyd RCBs_Gwest 
Wrth_Arsym gwrs_Egu ,_Atdcan materion_Egll ar_Arsym gyfer_Egu y_YFB blaid_Ebu wleidyddol_Anscadu neu_Cyscyd 'r_YFB sefydliad_Egu ymgyrchu_Be dynodedig_Anscadu perthnasol_Anscadu yn_Arsym unig_Anscadu yw_Bpres3u steil_Ebu a_Cyscyd chynnwys_Egu y_YFB PPB_Gwacr ,_Atdcan PEB_Gwacr a_Cyscyd 'r_YFB RCB_Gwacr ,_Atdcan ond_Arsym dylid_Egu cadw_Be at_Arsym rai_Banmeint canllawiau_Egbll a_Cyscyd chonfensiynau_Egll sylfaenol_Anscadu ,_Atdcan er_Arsym enghraifft_Ebu :_Atdcan 
a_Cyscyd )_Atdde dylid_Bamherffamhers lleoli_Be '_Atddyf Name_Egll Supers_Gwest '_Atddyf er_Arsym mwyn_Egu sicrhau_Be nad_Rhaperth yw_Bpres3u ceg_Ebu y_YFB person_Egu sy_Bpres3perth 'n_Uberf siarad_Be yn_Arsym cael_Be ei_Rhadib3gu guddio_Be ;_Atdcan 
b_Gwllyth )_Atdde dylai_Bamherff3u Graffeg_Ebu Gwybodaeth_Ebu fod_Be yn_Arsym ddealladwy_Anscadu ac_Cyscyd o_Arsym fewn_Arsym "_Atddyf Domestic_Gwest Cut_Egu Off_Adf "_Atddyf fel_Cyscyd bod_Be y_YFB gwyliwr_Egu adref_Adf yn_Arsym medru_Be darllen_Be y_YFB testun_Egu cyfan_Anscadu ;_Atdcan 
c_Gwllyth )_Atdde dylid_Bamherffamhers ystyried_Be defnydd_Egu gwyliwr_Egu o_Arsym isdeitlau_Egll wrth_Arsym fframio_Be golygfeydd_Ebll a_Cyscyd dylunio_Be graffeg_Ebu ;_Atdcan 
ch_unk )_Atdde dylid_Bamherffamhers cydbwyso_Be 'r_YFB defnydd_Egu o_Arsym gerddoriaeth_Ebu er_Arsym mwyn_Egu bod_Be pobl_Ebu sydd_Bpres3perth ag_Arsym anawsterau_Egll clywed_Be yn_Arsym medru_Be clywed_Be y_YFB gair_Egu llafar_Egu yn_Arsym glir_Anscadu ._Atdt 
Rhaid_Egu i_Arsym bob_Egll PPB_Gwacr ,_Atdcan PEB_Gwacr a_Cyscyd RCB_Gwacr gydymffurfio_Be a_Cyscyd ̂_Gwsym 'r_YFB un_Bancynnar safonau_Ebll technegol_Anscadu a_Arsym ̂_Gwsym phob_Banmeint un_Rhaamh o_Arsym raglenni_Ebll S4C_Ebu a_Cyscyd dylid_Bamherffamhers cadw_Be at_Arsym y_YFB manylebau_Ebll ar_Arsym bob_Egll amser_Egu ._Atdt 
Gellir_Bpresamhers dod_Be o_Rhapers3gu hyd_Egu i_Arsym wybodaeth_Ebu lawn_Anscadu am_Arsym fanylebau_Ebll technegol_Anscadu a_Arsym gwybodaeth_Ebu cyfleu_Be ar_Arsym ein_Banmedd1ll gwefan_Ebu ar_Arsym :_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
4_Gwdig ._Atdt 
Iaith_Ebu Gymraeg_Ebu 
Dylai_Bamherff3u 'r_YFB PPB_Gwacr /_Gwsym PEB_Gwacr /_Gwsym RCB_Gwacr fod_Be yn_Arsym yr_YFB iaith_Ebu Gymraeg_Ebu a_Cyscyd dylai_Bamherff3u gydymffurfio_Be a_Cyscyd ̂_Gwsym Chynllun_Egu Iaith_Ebu S4C_Ebu a_Arsym Chanllawiau_Egbll Iaith_Ebu S4C_Ebu y_Rhadib1u gellir_Bpresamhers dod_Be o_Rhapers3gu hyd_Arsym iddynt_Ar3ll ar_Arsym :_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde a_Cyscyd [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
Mae_Bpres3u darllediad_Egu yn_Arsym yr_YFB iaith_Ebu Gymraeg_Ebu yn_Arsym golygu_Be darllediad_Egu sydd_Bpres3perth wedi_Uberf ei_Rhadib3gu gynhyrchu_Be yn_Arsym y_YFB Gymraeg_Ebu yn_Arsym bennaf_Egu ac_Cyscyd sy_Bpres3perth 'n_Utra addas_Anscadu i'w_Arsym ddarlledu_Be yn_Arsym ystod_Ebu oriau_Ebll darlledu_Be brig_Egu S4C_Ebu ._Atdt 
Os_Cyscyd yw_Bpres3u 'r_YFB PPB_Gwacr /_Gwsym PEB_Gwacr /_Gwsym RCB_Gwacr yn_Arsym cynnwys_Egu defnydd_Egu helaeth_Anscadu o_Arsym drosleisio_Be ac_Cyscyd /_Gwsym neu_Cyscyd isdeitlo_Be ,_Atdcan neu_Cyscyd os_Cyscyd nad_Rhaperth yw_Bpres3u 'n_Uberf cydymffurfio_Be a_Cyscyd ̂_Gwsym Chanllawiau_Egbll Iaith_Ebu S4C_Ebu ,_Atdcan mae_Bpres3u S4C_Ebu yn_Arsym cadw_Be 'r_YFB hawl_Ebu i_Arsym beidio_Be a_Cyscyd ̂_Gwsym 'i_Rhadib3gu ddarlledu_Be ._Atdt 
5_Gwdig ._Atdt 
Cyfleu_Be 
Rhaid_Egu cyfleu_Be prif_Anseith da_Anscadu ̂_Gwsym p_Gwllyth y_YFB PPB_Gwacr ,_Atdcan PEB_Gwacr neu_Cyscyd 'r_YFB RCB_Gwacr wedi_Uberf 'i_Rhadib3gu farcio_Be 'n_Utra glir_Anscadu fel_Cyscyd '_Atddyf Deunydd_Egu Darlledu_Be '_Atddyf at_Arsym sylw_Egu 'r_YFB Uned_Ebu Rheoli_Be Cynnwys_Egu /_Gwsym Llyfrgell_Egu yn_Arsym S4C_Ebu erbyn_Arsym 10_Gwdig :_Atdcan 00_Gwdig TRI_Rhifol DIWRNOD_Egu GWAITH_Ebu CYN_Arsym y_YFB dyddiad_Egu darlledu_Be ._Atdt 
Bydd_Bdyf3u hyn_Rhadangd yn_Arsym galluogi_Be adolygiad_Egu technegol_Anscadu llawn_Anscadu ,_Atdcan adolygiad_Egu i_Arsym sicrhau_Be cydymffurfiaeth_Ebu a_Cyscyd bod_Be unrhyw_Bancynnar broblemau_Egbll a_Rhaperth gaiff_Bpres3u eu_Rhadib3ll canfod_Be yn_Arsym medru_Be cael_Be eu_Rhadib3ll datrys_Be cyn_Arsym darlledu_Be ._Atdt 
Os_Cyscyd nad_Rhaperth yw_Bpres3u hyn_Rhadangd yn_Arsym bosib_Anscadu ,_Atdcan yna_Adf cysylltwch_Bgorch2ll i_Arsym wneud_Be trefniadau_Egll amgen_Anscadu os_Cyscyd gwelwch_Bgorch2ll yn_Arsym dda_Anscadu ._Atdt 
Mae_Bpres3u S4C_Ebu yn_Arsym cadw_Be 'r_YFB hawl_Ebu i_Arsym ganslo_Be neu_Cyscyd aildrefnu_Be darllediad_Egu lle_Anscadu nad_Rhaperth yw_Bpres3u 'r_YFB deunydd_Egu wedi_Uberf ei_Rhadib3gu gyfleu_Be ar_Arsym amser_Egu neu_Cyscyd lle_Rhagof bo_Adf materion_Egll cydymffurfiaeth_Ebu yn_Arsym codi_Bpres2u ._Atdt 
Mae_Bpres3u S4C_Ebu hefyd_Adf yn_Arsym cadw_Be 'r_YFB hawl_Ebu i_Arsym godi_Bpres2u ta_Egu ̂_Gwsym l_Gwllyth am_Arsym unrhyw_Bancynnar gostau_Ebll ychwanegol_Anscadu a_Cyscyd achosir_Bpresamhers ._Atdt 
6_Gwdig ._Atdt 
Cysylltiadau_Egll a_Cyscyd Manylion_Egll Cyfleu_Be 
Y_YFB prif_Anseith gyswllt_Egu ar_Arsym gyfer_Egu materion_Egll technegol_Anscadu a_Cyscyd chyfleu_Be yw_Bpres3u Jen_Ep Pappas_Ep ,_Atdcan Pennaeth_Egu Rheoli_Be Cynnwys_Egu S4C_Ebu (_Atdchw e-bost_Egu :_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde ,_Atdcan rhif_Egu ffo_Egu ̂_Gwsym n_Gwllyth :_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde )_Atdde ._Atdt 
Tapiau_Egll i'w_Arsym hanfon_Be at_Arsym :_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde 
7_Gwdig ._Atdt 
Gwybodaeth_Ebu Hanfodol_Anscadu 
Bydd_Bdyf3u angen_Egu manylion_Egll am_Arsym unrhyw_Bancynnar gerddoriaeth_Ebu a_Rhaperth gaiff_Bpres3u ei_Rhadib3gu ddefnyddio_Be yn_Arsym y_YFB PPB_Gwacr ,_Atdcan PEB_Gwacr neu_Cyscyd 'r_YFB RCB_Gwacr ar_Arsym bob_Egll darlledwr_Egu ,_Atdcan boed_Bgorch3u y_YFB gerddoriaeth_Ebu yn_Arsym fasnachol_Anscadu ,_Atdcan llyfrgell_Egu neu_Cyscyd wedi_Adf ei_Rhadib3gu gyfansoddi_Be 'n_Utra arbennig_Anscadu ._Atdt 
Yn_Arsym ogystal_Anscyf ,_Atdcan bydd_Bdyf3u angen_Egu enwau_Egll unrhyw_Anscadu actorion_Egll a_Rhaperth ddefnyddir_Bpresamhers a_Cyscyd 'u_Banmedd3ll hasiantau_Egll ._Atdt 
Mae_Bpres3u angen_Egu y_YFB manylion_Egll hyn_Rhadangd er_Arsym mwyn_Egu i_Arsym ddarlledwyr_Egll allu_Egu cyflawni_Be eu_Banmedd3ll dyletswyddau_Ebll cyfreithiol_Anscadu ac_Cyscyd ni_Uneg ellir_Bpresamhers eu_Banmedd3ll hepgor_Egu ._Atdt 
Dylai_Bamherff3u enw_Egu a_Cyscyd rhifau_Egll cyswllt_Egu y_YFB person_Egu sy_Bpres3perth 'n_Utra gyfrifol_Anscadu am_Arsym y_YFB PPB_Gwacr /_Gwsym PEB_Gwacr /_Gwsym RCB_Gwacr fod_Be yn_Arsym amlwg_Anscadu ar_Arsym y_YFB danfoniad_Egu ,_Atdcan yn_Arsym ogystal_Anscyf ag_Arsym enw_Egu a_Cyscyd rhif_Egu ffo_Egu ̂_Gwsym n_Gwllyth cyswllt_Egu unrhyw_Anscadu gwmni_Egu cynhyrchu_Be a_Rhaperth ddefnyddiwyd_Bgorffamhers ._Atdt 
8_Gwdig ._Atdt 
Deunydd_Egu Cyfleu_Be 
Am_Arsym bob_Egll fersiwn_Egu o_Arsym 'r_YFB PPB_Gwacr ,_Atdcan PEB_Gwacr a_Cyscyd RCB_Gwacr mae_Bpres3u arnom_Ar1ll angen_Egu :_Atdcan 
•_Gwsym 1_Gwdig x_Gwllyth Prif_Anseith Da_Anscadu ̂_Gwsym p_Gwllyth Digibeta_Gwest /_Gwsym HDcam_Gwest 
•_Gwsym 1_Gwdig x_Gwllyth DVD_Gwacr gyda_Arsym Chod_Bpres3u Amser_Egu wedi_Uberf ei_Rhadib3gu losgi_Be i_Arsym 'r_YFB llun_Egu (_Atdchw yn_Arsym y_YFB golwg_Ebu )_Atdde 
•_Gwsym Sgript_Ebu wedi_Uberf 'i_Rhadib3bu argraffu_Be ac_Cyscyd Adroddiad_Egu Cerddoriaeth_Ebu 
•_Gwsym Rhestr_Ebu actorion_Egll 
•_Gwsym Datganiad_Egu PPB_Gwacr /_Gwsym PEB_Gwacr /_Gwsym RCB_Gwacr Safonol_Anscadu S4C_Ebu (_Atdchw y_Rhadib1u mae_Bpres3u copi_Egu ohono_Ar3gu ar_Arsym gael_Be yma_Adf :_Atdcan [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde )_Atdde 
9_Gwdig ._Atdt 
Ymholiadau_Egll 
Os_Cyscyd oes_Bpres3amhen unrhyw_Anscadu gwestiynau_Egll cyffredinol_Anscadu am_Arsym gyfleu_Be ,_Atdcan cynhyrchu_Be neu_Cyscyd faterion_Egll technegol_Anscadu ,_Atdcan gellir_Bpresamhers eu_Rhadib3ll cyfeirio_Be at_Arsym Jen_Ep Pappas_Ep ,_Atdcan Pennaeth_Egu Rheoli_Be Cynnwys_Egu (_Atdchw [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde /_Gwsym [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde /_Gwsym [_Atdchw ._Atdt ._Atdt ._Atdt ]_Atdde )_Atdde ._Atdt 