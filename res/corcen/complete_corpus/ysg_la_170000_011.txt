Pennod_Anscadu Dau_Rhifol 
Ychydig_Rhaamh iawn_Ebych o_Arsym dylwyth_Egu teg_Anscadu a_Rhaperth welodd_Bgorff3u Mwyaren_Bgorch3ll wrth_Arsym gerdded_Be drwy_Arsym 'r_YFB goedwig_Ebu ._Atdt 
Mae_Bpres3u 'n_Utra rhaid_Egu bod_Be pawb_Rhaamh yn_Arsym swatio_Be 'n_Utra ddiogel_Anscadu yn_Arsym eu_Banmedd3ll tai_Egll nes_Anscym bod_Be y_YFB gwynt_Egu yn_Arsym gostegu_Be ._Atdt 
Daliai_Bamherff3u 'r_YFB dail_Ebll i_Arsym gwympo_Be ,_Atdcan a_Cyscyd gwaith_Ebu anodd_Anscadu oedd_Bamherff3u ceisio_Be osgoi_Be 'r_YFB dail_Ebll a_Rhaperth oedd_Bamherff3u yn_Arsym chwythu_Be i'w_Arsym hwyneb_Egu ._Atdt 
Ofnai_Bamherff3u Mwyaren_Bgorch3ll y_Rhadib1u byddai_Bamod3u deilen_Ebu fawr_Anscadu yn_Arsym lapio_Be 'i_Rhadib3bu hun_Rhaatb dros_Arsym ei_Rhadib3bu llygaid_Egll a_Cyscyd byddai_Bamod3u 'n_Uberf methu_Be 'n_Utra lân_Anscadu â_Arsym gweld_Be i_Arsym ble_Egbu roedd_Bamherff3u hi_Rhapers3bu 'n_Uberf mynd_Be !_Atdt 
Yn_Arsym sydyn_Anscadu ,_Atdcan wrth_Arsym droi_Be 'r_YFB gornel_Ebu a_Cyscyd arweiniai_Bamherff3u at_Arsym afon_Ebu Grisial_Egu ,_Atdcan clywodd_Bgorff3u Mwyaren_Bgorch3ll sŵn_Egu parablu_Be uchel_Anscadu ,_Atdcan cynhyrfus_Anscadu ._Atdt 
Gwelodd_Bgorff3u griw_Egu bychan_Anscadu yn_Arsym sefyll_Be y_YFB tu_Egu allan_Adf i_Arsym Siop_Ebu Tan_Arsym yr_YFB Onnen_Ep ,_Atdcan 
â_Arsym golwg_Ebu bryderus_Anscadu iawn_Adf ar_Arsym eu_Banmedd3ll hwynebau_Egll ,_Atdcan a_Cyscyd 'i_Rhadib3bu ffrind_Egu Rhoswen_Gwest yn_Arsym eu_Banmedd3ll canol_Egu ._Atdt 
"_Atddyf O_Rhapers3gu ,_Atdcan Mwyaren_Bgorch3ll !_Atdt "_Atddyf meddai_Bamherff3u Rhoswen_Ep ,_Atdcan gan_Arsym ruthro_Be ati_Ar3bu ._Atdt 
"_Atddyf Mae_Bpres3u rhywbeth_Egu ofnadwy_Anscadu wedi_Uberf digwydd_Be !_Atdt "_Atddyf 
"_Atddyf Be_Rhagof sy_Bpres3perth 'n_Uberf bod_Be ?_Atdt "_Atddyf holodd_Bgorff3u Mwyaren_Bgorch3ll mewn_Arsym braw_Egu ._Atdt 
"_Atddyf Mae_Bpres3u Swnyn_Egu ar_Arsym goll_Egu !_Atdt "_Atddyf 
Dychrynodd_Bgorff3u Mwyaren_Bgorch3ll wrth_Arsym glywed_Be y_YFB newydd_Anscadu ._Atdt 
Swnyn_Egu oedd_Bamherff3u cefnder_Egu bach_Egu Rhoswen_Gwest ,_Atdcan a_Cyscyd doedd_Bamherff3u neb_Rhaamh wedi_Uberf ei_Rhadib3gu weld_Be ers_Arsym ben_Ebu bore_Egu ._Atdt 
Un_Rhaamh bychan_Anscadu oedd_Bamherff3u o_Rhapers3gu ,_Atdcan â_Arsym phen_Egu llawn_Anscadu o_Arsym wallt_Egu coch_Anscadu ,_Atdcan fel_Cyscyd gweddill_Egu y_YFB teulu_Egu ._Atdt 
Roedd_Bamherff3u o_Arsym wedi_Adf sleifio_Be allan_Adf i_Rhapers1u chwarae_Egu tra_Ebu oedd_Bamherff3u ei_Rhadib3gu fam_Ebu yn_Arsym golchi_Be dillad_Egll ,_Atdcan ac_Cyscyd roedd_Bamherff3u pawb_Rhaamh wedi_Uberf bod_Be wrthi_Ar3bu 'n_Uberf chwilio_Be amdano_Ar3gu ,_Atdcan heb_Arsym unrhyw_Bancynnar lwc_Ebu ._Atdt 
"_Atddyf Paid_Egbu â_Arsym phoeni_Be ,_Atdcan Rhoswen_Gwest ,_Atdcan "_Atddyf meddai_Bamherff3u Mwyaren_Bgorch3ll yn_Arsym garedig_Egu ._Atdt 
"_Atddyf Dwi_Bpres1u ar_Arsym fy_Banmedd1u ffordd_Ebu i_Rhapers1u 'r_YFB llwyni_unk mwyar_Egll ._Atdt 
Mi_Uberf helpa_Bgorch2u i_Rhapers1u i_Rhapers1u chwilio_Be amdano_Ar3gu ._Atdt "_Atddyf 
"_Atddyf Diolch_Egu yn_Arsym fawr_Anscadu iawn_Adf ,_Atdcan Mwyaren_Bgorch3ll ,_Atdcan "_Atddyf atebodd_Bgorff3u Rhoswen_Ep ._Atdt 
"_Atddyf Awn_Bgorch1ll ni_Rhapers1ll i_Rhapers1u chwilio_Be 'r_YFB llannerch_unk ac_Cyscyd mae_Bpres3u criw_Egu arall_Anscadu am_Arsym fynd_Be i_Arsym Siop_Ebu Sidan_Egu ac_Cyscyd Ogof_Ebu Di-Ben-Draw_Anscadu yng_Arsym nghornel_Ebu bellaf_Anscadu y_YFB goedwig_Ebu ._Atdt 
Gobeithio_Be daw_Bpres3u Swnyn_Egu adre_Adf 'n_Utra fuan_Anscadu ._Atdt 
Dwi_Bpres1u 'n_Uberf gweld_Be ei_Rhadib3gu golli_Be ,_Atdcan y_YFB gwalch_Egbu bach_Anscadu drwg_Anscadu !_Atdt "_Atddyf 
Aeth_Egu Mwyaren_Bgorch3ll yn_Arsym ei_Rhadib3gu blaen_Egu ,_Atdcan gan_Arsym addo_Be rhoi_Be gwybod_Egu pe_Cyscyd bai_Egu 'n_Arsym ei_Rhadib3gu weld_Be ._Atdt 
Doedd_Bamherff3u hi_Rhapers3bu ddim_Adf yn_Arsym hoffi_Be meddwl_Egu am_Arsym Swnyn_Egu bach_Anscadu ar_Arsym goll_Egu yn_Arsym y_YFB coed_Ebll ._Atdt 
"_Atddyf Bydd_Bdyf3u rhaid_Egu i_Arsym 'r_YFB darten_Ebu afal_Egu a_Cyscyd mwyar_Egll aros_Be wedi_Arsym 'r_YFB cwbl_Egu ,_Atdcan "_Atddyf meddyliodd_Bgorff3u ._Atdt 
Roedd_Bamherff3u hi_Rhapers3bu 'n_Utra bwysicach_Anscym o_Arsym lawer_Banmeint dod_Be o_Rhapers3gu hyd_Egu i_Arsym Swnyn_Egu cyn_Arsym iddi_Ar3bu ddechrau_Egu nosi_Be ._Atdt 
Lle_Rhagof tywyll_Anscadu ac_Cyscyd oer_Anscadu iawn_Adf oedd_Bamherff3u y_YFB goedwig_Ebu yn_Arsym y_YFB nos_Ebu ._Atdt 
Pennod_Anscadu Tri_Rhifol 
Wrth_Arsym ddringo_Be allt_Ebu Clychau_Ebll 'r_YFB 
Gog_Egu ,_Atdcan clywodd_Bgorff3u Mwyaren_Bgorch3ll sŵn_Egu gweiddi_Be mawr_Anscadu yn_Arsym dod_Be o_Arsym dŷ_Egu Nain_Ebu Derwen_Ebu a_Arsym oedd_Bamherff3u yn_Arsym byw_Be gerllaw_Arsym ._Atdt 
Wrth_Arsym nesáu_Be at_Arsym ei_Rhadib3bu thŷ_Egu ym_Arsym moncyff_Egu y_YFB goeden_Ebu hynaf_Anscadu yn_Arsym y_YFB goedwig_Ebu ,_Atdcan mentrodd_Bgorff3u 
Mwyaren_Bgorch3ll alw_Egu ,_Atdcan 
"_Atddyf Helô_Ebych ,_Atdcan oes_Bpres3amhen rhywun_Egu gartre_Egu ?_Atdt "_Atddyf 
Ar_Arsym unwaith_Adf ,_Atdcan gwelodd_Bgorff3u Mwyaren_Bgorch3ll fop_Gwest o_Arsym wallt_Egu arian_Egu cyrliog_Anscadu yn_Arsym dod_Be i_Arsym 'r_YFB golwg_Ebu ,_Atdcan a_Cyscyd hwnnw_Rhadangg wedi_Uberf ei_Rhadib3gu glymu_Be gyda_Arsym rhuban_Egu oren_Egbu ._Atdt 
Nain_Ebu Derwen_Ebu oedd_Bamherff3u y_YFB dylwythen_unk deg_Rhifol hynaf_Anscadu ym_Arsym Maes_Egu y_YFB Mes_Egu ._Atdt 
Yn_Arsym wir_Ebych ,_Atdcan tylwythen_unk ifanc_Anscadu oedd_Bamherff3u hi_Rhapers3bu pan_Egu oedd_Bamherff3u llawer_Rhaamh o_Arsym 'r_YFB coed_Ebll yno_Adf newydd_Anscadu gael_Be eu_Rhadib3ll plannu_Be ,_Atdcan a_Cyscyd hynny_Bandangd pan_Egu oedd_Bamherff3u y_YFB tylwyth_Egu yn_Arsym byw_Be ym_Arsym mhen_Egu draw_Adf gwlad_Ebu Bythwyrdd_Anscadu ._Atdt 
Pan_Egu oedd_Bamherff3u hi_Rhapers3bu 'n_Utra fabi_Egbu bach_Anscadu ,_Atdcan dim_Egu ond_Arsym mesen_unk oedd_Bamherff3u y_YFB goeden_Ebu roedd_Bamherff3u hi_Rhapers3bu 'n_Utra byw_Be ynddi_Ar3bu heddiw_Adf ._Atdt 
"_Atddyf O_Rhapers3gu !_Atdt 
Ti_Rhapers2u sydd_Bpres3perth yna_Adf ,_Atdcan Mwyaren_Bgorch3ll fach_Egu ,_Atdcan "_Atddyf meddai_Bamherff3u Nain_Ebu Derwen_Ebu ,_Atdcan â_Arsym golwg_Ebu boenus_Anscadu ar_Arsym ei_Rhadib3bu hwyneb_Egu ._Atdt 
"_Atddyf Dwi_Bpres1u ddim_Egu yn_Arsym gwybod_Egu be_Rhagof sy_Bpres3perth 'n_Uberf digwydd_Be ,_Atdcan nac_Cyscyd ydw_Bpres1u i_Rhapers1u wir_Anscadu !_Atdt "_Atddyf 
"_Atddyf Pam_Adf ?_Atdt 
Be_Rhagof yn_Arsym y_YFB byd_Egu mawr_Anscadu sy_Bpres3perth 'n_Uberf bod_Be ,_Atdcan Nain_Ebu Derwen_Ebu ?_Atdt "_Atddyf gofynnodd_Bgorff3u Mwyaren_Bgorch3ll mewn_Arsym braw_Egu ._Atdt 
"_Atddyf Dyna_Adf 'r_YFB drydedd_Rhitrefd darten_Ebu fwyar_Egll dwi_Bpres1u wedi_Uberf 'i_Rhadib3bu cholli_Be yr_YFB wythnos_Ebu hon_Rhadangb !_Atdt 
Mi_Uberf roddais_Bgorff1u hi_Rhapers3bu ar_Arsym y_YFB sil_Egbu ffenest_Ebu yma_Adf i_Arsym oeri_Be ddeg_Rhifol munud_Egu yn_Arsym ôl_Egu ,_Atdcan ond_Arsym mae_Bpres3u 'r_YFB darten_Ebu gyfan_Anscadu ,_Atdcan a_Cyscyd 'r_YFB plât_Egu ,_Atdcan wedi_Uberf diflannu_Be !_Atdt "_Atddyf 
Dychrynodd_Bgorff3u Mwyaren_Bgorch3ll o_Arsym glywed_Be bod_Be rhywun_Egu wedi_Uberf dwyn_Be tarten_Ebu Nain_Ebu Derwen_Ebu ._Atdt 
Roedd_Bamherff3u yn_Arsym anodd_Anscadu dychmygu_Be y_Rhadib1u byddai_Bamod3u unrhyw_Anscadu un_Rhaamh yn_Arsym lladrata_Be ym_Arsym Maes_Egu y_YFB Mes_Egu ._Atdt 
Roedd_Bamherff3u heddiw_Adf 'n_Utra ddiwrnod_Egu rhyfedd_Anscadu ar_Arsym y_YFB naw_Rhifol ,_Atdcan gyda_Arsym Swnyn_Egu ar_Arsym goll_Egu ,_Atdcan ac_Cyscyd yn_Arsym awr_Ebu ,_Atdcan tarten_Ebu gyfan_Anscadu wedi_Uberf diflannu_Be ._Atdt 
"_Atddyf Digwyddodd_Bgorff3u yr_YFB un_Bancynnar peth_Egu ddoe_Adf a_Cyscyd 'r_YFB diwrnod_Egu o_Arsym 'r_YFB blaen_Egu ._Atdt 
Cafodd_Bgorff3u y_YFB tair_Rhifol eu_Rhadib3ll cipio_Be !_Atdt 
Mwyaren_Bgorch3ll fach_Egu ,_Atdcan mae_Bpres3u lleidr_Egu yn_Arsym y_YFB goedwig_Ebu !_Atdt 
O_Rhapers3gu diar_Ebych mi_Rhapers1u ._Atdt 
Diar_Ebych ,_Atdcan diar_Ebych mi_Rhapers1u ._Atdt "_Atddyf 
Roedd_Bamherff3u Nain_Ebu Derwen_Ebu yn_Arsym agos_Anscadu iawn_Adf at_Arsym grio_Be ._Atdt 
Addawodd_Bgorff3u Mwyaren_Bgorch3ll y_Rhadib1u byddai_Bamod3u 'n_Uberf ceisio_Be dod_Be at_Arsym wraidd_Egll y_YFB dirgelwch_Egu ,_Atdcan ac_Cyscyd aeth_Bgorff3u ar_Arsym ei_Rhadib3gu hunion_Anscadu i_Arsym gnocio_Be ar_Arsym ddrws_Egu tŷ_Egu Briallen_Epb ,_Atdcan ei_Rhadib3bu ffrind_Egu ._Atdt 
Roedd_Bamherff3u yn_Arsym rhaid_Egu cael_Be help_Egbu ,_Atdcan a_Cyscyd hynny_Rhadangd ar_Arsym frys_Egu !_Atdt 
Pennod_Anscadu Pedwar_Rhifol 
Ar_Arsym ôl_Anscadu gwrando_Be ar_Arsym Mwyaren_Bgorch3ll yn_Arsym adrodd_Be hanes_Egu Swnyn_Egu a_Cyscyd helynt_Ebu y_YFB lladrad_Egu ,_Atdcan roedd_Bamherff3u Briallen_Ep wedi_Uberf dychryn_Egu yn_Arsym ofnadwy_Anscadu ,_Atdcan ac_Cyscyd am_Arsym wneud_Be ei_Rhadib3gu gorau_Egll glas_Anscadu i_Arsym helpu_Be Rhoswen_Ep a_Cyscyd Nain_Ebu Derwen_Ebu ._Atdt 
Erbyn_Arsym hyn_Rhadangd ,_Atdcan roedd_Bamherff3u y_YFB gwynt_Egu yn_Arsym dechrau_Egu tawelu_Be ,_Atdcan a_Cyscyd gallai_Bamherff3u Mwyaren_Bgorch3ll a_Cyscyd Briallen_Epb gerdded_Be yn_Arsym hawdd_Anscadu drwy_Arsym 'r_YFB goedwig_Ebu ._Atdt 
Penderfynodd_Bgorff3u y_YFB ddwy_Rhifol ddilyn_Be Llwybr_Egu Igam-Ogam_Anscadu at_Arsym y_YFB llwyni_unk mwyar_Egll ,_Atdcan gan_Arsym weiddi_Be enw_Egu Swnyn_Egu bob_Anscadu hyn_Rhadangd a_Cyscyd hyn_Rhadangd ._Atdt 
Roedd_Bamherff3u yn_Arsym daith_Ebu hir_Anscadu ._Atdt 
"_Atddyf Swnyn_Egu !_Atdt 
Ble_Rhagof wyt_Bpres2u ti_Rhapers2u ?_Atdt "_Atddyf gwaeddodd_Bgorff3u Mwyaren_Bgorch3ll ._Atdt 
"_Atddyf Ew_Ebych ,_Atdcan does_Bpres3amhen dim_Banmeint golwg_Ebu ohono_Ar3gu ,_Atdcan "_Atddyf meddai_Bamherff3u Briallen_Ep ,_Atdcan gan_Arsym aros_Be i_Arsym gymryd_Be ei_Rhadib3bu gwynt_Egu ._Atdt 
"_Atddyf Ac_Cyscyd mae_Bpres3u Maes_Egu y_YFB Mes_Egu yn_Arsym lle_Egu mawr_Anscadu ,_Atdcan "_Atddyf atebodd_Bgorff3u Mwyaren_Bgorch3ll ._Atdt 
"_Atddyf Gallai_Bamherff3u fod_Be yn_Arsym unrhyw_Bancynnar ran_Ebu o_Arsym 'r_YFB goedwig_Ebu ._Atdt "_Atddyf 
"_Atddyf Aw_Egu !_Atdt "_Atddyf gwaeddodd_Bgorff3u Mwyaren_Bgorch3ll yn_Arsym sydyn_Anscadu ,_Atdcan gan_Arsym rwbio_Be 'i_Rhadib3bu phen_Egu ._Atdt 
"_Atddyf Ooo_Gwest !_Atdt "_Atddyf meddai_Bamherff3u Briallen_Ep ,_Atdcan gan_Arsym afael_Ebu yn_Arsym ei_Rhadib3bu phen_Egu hithau_Rhacys3bu ._Atdt 
Edrychodd_Bgorff3u y_YFB ddwy_Rhifol ar_Arsym y_YFB llawr_Egu a_Cyscyd gweld_Be dwy_Rhifol fesen_unk yn_Arsym rowlio_Be o_Arsym 'u_Banmedd3ll blaenau_Egll ._Atdt 
Yn_Arsym sydyn_Anscadu ,_Atdcan disgynnodd_Bgorff3u mesen_unk arall_Anscadu ,_Atdcan gan_Arsym lanio_Be wrth_Arsym draed_Egbu Briallen_Epb y_YFB tro_Egu hwn_Rhadangg ._Atdt 
"_Atddyf Ha_Egu ,_Atdcan ha_Egu !_Atdt "_Atddyf meddai_Bamherff3u Mwyaren_Bgorch3ll ._Atdt 
"_Atddyf Mae_Bpres3u 'r_YFB goeden_Ebu dderwen_Ebu yn_Arsym bwrw_Be mes_Egu ,_Atdcan ond_Arsym dyna_Adf anlwcus_Anscadu oedden_Bamherff1ll ni_Rhapers1ll i_Arsym gael_Be ein_Banmedd1ll taro_Be ar_Arsym ein_Banmedd1ll pennau_Egll !_Atdt "_Atddyf 
Cododd_Bgorff3u Briallen_Ep y_YFB mes_Egu ,_Atdcan a_Cyscyd 'u_Rhadib3ll rhoi_Be yn_Arsym ofalus_Anscadu ym_Arsym masged_Ebu Mwyaren_Bgorch3ll ._Atdt 
"_Atddyf Mi_Uberf gaiff_Bpres3u Cochyn_Ep y_YFB wiwer_Ebu y_YFB mes_Egu yma_Adf ,_Atdcan "_Atddyf chwarddodd_Bgorff3u Briallen_Ep ._Atdt 
"_Atddyf O_Arsym leia_Anscadu bydd_Bdyf3u Cochyn_Ep yn_Arsym falch_Ebu ohonyn_Ar3ll nhw_Rhapers3ll !_Atdt "_Atddyf 
Byddai_Bamod3u 'r_YFB wiwer_Ebu gyfeillgar_Anscadu a_Cyscyd chlên_Anscadu yn_Arsym casglu_Be mes_Egu yr_YFB adeg_Ebu yma_Adf o_Arsym 'r_YFB flwyddyn_Ebu er_Arsym mwyn_Egu cael_Be stôr_Egu o_Arsym fwyd_Egu ar_Arsym gyfer_Egu y_YFB gaeaf_Egu ._Atdt 
Gyda_Arsym basged_Ebu Mwyaren_Bgorch3ll yn_Arsym drwm_Anscadu gan_Arsym fes_Egu ,_Atdcan nid_Uneg mwyar_Egll duon_Egll ,_Atdcan cerddodd_Bgorff3u y_YFB ddwy_Rhifol yn_Arsym eu_Banmedd3ll blaenau_Egll at_Arsym y_YFB llwyni_unk drain_Egbll ,_Atdcan gan_Arsym ddal_Be i_Arsym weiddi_Be enw_Egu Swnyn_Egu yn_Arsym uchel_Anscadu ._Atdt 
Ar_Arsym ôl_Anscadu cyrraedd_Be ,_Atdcan doedd_Bamherff3u dim_Banmeint golwg_Ebu ohono_Ar3gu yn_Arsym unman_Egu ._Atdt 
Ond_Arsym siawns_Ebu na_Uneg fyddai_Bamod3u Swnyn_Egu hyd_Arsym yn_Arsym oed_Egu yn_Arsym ddigon_Egu gwirion_Anscadu i_Arsym ddewis_Egu cuddio_Be yng_Arsym nghanol_Egu mieri_Ebll pigog_Anscadu y_YFB mwyar_Egll duon_Egll ._Atdt 
Roedd_Bamherff3u y_YFB ddwy_Rhifol ar_Arsym fin_Egu troi_Be am_Arsym adref_Adf pan_Egu sylwodd_Bgorff3u Mwyaren_Bgorch3ll ar_Arsym rywbeth_Egu glas_Anscadu a_Cyscyd gwyn_Anscadu yn_Arsym disgleirio_Be ar_Arsym y_YFB llawr_Egu ,_Atdcan wedi_Arsym 'i_Rhadib3bu hanner_Egu cuddio_Be o_Arsym dan_Egu y_YFB brigau_Egll trwchus_Anscadu ._Atdt 
Aeth_Egu yn_Arsym nes_Anscym ato_Arsym a_Cyscyd 'i_Rhadib3gu godi_Bpres2u ._Atdt 
Plât_Egu gwyn_Anscadu oedd_Bamherff3u yno_Adf ,_Atdcan gyda_Arsym phatrwm_Egu o_Arsym flodau_Egll glas_Anscadu o_Arsym gwmpas_Egu yr_YFB ymyl_Ebu ._Atdt 
"_Atddyf Hmm_Ebych ,_Atdcan "_Atddyf meddai_Bamherff3u Briallen_Ep yn_Arsym ddifrifol_Anscadu ._Atdt 
"_Atddyf Dwi_Bpres1u 'n_Uberf credu_Be ein_Rhadib1ll bod_Be ni_Rhapers1ll wedi_Uberf dod_Be o_Rhapers3gu hyd_Egu i_Arsym blât_Egu tarten_Ebu Nain_Ebu Derwen_Ebu ,_Atdcan ond_Arsym ble_Rhagof mae_Bpres3u 'r_YFB lleidr_Egu ?_Atdt "_Atddyf 
"_Atddyf Reit_Ebych ,_Atdcan "_Atddyf atebodd_Bgorff3u Mwyaren_Bgorch3ll ,_Atdcan "_Atddyf yn_Arsym ôl_Egu â_Arsym ni_Rhapers1ll i_Arsym Siop_Ebu Tan_Arsym yr_YFB Onnen_Ep ._Atdt 
Mi_Uberf allwn_Bamherff1u adael_Be y_YFB mes_Egu yno_Adf i_Rhapers1u Cochyn_Gwest ._Atdt 
Gobeithio_Be bod_Be rhywun_Egu wedi_Uberf dod_Be o_Rhapers3gu hyd_Egu i_Arsym Swnyn_Egu bach_Anscadu yn_Arsym y_YFB cyfamser_Egu ._Atdt "_Atddyf 
Pennod_Anscadu Pump_Rhifol 
Taith_Ebu hir_Anscadu oedd_Bamherff3u y_YFB siwrne_Ebu yn_Arsym ôl_Egu i_Arsym 
Siop_Ebu Tan_Arsym yr_YFB Onnen_Ep ._Atdt 
Ar_Arsym y_YFB ffordd_Ebu ,_Atdcan galwodd_Bgorff3u y_YFB ddwy_Rhifol heibio_Arsym tŷ_Egu Nain_Ebu Derwen_Ebu gyda_Arsym 'r_YFB plât_Egu ,_Atdcan ac_Cyscyd i_Arsym adrodd_Be eu_Banmedd3ll hanes_Egu o_Arsym ddod_Be o_Rhapers3gu hyd_Arsym iddo_Ar3gu wedi_Adf ei_Rhadib3gu guddio_Be yn_Arsym y_YFB llwyni_unk mwyar_Egll ._Atdt 