[_Atdchw Swn_Egu cefndirol_Anscadu sawl_Banmeint person_Egu yn_Arsym sgwrsio_Be a_Cyscyd chwerthin_Be yr_YFB un_Bancynnar pryd_Rhagof ]_Atdde ._Atdt 
S1_Anon O_Rhapers3gu 's_Gwllyth copi_Egu sbar_Anscadu i_Arsym rannu_Be [aneglur?]_Gwann ._Atdt 
S2_Anon [chwerthin]_Gwann ._Atdt 
Na_Rhaperth mewn_Arsym gair_Egu ._Atdt 
Reit_Ebych te_Egu ._Atdt 
Yn_Arsym gynta_Rhitrefd '_Atddyf ga_Bpres1u '_Atddyf i_Arsym ddiolch_Egu i_Rhapers1u chi_Rhapers2ll am_Arsym nos_Ebu Wener_Ebu a_Cyscyd o'n_Bamherff1u i_Rhapers1u 'n_Uberf meddwl_Be o'dd_Bamherff3u e_Rhapers3gu 'di_Uberf mynd_Be yn_Arsym hynod_Anscadu o_Arsym dda_Anscadu ._Atdt 
ymm_Ebych â_Arsym dweud_Be y_YFB gwir_Egu o'dd_Bamherff3u Dyffryn_Egu Aman_Ebu yn_Arsym impressed_Gwest ofnadw_Anscadu '_Atddyf ag_Arsym yn_Arsym meddwl_Egu bo_Adf '_Atddyf chi_Rhapers2ll 'n_Arsym [aneglur]_Gwann a_Rhaperth wnaeth_Bgorff3u un_Rhifold dweud_Be pob_Egll tro_Egu oeddech_Bamherff2ll chi_Rhapers2ll 'n_Uberf cerdded_Be mewn_Arsym i_Arsym dafarn_Egbu [aneglur?]_Gwann ._Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S2_Anon A_Cyscyd [-]_Gwann ._Atdt 
S1_Anon '_Atddyf Nath_Bgorff3u un_Rhifold stopio_Be hi_Rhapers3bu ar_Arsym y_YFB stryd_Ebu a_Cyscyd diolch_Egu iddi_Ar3bu hi_Rhapers3bu am_Arsym ganu_Egu '_Atddyf chos_unk bo_Adf '_Atddyf hi_Rhapers3bu 'n_Uberf gwisgo_Be du_Anscadu ._Atdt 
S2_Anon [aneglur?]_Gwann 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S2_Anon Y_YFB a_Cyscyd fel_Cyscyd '_Atddyf wedes_Bgorff1u i_Rhapers1u [=]_Gwann '_Atddyf wedodd_Bgorff3u [/=]_Gwann '_Atddyf wedodd_Bgorff3u enwg_Anon [aneglur]_Gwann [=]_Gwann bod_Be [/=]_Gwann bod_Be [aneglur]_Gwann yn_Arsym mynd_Be lan_Adf 'i_Rhadib3bu gefen_unk e_Rhapers3gu gyda_Arsym 'r_YFB amens_Gwest a_Arsym o_Arsym Dduw_Egu ac_Cyscyd amens_Gwest Okay_Gwest fine_Gwest so_Adf wy_Egu '_Atddyf i_Rhapers1u 'n_Uberf credu_Be [aneglur?]_Gwann ._Atdt 
Reit_Ebych te_Egu odi_Bpres3u ni_Rhapers1ll mynd_Be [aneglur?]_Gwann yn_Arsym gynta_Rhitrefd '_Atddyf neu_Cyscyd ni_Rhapers1ll 'n_Utra myn_Egu '_Atddyf i_Rhapers1u '_Atddyf neud_Be rh'wbeth_unk gwahanol_Anscadu ?_Atdt 
[_Atdchw Swn_Egu symud_Be dodrefn_Egll a_Cyscyd siarad_Be cefndirol_Anscadu ]_Atdde ._Atdt 
S3_Anon Gwahanol_Anscadu ._Atdt 
S2_Anon Gwahanol_Anscadu ._Atdt 
S3_Anon Achos_Cyscyd [aneglur?]_Gwann ._Atdt 
S2_Anon W_Ebych ._Atdt Rhywbeth_Rhaamh i_Arsym dwymo_Be lan_Adf ._Atdt 
Beth_Rhagof ni_Rhapers1ll 'n_Utra myn_Egu '_Atddyf i_Arsym wneud_Be i_Arsym dwymo_Be lan_Adf ?_Atdt 
[_Atdchw Sgwrsio_Be cefndirol_Anscadu ]_Atdde ._Atdt 
S1_Anon Requests_Gwest beth_Rhagof chi_Rhapers2ll mo'yn_Egu n'neud_unk fel_Cyscyd [-]_Gwann ._Atdt 
S2_Anon O_Rhapers3gu hang_Gwest on_Gwest [aneglur?]_Gwann ma_Bpres3u '_Atddyf raid_Egu ni_Rhapers1ll ga'l_Ebu rh'wbeth_unk teidi_Anscadu nawr_Adf i_Rhapers1u ddechre_Be [chwerthin]_Gwann Default_Gwest song_Gwest ._Atdt 
[_Atdchw Swn_Egu gweiddi_Be siarad_Be aneglur_Anscadu a_Cyscyd hefyd_Adf chwerthin_Be ]_Atdde ._Atdt 
[_Atdchw swn_Egu canu_Be piano_Egu a_Cyscyd phobl_Ebu yn_Arsym symud_Be a_Cyscyd chwerthin_Be ]_Atdde ._Atdt 
S2_Anon Chi_Rhapers2ll mo'yn_Egu sefyll_Be ?_Atdt 
Gw_Gwest '_Atddyf itho_unk 'n_Utra galed_Anscadu nawr_Adf come_Gwest on_Gwest good_Gwest [chwerthin]_Gwann [aneglur?]_Gwann ._Atdt 
[_Atdchw canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Good_Gwest da_Anscadu iawn_Adf ._Atdt 
Reit_Ebych te_Egu o_Rhapers3gu droies_unk i_Arsym hwn_Rhadangg '_Atddyf mla_unk 'n_Arsym ond_Arsym '_Atddyf s_Gwllyth dim_Adf mwy_Anscym o_Rhapers3gu gopie_Egll '_Atddyf da_Anscadu fi_Rhapers1u ._Atdt 
S4_Anon Ma_Bpres3u '_Atddyf nhw_Rhapers3ll yn_Arsym y_YFB bocs_Egu ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'di_Uberf roi_Be rhei_Rhaamh i_Arsym 'r_YFB bobol_Ebu sydd_Bpres3perth ddim_Egu oedd_Bamherff3u ddim_Egu yma_Adf ._Atdt 
S2_Anon O_Rhapers3gu mae_Bpres3u nhw_Rhapers3ll yn_Arsym y_YFB bocs_Egu ._Atdt 
S2_Anon Yn_Arsym y_YFB bocs_Egu fan_Ebu '_Atddyf yn_Arsym ._Atdt 
S1_Anon Ma_Bpres3u '_Atddyf pawb_Rhaamh 'di_Arsym ca'l_Ebu un_Rhaamh ._Atdt 
S4_Anon Ma_Bpres3u '_Atddyf pawb_Rhaamh 'di_Arsym ca'l_Ebu un_Rhaamh yndi_Bpres3u ._Atdt 
S2_Anon Ma_Bpres3u '_Atddyf pawb_Rhaamh efo_Arsym un_Rhaamh o_Rhapers3gu 's_Gwllyth e_Rhapers3gu ?_Atdt 
S2_Anon [aneglur?]_Gwann <_Atdchw pesychu_Be >_Atdde ._Atdt 
Reit_Ebych a_Cyscyd ewn_Bpres1ll ni_Rhapers1ll dros_Arsym y_YFB ranne_Ebll ta_Bpres3u reit_Anscadu ._Atdt 
S5_Anon [aneglur?]_Gwann ti_Rhapers2u '_Atddyf nabod_Be e_Rhapers3gu ?_Atdt 
[aneglur]_Gwann ._Atdt 
S2_Anon Reit_Ebych te_Egu dechreue_Bamherff3u ni_Rhapers1ll off_Adf gyda_Arsym 'r_YFB altos_Gwest ._Atdt 
Mae_Bpres3u 'ch_Rhadib2ll rhan_Ebu chi_Rhapers2ll yn_Arsym really_Gwest anodd_Anscadu ._Atdt 
So_Bpres1ll chi_Rhapers2ll 'n_Utra myn_Egu '_Atddyf i_Arsym bigo_Be fe_Rhapers3gu lan_Adf o_Arsym gwbwl_Anscadu ond_Arsym trio_Be 'ch_Rhadib2ll gore_Anseith ._Atdt 
Reit_Ebych te_Egu ._Atdt 
Gwel_Bgorch2u '_Atddyf lle_Rhagof '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Utra myn_Egu '_Atddyf o_Rhapers3gu ?_Atdt 
Un_Rhaamh dau_Rhifol tri_Rhifol a_Cyscyd ._Atdt 
[_Atdchw Canu_Be piano_Egu ]_Atdde ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Reit_Ebych chi_Rhapers2ll 'n_Uberf gw'bod_Be beth_Rhagof chi_Rhapers2ll 'n_Arsym '_Atddyf neud_Be '_Atddyf ych_Bpres2ll chi_Rhapers2ll ?_Atdt 
Reit_Ebych te_Egu a_Cyscyd 'r_YFB sopranos_Egbll rhaid_Egu chi_Rhapers2ll rannu_Be 'ch_Rhadib2ll hunen_Bgorch3ll yn_Arsym dou_Rhifold ._Atdt 
Rhywun_Rhaamh gyda_Arsym stiff_Gwest personality_Gwest [aneglur]_Gwann ._Atdt 
Reit_Ebych ._Atdt 
So_Adf ._Atdt 
Pwy_Rhagof sy_Bpres3perth 'n_Uberf mynd_Be i_Rhapers1u '_Atddyf neud_Be yr_Rhaperth ydych_Bpres2ll chi_Rhapers2ll 'n_Utra myn_Egu '_Atddyf i_Arsym rannu_Be fe_Rhapers3gu rhwng_Arsym rhes_Ebu gefn_Egu a_Cyscyd rhes_Ebu fla_unk 'n_Arsym ie_Adf ?_Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Gweld_Be e_Gwllyth pawb_Rhaamh ar_Arsym y_YFB tudalen_Egbu ble_Rhagof ma_Bpres3u 'r_YFB bar_Egu du_Anscadu '_Atddyf ne_Egu ie_Adf ?_Atdt 
Un_Rhaamh dau_Rhifol tri_Rhifol a_Cyscyd ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Reit_Ebych te_Egu so_Adf ._Atdt 
Chi_Rhapers2ll 'n_Arsym olreit_Adf [aneglur]_Gwann ?_Atdt 
'_Atddyf S_Gwllyth '_Atddyf im_unk ishe_Be mynd_Be drosto_Ar3gu fe_Rhapers3gu eto_Adf '_Atddyf ych_Bpres2ll chi_Rhapers2ll ?_Atdt 
Gret_Adf reit_Ebych ._Atdt 
So_Adf bar_Egu dau_Rhifol ddeg_Rhifol tri_Rhifol [aneglur?]_Gwann ._Atdt 
Na_Rhaperth ._Atdt 
Ie_Adf ._Atdt 
Un_Rhaamh dau_Rhifol ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Chi_Rhapers2ll 'n_Uberf gw'bod_Be y_YFB gweddill_Egu odych_unk chi_Rhapers2ll ?_Atdt 
Reit_Ebych te_Egu ewn_Bpres1ll ni_Rhapers1ll o_Arsym 'r_YFB dechre_Egu ._Atdt 
S'o_Bpres1ll chi_Rhapers2ll 'n_Uberf recordio_Be hwn_Rhadangg nawr_Adf '_Atddyf ych_Bpres2ll chi_Rhapers2ll ?_Atdt 
Good_Gwest ._Atdt 
O_Arsym 'r_YFB dechre_Egu te_Bpres3u ._Atdt 
Un_Rhaamh dau_Rhifol tri_Rhifol a_Cyscyd ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Wel_Ebych o'dd_Bamherff3u e_Rhapers3gu ddim_Adf yn_Arsym complete_Gwest disaster_Gwest o'dd_Bamherff3u e_Rhapers3gu ?_Atdt 
[chwerthin]_Gwann ._Atdt 
'M_Rhadib1u ond_Arsym slight_Gwest [chwerthin]_Gwann ._Atdt 
A_Cyscyd 'r_YFB peth_Egu yw_Bpres3u os_Egu ni_Rhapers1ll 'n_Uberf dweud_Be rhywbeth_Egu ofnadw_Anscadu '_Atddyf does_Bpres3amhen neb_Rhaamh yn_Arsym gwbod_Be it_Gwest 's_Gwllyth okay_Gwest ._Atdt 
Rhywun_Rhaamh yn_Arsym siarad_Be Sami_Ep o_Rhapers3gu 's_Gwllyth e_Rhapers3gu ?_Atdt 
Na_Rhaperth Whar_Gwest a_Gwest relief_Gwest ._Atdt 
Reit_Ebych te_Egu [aneglur?]_Gwann '_Atddyf to_Egu ._Atdt 
Un_Rhaamh ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Utra barod_Anscadu '_Atddyf da_Anscadu altos_Gwest starring_Gwest role_Gwest ._Atdt 
Fi_Rhapers1u 'n_Arsym [-]_Gwann ._Atdt 
Pwy_Rhagof yw_Bpres3u Elsa_Ep ?_Atdt 
Un_Rhaamh dau_Rhifol ._Atdt 
[_Atdchw Pobl_Ebu yn_Arsym sgwrsio_Be ar_Arsym draws_Arsym ei_Rhadib3gu gilydd_Egu ]_Atdde ._Atdt 
[_Atdchw Canu_Be pobl_Ebu a_Cyscyd phiano_Egu ]_Atdde ._Atdt 
S2_Anon Lic'o_Be ?_Atdt 
Gwahanol_Anscadu ._Atdt 
Reit_Ebych [saib]_Gwann ._Atdt 
Wel_Ebych o'n_Bamherff1u i_Rhapers1u 'n_Uberf teimlo_Be [aneglur?]_Gwann ._Atdt 
Chi_Rhapers2ll 'di_Uberf canu_Be hwn_Rhadangg o_Arsym bla_Egu 'n_Arsym '_Atddyf ych_Bpres2ll chi_Rhapers2ll ?_Atdt 
[_Atdchw Sgwrsio_Be cefndirol_Anscadu aneglur_Anscadu ]_Atdde ._Atdt 
S4_Anon Do_Egu ._Atdt 
S2_Anon Ma_Bpres3u '_Atddyf enwb_Anon 'di_Uberf gweud_Be so_Egu p'idwch_unk â_Arsym dweud_Be celwydd_Egu wrtha_Ar1u i_Rhapers1u [chwerthin]_Gwann ._Atdt 
S5_Anon Yn_Arsym ysgol_Ebu o'dd_Bamherff3u e_Rhapers3gu ._Atdt 
[_Atdchw Piano_Egu yn_Arsym canu_Egu a_Cyscyd sgwrsio_Be yn_Arsym y_YFB cefndir_Egu ]_Atdde ._Atdt 
S5_Anon Gano_Bdibdyf3u nhw_Rhapers3ll hon_Rhadangb yn_Arsym Eisteddfod_Ebu Genedlaethol_Anscadu [aneglur?]_Gwann ._Atdt 
S2_Anon Do_Egu fe_Rhapers3gu ?_Atdt 
S5_Anon Do_Egu cor_Egu merched_Ebll ._Atdt 
S2_Anon God_Gwest ma_Bpres3u '_Atddyf nhw_Rhapers3ll 'n_Uberf mynd_Be rownd_Ebu te_Bpres3u yfe_Bpres3u ._Atdt 
Na_Uneg o'n_Bamherff1u i_Rhapers1u 'n_Uberf feddwl_Be pan_Egu o'dd_Bamherff3u enwb_Anon yn_Arsym arwain_Be y_YFB cor_Egu [chwerthin]_Gwann ._Atdt 
Fi_Rhapers1u ffaelu_Be gweud_Be dim_Egu ._Atdt 
'Na_Bandangd gweithio_Be '_Atddyf da_Anscadu merched_Ebll ti_Rhapers2u wel_Ebych '_Atddyf ._Atdt 
'Na_Bandangd be_Rhagof '_Atddyf sy_Bpres3perth 'n_Uberf digwydd_Be [chwerthin]_Gwann [aneglur?]_Gwann '_Atddyf da_Anscadu cor_Egu meibion_Egll ._Atdt 
Reit_Ebych te_Egu '_Atddyf yn_Arsym ni_Rhapers1ll mynd_Be syth_Anscadu mewn_Arsym ?_Atdt 
S6_Anon Syth_Anscadu mewn_Arsym ._Atdt 
S2_Anon Dim_Adf [aneglur?]_Gwann ?_Atdt 
Let_Gwest 's_Gwllyth go_Gwest for_Gwest it_Gwest is_Gwest it_Gwest ?_Atdt [aneglur?]_Gwann [chwerthin]_Gwann ._Atdt 
Ar_Arsym ôl_Anscadu dau_Rhifol un_Rhaamh dau_Rhifol ._Atdt 
[_Atdchw Pobl_Ebu a_Cyscyd phiano_Egu yn_Arsym canu_Egu ]_Atdde ._Atdt 
S2_Anon 'Na_Bandangd fe_Uberf oedd_Bamherff3u hwnna_Rhadangg ddigon_Rhaamh good_Gwest ._Atdt 
-_Atdcys Fa_Egbu faint_Rhaamh o_Rhapers3gu flynydde_Ebll sy_Bpres3perth ers_Arsym i_Rhapers1u chi_Rhapers2ll 'di_Arsym '_Atddyf neud_Be hwnna_Rhadangg ?_Atdt 
Chi_Rhapers2ll 'n_Uberf cofio_Be 'n_Utra dda_Anscadu [saib]_Gwann ._Atdt 
Chi_Rhapers2ll mo'yn_Egu treial_Egu e_Rhapers3gu '_Atddyf to_Egu ?_Atdt 
Mo'yn_Egu treial_Egu e_Rhapers3gu '_Atddyf to_Egu ?_Atdt 
O_Rhapers3gu 's_Gwllyth rhywun_Rhaamh yn_Arsym meddwl_Egu beth_Rhagof uffach_Ebu yw_Bpres3u hwn_Rhadangg ?_Atdt 
Na_Rhaperth ?_Atdt 
O_Rhapers3gu 's_Gwllyth [-]_Gwann ._Atdt 
Os_Cyscyd rhywun_Rhaamh yn_Arsym meddwl_Egu alla_Bpres1u '_Atddyf i_Rhapers1u all-_Ublaen allai_Bamherff3u wneud_Be note_Gwest bash_Gwest e_Gwllyth 'i_Rhadib3bu hunan_Rhaatb ._Atdt 
-_Atdcys Al_Epg alla_Bpres1u '_Atddyf i_Arsym ddangos_Be right_Gwest okay_Gwest '_Atddyf nawn_Bpres1ll ni_Rhapers1ll [-]_Gwann ._Atdt 
Pawb_Rhaamh arall_Anscadu ar_Arsym ben_Egu 'u_Rhadib3ll huna_Bgorch2u 'n_Utra nawr_Adf am_Arsym funed_unk okay_Gwest un_Rhaamh dau_Rhifol tri_Rhifol a_Cyscyd ._Atdt 
[_Atdchw Sgwrsio_Be ar_Arsym draws_Arsym ]_Atdde ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd pherson_Egu ]_Atdde ._Atdt 
S2_Anon O_Arsym o_Rhapers3gu so_Adf ma_Bpres3u '_Atddyf nhw_Rhapers3ll ar_Arsym streic_Ebu nawr_Adf [aneglur?]_Gwann Os_Cyscyd bydde_Bamod3u rhywun_Egu yn_Arsym canu_Egu ._Atdt 
Un_Rhaamh ._Atdt 
Fi_Rhapers1u 'n_Arsym '_Atddyf whare_Be enwb_Anon 'na_Adf i_Arsym gyd_Egu ma_Bpres3u '_Atddyf pawb_Rhaamh yn_Arsym canu_Egu ?_Atdt 
Un_Rhaamh dau_Rhifol tri_Rhifol a_Cyscyd ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd pherson_Egu ]_Atdde ._Atdt 
S2_Anon [aneglur]_Gwann sopranos_Egbll [aneglur]_Gwann Okay_Gwest ma_Bpres3u '_Atddyf sopranos_Egbll un_Bancynnar angen_Egu oxygen_Gwest masks_Gwest a_Cyscyd [aneglur?]_Gwann central_Gwest European_Gwest [aneglur?]_Gwann yn_Arsym y_YFB canol_Egu [chwerthin]_Gwann ._Atdt 
Reit_Ebych ta_Egu oes_Bpres3amhen rhywun_Egu arall_Anscadu mo'yn_Egu help_Egbu ?_Atdt 
Dim_Adf yn_Arsym meddyliol_Anscadu ond_Arsym yn_Arsym cyffredinol_Anscadu ._Atdt 
Reit_Ebych altos_Gwest ._Atdt 
Sopranos_Egbll mae_Bpres3u jest_Adf yn_Arsym uchel_Anscadu trwy_Arsym 'r_YFB amser_Egu on_Gwest 'd_Gwllyth '_Atddyf yw_Bpres3u e_Rhapers3gu ?_Atdt 
On_Gwest '_Atddyf 'na_Adf ni_Rhapers1ll chi_Rhapers2ll ddewisodd_Bgorff3u hwn_Bandangg sopranos_Egbll so_Adf hei_Rhadib3bu [chwerthin]_Gwann Suck_Gwest it_Gwest up_Gwest [chwerthin]_Gwann ._Atdt 
Reit_Ebych te_Egu [aneglur]_Gwann '_Atddyf to_Egu ._Atdt 
Un_Rhaamh dau_Rhifol un_Rhaamh dau_Rhifol ._Atdt 
Co_Egu ni_Rhapers1ll off_Adf [chwerthin]_Gwann ._Atdt 
[_Atdchw Piano_Egu 'n_Uberf canu_Be ]_Atdde ._Atdt 
S2_Anon Mae_Bpres3u 'n_Utra myn_Egu '_Atddyf i_Arsym fod_Be yn_Arsym noson_Ebu hir_Anscadu reit_Ebych un_Rhaamh dau_Rhifol tri_Rhifol ._Atdt 
Y_YFB be_Rhagof '_Atddyf ?_Atdt 
-_Atdcys Sef_Cyscyd go_Gwest on_Gwest te_Bpres3u sefwch_Bgorch2ll lan_Adf [chwerthin]_Gwann ._Atdt 
Reit_Ebych un_Rhaamh dau_Rhifol ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Mo'yn_Egu '_Atddyf neud_Be unwaith_Adf '_Atddyf to_Egu ?_Atdt 
<_Atdchw SS_Gwest >_Atdde Ie_Adf ._Atdt 
S2_Anon [aneglur?]_Gwann ._Atdt 
Barod_Anscadu ?_Atdt 
Dim_Adf on_Gwest '_Atddyf pump_Rhifol [aneglur]_Gwann ni_Rhapers1ll 'n_Arsym gellu_unk g'neud_Be ._Atdt 
Barod_Anscadu ?_Atdt 
Ar_Arsym ôl_Anscadu dau_Rhifol ._Atdt 
Un_Rhaamh dau_Rhifol ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Chi_Rhapers2ll mo'yn_Egu break_Gwest bach_Anscadu nawr_Adf ?_Atdt 
<_Atdchw SS_Gwest >_Atdde Ie_Adf ._Atdt 
S2_Anon Reit_Ebych ._Atdt 
'_Atddyf S_Gwllyth a_Cyscyd rhywbeth_Rhaamh '_Atddyf da_Anscadu 'r_YFB management_Gwest i_Rhapers1u '_Atddyf weud_Be ?_Atdt 
S6_Anon '_Atddyf Dw_Bpres1u i_Rhapers1u 'm_Adf yn_Arsym me'l_Egu nag_Cyscyd oes_Bpres3amhen ?_Atdt 
'_Atddyf Da_Anscadu ni_Rhapers1ll 'di_Arsym ca'l_Ebu booking_Gwest do_Egu ar_Arsym gyfer_Egu Hydref_Egu dwy_Rhifol fil_Egu a_Arsym deunaw_Rhifold [aneglur?]_Gwann ._Atdt 
S2_Anon [chwerthin]_Gwann [=]_Gwann ma_Bpres3u '_Atddyf nhw_Rhapers3ll 'di_Arsym [/=]_Gwann ma_Bpres3u '_Atddyf nhw_Rhapers3ll 'di_Arsym bwco_unk 'r_YFB band_Egu yn_Arsym barod_Anscadu ar_Arsym gyfer_Egu -_Atdcys mi_Uberf so_Adf oe_unk '_Atddyf nhw_Rhapers3ll ffilu_unk '_Atddyf neud_Be e_Gwllyth 'r_YFB flwyddyn_Ebu hyn_Rhadangd so_Adf oe_unk '_Atddyf nhw_Rhapers3ll 'n_Utra awyddus_Anscadu iawn_Adf i_Arsym boco_Be ni_Rhapers1ll i_Arsym 'r_YFB flwyddyn_Ebu nesa_Anseith '_Atddyf ._Atdt 
S1_Anon Ddim_Egu yn_Arsym digon_Egu keen_Gwest i_Arsym wneud_Be noson_Ebu arbennig_Anscadu jest_Adf [chwerthin]_Gwann ddim_Egu yn_Arsym [-]_Gwann ._Atdt 
S2_Anon A_Cyscyd '_Atddyf wedo_Bdibdyf3u 'n_Arsym ti_Rhapers2u 'n_Arsym '_Atddyf mo_Adf '_Atddyf unrhyw_Anscadu beth_Rhagof chi_Rhapers2ll mo'yn_Egu rhyw_Bancynnar gyngerdd_Egbu neu_Cyscyd codi_Bpres2u arian_Egu bydd_Bdyf3u ni_Rhapers1ll 'na_Adf ._Atdt 
<_Atdchw SS_Gwest >_Atdde O_Rhapers3gu '_Atddyf whare_Be teg_Anscadu ._Atdt 
S2_Anon O_Rhapers3gu mae_Bpres3u nhw_Rhapers3ll 'n_Arsym dishgw'l_unk bwyd_Egu cofia_Bgorch2u so_Egu [-]_Gwann ._Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S2_Anon Jocan_Be ._Atdt 
S2_Anon Beth_Rhagof am_Arsym y_YFB thing_Gwest codi_Bpres2u arian_Egu ?_Atdt 
S6_Anon Ie_Adf ._Atdt 
S6_Anon '_Atddyf Da_Anscadu ni_Rhapers1ll 'n_Utra awyddus_Anscadu i_Arsym wneud_Be rhywbeth_Egu i_Arsym godi_Bpres2u arian_Egu '_Atddyf d_Gwllyth ydan_Bpres1ll ?_Atdt [aneglur?]_Gwann yn_Arsym yr_YFB Hydref_Egu so_Adf '_Atddyf falle_Adf allen_Bamherff1ll ni_Rhapers1ll [-]_Gwann ._Atdt 
S2_Anon Ie_Adf o_Arsym bosib_Anscadu ie_Adf ._Atdt 
S2_Anon Pennu_Be dyddiad_Egu ife_Bpres3u yw_Bpres3u 'r_YFB peth_Egu ._Atdt 
S6_Anon [=]_Gwann A_Cyscyd ma_Bpres3u '_Atddyf isho_Be [/=]_Gwann a_Cyscyd ma_Bpres3u '_Atddyf isho_Be noson_Ebu gymdeithasol_Anscadu arno_Ar3gu '_Atddyf ni_Rhapers1ll yn_Arsym fuan_Anscadu [chwerthin]_Gwann [aneglur?]_Gwann [=]_Gwann ond_Arsym [/=]_Gwann ond_Arsym 
[_Atdchw Pobl_Ebu yn_Arsym torri_Be ar_Arsym draws_Arsym a_Cyscyd chwerthin_Be ]_Atdde ._Atdt 
S7_Anon [aneglur?]_Gwann pwyllgor_Egu newydd_Anscadu ._Atdt 
S1_Anon Na_Adf fi_Rhapers1u 'di_Arsym '_Atddyf neud_Be e_Rhapers3gu am_Arsym ddigon_Egu o_Arsym amser_Egu nawr_Adf ._Atdt 
Mis_Egu Medi_Egu pwyllgor_Egu newydd_Anscadu [chwerthin]_Gwann ._Atdt 
S2_Anon [chwerthin]_Gwann ._Atdt 
S5_Anon Na_Adf chi_Rhapers2ll 'n_Arsym '_Atddyf neud_Be good_Gwest job_Gwest ._Atdt 
S1_Anon Ma_Bpres3u '_Atddyf dy_Banmedd2u enw_Egu di_Uberf cyn_Arsym '_Atddyf nny_Rhadib2u on_Gwest 'd_Gwllyth '_Atddyf yw_Bpres3u e_Rhapers3gu ?_Atdt 
O'dd_Bamherff3u ._Atdt 
S2_Anon Alla_Bpres1u '_Atddyf i_Rhapers1u jest_Adf gweud_Be o'n_Bamherff1u i_Rhapers1u 'n_Arsym [aneglur?]_Gwann amser_Egu egwyl_Ebu achos_Egu o'n_Bamherff1u i_Rhapers1u 'di_Uberf mynd_Be lan_Adf o'n_Bamherff1u i_Rhapers1u 'n_Utra desperate_Gwest i_Rhapers1u ga'l_Ebu jest_Adf am_Gwtalf rhywbeth_Rhaamh i_Arsym yfed_Be a_Cyscyd wedes_Bgorff1u i_Rhapers1u orange_Gwest juice_Gwest a_Cyscyd dim_Banmeint lot_Rhaamh [aneglur?]_Gwann ._Atdt 
A_Cyscyd '_Atddyf wedes_Bgorff1u i_Rhapers1u "_Atddyf o_Arsym alla_Bpres1u '_Atddyf i_Rhapers1u ga'l_Ebu orange_Gwest juice_Gwest "_Atddyf "_Atddyf ie_Adf "_Atddyf a_Cyscyd wedes_Bgorff1u i_Rhapers1u "_Atddyf o_Arsym faint_Egu yw_Bpres3u e_Rhapers3gu "_Atddyf a_Cyscyd wedodd_Bgorff3u hi_Rhapers3bu "_Atddyf o_Arsym wel_Bpres3u jest_Adf rhowch_Bgorch2ll cyfraniad_Egu "_Atddyf felly_Adf es_Bgorff1u i_Rhapers1u i_Arsym 'n_Utra boced_Ebu a_Arsym o'dd_Bamherff3u dim_Egu un_Rhaamh c_Gwllyth '_Atddyf in_Gwest '_Atddyf og_Ebu '_Atddyf da_Anscadu fi_Rhapers1u dim_Egu ond_Arsym papur_Egu pum_Rhifol punt_Ebu a_Cyscyd o'dd_Bamherff3u rhaid_Egu fi_Rhapers1u dalu_Be pum_Rhifol punt_Ebu a_Cyscyd o'n_Bamherff1u i_Rhapers1u 'n_Uberf meddwl_Be oh_Gwest my_Gwest God_Gwest mae_Bpres3u fel_Cyscyd bod_Be yn_Arsym Iceland_Gwest yma_Adf ._Atdt 
Sa_Bgorb1u i_Rhapers1u 'n_Uberf gweud_Be [aneglur?]_Gwann ond_Arsym o'dd_Bamherff3u rhai_Banmeint pobol_Ebu yn_Arsym cymryd_Be pethe_Egll a_Cyscyd jest_Adf cerdded_Be off_Adf ._Atdt 
Paid_Egbu â_Arsym becso_Be oedd_Bamherff3u e_Rhapers3gu ar_Arsym yn_Arsym tab_Egu i_Rhapers1u ti_Rhapers2u 'n_Arsym olreit_Adf [chwerthin]_Gwann ._Atdt 
Nawr_Adf te_Egu gwell_Anscym i_Arsym ni_Rhapers1ll wneud_Be bach_Egu o_Rhapers3gu rhyw_Egbu ganu_Be ._Atdt 
Newn_Bpres1ll ni_Rhapers1ll un_Bancynnar bach_Anscadu arall_Anscadu a_Arsym gorffen_Egu yfe_Bpres3u ?_Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
<_Atdchw SS_Gwest >_Atdde Ie_Adf ._Atdt 
S2_Anon [=]_Gwann Chi_Rhapers2ll 'di_Uberf bod_Be [/=]_Gwann chi_Rhapers2ll 'di_Uberf bod_Be yn_Arsym brysur_Anscadu yn_Arsym ca'l_Ebu cacen_Ebu a_Cyscyd joio_Be '_Atddyf ych_Bpres2ll chi_Rhapers2ll chi_Rhapers2ll 'n_Utra bown_Anscadu '_Atddyf o_Arsym fo_Adf 'n_Utra shattered_Gwest [chwerthin]_Gwann ._Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S6_Anon [aneglur?]_Gwann dechre_Be eto_Adf dydd_Egu [aneglur?]_Gwann ._Atdt 
'_Atddyf Nawn_Bgorch1ll ni_Rhapers1ll drafod_Be eto_Adf ar_Arsym ôl_Anscadu hanner_Egu tymor_Egu '_Atddyf falle_Adf ._Atdt 
[_Atdchw Siaradwr_Egu aneglur_Anscadu yn_Arsym y_YFB cefndir_Egu ]_Atdde ._Atdt 
S2_Anon [aneglur?]_Gwann ._Atdt 
Na_Rhaperth [=]_Gwann gad_Ebu e_Gwllyth fod_Be [/=]_Gwann gad_Ebu e_Gwllyth fod_Be ._Atdt 
Iawn_Ebych [chwerthin]_Gwann [aneglur?]_Gwann ._Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S2_Anon Ie_Adf fi_Rhapers1u 'n_Uberf synhwyro_Be '_Atddyf fyd_Egu ._Atdt 
Ie_Adf olreit_Adf ._Atdt 
Reit_Ebych be_Rhagof '_Atddyf chi_Rhapers2ll mo'yn_Egu canu_Be nawr_Adf te_Egu bois_Egll ?_Atdt 
[chwerthin]_Gwann ._Atdt 
O_Rhapers3gu [aneglur]_Gwann harmoni_Egu ._Atdt 
O_Ep ti_Rhapers2u 'm_Rhadib1u '_Atddyf bod_Be be_Rhagof '_Atddyf ti_Rhapers2u 'n_Utra treial_Egu addysgu_Be nhw_Rhapers3ll on_Gwest 'd_Gwllyth wyt_Bpres2u ti_Rhapers2u ?_Atdt 
Ti_Rhapers2u 'n_Arsym '_Atddyf bo_Adf '_Atddyf honestly_Gwest [aneglur]_Gwann ._Atdt 
[_Atdchw Nifer_Egu o_Arsym bobl_Ebu yn_Arsym siarad_Be yr_YFB un_Bancynnar pryd_Rhagof ]_Atdde ._Atdt 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
[_Atdchw Canu_Be piano_Egu a_Cyscyd phobl_Ebu ]_Atdde ._Atdt 
S2_Anon Waw_Ebych lovely_Gwest diolch_Egu enwb_Anon [aneglur?]_Gwann ._Atdt 
[_Atdchw Pobl_Ebu yn_Arsym siarad_Be ar_Arsym draws_Arsym ei_Rhadib3gu gilydd_Egu a_Arsym swn_Egu symud_Be dodrefn_Egll a_Cyscyd llestri_Egll ]_Atdde ._Atdt 
2347.862_Gwdig 