BA_Bangof Cymraeg_Ebu 
Cod_Bpres3u UCAS_Gwacr Ail_Rhitrefd Iaith_Ebu :_Atdcan Q_Gwllyth 560_Gwdig www.abertawe.ac.uk_Gwann 
Yn_Arsym Adran_Ebu y_YFB Gymraeg_Ebu ,_Atdcan Prifysgol_Ebu Abertawe_Ep rydym_Bpres1ll yn_Arsym :_Atdcan 
Ymrwymo_Be i_Arsym sicrhau_Be bod_Be ein_Banmedd1ll myfyrwyr_Egll yn_Arsym meithrin_Be ystod_Ebu eang_Anscadu o_Arsym sgiliau_Egll ,_Atdcan er_Arsym mwyn_Egu rhoiʼr_Bpresamhers siawns_Ebu orau_Anseith iddynt_Ar3ll ddisgleirio_Be yn_Arsym eu_Banmedd3ll cwrs_Anscadu ac_Cyscyd yn_Arsym eu_Banmedd3ll gyrfaoedd_Ebll yn_Arsym y_YFB dyfodol_Egu ._Atdt 
Ymroi_Be i_Arsym feithrin_Be yn_Arsym ein_Banmedd1ll myfyrwyr_Egll ddealltwriaeth_Ebu ddwfn_Anscadu o_Arsym feysydd_Egll fel_Cyscyd ieithyddiaeth_Ebu ,_Atdcan llenyddiaeth_Ebu a_Cyscyd diwylliant_Egu y_YFB Gymraeg_Ebu ._Atdt 
Trefnu_Be amrywiaeth_Ebu o_Arsym leoliadau_Egll gwaith_Ebu cyffrous_Anscadu fel_Cyscyd bod_Be ein_Banmedd1ll myfyrwyr_Egll yn_Arsym ennill_Egu profiadau_Egll gwerthfawr_Anscadu a_Rhaperth fydd_Bdyf3u yn_Arsym eu_Rhadib3ll paratoiʼn_Gwest dda_Anscadu ar_Arsym gyfer_Egu y_YFB gweithle_Egu ._Atdt 
Sicrhau_Be gofal_Egu bugeiliol_Anscadu oʼr_Gwest safon_Ebu uchaf_Anscadu a_Cyscyd chefnogaeth_Ebu barhaus_Anscadu iʼn_Gwest myfyrwyr_Egll yn_Arsym ystod_Ebu eu_Banmedd3ll cyfnod_Egu yn_Arsym Abertawe_Ep ._Atdt 
Barn_Ebu myfyrwyr_Egll am_Arsym ein_Banmedd1ll cwrs_Anscadu BA_Adf yn_Arsym y_YFB Gymraeg_Ebu 
Rebecca_Ep Morgan_Ep 
Shw'mae_Gwest !_Atdt 
Fy_Banmedd1u enw_Egu i_Rhapers1u ydy_Bpres3u Rebecca_Ep Morgan_Epg ._Atdt 
Yn_Arsym y_YFB brifysgol_Ebu ,_Atdcan astudiais_Bgorff1u i_Rhapers1u 'r_YFB llwybr_Egu ail_Rhitrefd iaith_Ebu ._Atdt 
Dw_Bpres1u i_Rhapers1u 'n_Uberf dod_Be o_Arsym gartref_Egu hollol_Anscadu ddi-_Ublaen Gymraeg_Ebu felly_Adf roedd_Bamherff3u y_YFB penderfyniad_Egu i_Arsym fynd_Be i_Arsym 'r_YFB brifysgol_Ebu i_Arsym astudio_Be Cymraeg_Ebu yn_Arsym un_Rhaamh anodd_Anscadu ._Atdt 
Ond_Arsym y_YFB penderfyniad_Egu hwn_Rhadangg oedd_Bamherff3u penderfyniad_Egu gorau_Anseith fy_Banmedd1u mywyd_Egu !_Atdt 
Roedd_Bamherff3u fy_Banmedd1u amser_Egu yn_Arsym y_YFB brifysgol_Ebu yn_Arsym un_Rhaamh bythgofiadwy_Anscadu ,_Atdcan o_Arsym ran_Ebu fy_Banmedd1u astudiaethau_Ebll a_Cyscyd fy_Banmedd1u mywyd_Egu cymdeithasol_Anscadu ._Atdt 
O_Arsym ran_Ebu yr_YFB Adran_Ebu Gymraeg_Ebu ,_Atdcan roedden_Bamherff1ll nhw_Rhapers3ll mor_Adf gefnogol_Anscadu ._Atdt 
Ar_Arsym unrhyw_Bancynnar adeg_Ebu ,_Atdcan gallwn_Bamherff1u i_Rhapers1u anfon_Be e-bost_Egu at_Arsym aelod_Egu o_Arsym staff_Egll ,_Atdcan neu_Cyscyd fynd_Be i'w_Arsym swyddfa_Ebu ,_Atdcan a_Cyscyd byddent_Bamod3ll yn_Arsym barod_Anscadu i_Arsym dreulio_Be amser_Egu yn_Arsym gweithio_Be gyda_Arsym fi_Rhapers1u ._Atdt 
Lauren_Ep Evans_Ep 
Lauren_Ep Evans_Ep ydw_Bpres1u i_Rhapers1u ._Atdt 
Rydw_Bpres1u i_Rhapers1u wedi_Adf dysgu_Be Cymraeg_Ebu fel_Cyscyd ail_Rhitrefd iaith_Ebu ._Atdt 
Mae_Bpres3u cefnogaeth_Ebu y_YFB darlithwyr_Egll yn_Arsym golygu_Be bod_Be pob_Egll myfyriwr_Egu yn_Arsym llwyddo_Be ac_Cyscyd yn_Arsym datblygu_Be yn_Arsym gyflym_Anscadu ._Atdt 
Mae_Bpres3u amrywiaeth_Ebu o_Arsym fodiwlau_Egll y_Rhadib1u gallwch_Bgorch2ll chi_Rhapers2ll eu_Rhadib3ll hastudio_Be ac_Cyscyd mae_Bpres3u 'r_YFB dosbarthiadau_Egll yn_Arsym fach_Egu sy_Bpres3perth 'n_Uberf sicrhau_Be bod_Be gyda_Arsym chi_Rhapers2ll gyfle_Egu gwych_Anscadu i_Arsym gael_Be digon_Egu o_Arsym help_Egbu gan_Arsym eich_Banmedd2ll darlithwyr_Egll a_Cyscyd 'ch_Rhadib2ll cydfyfyrwyr_unk ._Atdt 
Mae_Bpres3u 'r_YFB Gymdeithas_Ebu Gymraeg_Ebu yn_Arsym Abertawe_Ep yn_Arsym ardderchog_Anscadu ac_Cyscyd mae_Bpres3u 'n_Utra gyfle_Egu da_Anscadu i_Arsym ymarfer_Ebu eich_Banmedd2ll iaith_Ebu ._Atdt 
Roeddwn_Bamherff1u yn_Arsym ffodus_Anscadu iawn_Adf i_Arsym gael_Be fy_Rhadib1u ethol_Anscadu yn_Arsym Swyddog_Egu yr_YFB Iaith_Ebu Gymraeg_Ebu ar_Arsym gyfer_Egu Undeb_Egu Myfyrwyr_Egll Prifysgol_Ebu Abertawe_Ep ._Atdt 
Erbyn_Arsym hyn_Rhadangd ,_Atdcan rwy_Bpres1u 'n_Uberf cwblhau_Be gradd_Egbu MA_Gwtalf a_Cyscyd dw_Bpres1u i_Rhapers1u 'n_Uberf gweithio_Be yng_Arsym Nghanolfan_Egbu Gymraeg_Ebu y_YFB Brifysgol_Ebu ,_Atdcan Ty_Egu ̂_Gwsym 'r_YFB Gwrhyd_Ep ._Atdt 
Yn_Arsym bendant_Anscadu ,_Atdcan mae_Bpres3u astudio_Be 'r_YFB Gymraeg_Ebu yn_Arsym agor_Be llawer_Rhaamh o_Arsym ddrysau_Egll !_Atdt 
Os_Cyscyd bydd_Bdyf3u gennych_Ar2ll unrhyw_Bancynnar gwestiynau_Egll am_Arsym y_YFB cwrs_Egu neu_Cyscyd am_Arsym astudio_Be yn_Arsym Abertawe_Ep ,_Atdcan anfonwch_Bgorch2ll e-bost_Egu at_Arsym :_Atdcan 
Dr_Gwtalf Hannah_Ep Sams_Gwest ,_Atdcan Swyddog_Egu Derbyn_Be yr_YFB Adran_Ebu :_Atdcan H.L.Sams@abertawe.ac.uk_Gwann neu_Cyscyd at_Arsym Dr_Gwtalf Rhian_Ep Jones_Ep ,_Atdcan Pennaeth_Egu yr_YFB Adran_Ebu :_Atdcan Rhian.E.Jones@abertawe.ac.uk_Gwann neu_Cyscyd ffoniwch_Bgorch2ll (_Atdchw 01792_Gwdig )_Atdde 606942_Gwdig ._Atdt 
Dangosodd_Bgorff3u canlyniadau_Egll 'r_YFB Fframwaith_Egu Rhagoriaeth_Ebu Ymchwil_Egu diwethaf_Anscadu fod_Be 100_Gwdig %_Gwsym o_Arsym waith_Ebu ymchwil_Egu y_YFB Gymraeg_Ebu o_Arsym safon_Ebu ryngwladol_Anscadu neu_Cyscyd 'n_Utra uwch_Anscadu na_Adf hynny_Rhadangd ._Atdt 
Dyma_Adf enghreifftiau_Ebll o_Arsym gyhoeddiadau_Egu gan_Arsym staff_Egll a_Cyscyd myfyrwyr_Egll ._Atdt 