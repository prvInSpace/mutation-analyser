CYNLLUN_Egu PERSONOL_Anscadu GADAEL_Be MEWN_Arsym ARGYFWNG_Egu (_Atdchw CPGA_Gwacr )_Atdde 
HOLIADUR_Egu CASGLU_Be GWYBODAETH_Ebu A_Cyscyd THEMPLED_Egu AR_Arsym GYFER_Egu Y_YFB CPGA_Gwacr 
Defnyddiwch_Bgorch2ll ,_Atdcan ar_Arsym y_YFB cyd_Egu â_Arsym 'r_YFB Cod_Bpres3u Canllawiau_Egbll -_Atdcys 
Dulliau_Egll Dianc_Be i_Arsym Bobl_Ebu Anabl_Anscadu (_Atdchw 1_Gwdig ._Atdt CM_Gwtalf ._Atdt 180_Gwdig )_Atdde 
Bydd_Bdyf3u angen_Egu CPGA_Gwacr ar_Arsym wahân_Anscadu ar_Arsym unrhyw_Bancynnar leoliadau_Egll /_Gwsym adeiladau_Egbll cyngor_Egu eraill_Anscadu y_Rhadib1u gallai_Bamherff3u 'r_YFB unigolyn_Egu eu_Rhadib3ll defnyddio_Be neu_Cyscyd ymweld_Be â_Arsym nhw_Rhapers3ll 'n_Utra rheolaidd_Anscadu ._Atdt 
Enw_Egu 
Lleoliad_Egu /_Gwsym Lleoliadau_Egll 
Ymgymerwyd_Bgorffamhers gan_Arsym 
Rheolwr_Egu yr_YFB Adeilad_Egbu 
Dyddiad_Egu Asesu_Be 
Dyddiad_Egu Adolygu_Be 
CYFRINACHOL_Anscadu 
RHAN_Ebu UN_Rhaamh -_Atdcys CASGLU_Be GWYBODAETH_Ebu 
Gofynnir_Bpresamhers y_YFB cwestiynau_Egll canlynol_Anscadu i_Arsym 'r_YFB unigolyn_Egu ;_Atdcan 
Nam_Egu ar_Arsym y_YFB Clyw_Egu 
Nac_Uneg oes_Bpres3amhen 
Oes_Bpres3amhen 
Mae_Bpres3u angen_Egu gweithredu_Be pellach_Anscym 
Pe_Cyscyd bai_Egu argyfwng_Egu ,_Atdcan a_Rhaperth fyddech_Bamod2ll yn_Arsym gallu_Egu gadael_Be yr_YFB adeilad_Egbu heb_Arsym gymorth_Egu ?_Atdt 
Ydych_Bpres2ll chi_Rhapers2ll 'n_Uberf gallu_Be clywed_Be y_YFB larwm_Egu tân_Egu pan_Cyscyd fydd_Bdyf3u yn_Arsym cael_Be ei_Rhadib3gu actifadu_unk ,_Atdcan o_Arsym dan_Egu amgylchiadau_Egll arferol_Anscadu ?_Atdt 
A_Cyscyd oes_Bpres3amhen gennych_Ar2ll ,_Atdcan hyd_Arsym y_YFB gwn_Egu i_Rhapers1u ,_Atdcan unrhyw_Bancynnar system_Ebu neu_Cyscyd ddyfais_Ebu arbennig_Anscadu neu_Cyscyd bwrpasol_Anscadu ar_Arsym gyfer_Egu clywed_Be a_Rhaperth allai_Bamherff3u eich_Rhadib2ll helpu_Be i_Arsym 'ch_Rhadib2ll gwneud_Be yn_Arsym ymwybodol_Anscadu bod_Be y_YFB larwm_Egu tân_Egu yn_Arsym seinio_Be ?_Atdt 
A_Cyscyd oes_Bpres3amhen unrhyw_Bancynnar fesur_Be arall_Anscadu a_Rhaperth fyddai_Bamod3u 'n_Arsym eich_Rhadib2ll cynorthwyo_Be ?_Atdt 
Golwg_Ebu 
Nac_Uneg oes_Bpres3amhen 
Oes_Bpres3amhen 
Mae_Bpres3u angen_Egu gweithredu_Be pellach_Anscym 
Pe_Cyscyd bai_Egu argyfwng_Egu ,_Atdcan a_Rhaperth fyddech_Bamod2ll yn_Arsym gallu_Egu gadael_Be yr_YFB adeilad_Egbu heb_Arsym gymorth_Egu ?_Atdt 
Ydych_Bpres2ll chi_Rhapers2ll 'n_Uberf defnyddio_Be cymorth_Egu i_Arsym 'ch_Rhadib2ll helpu_Be i_Arsym symud_Be o_Arsym gwmpas_Egu yr_YFB adeilad_Egbu ?_Atdt 
(_Atdchw ffon_Ebu gerdded_Be wen_Ebu ,_Atdcan ci_Egu cymorth_Egu neu_Cyscyd offer_Egll arall_Anscadu )_Atdde 
A_Rhaperth fyddai_Bamod3u arwyddion_Egbll cyffyrddol_Anscadu neu_Cyscyd wybodaeth_Ebu ar_Arsym wyneb_Egu y_YFB llawr_Egu /_Gwsym wal_Ebu yn_Arsym ddefnyddiol_Anscadu i_Rhapers1u chi_Rhapers2ll ?_Atdt 
A_Cyscyd oes_Bpres3amhen unrhyw_Bancynnar broblemau_Egbll eraill_Anscadu yr_Rhaperth hoffech_Bamherff2ll dynnu_Be sylw_Egu atynt_Ar3ll neu_Cyscyd fesurau_Egll a_Rhaperth allai_Bamherff3u fod_Be o_Arsym gymorth_Egu i_Rhapers1u chi_Rhapers2ll ?_Atdt 
Datblygiadol_Anscadu /_Gwsym Iechyd_Egu Meddwl_Be 
Nac_Egbu oes_Bpres3amhen 
Oes_Bpres3amhen 
Mae_Bpres3u angen_Egu gweithredu_Be pellach_Anscym 
Pe_Cyscyd bai_Egu argyfwng_Egu ,_Atdcan a_Rhaperth fyddech_Bamod2ll yn_Arsym gallu_Egu gadael_Be yr_YFB adeilad_Egbu heb_Arsym gymorth_Egu ?_Atdt 
Os_Cyscyd na_Rhaperth ,_Atdcan pa_Adf gymorth_Egu sy_Bpres3perth 'n_Utra ofynnol_Anscadu i_Arsym 'ch_Rhadib2ll galluogi_Be i_Arsym adael_Be yr_YFB adeilad_Egbu yn_Arsym ddiogel_Anscadu ?_Atdt 
Symudedd_Egu 
Nac_Uneg oes_Bpres3amhen 
Oes_Bpres3amhen 
Mae_Bpres3u angen_Egu gweithredu_Be pellach_Anscym 
Pe_Cyscyd bai_Egu argyfwng_Egu ,_Atdcan a_Rhaperth fyddech_Bamod2ll yn_Arsym gallu_Egu gadael_Be yr_YFB adeilad_Egbu heb_Arsym gymorth_Egu ?_Atdt 
Os_Cyscyd nad_Rhaperth ydych_Bpres2ll ,_Atdcan a_Cyscyd oes_Bpres3amhen angen_Egu cymorth_Egu aelod_Egu o_Arsym staff_Egll arnoch_Ar2ll i_Arsym adael_Be yr_YFB adeilad_Egbu ?_Atdt 
Oes_Bpres3amhen gennych_Ar2ll chi_Rhapers2ll /_Gwsym a_Cyscyd ydych_Bpres2ll yn_Arsym defnyddio_Be /_Gwsym oes_Bpres3amhen angen_Egu cadair_Ebu olwyn_Ebu arnoch_Ar2ll ?_Atdt 
A_Cyscyd oes_Bpres3amhen angen_Egu y_YFB gadair_Ebu olwyn_Ebu o_Arsym dan_Arsym bob_Egll amgylchiad_Egu ?_Atdt 
A_Cyscyd yw_Bpres3u 'r_YFB gadair_Ebu olwyn_Ebu o_Arsym faint_Egu safonol_Anscadu ,_Atdcan neu_Cyscyd 'n_Utra drydanol_Anscadu gyda_Arsym dimensiynau_Egll ehangach_Anscym ?_Atdt 
A_Cyscyd ydych_Bpres2ll yn_Arsym gallu_Egu trosglwyddo_Be o_Arsym gadair_Ebu olwyn_Ebu i_Arsym gadair_Ebu ddianc_Be heb_Arsym gymorth_Egu ?_Atdt 
Mae_Bpres3u gan_Arsym y_YFB cadeiriau_Ebll dianc_Be sedd_Ebu bwced_Egu ac_Cyscyd maent_Bpres3ll â_Arsym chefn_Egu crwn_Anscadu ._Atdt 
Mae_Bpres3u seddi_Ebll a_Cyscyd byrddau_Egll cefn_Egu ychwanegol_Anscadu ar_Arsym gael_Be i_Arsym ddarparu_Be sedd_Ebu a_Cyscyd chefn_Egu caled_Anscadu ._Atdt 
A_Rhaperth fydd_Bdyf3u angen_Egu y_YFB naill_Anscadu neu_Cyscyd 'r_YFB llall_Rhaamh arnoch_Ar2ll neu_Cyscyd 'r_YFB ddau_Rhifol er_Arsym mwyn_Egu atal_Be anaf_Egu i_Rhapers1u chi_Rhapers2ll ?_Atdt 
A_Cyscyd oes_Bpres3amhen gennych_Ar2ll unrhyw_Bancynnar sylwadau_Egll eraill_Anscadu neu_Cyscyd a_Cyscyd ydych_Bpres2ll yn_Arsym ymwybodol_Anscadu o_Rhapers3gu unrhyw_Bancynnar offer_Egll arall_Anscadu a_Rhaperth allai_Bamherff3u eich_Rhadib2ll helpu_Be i_Arsym ddianc_Be yn_Arsym ddiogel_Anscadu ?_Atdt 
Gwybodaeth_Ebu Gyffredinol_Anscadu 
Nac_Uneg oes_Bpres3amhen 
Oes_Bpres3amhen 
Mae_Bpres3u angen_Egu gweithredu_Be pellach_Anscym 
A_Rhaperth fyddai_Bamod3u 'r_YFB mesurau_Egll sydd_Bpres3perth eu_Banmedd3ll hangen_Egu arnoch_Ar2ll i_Arsym adael_Be yr_YFB adeilad_Egbu mewn_Arsym argyfwng_Egu yn_Arsym effeithio_Be 'n_Utra andwyol_Anscadu ar_Arsym allu_Egu pobl_Ebu eraill_Anscadu i_Arsym ddianc_Be yn_Arsym ddiogel_Anscadu ?_Atdt 
A_Cyscyd oes_Bpres3amhen gofyniad_Egu i_Arsym staff_Egll gael_Be eu_Rhadib3ll hyfforddi_Be i_Arsym 'ch_Rhadib2ll cynorthwyo_Be ?_Atdt 
Ydych_Bpres2ll chi_Rhapers2ll 'n_Uberf gwybod_Be am_Arsym unrhyw_Bancynnar aelod_Egu o_Arsym staff_Egll a_Rhaperth fyddai_Bamod3u 'n_Utra fodlon_Anscadu /_Gwsym amharod_Anscadu i_Arsym helpu_Be ?_Atdt 
A_Cyscyd oes_Bpres3amhen unrhyw_Bancynnar wybodaeth_Ebu arall_Anscadu a_Rhaperth fyddai_Bamod3u 'n_Arsym eich_Rhadib2ll helpu_Be i_Arsym ddianc_Be o_Arsym 'r_YFB adeilad_Egbu ?_Atdt 
RHAN_Ebu DAU_Rhifol -_Atdcys Y_YFB TREFNIADAU_Egll GOFYNNOL_Anscadu 
Mae_Bpres3u 'r_YFB trefniadau_Egll canlynol_Anscadu i'w_Arsym sefydlu_Be drwy_Arsym ymatebion_Egll i_Arsym ran_Ebu un_Rhaamh (_Atdchw uchod_Adf )_Atdde a_Cyscyd thrafodaeth_Ebu gyda_Arsym 'r_YFB unigolyn_Egu 
CYNORTHWYWYR_Gwacr A_Arsym GYTUNWYD_Bgorffamhers :_Atdcan 
Mae_Bpres3u 'r_YFB bobl_Ebu a_Rhaperth ganlyn_Be wedi_Uberf cytuno_Be i_Arsym roi_Be cymorth_Egu wrth_Arsym ddianc_Be yr_YFB adeilad_Egbu 
Enw_Egu 
Manylion_Egll cyswllt_Egu 
Enw_Egu 
Manylion_Egll cyswllt_Egu 
DULLIAU_Egll CYMORTH_Egu :_Atdcan 
(_Atdchw e_Rhapers3gu ._Atdt e_Rhapers3gu ._Atdt gweithdrefnau_Ebll trosglwyddo_Be ,_Atdcan dulliau_Egll o_Arsym arweiniad_Egu ,_Atdcan ac_Cyscyd ati_Ar3bu ._Atdt )_Atdde 
Dyma_Adf rai_Banmeint enghreifftiau_Ebll o_Arsym ddulliau_Egll cymorth_Egu :_Atdcan 
1_Gwdig )_Atdde dianc_Be dau_Rhifol gam_Egu (_Atdchw e_Rhapers3gu ._Atdt e_Rhapers3gu ._Atdt gall_Bpres3u y_YFB person_Egu fod_Be yn_Arsym araf_Anscadu neu_Cyscyd 'n_Utra ansefydlog_Anscadu yn_Arsym defnyddio_Be 'r_YFB grisiau_Egll ,_Atdcan ond_Arsym gall_Bpres3u wneud_Be hynny_Rhadangd heb_Arsym gymorth_Egu )_Atdde ._Atdt 
Mae_Bpres3u 'r_YFB person_Egu yn_Arsym mynd_Be i_Arsym fan_Egbu lloches_Ebu ddiogel_Anscadu ,_Atdcan yn_Arsym aros_Be i_Arsym bawb_Rhaamh arall_Anscadu glirio_Be 'r_YFB ardal_Ebu ac_Cyscyd yna_Adf 'n_Uberf mynd_Be allan_Adf ei_Rhadib3bu hun_Rhaatb drwy_Arsym 'r_YFB llwybrau_Egll arferol_Anscadu cyn_Adf gynted_Anscyf â_Arsym phosibl_Anscadu ._Atdt 
2_Gwdig )_Atdde dianc_Be â_Arsym chymorth_Egu ond_Arsym heb_Arsym angen_Egu cyfarpar_Egu (_Atdchw e_Rhapers3gu ._Atdt e_Rhapers3gu ._Atdt efallai_Adf y_Rhaperth bydd_Bdyf3u angen_Egu ar_Arsym yr_YFB unigolyn_Egu rhywun_Rhaamh i_Arsym ddarparu_Be braich_Ebu i'w_Arsym sefydlogi_Be wrth_Arsym ddefnyddio_Be 'r_YFB grisiau_Egll )_Atdde ._Atdt 
Bydd_Bdyf3u cydweithiwr_Egu sy_Bpres3perth 'n_Utra agos_Anscadu yn_Arsym ddaearyddol_Anscadu yn_Arsym cael_Be ei_Rhadib3gu nodi_Be 'n_Utra benodol_Anscadu (_Atdchw ynghyd_Adf ag_Arsym un_Rhaamh arall_Banmeint os_Cyscyd yw_Bpres3u ar_Arsym wyliau_Ebll neu_Cyscyd 'n_Utra absennol_Anscadu oherwydd_Cyscyd salwch_Egu )_Atdde a_Rhaperth fydd_Bdyf3u yn_Arsym cwrdd_Egu â_Arsym 'r_YFB unigolyn_Egu mewn_Arsym lle_Egu lloches_Ebu diogel_Anscadu ._Atdt 
Byddant_Bdyf3ll yn_Arsym mynd_Be i_Arsym fan_Egbu lloches_Ebu ddiogel_Anscadu ,_Atdcan yn_Arsym aros_Be i_Arsym bawb_Rhaamh arall_Anscadu glirio_Be 'r_YFB ardal_Ebu ac_Cyscyd yna_Adf 'n_Uberf mynd_Be allan_Adf drwy_Arsym 'r_YFB llwybrau_Egll arferol_Anscadu cyn_Adf gynted_Anscyf â_Arsym phosibl_Anscadu ._Atdt 
3_Gwdig )_Atdde Gellir_Bpresamhers dianc_Be yr_YFB adeilad_Egbu gyda_Arsym chymorth_Egu cyfarpar_Egu megis_Cyscyd cadair_Ebu ddianc_Be (_Atdchw e_Rhapers3gu ._Atdt e_Rhapers3gu ._Atdt efallai_Adf y_Rhaperth bydd_Bdyf3u y_YFB person_Egu yn_Arsym llwyr_Anscadu ddibynnol_Anscadu ar_Arsym gadair_Ebu olwyn_Ebu am_Arsym symudedd_Egu ond_Arsym gall_Bpres3u drosglwyddo_Be ei_Rhadib3bu hun_Rhaatb )_Atdde ._Atdt 
Hyfforddiant_Egu sydd_Bpres3perth ei_Banmedd3gu angen_Egu 
Lle_Anscadu y_YFB bo_Adf 'n_Utra briodol_Anscadu ,_Atdcan efallai_Adf y_Rhaperth bydd_Bdyf3u angen_Egu hyfforddiant_Egu ychwanegol_Anscadu (_Atdchw er_Arsym enghraifft_Ebu ,_Atdcan gan_Arsym ddefnyddiwr_Egu cadair_Ebu olwyn_Ebu sy_Bpres3perth 'n_Uberf trosglwyddo_Be i_Arsym gadair_Ebu ddianc_Be ,_Atdcan ac_Cyscyd i_Arsym 'r_YFB cynorthwywyr_unk ar_Arsym sut_Egbu i_Arsym ddefnyddio_Be 'r_YFB gadair_Ebu )_Atdde ._Atdt 
Bydd_Bdyf3u angen_Egu cofnodi_Be hyn_Rhadangd ._Atdt 
Rhaid_Egu cynnal_Be hyfforddiant_Egu o_Arsym leiaf_Anseith unwaith_Adf y_YFB flwyddyn_Ebu ,_Atdcan a_Cyscyd dwywaith_Adf yn_Arsym ddelfrydol_Anscadu ._Atdt 
Ym_Arsym mhob_Egll achos_Egu ,_Atdcan bydd_Bdyf3u angen_Egu profi_Be 'r_YFB weithdrefn_Ebu ddianc_Be lawn_Anscadu o_Arsym leiaf_Anseith unwaith_Adf y_YFB flwyddyn_Ebu ._Atdt 
Y_YFB CYFARPAR_Egu SYDD_Bpres3perth I'W_Arsym DDARPARU_Be :_Atdcan 
Y_YFB Cyfarpar_Egu sydd_Bpres3perth ei_Banmedd3gu angen_Egu 
Ar_Arsym gael_Be 
DULLIAU_Egll DIANC_Be 
Gallai_Bamherff3u darparu_Be cadeiriau_Ebll i_Arsym ddianc_Be yr_YFB adeilad_Egbu helpu_Be pobl_Ebu anabl_Anscadu ond_Arsym rhaid_Egu ystyried_Be y_YFB materion_Egll fel_Cyscyd a_Rhaperth ganlyn_Be ._Atdt 
•_Gwsym Mae_Bpres3u angen_Egu i_Arsym ddefnyddwyr_Egll cadeiriau_Ebll olwyn_Ebu a_Cyscyd 'r_YFB gweithwyr_Egll sy_Bpres3perth 'n_Uberf defnyddio_Be cadeiriau_Ebll dianc_Be gael_Be eu_Rhadib3ll hyfforddi_Be 'n_Utra briodol_Anscadu i'w_Arsym defnyddio_Be er_Arsym mwyn_Egu osgoi_Be damweiniau_Ebll a_Cyscyd sicrhau_Be diogelwch_Egu digonol_Anscadu ar_Arsym gyfer_Egu defnyddwyr_Egll eraill_Anscadu y_YFB grisiau_Egll ._Atdt 
Bydd_Bdyf3u angen_Egu hyfforddi_Be nifer_Egu digonol_Anscadu o_Arsym staff_Egll i_Arsym ddefnyddio_Be 'r_YFB gadair_Ebu ,_Atdcan ac_Cyscyd mewn_Arsym unrhyw_Bancynnar weithrediadau_Egll codi_Bpres2u a_Cyscyd chario_Be ._Atdt 
Bydd_Bdyf3u angen_Egu asesiad_Egu risg_Egu codi_Bpres2u a_Cyscyd chario_Be ._Atdt 
•_Gwsym Bydd_Bdyf3u angen_Egu cael_Be digon_Egu o_Arsym staff_Egll ar_Arsym gael_Be bob_Egll amser_Egu sydd_Bpres3perth wedi_Uberf 'u_Rhadib3ll hyfforddi_Be 'n_Utra briodol_Anscadu i_Arsym oruchwylio_Be a_Cyscyd gweithredu_Be cadeiriau_Ebll dianc_Be ._Atdt 
•_Gwsym Yn_Arsym y_YFB rhan_Ebu fwyaf_Anseith o_Arsym achosion_Egll ,_Atdcan bydd_Bdyf3u angen_Egu i_Arsym 'r_YFB defnyddiwr_Egu cadair_Ebu olwyn_Ebu drosglwyddo_Be 'n_Utra ôl_Anscadu i'w_Arsym gadair_Ebu ei_Rhadib3bu hun_Rhaatb yn_Arsym y_YFB man_Egbu ymgynnull_Be ,_Atdcan gan_Arsym nad_Rhaperth yw_Bpres3u 'r_YFB cadeiriau_Ebll dianc_Be yn_Arsym ddigon_Egu cyfforddus_Anscadu i'w_Arsym defnyddio_Be am_Arsym gyfnod_Egu hir_Anscadu ._Atdt 
Dylid_Egu dirprwyo_Be rhywun_Egu i_Arsym sicrhau_Be bod_Be y_YFB gadair_Ebu olwyn_Ebu yn_Arsym cael_Be ei_Rhadib3bu chymryd_Be i_Arsym 'r_YFB man_Egbu ymgynnull_Be ar_Arsym yr_YFB un_Bancynnar pryd_Rhagof ._Atdt 
•_Gwsym Rhaid_Egu bod_Be dulliau_Egll storio_Be 'r_YFB cadeiriau_Ebll sicrhau_Be eu_Rhadib3ll bod_Be bob_Egll amser_Egu ar_Arsym gael_Be yn_Arsym rhwydd_Anscadu ac_Cyscyd nid_Uneg ydynt_Bpres3ll yn_Arsym rhwystro_Be 'r_YFB ffordd_Ebu o_Arsym ddianc_Be o_Arsym dan_Egu amgylchiadau_Egll arferol_Anscadu ._Atdt 
•_Gwsym Mae_Bpres3u 'n_Utra anochel_Anscadu y_Rhadib1u bydd_Bdyf3u defnyddio_Be cadair_Ebu o_Arsym 'r_YFB fath_Banhwyr yn_Arsym cyfyngu_Be ar_Arsym y_YFB defnydd_Egu o_Arsym lwybr_Egu dianc_Be gan_Arsym eraill_Rhaamh ._Atdt 
Er_Arsym mwyn_Egu lleihau_Be problem_Egbu tagfeydd_Ebll mewn_Arsym eiddo_Egu ,_Atdcan dylai_Bamherff3u 'r_YFB rhai_Rhaamh sy_Bpres3perth 'n_Uberf defnyddio_Be 'r_YFB cadeiriau_Ebll ganiatáu_Be i_Arsym 'r_YFB rhan_Ebu fwyaf_Anseith o_Arsym bobl_Ebu yr_YFB adeilad_Egbu eu_Rhadib3ll pasio_Be cyn_Arsym iddynt_Ar3ll fynd_Be i_Arsym lawr_Egu y_YFB llwybr_Egu dianc_Be ._Atdt 
Mae_Bpres3u hyn_Rhadangd er_Arsym diogelwch_Egu meddiannydd_Egu y_YFB gadair_Ebu ,_Atdcan y_YFB bobl_Ebu sy_Bpres3perth 'n_Uberf cynorthwyo_Be a_Cyscyd defnyddwyr_Egll eraill_Anscadu yr_YFB adeilad_Egbu sy_Bpres3perth 'n_Uberf defnyddio_Be 'r_YFB un_Bancynnar grisiau_Egll ._Atdt 
Y_YFB WEITHDREFN_Ebu YMADAEL_Be /_Gwsym DIANC_Be :_Atdcan 
(_Atdchw Nodiadau_Egll cam-wrth-gam_unk yn_Arsym dechrau_Egu o_Arsym 'r_YFB larwm_Egu cyntaf_Anscadu )_Atdde 
Cam_Epb 
Dull_Egu a_Rhaperth Gytunwyd_Bgorffamhers 
Dylid_Egu cadw_Be rhannau_Ebll Un_Rhaamh a_Cyscyd Dau_Rhifol o_Arsym 'r_YFB CPGA_Gwacr ar_Arsym y_YFB Ffeil_Ebu /_Gwsym Cofnod_Egu Personol_Anscadu ._Atdt 
Er_Arsym resymau_Egll cyfrinachedd_Egu ,_Atdcan ni_Uneg ddylai_Bamherff3u Rheolwr_Egu yr_YFB Adeilad_Egbu na_Uneg 'r_YFB cynorthwywyr_unk cytunedig_Anscadu gadw_Be copïau_Egll ._Atdt 
RHAN_Ebu TRI_Rhifol -_Atdcys CYNLLUN_Egu ARGYFWNG_Egu YSGRIFENEDIG_Anscadu 
O_Arsym 'r_YFB wybodaeth_Ebu uchod_Adf ,_Atdcan mae_Bpres3u angen_Egu cynllun_Egu ysgrifenedig_Anscadu (_Atdchw nad_Rhaperth yw_Bpres3u 'n_Uberf cynnwys_Be manylion_Egll personol_Anscadu am_Arsym anabledd_Egu yr_YFB unigolyn_Egu )_Atdde ._Atdt 
Dylai_Bamherff3u 'r_YFB cynllun_Egu gynnwys_Egu 
•_Gwsym Manylion_Egll yr_YFB adeilad_Egbu a_Cyscyd 'r_YFB lleoliad_Egu 
•_Gwsym Y_YFB cyfarpar_Egu sydd_Bpres3perth ei_Banmedd3gu angen_Egu 
•_Gwsym Y_YFB cymorth_Egu sydd_Bpres3perth ei_Banmedd3gu angen_Egu 
•_Gwsym Enwau_Egll 'r_YFB cynorthwywyr_unk y_Rhadib1u cytunwyd_Bgorffamhers arnynt_Ar3ll 
•_Gwsym Y_YFB weithdrefn_Ebu ddianc_Be ._Atdt 
Dylai_Bamherff3u gael_Be ei_Rhadib3gu lofnodi_Be gan_Arsym yr_YFB unigolyn_Egu (_Atdchw neu_Cyscyd ei_Rhadib3gu ofalwr_Egu un-i-un_unk os_Cyscyd yw_Bpres3u 'n_Utra briodol_Anscadu )_Atdde ,_Atdcan y_YFB rheolwr_Egu llinell_Ebu a_Cyscyd rheolwr_Egu yr_YFB adeilad_Egbu ._Atdt 
Dylid_Egu ei_Rhadib3gu adolygu_Be o_Arsym leiaf_Anseith unwaith_Adf y_YFB flwyddyn_Ebu ,_Atdcan neu_Cyscyd os_Cyscyd oes_Bpres3amhen unrhyw_Anscadu newidiadau_Egll o_Arsym ran_Ebu lleoliadau_Egll /_Gwsym staff_Egll neu_Cyscyd ofalwyr_Egll neu_Cyscyd osodiad_Egu yr_YFB adeilad_Egbu ._Atdt 
Cynllun_Egu Personol_Anscadu Gadael_Be Mewn_Arsym Argyfwng_Egu 
Enw_Egu 
Lleoliadau_Egll 
Di_Uberf Anc_Gwest 
Ystafell_Ebu 321_Gwdig 
Adeilad_Egbu y_YFB Sir_Ebu 
Ystafelloedd_Ebll Cyfarfod_Be ar_Arsym yr_YFB Ail_Rhitrefd Lawr_Egu 
Toiledau_Egll /_Gwsym Ffreutur_Egbu y_YFB Staff_Egll 
Cynorthwywyr_Gwest a_Rhaperth Gytunwyd_Bgorffamhers 
Adam_Ep Afal_Egu 
Ystafell_Ebu 321_Gwdig 
Est_Bgorff2u rhif_ffôn_Anon 
Bernie_Epg Beic_Egu 
(_Atdchw Porthor_Egu )_Atdde 
Ystafell_Ebu 111_Gwdig 
Est_Bgorff2u rhif_ffôn_Anon 
Gillian_Gwest Garej_Egu 
Ystafell_Ebu 322_Gwdig 
Est_Bgorff2u rhif_ffôn_Anon 
David_Gwest Diemwnt_Egu 
(_Atdchw Porthor_Egu )_Atdde 
Ystafell_Ebu 111_Gwdig 
Est_Bgorff2u rhif_ffôn_Anon 
Fiona_Epb Fflam_Ebu 
(_Atdchw Rheolwr_Egu Llinell_Ebu )_Atdde 
Ystafell_Ebu 323_Gwdig 
Est_Bgorff2u rhif_ffôn_Anon 
Clara_Epb Cilgant_Egu 
(_Atdchw Porthor_Egu )_Atdde 
Ystafell_Ebu 111_Gwdig 
Est_Bgorff2u rhif_ffôn_Anon 
Hyfforddiant_Egu sydd_Bpres3perth ei_Rhadib3bu angen_Egu 
Amlder_Egu 
Wedi_Uberf Cwblhau_Be 
Porthorion_Egll -_Atdcys defnydd_Egu y_YFB gadair_Ebu ddianc_Be 
Blynyddol_Anscadu 
•_Gwsym 
Cynorthwywyr_Gwest -_Atdcys gweithdrefnau_Ebll ac_Cyscyd ymarfer_Ebu 
Bob_Egll 6_Gwdig mis_Egu 
Di_Uberf -_Atdcys gweithdrefnau_Ebll trosglwyddo_Be ac_Cyscyd ymarfer_Ebu 
Blynyddol_Anscadu 
•_Gwsym 
Y_YFB cyfarpar_Egu sydd_Bpres3perth ei_Rhadib3bu angen_Egu 
Cadair_Ebu ddianc_Be 
•_Gwsym 
Bwrdd_Egu sêt_Ebu 
•_Gwsym 
Bwrdd_Egu cefn_Egu 
•_Gwsym 
Bwrdd_Egu trosglwyddo_Be 
•_Gwsym 
Blanced_Egbu 
•_Gwsym 
Ffôn_Bpres3ll symudol_Anscadu (_Atdchw Un_Rhaamh Di_Uberf ei_Rhadib3bu hunan_Rhaatb )_Atdde 
•_Gwsym 
Y_YFB Weithdrefn_Ebu Ddianc_Be 
Cam_Egu 1_Gwdig 
Wrth_Arsym glywed_Be y_YFB larwm_Egu ,_Atdcan bydd_Bdyf3u Di_Uberf yn_Arsym ffonio_Be enwg_Anon ,_Atdcan Rheolwr_Egu yr_YFB Adeilad_Egbu ar_Arsym rhif_ffôn_Anon ,_Atdcan neu_Cyscyd _rhif_ffôn__Anon o_Arsym 'r_YFB ffôn_Egu agosaf_Anseith neu_Cyscyd ei_Banmedd3gu ffôn_Bpres3ll symudol_Anscadu (_Atdchw DS_Gwacr rhaid_Egu iddi_Ar3bu storio_Be 'r_YFB rhif_Egu ar_Arsym ei_Rhadib3bu ffôn_Bpres3ll symudol_Anscadu )_Atdde i_Arsym roi_Be gwybod_Egu am_Gwtalf ei_Banmedd3gu lleoliad_Egu ._Atdt 
Bydd_Bdyf3u yn_Arsym aros_Be gyda_Arsym chynorthwyydd_Egu cytunedig_Anscadu ger_Arsym ystafell_Ebu 363_Gwdig ger_Arsym grisiau_Egll 'r_YFB Gogledd_Egu Orllewin_Egu (_Atdchw neu_Cyscyd fel_Cyscyd y_Rhaperth cynghorir_Bpresamhers gan_Arsym yr_YFB Ystafell_Ebu Reoli_Be )_Atdde i_Arsym 'r_YFB porthorion_Egll gyrraedd_Be ._Atdt 
Unwaith_Adf y_Rhadib1u bydd_Bdyf3u y_YFB rhan_Ebu fwyaf_Anseith o_Arsym 'r_YFB staff_Egll wedi_Uberf mynd_Be heibio_Arsym ,_Atdcan bydd_Bdyf3u yn_Arsym aros_Be y_YFB tu_Egu mewn_Arsym i_Arsym 'r_YFB drysau_Egll tân_Egu ar_Arsym ben_Egu y_YFB grisiau_Egll ._Atdt 
Cam_Epb 2_Gwdig 
Os_Cyscyd nad_Rhaperth yw_Bpres3u enwb_Anon wedi_Uberf cysylltu_Be â_Arsym 'r_YFB ystafell_Ebu reoli_Be o_Arsym fewn_Arsym tri_Rhifol munud_Egu ,_Atdcan bydd_Bdyf3u un_Rhaamh o_Arsym 'r_YFB porthorion_Egll yn_Arsym ffonio_Be enwb_Anon ar_Arsym ei_Banmedd3gu ffôn_Bpres3ll symudol_Anscadu i_Arsym gadarnhau_Be nad_Rhaperth yw_Bpres3u hi_Rhapers3bu yn_Arsym yr_YFB adeilad_Egbu ._Atdt 
(_Atdchw DS_Gwacr Rhaid_Egu arddangos_Be rhif_Egu enwb_Anon yn_Arsym glir_Anscadu yn_Arsym yr_YFB Ystafell_Ebu Reoli_Be )_Atdde 
Os_Cyscyd nad_Rhaperth oes_Bpres3amhen ateb_Egu ,_Atdcan bydd_Bdyf3u yr_YFB holl_Bancynnar Borthorion_Egll yn_Arsym cael_Be gwybod_Egu ,_Atdcan a_Cyscyd bydd_Bdyf3u y_YFB rhai_Rhaamh sydd_Bpres3perth wedi_Uberf 'u_Rhadib3ll penodi_Be i_Rhapers1u chwilio_Be pob_Egll llawr_Anscadu a_Cyscyd phob_Banmeint ystafell_Ebu yn_Arsym cael_Be gwybod_Egu am_Arsym y_YFB sefyllfa_Ebu ._Atdt 
Cam_Epb 3_Gwdig 
Dau_Rhifol o_Arsym 'r_YFB cynorthwywyr_unk neu_Cyscyd borthorion_Egll cytunedig_Anscadu i_Arsym fynd_Be at_Arsym Di_Rhapers2u ,_Atdcan gan_Arsym godi_Bpres2u 'r_YFB gadair_Ebu ddianc_Be ar_Arsym y_YFB ffordd_Ebu ._Atdt 
Cam_Epb 4_Gwdig 
Bydd_Bdyf3u y_YFB porthorion_Egll yn_Arsym trosglwyddo_Be Di_Uberf i_Arsym 'r_YFB gadair_Ebu ddianc_Be gan_Arsym ddefnyddio_Be 'r_YFB bwrdd_Egu trosglwyddo_Be ,_Atdcan ac_Cyscyd yn_Arsym ei_Rhadib3bu gorchuddio_Be â_Arsym 'r_YFB blanced_Egbu ._Atdt 
Bydd_Bdyf3u y_YFB cynorthwyydd_Egu cytunedig_Anscadu o_Arsym 'r_YFB swyddfa_Ebu yn_Arsym mynd_Be â_Arsym 'r_YFB gadair_Ebu olwyn_Ebu a_Arsym 'r_YFB bwrdd_Egu trosglwyddo_Be ac_Cyscyd yn_Arsym aros_Be wrth_Arsym y_YFB man_Egbu ymadael_Be ._Atdt 
Cam_Epb 5_Gwdig 
Bydd_Bdyf3u y_YFB porthorion_Egll yn_Arsym dod_Be â_Arsym Di_Uberf i_Arsym lawr_Egu y_YFB grisiau_Egll ,_Atdcan ac_Cyscyd erbyn_Arsym hynny_Rhadangd dylai_Bamherff3u 'r_YFB grisiau_Egll fod_Be yn_Arsym dawel_Anscadu ._Atdt 
Cam_Epb 6_Gwdig 
Unwaith_Adf y_Rhadib1u byddant_Bdyf3ll ar_Arsym y_YFB llawr_Egu gwaelod_Egu ,_Atdcan bydd_Bdyf3u Di_Uberf yn_Arsym cael_Be ei_Rhadib3bu throsglwyddo_Be 'n_Utra ôl_Anscadu i'w_Arsym chadair_Ebu olwyn_Ebu ,_Atdcan ac_Cyscyd os_Cyscyd yw_Bpres3u 'n_Utra briodol_Anscadu ,_Atdcan caiff_Bpres3u ei_Rhadib3bu gorchuddio_Be â_Arsym 'r_YFB blanced_Egbu ._Atdt 
Bydd_Bdyf3u un_Rhaamh o_Arsym 'r_YFB Porthorion_Egll yn_Arsym gyfrifol_Anscadu am_Arsym ddychwelyd_Be y_YFB gadair_Ebu ddianc_Be a_Cyscyd 'r_YFB bwrdd_Egu trosglwyddo_Be pan_Cyscyd gaiff_Bpres3u y_YFB sefyllfa_Ebu ei_Rhadib3bu datrys_Be ._Atdt 
Cam_Epb 7_Gwdig 
Bydd_Bdyf3u y_YFB porthor_Egu arall_Anscadu yn_Arsym sicrhau_Be bod_Be Di_Uberf yn_Arsym mynd_Be i_Arsym le_Egu diogel_Anscadu ;_Atdcan naill_Anscadu ai_Cyscyd 'r_YFB pwynt_Egu ymgynnull_Be ,_Atdcan neu_Cyscyd os_Cyscyd yw_Bpres3u 'n_Utra briodol_Anscadu i_Arsym fan_Egbu cysgod_Egu o_Arsym ryw_Egbu fath_Banhwyr ._Atdt 
Cam_Epb 8_Gwdig 
Dylai_Bamherff3u 'r_YFB cynorthwyydd_Egu cytunedig_Anscadu roi_Be gwybod_Egu i_Arsym 'r_YFB warden_Gwest ym_Arsym Man_Egbu Ymgynnull_Be X_Gwllyth bod_Be Di_Uberf wedi_Uberf cael_Be ei_Rhadib3gu symud_Be yn_Arsym ddiogel_Anscadu o_Arsym 'r_YFB adeilad_Egbu a_Cyscyd beth_Rhagof yw_Bpres3u ei_Banmedd3gu lleoliad_Egu presennol_Anscadu os_Cyscyd yw_Bpres3u hynny_Rhadangd 'n_Utra briodol_Anscadu ._Atdt 
Llofnod_Egu 
Dyddiad_Egu 
Di_Uberf Anc_Gwest 
30_Gwdig Gorffennaf_Egu 2007_Gwdig 
F_Gwllyth Fflam_Ebu 
30_Gwdig Gorffennaf_Egu 2007_Gwdig 
Rheolwr_Egu yr_YFB Adeilad_Egbu 
30_Gwdig Gorffennaf_Egu 2007_Gwdig 
DS_Gwacr Bydd_Bdyf3u angen_Egu CPGA_Gwacr ychwanegol_Anscadu ar_Arsym gyfer_Egu Neuadd_Ebu y_YFB Ddinas_Ebu a_Cyscyd Thŷ_Egu Willcox_Ep 
HYFFORDDIANT_Egu A_Arsym WNAED_Bgorffamhers 
Enw_Egu 
Dyddiad_Egu 
Llofnod_Egu 
Dyddiad_Egu 
Llofnod_Egu 
Adam_Epg Afal_Egu 
15_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
A_Cyscyd Afal_Egu 
Bernie_Epg Beic_Egu 
15_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
B_Gwllyth Beic_Egu 
Clara_Epb Cilgant_Egu 
22_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
C_Gwllyth Cilgant_Egu 
David_Gwest Diemwnt_Egu 
15_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
D_Gwllyth Diemwnt_Egu 
Fiona_Epb Fflam_Ebu 
Gillian_Gwest Garej_Egu 
22_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
G_Gwllyth Garej_Egu 
Dianc_Be o_Arsym 'r_YFB Adeilad_Egbu ac_Cyscyd Ymarferion_Ebll Tân_Egu 
Dyddiad_Egu 
Sylwadau_Egll 
30_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
Aeth_Egu y_YFB broses_Ebu ddianc_Be yn_Arsym dda_Anscadu ,_Atdcan ond_Arsym cymerodd_Bgorff3u bedair_Rhifol munud_Egu cyn_Arsym i_Arsym 'r_YFB cynorthwywyr_unk gyrraedd_Be (_Atdchw cyfnod_Egu y_YFB gwyliau_Ebll )_Atdde a_Cyscyd phedair_Rhifol munud_Egu arall_Anscadu i_Arsym wneud_Be y_YFB trosglwyddo_Be i_Arsym 'r_YFB gadair_Ebu ddianc_Be ._Atdt 
Bydd_Bdyf3u hyn_Rhadangd yn_Arsym gwella_Be gydag_Arsym ymarfer_Ebu ._Atdt 
Cynllun_Egu Personol_Anscadu Gadael_Be Mewn_Arsym Argyfwng_Egu 
Enw_Egu 
Lleoliadau_Egll 
Di_Uberf Anc_Gwest 
Ystafell_Ebu 321_Gwdig 
Adeilad_Egbu y_YFB Sir_Ebu 
Ystafelloedd_Ebll Cyfarfod_Be ar_Arsym yr_YFB Ail_Rhitrefd Lawr_Egu 
Toiledau_Egll /_Gwsym Ffreutur_Egbu y_YFB Staff_Egll 
Cynorthwywyr_Gwest a_Rhaperth Gytunwyd_Bgorffamhers 
Adam_Ep Afal_Egu 
Ystafell_Ebu 321_Gwdig 
Est_Bgorff2u 7654_Gwdig 
Bernie_Epg Beic_Egu 
(_Atdchw Porthor_Egu )_Atdde 
Ystafell_Ebu 111_Gwdig 
Est_Bgorff2u 2222_Gwdig 
Gillian_Gwest Garej_Egu 
Ystafell_Ebu 322_Gwdig 
Est_Bgorff2u 7678_Gwdig 
David_Gwest Diemwnt_Egu 
(_Atdchw Porthor_Egu )_Atdde 
Ystafell_Ebu 111_Gwdig 
Est_Bgorff2u 2222_Gwdig 
Fiona_Epb Fflam_Ebu 
(_Atdchw Rheolwr_Egu Llinell_Ebu )_Atdde 
Ystafell_Ebu 323_Gwdig 
Est_Bgorff2u 7777_Gwdig 
Clara_Epb Cilgant_Egu 
(_Atdchw Porthor_Egu )_Atdde 
Ystafell_Ebu 111_Gwdig 
Est_Bgorff2u 2222_Gwdig 
Hyfforddiant_Egu sydd_Bpres3perth ei_Rhadib3bu angen_Egu 
Amlder_Egu 
Wedi_Uberf Cwblhau_Be 
Porthorion_Egll -_Atdcys defnydd_Egu y_YFB gadair_Ebu ddianc_Be 
Blynyddol_Anscadu 
•_Gwsym 
Cynorthwywyr_Gwest -_Atdcys gweithdrefnau_Ebll ac_Cyscyd ymarfer_Ebu 
Bob_Egll 6_Gwdig mis_Egu 
Di_Uberf -_Atdcys gweithdrefnau_Ebll trosglwyddo_Be ac_Cyscyd ymarfer_Ebu 
Blynyddol_Anscadu 
•_Gwsym 
Y_YFB cyfarpar_Egu sydd_Bpres3perth ei_Rhadib3bu angen_Egu 
Cadair_Ebu ddianc_Be 
•_Gwsym 
Bwrdd_Egu sêt_Ebu 
•_Gwsym 
Bwrdd_Egu cefn_Egu 
•_Gwsym 
Bwrdd_Egu trosglwyddo_Be 
•_Gwsym 
Blanced_Egbu 
•_Gwsym 
Ffôn_Bpres3ll symudol_Anscadu (_Atdchw Un_Rhaamh Di_Uberf ei_Rhadib3bu hunan_Rhaatb )_Atdde 
•_Gwsym 
Y_YFB Weithdrefn_Ebu Ddianc_Be 
Cam_Egu 1_Gwdig 
Wrth_Arsym glywed_Be y_YFB larwm_Egu ,_Atdcan bydd_Bdyf3u Di_Uberf yn_Arsym ffonio_Be Bob_Egll ,_Atdcan Rheolwr_Egu yr_YFB Adeilad_Egbu ar_Arsym 029_Gwdig 2087_Gwdig 2222_Gwdig ,_Atdcan neu_Cyscyd 2223_Gwdig o_Arsym 'r_YFB ffôn_Egu agosaf_Anseith neu_Cyscyd ei_Banmedd3gu ffôn_Bpres3ll symudol_Anscadu (_Atdchw DS_Gwacr rhaid_Egu iddi_Ar3bu storio_Be 'r_YFB rhif_Egu ar_Arsym ei_Rhadib3bu ffôn_Bpres3ll symudol_Anscadu )_Atdde i_Arsym roi_Be gwybod_Egu am_Gwtalf ei_Banmedd3gu lleoliad_Egu ._Atdt 
Bydd_Bdyf3u yn_Arsym aros_Be gyda_Arsym chynorthwyydd_Egu cytunedig_Anscadu ger_Arsym ystafell_Ebu 363_Gwdig ger_Arsym grisiau_Egll 'r_YFB Gogledd_Egu Orllewin_Egu (_Atdchw neu_Cyscyd fel_Cyscyd y_Rhaperth cynghorir_Bpresamhers gan_Arsym yr_YFB Ystafell_Ebu Reoli_Be )_Atdde i_Arsym 'r_YFB porthorion_Egll gyrraedd_Be ._Atdt 
Unwaith_Adf y_Rhadib1u bydd_Bdyf3u y_YFB rhan_Ebu fwyaf_Anseith o_Arsym 'r_YFB staff_Egll wedi_Uberf mynd_Be heibio_Arsym ,_Atdcan bydd_Bdyf3u yn_Arsym aros_Be y_YFB tu_Egu mewn_Arsym i_Arsym 'r_YFB drysau_Egll tân_Egu ar_Arsym ben_Egu y_YFB grisiau_Egll ._Atdt 
Cam_Epb 2_Gwdig 
Os_Cyscyd nad_Rhaperth yw_Bpres3u Di_Uberf wedi_Uberf cysylltu_Be â_Arsym 'r_YFB ystafell_Ebu reoli_Be o_Arsym fewn_Arsym tri_Rhifol munud_Egu ,_Atdcan bydd_Bdyf3u un_Rhaamh o_Arsym 'r_YFB porthorion_Egll yn_Arsym ffonio_Be Di_Uberf ar_Arsym ei_Banmedd3gu ffôn_Bpres3ll symudol_Anscadu i_Arsym gadarnhau_Be nad_Rhaperth yw_Bpres3u hi_Rhapers3bu yn_Arsym yr_YFB adeilad_Egbu ._Atdt 
(_Atdchw DS_Gwacr Rhaid_Egu arddangos_Be rhif_Egu Di_Uberf yn_Arsym glir_Anscadu yn_Arsym yr_YFB Ystafell_Ebu Reoli_Be )_Atdde 
Os_Cyscyd nad_Rhaperth oes_Bpres3amhen ateb_Egu ,_Atdcan bydd_Bdyf3u yr_YFB holl_Bancynnar Borthorion_Egll yn_Arsym cael_Be gwybod_Egu ,_Atdcan a_Cyscyd bydd_Bdyf3u y_YFB rhai_Rhaamh sydd_Bpres3perth wedi_Uberf 'u_Rhadib3ll penodi_Be i_Rhapers1u chwilio_Be pob_Egll llawr_Anscadu a_Cyscyd phob_Banmeint ystafell_Ebu yn_Arsym cael_Be gwybod_Egu am_Arsym y_YFB sefyllfa_Ebu ._Atdt 
Cam_Epb 3_Gwdig 
Dau_Rhifol o_Arsym 'r_YFB cynorthwywyr_unk neu_Cyscyd borthorion_Egll cytunedig_Anscadu i_Arsym fynd_Be at_Arsym Di_Rhapers2u ,_Atdcan gan_Arsym godi_Bpres2u 'r_YFB gadair_Ebu ddianc_Be ar_Arsym y_YFB ffordd_Ebu ._Atdt 
Cam_Epb 4_Gwdig 
Bydd_Bdyf3u y_YFB porthorion_Egll yn_Arsym trosglwyddo_Be Di_Uberf i_Arsym 'r_YFB gadair_Ebu ddianc_Be gan_Arsym ddefnyddio_Be 'r_YFB bwrdd_Egu trosglwyddo_Be ,_Atdcan ac_Cyscyd yn_Arsym ei_Rhadib3bu gorchuddio_Be â_Arsym 'r_YFB blanced_Egbu ._Atdt 
Bydd_Bdyf3u y_YFB cynorthwyydd_Egu cytunedig_Anscadu o_Arsym 'r_YFB swyddfa_Ebu yn_Arsym mynd_Be â_Arsym 'r_YFB gadair_Ebu olwyn_Ebu a_Arsym 'r_YFB bwrdd_Egu trosglwyddo_Be ac_Cyscyd yn_Arsym aros_Be wrth_Arsym y_YFB man_Egbu ymadael_Be ._Atdt 
Cam_Epb 5_Gwdig 
Bydd_Bdyf3u y_YFB porthorion_Egll yn_Arsym dod_Be â_Arsym Di_Uberf i_Arsym lawr_Egu y_YFB grisiau_Egll ,_Atdcan ac_Cyscyd erbyn_Arsym hynny_Rhadangd dylai_Bamherff3u 'r_YFB grisiau_Egll fod_Be yn_Arsym dawel_Anscadu ._Atdt 
Cam_Epb 6_Gwdig 
Unwaith_Adf y_Rhadib1u byddant_Bdyf3ll ar_Arsym y_YFB llawr_Egu gwaelod_Egu ,_Atdcan bydd_Bdyf3u Di_Uberf yn_Arsym cael_Be ei_Rhadib3bu throsglwyddo_Be 'n_Utra ôl_Anscadu i'w_Arsym chadair_Ebu olwyn_Ebu ,_Atdcan ac_Cyscyd os_Cyscyd yw_Bpres3u 'n_Utra briodol_Anscadu ,_Atdcan caiff_Bpres3u ei_Rhadib3bu gorchuddio_Be â_Arsym 'r_YFB blanced_Egbu ._Atdt 
Bydd_Bdyf3u un_Rhaamh o_Arsym 'r_YFB Porthorion_Egll yn_Arsym gyfrifol_Anscadu am_Arsym ddychwelyd_Be y_YFB gadair_Ebu ddianc_Be a_Cyscyd 'r_YFB bwrdd_Egu trosglwyddo_Be pan_Cyscyd gaiff_Bpres3u y_YFB sefyllfa_Ebu ei_Rhadib3bu datrys_Be ._Atdt 
Cam_Epb 7_Gwdig 
Bydd_Bdyf3u y_YFB porthor_Egu arall_Anscadu yn_Arsym sicrhau_Be bod_Be Di_Uberf yn_Arsym mynd_Be i_Arsym le_Egu diogel_Anscadu ;_Atdcan naill_Anscadu ai_Cyscyd 'r_YFB pwynt_Egu ymgynnull_Be ,_Atdcan neu_Cyscyd os_Cyscyd yw_Bpres3u 'n_Utra briodol_Anscadu i_Arsym fan_Egbu cysgod_Egu o_Arsym ryw_Egbu fath_Banhwyr ._Atdt 
Cam_Epb 8_Gwdig 
Dylai_Bamherff3u 'r_YFB cynorthwyydd_Egu cytunedig_Anscadu roi_Be gwybod_Egu i_Arsym 'r_YFB warden_Gwest ym_Arsym Man_Egbu Ymgynnull_Be X_Gwllyth bod_Be Di_Uberf wedi_Uberf cael_Be ei_Rhadib3gu symud_Be yn_Arsym ddiogel_Anscadu o_Arsym 'r_YFB adeilad_Egbu a_Cyscyd beth_Rhagof yw_Bpres3u ei_Banmedd3gu lleoliad_Egu presennol_Anscadu os_Cyscyd yw_Bpres3u hynny_Rhadangd 'n_Utra briodol_Anscadu ._Atdt 
Llofnod_Egu 
Dyddiad_Egu 
Di_Uberf Anc_Gwest 
30_Gwdig Gorffennaf_Egu 2007_Gwdig 
F_Gwllyth Fflam_Ebu 
30_Gwdig Gorffennaf_Egu 2007_Gwdig 
Rheolwr_Egu yr_YFB Adeilad_Egbu 
30_Gwdig Gorffennaf_Egu 2007_Gwdig 
DS_Gwacr Bydd_Bdyf3u angen_Egu CPGA_Gwacr ychwanegol_Anscadu ar_Arsym gyfer_Egu Neuadd_Ebu y_YFB Ddinas_Ebu a_Cyscyd Thŷ_Egu Willcox_Ep 
HYFFORDDIANT_Egu A_Arsym WNAED_Bgorffamhers 
Enw_Egu 
Dyddiad_Egu 
Llofnod_Egu 
Dyddiad_Egu 
Llofnod_Egu 
Adam_Epg Afal_Egu 
15_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
A_Cyscyd Afal_Egu 
Bernie_Epg Beic_Egu 
15_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
B_Gwllyth Beic_Egu 
Clara_Epb Cilgant_Egu 
22_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
C_Gwllyth Cilgant_Egu 
David_Gwest Diemwnt_Egu 
15_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
D_Gwllyth Diemwnt_Egu 
Fiona_Epb Fflam_Ebu 
Gillian_Gwest Garej_Egu 
22_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
G_Gwllyth Garej_Egu 
Dianc_Be o_Arsym 'r_YFB Adeilad_Egbu ac_Cyscyd Ymarferion_Ebll Tân_Egu 
Dyddiad_Egu 
Sylwadau_Egll 
30_Gwdig /_Gwsym 8_Gwdig /_Gwsym 07_Gwdig 
Aeth_Egu y_YFB broses_Ebu ddianc_Be yn_Arsym dda_Anscadu ,_Atdcan ond_Arsym cymerodd_Bgorff3u bedair_Rhifol munud_Egu cyn_Arsym i_Arsym 'r_YFB cynorthwywyr_unk gyrraedd_Be (_Atdchw cyfnod_Egu y_YFB gwyliau_Ebll )_Atdde a_Cyscyd phedair_Rhifol munud_Egu arall_Anscadu i_Arsym wneud_Be y_YFB trosglwyddo_Be i_Arsym 'r_YFB gadair_Ebu ddianc_Be ._Atdt 
Bydd_Bdyf3u hyn_Rhadangd yn_Arsym gwella_Be gydag_Arsym ymarfer_Ebu ._Atdt 