[_Atdchw Cyfarwyddyd_Egu i_Arsym beidio_Be trawsgrifio_Be ]_Atdde ._Atdt 
S1_Anon Dwy_Rhifol flynedd_Ebll yn_Arsym ôl_Egu mi_Uberf ddes_Bgorff1u i_Rhapers1u yma_Adf i_Rhapers1u Dieppe_Gwest yng_Arsym ngogledd_Egu Ffrainc_Epb ar_Arsym drywydd_Egu yn_Arsym o_Rhapers3gu artistie_unk 'd_Gwllyth pwysica_Anscadu '_Atddyf Cymru_Epb enwb_Anon cyfenw_Anon ._Atdt 
Mae_Bpres3u ei_Rhadib3bu llunie_Egll '_Atddyf yn_Arsym rai_Rhaamh o_Arsym oriele_Ebll celf_Ebu pwysica_Anscadu 'r_YFB byd_Egu ._Atdt 
Ei_Rhadib3bu charwrie_unk 'th_Rhadib2u gyda_Arsym 'r_YFB cerflunydd_Egu enwg_Anon cyfenw_Anon wedi_Arsym ca'l_Ebu sylw_Egu mawr_Anscadu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde A_Cyscyd llu_Egu o_Arsym awduron_Egll wedi_Arsym '_Atddyf sgwennu_Be am_Arsym fywyd_Egu cymhleth_Anscadu enwb_Anon ._Atdt 
Ond_Arsym ma_Bpres3u '_Atddyf dyddia_Bgorch2u olaf_Anscadu ei_Rhadib3bu hoes_Bpres3amhen ac_Cyscyd union_Anscadu fan_Ebu ei_Rhadib3bu chladdu_Be wedi_Uberf bod_Be yn_Arsym ddirgelwch_Egu i_Arsym bawb_Rhaamh ._Atdt 
S2_Anon We_Gwest have_Gwest thought_Gwest abot_Gwest it_Gwest a_Gwest lot_Gwest over_Gwest the_Gwest years_Gwest and_Gwest there_Gwest have_Gwest been_Gwest various_Gwest atempts_Gwest to_Gwest find_Gwest her_Gwest without_Gwest success_Gwest ._Atdt 
S1_Anon Wrth_Arsym i_Arsym ni_Rhapers1ll fynd_Be i_Rhapers1u chwilio_Be 'n_Utra ddyfnach_Anscym i_Arsym 'r_YFB cyfnod_Egu yma_Adf a_Cyscyd dod_Be i_Rhapers1u Dieppe_Gwest ei_Rhadib3bu hun_Rhaatb mi_Uberf wnaethon_Bgorff3ll ni_Rhapers1ll lwyddo_Be i_Arsym ddarganfod_Be ble_Egu ma_Bpres3u '_Atddyf gorffwysfa_Egbu olaf_Anscadu enwb_Anon cyfenw_Anon ._Atdt 
A_Cyscyd pham_Adf '_Atddyf d_Gwllyth oes_Bpres3amhen 'na_Adf ddim_Egu carreg_Ebu fedd_Egu ._Atdt 
S2_Anon So_Adf she_Gwest was_Egu lost_Gwest in_Gwest Dieppe_Gwest and_Gwest now_Gwest eighty_Gwest five_Gwest or_Gwest eighty_Gwest six_Gwest years_Gwest later_Gwest here_Gwest we_Ebu are_Egbll ._Atdt 
Its_Gwest [-]_Gwann ._Atdt 
Its_Gwest an_Ebu extraordinary_Gwest moment_Egu ._Atdt 
S3_Anon Mae_Bpres3u e_Rhapers3gu 'n_Utra rywbeth_Rhaamh go_Adf emosiynol_Anscadu i_Arsym fi_Rhapers1u ._Atdt 
Meddwl_Egu amdani_Ar3bu o_Arsym 'r_YFB diwedd_Egu wedi_Arsym ffendio_unk 'r_YFB tawelwch_Egu yma_Adf yn_Arsym fan_Ebu hyn_Rhadangd yn_Arsym Dieppe_Ep ._Atdt 
S1_Anon Dros_Arsym saith_Egu deg_Rhifol mlynedd_Ebll ar_Arsym ôl_Anscadu ei_Rhadib3bu marwol'eth_unk heddiw_Adf allwn_Bpres1ll ni_Rhapers1ll goffau_Be enwb_Anon cyfenw_Anon â_Arsym chofeb_Ebu arbennig_Anscadu ger_Arsym man_Anscadu ei_Rhadib3bu chladdu_Be a_Cyscyd dod_Be o_Arsym 'r_YFB diwedd_Egu at_Arsym y_YFB daith_Ebu olaf_Anscadu ._Atdt 
[_Atdchw Cerddoriaeth_Ebu ]_Atdde ._Atdt 
S1_Anon Ar_Arsym ddechre_Egu '_Atddyf mis_Egu Medi_Egu un_Rhaamh naw_Rhifol tri_Rhifol naw_Rhifol mi_Uberf fyddai_Bamod3u 'r_YFB platfform_Egu yma_Adf yng_Arsym ngogledd_Egu Ffrainc_Epb wedi_Uberf bod_Be yn_Arsym ferw_Egu o_Arsym banic_Egu dechre_Be '_Atddyf rhyfel_Egbu ._Atdt 
Ac_Cyscyd yng_Arsym nghanol_Egu y_YFB cyffro_Egu roedd_Bamherff3u 'na_Adf un_Bancynnar Gymraes_Ebu eiddil_Anscadu ganol_Egu oed_Egu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Artist_Egu a_Rhaperth ddaeth_Bgorff3u yn_Arsym fyd_Egu enwog_Anscadu ._Atdt 
Ond_Arsym y_YFB diwrnod_Egu hwnnw_Rhadangg yn_Arsym Dieppe_Ep '_Atddyf d_Gwllyth oedd_Bamherff3u neb_Rhaamh yn_Arsym gw'bod_Be pwy_Egu oedd_Bamherff3u enwb_Anon cyfenw_Anon ._Atdt 
'_Atddyf D_Gwllyth oedd_Bamherff3u ganddi_Ar3bu neb_Rhaamh yn_Arsym gwmni_Egu ._Atdt 
Ro'dd_Ebu 'i_Rhadib3bu ar_Arsym 'i_Rhadib3bu phen_Egu 'i_Rhadib3bu hun_Rhaatb ._Atdt 
[_Atdchw Cerddoriaeth_Ebu ]_Atdde ._Atdt 
S1_Anon '_Atddyf D_Gwllyth oes_Bpres3amhen neb_Rhaamh yn_Arsym gw'bod_Be yn_Arsym sicr_Anscadu i_Arsym ble_Egbu roedd_Bamherff3u enwb_Anon yn_Arsym mynd_Be y_YFB diwrnod_Egu hwnnw_Rhadangg ._Atdt 
Ffoi_Bamherff3u o_Rhapers3gu Baris_Gwest achos_Egu y_YFB rhyfel_Egbu efalle_Adf '_Atddyf ._Atdt 
Ond_Arsym yn_Arsym sicr_Anscadu 'na_Adf 'th_Banmedd2u hi_Rhapers3bu ddim_Egu cyrr'edd_unk pen_Egu y_YFB daith_Ebu ._Atdt 
Fe_Uberf thrawyd_Bgorffamhers hi_Rhapers3bu 'n_Utra wael_Anscadu yn_Arsym y_YFB stesion_Ebu a_Cyscyd 'i_Rhadib3bu chymryd_Be i_Rhapers1u '_Atddyf sbyty_Egbu mewn_Arsym lleiandy_Egu gerllaw_Arsym lle_Egu bu_Bgorff3u hi_Rhapers3bu farw_Anscadu rhai_Banmeint dyddie_Egll 'n_Utra ddiweddarach_Anscym ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Daeth_Bgorff3u dim_Banmeint teulu_Egu na_Bandangd ffrindiau_Egll i_Arsym 'r_YFB angladd_Egbu a_Arsym mae_Bpres3u 'na_Adf beth_Rhagof dirgelwch_Egu o_Rhapers3gu hyd_Arsym ynglŷn_Arsym â_Arsym 'r_YFB dyddia_Bgorch2u ola_Anscadu '_Atddyf 'na_Adf [saib]_Gwann ._Atdt 
Merch_Ebu o_Arsym Sir_Ebu Benfro_Epb oedd_Bamherff3u enwb_Anon cyfenw_Anon ._Atdt 
Ar_Arsym ôl_Anscadu marwol'eth_unk sydyn_Anscadu 'i_Rhadib3bu mam_Ebu pan_Egu oedd_Bamherff3u enwb_Anon yn_Arsym ddim_Egu ond_Arsym wyth_Rhifol oed_Egu fe_Uberf symudodd_Bgorff3u y_YFB teulu_Egu o_Arsym Hwlffordd_Ep i_Rhapers1u Ddinbych-y-Pysgod_Gwest ._Atdt 
'_Atddyf D_Gwllyth oedd_Bamherff3u hi_Rhapers3bu ddim_Adf yn_Arsym blentyndod_Egu hawdd_Anscadu ._Atdt 
S3_Anon Ma_Bpres3u 'r_YFB llythyron_Egll dderbyniodd_Bgorff3u hi_Rhapers3bu oddi_Arsym wrth_Arsym aelode_Egll yyy'ill_unk y_YFB teulu_Egu ac_Cyscyd ma_Bpres3u '_Atddyf llythyron_Egll o'dd_Bamherff3u hi_Rhapers3bu 'n_Arsym '_Atddyf sgwennu_Be at_Arsym ei_Rhadib3bu ffrind_Egu [aneglur]_Gwann ._Atdt 
[_Atdchw Cyfarwyddyd_Egu i_Arsym beidio_Be trawsgrifio_Be ]_Atdde ._Atdt 
S1_Anon A_Cyscyd pa_Bangof mor_Adf anodd_Anscadu oedd_Bamherff3u hi_Rhapers3bu i_Arsym ferch_Ebu i_Arsym fod_Be yn_Arsym artist_Egu proffesiynol_Anscadu ?_Atdt 
S3_Anon O'dd_Bamherff3u e_Rhapers3gu 'n_Utra ddim_Egu yn_Arsym normal_Anscadu i_Arsym fod_Be yn_Arsym artist_Egu proffesiynol_Anscadu ar_Arsym y_YFB pryd_Egu i_Arsym ferched_Ebll ._Atdt 
O'n_Bamherff1u nhw_Rhapers3ll 'n_Uberf priodi_Be neu_Cyscyd ymm_Ebych yn_Arsym mynd_Be i_Rhapers1u dysgu_Be ._Atdt 
O'dd_Bamherff3u e_Rhapers3gu 'n_Utra rhaid_Egu iddi_Ar3bu fod_Be yn_Arsym '_Atddyf styfnig_Anscadu iawn_Adf ymm_Ebych i_Rhapers1u penderfynu_Be i_Rhapers1u bod_Be yn_Arsym artist_Egu proffesiynol_Anscadu ._Atdt 
S1_Anon Ar_Arsym ôl_Anscadu gorffen_Be yn_Arsym y_YFB Slade_Ep fe_Uberf ddaeth_Bgorff3u enwb_Anon yma_Adf i_Rhapers1u Baris_Gwest i_Arsym astudio_Be gyda_Arsym Whistler_Ep yn_Arsym yr_YFB academi_Egu Carmen_Gwest a_Cyscyd byw_Be ymm_Ebych [aneglur]_Gwann ._Atdt 
S4_Anon '_Atddyf D_Gwllyth o'dd_Bamherff3u 'i_Rhadib3gu ddim_Adf yn_Arsym arferol_Anscadu i_Arsym ferch_Ebu o_Arsym dref_Ebu fach_Anscadu fel_Cyscyd Dinbych-y-Pysgod_Gwest i_Rhapers1u diengid_unk i_Rhapers1u Paris_Gwest a_Cyscyd byw_Be ar_Arsym ei_Rhadib3bu phen_Egu ei_Rhadib3bu hun_Rhaatb yn_Arsym y_YFB cyfnod_Egu yna_Adf ._Atdt 
Ond_Arsym mae_Bpres3u 'n_Utra rhaid_Egu rhoi_Be hyn_Rhadangd yn_Arsym erbyn_Be y_YFB cyd-destun_Egu ehangach_Anscym sef_Cyscyd bod_Be Paris_Gwest yn_Arsym hysbys_Anscadu iawn_Adf i_Arsym ferched_Ebll y_YFB cyfnod_Egu fel_Cyscyd lle_Rhagof oedd_Bamherff3u merched_Ebll yn_Arsym gallu_Egu byw_Anscadu yn_Arsym annibynnol_Anscadu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ac_Cyscyd o'ddan_unk nhw_Rhapers3ll 'n_Uberf gallu_Be dilyn_Be 'u_Banmedd3ll gyrfa_Ebu fel_Cyscyd artist_Egu neu_Cyscyd lenor_Egu a_Cyscyd '_Atddyf dan_Bpres1ll ni_Rhapers1ll 'n_Uberf gweld_Be merched_Ebll yn_Arsym heidio_Be i_Rhapers1u Baris_Gwest i_Arsym fyw_Be yn_Arsym rhydd_Anscadu ._Atdt 
S1_Anon Mae_Bpres3u hunan-_Ublaen bortread_Egu o_Rhapers3gu enwb_Anon o_Arsym 'r_YFB cyfnod_Egu yma_Adf yn_Arsym cynnig_Egu cipolwg_Egu o_Arsym 'i_Rhadib3bu phersonoliaeth_Ebu a_Cyscyd 'i_Rhadib3bu hawydd_Egu i_Arsym gael_Be ei_Rhadib3bu chymryd_Be o_Arsym ddifri_Anscadu '_Atddyf fel_Cyscyd artist_Egu ._Atdt 
S3_Anon '_Atddyf Dan_Bpres1ll ni_Rhapers1ll 'n_Utra ca'l_Ebu syniad_Egu o_Arsym gymeriad_Egu eitha_Adf '_Atddyf difrifol_Anscadu ymm_Ebych eitha_Adf '_Atddyf dwys_Anscadu ._Atdt 
Ma_Bpres3u '_Atddyf 'na_Bandangd ffocws_Egu cryf_Anscadu iawn_Adf ar_Arsym y_YFB wynab_unk ._Atdt 
Ma_Bpres3u 'r_YFB cefndir_Egu yn_Arsym blaen_Egu a_Cyscyd mae_Bpres3u 'n_Rhadib1ll [-]_Gwann ._Atdt 
Ma_Bpres3u '_Atddyf 'na_Bandangd oleuni_Egu cryf_Anscadu iawn_Adf yn_Arsym goleuo_Be 'r_YFB gwynab_unk ._Atdt 
Ond_Arsym mae_Bpres3u 'n_Utra bwysig_Anscadu hefyd_Adf edrych_Egu ar_Arsym y_YFB ffordd_Ebu mae_Bpres3u 'di_Uberf gwisgo_Be ._Atdt 
Y_YFB ffordd_Ebu mae_Bpres3u 'di_Uberf cyflwyno_Be 'i_Rhadib3bu hun_Rhaatb ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Mae_Bpres3u 'n_Uberf gwisgo_Be dillad_Egll o'dd_Bamherff3u yn_Arsym ôl_Egu pob_Anscadu tebyg_Anscadu yn_Arsym ffasiynol_Anscadu sawl_Banmeint degawd_Egu yn_Arsym gynt_Anscadu felly_Adf '_Atddyf falle_Adf '_Atddyf bo_Adf '_Atddyf 'na_Bandangd elfen_Ebu ychydig_Rhaamh yn_Arsym ecsentrig_Anscadu yn_Arsym artistig_Anscadu ac_Cyscyd trw_Arsym '_Atddyf ddangos_Be ei_Rhadib3bu hymwybyddiaeth_Ebu o_Arsym hyn_Rhadangd yn_Arsym dangos_Be ei_Rhadib3bu hun_Rhaatb fel_Cyscyd rhywun_Rhaamh sy_Bpres3perth 'n_Uberf meddwl_Be ._Atdt 
Fel_Cyscyd rhywun_Rhaamh sydd_Bpres3perth yn_Arsym addysgiedig_Anscadu ._Atdt 
Y_YFB new_Gwest woman_Gwest neu_Cyscyd 'r_YFB thinking_Gwest woman_Gwest ._Atdt 
S1_Anon Yma_Adf ym_Arsym Mharis_Ep wnaeth_Bgorff3u enwb_Anon gyfarfod_Be â_Arsym 'r_YFB mawrion_Anscadll ._Atdt 
Picasso_Gwest a_Cyscyd Matisse_Gwest ._Atdt 
Roedd_Bamherff3u Paris_Gwest yn_Arsym le_Egu mor_Adf gyffrous_Anscadu i_Arsym arlunydd_Egu ifanc_Anscadu ._Atdt 
Ond_Arsym roedd_Bamherff3u hi_Rhapers3bu ar_Arsym fin_Egu cwrdd_Egu â_Arsym un_Rhaamh o_Arsym artistiaid_Egll enwoca_Anscadu '_Atddyf Ewrop_Ep ._Atdt 
Mi_Uberf fydda_Bdyf1u '_Atddyf hynny_Rhadangd 'n_Uberf newid_Be ei_Rhadib3bu bywyd_Egu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde yyy_Ebych mwyn_Egu cynnal_Be ei_Rhadib3gu hun_Rhaatb yn_Arsym ariannol_Anscadu roedd_Bamherff3u rhaid_Egu i_Rhapers1u enwb_Anon fodelu_Be hefyd_Adf i_Arsym 'r_YFB arlunwyr_Egll yma_Adf ._Atdt 
Arfer_Egu cyffredin_Anscadu ar_Arsym y_YFB pryd_Egu ._Atdt 
Ei_Rhadib3bu brawd_Egu enwg_Anon awgrymodd_Bgorff3u iddi_Ar3bu gynnig_Egu ei_Banmedd3gu gwasanaeth_Be i_Arsym neb_Rhaamh llai_Rhaamh na_Cyscyd 'r_YFB cerflunydd_Egu byd_Egu enwog_Anscadu enwg_Anon _cyfenw__Anon ._Atdt 
S3_Anon Fe_Uberf annogodd_Bgorff3u enwg_Anon iddi_Ar3bu mae_Bpres3u 'n_Utra debyg_Anscadu ymm_Ebych i_Arsym fynd_Be at_Arsym cyfenw_Anon ._Atdt 
At_Arsym yr_YFB dyn_Egu mawr_Anscadu ei_Rhadib3gu hun_Rhaatb ._Atdt 
Y_YFB cerflunydd_Egu mawr_Anscadu ._Atdt 
A_Rhaperth roi_Be cnoc_Egbu ar_Arsym ei_Rhadib3gu ddrws_Egu o_Rhapers3gu a_Arsym dweud_Be "_Atddyf dyma_Adf fi_Rhapers1u ymm_Ebych enwb_Anon cyfenw_Anon a_Cyscyd '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Utra chwaer_Ebu i_Rhapers1u _enwg__Anon a_Cyscyd '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf chwilio_Be am_Arsym waith_Ebu fel_Cyscyd model_Egu ._Atdt "_Atddyf 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde "_Atddyf A_Cyscyd os_Cyscyd wneith_Bdyf3u e_Gwllyth dy_Rhadib2u dderbyn_Be di_Uberf "_Atddyf '_Atddyf wedodd_Bgorff3u enwg_Anon wrthi_Ar3bu "_Atddyf ymm_Ebych alli_Bpres2u di_Uberf 'i_Rhadib3bu chyfri_Egu hi_Rhapers3bu yn_Arsym fraint_Egbu i_Arsym fod_Be o_Rhapers3gu wasan'eth_unk i_Rhapers1u cyfenw_Anon "_Atddyf ._Atdt 
Byddai_Bamod3u hi_Rhapers3bu yn_Arsym '_Atddyf stafell_Ebu enfawr_Anscadu a_Cyscyd bydde_Bamod3u '_Atddyf yna_Bandangd nifer_Egu dwsinne_unk o_Arsym bobl_Ebu yn_Arsym gweithio_Be yna_Adf ._Atdt 
A_Cyscyd dyna_Adf lle_Egu bydde_Bamod3u '_Atddyf enwb_Anon '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf dychmygu_Be yn_Arsym ca'l_Ebu y_YFB syndod_Egu mowr_Anscadu 'ma_Adf o_Arsym weld_Be y_YFB ffasiwn_Egu le_Anscadu ac_Cyscyd o_Arsym weld_Be y_YFB ffordd_Ebu yr_Rhaperth oedd_Bamherff3u y_YFB cyfenw_Anon yn_Arsym gweithio_Be ._Atdt 
[_Atdchw Cyfarwyddyd_Egu i_Arsym beidio_Be trawsgrifio_Be ]_Atdde ._Atdt 
S1_Anon Bu_Bgorff3u yr_YFB eglwys_Ebu yn_Arsym gysur_Egu mawr_Anscadu iddi_Ar3bu pan_Egu bu_Bgorff3u farw_Anscadu y_YFB cawr_Egu o_Arsym ddyn_Egu oedd_Bamherff3u wedi_Uberf meddiannu_Be 'i_Rhadib3bu bywyd_Egu am_Arsym bymtheg_Rhifold mlynedd_Ebll ._Atdt 
Torrodd_Bgorff3u enwb_Anon ei_Rhadib3bu chalon_Ebu gan_Arsym amauei_unk bod_Be hi_Rhapers3bu 'n_Uberf mynd_Be yn_Arsym orffwyll_Egbu yn_Arsym ei_Rhadib3bu phrofedigaeth_Egbu ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Ond_Arsym o_Arsym dipyn_Egu i_Arsym beth_Egu dechreuodd_Bgorff3u baentio_Be unwaith_Adf eto_Adf ar_Arsym ôl_Anscadu bod_Be yn_Arsym gaeth_Bgorff3u i_Rhapers1u cyfenw_Anon cyhyd_Anscadu roedd_Bamherff3u ei_Rhadib3gu farwol'eth_unk hefyd_Adf mewn_Arsym ffordd_Ebu yn_Arsym 'i_Rhadib3gu ryddhau_Be hi_Rhapers3bu ._Atdt 
Bu_Bgorff3u 'n_Uberf paentio_Be 'n_Utra gyson_Anscadu ._Atdt 
Cafodd_Bgorff3u arddangosfa_Ebu iddi_Ar3bu hi_Rhapers3bu 'i_Rhadib3bu hun_Rhaatb yn_Arsym Llunde_Ep 'n_Arsym ._Atdt 
Ac_Cyscyd fe_Uberf brynodd_Bgorff3u Amgueddfa_Ebu Genedlaethol_Anscadu Cymru_Epb un_Rhaamh o_Arsym 'i_Rhadib3bu llunie_Egll ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Erbyn_Arsym hyn_Rhadangd roedd_Bamherff3u hi_Rhapers3bu wedi_Uberf prynu_Be darn_Egu o_Arsym dir_Egu yn_Arsym [aneglur]_Gwann gyda_Arsym 'r_YFB bwriad_Egu o_Arsym adfer_Egu y_YFB cwt_Egu yng_Arsym ngwaelod_Egu yr_YFB ardd_Ebu yn_Arsym gartref_Adf ._Atdt 
Ond_Arsym yna_Adf fe_Uberf dorrodd_Bgorff3u ei_Rhadib3bu hiechyd_Egu ._Atdt 
Roedd_Bamherff3u blynyddoedd_Ebll olaf_Anscadu enwb_Anon yn_Arsym gyfnod_Egu gymharol_Anscadu unig_Anscadu ac_Cyscyd roedd_Bamherff3u hi_Rhapers3bu 'n_Uberf trulio_Be mwy_Rhaamh a_Cyscyd mwy_Rhaamh o_Arsym 'i_Rhadib3bu hamser_Egu yn_Arsym myfyrio_Be ac_Cyscyd yn_Arsym gweddïo_Be ._Atdt 
<_Atdchw S_Gwllyth ?_Atdt >_Atdde Yma_Adf yn_Arsym y_YFB cwt_Egu yn_Arsym [aneglur]_Gwann ro'dd_Ebu hi_Rhapers3bu 'n_Uberf treulio_Be mwy_Rhaamh o_Arsym 'i_Rhadib3bu dyddie_Egll ._Atdt 
'_Atddyf D_Gwllyth o'dd_Bamherff3u e_Rhapers3gu 'm_Adf yn_Arsym glyd_Anscadu iawn_Adf a_Cyscyd pan_Cyscyd fydde_Bamod3u 'r_YFB gweithwyr_Egll yn_Arsym dod_Be i_Arsym drwsio_Be 'r_YFB to_Egu byddai_Bamod3u 'n_Uberf cysgu_Be yn_Arsym yr_YFB awyr_Ebu agored_Anscadu pa_Bangof mor_Adf fregus_Anscadu bynnag_Anscadu o'dd_Bamherff3u ei_Rhadib3bu hiechyd_Egu hi_Rhapers3bu ._Atdt 
[_Atdchw Cerddoriaeth_Ebu ]_Atdde ._Atdt 
[_Atdchw Cyfarwyddyd_Egu i_Arsym beidio_Be trawsgrifio_Be ]_Atdde ._Atdt 
1442.944_Gwdig 