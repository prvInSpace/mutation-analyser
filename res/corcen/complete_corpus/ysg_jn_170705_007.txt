Pan_Egu mae_Bpres3u gwaed_Egu yn_Arsym dewach_Anscym na_Cyscyd dŵr_Egu …_Gwsym 
Mae_Bpres3u Macs_Ep White_Gwest yn_Arsym un_Rhaamh o_Arsym gymeriadau_Egu cymhletha_Bgorch2u '_Atddyf Pobol_Ebu y_YFB Cwm_Egu ac_Cyscyd ers_Arsym iddo_Ar3gu ddychwelyd_Be i_Rhapers1u Gwmderi_Gwest yn_Arsym ddiweddar_Anscadu ,_Atdcan mae_Bpres3u helynt_Ebu wedi_Uberf ei_Rhadib3gu ddilyn_Be unwaith_Adf eto_Adf ._Atdt 
Y_YFB tro_Egu hwn_Rhadangg fe_Uberf laniodd_Bgorff3u ym_Arsym mreichiau_Ebll cynllwyngar_unk Gwyneth_Epb (_Atdchw Llinor_Gwest ap_Arsym Gwynedd_Ep )_Atdde cyn_Arsym sylweddoli_Be ei_Rhadib3bu bod_Be hi_Rhapers3bu â_Arsym 'i_Rhadib3gu bryd_Rhagof ar_Arsym ladd_Be ei_Rhadib3gu dad_Egu a_Cyscyd dwyn_Be ei_Rhadib3gu gartref_Egu ._Atdt 
Ond_Arsym i_Rhapers1u Macs_Gwest ,_Atdcan mae_Bpres3u gwaed_Egu yn_Arsym dewach_Anscym na_Cyscyd dŵr_Egu ac_Cyscyd mae_Bpres3u bellach_Adf wedi_Uberf cefnu_Be ar_Arsym Gwyneth_Ep i_Arsym ddychwelyd_Be i_Arsym freichiau_Ebll cynnes_Anscadu ei_Rhadib3gu deulu_Egu ._Atdt 
I_Arsym 'r_YFB actor_Egu Rhys_Egu Bidder_Ep ,_Atdcan 32_Gwdig ,_Atdcan sy_Bpres3perth 'n_Arsym ei_Rhadib3gu bortreadu_Be ,_Atdcan mae_Bpres3u hynny_Rhadangd 'n_Uberf dangos_Be bod_Be Macs_Ep ymhell_Adf o_Arsym fod_Be yn_Arsym ddrwg_Anscadu i_Arsym gyd_Egu ._Atdt 
Cawn_Bamherff1u weld_Be a_Cyscyd yw_Bpres3u hynny_Rhadangd 'n_Arsym wir_Ebych yn_Arsym ystod_Ebu yr_YFB wythnosau_Ebll nesaf_Anseith ,_Atdcan ond_Arsym ,_Atdcan yn_Arsym y_YFB cyfamser_Egu rydyn_Bpres3ll ni_Rhapers1ll wedi_Uberf gofyn_Be ambell_Anscadu gwestiwn_Bamherff1u i_Rhapers1u 'r_YFB actor_Egu sy_Bpres3perth 'n_Utra wreiddiol_Anscadu o_Arsym 'r_YFB Crwys_Ebu ,_Atdcan Abertawe_Ep ac_Cyscyd sydd_Bpres3perth bellach_Adf yn_Arsym byw_Be yng_Arsym Nghaerdydd_Ep ._Atdt 
Sut_Bandangd brofiad_Egu oedd_Bamherff3u hi_Rhapers3bu i_Arsym ddychwelyd_Be i_Arsym 'r_YFB Cwm_Egu ?_Atdt 
Roedd_Bamherff3u e_Rhapers3gu 'n_Utra od_Anscadu ac_Cyscyd yn_Arsym braf_Anscadu ar_Arsym yr_YFB un_Bancynnar pryd_Rhagof ._Atdt 
Roedd_Bamherff3u yn_Arsym braf_Anscadu cael_Be gweld_Be a_Cyscyd gweithio_Be gyda_Arsym chymaint_Anscadu o_Arsym ffrindiau_Egll eto_Adf ._Atdt 
Ond_Arsym yn_Arsym od_Anscadu sefyll_Be ar_Arsym set_Ebu benodol_Anscadu ,_Atdcan cerdded_Be lawr_Adf cyntedd_Egu neu_Cyscyd eistedd_Be mewn_Arsym ystafell_Ebu ymarfer_Ebu a_Cyscyd theimlo_Be fel_Cyscyd taw_Egu dim_Egu ond_Arsym ddoe_Adf '_Atddyf nes_Bgorff1u i_Rhapers1u adael_Be ._Atdt 
Mae_Bpres3u rhai_Banmeint blynyddoedd_Ebll wedi_Uberf pasio_Be mewn_Arsym gwirionedd_Egu ._Atdt 
Dwi_Bpres1u 'n_Uberf dal_Be i_Arsym edmygu_Be a_Cyscyd synnu_Be sut_Egbu mae_Bpres3u 'r_YFB holl_Anscadu beth_Rhagof yn_Arsym cael_Be ei_Rhadib3gu dynnu_Be at_Arsym ei_Rhadib3gu gilydd_Egu ,_Atdcan mae_Bpres3u 'r_YFB holl_Bancynnar dîm_Egu yn_Arsym anhygoel_Anscadu ._Atdt 
Ydy_Bpres3u Cwmderi_Ep yn_Arsym debyg_Anscadu i_Rhapers1u unrhyw_Bancynnar le_Egu ti_Rhapers2u 'n_Arsym '_Atddyf nabod_Be ?_Atdt 
Rwy_Bpres1u 'n_Utra siŵr_Anscadu bod_Be 'na_Adf rannau_Ebll o_Rhapers3gu Gwmderi_Gwest ledled_Adf y_YFB wlad_Ebu mewn_Arsym sawl_Banmeint pentref_Egu Cymreig_Anscadu ._Atdt 
Mae_Bpres3u 'r_YFB Crwys_Ebu ger_Egu Abertawe_Ep lle_Anscadu ges_Bgorff1u i_Rhapers1u 'n_Uberf magu_Be yn_Arsym debyg_Anscadu ._Atdt 
Mae_Bpres3u gennym_Ar1ll ni_Rhapers1ll siop_Ebu a_Cyscyd chapel_Egu ,_Atdcan ond_Arsym mae_Bpres3u yna_Ar1u ddwy_Rhifol dafarn_Egbu a_Cyscyd dim_Egu ond_Arsym un_Bancynnar Gog_Egu (_Atdchw Helo_Ebych Siân_Bpres3ll C_Gwllyth !_Atdt )_Atdde ._Atdt 
A_Rhaperth allet_Bamherff2u ti_Rhapers2u ddisgrifio_Be beth_Rhagof sy_Bpres3perth 'n_Utra dda_Anscadu am_Gwtalf Macs_Gwest mewn_Arsym tri_Rhifol gair_Egu ?_Atdt 
Penderfynol_Anscadu ._Atdt 
Galluog_Anscadu ._Atdt 
Ffyddlon_Anscadu (_Atdchw i'w_Arsym deulu_Egu -_Atdcys credwch_Bgorch2ll neu_Cyscyd beidio_Be )_Atdde ._Atdt 
A_Cyscyd beth_Rhagof sy_Bpres3perth 'n_Utra ddrwg_Anscadu amdano_Ar3gu mewn_Arsym tri_Rhifol gair_Egu ?_Atdt 
'_Atddyf Control_Gwest -_Atdcys freak_Gwest '_Atddyf ._Atdt 
Byrbwyll_Anscadu ._Atdt 
Anaeddfed_Anscadu ._Atdt 
A_Rhaperth fyddet_Bamod2u ti_Rhapers2u 'n_Uberf annog_Be rhywun_Egu i_Arsym fentro_Be i_Arsym berthynas_Ebu gyda_Arsym Macs_Ep ,_Atdcan o_Arsym gofio_Be ei_Rhadib3gu berthnasau_Ebll gyda_Arsym menywod_Egll a_Arsym bod_Be ganddo_Ar3gu lovechild_Gwest ?_Atdt 
Falle_Adf ddim_Egu ._Atdt 
Mae_Bpres3u 'na_Adf dueddiad_Ebu i_Rhapers1u drwbwl_unk '_Atddyf ffeindio_Be '_Atddyf Macs_Gwest ._Atdt 
Beth_Rhagof yw_Bpres3u dyfodol_Egu y_YFB teulu_Egu White_Gwest ?_Atdt 
Pwy_Rhagof a_Cyscyd ŵyr_Egu ?_Atdt 
Os_Cyscyd gwnawn_Bamherff1u nhw_Rhapers3ll ganolbwyntio_Be ar_Arsym gefnogi_Be ei_Rhadib3gu gilydd_Egu ,_Atdcan falle_Adf bydden_Bamod3ll nhw_Rhapers3ll 'n_Utra iawn_Ans ,_Atdcan ond_Arsym cawn_Egll weld_Be ._Atdt 
Nawr_Adf ,_Atdcan bach_Anscadu mwy_Anscym amdanat_Ar2u ti_Rhapers2u ._Atdt 
Beth_Rhagof yw_Bpres3u dy_Rhadib2u hoff_Anscadu le_Egu am_Arsym sundowner_Gwest ?_Atdt 
Unrhyw_Bancynnar le_Rhagof ar_Arsym y_YFB Gwŷr_Egll ._Atdt 
Ambell_Banmeint le_Rhagof yn_Arsym Sir_Ebu Fôn_Egu '_Atddyf fyd_Egu ._Atdt 
Maen_Bpres3ll nhw_Rhapers3ll 'n_Utra eithaf_Anscadu tebyg_Anscadu mewn_Arsym rhai_Banmeint ffyrdd_Egll ._Atdt 
Pe_Cyscyd baet_unk ti_Rhapers2u 'n_Uberf ennill_Be y_YFB loteri_Egu ,_Atdcan lle_Rhagof byddet_Bamod2u ti_Rhapers2u 'n_Utra byw_Be a_Cyscyd gyda_Arsym phwy_Egu ?_Atdt 
Wel_Ebych ,_Atdcan mae_Bpres3u Mam_Ebu yn_Arsym Abertawe_Ep ,_Atdcan fy_Banmedd1u nghariad_Egbu ,_Atdcan fy_Banmedd1u mrawd_Egu a_Cyscyd ffrindiau_Egll yng_Arsym Nghaerdydd_Ep ,_Atdcan fy_Banmedd1u mrawd_Egu arall_Anscadu yn_Arsym San_Egu Francisco_Gwest a_Cyscyd fy_Banmedd1u chwaer_Ebu yn_Arsym Lerpwl_Ep /_Gwsym Ynys_Ebu Manaw_Ep ._Atdt 
Os_Cyscyd gallwn_Bamherff1u i_Rhapers1u rannu_Be 'n_Utra amser_Egu rhwng_Arsym y_YFB lleoliadau_Egll yma_Adf byddwn_Bamod1u i_Rhapers1u 'n_Utra ddigon_Rhaamh hapus_Anscadu ._Atdt 
Pa_Adf gymeriad_Egu ti_Rhapers2u wedi_Uberf mwynhau_Be chwarae_Egu fwya_Anseith '_Atddyf ?_Atdt 
Rydw_Bpres1u i_Rhapers1u wedi_Adf chwarae_Egu Macs_Ep am_Arsym y_YFB cyfnod_Egu hiraf_Anscadu yn_Arsym fy_Banmedd1u ngyrfa_Ebu felly_Adf dwi_Bpres1u 'n_Uberf teimlo_Be fy_Rhadib1u mod_Be i_Rhapers1u 'n_Arsym ei_Rhadib3bu '_Atddyf nabod_Be e_Rhapers3gu orau_Anseith ._Atdt 
Ond_Arsym ges_Bgorff1u i_Rhapers1u brofiad_Egu anhygoel_Anscadu yn_Arsym chwarae_Egu Gronw_Ep Pebr_Gwest yn_Arsym nrama_Ebu Blodeuwedd_Epb ,_Atdcan Saunders_Ep Lewis_Gwest ._Atdt 
Roedd_Bamherff3u yn_Arsym gynhyrchiad_Egu arbennig_Anscadu ar_Arsym ben_Egu mynydd_Egu -_Atdcys profiad_Egu heriol_Anscadu fel_Cyscyd actor_Egu lle_Anscadu gwnes_Bgorff1u i_Rhapers1u ddysgu_Be lot_Ebu ._Atdt 
Pwy_Rhagof yw_Bpres3u dy_Rhadib2u hoff_Anscadu actorion_Egll ?_Atdt 
Un_Rhaamh o_Arsym fy_Rhadib1u hoff_Anscadu ffilmiau_Ebll yw_Bpres3u Butch_Ep Cassidy_Ep and_Gwest the_Egu Sundance_Gwest Kid_Gwest ._Atdt 
Dyma_Adf 'r_YFB tro_Egu cyntaf_Anscadu i_Arsym fi_Rhapers1u weld_Be Paul_Gwest Newman_Ep a_Cyscyd Robert_Ep Redford_Ep ._Atdt 
Actorion_Egll anhygoel_Anscadu ._Atdt 
Rwy_Bpres1u wedi_Uberf cael_Be gweithio_Be '_Atddyf dag_Egu actorion_Egll anhygoel_Anscadu hefyd_Adf ac_Cyscyd wedi_Uberf dysgu_Be llawer_Rhaamh wrthyn_Ar3ll nhw_Rhapers3ll trwy_Arsym siarad_Be am_Arsym y_YFB grefft_Ebu a_Cyscyd 'u_Banmedd3ll profiadau_Egll nhw_Rhapers3ll ond_Arsym hefyd_Adf trwy_Arsym eu_Rhadib3ll gwylio_Be ._Atdt 
Pobol_Ebu y_YFB Cwm_Egu 
Llun_Egu -_Atdcys Gwener_Ebu 8.00_Gwdig ,_Atdcan S4C_Ebu 
Isdeitlau_Egll Cymraeg_Ebu a_Cyscyd Saesneg_Ebu 
Bob_Anscadu prynhawn_Egu Sul_Egu gydag_Arsym isdeitlau_Egll Saesneg_Ebu ar_Arsym y_YFB sgrin_Ebu 
Ar_Arsym alw_Egu :_Atdcan s4c.cymru_Gwann ;_Atdcan BBC_Gwacr iPlayer_Gwest a_Cyscyd llwyfannau_Egll eraill_Anscadu 
Cynhyrchiad_Egu BBC_Gwacr Cymru_Epb 