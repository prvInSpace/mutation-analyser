Enw_Egu 'r_YFB Ffederasiwn_Egu 
ETHOLIAD_Egu RHIANT_Egu LYWODRAETHWR_Egu -_Atdcys FFURFLEN_Ebu ENWEBU_Be 
1_Gwdig ._Atdt 
Manylion_Egll y_YFB rhiant_Egu /_Gwsym gwarcheidwad_Egu :_Atdcan 
Teitl_Egu 
Cyfenw_Egu 
Enwau_Egll Cyntaf_Anscadu 
Cyfeiriad_Egu 
Ffôn_Bpres3ll 
E-bost_Egu 
2_Gwdig ._Atdt 
Rwy_Bpres1u 'n_Uberf cadarnhau_Be fy_Banmedd1u nymuniad_Egu i_Rhapers1u sefyll_Be fel_Cyscyd ymgeisydd_Egu am_Arsym le_Egu fel_Cyscyd rhiant_Egu lywodraethwr_Egu ar_Arsym gyfer_Egu 
(_Atdchw Enw_Egu 'r_YFB Ffederasiwn_Egu )_Atdde 
Ac_Cyscyd rwy_Bpres1u 'n_Uberf cadarnhau_Be fy_Rhadib1u mod_Be yn_Arsym gymwys_Anscadu i_Arsym wneud_Be hynny_Rhadangd (_Atdchw gweler_Bgorchamhers y_YFB rhestr_Ebu o_Arsym eithriadau_Egll sydd_Bpres3perth wedi_Uberf eu_Rhadib3ll hatodi_Be )_Atdde ._Atdt 
3_Gwdig ._Atdt 
Llofnod_Egu y_YFB rhiant_Egu /_Gwsym gwarcheidwad_Egu :_Atdcan Dyddiad_Egu :_Atdcan 
Mae_Bpres3u fy_Banmedd1u mhlentyn_Egu yn_Arsym Ysgol_Ebu /_Gwsym Blwyddyn_Ebu /_Gwsym Dosbarth_Egu :_Atdcan 
4_Gwdig ._Atdt 
Os_Cyscyd cynhelir_Bpresamhers etholiad_Egu ,_Atdcan carwn_Bamherff1u i_Rhapers1u 'r_YFB wybodaeth_Ebu ganlynol_Anscadu amdanaf_Ar1u gael_Be ei_Rhadib3bu dosbarthu_Be i_Arsym 'r_YFB holl_Bancynnar rieni_Egll gyda_Arsym 'r_YFB papur_Egu pleidleisio_Be (_Atdchw Cyfyngiad_Egu llym_Anscadu o_Arsym 50_Gwdig gair_Egu yn_Arsym unig_Anscadu )_Atdde ._Atdt 
Noder_Bgorchamhers y_Rhadib1u caiff_Bpres3u unrhyw_Anscadu beth_Rhagof dros_Arsym 50_Gwdig gair_Egu ei_Rhadib3gu gyfeirio_Be nôl_Be atoch_Ar2ll ac_Cyscyd y_Rhaperth byddai_Bamod3u 'n_Uberf oedi_Be 'r_YFB broses_Ebu hon_Rhadangb )_Atdde ._Atdt 
Os_Cyscyd nad_Rhaperth ydych_Bpres2ll yn_Arsym dymuno_Be rhoi_Be unrhyw_Bancynnar wybodaeth_Ebu neu_Cyscyd os_Cyscyd gadewir_Bpresamhers y_YFB gofod_Egu yn_Arsym wag_Anscadu ,_Atdcan bydd_Bdyf3u eich_Banmedd2ll enw_Egu yn_Arsym mynd_Be yn_Arsym ei_Rhadib3gu flaen_Egu i_Arsym 'r_YFB etholiad_Egu heb_Arsym unrhyw_Bancynnar fanylion_Egll pellach_Anscym ._Atdt 
Defnyddir_Bpresamhers y_YFB data_Egu a_Rhaperth gaiff_Bpres3u ei_Rhadib3gu gasglu_Be gyda_Arsym 'r_YFB ffurflen_Ebu hon_Rhadangb at_Arsym ddibenion_Egll recriwtio_Be ._Atdt 
Caiff_Bpres3u ei_Rhadib3gu rannu_Be â_Arsym 'r_YFB Pennaeth_Egu ,_Atdcan Clerc_Egu y_YFB Llywodraethwyr_Egll a_Cyscyd 'r_YFB Awdurdod_Egbu Lleol_Anscadu ar_Arsym gael_Be eich_Rhadib2ll penodi_Be a_Cyscyd 'i_Rhadib3gu brosesu_Be yn_Arsym unol_Anscadu â_Arsym Deddf_Ebu Diogelu_Be Data_Egu 2018_Gwdig ._Atdt 
Am_Arsym fwy_Rhaamh o_Arsym wybodaeth_Ebu am_Arsym sut_Egbu y_Rhadib1u byddwn_Bdyf1ll yn_Arsym prosesu_Be eich_Banmedd2ll data_Egu personol_Anscadu cyfeiriwch_Bgorch2ll at_Arsym ein_Banmedd1ll polisi_Egu Preifatrwydd_Egu ._Atdt 
Caiff_Bpres3u y_YFB ffurflen_Ebu ei_Rhadib3bu dinistrio_Be wedi_Arsym 6_Gwdig mis_Egu ._Atdt 
Am_Arsym fwy_Rhaamh o_Arsym wybodaeth_Ebu am_Arsym ofynion_Egll diogelu_Be data_Egu 'r_YFB ysgol_Ebu cysylltwch_Bgorch2ll â_Arsym cyfenw_Anon ,_Atdcan cyfeiriad__Anon 
e-bost_Egu :_Atdcan cyfeiriad_e-bost_Anon 
PWYSIG_Anscadu :_Atdcan Rhaid_Egu dychwelyd_Be y_YFB ffurflen_Ebu hon_Rhadangb i_Arsym 'r_YFB Pennaeth_Egu erbyn_Arsym 3:30_Gwann pm_Gwtalf ,_Atdcan (_Atdchw Diwrnod_Egu a_Cyscyd Dyddiad_Egu dychwelyd_Be enwebiadau_Egll )_Atdde 
Defnydd_Egu Mewnol_Anscadu yn_Arsym Unig_Anscadu 
ENW'R_Gwacr YMGEISYDD_Egu :_Atdcan 
CYMWYS_Anscadu I_Arsym SEFYLL_Be MEWN_Arsym ETHOLIAD_Egu YDI_Bpres3u NAC_Egbu YDI_Bpres3u 
Llofnod_Egu :_Atdcan Dyddiad_Egu :_Atdcan 
Pennaeth_Egu (_Atdchw Swyddog_Egu Canlyniadau_Egll )_Atdde 