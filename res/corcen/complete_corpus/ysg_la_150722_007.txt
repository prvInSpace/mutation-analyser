Embargo_Egu llym_Anscadu tan_Arsym 15.15_Gwdig 7_Gwdig Awst_Egu 2015_Gwdig 
GWENNO_Gwacr YN_Arsym ENNILL_Egu GWOBR_Egbu ALBWM_Egu CYMRAEG_Ebu Y_YFB FLWYDDYN_Ebu 
Gwenno_Bdibdyf3u sy_Bpres3perth 'n_Uberf ennill_Be Gwobr_Egbu Albwm_Egu Cymraeg_Ebu y_YFB Flwyddyn_Ebu 2015_Gwdig ._Atdt 
Cyhoeddwyd_Bgorffamhers hyn_Rhadangd mewn_Arsym seremoni_Ebu arbennig_Anscadu yng_Arsym Nghaffi_Egu Maes_Egu B_Gwllyth y_YFB prynhawn_Egu ma_Bandangg ._Atdt 
Daeth_Bgorff3u yr_YFB albwm_Egu i_Arsym 'r_YFB brig_Egu ar_Arsym ôl_Anscadu trafodaeth_Ebu ar_Arsym Faes_Egu yr_YFB Eisteddfod_Ebu ._Atdt 
Roedd_Bamherff3u rheithgor_Egu o_Arsym unigolion_Egll sy_Bpres3perth 'n_Uberf ymwneud_Be gyda_Arsym 'r_YFB diwydiant_Egu cerdd_Ebu yma_Adf yng_Arsym Nghymru_Epb wedi_Uberf trafod_Be cynnyrch_Egu y_YFB flwyddyn_Ebu ,_Atdcan gyda_Arsym 'r_YFB albymau_Egll canlynol_Anscadu ar_Arsym y_YFB rhestr_Ebu fer_Anscadbu :_Atdcan 
•_Gwsym 9_Gwdig Bach_Egu -_Atdcys Tincian_Bpres3ll (_Atdchw Real_Anscadu World_Gwest )_Atdde 
•_Gwsym Al_Epg Lewis_Gwest -_Atdcys Heulwen_Ebu o_Arsym Hiraeth_Egu (_Atdchw Al_Epg Lewis_Gwest )_Atdde 
•_Gwsym Candelas_Gwest -_Atdcys Bodoli_Be 'n_Utra Ddistaw_Anscadu (_Atdchw I_Rhapers1u -_Atdcys Kaching_Gwest )_Atdde 
•_Gwsym Datblygu_Be -_Atdcys Erbyn_Arsym Hyn_Rhadangd (_Atdchw Ankst_Gwest Music_Gwest )_Atdde 
•_Gwsym Fernhill_Ep -_Atdcys Amser_Egu 
•_Gwsym Gwenno_Bdibdyf3u -_Atdcys Y_YFB Dydd_Egu Olaf_Anscadu (_Atdchw Peski_Gwest )_Atdde 
•_Gwsym Yws_Ep Gwynedd_Ep -_Atdcys Codi_Bpres2u /\_Gwsym Cysgu_Be 
•_Gwsym Geraint_Egll Jarman_Ep -_Atdcys Dwyn_Anscadu yr_YFB Hogyn_Egu Nol_Be (_Atdchw Ankst_Gwest )_Atdde 
•_Gwsym Plu_Egll -_Atdcys Holl_Bancynnar Anifeiliaid_Egll y_YFB Goedwig_Ebu (_Atdchw Sain_Ebu )_Atdde 
•_Gwsym R_Gwllyth Seiliog_Gwest -_Atdcys In_Gwest HZ_Gwacr (_Atdchw Turnstyle_Gwest )_Atdde 
Dywedodd_Bgorff3u un_Rhaamh o_Arsym drefnwyr_Egll y_YFB wobr_Egbu ,_Atdcan Guto_Epg Brychan_Bpres3ll ,_Atdcan "_Atddyf Mae_Bpres3u 'n_Utra braf_Anscadu iawn_Adf gweld_Be cymaint_Anscadu o_Arsym amrywiaeth_Ebu ar_Arsym restr_Ebu fer_Anscadbu Gwobr_Egbu Albwm_Egu Cymraeg_Ebu y_YFB Flwyddyn_Ebu eleni_Adf ,_Atdcan ac_Cyscyd mae_Bpres3u 'n_Utra restr_Ebu eithaf_Anscadu gwahanol_Anscadu i_Arsym 'r_YFB llynedd_Adf ._Atdt 
Mae_Bpres3u wedi_Uberf bod_Be yn_Arsym flwyddyn_Ebu dda_Anscadu o_Arsym ran_Ebu cynnyrch_Egu eleni_Adf ,_Atdcan ac_Cyscyd mae_Bpres3u 'r_YFB amrywiaeth_Ebu ar_Arsym y_YFB rhestr_Ebu fer_Anscadbu yn_Arsym adlewyrchu_Be hynny_Rhadangd ._Atdt 
Cydiodd_Bgorff3u amryw_Rhaamh o_Arsym 'r_YFB albymau_Egll yn_Arsym nychymyg_Egu y_YFB rheithgor_Egu ,_Atdcan a_Cyscyd Gwenno_Bdibdyf3u ddaeth_Bgorff3u i_Arsym 'r_YFB brig_Egu yn_Arsym ystod_Ebu y_YFB sesiwn_Ebu feirniadu_Be heddiw_Adf ._Atdt 
"_Atddyf Dyma_Adf 'r_YFB eildro_Egu i_Arsym ni_Rhapers1ll gynnig_Be y_YFB wobr_Egbu ,_Atdcan a_Cyscyd datblygwyd_Bgorffamhers y_YFB syniad_Egu gan_Arsym fod_Be yr_YFB Eisteddfod_Ebu yn_Arsym awyddus_Anscadu i_Arsym roi_Be sylw_Egu haeddiannol_Anscadu i_Arsym gerddoriaeth_Ebu sydd_Bpres3perth wedi_Uberf 'i_Rhadib3bu recordio_Be neu_Cyscyd 'i_Rhadib3bu chreu_Be 'n_Utra ddiweddar_Anscadu ._Atdt 
Mae_Bpres3u nifer_Egu o_Arsym wahanol_Anscadu ddisgyblaethau_Ebll 'n_Uberf cael_Be eu_Rhadib3ll gwobrwyo_Be yn_Arsym yr_YFB Eisteddfod_Ebu ,_Atdcan ac_Cyscyd mae_Bpres3u gwobrau_Egbll 'n_Uberf bodoli_Be ar_Arsym gyfer_Egu mathau_Egll arbennig_Anscadu o_Arsym gerddoriaeth_Ebu ,_Atdcan a_Cyscyd bwriad_Egu y_YFB wobr_Egbu hon_Rhadangb yw_Bpres3u dathlu_Be pob_Egll math_Ebu o_Arsym gerddoriaeth_Ebu sy_Bpres3perth 'n_Uberf cael_Be ei_Rhadib3bu chreu_Be yn_Arsym y_YFB Gymraeg_Ebu ar_Arsym hyn_Rhadangd o_Arsym bryd_Egu ._Atdt 
Enillwyd_Bgorffamhers y_YFB wobr_Egbu y_YFB llynedd_Adf gan_Arsym The_Egu Gentle_Gwest Good_Gwest (_Atdchw Gareth_Epg Bonello_Gwest )_Atdde am_Gwtalf ei_Rhadib3bu albwm_Egu '_Atddyf Y_YFB Bardd_Egu Anfarwol_Anscadu '_Atddyf ._Atdt "_Atddyf 
Gwenno_Bdibdyf3u 
Mae_Bpres3u Gwenno_Bdibdyf3u Saunders_Ep yn_Arsym gynhyrchydd_Egu cerddoriaeth_Ebu ,_Atdcan DJ_Gwacr ,_Atdcan cyflwynydd_Egu radio_Egu a_Cyscyd chantores_Ebu o_Arsym Gaerdydd_Ep ._Atdt 
Mae_Bpres3u hi_Rhapers3bu wedi_Uberf rhyddhau_Be ei_Rhadib3bu halbwm_Egu gyntaf_Anscadu unigol_Anscadu yn_Arsym ddiweddar_Anscadu ,_Atdcan sef_Cyscyd '_Atddyf Y_YFB Dydd_Egu Olaf_Anscadu '_Atddyf ,_Atdcan yn_Arsym dilyn_Be cyfres_Ebu o_Arsym EPs_Ep llwyddiannus_Anscadu -_Atdcys '_Atddyf Chwyldro_Egu '_Atddyf ,_Atdcan '_Atddyf Golau_Egu Arall_Anscadu '_Atddyf a_Cyscyd '_Atddyf Fratolish_Ep Hiang_Ep Perpeshki_Gwest /_Gwsym Calon_Ebu Peiriant_Egu '_Atddyf ._Atdt 
Mae_Bpres3u 'n_Uberf siarad_Be Cymraeg_Ebu a_Cyscyd Chernyweg_Gwest ac_Cyscyd mae_Bpres3u ei_Rhadib3bu cherddoriaeth_Ebu yn_Arsym trosi_Be ei_Rhadib3bu dylanwadau_Egll rhyngwladol_Anscadu i_Rhapers1u mewn_Arsym i_Arsym synau_Egll electronig_Anscadu lo_Egu -_Atdcys fi_Uberf sy_Bpres3perth '_Atddyf wedi_Uberf 'u_Rhadib3ll lapio_Be mewn_Arsym llais_Egu adleisio_Be a_Cyscyd churiadau_unk musique_Gwest concrète_Gwest ._Atdt 