Cynigion_Bgorff1ll Bwyd_Egu Amser_Egu Cinio_Egu yn_Arsym enw_sefydliad_Anon 
Dim_Egu ond_Arsym ar_Arsym gyfer_Egu deiliaid_Egll Cerdyn_Egu Teyrngarwch_Egu y_Rhadib1u mae_Bpres3u 'r_YFB cynigion_Egll hyn_Rhadangd ar_Arsym gael_Be ._Atdt 
Cliciwch_Bgorch2ll ar_Arsym y_YFB botwm_Egu isod_Adf i_Arsym wneud_Be cais_Egu am_Arsym Gerdyn_Egu Teyrngarwch_Egu 
Gwnewch_Bgorch2ll gais_Egu am_Arsym gerdyn_Egu teyrngarwch_Egu 
COFIWCH_Bgorch2ll DANGOS_Be EICH_Banmedd2ll CERDYN_Egu TEYRNGARWCH_Egu IR_Anscadu STAFF_Egll CYN_Arsym RHOI_Be EICH_Banmedd2ll ARCHEB_Ebu FEL_Cyscyd A_Cyscyd GALLENT_Bamherff3ll GWEITHREDU_Be EICH_Banmedd2ll DISGOWNT_Egu TEYRNGARWCH_Egu ._Atdt 
<_Atdchw img_unk /_Gwsym >_Atdde 
<_Atdchw img_unk /_Gwsym >_Atdde 
Yr_Rhaperth ydym_Bpres1ll yn_Arsym hoffi_Be gwobrwyo_Be ein_Banmedd1ll cwsmeriaid_Egll ._Atdt ._Atdt ._Atdt 
Ar_Arsym gyfer_Egu ein_Banmedd1ll gwesteion_Egll gyda_Arsym chardiau_Ebll teyrngarwch_Egu ,_Atdcan mae_Bpres3u gennym_Ar1ll nifer_Egu o_Arsym gynigion_Egll ar_Arsym fwyd_Egu a_Cyscyd diodydd_Ebll yn_Arsym enw_sefydliad_Anon yr_YFB wythnos_Ebu hon_Rhadangb ._Atdt 
Cask_Gwest ale_Gwest of_Gwest the_Gwest day_Gwest -_Atdcys £_Gwsym 2.50_Gwdig 
Craft_Gwest beer_Gwest -_Atdcys £_Gwsym 3.90_Gwdig 
Pint_Gwest of_Anscadu Aspalls_Gwest -_Atdcys £_Gwsym 3.50_Gwdig 
Bottle_Gwest of_Gwest porta_Gwest wine_Gwest -_Atdcys £_Gwsym 8.00_Gwdig 
Bottle_Gwest of_Gwest Cabernet_Gwest Sauvignon_Gwest -_Atdcys £_Gwsym 11.95_Gwdig 
Gin_Gwest of_Gwest the_Gwest week_Gwest with_Gwest dash_Gwest -_Atdcys £_Gwsym 3.50_Gwdig 
Whiskey_Gwest of_Gwest the_Gwest week_Gwest 
Rum_Gwest of_Gwest the_Gwest week_Gwest 
J_Gwllyth 20_Gwdig Apple_Gwest &_Gwsym Mango_Gwest -_Atdcys £_Gwsym 1.00_Gwdig 
Bitter_Gwest Lemon_Gwest -_Atdcys 50_Gwdig p_Gwllyth 
Diet_Gwest coke_Gwest -_Atdcys 1.50_Gwdig p_Gwllyth 
Latte_Gwest -_Atdcys £_Gwsym 1.50_Gwdig 
Mae_Bpres3u ein_Banmedd1ll cynigion_Egll diod_Ebu ar_Arsym gael_Be drwy_Arsym 'r_YFB dydd_Egu ,_Atdcan bob_Banmeint dydd_Egu ._Atdt 
(_Atdchw Cynnig_Be yn_Arsym dod_Be i_Arsym ben_Egu 09_Gwdig /_Gwsym 02_Gwdig /_Gwsym 2018_Gwdig )_Atdde 
Sylwch_Bgorch2ll fod_Be y_YFB cerdyn_Egu teyrngarwch_Egu hefyd_Adf yn_Arsym rhoi_Be cyfraddau_Ebll gostyngol_Anscadu ar_Arsym y_YFB rhan_Ebu fwyaf_Anseith o'n_Bamherff1u diodydd_Ebll bar_Egu eraill_Anscadu hefyd_Adf ._Atdt 
Mwy_Anscym o_Rhapers3gu gynnigion_Bgorff1ll ._Atdt ._Atdt ._Atdt 
Mwynhewch_Bgorch2ll bwyd_Egu blasus_Anscadu gyda_Arsym ni_Rhapers1ll ._Atdt ._Atdt ._Atdt 
Mae_Bpres3u gennym_Ar1ll amrywiaeth_Ebu o_Rhapers3gu gynnigion_Bgorff1ll ar_Arsym fwydydd_Egll dethol_Be o'n_Bamherff1u bwydlen_Ebu bwyd_Egu cinio_Egu a_Rhaperth fydd_Bdyf3u ar_Arsym gael_Be yn_Arsym ystod_Ebu dyddiau_Egll 'r_YFB wythnos_Ebu yn_Arsym unig_Anscadu ._Atdt 
Roast_Gwest of_Gwest the_Gwest day_Gwest -_Atdcys £_Gwsym 5.50_Gwdig 
Breaded_Gwest plaice_Gwest ,_Atdcan chips_Gwest &_Gwsym peas_Gwest -_Atdcys £_Gwsym 3.00_Gwdig 
Beef_Gwest stew_Gwest with_Gwest roast_Gwest root_Gwest veg_Gwest -_Atdcys £_Gwsym 5.50_Gwdig 
Chicken_Gwest &_Gwsym pasta_Gwest in_Gwest basil_Gwest sauce_Gwest -_Atdcys £_Gwsym 5.50_Gwdig 
Trio_Gwest of_Gwest pork_Gwest leek_Gwest sausages_Gwest mash_Gwest &_Gwsym gravy_Gwest -_Atdcys £_Gwsym 4.50_Gwdig 
Steamed_Gwest mussels_Gwest with_Gwest garlic_Gwest cream_Gwest -_Atdcys £_Gwsym 4.95_Gwdig 
Roast_Gwest of_Gwest the_Gwest day_Gwest -_Atdcys £_Gwsym 5.50_Gwdig 
Breaded_Gwest plaice_Gwest ,_Atdcan chips_Gwest &_Gwsym peas_Gwest -_Atdcys £_Gwsym 3.00_Gwdig 
Venison_Gwest with_Gwest onion_Gwest gravy_Gwest -_Atdcys £_Gwsym 5.25_Gwdig 
Chicken_Gwest &_Gwsym pasta_Gwest in_Gwest basil_Gwest sauce_Gwest -_Atdcys £_Gwsym 5.50_Gwdig 
Chilli_Gwest and_Gwest rice_Gwest -_Atdcys £_Gwsym 4.95_Gwdig 
Steamed_Gwest mussels_Gwest with_Gwest garlic_Gwest cream_Gwest -_Atdcys £_Gwsym 4.95_Gwdig 
Small_Gwest fish_Gwest ,_Atdcan triple_Gwest cooked_Gwest chips_Gwest &_Gwsym mushy_Gwest peas_Gwest 
-_Atdcys £_Gwsym 6.95_Gwdig 
Ham_Gwest ,_Atdcan egg_Gwest ,_Atdcan chips_Gwest &_Gwsym peas_Gwest -_Atdcys £_Gwsym 3.50_Gwdig 
Vegetarian_Gwest chilli_Gwest and_Gwest rice_Gwest -_Atdcys £_Gwsym 4.95_Gwdig 
Mae_Bpres3u 'r_YFB ddewislen_Ebu amser_Egu cinio_Egu ar_Arsym gael_Be rhwng_Arsym 12_Gwdig y_YFB pnawn_Egu -_Atdcys 6_Gwdig y_YFB pnawn_Egu 
Llun_Egu -_Atdcys Gwener_Ebu 
(_Atdchw Cynnig_Be yn_Arsym dod_Be i_Arsym ben_Egu 09_Gwdig /_Gwsym 02_Gwdig /_Gwsym 2018_Gwdig )_Atdde 
Llogi_Be bwrdd_Egu 
<_Atdchw img_unk /_Gwsym >_Atdde 
<_Atdchw img_unk /_Gwsym >_Atdde 
<_Atdchw img_unk /_Gwsym >_Atdde 
<_Atdchw img_unk /_Gwsym >_Atdde 
<_Atdchw img_unk /_Gwsym >_Atdde 