Crynodeb_Egu Rownd_Ebu a_Cyscyd Rownd_Ebu W_Ebych 51_Gwdig 
Mae_Bpres3u diwrnod_Egu yr_YFB arwerthiant_Egu corachod_Egll wedi_Uberf cyrraedd_Be ,_Atdcan ac_Cyscyd mae_Bpres3u pawb_Rhaamh yn_Arsym mwynhau_Be bwrlwm_Egu a_Cyscyd miri_Egu 'r_YFB Nadolig_Egu ._Atdt 
Yr_YFB unig_Anscadu un_Rhaamh sydd_Bpres3perth ddim_Egu yn_Arsym fodlon_Anscadu ymuno_Be yn_Arsym yr_YFB hwyl_Ebu ydy_Bpres3u David_Gwest ._Atdt 
Mi_Uberf ddaw_Bpres3u hi_Rhapers3bu 'n_Utra amlwg_Anscadu wrth_Arsym i_Arsym 'r_YFB noswaith_Ebu fynd_Be rhagddi_Ar3bu bod_Be ganddo_Ar3gu fo_Adf bethau_Egll eraill_Anscadu ar_Arsym ei_Rhadib3gu feddwl_Be ._Atdt 
Daw_Bpres3u Sophie_Ep i_Arsym wybod_Egu bod_Be Wyn_Egll wedi_Uberf ei_Rhadib3bu thwyllo_Be yn_Arsym y_YFB trafodaethau_Ebll ynglŷn_Arsym â_Arsym phrynu_Be les_Egu y_YFB caffi_Egu ,_Atdcan siop_Ebu a_Cyscyd 'r_YFB salon_Gwest ,_Atdcan ac_Cyscyd er_Arsym iddi_Ar3bu hi_Rhapers3bu wylltio_Be a_Cyscyd thynnu_Be ei_Rhadib3bu chynnig_Be yn_Arsym ôl_Egu ,_Atdcan erbyn_Arsym diwedd_Egu y_YFB dydd_Egu llwydda_Bgorch2u Wyn_Egll a_Cyscyd hithau_Rhacys3bu i_Arsym ddod_Be i_Arsym gytundeb_Egu ._Atdt 
Draw_Adf yn_Arsym nhŷ_Egu Llio_Epb ac_Cyscyd Iolo_Epg ,_Atdcan gwelwn_Bamherff1u Erin_Ep yn_Arsym gweithredu_Be cynllwyn_Egu sinistr_Anscadu iawn_Adf ,_Atdcan sy_Bpres3perth 'n_Utra siŵr_Anscadu o_Arsym gael_Be oblygiadau_Egll difrifol_Anscadu ._Atdt 
Dydy_Bpres3u pethau_Egll ddim_Egu yn_Arsym edrych_Egu yn_Arsym rhy_Adf dda_Anscadu i_Rhapers1u Philip_Epg chwaith_Adf wrth_Arsym iddo_Ar3gu gyrraedd_Be Alwena_Ep yn_Arsym Sbaen_Ep ._Atdt 
Daw_Bpres3u i_Arsym ddeall_Be bod_Be gan_Arsym Alwena_Ep gynlluniau_Egll cwbl_Anscadu wahanol_Anscadu ar_Arsym gyfer_Egu y_YFB dyfodol_Egu ac_Cyscyd mae_Bpres3u hyn_Rhadangd yn_Arsym arwain_Be at_Arsym dor_Bpres3u -_Atdcys calon_Ebu llwyr_Anscadu ._Atdt 
Rownd_Ebu a_Cyscyd Rownd_Ebu 
Mawrth_Egu ac_Cyscyd Iau_Egu 7.30_Gwdig ,_Atdcan S4C_Ebu 
Isdeitlau_Egll Cymraeg_Ebu a_Cyscyd Saesneg_Ebu 
Omnibws_Egu dydd_Egu Sul_Egu gydag_Arsym isdeitlau_Egll Saesneg_Ebu ar_Arsym y_YFB sgrin_Ebu 
Gwefan_Ebu :_Atdcan s4c.cymru_Gwann 
Cynhyrchiad_Egu Rondo_Gwest Media_Gwest ar_Arsym gyfer_Egu S4C_Ebu 