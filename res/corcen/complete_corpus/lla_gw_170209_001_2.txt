[_Atdchw Rhan_Ebu o_Arsym 'r_YFB trawsgrifiad_unk cyntaf_Rhitrefd ]_Atdde ._Atdt 
[_Atdchw Sgwrs_Ebu paralel_Anscadu rhwng_Arsym S_Gwllyth 3_Gwdig a_Cyscyd S_Gwllyth ]_Atdde ._Atdt 
S3_Anon Pa_Bangof ysgol_Ebu aethoch_Bgorff2ll chi_Rhapers2ll ?_Atdt 
S1_Anon ymm_Ebych yng_Arsym lleoliad_Anon ._Atdt 
Yr_YFB ysgol_Ebu Gymraeg_Ebu yng_Arsym lleoliad_Anon a_Cyscyd wedyn_Adf lleoliad_Anon ?_Atdt 
S3_Anon lleoliad_Anon ie_Adf [=]_Gwann Dwi_Bpres1u 'n_Rhadib1ll [/=]_Gwann dwi_Bpres1u 'n_Rhadib1ll '_Atddyf nabod_Be lleoliad_Anon ._Atdt 
Mae_Bpres3u fy_Banmedd1u chwaer_Ebu i_Rhapers1u 'n_Utra byw_Be yn_Arsym [aneglur]_Gwann ._Atdt 
S1_Anon O_Rhapers3gu ok_Gwest ._Atdt 
Ie_Adf ._Atdt 
Ie_Adf ._Atdt 
So_Adf mae_Bpres3u loads_Gwest o_Arsym ysgolion_Ebll Cymraeg_Ebu newydd_Anscadu yn_Arsym yr_YFB ardal_Ebu yna_Ar1u nawr_Egu so_Adf mae_Bpres3u 'n_Utra really_Gwest dda_Anscadu ._Atdt 
ym_Ebych mae_Bpres3u fe_Rhapers3gu really_Gwest 'di_Uberf dod_Be yn_Arsym mwy_Rhaamh poblogaidd_Anscadu ._Atdt 
S3_Anon Ie_Adf ._Atdt 
Ond_Arsym mae_Bpres3u nhw_Rhapers3ll 'n_Uberf mynd_Be i_Rhapers1u codi_Bpres2u ysgol_Ebu newydd_Anscadu yyy_Ebych Cynon_Ep Taf_Ep ._Atdt 
S1_Anon Ie_Adf ._Atdt 
Yn_Arsym ogystal_Anscyf â_Arsym 'r_YFB ysgol_Ebu uwchradd_Anscadu newydd_Anscadu ymm_Ebych oedd_Bamherff3u ar_Arsym bwys_Egu lleoliad_Anon fi_Rhapers1u 'n_Uberf credu_Be wedi_Uberf agor_Be cwpl_Egu o_Rhapers3gu [=]_Gwann flynydde_Ebll [/=]_Gwann flynyddoedd_Ebll yn_Arsym ôl_Egu ._Atdt 
So_Adf ie_Adf mae_Bpres3u 'na_Adf loads_Gwest o_Rhapers3gu ymm_Ebych loads_Gwest mwy_Rhaamh o_Arsym ddiddordeb_Egu nawr_Adf gyda_Arsym pobl_Ebu yn_Arsym anfon_Be 'i_Rhadib3bu plant_Egll so_Adf mae_Bpres3u hwnna_Rhadangg 'n_Utra really_Gwest good_Gwest [saib]_Gwann [=]_Gwann Oes_Bpres3amhen [/=]_Gwann oes_Bpres3amhen plant_Egll '_Atddyf da_Anscadu ti_Rhapers2u ?_Atdt 
S3_Anon Ie_Adf mae_Bpres3u yyy_Ebych pedwar_Rhifol i_Arsym gyd_Egu ond_Arsym mae_Bpres3u dau_Rhifol bach_Anscadu gyda_Arsym fi_Rhapers1u [=]_Gwann maen_Bpres3ll nhw_Rhapers3ll 'n_Arsym [/=]_Gwann maen_Bpres3ll nhw_Rhapers3ll 'n_Utra byw_Be gyda_Arsym fy_Rhadib1u nghyn_Arsym wraig_Ebu i_Arsym o_Arsym 'r_YFB [aneglur]_Gwann ac_Cyscyd mae_Bpres3u dwy_Rhifol ferch_Ebu gyda_Arsym fi_Rhapers1u hefyd_Adf [saib]_Gwann ._Atdt 
[_Atdchw Sgwrsio_Be paralel_Anscadu yn_Arsym dod_Be i_Arsym ben_Egu ]_Atdde ._Atdt 
[_Atdchw Rhan_Ebu o_Arsym 'r_YFB trawsgrifiad_unk cyntaf_Rhitrefd ]_Atdde ._Atdt 
[_Atdchw Sgwrs_Ebu paralel_Anscadu rhwng_Arsym S_Gwllyth 1_Gwdig a_Cyscyd S_Gwllyth ]_Atdde ._Atdt 
S3_Anon Dyw_Bpres3u hi_Rhapers3bu dim_Adf yn_Arsym dda_Anscadu iawn_Adf yn_Arsym dda_Anscadu iawn_Adf yn_Arsym y_YFB ysgol_Ebu ._Atdt 
Mae_Bpres3u [=]_Gwann traff_unk [/=]_Gwann ymm_Ebych mae_Bpres3u 'n_Utra anodd_Anscadu iddi_Ar3bu siarad_Be yn_Arsym Saesneg_Ebu gyda_Arsym 'r_YFB [-]_Gwann 
S1_Anon O_Rhapers3gu ok_Gwest ._Atdt 
Ie_Adf ._Atdt 
Yn_Arsym enwedig_Anscadu efalle_Adf os_Egu mae_Bpres3u nhw_Rhapers3ll wedi_Uberf symud_Be rhwng_Arsym ysgolion_Ebll ._Atdt 
S3_Anon Ie_Adf [saib]_Gwann ._Atdt 
Mae_Bpres3u ferch_Ebu yyy_Ebych ifancach_Anscym yn_Arsym dda_Anscadu iawn_Adf [=]_Gwann mae_Bpres3u [/=]_Gwann mae_Bpres3u 'n_Rhadib1ll [aneglur]_Gwann does_Bpres3amhen dim_Egu problems_Gwest gyda_Arsym hi_Rhapers3bu ond_Arsym mae_Bpres3u problems_Gwest gyda_Arsym [aneglur]_Gwann gweddill_Egu dysgu_Be ._Atdt 
S1_Anon Mae_Bpres3u ffrind_Egu gorau_Anseith fi_Rhapers1u 'n_Utra byw_Be ym_Ebych lleoliad_Anon a_Cyscyd mae_Bpres3u hi_Rhapers3bu eisiau_Egu symud_Be adref_Adf a_Cyscyd mae_Bpres3u hi_Rhapers3bu aethon_Bgorff1ll ni_Rhapers1ll i_Arsym 'r_YFB ysgol_Ebu gyda_Arsym 'n_Utra gilydd_Egu so_Adf mae_Bpres3u hi_Rhapers3bu 'n_Uberf siarad_Be Cymraeg_Ebu a_Cyscyd ni_Rhapers1ll 'n_Uberf siarad_Be Cymraeg_Ebu gyda_Arsym 'n_Utra gilydd_Egu tua_Arsym hanner_Egu yr_YFB amser_Egu ._Atdt 
[_Atdchw Sgwrs_Ebu paralel_Anscadu yn_Arsym dod_Be i_Arsym ben_Egu ]_Atdde ._Atdt 
[_Atdchw Rhan_Ebu o_Arsym 'r_YFB trawsgrifiad_unk cyntaf_Rhitrefd ]_Atdde ._Atdt 
2598.870_Gwdig 