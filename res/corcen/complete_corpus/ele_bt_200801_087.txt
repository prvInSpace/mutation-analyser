"_Atddyf *_Gwsym *_Gwsym *_Gwsym Late_Egbu Change_Egbll S4C_Ebu -_Atdcys Newid_Be Hwyr_Egu S4C_Ebu *_Gwsym *_Gwsym *_Gwsym -_Atdcys Wythnos_Ebu 26_Gwdig &_Gwsym Wythnos_Ebu 27_Gwdig "_Atddyf ,_Atdcan "_Atddyf *_Gwsym *_Gwsym Late_Egbu Change_Egbll S4C_Ebu -_Atdcys Newid_Be Hwyr_Egu S4C_Ebu *_Gwsym *_Gwsym 
Wythnos_Ebu 26_Gwdig 
22_Gwdig /_Gwsym 6_Gwdig /_Gwsym 20_Gwdig Dydd_Egu Llun_Egu 
22_Gwdig :_Atdcan 00_Gwdig Dim_Adf Newid_Be DRYCH_Egu 
23_Gwdig :_Atdcan 05_Gwdig Amser_Egu Newydd_Anscadu Y_YFB Ditectif_Egu 
23_Gwdig :_Atdcan 40_Gwdig Amser_Egu Newydd_Anscadu Diwedd_Egu 
22_Gwdig :_Atdcan 00_Gwdig 
DRYCH_Egu :_Atdcan Babis_Egu Covid_Ep ,_Atdcan Babis_Egu Gobaith_Egu (_Atdchw R_Gwllyth )_Atdde (_Atdchw S_Gwllyth )_Atdde (_Atdchw SC_Gwacr )_Atdde (_Atdchw HD_Gwacr )_Atdde 
Yn_Arsym y_YFB rhaglen_Ebu ,_Atdcan Babis_Egu Covid_Ep :_Atdcan Babis_Egu Gobaith_Egu ,_Atdcan byddwn_Bdyf1ll yn_Arsym dilyn_Be 6_Gwdig cwpwl_Egu sydd_Bpres3perth ar_Arsym fin_Egu rhoi_Be genedigaeth_Ebu i_Arsym blentyn_Egu ._Atdt 
Mi_Uberf fydd_Bdyf3u y_YFB 6_Gwdig cwpwl_Egu yn_Arsym ffilmio_Be eu_Rhadib3ll hunain_Rhaatb wrth_Arsym iddyn_Ar3ll nhw_Rhapers3ll wynebu_Be heriau_Ebll gwahanol_Anscadu yn_Arsym ystod_Ebu cyfnod_Egu COVID_Gwacr -_Atdcys 19_Gwdig ._Atdt 
Mae_Bpres3u problemau_Egbll gwahanol_Anscadu i_Arsym 'r_YFB arfer_Egu yn_Arsym codi_Bpres2u cyn_Arsym y_YFB genedigaeth_Ebu ,_Atdcan yn_Arsym ystod_Ebu ac_Cyscyd ar_Arsym ol_Anscadu ._Atdt 
23_Gwdig :_Atdcan 05_Gwdig 
Y_YFB DITECTIF_Egu :_Atdcan Achos_Egu Connor_Ep Marshall_Ep (_Atdchw R_Gwllyth )_Atdde (_Atdchw S_Gwllyth )_Atdde (_Atdchw SC_Gwacr )_Atdde (_Atdchw AD_Gwacr )_Atdde (_Atdchw HD_Gwacr )_Atdde 
Bu_Bgorff3u farw_Anscadu Connor_Ep Marshall_Ep ar_Arsym ôl_Anscadu ymosodiad_Egu treisgar_Anscadu ym_Arsym Mhorthcawl_Ep yn_Arsym 2015_Gwdig ._Atdt 
Ond_Arsym sut_Egbu y_Rhadib1u gwnaeth_Bgorff3u yr_YFB heddlu_Egu ddal_Be y_YFB llofrudd_Egu ?_Atdt 
Mae_Bpres3u Ditectifs_Egll De_Egu Cymru_Epb yn_Arsym datgelu_Be eu_Banmedd3ll tystiolaeth_Ebu fforensig_Anscadu a_Cyscyd 'u_Banmedd3ll cyfweliadau_Egll gyda_Arsym 'r_YFB llofrudd_Egu ._Atdt 
A_Cyscyd bydd_Bdyf3u Mali_Bpres2u Harries_Ep yn_Arsym cyfweld_Be â_Arsym 'r_YFB fam_Ebu ,_Atdcan Nadine_Ep Marshall_Ep ,_Atdcan sydd_Bpres3perth yn_Arsym ymgyrchu_Be er_Arsym cof_Egu am_Gwtalf ei_Banmedd3gu mab_Egu ._Atdt 
23_Gwdig :_Atdcan 40_Gwdig 
DIWEDD_Egu 
Mercher_Ebu 24_Gwdig /_Gwsym 6_Gwdig /_Gwsym 20_Gwdig Dydd_Egu Mercher_Ebu 
21_Gwdig :_Atdcan 30_Gwdig Dim_Adf Newid_Be Clwb_Egu Rygbi_Egu 
22_Gwdig :_Atdcan 35_Gwdig Amser_Egu Newydd_Anscadu Rhaglen_Ebu Deledu_Egu Gareth_Epg 
23_Gwdig :_Atdcan 05_Gwdig Amser_Egu Newydd_Anscadu Wil_Ebu ac_Cyscyd Aeron_Ep 
23_Gwdig :_Atdcan 40_Gwdig Amser_Egu Newydd_Anscadu Diwedd_Egu 
22_Gwdig :_Atdcan 35_Gwdig 
RHAGLEN_Ebu DELEDU_Be GARETH_Gwacr :_Atdcan Chwaraeon_Egll (_Atdchw R_Gwllyth )_Atdde (_Atdchw S_Gwllyth )_Atdde (_Atdchw AD_Gwacr )_Atdde (_Atdchw HD_Gwacr )_Atdde 
Mae_Bpres3u Gareth_Epg am_Arsym wisgo_Be ei_Rhadib3bu esgidiaau_unk rhedeg_Be yr_YFB wythnos_Ebu yma_Adf oherwydd_Cyscyd thema_Ebu 'r_YFB rhaglen_Ebu ydi_Bpres3u chwaraeon_Egll !_Atdt Yn_Arsym ogystal_Anscyf a_Cyscyd chael_Be gwersi_Ebll bocsio_Be a_Cyscyd sgwrsio_Be gyda_Arsym seren_Ebu tim_Egu rygbi_Egu Cymru_Epb ,_Atdcan bydd_Bdyf3u Gareth_Epg yn_Arsym trafod_Egu y_YFB bêl_Ebu gron_Anscadbu gyda_Arsym un_Rhaamh o_Rhapers3gu wybnebau_unk mwyaf_Anseith cyfarwydd_Anscadu Cymru_Epb ._Atdt Hyn_Rhadangd oll_Anscadu i_Arsym gyd_Egu ,_Atdcan a_Cyscyd chân_Bgorch2u i_Arsym orffen_Egu ,_Atdcan wrth_Arsym gwrs_Egu !_Atdt 
23_Gwdig :_Atdcan 05_Gwdig 
WIL_Ebu AC_Cyscyd AERON_Gwacr :_Atdcan TAITH_Ebu RWMANIA_Gwacr :_Atdcan Bugeiliaid_Egll (_Atdchw R_Gwllyth )_Atdde (_Atdchw S_Gwllyth )_Atdde (_Atdchw SC_Gwacr )_Atdde (_Atdchw HD_Gwacr )_Atdde 
Yn_Arsym rhaglen_Ebu ola_Anscadu 'r_YFB gyfres_Ebu ,_Atdcan mae_Bpres3u Wil_Ebu ac_Cyscyd Aeron_Ep yn_Arsym cyd-_Ublaen fyw_Be â_Arsym theulu_Egu o_Arsym fugeiliaid_Egll cyntefig_Anscadu yng_Arsym nghanol_Egu mynyddoedd_Egll y_YFB Maramures_Ep ._Atdt 
Ond_Arsym gyda_Arsym storm_Ebu fwya_Anseith 'r_YFB degawd_Egu newydd_Anscadu ruo_Be drwy_Arsym 'r_YFB wlad_Ebu mae_Bpres3u Wil_Ebu ac_Cyscyd Aeron_Ep yn_Arsym cael_Be profiad_Egu uniongyrchol_Anscadu o_Arsym amodau_Egbll bywyd_Egu anodd_Anscadu y_YFB bugail_Egu ._Atdt 
23_Gwdig :_Atdcan 40_Gwdig 
DIWEDD_Egu 
Wythnos_Ebu 27_Gwdig 
28_Gwdig /_Gwsym 6_Gwdig /_Gwsym 20_Gwdig Dydd_Egu Sul_Egu 
22_Gwdig :_Atdcan 00_Gwdig Dileu_Be Lowri_Ep Morgan_Epg :_Atdcan Her_Arsym 333_Gwdig 
22_Gwdig :_Atdcan 00_Gwdig Rhaglen_Ebu Newydd_Anscadu Ras_Ebu yn_Arsym Erbyn_Be Amser_Egu Ep_Gwest 4_Gwdig I_Rhapers1u 24504_Gwdig /_Gwsym 004_Gwdig Z_Gwllyth 69741_Gwdig 
23_Gwdig :_Atdcan 00_Gwdig Amser_Egu Newydd_Anscadu Y_YFB Fets_Egll 
00_Gwdig :_Atdcan 05_Gwdig Amser_Egu Newydd_Anscadu Diwedd_Egu 
22_Gwdig :_Atdcan 00_Gwdig 
RAS_Ebu YN_Arsym ERBYN_Be AMSER_Egu (_Atdchw R_Gwllyth )_Atdde (_Atdchw S_Gwllyth )_Atdde (_Atdchw HD_Gwacr )_Atdde 
Yn_Arsym y_YFB rhaglen_Ebu hon_Rhadangb ,_Atdcan cawn_Bamherff1u weld_Be Lowri_Ep Morgan_Epg yn_Arsym dechrau_Egu 'r_YFB sialens_Ebu o_Arsym redeg_Be Marathon_Ebu y_YFB Jwngl_Ep ._Atdt 
Bydd_Bdyf3u yn_Arsym rhaid_Egu iddi_Ar3bu redeg_Be dros_Arsym 222_Gwdig cilomedr_Egu dros_Arsym gyfnod_Egu o_Arsym wythnos_Ebu gan_Arsym gario_Be 'r_YFB holl_Bancynnar offer_Egll sydd_Bpres3perth angen_Egu arni_Ar3bu ar_Arsym ei_Rhadib3bu chefn_Egu ._Atdt 
Mae_Bpres3u 'r_YFB ras_Ebu mor_Adf galed_Anscadu ,_Atdcan dim_Egu ond_Arsym hanner_Egu y_YFB cystadleuwyr_Egll sy_Bpres3perth 'n_Uberf llwyddo_Be i_Arsym groesi_Be 'r_YFB llinell_Ebu derfyn_Egu ._Atdt 
Beth_Rhagof fydd_Bdyf3u tynged_Egbu Lowri_Epb ?_Atdt 
22_Gwdig :_Atdcan 28_Gwdig 
Y_YFB TYWYDD_Egu (_Atdchw HD_Gwacr )_Atdde 
23_Gwdig :_Atdcan 00_Gwdig 
Y_YFB FETS_Egll (_Atdchw R_Gwllyth )_Atdde (_Atdchw S_Gwllyth )_Atdde (_Atdchw SC_Gwacr )_Atdde (_Atdchw AD_Gwacr )_Atdde (_Atdchw HD_Gwacr )_Atdde 
Y_YFB tro_Egu yma_Adf ar_Arsym y_YFB Fets_Egll mae_Bpres3u pob_Egll math_Ebu o_Arsym anifeiliaid_Egll angen_Egu ei_Rhadib3bu thrin_Be ._Atdt 
Mae_Bpres3u un_Rhaamh o_Arsym brif_Anseith bartneriaid_Ebll y_YFB practis_Egu Hywel_Anscadu yn_Arsym brysur_Anscadu yn_Arsym y_YFB sied_Egu wyna_Be yn_Arsym helpu_Be rhoi_Be genedigaeth_Ebu i_Arsym dri_Rhifol oen_Egu bach_Anscadu ._Atdt Ac_Cyscyd mae_Bpres3u theatr_Ebu y_YFB practis_Egu yn_Arsym brysur_Anscadu gyda_Arsym llawdriniaethau_Ebll mawr_Anscadu i_Rhapers1u Pabi_Egbu 'r_YFB ci_Egu a_Cyscyd Bella_Anseith 'r_YFB gwningen_Ebu ._Atdt 
23_Gwdig :_Atdcan 33_Gwdig 
Y_YFB TYWYDD_Egu (_Atdchw HD_Gwacr )_Atdde 
00_Gwdig :_Atdcan 05_Gwdig 
DIWEDD_Egu 