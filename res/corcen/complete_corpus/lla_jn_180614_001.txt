S1_Anon Pryn'awn_Gwest da_Anscadu i_Rhapers1u chi_Rhapers2ll a_Cyscyd chroeso_Egu i_Arsym hanner_Egu awr_Ebu arall_Anscadu yng_Arsym nghwmni_Egu criw_Egu enw_rhaglen_Anon ._Atdt 
Fi_Rhapers1u enwb_Anon cyfenw_Anon sy_Bpres3perth 'n_Utra y_YFB gadair_Ebu p'nawn_Egu 'ma_Adf ._Atdt 
Ac_Cyscyd yn_Arsym cadw_Be cwmni_Egu i_Arsym fi_Rhapers1u yn_Arsym y_YFB stiwdio_Ebu yma_Adf lleoliad_Anon ma_Bpres3u '_Atddyf enwb_Anon _cyfenw__Anon sy_Bpres3perth 'n_Utra gyfarwyddwr_Egu marchnata_Be i_Rhapers1u __sefydliad___Anon ._Atdt 
Draw_Adf yn_Arsym lleoliad_Anon ma_Bpres3u '_Atddyf enwb_Anon _cyfenw__Anon sy_Bpres3perth 'n_Rhadib1ll flogwraig_unk ac_Cyscyd yn_Arsym awdur_Egu ._Atdt 
Ag_Arsym yn_Arsym ein_Banmedd1ll stiwdio_Ebu ni_Rhapers1ll ym_Ebych lleoliad_Anon ma_Bpres3u '_Atddyf un_Rhaamh o_Arsym selogion_Egll y_YFB rhaglen_Ebu 'ma_Adf sef_Cyscyd enwb_Anon _cyfenw__Anon [anadlu_i_mewn_yn_sydyn]_Gwann ._Atdt 
A_Rhaperth ry_Bpres3u 'n_Arsym ni_Rhapers1ll am_Gwtalf ddechre_Be '_Atddyf heddi_Adf '_Atddyf gyda_Arsym noson_Ebu wobrwyo_Be arbennig_Anscadu a_Rhaperth gynhaliwyd_Bgorffamhers yng_Rhadib1u sefydliad_Anon yng_Arsym lleoliad_Anon r'yw_Egbu w'thnos_unk yn_Arsym ol_Egu sef_Cyscyd gwobre_Egbll _sefydliad__Anon yn_Arsym cydnabod_Be cyfraniad_Egu mewnywod_unk o_Arsym bob_Egll agwedd_Ebu o_Arsym fywyd_Egu yng_Arsym Nghymru_Epb [anadlu_i_mewn_yn_sydyn]_Gwann __enwb___Anon o'dd_Bamherff3u ___sefydliad____Anon yn_Arsym un_Rhaamh o_Arsym noddwyr_Egll y_YFB noson_Ebu ac_Cyscyd mi_Uberf oeddet_Bamherff2u ti_Rhapers2u yno_Adf ._Atdt 
Esbonia_Bgorch2u '_Atddyf chydig_Banmeint mwy_Rhaamh am_Arsym y_YFB gwobre_Egbll 'ma_Adf '_Atddyf te_Bpres3u ._Atdt 
S2_Anon [anadlu_sydyn_i_mewn]_Gwann ._Atdt 
Wel_Ebych dathliad_Egu yw_Bpres3u 'r_YFB gwobre_Egbll 'ma_Adf [=]_Gwann mewn_Arsym [/=]_Gwann mewn_Arsym gwirionedd_Egu ._Atdt 
Dathliad_Egu o_Arsym 'r_YFB cyfraniad_Egu ma_Bpres3u '_Atddyf menywod_Egll yn_Arsym '_Atddyf neud_Be ar_Arsym draws_Arsym Cymru_Epb ._Atdt 
ym_Ebych ._Atdt 
A_Cyscyd trio_Be annog_Egu y_YFB genhedlaeth_Ebu nesaf_Anseith o_Arsym fenywod_Egll [=]_Gwann i_Rhapers1u [/=]_Gwann i_Arsym ddod_Be trwyddo_Ar3gu ._Atdt 
S1_Anon Felly_Adf o't_unk ti_Rhapers2u enwb_Anon hefyd_Adf yno_Adf ond_Arsym am_Arsym reswm_Egu arbennig_Anscadu ._Atdt 
Yn_Arsym seren_Ebu ddisglair_Anscadu ._Atdt 
S3_Anon [chwerthin]_Gwann ._Atdt 
S1_Anon D'wed_Bgorch2u '_Atddyf tho_Egu '_Atddyf ni_Rhapers1ll ._Atdt 
<_Atdchw SB_Gwacr >_Atdde [chwerthin]_Gwann ._Atdt 
S3_Anon [chwerthin]_Gwann ._Atdt 
Ia_Adf ._Atdt 
Dio_Bpres3u 'ch_Rhadib2ll ._Atdt 
'_Atddyf Dw_Bpres1u 'i_Rhadib3bu 'm_Adf yn_Arsym siŵr_Anscadu am_Arsym hynna_Anscadu ._Atdt 
ym_Ebych ._Atdt 
Ia_Adf ._Atdt 
Wel_Ebych o'n_Bamherff1u i_Rhapers1u 'n_Arsym un_Rhaamh o_Arsym 'r_YFB pedwar_Rhifol deg_Rhifol pedwar_Rhifol o_Arsym fenywod_Egll o'dd_Bamherff3u wedi_Arsym ca'l_Ebu 'i_Rhadib3bu henwebu_Be ymm_Ebych a_Cyscyd wedi_Arsym cyrr'edd_unk y_YFB rhestr_Ebu fer_Anscadbu ymm_Ebych yng_Arsym ngwobre_Egbll sefydliad_Anon ._Atdt 
'_Atddyf Oeddan_Bamherff1ll nhw_Rhapers3ll 'di_Arsym ca'l_Ebu dros_Arsym pedwar_Rhifol cant_Egu '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be o_Arsym ymm_Ebych enwebiada_Egll i_Arsym gyd_Egu a_Cyscyd wedi_Uberf dewis_Egu pedwar_Rhifol i_Rhapers1u ymm_Ebych gynrychioli_Be unarddeg_Egu categori_Egu gwahanol_Anscadu ._Atdt 
Ag_Arsym ia_Egu o'n_Bamherff1u i_Rhapers1u yn_Arsym un_Rhaamh o_Arsym 'r_YFB rhai_Rhaamh o'dd_Bamherff3u yn_Arsym y_YFB rhestr_Ebu fer_Anscadbu ar_Arsym gyfer_Egu Seren_Ebu Ddisglair_Anscadu ._Atdt 
S1_Anon So_Adf beth_Rhagof o'dd_Bamherff3u Seren_Ebu Ddisglair_Anscadu '_Atddyf te_Bpres3u ?_Atdt 
Achos_Cyscyd '_Atddyf wi_Bpres1u 'n_Uberf gw'bod_Be o'dd_Bamherff3u 'na_Adf gategorie_Egll ti_Rhapers2u 'n_Arsym '_Atddyf bod_Be fel_Cyscyd ym_Arsym maes_Egu adeiladu_Be a_Cyscyd busnes_Egbu a_Arsym ymm_Ebych ymm_Ebych ti_Rhapers2u 'n_Arsym '_Atddyf bod_Be yr_YFB adran_Ebu greadigol_Anscadu ._Atdt 
Beth_Rhagof yn_Arsym union_Anscadu yw_Bpres3u Seren_Ebu Ddisglair_Anscadu a_Cyscyd 'r_YFB categori_Egu yma_Adf ?_Atdt 
S3_Anon Wel_Ebych y_YFB Seren_Ebu Ddisglair_Anscadu ydy_Bpres3u categori_Egu ar_Arsym gyfer_Egu merched_Ebll sydd_Bpres3perth wedi_Uberf dechre_Be '_Atddyf gyrfa_Ebu ne_Anscadu '_Atddyf prosiect_Egu newydd_Anscadu ymm_Ebych yn_Arsym [-]_Gwann wel_Ebych yn_Arsym gymharol_Anscadu ddiweddar_Anscadu felly_Adf ._Atdt 
ym_Ebych ._Atdt 
Felly_Adf '_Atddyf nes_Bgorff1u i_Rhapers1u fynd_Be ati_Ar3bu i_Arsym sefydlu_Be blog_Egu o_Arsym 'r_YFB enw_Egu sefydliad_Anon [clecian_gwefusau]_Gwann ymm_Ebych bron_Ebu i_Arsym flwyddyn_Ebu yn_Arsym ol_Egu '_Atddyf wan_Adf ._Atdt 
A_Cyscyd hefyd_Adf '_Atddyf dw_Bpres1u i_Rhapers1u 'di_Arsym '_Atddyf sgwennu_Be llyfyr_unk ymm_Ebych '_Atddyf y_YFB llyfr_Egu cynta_Rhitrefd '_Atddyf fi_Rhapers1u o_Arsym 'r_YFB enw_Egu llyfr_Anon ._Atdt 
Llyfr_Egu i_Rhapers1u fame_Ebll ._Atdt 
A_Cyscyd wedyn_Adf ca'l_Ebu '_Atddyf y_YFB enwebu_Be '_Atddyf nas_Rhaperth i_Rhapers1u oherwydd_Cyscyd y_YFB cyfraniad_Egu a_Cyscyd 'r_YFB gefnog'eth_unk '_Atddyf dw_Bpres1u i_Rhapers1u 'di_Uberf r'oid_Bamherffamhers i_Rhapers1u fame_Ebll dros_Arsym Gymru_Epb dros_Arsym y_YFB flwyddyn_Ebu d'wytha_Anscadu '_Atddyf ._Atdt 
S1_Anon So_Adf ydy_Bpres3u hwn_Rhadangg yn_Arsym r'yw_Egbu fath_Banhwyr o_Rhapers3gu Mumsnet_Gwest ti_Rhapers2u 'n_Arsym '_Atddyf bod_Be ?_Atdt 
S3_Anon Na_Rhaperth ._Atdt 
'_Atddyf D'ydy_Bpres3u o_Arsym ddim_Egu yn_Arsym Mumsnet_Gwest i_Arsym bob_Egll pwrpas_Egu ._Atdt 
Ma_Bpres3u '_Atddyf sefydliad_Anon yn_Arsym ffocysu_Be 'n_Utra bennaf_Egu ar_Arsym brofiade_Egll rhieni_Egll yng_Arsym Nghymru_Epb ag_Arsym yn_Arsym ffocysu_Be ar_Arsym Gymru_Epb a_Cyscyd 'r_YFB profiad_Egu o_Arsym fagu_Be plant_Egll yng_Arsym Nghymru_Epb ._Atdt 
A_Cyscyd mae_Bpres3u o_Rhapers3gu hefyd_Adf yn_Arsym ddwy_Rhifol -_Atdcys ieithog_Anscadu ._Atdt 
Felly_Adf hwn_Rhadangg ydy_Bpres3u 'r_YFB blog_Egu cynta_Rhitrefd '_Atddyf dwy_Rhifol -_Atdcys ieithog_Anscadu ar_Arsym gyfer_Egu ym_Arsym mame_Ebll a_Cyscyd rhieni_Egll Cymru_Epb wedyn_Adf ._Atdt 
[_Atdchw DIWEDD_Egu Y_YFB CLIP_Egu CYNTAF_Anscadu ]_Atdde ._Atdt 
S1_Anon Ma_Bpres3u '_Atddyf rhaid_Egu fi_Rhapers1u '_Atddyf weud_Be enwb_Anon +_Gwsym 
S4_Anon Hmm_Ebych ._Atdt 
S1_Anon +_Gwsym '_Atddyf dw_Bpres1u i_Rhapers1u 'di_Arsym ca'l_Ebu llond_Egu bola_Egu erbyn_Arsym hyn_Rhadangd o_Arsym 'r_YFB holl_Bancynnar nosweithie_Ebll gwobrwyo_Be 'ma_Adf ._Atdt 
Ym_Arsym maes_Egu busnes_Egbu ym_Arsym maes_Egu chwaraeon_Egll +_Gwsym 
S4_Anon [chwerthin]_Gwann ._Atdt 
S1_Anon +_Gwsym ym_Arsym maes_Egu bwyd_Egu a_Cyscyd diod_Ebu a_Arsym mentergarwch_unk ._Atdt 
Be_Rhagof '_Atddyf ti_Rhapers2u feddwl_Be enwb_Anon ?_Atdt 
S4_Anon ym_Ebych ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be bod_Be 'n_Utra rheswm_Egu neis_Anscadu i_Arsym roid_Be ffrog_Ebu a_Cyscyd ti_Rhapers2u 'm_Adf bach_Egu o_Arsym fec_Egu -_Atdcys yp_Gwtalf on_Gwest +_Gwsym 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S4_Anon +_Gwsym ond_Arsym ymm_Ebych [-]_Gwann ._Atdt 
Ond_Arsym na_Rhaperth ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be bod_Be o'n_Bamherff1u gret_Adf bo_Adf '_Atddyf 'na_Bandangd wobra_Egbll sefydliad_Anon achos_Egu '_Atddyf dw_Bpres1u i_Rhapers1u dal_Be yn_Arsym meddwl_Egu bo_Adf '_Atddyf 'na_Adf gap_Gwest yn_Arsym ti_Rhapers2u 'n_Uberf gw'bod_Be y_YFB gender_Gwest gap_Gwest mawr_Anscadu ma_Bpres3u '_Atddyf nhw_Rhapers3ll 'n_Uberf son_Be am_Arsym ._Atdt 
So_Adf '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be bod_Be o'n_Bamherff1u gret_Adf ._Atdt 
Ond_Arsym ymm_Ebych '_Atddyf dw_Bpres1u i_Rhapers1u yn_Arsym [-]_Gwann '_Atddyf dw_Bpres1u i_Rhapers1u am_Arsym ofyn_Be [=]_Gwann i_Arsym 'r_YFB [/=]_Gwann i_Arsym 'r_YFB ddwy_Rhifol o'dd_Bamherff3u yna_Adf actually_Gwest '_Atddyf dach_Bpres2ll chi_Rhapers2ll 'n_Uberf teimlo_Be bod_Be o_Arsym wedi_Adf ca'l_Ebu 'i_Rhadib3bu hysbysebu_Be achos_Egu '_Atddyf d_Gwllyth '_Atddyf o_Rhapers3gu 'n_Arsym i_Arsym ddim_Egu 'di_Arsym cl'wad_unk lot_Rhaamh amdano_Ar3gu fo_Adf +_Gwsym 
S3_Anon Wel_Ebych [-]_Gwann ._Atdt 
S4_Anon +_Gwsym a_Cyscyd bo_Adf '_Atddyf ni_Rhapers1ll 'n_Uberf d'eud_Be +_Gwsym 
<_Atdchw SB_Gwacr >_Atdde Hmm_Ebych ._Atdt 
S4_Anon +_Gwsym bo_Adf '_Atddyf ni_Rhapers1ll rili_Adf '_Atddyf isio_Be '_Atddyf ti_Rhapers2u '_Atddyf bod_Be ca'l_Ebu ym_Arsym merched_Ebll [=]_Gwann i_Rhapers1u [/=]_Gwann i_Rhapers1u ddechra_Be '_Atddyf busnesa_Be ne_Egu '_Atddyf to_Gwest push_Gwest the_Gwest boat_Gwest out_Gwest a_Cyscyd '_Atddyf '_Atddyf ballu_Be ._Atdt 
O'n_Bamherff1u i_Rhapers1u 'n_Uberf teimlo_Be e'lla_Adf '_Atddyf o'dd_Bamherff3u o_Rhapers3gu 'di_Arsym ca'l_Ebu 'i_Rhadib3bu hysbysebu_Be ti_Rhapers2u 'n_Uberf meddwl_Be ne_Egu '_Atddyf [-]_Gwann ?_Atdt 
S3_Anon Wel_Ebych '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf teimlo_Be fel_Cyscyd ymm_Ebych pan_Egu '_Atddyf nes_Bgorff1u i_Rhapers1u ddechre_Be '_Atddyf sefydliad_Anon llynedd_Adf '_Atddyf nes_Anscym [-]_Gwann dyna_Adf pryd_Egu '_Atddyf nes_Bgorff1u i_Rhapers1u glywed_Be am_Gwtalf gwobre_Egbll sefydliad_Anon ._Atdt 
O'n_Bamherff1u i_Rhapers1u '_Atddyf rioed_Adf wedi_Uberf clywed_Be amdano_Ar3gu fo_Adf o_Arsym +_Gwsym 
S4_Anon Na_Rhaperth ._Atdt 
S3_Anon +_Gwsym gwbwl_Anscadu ._Atdt 
Ond_Arsym sefydliad_Anon sydd_Bpres3perth yn_Arsym rhedeg_Be o_Rhapers3gu ._Atdt 
Hwn_Rhadangg 'di_Arsym 'r_YFB drydedd_Rhitrefd gwaith_Ebu ma_Bpres3u '_Atddyf 'di_Arsym ca'l_Ebu 'i_Rhadib3gu redeg_Be '_Atddyf wan_Adf a_Cyscyd mae_Bpres3u o_Rhapers3gu yn_Arsym tyfu_Be ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be bod_Be +_Gwsym 
<_Atdchw SB_Gwacr >_Atdde Hmm_Ebych ._Atdt 
S3_Anon +_Gwsym yr_YFB mwyafrif_Egu o_Arsym enwebiade_Egll a_Cyscyd 'r_YFB ymm_Ebych digwyddiad_Egu wedi_Uberf bod_Be yn_Arsym eitha_Adf '_Atddyf canolog_Anscadu yng_Arsym lleoliad_Anon ._Atdt 
Ond_Arsym '_Atddyf wan_Adf mae_Bpres3u o'n_Bamherff1u tyfu_Be achos_Egu ar_Arsym y_YFB noson_Ebu o'dd_Bamherff3u 'na_Adf ferched_Ebll yna_Adf o_Arsym bob_Egll ban_Anscadu ._Atdt 
Ag_Arsym [=]_Gwann o'n_Bamherff1u i_Rhapers1u 'n_Arsym [/=]_Gwann o'n_Bamherff1u i_Rhapers1u 'n_Utra falch_Anscadu iawn_Adf ma_Bpres3u '_Atddyf nid_Uneg fi_Uberf o'dd_Bamherff3u yr_YFB unig_Anscadu un_Rhaamh ar_Arsym y_YFB rhestr_Ebu fer_Anscadbu o'n_Bamherff1u dod_Be o_Rhapers3gu lleoliad_Anon ._Atdt 
So_Adf ma_Bpres3u '_Atddyf +_Gwsym 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S3_Anon +_Gwsym hynna_Anscadu 'n_Utra dipyn_Egu o_Arsym gamp_Ebu ._Atdt 
ym_Ebych ._Atdt 
Ond_Arsym ie_Adf ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'm_Adf yn_Arsym gw'bod_Be sut_Egbu oeddet_Bamherff2u ti_Rhapers2u 'n_Uberf teimlo_Be enwb_Anon ?_Atdt 
ym_Ebych ._Atdt 
Sut_Rhagof '_Atddyf na'thoch_Bgorff2ll chi_Rhapers2ll ddod_Be ar_Arsym draws_Arsym o_Rhapers3gu [=]_Gwann fel_Cyscyd [/=]_Gwann fel_Cyscyd noddwyr_Egll wedyn_Adf ?_Atdt 
S2_Anon [anadlu_i_mewn_yn_sydyn]_Gwann ._Atdt 
Wel_Ebych '_Atddyf dan_Bpres1ll ni_Rhapers1ll 'n_Uberf gweithio_Be fel_Cyscyd cwmni_Egu gyda_Arsym sefydliad_Anon nawr_Adf i_Arsym fynd_Be ar_Arsym ol_Anscadu +_Gwsym 
S3_Anon Ah_Ebych OK_Gwest ._Atdt 
S2_Anon +_Gwsym ymm_Ebych safon_Ebu ymm_Ebych cyflogwr_Egu [=]_Gwann ma_Bpres3u '_Atddyf [/=]_Gwann ma_Bpres3u '_Atddyf sefydliad_Anon [=]_Gwann yn_Arsym [/=]_Gwann yn_Arsym cynnig_Egu ymm_Ebych o_Arsym ran_Ebu cydraddoldeb_Egu ._Atdt 
ym_Ebych ._Atdt 
Felly_Adf dyna_Adf sut_Egbu '_Atddyf naethon_Bgorff3ll ni_Rhapers1ll ddod_Be ar_Arsym draws_Arsym [=]_Gwann y_YFB [/=]_Gwann y_YFB '_Atddyf wobre_Egbll ._Atdt 
ym_Ebych ._Atdt 
Ond_Arsym -_Atdcys s_Gwllyth [-]_Gwann ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Uberf cytuno_Be ti_Rhapers2u 'n_Arsym '_Atddyf bo_Adf '_Atddyf [=]_Gwann Ma_Bpres3u '_Atddyf [/=]_Gwann ma_Bpres3u '_Atddyf pethe_Egll yn_Arsym tueddol_Anscadu o_Arsym gael_Be 'i_Rhadib3bu canoli_Be '_Atddyf chydig_Banmeint bach_Anscadu yn_Arsym lleoliad_Anon oherwydd_Cyscyd maint_Egu y_YFB peth_Egu ti_Rhapers2u 'n_Arsym '_Atddyf bod_Be [=]_Gwann O'dd_Bamherff3u [/=]_Gwann o'dd_Bamherff3u 'na_Adf dros_Arsym chwe_Rhifol chant_Bpres3ll o_Rhapers3gu fobol_unk yna_Adf +_Gwsym 
<_Atdchw SB_Gwacr >_Atdde Oedd_Bamherff3u ._Atdt 
S2_Anon +_Gwsym ymm_Ebych w'thnos_unk d'wetha_Anscadu '_Atddyf ._Atdt 
ym_Ebych ._Atdt 
Ond_Arsym faswn_Bgorb1u i_Rhapers1u 'n_Arsym lecio_unk gweld_Be honna_Rhadangb yn_Arsym teithio_Be nawr_Adf ychydig_Rhaamh -_Atdcys ma_Adf [-]_Gwann [=]_Gwann tu_Egu [/=]_Gwann tu_Egu fas_Ebu o_Rhapers3gu lleoliad_Anon a_Cyscyd ymm_Ebych [-]_Gwann ._Atdt 
Ti_Rhapers2u 'n_Arsym '_Atddyf bod_Be ._Atdt 
Achos_Cyscyd ma_Bpres3u '_Atddyf 'na_Bandangd menywod_Egll ffantastic_unk ar_Arsym draws_Arsym y_YFB wlad_Ebu a_Cyscyd [=]_Gwann i_Rhapers1u i_Rhapers1u i_Arsym [/=]_Gwann i_Arsym hybu_Be nhw_Rhapers3ll hefyd_Adf ._Atdt 
[_Atdchw DIWEDD_Egu YR_YFB AIL_Rhitrefd GLIP_Egu ]_Atdde ._Atdt 
S4_Anon ym_Ebych ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u jest_Adf yn_Arsym meddwl_Egu bod_Be arferion_Egll siopa_Be pobol_Ebu 'di_Uberf newid_Be ti_Rhapers2u '_Atddyf bo_Adf '_Atddyf ._Atdt 
Mae_Bpres3u mor_Adf gyfleus_Anscadu rhoid_Bamherffamhers y_YFB cyfrifiadur_Egu ymlaen_Adf a_Cyscyd ymm_Ebych [=]_Gwann -_Atdcys myn_Egu [/=]_Gwann mynd_Be ar_Arsym hwnnw_Rhadangg a_Arsym ordro_Be petha_Egll dros_Arsym y_YFB we_Egbu ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be achos_Egu bod_Be y_YFB we_Egbu 'di_Uberf datblygu_Be gymaint_Anscadu dros_Arsym y_YFB blynyddoedd_Ebll [anadlu_i_mewn_yn_sydyn]_Gwann d'wytha_Anscadu '_Atddyf mae_Bpres3u o_Rhapers3gu mor_Adf gyfleus_Anscadu ._Atdt 
'_Atddyf D'wyt_Bpres2u i_Rhapers1u 'm_Adf yn_Arsym gor'od_Egu mynd_Be i_Arsym fewn_Arsym i_Arsym 'r_YFB car_Egu ._Atdt 
Ti_Rhapers2u 'm_Rhadib1u yn_Arsym gor'od_Egu chwilio_Be am_Arsym le_Egu i_Arsym barcio_Be ._Atdt 
Ti_Rhapers2u 'm_Rhadib1u yn_Arsym gor'od_Egu talu_Be i_Arsym barcio_Be ._Atdt 
ym_Ebych [=]_Gwann So_Adf [/=]_Gwann so_Adf '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be ia_Egu [-]_Gwann [=]_Gwann '_Atddyf Dw_Bpres1u '_Atddyf dw_Bpres1u '_Atddyf dw_Bpres1u [/=]_Gwann '_Atddyf dw_Bpres1u i_Rhapers1u 'm_Adf yn_Arsym meindio_Be '_Atddyf neud_Be y_YFB department_Gwest stores_Gwest [=]_Gwann Ma_Bpres3u 'n_Rhadib1ll [/=]_Gwann ma_Bpres3u 'n_Rhadib1ll drit_unk weithia_Adf '_Atddyf ond_Arsym '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Utra dueddol_Anscadu o_Arsym wario_Be gormod_Egu pan_Egu '_Atddyf dw_Bpres1u i_Rhapers1u yna_Adf [chwerthin]_Gwann ._Atdt 
S1_Anon [chwerthin]_Gwann ._Atdt 
S4_Anon Ond_Arsym ymm_Ebych [-]_Gwann ._Atdt 
ym_Ebych ._Atdt 
Ond_Arsym na_Rhaperth ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be ma_Bpres3u '_Atddyf arferion_Egll pobol_Ebu sy_Bpres3perth 'di_Uberf newid_Be ._Atdt 
ym_Ebych ._Atdt 
S1_Anon [anadlu_i_mewn_yn_sydyn]_Gwann ._Atdt 
S4_Anon Ia_Adf ._Atdt 
Arferion_Egll siopa_Be ._Atdt 
S1_Anon Beth_Rhagof amdanot_unk ti_Rhapers2u enwb_Anon ?_Atdt 
Achos_Cyscyd ma_Bandangg 'n_Utra rhaid_Egu i_Arsym fi_Rhapers1u '_Atddyf weud_Be '_Atddyf dw_Bpres1u i_Rhapers1u ddim_Adf yn_Arsym berson_Egu sy_Bpres3perth 'n_Rhadib1ll lecio_unk siopa_Be ar_Arsym y_YFB we_Egbu ._Atdt 
Ond_Arsym -_Atdcys s_Gwllyth [-]_Gwann '_Atddyf n'enwedig_unk os_Cyscyd wy_Bpres1u 'n_Uberf prynu_Be dillad_Egll ne_Anscadu '_Atddyf r'wbeth_Rhaamh fel'na_Adf +_Gwsym 
S3_Anon Hmm_Ebych ._Atdt 
S1_Anon +_Gwsym '_Atddyf dw_Bpres1u i'sie_Be '_Atddyf gweld_Be e_Rhapers3gu '_Atddyf ._Atdt 
'_Atddyf Dw_Bpres1u i'sie_Be '_Atddyf trio_Be fe_Rhapers3gu '_Atddyf mlaen_Adf ._Atdt 
A_Cyscyd '_Atddyf dw_Bpres1u i'sie_Be '_Atddyf hefyd_Adf yr_YFB instant_Gwest gratification_Gwest 'na_Adf o_Arsym dalu_Be amdano_Ar3gu fe_Uberf a_Cyscyd cerdded_Be mas_Egu o_Arsym 'r_YFB siop_Ebu gyda_Arsym bag_Egu pert_Anscadu disglair_Anscadu gyda_Arsym 'r_YFB +_Gwsym 
<_Atdchw SS_Gwest >_Atdde [chwerthin]_Gwann ._Atdt 
S1_Anon +_Gwsym peth_Egu '_Atddyf dw_Bpres1u i_Rhapers1u newydd_Anscadu 'i_Rhadib3bu -_Atdcys bry_Egu [-]_Gwann ._Atdt 
ym_Ebych ._Atdt 
Fasen_Bgorb1ll i_Arsym ddim_Egu yn_Arsym meddwl_Egu am_Arsym brynu_Be dillad_Egll ar_Arsym y_YFB we_Egbu ._Atdt 
S3_Anon Mmm_Ebych ._Atdt 
Wel_Ebych '_Atddyf dw_Bpres1u i_Rhapers1u yn_Arsym siopaholic_unk ma_Bpres3u 'n_Utra rhaid_Egu fi_Rhapers1u dd'eud_Be +_Gwsym 
S1_Anon [chwerthin]_Gwann ._Atdt 
S3_Anon +_Gwsym e_Rhapers3gu 's_Gwllyth pan_Egu '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Utra fach_Egu dio_Bpres3u 'ch_Rhadib2ll i_Arsym fy_Banmedd1u Anti_Ebu enwb_Anon ._Atdt 
<_Atdchw SB_Gwacr >_Atdde [chwerthin]_Gwann ._Atdt 
S3_Anon ym_Ebych ._Atdt 
Ag_Arsym ym_Ebych ma_Bpres3u '_Atddyf r'aid_Egu i_Arsym fi_Rhapers1u dd'eud_Be hefyd_Adf o'n_Bamherff1u i_Rhapers1u 'n_Utra berchennog_Egu ag_Arsym yn_Arsym rhedeg_Be siop_Ebu ddillad_Egll fy_Rhadib1u hun_Rhaatb am_Arsym gyfnod_Egu ._Atdt 
ym_Ebych ._Atdt 
A_Cyscyd ia_Adf ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u '_Atddyf r'un_unk peth_Egu a_Cyscyd ti_Rhapers2u ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Arsym lecio_unk mynd_Be i_Arsym siop_Ebu ._Atdt 
A_Cyscyd '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Arsym lecio_unk ymm_Ebych gwisgo_Be pethe_Egll sy_Bpres3perth 'n_Utra wahanol_Anscadu ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u '_Atddyf rioed_Adf wedi_Uberf poeni_Be am_Arsym wisgo_Be be_Rhagof '_Atddyf '_Atddyf dw_Bpres1u i'sio_Be '_Atddyf ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u [-]_Gwann [=]_Gwann Ma_Bpres3u '_Atddyf [/=]_Gwann ma_Bpres3u 'n_Rhadib1ll [-]_Gwann wel_Ebych ma_Bpres3u '_Atddyf nghwpwrdd_Egu dillad_Egll i_Rhapers1u fel_Cyscyd bocs_Egu gwisg_Ebu ffansi_Ebu bron_Ebu a_Cyscyd bod_Be ._Atdt 
Ma_Bpres3u 'n_Rhadib1ll -_Atdcys sa_Bgorb1u [-]_Gwann ._Atdt 
Ma_Bpres3u 'n_Utra llawn_Anscadu plu_Egll a_Cyscyd sequins_Gwest a_Arsym phopeth_Egu [chwerthin]_Gwann +_Gwsym 
<_Atdchw SB_Gwacr >_Atdde [chwerthin]_Gwann ._Atdt 
S3_Anon +_Gwsym dan_Arsym -_Atdcys hau_Be Glitter_Gwest ._Atdt 
S1_Anon Hmm_Ebych ._Atdt 
S3_Anon ym_Ebych ._Atdt 
S1_Anon '_Atddyf Dw_Bpres1u i'sie_Be '_Atddyf gweld_Be y_YFB bocs_Egu [aneglur]_Gwann 'ma_Adf ._Atdt 
<_Atdchw SB_Gwacr >_Atdde [chwerthin]_Gwann ._Atdt 
S3_Anon Ah_Ebych ie_Adf ._Atdt 
Ma_Bpres3u 'n_Utra werth_Egu 'i_Rhadib3gu weld_Be siŵr_Anscadu o_Arsym fod_Be ._Atdt 
ym_Ebych ._Atdt 
Ond_Arsym ie_Adf [-]_Gwann ._Atdt 
Ond_Arsym fyddai_Bamod3u mynd_Be ar_Arsym y_YFB we_Egbu ddim_Egu yn_Arsym rhoi_Be 'r_YFB un_Bancynnar boddhad_Egu i_Arsym fi_Rhapers1u ._Atdt 
A_Cyscyd [=]_Gwann '_Atddyf dw_Bpres1u [/=]_Gwann '_Atddyf dw_Bpres1u i_Rhapers1u hefyd_Adf +_Gwsym 
S1_Anon Na_Rhaperth ._Atdt 
Na_Adf finne_Rhacys1u '_Atddyf ._Atdt 
S3_Anon +_Gwsym yn_Arsym lecio_unk mynd_Be i_Arsym siopa_Be '_Atddyf bach_Anscadu ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'm_Adf yn_Arsym lecio_unk mynd_Be [-]_Gwann ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Arsym eitha_Adf '_Atddyf lecio_unk department_Gwest store_Gwest ond_Arsym well_Anscym gen'a_Ar1u '_Atddyf i_Arsym o_Arsym lawer_Banmeint fynd_Be i_Arsym siopa_Be '_Atddyf bach_Anscadu annibynnol_Anscadu ymm_Ebych lle_Rhagof '_Atddyf d'oes_Bpres3amhen 'na_Adf ddim_Egu ond_Arsym hyn_Rhadangd a_Cyscyd hyn_Rhadangd o_Rhapers3gu bethe_Egll i_Arsym gael_Be yn_Arsym y_YFB maint_Egu yna_Adf ._Atdt 
Mae_Bpres3u o'n_Bamherff1u brofiad_Egu mwy_Anscym arbennig_Anscadu yn_Arsym hytrach_Adf na_Uneg mynd_Be i_Rhapers1u r'ywle_Rhaamh lle_Anscadu ma_Bpres3u '_Atddyf nhw_Rhapers3ll 'n_Utra ca'l_Ebu i_Rhapers1u [-]_Gwann ti_Rhapers2u '_Atddyf bo_Adf '_Atddyf ._Atdt 
Lle_Rhagof ma_Bpres3u '_Atddyf miloedd_Ebll ar_Arsym filoedd_Ebll o_Rhapers3gu '_Atddyf r'un_unk peth_Egu yn_Arsym ca'l_Ebu 'i_Rhadib3bu creu_Be yn_Arsym rhad_Egu ._Atdt 
'_Atddyf Dw_Bpres1u i_Rhapers1u 'n_Arsym lecio_unk gwario_Be buddsoddi_Be yn_Arsym r'wbeth_Rhaamh fydd_Bdyf3u efo_Arsym fi_Rhapers1u am_Arsym flynyddoedd_Ebll ti_Rhapers2u 'n_Uberf gw'bod_Be ._Atdt 
A_Cyscyd -_Atdcys d_Gwllyth '_Atddyf dw_Bpres1u i_Rhapers1u 'n_Uberf meddwl_Be '_Atddyf falle_Adf '_Atddyf dyna_Adf pam_Adf fod_Be ymm_Ebych y_YFB siope_Ebll mawr_Anscadu 'ma_Adf yn_Arsym diodde_Be '_Atddyf hefyd_Adf achos_Egu ma_Bpres3u '_Atddyf nhw_Rhapers3ll 'di_Uberf mynd_Be yn_Arsym rhy_Adf fawr_Anscadu ._Atdt 
1610.709_Gwdig 